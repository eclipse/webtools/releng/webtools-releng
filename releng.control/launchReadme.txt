The launch file provided in this directory is 
an example only. It will need to be cusomized for 
particular machines, and particular needs. 
It is an example of how to run the build.xml file from the IDE, 
for local developer tests. It is not used as part of production builds.

In addition to the environment variables and properties in this launch config, 
the directory pointed to by LOCAL_BUILD_PROPERTIES_DIR can be used 
to supply many other properties. See the directorty 'localBuildExample' 
for a set of example files and properties. 

