#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2008, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
PIDFILE=cc.pid
echo;
if [ -f ${PIDFILE} ] ; then
  PID=`cat ${PIDFILE}`
  nthreads=`ps -mp $PID | wc -l`
  echo "    Number of threads in wtp's CC process: $nthreads"
else
        echo "    PID file (${PIDFILE}) does not exist."
        echo "        Either CC not running, or PID file deleted"
fi
echo
