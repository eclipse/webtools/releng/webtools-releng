#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2010, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************


PIDFILE=ccxvfb.pid

if [ -f ${PIDFILE} ] ; then
  echo "    Killing Xvfb process from PID file"
  PID=`cat ${PIDFILE}`
  kill -3 $PID
  # if permission denied, for example, then be sure not to remove PID file
  if [ $? ]
  then
          if kill -9 $PID ; then
            echo "    Xvfb process stopped"
            rm -f ${PIDFILE}
          else
            echo "    Xvfb process could not be stopped"
          fi
  else
    echo "    Could not kill the process."
  fi
else
        echo "    PID file (${PIDFILE}) does not exist."
        echo "        Either Xvfb not running, or PID file has been deleted"
fi
echo;

#!/usr/bin/env bash


PIDFILE=ccmetacity.pid

if [ -f ${PIDFILE} ] ; then
  echo "    Killing METACITY process from PID file"
  PID=`cat ${PIDFILE}`
  kill -3 $PID
  # if permission denied, for example, then be sure not to remove PID file
  if [ $? ]
  then
          if kill -9 $PID ; then
            echo "    METACITY process stopped"
            rm -f ${PIDFILE}
          else
            echo "    METACITY process could not be stopped"
          fi
  else
    echo "    Could not kill the process."
  fi
else
        echo "    PID file (${PIDFILE}) does not exist."
        echo "        Either METACITY not running, or PID file has been deleted"
fi
echo;