#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2008, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# example as used in script
    fromString="webtools/committers"
    if [ "patches" == $distribution ] 
    then
      toString="webtools/patches"
    else
      toString="webtools/downloads"
    fi
    replaceCommand="s!${fromString}!${toString}!g"

    # remember TODIR already has a slash
    perl -w -pi -e ${replaceCommand} ${TODIR}${dropDirName}/*.php

# example of one liner    
    find ./features/ -name "*.jar"  | sort | sed -e 's/\.jar$//' > features-jar.txt
    
# example of one liner     
    find ./ -name "consoleLogs.php"  | sed -iORIG -e 's/aDirectory\.closedir/aDirectory->close/' 
    