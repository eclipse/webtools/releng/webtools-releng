<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output
        method="html"
        indent="yes" />

    <xsl:template match="/">
        <html>
            <body>

                <xsl:apply-templates select="build" />

            </body>
        </html>
    </xsl:template>

    <xsl:template
        name="build"
        match="/build">
        <h2>Build</h2>
        <pre>
            <xsl:value-of select="@error" />
        </pre>


        <xsl:call-template name="target" />
        <xsl:call-template name="stacktrace" />
    </xsl:template>

    <xsl:template
        name="target"
        match="build/target">
        <h2>Targets exectued</h2>
        <xsl:for-each select="//target">
            <li>
                <xsl:text>ant target: </xsl:text>
                <xsl:value-of select="@name" />
                <xsl:text>   time:  </xsl:text>
                <xsl:value-of select="@time" />

            </li>
        </xsl:for-each>
    </xsl:template>

    <xsl:template
        name="stacktrace"
        match="/stacktrace">
        <h2>Stacktrace</h2>
        <pre>
            <xsl:value-of select="//stacktrace" />
        </pre>
    </xsl:template>
</xsl:stylesheet>