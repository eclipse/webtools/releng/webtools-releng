#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2006, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************


# remember to leave no slashes on filename in source command,
# (the commonVariations.shsource file, that is)
# so that users path is used to find it (first)
if [ -z $BUILD_INITIALIZED ]
then
   source commonVariations.shsource
   source ${RELENG_CONTROL}/commonComputedVariables.shsource
fi

# it is assumed we are executing this in RELENG_CONTROL or the parent of RELENG_CONTROL
RELENG_CONTROL=releng.control

# This script file is to help get builds started "fresh", when
# the ${RELENG_CONTROL} directory already exists on local file system.
# While it is in the cvs repository in ${RELENG_CONTROL}, it is
# meant to be executed from the parent directory
# of ${RELENG_CONTROL} on the file system.

# export is used, instead of checkout, just to avoid the CVS directories and since this code
# for a local build, there should never be a need to check it back in to CVS.


if [ ! -e ${RELENG_CONTROL} ]
then
	cd ..
	if [ ! -e ${RELENG_CONTROL} ]
	then	
    echo "${RELENG_CONTROL} does not exist as sub directory";
	exit 1;
	fi
fi

if [ -e ${RELENG_CONTROL}/cc.pid ] 
then 
	echo;
    echo "    PID file exists. Please stop CC first";
    echo;
    exit 2;
fi

echo
echo "    saving serialized CC state ..."
cp ${RELENG_CONTROL}/*.ser . >/dev/null 2>/dev/null

echo "    removing all of ${RELENG_CONTROL} ..."
rm -fr ${RELENG_CONTROL}/*
rm -fr ${RELENG_CONTROL}/.project
rm -fr ${RELENG_CONTROL}/.settings
mkdir -p ${RELENG_CONTROL}

# if running remotely, may want to use ext, and committer id, to avoid
# out of synch data from pserver shadow
if [ -z ${BUILD_CVS_READ_PROTOCOL} ]
then
    BUILD_CVS_READ_PROTOCOL=pserver
fi

if [ -z ${BUILD_CVS_READ_USER} ] 
then 
    BUILD_CVS_READ_USER=anonymous
fi

# This CONTROLTAG controls _only_ the version of releng.control that is fetched by build administrator
# It may, for example, contain a version of cc_config.xml where it, in turn, specifies 
# other branches to do non standard builds to use for tests (HEAD is the standard, production value) 
#CONTROLTAG=<exampleID>_tempTestBranch
CONTROLTAG=HEAD
echo "    checking out $CONTROLTAG of ${RELENG_CONTROL} from cvs "
echo "    using :${BUILD_CVS_READ_PROTOCOL}:${BUILD_CVS_READ_USER}@dev.eclipse.org: "
cvs -Q -f -d :${BUILD_CVS_READ_PROTOCOL}:${BUILD_CVS_READ_USER}@dev.eclipse.org:/cvsroot/webtools  export -d ${RELENG_CONTROL} -r $CONTROLTAG webtools.releng/${RELENG_CONTROL}

echo "    restoring serialized CC state ..."
mv *.ser ${RELENG_CONTROL} >/dev/null 2>/dev/null

echo "    making sure releng control files are executable and have proper EOL ..."
dos2unix ${RELENG_CONTROL}/*.sh* ${RELENG_CONTROL}/*.properties ${RELENG_CONTROL}/*.xml >/dev/null 2>>/dev/null
chmod +x ${RELENG_CONTROL}/*.sh > /dev/null
echo

