#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2008, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
#
# remember to leave no slashes on commonVariations in source command,
# so that users path is used to find it (first). But, path on
# commonComputedVariables means we expect to execute only our
# version

if [ -z $BUILD_INITIALIZED ]
then
# if releng_control not set, we assume we are already in releng_control directory
   if [ -z $RELENG_CONTROL ]
   then
        RELENG_CONROL=`pwd`
   fi
   pushd .
   cd ${RELENG_CONTROL}
   source commonVariations.shsource
   source ${RELENG_CONTROL}/commonComputedVariables.shsource
   popd
fi


echo "Running ant at priority " $JOB_NICER
echo "args to ant: " "$@" 
exec nice --adjustment $JOB_NICER "${RELENG_CONTROL}/ant.sh" "$@"
