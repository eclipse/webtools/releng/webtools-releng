#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2009, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
echo;
echo "    "`date`;

PIDFILE=/shared/webtools/releng.control/cc.pid


echo;
if [ -f ${PIDFILE} ] ; then
  PID=`cat ${PIDFILE}`
else
  echo "    PID file (${PIDFILE}) does not exist."
  echo "        Either CC not running, or PID file deleted"
  exit 5
fi
  # I've found lsof is in /user/sbin on some machines
  nhandles=`/usr/bin/lsof -p $PID | wc -l`
  echo "    Number of handles in wtp's CC process: $nhandles"
  echo;
  nthreads=`ps -mp $PID | wc -l`
  echo "    Number of threads in wtp's CC process: $nthreads"
  echo;
  top -c -b -n 1 -p$PID
echo;

ps -A u | grep "\(Xvfb\|PID\)" | grep -v grep
echo;
ps -A u | grep "\(metacity\|PID\)" | grep -v grep
echo;

