#*******************************************************************************
# Copyright (c) 2008, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
 #!/usr/bin/env bash

echo;
echo "   Kill webtools Cruisecontrol server";
./killcc.sh 

echo "   Kill any other process under 'webtools'";
pkill -f webtools
echo;
