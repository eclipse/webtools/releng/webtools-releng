#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2010, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# remember to leave no slashes on filename in source command,
# (the commonVariations.shsource file, that is)
# so that users path is used to find it (first)
if [ -z $BUILD_INITIALIZED ]
then
   source commonVariations.shsource
   source ${RELENG_CONTROL}/commonComputedVariables.shsource
fi

if [ -z $DISPLAYNUMBER ]
then
DISPLAYNUMBER=9
fi

if [ -z $XVFBFONTPATH ] 
then 
  XVFBFONTPATH=/usr/share/fonts/misc/
fi


# limit data space (ld) to 10m bytes. 
# limit stack space (ls) to 1m bytes.
# limit number of open files (lf) to 20
# set connection time out to 60 seconds
XVFBSCREEN=${BUILD_HOME}/tmp
mkdir -p  ${XVFBSCREEN}

#Xvfb :$DISPLAYNUMBER -screen 0 1600x1200x16 -fbdir "${XVFBSCREEN}" -fp "${XVFBFONTPATH}" -ld 5120 -ls 512 -lf 20 -to 60 -reset -bs -auth ${RELENG_CONTROL}/xvfb-auth.cfg  &
# be sure to see /etc/X11/xorg.conf to see what screens are supported
Xvfb :$DISPLAYNUMBER -screen 0 1280x1024x24 -fbdir "${XVFBSCREEN}" -fp "${XVFBFONTPATH}" &

XVFBPID=$!
echo $XVFBPID > ccxvfb.pid

# 
DISPLAY=:$DISPLAYNUMBER metacity --display=:$DISPLAYNUMBER --replace --sm-disable >/dev/null 2>&1 &
METACITYPID=$!
echo $METACITYPID > ccmetacity.pid

echo "DISPLAYNUMBER: " $DISPLAYNUMBER;
echo "XVFBFONTPATH: " $XVFBFONTPATH;
echo "XVFBSCREEN: " $XVFBSCREEN;
echo "XVFBPID: " $XVFBPID;
echo "METACITYPID: " $METACITYPID;


