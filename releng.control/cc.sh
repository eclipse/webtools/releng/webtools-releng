#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2006, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

ulimit -n 2048
echo "ulimit: " `ulimit -n`

# remember to leave no slashes on filename in source command,
# (the commonVariations.shsource file, that is)
# so that users path is used to find it (first)
if [ -z $BUILD_INITIALIZED ]
then
   source commonVariations.shsource
   source ${RELENG_CONTROL}/commonComputedVariables.shsource
fi

${RELENG_CONTROL}/xvfb-start.sh

jmxport="7000"
webport="7777"
rmiport="1099"
# its ok for these to be trivial, just used to prevent
# accidental use, no real security needed.
trivialUserName="wtp"
trivialPw="ballad"

rm -fr ./workspace

CCNAME="Webtools-"

# We want to execute CC itself in Java 5
export JAVA_HOME=${JAVA_5_HOME}

export JETTY_HOME=${CCDIR}


sh ${CCDIR}/cruisecontrol.sh -configfile ${RELENG_CONTROL}/cc_config.xml -jmxport $jmxport -rmiport $rmiport -webport $webport -user $trivialUserName -password $trivialPw -cchome $CCDIR -ccname ${CCNAME} -webapppath ${CCDIR}/webapps/cruisecontrol -jettyxml ${CCDIR}/etc/jetty.xml 1>out.txt 2>err.txt &


echo
echo "    Project Home: ${PROJECT_HOME}"
echo "    Build Home: ${BUILD_HOME}"
echo "    DISPLAY: ${DISPLAY}"
echo
echo "    JAVA_4_HOME: ${JAVA_4_HOME}"
echo
echo "    JAVA_5_HOME: ${JAVA_5_HOME}"
echo
echo "    JAVA_6_HOME: ${JAVA_6_HOME}"
echo
echo "    JAVA_7_HOME: ${JAVA_7_HOME}"
echo
echo "    ANT_HOME: ${ANT_HOME}"
echo
echo "    PATH: ${PATH}"
echo
echo "    BASEOS: ${BASEOS}"
echo "    BASEWS: ${BASEWS}"
echo "    BASEARCH: ${BASEARCH}"
echo
echo "    LOCAL_BUILD_PROPERTIES_DIR: ${LOCAL_BUILD_PROPERTIES_DIR}"
echo
echo "    remember to check that X virtual frame buffer is running for display :$DISPLAYNUMBER for headless unit tests";
echo
echo "    IBM_JAVA_OPTIONS: ${IBM_JAVA_OPTIONS}"
echo

ps -ef | grep Xvfb
echo;
