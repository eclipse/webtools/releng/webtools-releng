#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2008, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# it is assumed we are executing this in COMMON_PAGES or the parent of COMMON_PAGES
COMMON_PAGES=commonPages

# This script file is to help get builds started "fresh", when
# the ${COMMON_PAGES} directory already exists on local file system.
# While it is in the cvs repository in ${COMMON_PAGES}, it is
# meant to be executed from the parent directory
# of ${COMMON_PAGES} on the file system.

# export is used, instead of checkout, just to avoid the CVS directories and since this code
# for a local build, there should never be a need to check it back in to CVS.


if [ ! -e ${COMMON_PAGES} ]
then
	cd ..
	if [ ! -e ${COMMON_PAGES} ]
	then	
    echo "${COMMON_PAGES} does not exist as sub directory";
	exit 1;
	fi
fi

echo

current=`pwd`
echo "    removing all of ${COMMON_PAGES} from ${current} ..."
rm -fr downloadsites
rm -fr ${COMMON_PAGES}/*
rm -fr ${COMMON_PAGES}/.project
rm -fr ${COMMON_PAGES}/.settings
mkdir -p ${COMMON_PAGES}
mkdir -p committers

echo "    checking out head of ${COMMON_PAGES} from cvs ..."
cvs -Q -f -d :pserver:anonymous@dev.eclipse.org:/cvsroot/webtools  export -d downloadsites -r HEAD webtools.releng/downloadsites

echo "    copying commonPages"
cp -r downloadsites/webtools/commonPages/* ${COMMON_PAGES}
echo "    copying artifacts to committers"
cp -r downloadsites/webtools/artifacts/* committers

echo

