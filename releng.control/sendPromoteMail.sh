#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2008, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

PROJECT=$1
if [ -z $PROJECT ]
then
    echo "must provide project name as first argument"
    exit 9;
fi
TODIR=$2
if [ -z $TODIR ]
then
    echo "must provide TODIR name as second argument"
    exit 8;
fi
DROPDIR=$3
if [ -z $DROPDIR ]
then
    echo "must provide DROPDIR name as third argument"
    exit 7;
fi

# ideally, the user executing this mail will have this special file in their home directory,
# that can specify a custom 'from' variable, but still you must use your "real" ID that is subscribed
# to the wtp-dev mailing list
#   set from="\"Your Friendly WTP Builder\" <real-subscribed-id@real.address>"
# correction ... doesn't work. Seems the subscription system set's the "from" name, so doesn't work when 
# sent to mail list (just other email addresses)
export MAILRC=~/.buildermailrc

SUBJECT="Smoke Test Candidate Build for $PROJECT: $DROPDIR"

# wtp-dev for promotes, wtp-releng for smoketest requests
TO="wtp-dev@eclipse.org"

#make sure reply to goes back to the list
REPLYTO="wtp-dev@eclipse.org"
#we need to "fix up" TODIR since it's in file form, not URL
URLTODIR=${TODIR##*${DOWNLOAD_ROOT}}
mail -s "$SUBJECT" -R "$REPLYTO" "$TO"  <<EOF 
 

Download Page: 
http://download.eclipse.org$URLTODIR$DROPDIR

This build will be pushed to the Eclipse Simrel 
aggregate site on Mon or Tues next week, assuming 
that no one has voiced an objection on this list 
beforehand. 

Smoke tests:
https://wiki.eclipse.org/WTP_Smoke_Test_Scenarios_R39

Past smoke test results:
http://wiki.eclipse.org/WTP_Smoke_Test_Results

EOF

