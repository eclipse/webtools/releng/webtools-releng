#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2008, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
PIDFILE=cc.pid
echo;
if [ -f ${PIDFILE} ] ; then
  echo "    Killing CC process from PID file"
  PID=`cat ${PIDFILE}`
  kill -3 $PID
  # if permission denied, for example, then be sure not to remove PID file
  if [ $? ] 
  then
	  if kill -9 $PID ; then
	    echo "    CC process stopped"
	    rm -f ${PIDFILE}
	  else
	    echo "    CC process could not be stopped"
	  fi
  else
    echo "    Could not kill the process."  
  fi
else
	echo "    PID file (${PIDFILE}) does not exist."
	echo "        Either CC not running, or PID file deleted"
fi
echo;

./xvfb-stop.sh
