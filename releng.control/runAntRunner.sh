#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2011, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
# specify devworkspace and JRE to use to runEclipse


devworkspace=./workspace-antRunner

BUILDFILE=$1
if [ -e $BUILDFILE ]
then
    BUILDFILESTR=" -file $BUILDFILE"
    shift
fi

extraArgs="$@"

echo "BUILDFILE: $BUILDFILE"
echo "extraArgs: ${extraArgs}"


# remember to leave no slashes on filename in source command,
# (the commonVariations.shsource file, that is)
# so that users path is used to find it (first)
if [ -z $BUILD_INITIALIZED ]
then
   source commonVariations.shsource
   source ${RELENG_CONTROL}/commonComputedVariables.shsource
fi

export JAVA_HOME=${JAVA_5_HOME}

devJRE=$JAVA_HOME/jre/bin/java

# -Xms128M -Xmx256M 
ibmDevArgs="-Dosgi.ws=gtk -Dosgi.os=linux -Dosgi.arch=x86 ${extraArgs}"

devArgs=$ibmDevArgs

echo dev:          $0
echo devworkspace: $devworkspace
echo devJRE:       $devJRE
#$devJRE -version
echo

if [ -n ${ECLIPSE_EXE} -a -x ${ECLIPSE_EXE} ]
then 
   ${ECLIPSE_EXE}  --launcher.suppressErrors  -nosplash -console -data $devworkspace -application org.eclipse.ant.core.antRunner $BUILDFILESTR -vm $devJRE -vmargs $devArgs
   RC=$?
else
   echo "ERROR: ECLIPSE_EXE is not defined to executable eclipse"
   RC=1001
fi 
exit $RC
