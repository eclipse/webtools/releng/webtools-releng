#!/usr/bin/env bash

# simple function that can be called via 'find' that removes and displays name of removed directory
function removeIf () 
{
       # echo "arg: $1";
       
       foundDirectory=$1
       if [ -z "${foundDirectory}" ]
         then
           echo "ERROR: No argument. This function requires a directory as an argument. " ;
           return 1;
         fi
      
       
       if [ ! \( -d "${foundDirectory}" \) ] 
       then
         echo "ERROR: " "${foundDirectory}" ", is not a directory. This function requires a directory as an argument. "
         return 2;
       fi 
       
       # should already be in foundDirectory, if execDir used (and execDir, in a 'find' is recommended, as more secure)
       #cd $foundDirectory
       
       rm -fr "${foundDirectory}"
       rcode=$?
       if [ $rcode -eq 0 ]
       then 
         echo "               removed: " "${foundDirectory}";
       else
         echo "WARNING: rm returned non zero return code: " $rccode ". could not remove " "${foundDirectory}";
       fi 
       
       return $rcode;
}

# function that can remove a directory (e.g. via find) but checks to make sure not to remove the last one (or, last 'saveAtLeast')
function removeArtifactsDirIf ()
{
       # echo "arg: $1";
       foundDirectory=$1
       if [ -z "${foundDirectory}" ]
         then
           echo "ERROR: No argument. This function requires a directory as an argument. " ;
           return 1;
         fi
       
       nSave=$2
       
       if [ -z $nSave ]
       then
          nSave=1;
       fi   
       
       if [ ! \( -d "${foundDirectory}" \) ] 
       then
         echo "ERROR: " "${foundDirectory}" ", is not a directory. This function requires a directory as an argument. "
         return 2;
       fi 
       
       # should already be in appropriate directory, if execDir in 'find' is used (and execDir, in a 'find' is recommended, as more secure)
       # cd "${foundDirectory}"
       # move up one so we can examine syblings
       #cd ..
       currentDirectory=`pwd`
       echo "               current working directory: $currentDirectory";
       ndirs=`ls -lA | wc -l`
       ndirs=$(($ndirs - 1)); # don't count the "totals" line
       # echo "NDirs: $ndirs"
       
       # if only one left, do not remove it, no matter how old
       # or, as improved ... if less than or equal to nSave is left, do not remove
       if [ $ndirs -le $nSave ]
       then
           return 0;
       fi
       # To have no directories is unexpected, since otherwise this method should not have been called. 
       # So, this check is just a safety check.
       if [ $ndirs -lt 1 ]
       then
           exit 101;
       fi
       
       rm -fr "${foundDirectory}"
       rcode=$?
       if [ $rcode -eq 0 ]
       then 
         echo "               removed: " "${foundDirectory}";
       else
         echo "WARNING: rm returned non zero return code: " $rccode ". could not remove " "${foundDirectory}";
       fi 
       
       return $rcode;

}
