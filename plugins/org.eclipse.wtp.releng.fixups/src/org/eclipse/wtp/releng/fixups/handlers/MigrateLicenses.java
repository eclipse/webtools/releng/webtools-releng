/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.fixups.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.wtp.releng.fixups.MigrateLicenseSupport;


public class MigrateLicenses extends AbstractHandler {

    public MigrateLicenses() {
    }

    public Object execute(ExecutionEvent event) throws ExecutionException {

        return new MigrateLicenseSupport().checkAndMigrate();
    }


}
