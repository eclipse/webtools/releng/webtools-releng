/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.fixups;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

/**
 * Simple tool to make features more consistent. Check license property is the
 * standard value, and if not fixes that, and write new license.html file.
 * 
 * @author davidw
 * 
 */

public class MigrateLicenseSupport {


    class SourceTemplateProject {
        public String sourceTemplateDirectory = "sourceTemplateFeature/";
        IProject      project;

        public SourceTemplateProject(IProject project) {
            this.project = project;
        }

        public IFile getFile(String filename) {
            return project.getFile(sourceTemplateDirectory + filename);

        }

        public String getName() {
            return this.project.getName() + "[sourceTemplateFeature]";
        }
    }

    /**
     * space, tab
     */
    private static final String ANY_NONEOL_WHITESPACE            = "[ \\t]*";
    private static final String ANY_END                          = ".*$";
    private static final String ANY                              = ".*";
    private static final String ONE_PROPERTY_DELIMITER           = "[=:]";
    private static final String START_OF_LINE                    = "^";
    private static final String KEY_WORD                         = "license";
    private static final String URL_KEY_WORD                     = "licenseURL";
    /*
     * remember a slash '\' denotes continuation in a properties file, and the
     * regex expression needs to escape that, so "\\", and to get in a string,
     * each needs to be escaped, so that's why we need "\\\\" in continuation
     * regex pattern
     */
    private static final String CONTINUATION                     = "\\\\ *$";
    int                         CR                               = 0x0d;
    int                         LF                               = 0x0a;
    /**
     * This expression is not the general rule for Java property key/values,
     * but should cover most cases. (To cover all cases, would take full
     * parsing of the whole file, not one line at a time like we are doing.
     */
    private static String       LICENSE_KEY                      = START_OF_LINE + KEY_WORD + ANY_NONEOL_WHITESPACE + ONE_PROPERTY_DELIMITER + ANY_END;
    private static String       LICENSE_URL                      = START_OF_LINE + URL_KEY_WORD + ANY_NONEOL_WHITESPACE + ONE_PROPERTY_DELIMITER + ANY_END;
    private static String       LICENSE_FILE_LINE                = START_OF_LINE + ANY_NONEOL_WHITESPACE + "license.html" + ANY_END;
    private static String       EPL_FILE_LINE                    = START_OF_LINE + ANY_NONEOL_WHITESPACE + "epl-2.0.html" + ANY_END;
    private static String       FEATURE_START                    = START_OF_LINE + ANY + "<feature" + ANY_END;
    private static String       FEATURE_END                      = START_OF_LINE + ANY + ">" + ANY_END;

    private static final String PLUGINID                         = "org.eclipse.wtp.releng.fixups";
    private static final String CONSOLE_NAME                     = "Releng Console";
    private static boolean      DEBUG                            = false;
    private ArrayList           featureProjects                  = new ArrayList();
    private ArrayList           featureSourceProjects            = new ArrayList();
    private ArrayList           featureProjectsMissingProperties = new ArrayList();
    private ArrayList           featureProjectsMissingLicense    = new ArrayList();
    private ArrayList           featuresModified                 = new ArrayList();
    private ArrayList           featuresOkNotModified            = new ArrayList();
    private ArrayList           featuresCouldNotBeModified       = new ArrayList();
    private int                 nFeatures;
    private long                starttime;

    public MigrateLicenseSupport() {
    }

    /**
     * the command has been executed, so extract extract the needed
     * information from the application context.
     */
    public Object checkAndMigrate() throws ExecutionException {

        // assume return is ok, unless set otherwise.
        Object returnobject = IStatus.OK;
        starttime = System.currentTimeMillis();
        IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
        initCounterArrays();

        for (int j = 0; j < projects.length; j++) {
            IProject project = projects[j];
            IFile file = project.getFile("feature.xml");
            if ((file != null) && file.exists() && !project.getName().equals("org.eclipse.license")) {
                nFeatures++;
                returnobject = processProjectDirectory(project);
                // check if these feature has sourceTemplateFeature
                // if so, check it and fix it up
                IFolder sourceTemplate = project.getFolder("sourceTemplateFeature");
                if ((sourceTemplate != null) && (sourceTemplate.exists())) {
                    nFeatures++;
                    SourceTemplateProject sourceTemplateProject = new SourceTemplateProject(project);
                    returnobject = processProjectDirectory(sourceTemplateProject);
                }
            }
        }

        reportSummaryOfResults();


        return returnobject;
    }

    private Object processProjectDirectory(IProject project) {
        // assume return is ok, unless set otherwise.
        // TODO: we just overwrite status in for loop. Should not overwrite
        // "not ok".
        Object returnobject = IStatus.OK;
        String propNamePath = "feature.properties";
        IFile propfile = project.getFile(propNamePath);
        if ((propfile != null) && propfile.exists()) {
            featureProjects.add(project);
            try {
                fixup(propfile, project);
            }
            catch (IOException e) {
                returnobject = new Status(0, PLUGINID, e.getMessage());
            }
            catch (CoreException e) {
                returnobject = new Status(0, PLUGINID, e.getMessage());
            }
        }
        else {
            // we just save these for counting. Seems hard
            // to easily know what is correct action, if any.
            featureProjectsMissingProperties.add(project);
        }
        return returnobject;
    }

    private Object processProjectDirectory(SourceTemplateProject project) {
        // assume return is ok, unless set otherwise.
        // TODO: we just overwrite status in for loop. Should not overwrite
        // "not ok".
        Object returnobject = IStatus.OK;
        String propNamePath = "feature.properties";
        IFile propfile = project.getFile(propNamePath);
        if ((propfile != null) && propfile.exists()) {
            featureSourceProjects.add(project);
            try {
                fixup(propfile, project);
            }
            catch (IOException e) {
                returnobject = new Status(0, PLUGINID, e.getMessage());
            }
            catch (CoreException e) {
                returnobject = new Status(0, PLUGINID, e.getMessage());
            }
        }
        else {
            // we just save these for counting. Seems hard
            // to easily know what is correct action, if any.
            featureProjectsMissingProperties.add(project);
        }
        return returnobject;
    }

    private void reportSummaryOfResults() {
        MessageConsole myConsole = findConsole(CONSOLE_NAME);
        MessageConsoleStream out = myConsole.newMessageStream();
        myConsole.activate();
        out.println();
        out.println("\tTotal number of features found: " + nFeatures);
        out.println("\t\t Total number of primary features found: " + featureProjects.size());
        out.println("\t\t Total number of source features found: " + featureSourceProjects.size());

        out.println();
        report(out, featureProjectsMissingProperties, "The following feature(s) contained no feature.properties file");
        report(out, featureProjectsMissingLicense, "The following feature(s) contained no license property");
        report(out, featuresOkNotModified, "The following feature(s) were found ok, and not modified");
        report(out, featuresModified, "The following feature(s) were modified");
        report(out, featuresCouldNotBeModified, "The following feature(s) needed to be modified, but for some reason could not be. \n\tThis is likely due to a non-standard (but legal) way of writing property keys and values");

        out.println();
        long elapsedTime = System.currentTimeMillis() - starttime;
        out.println("\tElapsed processing time : " + elapsedTime + " ms");
        out.println();
    }

    /*
     * Make sure arrays are cleared, incase instance is ran more than once.
     */
    private void initCounterArrays() {
        featureProjects.clear();
        featureSourceProjects.clear();
        featureProjectsMissingProperties.clear();
        featureProjectsMissingLicense.clear();
        featuresModified.clear();
        featuresOkNotModified.clear();
        featuresCouldNotBeModified.clear();
    }

    private void fixup(IFile propfile, IProject project) throws IOException, CoreException {



        Properties featureproperties = new Properties();
        featureproperties.load(propfile.getContents());
        String license = featureproperties.getProperty("license");
        if (license == null) {
            featuresOkNotModified.add(project);
        }
        else {
            if (rewrite(propfile, project)) {
                featuresModified.add(project);
                // also, if we tweak license property, we'll blindly remove licesne.html and epl files.
                removeLicenseHtml(project);
            }
            else {
                featuresCouldNotBeModified.add(project);
            }
        }

    }



    private String inferEOL(IFile featureProperties) throws CoreException, IOException {
        InputStream contentToPeekForEOL = featureProperties.getContents();
        String likelyEOL = computeEOL(contentToPeekForEOL);
        contentToPeekForEOL.close();
        return likelyEOL;
    }

    private String computeEOL(InputStream contentToPeekForEOL) throws IOException {
        int linuxLF = 0;
        int windowsCRLF = 0;
        int macCR = 0;
        int MAX = 1000;
        int count = 0;
        // simplist checking up to 1000 chars
        while ((contentToPeekForEOL.available() > 0) && (count < MAX)) {
            int c = contentToPeekForEOL.read();
            if (c != -1) {
                count++;
                if (c == LF) {
                    linuxLF++;
                }
                else if (c == CR) {
                    // two cases
                    int c1 = contentToPeekForEOL.read();
                    if (c1 != -1) {
                        count++;
                        if (c1 == LF) {
                            windowsCRLF++;
                        }
                        else if (c1 == CR) {
                            macCR++;
                            macCR++;
                        }
                        else {
                            macCR++;
                        }
                    }
                }
            }
        }
        if (DEBUG) {
            System.out.println("linuxLF: " + linuxLF);
            System.out.println("windowsCRLF: " + windowsCRLF);
            System.out.println("macCR: " + macCR);
        }
        String result = null;
        if (linuxLF > windowsCRLF) {
            if (linuxLF > macCR) {
                result = "" + ((char) LF);
            }
            else {
                result = "" + ((char) CR);
            }
        }
        else {
            if (windowsCRLF > macCR) {
                result = "" + ((char) CR) + ((char) LF);
            }
            else {
                result = "" + ((char) CR);
            }
        }

        return result;
    }

    private void closereader(BufferedReader br) {
        if (br != null) {
            try {
                br.close();
            }
            catch (IOException e) {
                // weirdness
                e.printStackTrace();
            }
        }
    }

    private boolean removeLicenseHtml(IProject project) {
        // in addition to removing fields from features.properties, if we are re-writing it, we
        // will blindly assume the license.html and epl-2.0.html need to be removed, if they exist.

        IFile license = project.getFile("license.html");
        IFile epl = project.getFile("epl-2.0.html");
        IFile buildProperties = project.getFile("build.properties");
        IFile featureFile = project.getFile("feature.xml");
        boolean result = commonRemoveFiles(license, epl, buildProperties, featureFile);
        return result;
    }

    private void report(MessageConsoleStream out, ArrayList tocheck, String message) {



// IWorkbenchPage page = ...;//obtain the active page
// String id = IConsoleConstants.ID_CONSOLE_VIEW;
// IConsoleView view = (IConsoleView) page.showView(id);
// view.display(myConsole);


        if ((tocheck != null) && (tocheck.size() > 0)) {
            out.println();
            out.println("\t" + message);
            out.println("\t\tCount: " + tocheck.size());
            for (Object object : tocheck) {
                String name = null;
                if (object instanceof IProject) {
                    name = ((IProject) object).getName();
                }
                else {
                    name = ((SourceTemplateProject) object).getName();
                }
                out.println("\t\t" + name);
            }
            out.println();
        }
    }

    private MessageConsole findConsole(String name) {

        MessageConsole myConsole = null;
        ConsolePlugin plugin = ConsolePlugin.getDefault();
        IConsoleManager conMan = plugin.getConsoleManager();
        IConsole[] existing = conMan.getConsoles();
        for (int i = 0; i < existing.length; i++) {
            if (name.equals(existing[i].getName())) {
                myConsole = (MessageConsole) existing[i];
            }
        }
        if (myConsole == null) {
            // no console found, so create a new one
            myConsole = new MessageConsole(name, null);
            conMan.addConsoles(new IConsole[]{myConsole});
        }

        return myConsole;
    }

    private void fixup(IFile propfile, SourceTemplateProject project) throws IOException, CoreException {

        Properties featureproperties = new Properties();
        featureproperties.load(propfile.getContents());
        String license = featureproperties.getProperty("license");
        if (license == null) {
            featuresOkNotModified.add(project);
        }
        else {
            if (rewrite(propfile, project)) {

                featuresModified.add(project);
                // also, if we tweak license property, we'll blindly remove licesne.html and epl files.
                removeLicenseHtml(project);
            }
            else {
                featuresCouldNotBeModified.add(project);
            }
        }
    }

    private boolean rewrite(IFile propfile, IProject project) throws CoreException, IOException {


        /*
         * We rewrite line by line to preserve existing comments and spaceing.
         * The idea is to read and echo old one until we find "license=". At
         * that point we read and echo from standard properties file (which is
         * why its required that file contain only the exact license property
         * we want). We skip old license property lines until we find a line
         * that does not end in conttinuation character. Then, echo the rest
         * of the old stuff. Remember, an "end of line" need not be platform's
         * EOL, it could be any of \n, \r, \r\n, etc. Technically, what ever
         * we find in use, is what we should rewite. (Not sure how "readline"
         * works, on non-platform EOLs?).
         */
        IFile featureProperties = project.getFile("feature.properties");

        return rewriteFeaturePropertiesCommon(featureProperties);
    }

    private boolean rewriteFeaturePropertiesCommon(IFile featureProperties) throws CoreException, IOException {
        boolean result = false;
        String inferredEOL = inferEOL(featureProperties);
        BufferedReader br = null;
        StringWriter bw = null;
        boolean foundKey = false;
        try {
            File featurePropertiesFile = new File(featureProperties.getLocationURI());
            br = new BufferedReader(new FileReader(featurePropertiesFile));
            bw = new StringWriter();

            String oneline = null;

            do {
                oneline = br.readLine();
                if (oneline != null) {
                    if (oneline.matches(LICENSE_KEY) || oneline.matches(LICENSE_URL)) {
                        foundKey = true;
                        // flush (read without re-writing) old input to end of property
                        while ((oneline != null) && oneline.matches(".*" + CONTINUATION)) {
                            oneline = br.readLine();
                        }
                        bw.write("# license and licenseURL properties were removed as a result to migrating to new PDE license support. " + inferredEOL);
                        bw.write("#    Those properties are now added at build time. See http://wiki.eclipse.org/Equinox/p2/License_Mechanism. " + inferredEOL);
                    }
                    else {
                        bw.write(oneline + inferredEOL);
                    }
                }
            }
            // end of do while
            while (oneline != null);
            closereader(br);

            if (!foundKey) {
                System.out.println("ERROR: Key/value was intially found via properties api, but we did not find key while trying to remove it via rewrite.");
                System.out.println("       This is likely due to non-standard (but technically legal) way of writing property keys and values");
                System.out.println("       Check (and manually fix) the file " + featureProperties.getLocation());
                result = false;
            }
            else {
                StringBufferInputStream newcontent = new StringBufferInputStream(bw.toString());
                featureProperties.setContents(newcontent, true, true, null);
                // if we got through all that without exception, all is ok
                result = true;
            }
        }

        catch (FileNotFoundException e) {
            e.printStackTrace();
            result = false;
        }
        catch (IOException e) {
            e.printStackTrace();
            result = false;
        }
        catch (CoreException e) {
            e.printStackTrace();
            result = false;
        }
        finally {
            closereader(br);
        }

        return result;
    }


    private boolean rewrite(IFile propfile, SourceTemplateProject project) throws CoreException, IOException {

        /*
         * We rewrite line by line to preserve existing comments and spaceing.
         * The idea is to read and echo old one until we find "license=". At
         * that point we read and echo from standard properties file (which is
         * why its required that file contain only the exact license property
         * we want). We skip old license property lines until we find a line
         * that does not end in conttinuation character. Then, echo the rest
         * of the old stuff. Remember, an "end of line" need not be platform's
         * EOL, it could be any of \n, \r, \r\n, etc. Technically, what ever
         * we find in use, is what we should rewite. (Not sure how "readline"
         * works, on non-platform EOLs?).
         */
        IFile featureProperties = project.getFile("feature.properties");

        return rewriteFeaturePropertiesCommon(featureProperties);
    }


    private boolean removeLicenseHtml(SourceTemplateProject project) {
        // in addition of features.properties, if we are re-writing it, we
        // will blindly assume the license.html file also needs to be replaced
        // with standard one.

        IFile license = project.getFile("license.html");
        IFile epl = project.getFile("epl-2.0.html");
        IFile buildProperties = project.getFile("build.properties");
        // for a SourceTemplateProject, there is no feature.xml file
        boolean result = commonRemoveFiles(license, epl, buildProperties, null);
        return result;
    }

    private boolean commonRemoveFiles(IFile license, IFile epl, IFile buildProperties, IFile featureFile) {
        boolean result = true;
        try {
            if ((license != null) && license.exists()) {
                license.delete(true, null);
            }
            if ((epl != null) && epl.exists()) {
                epl.delete(true, null);
            }
            if ((buildProperties != null) && buildProperties.exists()) {
                result = rewriteBuildProperties(buildProperties);
            }
            if (result && (featureFile != null) && (featureFile.exists())) {
                result = rewriteFeatureXML(featureFile);
            }
        }
        catch (CoreException e) {
            e.printStackTrace();
            result = false;
        }
        catch (IOException e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    private boolean rewriteFeatureXML(IFile featureFile) throws CoreException, IOException {
        boolean result = false;
        String inferredEOL = inferEOL(featureFile);
        BufferedReader br = null;
        StringWriter bw = null;
        try {
            File featurePropertiesFile = new File(featureFile.getLocationURI());
            br = new BufferedReader(new FileReader(featurePropertiesFile));
            bw = new StringWriter();

            String oneline = null;

            do {
                oneline = br.readLine();
                if (oneline != null) {
                    if (oneline.matches(FEATURE_START)) {
                        // write/flush feature tag attributes until line with end character ('>')
                        bw.write(oneline + inferredEOL);
                        boolean done = false;
                        do {
                            oneline = br.readLine();
                            if (oneline != null) {
                                if (oneline.matches(FEATURE_END)) {
                                    // get inferred indent, remove trailing '>'
                                    String inferredIndent = inferrIndent(oneline);
                                    oneline = removeChar(oneline, '>');
                                    bw.write(oneline + inferredEOL);
                                    // then add new attributes 
                                    bw.write(inferredIndent + "license-feature=\"org.eclipse.license\"" + inferredEOL);
                                    bw.write(inferredIndent + "license-feature-version=\"1.0.0.qualifier\"" + ">" + inferredEOL);
                                    done = true;
                                }
                                else {
                                    bw.write(oneline + inferredEOL);
                                }
                            }
                        }
                        // end of do while
                        while ((oneline != null) && !done);
                        // after handline initial element change, simply read/write rest of feature.xml file
                        do {
                            oneline = br.readLine();
                            if (oneline != null) {
                                bw.write(oneline + inferredEOL);
                            }
                        }
                        while (oneline != null);
                    }
                    else {
                        bw.write(oneline + inferredEOL);
                    }
                }
            }
            // end of do while
            while (oneline != null);
            closereader(br);


            StringBufferInputStream newcontent = new StringBufferInputStream(bw.toString());
            featureFile.setContents(newcontent, true, true, null);
            // if we got through all that without exception, all is ok
            result = true;

        }

        catch (FileNotFoundException e) {
            e.printStackTrace();
            result = false;
        }
        catch (IOException e) {
            e.printStackTrace();
            result = false;
        }
        catch (CoreException e) {
            e.printStackTrace();
            result = false;
        }
        finally {
            closereader(br);
        }

        return result;
    }

    private String removeChar(String oneline, char c) {
        int pos = oneline.lastIndexOf(c);
        String result = oneline.substring(0, pos);
        result = result + oneline.substring(pos + 1);
        return result;
    }

    private String inferrIndent(String oneline) {
        String inferredIndent = "";
        for (int i = 0; i < oneline.length(); i++) {
            char wsChar = oneline.charAt(i);
            if ((wsChar == ' ') || (wsChar == '\t')) {
                inferredIndent = inferredIndent + wsChar;
            }
            else {
                // break out at first non whitespace
                break;
            }
        }
        return inferredIndent;
    }

    /*
    * We make the strong assumption that each property occurs on its own line (and is not on the same line as bin.includes.
    * TODO: known problem that just removing a property line can leave an extra comma and/or continuation character.
    */
    private boolean rewriteBuildProperties(IFile buildProperties) throws CoreException, IOException {

        boolean result = false;
        String inferredEOL = inferEOL(buildProperties);
        BufferedReader br = null;
        StringWriter bw = null;
        try {
            File featurePropertiesFile = new File(buildProperties.getLocationURI());
            br = new BufferedReader(new FileReader(featurePropertiesFile));
            bw = new StringWriter();

            String oneline = null;

            do {
                oneline = br.readLine();
                if (oneline != null) {
                    if (oneline.matches(LICENSE_FILE_LINE) || oneline.matches(EPL_FILE_LINE)) {
                        // assume on a line by itself, so just do nothing: do not write that one line.
                    }
                    else {
                        bw.write(oneline + inferredEOL);
                    }
                }
            }
            // end of do while
            while (oneline != null);
            closereader(br);


            StringBufferInputStream newcontent = new StringBufferInputStream(bw.toString());
            buildProperties.setContents(newcontent, true, true, null);
            // if we got through all that without exception, all is ok
            result = true;

        }

        catch (FileNotFoundException e) {
            e.printStackTrace();
            result = false;
        }
        catch (IOException e) {
            e.printStackTrace();
            result = false;
        }
        catch (CoreException e) {
            e.printStackTrace();
            result = false;
        }
        finally {
            closereader(br);
        }

        return result;
    }

}
