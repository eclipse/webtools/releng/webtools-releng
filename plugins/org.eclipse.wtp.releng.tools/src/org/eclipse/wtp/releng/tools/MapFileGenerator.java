/*******************************************************************************
 * Copyright (c) 2007, 2009 IBM Corporation and others. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: IBM Corporation - initial API and implementation
 * 
 * This Class was originally copied and modified from similar package in Orbit, org.eclipse.orbit.releng.tools
 * Thanks to DJ and others. 
 *******************************************************************************/
package org.eclipse.wtp.releng.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * <pre>
 * &lt;code&gt;
 *         
 *    &lt;!-- This declares the task to ant. It needs to be tweaked to point to the jar containing the task --&gt;
 *    &lt;taskdef
 *    classname=&quot;org.eclipse.orbit.internal.releng.tools.MapFileGenerator&quot;
 *    name=&quot;eclipse.mapGenerator&quot;&gt;
 *    &lt;classpath&gt;
 *    &lt;pathelement path=&quot;${classpath}&quot; /&gt;
 *    &lt;pathelement
 *    location=&quot;${builder.home}/scripts/tools/mapGenerator.jar&quot; /&gt;
 *    &lt;/classpath&gt;
 *    &lt;/taskdef&gt;
 *   
 *   
 *   
 *   
 *    &lt;target name=&quot;default&quot;&gt;
 *    &lt;eclipse.mapGenerator
 *    root=&quot;${topBuildDirectory}/${buildLabel}/bundles&quot;
 *    addressPrefix=&quot;http://download.eclipse.org/tools/orbit/committers/drops/${buildLabel}/bundles/&quot;
 *    inputFilePluginVersions=&quot;${topBuildDirectory}/finalPluginsVersions.properties&quot;
 *    outputFile=&quot;${topBuildDirectory}/${buildLabel}/orbitBundles.map&quot; /&gt;
 *    &lt;/target&gt;
 * &lt;/code&gt;
 * </pre>
 */
public class MapFileGenerator extends Task {
	private static final String EOL = System.getProperty("line.separator");
	// Longterm we'll want to use 'bundle', but due to
	// https://bugs.eclipse.org/bugs/show_bug.cgi?id=174331
	// we'll use 'plugin' for now.
	private static final String BUNDLE = "bundle";
	private static final String PLUGIN = "plugin";
	private String BUNDLE_LINE_PREFIX = BUNDLE;
	private String FEATURE_LINE_PREFIX = "feature";
	private boolean usePlugin = true;

	private DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG);

	private String inputFilePluginVersions;
	private String inputFileFeatureVersions;
	private String p2MapFile;
	private String p2Repository;
	private String buildlabel;
	private String pwd;
	private String username;
	private String addressPrefix;
	private String httpMapFile;

	static class BundleInfo implements Comparable {
		String id;
		String version;
		String fullVersion;

		public int compareTo(Object o) {
			BundleInfo other = (BundleInfo) o;
			return (id + ',' + version).compareTo(other.id + ',' + version);
		}
	}

	public void execute() throws BuildException {
		if (usePlugin) {
			BUNDLE_LINE_PREFIX = PLUGIN;
		}
		try {
			List listbundleinfo = getVersionDetails(inputFilePluginVersions);
			List listfeatureinfo = getVersionDetails(inputFileFeatureVersions);
			// only write out a p2 map file if the required attributes are
			// present
			if (p2MapFile != null && p2Repository != null)
				writeList(listfeatureinfo, listbundleinfo, p2MapFile);

			// just do bunles for tradtional http-get map file (features don't
			// make sense)
			if (httpMapFile != null) {
				writeList(listbundleinfo, httpMapFile);
			}

		}
		catch (IOException e) {
			new BuildException(e);
		}
	}

	// plugin@my.bundle.id,1.0.0=p2IU,id=my.bundle.id,version=1.0.0.v20081201,repository=http:/example.com/repo
	private String generateP2Fetch(String prefix, BundleInfo info) {
		return prefix + '@' + info.id + ',' + info.version + "=p2IU,id=" + info.id + ",version=" + info.fullVersion + ",repository=" + p2Repository + EOL;
	}

	private List getVersionDetails(String inputFile) throws FileNotFoundException, IOException {
		List bundleDetailsList = new ArrayList();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(inputFile));
			// This expression matches an entry in a
			// finalPluginsVersions.properties file and split it into 3
			// segments (id, version, full version)
			Pattern replacementExpression = Pattern.compile("^([a-zA-Z0-9\\-._]*)_([0-9]*\\.[0-9]*\\.[0-9]*)?=(.*)$");
			Matcher m = null;
			String current = null;
			while ((current = reader.readLine()) != null) {
				m = replacementExpression.matcher(current);
				if (!m.matches())
					continue;
				// the properties file contains entries with and without a
				// version and
				// we want to skip the non-version ones
				if (m.group(2) == null)
					continue;

				BundleInfo info = new BundleInfo();
				info.id = m.group(1);
				info.version = m.group(2);
				info.fullVersion = m.group(3);

				bundleDetailsList.add(info);
			}
		}
		finally {
			if (reader != null)
				reader.close();
		}
		return bundleDetailsList;
	}

	private void writeList(List featureDetailsList, List bundleDetailsList, String output) throws IOException {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(output));
			writer.write("!*** This file was created on " + dateFormat.format(new Date()) + EOL);
			writer.write("!*** Build Label: " + buildlabel + EOL);

			Collections.sort(featureDetailsList);
			Iterator iterator = featureDetailsList.iterator();
			while (iterator.hasNext()) {
				BundleInfo info = (BundleInfo) iterator.next();
				// double space for readability
				// writer.write(EOL);
				writer.write(generateP2Fetch(FEATURE_LINE_PREFIX, info));
			}

			Collections.sort(bundleDetailsList);
			Iterator iterator2 = bundleDetailsList.iterator();
			while (iterator2.hasNext()) {
				BundleInfo info = (BundleInfo) iterator2.next();
				// double space for readability
				// writer.write(EOL);
				writer.write(generateP2Fetch(BUNDLE_LINE_PREFIX, info));
			}
		}
		finally {
			if (writer != null)
				writer.close();
		}
	}

	public void setInputFilePluginVersions(String inputFile) {
		this.inputFilePluginVersions = inputFile;
	}

	public void setP2MapFile(String value) {
		this.p2MapFile = value;
	}

	public void setP2Repository(String value) {
		this.p2Repository = value;
	}

	public void setInputFileFeatureVersions(String inputFileFeatureVersions) {
		this.inputFileFeatureVersions = inputFileFeatureVersions;
	}

	public void setBuildlabel(String buildlabel) {
		this.buildlabel = buildlabel;
	}

	private void writeList(List bundleDetailsList, String output) throws IOException {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(output));
			writer.write("!*** This file was created on " + dateFormat.format(new Date()));
			writer.write("!*** Build Label: " + buildlabel + EOL);
			Collections.sort(bundleDetailsList);
			Iterator iterator = bundleDetailsList.iterator();
			while (iterator.hasNext()) {
				BundleInfo info = (BundleInfo) iterator.next();
				// double space for readability
				writer.write(EOL);
				writer.write(generateCVSFetch(info));
			}
		}
		finally {
			if (writer != null)
				writer.close();
		}
	}

	// plugin@my.bundle.id,1.0.0=GET,http://eclipse.org/downloads/orbit/my.bundle.id_1.0.0.v20081201.zip,unpack=true|false
	private String generateCVSFetch(BundleInfo info) {
		String fileName = info.id + "_" + info.fullVersion;
		boolean shouldUnpack = shouldUnpack(fileName);
		return new String(BUNDLE_LINE_PREFIX + "@" + info.id + ',' + info.version + "=GET," + addressPrefix + fileName + (shouldUnpack ? ".jar,unpack=true" : ".jar") + (username == null ? "" : ',' + username) + (pwd == null ? "" : ',' + pwd) + EOL);
	}

	private boolean shouldUnpack(String path) {
		// TODO: unlike the orbit bundle system, we should look at feature
		// attribute (in feature, or p2 metadata)
		// to decide unpack attribute
// File checked = new File(root, path + ".jar");
// if (checked.exists())
// return true;
		return false;
	}

	public String getAddressPrefix() {
		return addressPrefix;
	}

	public void setAddressPrefix(String addressPrefix) {
		this.addressPrefix = addressPrefix;
	}

	public String getHttpMapFile() {
		return httpMapFile;
	}

	public void setHttpMapFile(String httpMapFile) {
		this.httpMapFile = httpMapFile;
	}
}
