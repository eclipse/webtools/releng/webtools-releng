/*******************************************************************************
 * Copyright (c) 2010, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools;

import java.net.URISyntaxException;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.equinox.p2.core.ProvisionException;

public class AddRepoPropertiesApplication implements IApplication {

    public Object start(IApplicationContext context) throws Exception {
        Object result = null;
        try {
            new AddRepoProperties().addRepoProperties();
            result = IApplication.EXIT_OK;
        }
        catch (ProvisionException e) {
            result = e.getMessage();
        }
        catch (URISyntaxException e) {
            result = e.getMessage();
        }
        return result;
    }

    public void stop() {

    }

}
