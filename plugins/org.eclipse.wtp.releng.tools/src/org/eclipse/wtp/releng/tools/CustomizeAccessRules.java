/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class CustomizeAccessRules extends Task {

	private static final String LINE_SEPARATOR_PROPERTY_NAME = "line.separator";
	private static final String PATH_SEPARATOR_PROPERTY_NAME = "path.separator";

	static class JavaCompilerFilter implements FilenameFilter {

		private static final String JAVA_COMPILER_FILENAME_EXTENSION = "args";
		private static final String JAVA_COMPILER_FILENAME_PREFIX = "javaCompiler";

		public boolean accept(File dir, String name) {
			if (name.startsWith(JAVA_COMPILER_FILENAME_PREFIX) && name.endsWith(JAVA_COMPILER_FILENAME_EXTENSION)) {
				return true;
			}
			return false;
		}

	}

	private String bundleDirectory;
	private String defaultRules;


	private static final String FORBID_CHARACTER = "-";
	private static final String DISCOURAGED_CHARACTER = "~";
	private static final String ACCESSIBLE_CHARACTER = "+";
	private static final String NONACCESSIBLE_RULE_VALUE = "nonaccessible";
	private static final String DISCOURAGED_RULE_VALUE = "discouraged";
	private static final String ACCESSIBLE_RULE_VALUE = "accessible";
	private static final String PATTERN_ATTRIBUTE_NAME = "pattern";
	private static final String KIND_ATTRIBUTE_NAME = "kind";
	private static final String ACCESSRULE_ELEMENT_NAME = "accessrule";
	private static final String ORG_ECLIPSE_PDE_CORE_REQUIRED_PLUGINS = "org.eclipse.pde.core.requiredPlugins";
	private static final String PATH_ATTRIBUTE_NAME = "path";
	private static final String CLASSPATHENTRY_ELEMENT_NAME = "classpathentry";
	private static final String RBRACKET = "]";
	private static final String LBRACKET = "[";
	private static final String BACKUP_FILE_EXTENSION = ".bak";
	private static final String CLASSPATH_FILENAME = ".classpath";
	private static final String COMMA = ",";
	private static final String ADAPTER_ACCESS = "#ADAPTER#ACCESS#";


	private static String EOL = System.getProperty(LINE_SEPARATOR_PROPERTY_NAME);
	private static String PATH_SEPERATOR = System.getProperty(PATH_SEPARATOR_PROPERTY_NAME);
	private static FilenameFilter javaCompilerFilter = new JavaCompilerFilter();

	private static Pattern adapterAccessLinePattern = Pattern.compile(ADAPTER_ACCESS + "(.*)\\[(.*)\\]");


	private String computeCustomizedRules(File classpathFile) {
		// first priority is to use any from classpath file.
		String results = extractClassPathRules(classpathFile);
		// if none, see if default rules have been specified in task element
		if ((results == null) || (results.length() == 0)) {
			if ((getDefaultRules() != null) && (getDefaultRules().length() > 0)) {
				results = convertForm(getDefaultRules());
			}
		}
		return results;
	}

	private boolean contains(String mainString, String toBeFound) {
		return (-1 < mainString.indexOf(toBeFound));
	}

	private String convertForm(String commaSeperatedList) {
		String result = commaSeperatedList;
		result = result.replaceAll(COMMA, PATH_SEPERATOR);
		result = removeSpace(result);
		return result + PATH_SEPERATOR;
	}

	private void customizeAccess(File javaCompilerFile, File classpathFile) {
		try {

			String name = javaCompilerFile.getName();
			String bakName = name + BACKUP_FILE_EXTENSION;
			File bakFile = new File(javaCompilerFile.getParentFile(), bakName);

			// if backup file already exists, take that as a sign we do not
			// need to process again ... we already have.
			if (!bakFile.exists()) {

				// if backup already exists, just keep adding '.bak' until
				// doesn't
// while (bakFile.exists()) {
// bakName = bakName + BACKUP_FILE_EXTENSION;
// bakFile = new File(javaCompilerFile.getParentFile(), bakName);
// }

				/*
				 * FYI. Seems like the javaCompiler file is recreated, from
				 * one compile/generate step to another, so we need to
				 * re-process (that is, the existence of the .bak file doesn't
				 * mean we are done).
				 */

				FileReader fileReader = new FileReader(javaCompilerFile);
				BufferedReader bufferedReader = new BufferedReader(fileReader);

				File newFile = new File(javaCompilerFile.getParentFile(), "tempnew" + javaCompilerFile.getName());
				FileWriter newFileWriter = new FileWriter(newFile);

				while (bufferedReader.ready()) {
					String line = bufferedReader.readLine();
					Matcher matcher = adapterAccessLinePattern.matcher(line);
					if (matcher.matches()) {
						String cp = matcher.group(1);
						String ar = matcher.group(2);

						String customizedRules = computeCustomizedRules(classpathFile);

						if (contains(ar, customizedRules)) {
							// simply re-write what we already have
							newFileWriter.write(ADAPTER_ACCESS + cp + LBRACKET + ar + RBRACKET + EOL);
						}
						else {
							// or, add if not already there
							// System.out.println(" Info: customized access
							// rules with pattern: " + customizedRules);
							newFileWriter.write(ADAPTER_ACCESS + cp + LBRACKET + customizedRules + ar + RBRACKET + EOL);
						}
					}
					else {
						System.out.println("Debug: Line did not match grammar syntax expectations: " + line);
						newFileWriter.write(line + EOL);
					}

				}

				newFileWriter.close();
				fileReader.close();

				File holdFile = new File(javaCompilerFile.getParentFile(), javaCompilerFile.getName());
				javaCompilerFile.renameTo(bakFile);
				newFile.renameTo(holdFile);
			}

		}
		catch (FileNotFoundException e) {
			throw new BuildException(e);
		}
		catch (IOException e) {
			System.out.println("Could not read/write javaCompilerFile");
			e.printStackTrace();
		}

	}

	public void execute() throws BuildException {

		try {
			System.out.println("bundleDirectory: " + getBundleDirectory());

			if ((getBundleDirectory() != null) && (getBundleDirectory().length() > 0)) {
				File directory = new File(getBundleDirectory());
				if (directory.exists() && directory.isDirectory()) {
					processBundlesDirectory(directory);
				}
				else {
					String msg = "the directory does not exist";
					System.out.println(msg);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new BuildException(e);
		}

	}

	private String extractClassPathRules(File classpathFile) {
		StringBuffer patterns = new StringBuffer();
		Document aDocument = getDOM(classpathFile);
		Element element = getElementWithAttribute(aDocument, CLASSPATHENTRY_ELEMENT_NAME, PATH_ATTRIBUTE_NAME, ORG_ECLIPSE_PDE_CORE_REQUIRED_PLUGINS);
		NodeList nodeList = element.getElementsByTagName(ACCESSRULE_ELEMENT_NAME);

		int length = nodeList.getLength();
		if (length > 0) {
			ArrayList accessible = new ArrayList();
			ArrayList discouraged = new ArrayList();
			ArrayList forbidden = new ArrayList();
			for (int i = 0; i < length; i++) {
				Node node = nodeList.item(i);
				NamedNodeMap aNamedNodeMap = node.getAttributes();
				Node kindAttribute = aNamedNodeMap.getNamedItem(KIND_ATTRIBUTE_NAME);
				String kindValue = kindAttribute.getNodeValue();
				Node patternAttribute = aNamedNodeMap.getNamedItem(PATTERN_ATTRIBUTE_NAME);
				String patternValue = patternAttribute.getNodeValue();

				if (ACCESSIBLE_RULE_VALUE.equals(kindValue)) {
					accessible.add(patternValue);
				}
				else if (DISCOURAGED_RULE_VALUE.equals(kindValue)) {
					discouraged.add(patternValue);
				}
				else if (NONACCESSIBLE_RULE_VALUE.equals(kindValue)) {
					forbidden.add(patternValue);
				}
			}

			/*
			 * we store all the node/attribute values in arrayLists first,
			 * just to be positive we add them in order or acceessible,
			 * discouraged, and forbidden. (I'm not positive the
			 * getElementsByTagName gaurentees the order we want).
			 */
			for (int j = 0; j < accessible.size(); j++) {
				patterns.append(ACCESSIBLE_CHARACTER + (String) accessible.get(j) + PATH_SEPERATOR);
			}
			for (int j = 0; j < discouraged.size(); j++) {
				patterns.append(DISCOURAGED_CHARACTER + (String) discouraged.get(j) + PATH_SEPERATOR);
			}
			for (int j = 0; j < forbidden.size(); j++) {
				patterns.append(FORBID_CHARACTER + (String) forbidden.get(j) + PATH_SEPERATOR);
			}
		}
		String result = patterns.toString();
		return result;
	}

	public String getBundleDirectory() {
		return bundleDirectory;
	}

	public String getDefaultRules() {
		return defaultRules;
	}

	private Document getDOM(File file) {

		Document aDocument = null;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
			InputSource inputSource = new InputSource(reader);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			aDocument = builder.parse(inputSource);
		}
		catch (SAXException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		finally {
			if (reader != null) {
				try {
					reader.close();
				}
				catch (IOException e) {
					// ignore this one
				}
			}
		}

		if (aDocument == null) {
			String msg = "Error: could not parse xml in classpath file: " + file.getAbsolutePath();
			throw new BuildException(msg);
		}
		return aDocument;

	}

	private Element getElementWithAttribute(Document aDocument, String elementName, String attributeName, String attributeValue) {
		Element element = null;
		NodeList nodeList = aDocument.getElementsByTagName(elementName);

		int length = nodeList.getLength();
		for (int i = 0; i < length; i++) {
			Node node = nodeList.item(i);
			NamedNodeMap aNamedNodeMap = node.getAttributes();
			Node attribute = aNamedNodeMap.getNamedItem(attributeName);
			if (attribute.getNodeValue().equals(attributeValue)) {
				element = (Element) node;
				break;
			}
		}
		return element;
	}

	private boolean isSuitable(File file) {
		return (file != null) && file.exists() && file.canRead() && file.canWrite();
	}

	private void processBundlesDirectory(File bundlesDirectory) {

		if (bundlesDirectory == null) {
			throw new BuildException("Error: bundlesDirectory can not be null");
		}

		String[] files = bundlesDirectory.list();
		if ((files == null) || (files.length == 0)) {
			throw new BuildException("Error: bundlesDirectory was empty");
		}

		for (int i = 0; i < files.length; i++) {
			File file = new File(bundlesDirectory, files[i]);
			if (file.isFile()) {
				System.out.println("debug info: top level file ignored: " + file.getName());
			}
			else {
				processDirectory(file);
			}
		}

	}

	private void processDirectory(File directory) {

		String[] allFiles = directory.list();
		if (allFiles == null) {
			throw new BuildException("Error: bundlesDirectory was empty");
		}

		File classpathFile = new File(directory, CLASSPATH_FILENAME);
		File[] javaCompilerFiles = directory.listFiles(javaCompilerFilter);

		File javaCompilerFile = null;
		if (javaCompilerFiles != null) {
			for (int j = 0; j < javaCompilerFiles.length; j++) {
				javaCompilerFile = javaCompilerFiles[j];
				if (isSuitable(javaCompilerFile) && isSuitable(classpathFile)) {
					System.out.println("   Info: customizing access rules in " + directory.getName());
					customizeAccess(javaCompilerFile, classpathFile);
				}
			}
		}
	}

	/**
	 * Simply space remover (not for natural language)
	 * 
	 * @param s
	 * @return String
	 */
	private String removeSpace(String s) {
		String results = null;
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (Character.isWhitespace(c)) {
				// do not copy to buffer
			}
			else {
				// add to buffer
				sb.append(c);
			}
		}
		if (sb.length() > 0) {
			results = sb.toString();
		}
		return results;

	}

	public void setBundleDirectory(String bundleDirectory) {
		if (bundleDirectory == null) {
			this.bundleDirectory = "";
		}
		else {
			this.bundleDirectory = bundleDirectory;
		}
	}

	public void setDefaultRules(String defaultRules) {
		if (defaultRules == null) {
			this.defaultRules = "";
		}
		else {
			this.defaultRules = defaultRules;
		}
	}
}
