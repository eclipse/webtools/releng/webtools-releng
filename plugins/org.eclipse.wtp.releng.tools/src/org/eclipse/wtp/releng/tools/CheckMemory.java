/*******************************************************************************
 * Copyright (c) 2010, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools;


public class CheckMemory {

    static final float onemeg = 1024 * 1024;

    public static void main(String[] args) {

        new CheckMemory().displayMemory();
    }

    public void displayMemory() {

        long maxMem = Runtime.getRuntime().maxMemory();
        long totalMem = Runtime.getRuntime().totalMemory();
        long freeMem = Runtime.getRuntime().freeMemory();
        long inUse = totalMem - freeMem;
        System.out.printf("%n");
        System.out.printf("%1$20s \t%2$7.2f MB%n", "Max Memory: ", (maxMem / onemeg));
        System.out.printf("%1$20s \t%2$7.2f MB%n", "Total Memory: ", (totalMem / onemeg));
        System.out.printf("%1$20s \t%2$7.2f MB%n", "Free Memory: ", (freeMem / onemeg));
        System.out.printf("%1$20s \t%2$7.2f MB%n", "In Use: ", (inUse / onemeg));
        System.out.printf("%n");
    }
}
