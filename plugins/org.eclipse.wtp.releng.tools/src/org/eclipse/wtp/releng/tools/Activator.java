/*******************************************************************************
 * Copyright (c) 2010, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools;

import org.eclipse.equinox.p2.core.IProvisioningAgent;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class Activator implements BundleActivator {

    private static BundleContext bundleContext = null;

    public void start(BundleContext context) throws Exception {
        Activator.bundleContext = context;
    }

    public void stop(BundleContext context) throws Exception {
        Activator.bundleContext = null;
    }

    public static IProvisioningAgent getProvisioningAgent() {
        IProvisioningAgent result = null;
        if (bundleContext == null) {
            throw new IllegalStateException("bundleContext was null. can not get provisioning agenet. Perhaps bundle was not started?");
        }
        ServiceReference reference = bundleContext.getServiceReference(IProvisioningAgent.SERVICE_NAME);
        if (reference == null) {
            result = null;
            System.out.println("Could not get IProvisioningAgent service?");
        }
        result = (IProvisioningAgent) bundleContext.getService(reference);

        return result;
    }
}
