/*******************************************************************************
 * Copyright (c) 2010, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Hashtable;
import java.util.Set;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.equinox.internal.p2.artifact.repository.simple.SimpleArtifactRepositoryFactory;
import org.eclipse.equinox.internal.p2.metadata.repository.SimpleMetadataRepositoryFactory;
import org.eclipse.equinox.p2.core.IProvisioningAgent;
import org.eclipse.equinox.p2.core.ProvisionException;
import org.eclipse.equinox.p2.publisher.IPublisherAction;
import org.eclipse.equinox.p2.publisher.IPublisherInfo;
import org.eclipse.equinox.p2.publisher.Publisher;
import org.eclipse.equinox.p2.publisher.PublisherInfo;
import org.eclipse.equinox.p2.query.IQueryResult;
import org.eclipse.equinox.p2.query.IQueryable;
import org.eclipse.equinox.p2.repository.IRepository;
import org.eclipse.equinox.p2.repository.IRepositoryManager;
import org.eclipse.equinox.p2.repository.artifact.ArtifactDescriptorQuery;
import org.eclipse.equinox.p2.repository.artifact.IArtifactDescriptor;
import org.eclipse.equinox.p2.repository.artifact.IArtifactRepository;
import org.eclipse.equinox.p2.repository.artifact.spi.ArtifactDescriptor;
import org.eclipse.equinox.p2.repository.metadata.IMetadataRepository;

public class AddRepoProperties extends Task {

    private static final String       DOWNLOAD_STATS                   = "download.stats";
    private static final String       IREPOSITORY_P2_STATS_URI         = "p2.statsURI";
    private static final String       ARTIFACT_REPO_DIRECTORY          = "artifactRepoDirectory";
    private static final String       METADATA_REPO_DIRECTORY          = "metadataRepoDirectory";
    private static final String       P2_MIRRORS_URL                   = "p2MirrorsURL";
    private static final String       P2_STATS_URI                     = "p2StatsURI";
    private static final String       P2_ARTIFACT_REPOSITORY_NAME      = "p2ArtifactRepositoryName";
    private static final String       P2_METADATA_REPOSITORY_NAME      = "p2MetadataRepositoryName";
    private static final String       STATS_TRACKED_ARTIFACTS_PROPERTY = "statsTrackedArtifacts";
    private static final String       STATS_ARTIFACTS_SUFFIX           = "statsArtifactsSuffix";

    private String                    statsTrackedArtifacts;
    private String                    statsArtifactsSuffix;
    private String                    artifactRepoDirectory;
    private String                    metadataRepoDirectory;
    private String                    p2MirrorsURL;
    private String                    p2StatsURI;
    private String                    p2ArtifactsRepositoryName;
    private String                    p2MetadataRepositoryName;
    private Hashtable<String, String> bundleTable;
    private static final long         SERVICE_TIMEOUT                  = 5000;

    /**
     * Example arguments:
     * 
     * <pre>
     * -DartifactRepoDirectory=D:/buildzips/RC3/
     * -Dp2MirrorsURL="http://www.eclipse.org/downloads/download.php?format=xml&file=/webtools/downloads/drops/R3.2.0/S-3.2.0RC3-20100527211052/repository/&protocol=http"
     * -Dp2StatsURI=http://download.eclipse.org/stats/webtools/repository/helios 
     * -DstatsArtifactsSuffix=_helios_sr0
     * -DstatsTrackedArtifacts=org.eclipse.wst.jsdt.feature,org.eclipse.wst.xml_ui.feature,org.eclipse.wst.web_ui.feature,org.eclipse.jst.enterprise_ui.feature
     * -Dp2ArtifactsRepositoryName="Web Tools Platform Repository Helios SR0"
     * </pre>
     */

    public void addRepoProperties() throws ProvisionException, URISyntaxException {
        try {
            IPublisherInfo info = createPublisherInfo();
            if (info != null) {
                IPublisherAction[] actions = createActions();
                Publisher publisher = new Publisher(info);
                publisher.publish(actions, new NullProgressMonitor());
                infoOutput("INFO: repository rewritten");
            }
            else {
                infoOutput("WARNING: repository was not rewritten");
            }
        }
        catch (ProvisionException e) {
            infoOutput(e.getMessage());
            throw e;
        }
        catch (URISyntaxException e) {
            infoOutput(e.getMessage());
            throw e;
        }
    }

    private IPublisherInfo createPublisherInfo() throws ProvisionException, URISyntaxException {
        PublisherInfo result = null;

        String pathPartArtifacts = getArtifactRepoDirectory();
        if ((pathPartArtifacts == null) || (pathPartArtifacts.length() == 0)) {
            infoOutput("The artifact repository directory needs to be specified for this task. Try setting system property '" + ARTIFACT_REPO_DIRECTORY + "'?");
        }
        else {

            String pathPartMetadata = getMetadataRepoDirectory();
            if ((pathPartMetadata == null) || (pathPartMetadata.length() == 0)) {
                infoOutput("INFO: No metadata repository was specified. If desired, set the system property '" + METADATA_REPO_DIRECTORY + "'.");
            }

            String repoURIArtifacts = "file:/" + pathPartArtifacts;
            String repoURIMetadata = null;
            if (pathPartMetadata != null) {
                repoURIMetadata = "file:/" + pathPartMetadata;
            }


            SimpleArtifactRepositoryFactory simpleArtifactRepositoryFactory = new SimpleArtifactRepositoryFactory();

            SimpleMetadataRepositoryFactory simpleMetadataRepositoryFactory = null;
            if (repoURIMetadata != null) {
                simpleMetadataRepositoryFactory = new SimpleMetadataRepositoryFactory();
            }


            // NPE is thrown during "reload" if repository is written, and
            // agent is not set
            simpleArtifactRepositoryFactory.setAgent(Activator.getProvisioningAgent());
            if (simpleMetadataRepositoryFactory != null) {
                simpleMetadataRepositoryFactory.setAgent(Activator.getProvisioningAgent());
            }
            IArtifactRepository artifactRepository = simpleArtifactRepositoryFactory.load(new URI(repoURIArtifacts), IRepositoryManager.REPOSITORY_HINT_MODIFIABLE, new NullProgressMonitor());
            if (artifactRepository != null) {

                result = processP2ArtifactRepositoryName(result, artifactRepository);
                result = processMirrorsURL(result, artifactRepository);
                result = processStatsURI(result, artifactRepository);
                result = processArtifactProperties(result, artifactRepository);
                if (simpleMetadataRepositoryFactory != null) {
                    IMetadataRepository metadataRepository = simpleMetadataRepositoryFactory.load(new URI(repoURIMetadata), IRepositoryManager.REPOSITORY_HINT_MODIFIABLE, new NullProgressMonitor());
                    if (metadataRepository != null) {
                        result = processP2MetadataRepositoryName(result, metadataRepository);
                    }
                    else {
                        infoOutput("metadata repository was null, not local, or otherwise not modifiable");
                    }
                }
            }
            else {
                infoOutput("artifact repository was null, not local, or otherwise not modifiable");
            }
        }
        return result;
    }

    private PublisherInfo processP2MetadataRepositoryName(PublisherInfo result, IMetadataRepository metadataRepository) {
        //String existingP2MetadataRepositoryName = metadataRepository.getProperty(IRepository.PROP_NAME);
        String existingP2MetadataRepositoryName = metadataRepository.getName();
        if (existingP2MetadataRepositoryName != null) {
            infoOutput("INFO: the metadata repository had existing name: " + existingP2MetadataRepositoryName);
        }
        else {
            infoOutput("INFO: the metadata repository had no existing name");
        }

        String newName = getP2MetadataRepositoryName();
        // if the property given to us is null (i.e. no specified, then do
        // nothing to touch any existing values.
        // if property given to us is empty string, assume that it means
        // to remove any existing properties.
        if (newName == null) {
            infoOutput("INFO: " + P2_METADATA_REPOSITORY_NAME + " was not specified. Any existing values not changed.");
        }
        else {

            if (newName.length() == 0) {
                newName = null;
            }
            boolean needrewrite = false;
            String reasonForNoWrite = null;
            if (newName != null) {
                // if they are the same, don't bother
                if (!newName.equals(existingP2MetadataRepositoryName)) {
                    needrewrite = true;
                }
                else {
                    reasonForNoWrite = "INFO: new value and existing value of metadata repository name are the same";
                }
            }
            else {
                // value of null, means removal is desired
                if (existingP2MetadataRepositoryName != null) {
                    needrewrite = true;
                }
                else {
                    reasonForNoWrite = "INFO: removal of metadata repository name was indicated, but repository has no existing value.";
                }
            }
            if (needrewrite) {
                if (result == null) {
                    result = initRepoOverwrite(metadataRepository);
                }
                metadataRepository.setProperty(IRepository.PROP_NAME, newName);
            }
            else {
                infoOutput(reasonForNoWrite);
            }
        }
        return result;
    }

    private PublisherInfo processP2ArtifactRepositoryName(PublisherInfo result, IArtifactRepository artifactRepository) {

//        IProvisioningAgent agent = Activator.getProvisioningAgent();
//        IArtifactRepositoryManager manager = getService(agent, IArtifactRepositoryManager.SERVICE_NAME);
//        String existingP2ArtifactRepositoryName  = manager.getRepositoryProperty(artifactRepository.getLocation(), IRepository.PROP_NAME);
//        String existingP2ArtifactRepositoryName = artifactRepository.getProperty(IRepository.PROP_NAME);
        String existingP2ArtifactRepositoryName = artifactRepository.getName();

        // String existingP2ArtifactRepositoryName = artifactRepository.getProperty(IRepository.PROP_NAME);
        if (existingP2ArtifactRepositoryName != null) {
            infoOutput("INFO: the artifact repository had existing name: " + existingP2ArtifactRepositoryName);
        }
        else {
            infoOutput("INFO: the artifact repository had no existing name");
        }

//        Map<String, String> repoInfo = artifactRepository.getProperties();
//        System.out.println(repoInfo);


        String newName = getP2ArtifactRepositoryName();
        // if the property given to us is null (i.e. no specified, then do
        // nothing to touch any existing values.
        // if property given to us is empty string, assume that it means
        // to remove any existing properties.
        if (newName == null) {
            infoOutput("INFO: " + P2_ARTIFACT_REPOSITORY_NAME + " was not specified. Any existing values not changed.");
        }
        else {

            if (newName.length() == 0) {
                newName = null;
            }
            boolean needrewrite = false;
            String reasonForNoWrite = null;
            if (newName != null) {
                // if they are the same, don't bother
                if (!newName.equals(existingP2ArtifactRepositoryName)) {
                    needrewrite = true;
                }
                else {
                    reasonForNoWrite = "INFO: new value and existing value of artifact repository name are the same";
                }
            }
            else {
                // newMirror is null, means removal is desired
                if (existingP2ArtifactRepositoryName != null) {
                    needrewrite = true;
                }
                else {
                    reasonForNoWrite = "INFO: removal of artifact repository name was indicated, but the repository has no existing value.";
                }
            }
            if (needrewrite) {
                if (result == null) {
                    result = initRepoOverwrite(artifactRepository);
                }
                artifactRepository.setProperty(IRepository.PROP_NAME, newName);
            }
            else {
                infoOutput(reasonForNoWrite);
            }
        }
        return result;
    }

    private PublisherInfo processMirrorsURL(PublisherInfo result, IArtifactRepository artifactRepository) {

        String existingMirrorURL = artifactRepository.getProperty(IRepository.PROP_MIRRORS_URL);
        if (existingMirrorURL != null) {
            infoOutput("INFO: the repository had existing mirrorURL: " + existingMirrorURL);
        }
        else {
            infoOutput("INFO: the repository had no existing mirrorURL");
        }

        String newMirrorURL = getP2MirrorsURL();
        // if the property given to us is null (i.e. no specified, then do
        // nothing to touch any existing values.
        // if property given to us is empty string, assume that it means
        // to remove any existing properties.
        if (newMirrorURL == null) {
            infoOutput("INFO: " + P2_MIRRORS_URL + " was not specified. Any existing values not changed.");
        }
        else {

            if (newMirrorURL.length() == 0) {
                newMirrorURL = null;
            }
            boolean needrewrite = false;
            String reasonForNoWrite = null;
            if (newMirrorURL != null) {
                // if they are the same, don't bother
                if (!newMirrorURL.equals(existingMirrorURL)) {
                    needrewrite = true;
                }
                else {
                    reasonForNoWrite = "INFO: new value and existing value of mirrorURL are the same";
                }
            }
            else {
                // newMirror is null, means removal is desired
                if (existingMirrorURL != null) {
                    needrewrite = true;
                }
                else {
                    reasonForNoWrite = "INFO: removal of mirrorURL was indicated, but repository has no existing value.";
                }
            }
            if (needrewrite) {
                if (result == null) {
                    result = initRepoOverwrite(artifactRepository);
                }
                artifactRepository.setProperty(IRepository.PROP_MIRRORS_URL, newMirrorURL);
            }
            else {
                infoOutput(reasonForNoWrite);
            }
        }
        return result;
    }

    private IPublisherAction[] createActions() {
        IPublisherAction[] result = new IPublisherAction[0];
        return result;
    }

    public String getArtifactRepoDirectory() {
        // if null, that is not explicitly 'set', then see if it was
        // passed in as a system property. TODO: Does this contradict
        // ant task rules?
        if (artifactRepoDirectory == null) {
            // for now, be explicit we are working with "artifact"
            // repositories ... but could also do metadata and/or accept other
            // input forms, such as p2.buildRepo.
            artifactRepoDirectory = System.getProperty(ARTIFACT_REPO_DIRECTORY);
            infoOutput("INFO: artifact repository directory set from '" + ARTIFACT_REPO_DIRECTORY + "': " + artifactRepoDirectory);
        }
        return artifactRepoDirectory;
    }

    public String getMetadataRepoDirectory() {
        // if null, that is not explicitly 'set', then see if it was
        // passed in as a system property. TODO: Does this contradict
        // ant task rules? (That is, system property is to take priority?)
        if (metadataRepoDirectory == null) {
            // for now, be explicit we are working with "artifact"
            // repositories ... but could also do metadata and/or accept other
            // input forms, such as p2.buildRepo.
            metadataRepoDirectory = System.getProperty(METADATA_REPO_DIRECTORY);
            infoOutput("INFO: metadata repository directory set from '" + METADATA_REPO_DIRECTORY + "': " + metadataRepoDirectory);
        }
        return metadataRepoDirectory;
    }

    private void infoOutput(String info) {
        System.out.println("\t" + info);
    }

    public void setArtifactRepoDirectory(String repositoryDirectory) {
        this.artifactRepoDirectory = repositoryDirectory;
    }

    public String getP2MirrorsURL() {
        if (p2MirrorsURL == null) {
            p2MirrorsURL = System.getProperty(P2_MIRRORS_URL);
            infoOutput("INFO: mirror URL value set from '" + P2_MIRRORS_URL + "': " + p2MirrorsURL);

        }
        return p2MirrorsURL;
    }

    public void setP2MirrorsURL(String mirrorURLString) {
        this.p2MirrorsURL = mirrorURLString;
    }

    public void setP2StatsURI(String statsURIString) {
        this.p2StatsURI = statsURIString;
    }

    @Override
    public void execute() throws BuildException {
        try {
            addRepoProperties();
        }
        catch (ProvisionException e) {
            // e.printStackTrace();
            throw new BuildException(e);
        }
        catch (URISyntaxException e) {

            e.printStackTrace();
            throw new BuildException(e);
        }
    }

    private PublisherInfo processStatsURI(PublisherInfo result, IArtifactRepository artifactRepository) {

        /*
         * <property name='p2.statsURI' value='http://your.stats.server/stats'/>
         */
        String existingStatsURI = artifactRepository.getProperty(IREPOSITORY_P2_STATS_URI);
        if (existingStatsURI != null) {
            infoOutput("INFO: the repository had existing statsURI: " + existingStatsURI);
        }
        else {
            infoOutput("INFO: the repository had no existing statsURI");
        }

        String newStatsURI = getP2StatsURI();
        // if the property is null, that is, not specified, do nothing.
        // if property given to us is empty string, assume that is
        // the same as "removal" which is done by passing in null.
        if (newStatsURI == null) {
            infoOutput("INFO: " + P2_STATS_URI + " was not specified. Any existing values not changed.");
        }
        else {
            if (newStatsURI.length() == 0) {
                newStatsURI = null;
            }
            boolean needrewrite = false;
            String reasonForNoWrite = null;
            if (newStatsURI != null) {
                // if they are the same, don't bother
                if (!newStatsURI.equals(existingStatsURI)) {
                    needrewrite = true;
                }
                else {
                    reasonForNoWrite = "INFO: new value and existing value of statsURI are the same";
                }
            }
            else {
                // newStatsURI is null, means removal is desired
                if (existingStatsURI != null) {
                    needrewrite = true;
                }
                else {
                    reasonForNoWrite = "INFO: removal of statusURI was indicated, but repository has no existing value.";
                }
            }
            if (needrewrite) {
                if (result == null) {
                    result = initRepoOverwrite(artifactRepository);
                }
                artifactRepository.setProperty(IREPOSITORY_P2_STATS_URI, newStatsURI);

            }
            else {
                infoOutput(reasonForNoWrite);
            }
        }
        return result;
    }

    public String getP2StatsURI() {
        if (p2StatsURI == null) {
            p2StatsURI = System.getProperty(P2_STATS_URI);
            infoOutput("INFO: stats URI value set from '" + P2_STATS_URI + "': " + p2StatsURI);

        }
        return p2StatsURI;
    }

    private PublisherInfo initRepoOverwrite(IArtifactRepository artifactRepository) {
        PublisherInfo result;
        result = new PublisherInfo();
        result.setArtifactRepository(artifactRepository);
        result.setArtifactOptions(IPublisherInfo.A_OVERWRITE);
        return result;
    }

    private PublisherInfo initRepoOverwrite(IMetadataRepository metadataRepository) {
        PublisherInfo result;
        result = new PublisherInfo();
        result.setMetadataRepository(metadataRepository);
        result.setArtifactOptions(IPublisherInfo.A_OVERWRITE);
        return result;
    }

    /*
     * This method was original based on code attached to bug 310132 https://bugs.eclipse.org/bugs/show_bug.cgi?id=310132
     */
    private PublisherInfo processArtifactProperties(PublisherInfo result, IArtifactRepository artifactRepository) {

        Set<String> statsTrackedBundles = getStatsTrackedBundles();
        if (statsTrackedBundles.size() > 0) {
            if (result == null) {
                result = initRepoOverwrite(artifactRepository);
            }
            for (String bundleId : statsTrackedBundles) {
                IQueryable<IArtifactDescriptor> queryable = result.getArtifactRepository().descriptorQueryable();
                IQueryResult<IArtifactDescriptor> artifacts = queryable.query(new ArtifactDescriptorQuery(bundleId, null, null), null);

                for (IArtifactDescriptor artifact : artifacts.toArray(IArtifactDescriptor.class)) {
                    // afaik, we always get ArtifactDesciptors, but
                    // 'setProperty' is not on IArtifactDescriptor?
                    if (artifact instanceof ArtifactDescriptor) {
                        String descriptiveValue = getStatsValue(bundleId);
                        ((ArtifactDescriptor) artifact).setProperty(DOWNLOAD_STATS, descriptiveValue);
                        infoOutput("INFO: set " + DOWNLOAD_STATS + " property for: " + artifact);
                        infoOutput("INFO:                              to:  " + descriptiveValue);
                    }
                }
            }
        }
        else {
            infoOutput("INFO: No artifacts requested to be tracked");
        }
        return result;
    }

    private Set<String> getStatsTrackedBundles() {
        if (bundleTable == null) {
            initBundleTable();
        }
        return bundleTable.keySet();
    }


    /*
     * Example arguments
     * -DstatsArtifactsSuffix=_helios_sr0;
     * -DstatsTrackedArtifacts=org.eclipse.wst.jsdt.feature,org.eclipse.wst.xml_ui.feature,org.eclipse.wst.web_ui.feature,org.eclipse.jst.enterprise_ui.feature
     */
    private void initBundleTable() {
        if (bundleTable == null) {
            bundleTable = new Hashtable<String, String>();

            String trackedArtifacts = getStatsTrackedArtifacts();
            String[] trackedStringsArray = null;
            if (trackedArtifacts != null) {
                trackedStringsArray = trackedArtifacts.split("[,;: ]");


                String suffix = getStatsArtifactsSuffix();

                // ISSUE: technically suffix can be empty (no version info)
                // What is a good default, if none provided?
                if ((suffix == null) || (suffix.length() == 0)) {
                    suffix = "";
                    infoOutput("WARNING: artifacts suffix was null or empty. No suffix will be used.");
                }

                for (int i = 0; i < trackedStringsArray.length; i++) {
                    String artifactitem = trackedStringsArray[i];
                    bundleTable.put(artifactitem, artifactitem + suffix);
                }

                int nArtifacts = bundleTable.size();
                infoOutput("INFO: Artifact stats processing: " + nArtifacts + " were requested for tagging.");
                for (String bundleId : bundleTable.keySet()) {
                    infoOutput("INFO: " + bundleId + "\t\t" + bundleTable.get(bundleId));
                }
            }
        }
        else {
            infoOutput("WARNING: bundleTable already exsisted when asked to initialize. Check program to see if there is a calling sequence error.");
        }
    }

    private String getStatsValue(String bundleId) {
        // technically, may be impossible, or an error, for bundleTable to be null here,
        // but just in case, we'll handle.
        if (bundleTable == null) {
            initBundleTable();
        }
        return bundleTable.get(bundleId);
    }

    public String getStatsTrackedArtifacts() {
        if (statsTrackedArtifacts == null) {
            statsTrackedArtifacts = System.getProperty(STATS_TRACKED_ARTIFACTS_PROPERTY);
        }
        return statsTrackedArtifacts;
    }

    public void setStatsTrackedArtifacts(String statsTrackedArtifacts) {
        this.statsTrackedArtifacts = statsTrackedArtifacts;
    }

    public String getStatsArtifactsSuffix() {
        if (statsArtifactsSuffix == null) {
            statsArtifactsSuffix = System.getProperty(STATS_ARTIFACTS_SUFFIX);
        }
        return statsArtifactsSuffix;
    }

    public void setStatsArtifactsSuffix(String statsArtifactsSuffix) {
        this.statsArtifactsSuffix = statsArtifactsSuffix;
    }

    public String getP2ArtifactRepositoryName() {
        if (p2ArtifactsRepositoryName == null) {
            p2ArtifactsRepositoryName = System.getProperty(P2_ARTIFACT_REPOSITORY_NAME);
            infoOutput("INFO: p2ArtifactRepositoryName value set from '" + P2_ARTIFACT_REPOSITORY_NAME + "': " + p2ArtifactsRepositoryName);
        }
        return p2ArtifactsRepositoryName;
    }

    public String getP2MetadataRepositoryName() {
        if (p2MetadataRepositoryName == null) {
            p2MetadataRepositoryName = System.getProperty(P2_METADATA_REPOSITORY_NAME);
            infoOutput("INFO: p2MetadataRepositoryName value set from '" + P2_METADATA_REPOSITORY_NAME + "': " + p2MetadataRepositoryName);
        }
        return p2MetadataRepositoryName;
    }

    public void setP2RepositoryName(String p2ArtifactsRepositoryName) {
        this.p2ArtifactsRepositoryName = p2ArtifactsRepositoryName;
    }

    @SuppressWarnings("unchecked")
    protected static <T> T getService(IProvisioningAgent agent, String serviceName) {
        T service = (T) agent.getService(serviceName);
        if (service != null)
            return service;
        long start = System.currentTimeMillis();
        do {
            try {
                Thread.sleep(100);
            }
            catch (InterruptedException e) {
                //ignore and keep waiting
            }
            service = (T) agent.getService(serviceName);
            if (service != null)
                return service;
        }
        while ((System.currentTimeMillis() - start) < SERVICE_TIMEOUT);
        //could not obtain the service
        throw new IllegalStateException("Unable to obtain required service: " + serviceName); //$NON-NLS-1$
    }
}
