/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools;

import java.io.File;


public class UpdateFeatureUpdateFile {

	private String featuresDirectoryDefaultLocation = "D:\\builds\\freshZips\\postM3\\eclipse\\features";
	final private static String FEATURES_PROPERTY = "featuresLocation";
	private FullJarNameParser fullJarNameParser = new FullJarNameParser();
	
	private String[] featuresOfInterest =  new String[]{"org.eclipse.wst.web_ui.feature", "org.eclipse.jst.enterprise_ui.feature", "org.eclipse.jpt.feature", "org.eclipse.jpt.eclipselink.feature", "org.eclipse.wst.xml_ui.feature", "org.eclipse.wst.jsdt.feature","org.eclipse.wst.common_ui.feature", "org.eclipse.jst.webpageeditor.feature", "org.eclipse.jst.jsf.apache.trinidad.tagsupport.feature", "org.eclipse.jst.ws.axis2tools.feature", "org.eclipse.wst.web_sdk.feature", "org.eclipse.jst.enterprise_sdk.feature", "org.eclipse.jpt_sdk.feature", "org.eclipse.jpt.eclipselink_sdk.feature", "org.eclipse.wst.xml_sdk.feature", "org.eclipse.wst.jsdt_sdk.feature", "org.eclipse.wst.common_sdk.feature", "org.eclipse.jst.webpageeditor_sdk.feature", "org.eclipse.jst.jsf.apache.trinidad.tagsupport_sdk.feature", "org.eclipse.jst.ws.axis2tools_sdk.feature",};

	private String[] shortListOfInterest = new String[]{"org.eclipse.wst.web_ui.feature", "org.eclipse.jst.enterprise_ui.feature", "org.eclipse.jpt.feature", "org.eclipse.jpt.eclipselink.feature", "org.eclipse.wst.xml_ui.feature", "org.eclipse.wst.jsdt.feature", "org.eclipse.wst.common_ui.feature", "org.eclipse.jst.webpageeditor.feature", "org.eclipse.jst.jsf.apache.trinidad.tagsupport.feature", "org.eclipse.jst.ws.axis2tools.feature", };

	public static void main(String[] args) {

		new UpdateFeatureUpdateFile().createFeatureFiles();
	}

	private void createFeatureFiles() {
		String featuresProperty = System.getProperty(FEATURES_PROPERTY, featuresDirectoryDefaultLocation);
		System.out.println("featuresDirectoryLocation: " + featuresProperty);
		File featuresDirectory = new File(featuresProperty);
		if (!featuresDirectory.exists()) {
			System.out.println("ERROR: no directory at location: " + featuresProperty);
		}
		else {
			String[] featuresDirectories = featuresDirectory.list();
			if (featuresDirectories == null || featuresDirectories.length <= 0) {
				System.out.println("ERROR: no featues found at location: " + featuresProperty);
			}
			else {
				System.out.println("Full List for main WTP site");
				System.out.println();
				doList(featuresDirectories, featuresOfInterest);
				System.out.println();
				System.out.println("Short List for Europa site");
				System.out.println();
				doList(featuresDirectories, shortListOfInterest);
				System.out.println("Short List for Ganymede site");
				System.out.println();
				doList2(featuresDirectories, shortListOfInterest);
			}
		}
	}

	private void doList(String[] featureDirectories, String[] featureOfFocus) {
		for (int i = 0; i < featureDirectories.length; i++) {
			String directoryName = featureDirectories[i];
			fullJarNameParser.parse(directoryName);
			String projectName = fullJarNameParser.getProjectString();
			String versionString = fullJarNameParser.getVersionString();
			if (contains(projectName, featureOfFocus)) {
				System.out.println("        <ant antfile=\"updateMirrorProject.xml\">\r\n" + "            <property\r\n" + "                name=\"featureId\"\r\n" + "                value=\"" + projectName + "\" />\r\n" + "            <property\r\n" + "                name=\"version\"\r\n" + "                value=\"" + versionString + "\" />\r\n" + "        </ant>");

			}


		}
	}

	private boolean contains(String needle, String[] haystack) {
		boolean result = false;

		for (int i = 0; i < haystack.length; i++) {
			if (needle.equals(haystack[i])) {
				result = true;
				break;
			}
		}
		return result;
	}

	private void doList2(String[] featureDirectories, String[] featureOfFocus) {
		for (int i = 0; i < featureDirectories.length; i++) {
			String directoryName = featureDirectories[i];
			fullJarNameParser.parse(directoryName);
			String projectName = fullJarNameParser.getProjectString();
			String versionString = fullJarNameParser.getVersionString();
			if (contains(projectName, featureOfFocus)) {

				System.out.println("\t\t\t" + "<dependency name=\"" + projectName + "\" versionDesignator=\"[" + versionString + "]\" />");
			}


		}
	}
}
