#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2010, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source webtools.apps.shsource


echo JAVA_EXEC_DIR: $JAVA_EXEC_DIR;
echo ECLIPSE_HOME_36: $ECLIPSE_HOME_36; 

APP_NAME=org.eclipse.equinox.p2.director
REPO_LOCATION="file:/${HOME}/download/webtools/releng/"
REPO_ARGS="-metadataRepository ${REPO_LOCATION} -artifactRepository ${REPO_LOCATION}"
IU_ARGS="-installIU org.eclipse.wt.releng.feature.feature.group"

echo APP_NAME: ${APP_NAME}
echo REPO_LOCATION: ${REPO_LOCATION}
echo REPO_ARGS: ${REPO_ARGS}
echo IU_ARGS: ${IU_ARGS}

VM_ARGS="-vmargs -Xmx512m -Declipse.p2.mirrors=false"
echo VMARGS: ${VM_ARGS}

"${ECLIPSE_HOME_36}"/eclipse -consolelog -data ./workspace-getapp -debug -nosplash -vm "${JAVA_EXEC_DIR}" -application ${APP_NAME} ${REPO_ARGS} ${IU_ARGS} ${VM_ARGS}


