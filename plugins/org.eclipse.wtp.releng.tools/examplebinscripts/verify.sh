#*******************************************************************************
# Copyright (c) 2009, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
# echo 
# echo $(basename $1
filename="${1}"
jarname=$(basename "${filename}")

export JAVA_HOME=/shared/common/ibm-java2-ppc-50
#JAVA_HOME=/shared/common/ibm-java-ppc-604
#JAVA_HOME=/shared/common/ibm-java-jdk-ppc-60

# if not provided by calling program, assume a simple place
if [[ -z $VERIFYOUTDIR ]] 
then
    VERIFYOUTDIR="${HOME}"/temp/
    mkdir -p $VERIFYOUTDIR
fi


PPAT_PACKGZ="(.*).pack.gz$"
if [[ "$jarname" =~  $PPAT_PACKGZ ]]
then 
    basejarname=${BASH_REMATCH[1]}
    #echo -e "\n basejarname: " $basejarname "\n"
    $JAVA_HOME/jre/bin/unpack200 $filename /tmp/$basejarname
    #unpack200 $filename /tmp/$basejarname
    vresult=`$JAVA_HOME/bin/jarsigner -verify /tmp/$basejarname`
    exitcode=$?
    rm /tmp/$basejarname
else
    #echo -e "\n filename: " $filename "\n"
    vresult=`$JAVA_HOME/bin/jarsigner -verify $filename`
    exitcode=$?
fi

PPAT_VERIFIED="^jar\ verified.*"
PPAT_UNSIGNED="^jar is unsigned.*"
PPAT_NOMANIFEST="^no manifest.*"
if [[ "${vresult}" =~ $PPAT_VERIFIED ]]
then
    printf '%-100s \t\t' "   ${jarname}: " >> "${VERIFYOUTDIR}"/verified.txt 
    printf '%s\n' " ${vresult} " >> "${VERIFYOUTDIR}"/verified.txt
elif [[ "${vresult}" =~ $PPAT_UNSIGNED ]]
then

# purposely no line delimiter, so output of jarsigner is on same line
    printf '%-100s \t\t' "   ${jarname}: "  >> "${VERIFYOUTDIR}"/unsigned.txt 
    printf '%s\n' " ${vresult} "  >> "${VERIFYOUTDIR}"/unsigned.txt 
elif [[ "${vresult}" =~ PPAT_NOMANIFEST ]]
then

# purposely no line delimiter, so output of jarsigner is on same line
    printf '%-100s \t\t' "   ${jarname}: "  >> "${VERIFYOUTDIR}"/nomanifest.txt 
    printf '%s\n' " ${vresult} "  >> "${VERIFYOUTDIR}"/nomanifest.txt 

else 
    printf '%-100s \t\t' "   ${jarname}: "  >> "${VERIFYOUTDIR}"/error.txt 
    printf '%s\n' " ${vresult} "  >> "${VERIFYOUTDIR}"/error.txt 
fi
 
if [[ $exitcode -gt 0 ]]
then

    echo -e "\n exitcode: " $exitcode: $(basename $filename)" \n"  >> "${VERIFYOUTDIR}"/errorexit.txt  
fi


