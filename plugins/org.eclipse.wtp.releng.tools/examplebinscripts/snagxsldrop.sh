#*******************************************************************************
# Copyright (c) 2009, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

xslSite=/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R0.5/R-0.5-20080820002020/updateSite
wtpSite=/opt/public/webtools/committers/wtp-R3.0-M/20080918035538/M-3.0.2-20080918035538

cp -r "${xslSite}" "${wtpSite}"


