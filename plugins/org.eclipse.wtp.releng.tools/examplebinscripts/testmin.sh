#!/bin/bash
#*******************************************************************************
# Copyright (c) 2009, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# Simple script to count number of mirrors available for a particular URL

# Copyright (c) 2009 IBM
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which is available at
# http://www.eclipse.org/legal/epl-2.0/
#
# Contributors:
#    David Williams - initial API and implementation


function usage() {
    printf "\n\tSimple script to count number of mirrors available for a particular URL" >&2
    printf "\n\tUsage: %s [-h] | [-v] [-f] [-p] [-t number] urls" $(basename $0) >&2
    printf "\n\t\t%s" "where h==help, v==verbose, f==only ftp mirrors, p==only http mirrors, t==test against 'number' " >&2
    printf "\n\t\t%s"    "and urls is space delimited list of URL parameters," >&2
    printf "\n\t\t%s\n"  "such as /ganymede/releases/site.xml" >&2
}

function checkMirrorsForURL() {

    if [ -z $1 ]
    then
      echo "Error: internal funtion requires mirror url parameter";
      exit 3;
    else
      mirrorURL=$1
    fi  
    if [ -z $2 ]
    then
      protocol=
      pword="http and ftp"  
    else
      protocolarg="&protocol=$2"
      pword="$2"
    fi
    
    nMirrors=$(wget -q -O - "http://www.eclipse.org/downloads/download.php?file=${mirrorURL}&format=xml${protocolarg}" | grep \<mirror\ | wc -l)
    echo "    number of" ${pword} "mirrors: " ${nMirrors} "  for" ${mirrorURL} 
    exit $nMirrors
}

function minvalue() {
ref=$1
comp=$2
result=
#echo "ref: $ref \r";
#echo "comp: " $comp "\r"
if [ -z $ref ]
then
    #echo "no ref\r"
    result=$comp
else
    if [ $ref -lt $comp ]
    then
        #echo "ref lt comp\r"
        result=$ref
    else
        #echo "comp <= ref\r"
        result=$comp
    fi
fi
echo $result
}


urls=
ftponly=0
httponly=0
protocol=
while getopts 'hvfpt:' OPTION
do
    case $OPTION in
        h)    usage
        exit 1
        ;;
        f)    ftponly=1
        ;;
        p)    httponly=1
        ;;
        v)    verbose=1
        ;;  
        t)    testNumber=$OPTARG
        ;;
        ?)    usage
        exit 2
        ;;
    esac
done

shift $(($OPTIND - 1))

urls="$@"

if [ $ftponly == 1 ]
then
 protocol="ftp"
fi 
if [ $httponly == 1 ]
then
 protocol="http"
fi 
if [ $ftponly == 1 -a $httponly == 1 ]
then
 protocol=
fi

if [ $verbose ]
then
 echo "ftponly: " $ftponly " httponly: " $httponly
 echo "protocol: " $protocol
 echo "urls: " $urls
fi 

minimumMirrors=
if [ "${urls}" ] 
then 
  echo
  for mirrorURL in ${urls}
  do
   nm=checkMirrorsForURL $mirrorURL $protocol
   minimumMirrors=min $minimumMirrors $nm
  done
  echo
else
  usage
fi

if [ -z $testNumber ]
then
 if [ $verbose ]
 then 
   echo "no test mode"
 fi
 exit 0
else
 result=$($testNumber - $minimumMirrors)
 if [ $result <= 0 ]
 then
   if [ $verbose ]
   then 
     echo "minimum mirrors was greater than or equal to criteria"
   fi
   exit 0
 else  
    if [ $verbose ]
     then 
       echo "minimum mirrors was not as large as criteria"
     fi
   exit result
 fi 
fi