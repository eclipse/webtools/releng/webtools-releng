#!/bin/bash
#*******************************************************************************
# Copyright (c) 2009, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# Simple script to count number of mirrors available for a particular URL

# Copyright (c) 2009 IBM
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which is available at
# http://www.eclipse.org/legal/epl-2.0/
#
# Contributors:
#    David Williams - initial API and implementation


function usage() {
    printf "\n\tSimple script to count number of mirrors available for a particular URL" >&2
    printf "\n\tUsage: %s [-h] | [-v] [-f] [-p] [-t number] urls" $(basename $0) >&2
    printf "\n\t\t%s" "where h==help, v==verbose, f==only ftp mirrors, p==only http mirrors, t==test against 'number' " >&2
    printf "\n\t\t%s"    "and urls is space delimited list of URL parameters," >&2
    printf "\n\t\t%s\n"  "such as /releases/galileo/" >&2
}

function checkMirrorsForURL() {

    if [ -z $1 ]
    then
      echo "Error: internal funtion requires mirror url parameter";
      exit 3;
    else
      mirrorURL=$1
    fi  
    if [ -z $2 ]
    then
      protocol=
      pword="http and ftp"  
    else
      protocolarg="&protocol=$2"
      pword="$2"
    fi
    
    nMirrors=$(wget -q -O - "http://www.eclipse.org/downloads/download.php?file=${mirrorURL}&format=xml${protocolarg}" | grep \<mirror\ | wc -l)
    echo "    number of" ${pword} "mirrors: " ${nMirrors} "  for" ${mirrorURL} >&2 
    echo $nMirrors
}

function minvalue() {
ref=$1
comp=$2
result=
if [ -z $comp ]
then
    result=$ref
else
    if [ $ref -lt $comp ]
    then
        result=$ref
    else
        result=$comp
    fi
fi
echo $result
}


urls=
ftponly=0
httponly=1
protocol=
while getopts 'hvfpt:' OPTION
do
    case $OPTION in
        h)    usage
        exit 1
        ;;
        f)    ftponly=1
        ;;
        p)    httponly=1
        ;;
        v)    verbose=1
        ;;  
        t)    testNumber=$OPTARG
        ;;
#        ?)    usage
#        exit 2
#        ;;
    esac
done

shift $(($OPTIND - 1))

urls="$@"

if [ $ftponly == 1 ]
then
 protocol="ftp"
fi 
if [ $httponly == 1 ]
then
 protocol="http"
fi 
if [ $ftponly == 1 -a $httponly == 1 ]
then
 protocol=
fi

if [ $verbose ]
then
 echo "ftponly: " $ftponly " httponly: " $httponly
 echo "protocol: " $protocol
 echo "urls: " $urls
fi 

if [ -z "${urls}" ] 
then

    urls="/releases/helios/201006110900/aggregate/ \
    /releases/helios/201006230900/aggregate/ \
    /releases/galileo/200909241140/aggregate/ \
    /releases/galileo/201002260900/aggregate/ \
    /eclipse/updates/3.6/ /webtools/updates/3.1/3.1.1/200909252200/ \
    /webtools/updates/3.1/3.1.2/201002260900/ \
    /webtools/repository/helios/  \
    /webtools/downloads/drops/R3.2.0/R-3.2.0-20100615235519/repository/   \
    /webtools/downloads/drops/R3.2.1/R-3.2.1-20100730021206/repository"

    
fi
    

minimumMirrors=
if [ "${urls}" ] 
then 
  echo
  for mirrorURL in ${urls}
  do
   nm=$(checkMirrorsForURL $mirrorURL $protocol)
   minimumMirrors=`minvalue $nm $minimumMirrors`
  done
  echo
else
  usage
fi

if [ -z $testNumber ]
then
 if [ $verbose ]
 then 
   echo "no test mode"
 fi
 exit 0
else
 fresult=$((testNumber - minimumMirrors))
 if [ $fresult -le 0 ]
 then
   if [ $verbose ]
   then 
     echo "minimum mirrors was greater than or equal to criteria"
   fi
   exit 0
 else  
    if [ $verbose ]
     then 
       echo "minimum mirrors was not as large as criteria"
     fi
   exit $fresult
 fi 
fi
