#!/bin/sh
#*******************************************************************************
# Copyright (c) 2010, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

echo;
PID=$1

if [ ! -z $PID ] ; then
    nthreads=`ps -mp $PID | wc -l`
    echo "   process PID: " $PID
    echo "   Number of threads in process: $nthreads"
else
    PID=`pgrep -f configure=/opt/users/hudsonbuild/`
    if [ ! -z $PID ] ; then
        nthreads=`ps -mp $PID | wc -l`

        echo "    hudson process PID: " $PID
        echo "    Number of threds n process: $nthreads"
    else
        echo "    PID $PID not found/defined (maybe pass in as first argument)?."
    fi
fi
echo

