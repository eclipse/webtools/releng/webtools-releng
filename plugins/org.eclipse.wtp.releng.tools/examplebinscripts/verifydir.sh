#*******************************************************************************
# Copyright (c) 2009, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

if [ -z $1 ]
then
    loc=./
else
    loc=${1}
fi
if [ -z $2 ]
then
    pat=".*\(\.jar$\|\.jar\.pack\.gz$\)"
else
    pat="${2}"
fi

# VERIFYOUTDIR is used in 'verify.sh' and must be clean before
# beginning
export VERIFYOUTDIR="${HOME}"/verifyoutdir

# make, in case doesn't exist
mkdir -p "${VERIFYOUTDIR}"

# even though we just set it above, include the if check 
# here to gaurd against future errors, since if 'verfiyoutdir' is 
# not defined, the remove if for the root directory! 
if [[ -n "${VERIFYOUTDIR}" ]]
then
       rm -fr "${VERIFYOUTDIR}"/*
fi

echo;
echo "verify directory: ${loc}";
echo "   for pattern:    ${pat}";
echo "   output files in " "${VERIFYOUTDIR}" 
echo;

find "${loc}" -regex "${pat}" -exec verify.sh '{}' \;

echo;
echo "verify directory: ${loc}";
echo "   for pattern:    ${pat}";
echo "   output files in " "${VERIFYOUTDIR}" 
echo;