#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2010, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source webtools.apps.shsource


echo JAVA_EXEC_DIR: $JAVA_EXEC_DIR;
echo ECLIPSE_HOME_36: $ECLIPSE_HOME_36; 

MIRRORS_URL_KEY_SEGMENT=releases/helios/201005070900/aggregate/

OTHER_ARGS="-Dartifact.repo.directory=${HOME}/downloads/releases/helios/201005070900/aggregate/   -Dp2.mirrorsURL=http://www.eclipse.org/downloads/download.php?format=xml&amp;file=${MIRRORS_URL_KEY_SEGMENT}&amp;protocol=http"

VM_ARGS="-vmargs -Declipse.p2.mirrors=false -Xmx256m"

APP_NAME=org.eclipse.wtp.releng.addmirrorsurl

echo "ECLIPSE_HOME_36: " ${ECLIPSE_HOME_36}
EQLAUNCHER=`find ${ECLIPSE_HOME_36}/plugins -name org.eclipse.equinox.launcher_*.jar`
echo "EQLAUNCHER: " $EQLAUNCHER

# -noexit

"${JAVA_CMD}" -jar ${EQLAUNCHER} -vm "${JAVA_CMD}" -console  -data ./workspace-runapp -debug -nosplash -application ${APP_NAME} ${OTHER_ARGS} ${VM_ARGS}



