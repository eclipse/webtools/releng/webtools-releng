

Each API package is required to have a package overview document describing
at a high level the problem solved by the API, how the API is meant to be used, 
and where applicable diagrams and images explaining the expected and correct 
usage. 

Like the design of the WTP website, the package overviews are stored as 
XML files within the root of the package that they describe. The file
name must always be "package.xml". The XSLT stylesheet that runs as part 
of the Javadoc process will transform this file into a "package.html" file,
which is expected by the javadoc tool. 

Developers have four tags to be aware of, in addition to the normal
tag redefinitions that occur for the website (e.g. some header tags 
are redefined as white text with light blue backgrounds that span 
the page, like the Eclipse style). These four tags are:

<p> tag: Each paragraph tag will be wrapped in a table that forces 
a fixed width for the text lines. Overviews that choose to add images 
can often stretch the text out into a width that is difficult or at a 
minimum inconvenient to read. Each <p> tag will be formatted consistently
across all overview documents. Please refrain from adding extra formatting
around paragraph tags.

<img src="" caption=""> tag: Each image will be displayed in a specialized table format, with
a caption. The <img> tag has an attribute "caption" that will be displayed
just under the image explaining in text what the picture intends to
convey graphically. For example:

<img src=".." caption="This sample text" />

__________
|         |
|  img    |
|_________|
|__text___|

*Also note that all images used in a package overview must be placed in the 
root of the project under a directory named "javadoc-images". Whenever an
image is linked to, the link must escape to the root package level
to reference the image, and include "overview" in the path. So if you want to 
link "model_overview.jpg" into package overview for "com.acme.model":

(1) Place "model_overview.jpg" in <project>\javadoc-images
(2) Add the following element to your document: 
	<img src="../../../overview/model_overview.jpg" caption="An overview of the Acme Model API" />  
	--> Note that the three "../" escapes is the same number of elements in the 
		package path: com/acme/model/. 
	--> "overview/" is required as all images will be pushed into the "overview" 
		directory in the final javadoc layout. 

		
<codesnippet caption=""> tag: The codesnippet is provided to allow quick addition of code 
snippet examples or usage patterns for API. The content of the <codesnippet />
tag should be the actual source code. The tag will use the text exactly as it 
appears in an HTML <pre> tag, and will wrap the snippet in a table for formatting.
The codesnippet tag allows for a caption just as the img tag does. 

<abstract> tag: The abstract tag should always occur first in your content, and should 
contain a brief synopsis of your package overview ("package overview overview"). The 
contents of this tag will be displayed in the full API overview page beside the linked
package name. The tag should contain no extra HTML formatting.