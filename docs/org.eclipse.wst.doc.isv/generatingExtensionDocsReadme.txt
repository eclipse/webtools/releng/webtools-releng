To generate extension documentation:

1. Add the PDE generation task for the plug-in to buildExtDocs.xml.

2. If the plug-in is in a new component add a generation task for the 
   component to buildExtDocs.xml and add the component to extDocListings.xml.