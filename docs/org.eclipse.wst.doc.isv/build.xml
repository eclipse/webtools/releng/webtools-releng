<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright (c) 2005, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->

<project
    name="org.eclipse.wst.doc.isv"
    default="build.jars"
    basedir=".">

    <target name="init">
        <property
            name="plugin"
            value="org.eclipse.wst.doc.isv" />
        <property
            name="version.suffix"
            value="1.0.201" />
        <property
            name="full.name"
            value="${plugin}_${version.suffix}" />
        <property
            name="temp.folder"
            value="${basedir}/temp.folder" />
        <property
            name="plugin.destination"
            value="${basedir}" />
        <property
            name="build.result.folder"
            value="${basedir}" />
    </target>

    <target
        name="build.update.jar"
        depends="init"
        description="Build the plug-in: org.eclipse.wst.doc.isv for an update site.">
        <delete dir="${temp.folder}" />
        <mkdir dir="${temp.folder}" />
        <antcall target="build.jars" />
        <antcall target="gather.bin.parts">
            <param
                name="destination.temp.folder"
                value="${temp.folder}/" />
        </antcall>
        <zip
            zipfile="${plugin.destination}/${full.name}.jar"
            basedir="${temp.folder}/${full.name}"
            filesonly="false" />
        <delete dir="${temp.folder}" />
    </target>

    <target
        name="build.jars"
        depends="init"
        description="Build all the jars for the plug-in: org.eclipse.wst.doc.isv.">
    </target>

    <target
        name="build.sources"
        depends="init">
    </target>

    <target
        name="gather.bin.parts"
        depends="init"
        if="destination.temp.folder">
        <ant
            antfile="javadoc.xml"
            dir="${basedir}" />
        <ant
            antfile="buildExtDocs.xml"
            dir="${basedir}" />
        <mkdir dir="${destination.temp.folder}/${full.name}" />
        <copy todir="${destination.temp.folder}/${full.name}">
            <fileset
                dir="${basedir}"
                includes="plugin.xml,plugin.properties,about.html,toc.xml,toc_WST.xml,topics_Reference.xml,META-INF/,html/,reference/,book.css,notices.html,topics_ExtPoint_Reference.xml,schema.css,javadoctoc.xml" />
        </copy>
        <eclipse.versionReplacer
            path="${destination.temp.folder}/${full.name}"
            version="${version.suffix}" />
    </target>

    <target
        name="build.zips"
        depends="init">
    </target>

    <target
        name="gather.sources"
        depends="init"
        if="destination.temp.folder">
    </target>

    <target
        name="gather.logs"
        depends="init"
        if="destination.temp.folder">
    </target>

    <target
        name="clean"
        depends="init"
        description="Clean the plug-in: org.eclipse.wst.doc.isv of all the zips, jars and logs created.">
        <delete file="${plugin.destination}/${full.name}.jar" />
        <delete file="${plugin.destination}/${full.name}.zip" />
        <delete dir="${temp.folder}" />
    </target>

    <target
        name="refresh"
        depends="init"
        if="eclipse.running"
        description="Refresh this folder.">
        <eclipse.refreshLocal
            resource="${plugin}"
            depth="infinite" />
    </target>

    <target
        name="zip.plugin"
        depends="init"
        description="Create a zip containing all the elements for the plug-in: org.eclipse.wst.doc.isv.">
        <delete dir="${temp.folder}" />
        <mkdir dir="${temp.folder}" />
        <antcall target="build.jars" />
        <antcall target="build.sources" />
        <antcall target="gather.bin.parts">
            <param
                name="destination.temp.folder"
                value="${temp.folder}/" />
        </antcall>
        <antcall target="gather.sources">
            <param
                name="destination.temp.folder"
                value="${temp.folder}/" />
        </antcall>
        <delete>
            <fileset
                dir="${temp.folder}"
                includes="**/*.bin.log" />
        </delete>
        <zip
            zipfile="${plugin.destination}/${full.name}.zip"
            basedir="${temp.folder}"
            filesonly="true" />
        <delete dir="${temp.folder}" />
    </target>

</project>
