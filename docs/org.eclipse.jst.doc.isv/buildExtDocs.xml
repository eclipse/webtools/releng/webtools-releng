<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright (c) 2005, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->

<project name="Build Extension Point Documentation" default="createExtDocs" basedir=".">

	<property file="buildExtDocs.properties"/>
	<property name="outputDir" value="${basedir}${file.separator}reference${file.separator}extension-points"/>
	<property name="entOutputDir" value="${outputDir}${file.separator}enterprise${file.separator}"/>
	<property name="serverOutputDir" value="${outputDir}${file.separator}server${file.separator}"/>
	<property name="webOutputDir" value="${outputDir}${file.separator}web${file.separator}"/>

	<target name="createExtDocs" depends="buildExtDocs, createDocListings"/>
	
	<target name="buildExtDocs" depends="buildEntExtDocs, buildServerExtDocs, buildWebExtDocs"/>
	
 	<target name="buildEntExtDocs">
 		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.j2ee/plugin.xml" destination="${entOutputDir}" />
 		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.j2ee.core/plugin.xml" destination="${entOutputDir}" />
 		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.j2ee.ejb.annotation.model/plugin.xml" destination="${entOutputDir}" />
 		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.j2ee.ejb.annotations.emitter/plugin.xml" destination="${entOutputDir}" />
 		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.j2ee.ejb.annotations.xdoclet/plugin.xml" destination="${entOutputDir}" />
 		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.j2ee.webservice/plugin.xml" destination="${entOutputDir}" />
 		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.j2ee.navigator.ui/plugin.xml" destination="${entOutputDir}" />
 		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.ws.consumption/plugin.xml" destination="${entOutputDir}" />
 		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.ws.consumption.ui/plugin.xml" destination="${entOutputDir}" />
 		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.ws.uddiregistry/plugin.xml" destination="${entOutputDir}" />
 		
 		<!-- Get the list of extension doc files. -->
 		<fileset dir="${entOutputDir}" casesensitive="yes" id="ext-docs-list-ent">
 			<include name="*.html"/>
 		</fileset>
 		 						
 		<!-- Convert the list to an XML representation. -->
 		<pathconvert property="ext-doc-ent" refid="ext-docs-list-ent" pathsep="&lt;/file&gt;&lt;file&gt;">
 			<map from="${entOutputDir}" to=""/>
 		</pathconvert>
 		<echo file="entextdocs.xml">&lt;files component="enterprise"&gt;&lt;file&gt;${ext-doc-ent}&lt;/file&gt;&lt;/files&gt;</echo>
 		 						
 	</target>
	
	 	
	<target name="buildServerExtDocs">
	 	<pde.convertSchemaToHTML manifest="../org.eclipse.jst.server.core/plugin.xml" destination="${serverOutputDir}" />
	 	<pde.convertSchemaToHTML manifest="../org.eclipse.jst.server.generic.core/plugin.xml" destination="${serverOutputDir}" />
		
		<!-- Get the list of extension doc files. -->
		<fileset dir="${serverOutputDir}" casesensitive="yes" id="ext-docs-list-server">
			<include name="*.html"/>
		</fileset>
								
		<!-- Convert the list to an XML representation. -->
		<pathconvert property="ext-doc-server" refid="ext-docs-list-server" pathsep="&lt;/file&gt;&lt;file&gt;">
			<map from="${serverOutputDir}" to=""/>
		</pathconvert>
		<echo file="serverextdocs.xml">&lt;files component="server"&gt;&lt;file&gt;${ext-doc-server}&lt;/file&gt;&lt;/files&gt;</echo>
						
	</target>
	
	<target name="buildWebExtDocs">
		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.common.annotations.controller/plugin.xml" destination="${webOutputDir}" />
		<pde.convertSchemaToHTML manifest="../org.eclipse.jst.j2ee.web/plugin.xml" destination="${webOutputDir}" />
		
		<!-- Get the list of extension doc files. -->
		<fileset dir="${webOutputDir}" casesensitive="yes" id="ext-docs-list-web">
			<include name="*.html"/>
		</fileset>
								
		<!-- Convert the list to an XML representation. -->
		<pathconvert property="ext-doc-web" refid="ext-docs-list-web" pathsep="&lt;/file&gt;&lt;file&gt;">
			<map from="${webOutputDir}" to=""/>
		</pathconvert>
		<echo file="webextdocs.xml">&lt;files component="web"&gt;&lt;file&gt;${ext-doc-web}&lt;/file&gt;&lt;/files&gt;</echo>
					
	</target>
	
	<target name="createDocListings">
		<!-- Create the overview document. -->
		<xslt basedir="${basedir}" in="extDocListings.xml" out="${outputDir}${file.separator}index.html" style="extDocOverview.xsl">
	  		<param name="title" expression="${title}"/>
	  		<param name="header" expression="${header}"/>
	  		<param name="overview" expression="${overview}"/>
			<param name="listed-ext-points" expression="${listed-ext-points}"/>
		</xslt>
			
		<!-- Create the extention point toc. -->
		<xslt basedir="${basedir}" in="extDocListings.xml" out="topics_ExtPoint_Reference.xml" style="extDocToc.xsl">
			<param name="listed-ext-points" expression="${listed-ext-points}"/>
			<outputproperty name="method" value="xml"/>
			<outputproperty name="indent" value="yes"/>
		</xslt>
			
	</target>

</project>