/*******************************************************************************
 * Copyright (c) 2003, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - Initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.component.ui.internal.adopter.view;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wtp.releng.tools.component.adopters.ClassRef;
import org.eclipse.wtp.releng.tools.component.adopters.FieldRef;
import org.eclipse.wtp.releng.tools.component.adopters.MethodRef;

public class BreakageReportView extends ViewPart
{
  protected Tree tree;
  protected TreeViewer treeViewer;

  public void createPartControl(Composite superparent)
  {
    Composite parent = new Composite(superparent, SWT.NONE);
    GridLayout layout = new GridLayout();
    layout.numColumns = 1;
    parent.setLayout(layout);
    parent.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

    GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);
    gd.grabExcessVerticalSpace = true;
    gd.grabExcessHorizontalSpace = true;
    parent.setLayoutData(gd);

    tree = new Tree(parent, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
    gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);
    gd.grabExcessVerticalSpace = true;
    gd.grabExcessHorizontalSpace = true;
    tree.setLayoutData(gd);

    treeViewer = new TreeViewer(tree);
    treeViewer.setContentProvider(new BreakageReportContentProvider());
    treeViewer.setLabelProvider(new BreakageReportLabelProvider());
    treeViewer.setInput(Boolean.TRUE);
    treeViewer.addDoubleClickListener
    (
      new IDoubleClickListener()
      {
        public void doubleClick(DoubleClickEvent event)
        {
          ISelection selection = event.getSelection();
          if (selection instanceof IStructuredSelection)
          {
            IStructuredSelection structuredSel = (IStructuredSelection)selection;
            Object obj = structuredSel.getFirstElement();
            if (obj instanceof ClassRef || obj instanceof MethodRef || obj instanceof FieldRef)
            {
              String className = null;
              if (obj instanceof ClassRef)
              {
                className = ((ClassRef)obj).getName();
              }
              else
              {
                TreeItem[] items = tree.getSelection();
                if (items.length > 0)
                {
                  className = items[0].getParentItem().getText();
                }
              }
              if (className != null)
              {
                TreeItem[] items = tree.getSelection();
                if (items.length > 0)
                {
                  TreeItem currItem = items[0];
                  TreeItem parentItem = items[0].getParentItem();
                  while (parentItem != null)
                  {
                    currItem = parentItem;
                    parentItem = currItem.getParentItem();
                  }
                  openJava(currItem.getText(), className);
                }
              }
            }
          }
        }
      }
    );
  }

  private void openJava(String projectName, String className)
  {
    try
    {
      IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
      if (project.exists())
      {
        IJavaProject javaProject = JavaCore.create(project);
        IClasspathEntry[] cps = javaProject.getRawClasspath();
        for (int i = 0; i < cps.length; i++)
        {
          if (cps[i].getEntryKind() == IClasspathEntry.CPE_SOURCE)
          {
            StringBuffer sb = new StringBuffer();
            sb.append(cps[i].getPath().toString());
            sb.append(className.replace('.', '/'));
            sb.append(".java");
            IResource res = ResourcesPlugin.getWorkspace().getRoot().findMember(sb.toString());
            if (res != null && res.exists())
            {
              JavaUI.openInEditor(JavaCore.create(res));
            }
          }
        }
      }
    }
    catch (JavaModelException jme)
    {
      jme.printStackTrace();
    }
    catch (PartInitException pie)
    {
      pie.printStackTrace();
    }
  }

  public void setFocus()
  {
    if (tree != null)
      tree.setFocus();
  }

  public void refresh()
  {
    treeViewer.setInput(Boolean.TRUE);
  }

  public void clear()
  {
    treeViewer.setInput(null);
  }
}