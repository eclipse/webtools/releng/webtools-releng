/*******************************************************************************
 * Copyright (c) 2005, 2025 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.action;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.internal.ComponentXML;
import org.eclipse.wtp.releng.tools.component.internal.Plugin;
import org.eclipse.wtp.releng.tools.component.ui.internal.ComponentUIPlugin;
import org.eclipse.wtp.releng.tools.component.ui.internal.WorkspaceFileLocation;

public class Scan4UnitTestCoverage extends Action implements IActionDelegate
{
  public void run()
  {
    String outputDir = ComponentUIPlugin.getDefault().getStateLocation().append("_output_").toString();
    IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
    if (window != null)
    {
      IStructuredSelection selection = (IStructuredSelection)window.getSelectionService().getSelection();
      if (selection != null && !selection.isEmpty())
      {
        Object firstElement = selection.getFirstElement();
        if (firstElement != null && firstElement instanceof IFile)
        {
          try
          {
            IFile file = (IFile)firstElement;
            ILocation compXMLLoc = new WorkspaceFileLocation(file);
            ComponentXML selectedCompXML = new ComponentXML();
            selectedCompXML.setLocation(compXMLLoc);
            selectedCompXML.load();
            Collection plugins = selectedCompXML.getPlugins();
            List srcs = new ArrayList(plugins.size());
            IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
            IPath workspace = workspaceRoot.getLocation();
            for (Iterator it = plugins.iterator(); it.hasNext();)
            {
              Plugin plugin = (Plugin)it.next();
              IProject project = workspaceRoot.getProject(plugin.getId());
              if (project.exists())
              {
                StringBuffer sb = new StringBuffer();
                sb.append(outputDir);
                sb.append('/');
                sb.append(project.getName());
                sb.append("/api-info.xml");
                new File(sb.toString()).delete();
                srcs.add(workspace.append(project.getFullPath()).toFile().toURL().toString().substring(6));
              }
            }
            String[] args = new String[srcs.size() + 6];
            int i = 0;
            args[i++] = "-src";
            for (Iterator it = srcs.iterator(); it.hasNext();)
              args[i++] = (String)it.next();
            args[i++] = "-api";
            args[i++] = compXMLLoc.getAbsolutePath();
            args[i++] = "-outputDir";
            args[i++] = outputDir;
            args[i++] = "-html";
            JavadocScanner2.main(args);
          }
          catch (IOException ioe)
          {
            ioe.printStackTrace();
          }
        }
      }
    }
    // Open report
    try
    {
      IWorkbenchBrowserSupport browserSupport = PlatformUI.getWorkbench().getBrowserSupport();
      IWebBrowser browser = browserSupport.createBrowser("org.eclipse.wtp.releng.tools.component.ui.internal.action.Scan4MissingJavadoc");
      browser.openURL(new File(outputDir + "/api-javadoc-summary.html").toURL());
    }
    catch (PartInitException e)
    {
    }
    catch (MalformedURLException e)
    {
    }
  }

  public void run(IAction action)
  {
    run();
  }

  public void selectionChanged(IAction action, ISelection selection)
  {
  }
}