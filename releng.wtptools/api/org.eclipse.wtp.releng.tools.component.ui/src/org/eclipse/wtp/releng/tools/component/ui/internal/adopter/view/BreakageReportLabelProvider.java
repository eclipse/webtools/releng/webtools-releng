/*******************************************************************************
 * Copyright (c) 2003, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - Initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.component.ui.internal.adopter.view;

import java.util.List;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.wtp.releng.tools.component.adopters.BreakageReport;
import org.eclipse.wtp.releng.tools.component.adopters.ClassRef;
import org.eclipse.wtp.releng.tools.component.adopters.FieldRef;
import org.eclipse.wtp.releng.tools.component.adopters.MethodRef;
import org.eclipse.wtp.releng.tools.component.adopters.PluginRef;
import org.eclipse.wtp.releng.tools.component.adopters.References;

public class BreakageReportLabelProvider extends LabelProvider
{
  /**
  * @see org.eclipse.jface.viewers.ILabelProvider#getImage(Object)
  */
  public Image getImage(Object arg0) 
  { 
    return null;
  }

  /**
   * @see org.eclipse.jface.viewers.ILabelProvider#getText(Object)
   */
  public String getText(Object value) 
  {
    if (value instanceof BreakageReport)
    {
      BreakageReport report = (BreakageReport)value;
      List refs = report.getRefs();
      if (!refs.isEmpty())
      {
        References ref = (References)refs.get(0);
        List pluginRefs = ref.getPluginRefs();
        if (!pluginRefs.isEmpty())
        {
          PluginRef pluginRef = (PluginRef)pluginRefs.get(0);
          return pluginRef.getId();
        }
      }
      return "";
    }
    else if (value instanceof References)
    {
      return ((References)value).getContactInfo();
    }
    else if (value instanceof ClassRef)
    {
      return ((ClassRef)value).getName();
    }
    else if (value instanceof MethodRef)
    {
      MethodRef methodRef = (MethodRef)value;
      StringBuffer sb = new StringBuffer();
      sb.append(methodRef.getName());
      sb.append(methodRef.getDescriptor());
      return sb.toString();
    }
    else if (value instanceof FieldRef)
    {
      return ((FieldRef)value).getName();
    }
    else
    {
      return "";
    }
  }
}