/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal;

import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.plugin.*;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;
import org.osgi.framework.BundleContext;
import java.util.*;

/**
 * The main plugin class to be used in the desktop.
 */
public class ComponentUIPlugin extends AbstractUIPlugin implements IStartup
{
  public static final String ID = "org.eclipse.wtp.releng.tools.component.ui";
  // The shared instance.
  private static ComponentUIPlugin plugin;
  // Resource bundle.
  private ResourceBundle resourceBundle;

  /**
   * The constructor.
   */
  public ComponentUIPlugin()
  {
    super();
    plugin = this;
  }

  public void earlyStartup()
  {
    ComponentManager manager = ComponentManager.getManager();
    /*
     * TODO: Restore resource change listener
     *
    manager.init();
    ResourcesPlugin.getWorkspace().addResourceChangeListener(manager, IResourceChangeEvent.POST_CHANGE);
    */
  }

  /**
   * This method is called upon plug-in activation
   */
  public void start(BundleContext context) throws Exception
  {
    super.start(context);
  }

  /**
   * This method is called when the plug-in is stopped
   */
  public void stop(BundleContext context) throws Exception
  {
    super.stop(context);
    plugin = null;
    resourceBundle = null;
  }

  /**
   * Returns the shared instance.
   */
  public static ComponentUIPlugin getDefault()
  {
    return plugin;
  }

  /**
   * Returns the string from the plugin's resource bundle, or 'key' if not
   * found.
   */
  public static String getResourceString(String key)
  {
    ResourceBundle bundle = ComponentUIPlugin.getDefault().getResourceBundle();
    try
    {
      return (bundle != null) ? bundle.getString(key) : key;
    }
    catch (MissingResourceException e)
    {
      return key;
    }
  }

  /**
   * Returns the plugin's resource bundle,
   */
  public ResourceBundle getResourceBundle()
  {
    try
    {
      if (resourceBundle == null)
        resourceBundle = ResourceBundle.getBundle("org.eclipse.wtp.releng.tools.component.ui.internal.ComponentUIPluginResources");
    }
    catch (MissingResourceException x)
    {
      resourceBundle = null;
    }
    return resourceBundle;
  }

  public String getPluginStateLocation()
  {
    return getStateLocation().addTrailingSeparator().toOSString();
  }
}