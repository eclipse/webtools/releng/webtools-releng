/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.action;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.adopters.Java2API;
import org.eclipse.wtp.releng.tools.component.api.ComponentXMLVisitor;
import org.eclipse.wtp.releng.tools.component.api.violation.NonAPIDependencyScanner;
import org.eclipse.wtp.releng.tools.component.internal.ComponentXML;
import org.eclipse.wtp.releng.tools.component.internal.Location;
import org.eclipse.wtp.releng.tools.component.internal.Plugin;
import org.eclipse.wtp.releng.tools.component.ui.internal.ComponentUIPlugin;
import org.eclipse.wtp.releng.tools.component.ui.internal.WorkspaceFileLocation;
import org.osgi.service.prefs.Preferences;

public class Scan4NonAPIDependency extends Action implements IActionDelegate
{
  public void run()
  {
    // Get src
    IPreferencesService prefService = Platform.getPreferencesService();
    IEclipsePreferences eclipsePref = prefService.getRootNode();
    Preferences pdePref = eclipsePref.node("/instance/org.eclipse.pde.core");
    String platformPath = pdePref.get("platform_path", null);
    String src = platformPath != null ? platformPath : Platform.getInstallLocation().toString();

    // Get outputDir
    IPath outputDir = ComponentUIPlugin.getDefault().getStateLocation();
    String compXMLOutputDir = outputDir.addTrailingSeparator().append(src.replace(':', '_').replace('/', '_').replace('\\', '_')).toString();
    if (!(new File(compXMLOutputDir).exists()))
    {
      // Get existing component.xml
      List excludePlugins = new ArrayList();
      ComponentXMLVisitor compXMLVisitor = new ComponentXMLVisitor();
      Location.createLocation(new File(src)).accept(compXMLVisitor);
      for (Iterator it = compXMLVisitor.getCompXMLs().iterator(); it.hasNext();)
      {
        ComponentXML compXML = (ComponentXML)it.next();
        for (Iterator it2 = compXML.getPlugins().iterator(); it2.hasNext();)
        {
          Plugin plugin = (Plugin)it2.next();
          excludePlugins.add(plugin.getId());
        }
      }
      // Generate missing component.xml
      List excludes = new ArrayList(1);
      excludes.add(".*internal.*");
      Java2API java2api = new Java2API();
      java2api.setSrc(src);
      java2api.setOutputDir(compXMLOutputDir);
      java2api.setExcludes(excludes);
      java2api.setExcludePlugins(excludePlugins);
      java2api.execute();
    }

    // Generate Non-API dependency report
    String reportOutputDir = null;
    IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
    if (window != null)
    {
      IStructuredSelection selection = (IStructuredSelection)window.getSelectionService().getSelection();
      if (selection != null && !selection.isEmpty())
      {
        Object firstElement = selection.getFirstElement();
        if (firstElement != null && firstElement instanceof IFile)
        {
          try
          {
            IFile file = (IFile)firstElement;
            ILocation compXMLLoc = new WorkspaceFileLocation(file);
            ComponentXML selectedCompXML = new ComponentXML();
            selectedCompXML.setLocation(compXMLLoc);
            selectedCompXML.load();
            Collection plugins = selectedCompXML.getPlugins();
            List srcs = new ArrayList(plugins.size());
            IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
            IPath workspace = workspaceRoot.getLocation();
            for (Iterator it = plugins.iterator(); it.hasNext();)
            {
              Plugin plugin = (Plugin)it.next();
              IProject project = workspaceRoot.getProject(plugin.getId());
              if (project.exists())
                srcs.add(workspace.append(project.getFullPath()).toFile().toURL().toString().substring(6));
            }
            reportOutputDir = outputDir.append("_output_").toString();
            delete(new File(reportOutputDir));
            String[] args = new String[srcs.size() + 13];
            int i = 0;
            args[i++] = "-src";
            for (Iterator it = srcs.iterator(); it.hasNext();)
              args[i++] = (String)it.next();
            args[i++] = "-api";
            args[i++] = compXMLLoc.getAbsolutePath();
            args[i++] = "-outputDir";
            args[i++] = reportOutputDir;
            args[i++] = "-refapi";
            args[i++] = compXMLOutputDir;
            args[i++] = "-includes";
            args[i++] = "org.eclipse.*";
            NonAPIDependencyScanner.main(args);
          }
          catch (Throwable ioe)
          {
            ioe.printStackTrace();
          }
        }
      }
    }

    // Open report
    try
    {
      IWorkbenchBrowserSupport browserSupport = PlatformUI.getWorkbench().getBrowserSupport();
      IWebBrowser browser = browserSupport.createBrowser("org.eclipse.wtp.releng.tools.component.ui.internal.action.Scan4APIViolation");
      browser.openURL(new File(reportOutputDir + "/component-api-violation-all.html").toURL());
    }
    catch (PartInitException e)
    {
    }
    catch (MalformedURLException e)
    {
    }
  }

  public void run(IAction action)
  {
    run();
  }

  public void selectionChanged(IAction action, ISelection selection)
  {
  }

  private boolean delete(File file)
  {
    String name = file.getName();
    if (name.endsWith(".gif") || name.endsWith(".jpg") || name.endsWith(".html"))
      return true;
    else if (file.isDirectory())
    {
      boolean b = true;
      File[] children = file.listFiles();
      for (int i = 0; i < children.length; i++)
        if (!delete(children[i]))
          b = false;
      return b;
    }
    else
      return file.delete();
  }
}