/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.editor;

import java.util.Collection;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.wtp.releng.tools.component.internal.ComponentDepends;
import org.eclipse.wtp.releng.tools.component.internal.ComponentRef;
import org.eclipse.wtp.releng.tools.component.internal.ComponentXML;
import org.eclipse.wtp.releng.tools.component.internal.Package;
import org.eclipse.wtp.releng.tools.component.internal.Plugin;
import org.eclipse.wtp.releng.tools.component.internal.Type;

public class ComponentXMLProvider extends LabelProvider implements ITreeContentProvider, ILabelProvider
{
  public static final int SHOW_COMPONENT_REFS = 0x0;
  public static final int SHOW_PLUGINS = 0x1;
  public static final int SHOW_APIS = 0x2;
  private int show;

  public ComponentXMLProvider(int show)
  {
    this.show = show;
  }

  // ITreeContentProvider

  public boolean hasChildren(Object element)
  {
    if (element instanceof Package)
    {
      return ((Package)element).getTypes().size() > 0;
    }
    return false;
  }

  public Object[] getChildren(Object parentElement)
  {
    if (parentElement instanceof Package)
    {
      Collection types = ((Package)parentElement).getTypes();
      return types.toArray(new Type[0]);
    }
    return new Object[0];
  }

  public Object[] getElements(Object inputElement)
  {
    if (show == SHOW_COMPONENT_REFS && inputElement instanceof ComponentDepends)
    {
      Collection compRefs = ((ComponentDepends)inputElement).getComponentRefs();
      return compRefs.toArray(new ComponentRef[0]);
    }
    else if (show == SHOW_PLUGINS && inputElement instanceof ComponentXML)
    {
      Collection plugins = ((ComponentXML)inputElement).getPlugins();
      return plugins.toArray(new Plugin[0]);
    }
    else if (show == SHOW_APIS && inputElement instanceof ComponentXML)
    {
      Collection packages = ((ComponentXML)inputElement).getPackages();
      return packages.toArray(new Package[0]);
    }
    return new Object[0];
  }

  public Object getParent(Object element)
  {
    return null;
  }

  public void dispose()
  {
    // do nothing
  }

  public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
  {
    // do nothing
  }

  // ILabelProvider

  public String getText(Object element)
  {
    if (show == SHOW_COMPONENT_REFS && element instanceof ComponentRef)
    {
      return ((ComponentRef)element).getName();
    }
    else if (show == SHOW_PLUGINS && element instanceof Plugin)
    {
      return ((Plugin)element).getId();
    }
    else if (show == SHOW_APIS)
    {
      if (element instanceof Package)
      {
        return ((Package)element).getName();
      }
      else if (element instanceof Type)
      {
        return ((Type)element).getName();
      }
    }
    return element.toString();
  }
}
