/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.editor;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IBundleGroup;
import org.eclipse.core.runtime.IBundleGroupProvider;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wtp.releng.tools.component.IPluginXML;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;
import org.osgi.framework.Bundle;

public class PluginDialog extends Dialog implements ITreeContentProvider
{
  private List ignoreNames;
  private Tree plugins;
  private String[] pluginIds;

  public PluginDialog(Shell shell, List ignoreNames)
  {
    super(shell);
    this.ignoreNames = (ignoreNames != null) ? ignoreNames : new ArrayList(0);
  }

  protected void configureShell(Shell shell)
  {
    super.configureShell(shell);
    shell.setText(ComponentManager.getManager().getMessage("DIALOG_TITLE_ADD_PLUGINS"));
  }

  protected Control createDialogArea(Composite parent)
  {
    Composite composite = (Composite)super.createDialogArea(parent);
    GridLayout gl = new GridLayout();
    gl.marginWidth = 5;
    gl.marginHeight = 5;
    GridData gd = new GridData(GridData.GRAB_HORIZONTAL | GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL | GridData.FILL_VERTICAL);
    gd.widthHint = 300;
    gd.heightHint = 300;
    composite.setLayout(gl);
    composite.setLayoutData(gd);
    plugins = new Tree(composite, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
    plugins.setLayout(gl);
    plugins.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL | GridData.FILL_VERTICAL));
    TreeViewer pluginsViewer = new TreeViewer(plugins);
    pluginsViewer.setContentProvider(this);
    pluginsViewer.setLabelProvider(new LabelProvider());
    pluginsViewer.setInput(new byte[0]);
    return composite;
  }

  protected void okPressed()
  {
    TreeItem[] items = plugins.getSelection();
    pluginIds = new String[items.length];
    for (int i = 0; i < pluginIds.length; i++)
      pluginIds[i] = items[i].getText();
    super.okPressed();
  }

  protected void cancelPressed()
  {
    pluginIds = new String[0];
    super.cancelPressed();
  }

  public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
  {
    // do nothing
  }

  public void dispose()
  {
    // do nothing
  }

  public boolean hasChildren(Object element)
  {
    return false;
  }

  public Object[] getChildren(Object parentElement)
  {
    return new Object[0];
  }

  public Object getParent(Object element)
  {
    return null;
  }

  public Object[] getElements(Object inputElement)
  {
    List names = new ArrayList();
    IBundleGroupProvider[] bundleProviders = Platform.getBundleGroupProviders();
    for (int i = 0; i < bundleProviders.length; i++)
    {
      IBundleGroup[] bundleGroups = bundleProviders[i].getBundleGroups();
      for (int j = 0; j < bundleGroups.length; j++)
      {
        Bundle[] bundles = bundleGroups[j].getBundles();
        for (int k = 0; k < bundles.length; k++)
        {
          String symbolicName = bundles[k].getSymbolicName();
          if (!ignoreNames.contains(symbolicName))
            names.add(symbolicName);
        }
      }
    }
    IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
    for (int i = 0; i < projects.length; i++)
    {
      IResource res = projects[i].findMember(IPluginXML.CONST_PLUGIN_XML);
      if (res != null && res.getType() == IResource.FILE)
      {
        String projectName = projects[i].getName();
        if (!ignoreNames.contains(projectName))
          names.add(projectName);
      }
    }
    return names.toArray(new String[0]);
  }

  public String[] getPluginIds()
  {
    return pluginIds;
  }
}
