/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal;

import java.io.IOException;
import org.eclipse.wtp.releng.tools.component.internal.Clazz;

public class BundleClazz extends Clazz
{
  private byte[] content;

  public BundleClazz(byte[] content)
  {
    super(null);
    this.content = content;
  }

  protected byte[] getInputBytes() throws IOException
  {
    return content;
  }
}
