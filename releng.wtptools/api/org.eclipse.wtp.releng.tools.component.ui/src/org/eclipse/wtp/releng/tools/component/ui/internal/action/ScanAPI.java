/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.action;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wtp.releng.tools.component.IPluginXML;
import org.eclipse.wtp.releng.tools.component.api.Source;
import org.eclipse.wtp.releng.tools.component.api.violation.ComponentViolationEmitter;
import org.eclipse.wtp.releng.tools.component.internal.ComponentXML;
import org.eclipse.wtp.releng.tools.component.internal.Plugin;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;
import org.eclipse.wtp.releng.tools.component.ui.internal.ScannableComponent;
import org.eclipse.wtp.releng.tools.component.ui.internal.WorkspaceFileLocation;
import org.eclipse.wtp.releng.tools.component.ui.internal.WorkspacePluginXML;
import org.eclipse.wtp.releng.tools.component.ui.internal.job.ScanComponent;

public class ScanAPI extends Action implements IActionDelegate
{
  private ScanComponent job;

  public void run()
  {
    IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
    if (window != null)
    {
      IStructuredSelection selection = (IStructuredSelection)window.getSelectionService().getSelection();
      if (selection != null && !selection.isEmpty())
      {
        Object firstElement = selection.getFirstElement();
        if (firstElement != null && firstElement instanceof IFile)
        {
          ComponentManager manager = ComponentManager.getManager();
          ScannableComponent scannableComp = getScannableComponent((IFile)firstElement);
          if (scannableComp != null)
          {
            job = new ScanComponent(scannableComp, true);
            job.schedule();
            ProgressMonitorDialog dialog = new ProgressMonitorDialog(window.getShell());
            try
            {
              dialog.run(false, false, new IRunnableWithProgress()
              {
                public void run(IProgressMonitor monitor)
                {
                  boolean interrupted = true;
                  while (interrupted)
                  {
                    try
                    {
                      job.join();
                      interrupted = false;
                    }
                    catch (InterruptedException e)
                    {
                      interrupted = true;
                    }
                  }
                }
              });
            }
            catch (InterruptedException e)
            {
              e.printStackTrace();
            }
            catch (InvocationTargetException e)
            {
              e.printStackTrace();
            }
            if (hasAPIViolation(job.getSources()))
              MessageDialog.openError(window.getShell(), manager.getMessage("TITLE_HAS_API_VIOLATIONS"), manager.getMessage("ERROR_MSG_HAS_API_VIOLATIONS"));
            else
              MessageDialog.openInformation(window.getShell(), manager.getMessage("TITLE_HAS_API_VIOLATIONS"), manager.getMessage("INFO_MSG_NO_API_VIOLATIONS"));
          }
          else
            MessageDialog.openError(window.getShell(), manager.getMessage("TITLE_NO_WORKSPACE_PLUGINS"), manager.getMessage("ERROR_MSG_NO_WORKSPACE_PLUGINS"));
        }
      }
    }
  }

  private ScannableComponent getScannableComponent(IFile compXMLFile)
  {
    ComponentXML compXML = new ComponentXML();
    compXML.setLocation(new WorkspaceFileLocation(compXMLFile));
    try
    {
      compXML.load();
    }
    catch (IOException e)
    {
      return null;
    }
    Collection plugins = compXML.getPlugins();
    Map pluginId2Plugins = new HashMap(plugins.size());
    List workspacePluginProjects = new ArrayList();
    ComponentManager manager = ComponentManager.getManager();
    for (Iterator it = plugins.iterator(); it.hasNext();)
    {
      Plugin plugin = (Plugin)it.next();
      IPluginXML pluginXML = manager.getPlugin(plugin.getId());
      pluginId2Plugins.put(pluginXML.getName(), pluginXML);
      if (pluginXML instanceof WorkspacePluginXML)
        workspacePluginProjects.add(((WorkspacePluginXML)pluginXML).getProject());
    }
    if (workspacePluginProjects.size() > 0)
    {
      Map compXMLs = new HashMap(1);
      compXMLs.put(compXML.getLocation().getAbsolutePath(), compXML);
      ComponentViolationEmitter emitter = new ComponentViolationEmitter(null);
      emitter.setDebug(true);
      List excludes = new ArrayList();
      excludes.add("java.");
      excludes.add("javax.");
      excludes.add("org.w3c.");
      excludes.add("org.xml.");
      excludes.add("org.apache.");
      excludes.add("sun.");
      emitter.setClassUseExcludes(excludes);
      emitter.init(compXMLs, getCompRefs(), pluginId2Plugins, new HashMap(0));
      return new ScannableComponent(compXML, emitter, workspacePluginProjects);
    }
    else
      return null;
  }

  /*
  private ScannableComponent getScannableComponent(IFile compXML)
  {
    for (Iterator it = ComponentManager.getManager().getScannableComponents().values().iterator(); it.hasNext();)
    {
      ScannableComponent scannableComp = (ScannableComponent)it.next();
      String scannableCompLoc = scannableComp.getCompXML().getLocation().getAbsolutePath();
      String compLoc = new WorkspaceFileLocation(compXML).getAbsolutePath();
      if (scannableCompLoc.equals(compLoc))
        return scannableComp;
    }
    return null;
  }
  */

  private Map getCompRefs()
  {
    final Map refs = new HashMap();
    try
    {
      ResourcesPlugin.getWorkspace().getRoot().accept
      (
        new IResourceProxyVisitor()
        {
          public boolean visit(IResourceProxy res)
          {
            if (res.getType() == IResource.FILE && res.getName().equals(ComponentXML.CONST_COMPONENT_XML))
            {
              IFile file = (IFile)res.requestResource();
              WorkspaceFileLocation location = new WorkspaceFileLocation(file);
              ComponentXML compXML = new ComponentXML();
              compXML.setLocation(location);
              try
              {
                compXML.load();
                refs.put(compXML.getLocation().getAbsolutePath(), compXML);
              }
              catch (IOException e)
              {
              }
            }
            return true;
          }
        },
        IResource.DEPTH_INFINITE | IResource.NONE
      );
    }
    catch (CoreException e)
    {
    }
    return refs;
  }

  private boolean hasAPIViolation(List sources)
  {
    if (sources != null)
      for (int i = 0; i < sources.size(); i++)
        if (((Source)sources.get(i)).getClassUses().size() > 0)
          return true;
    return false;
  }

  public void run(IAction action)
  {
    run();
  }

  public void selectionChanged(IAction action, ISelection selection)
  {
  }
}
