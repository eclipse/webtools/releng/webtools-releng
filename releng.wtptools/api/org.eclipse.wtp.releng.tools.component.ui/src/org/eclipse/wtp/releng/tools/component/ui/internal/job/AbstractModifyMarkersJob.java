/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.job;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.wtp.releng.tools.component.api.ClassUse;
import org.eclipse.wtp.releng.tools.component.api.FieldUse;
import org.eclipse.wtp.releng.tools.component.api.MethodUse;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;

public abstract class AbstractModifyMarkersJob extends Job
{
  public AbstractModifyMarkersJob(String name)
  {
    super(name);
  }

  public void createClassViolationMarker(IResource javaSource, ClassUse classUse) throws CoreException
  {
    String className = classUse.getName();
    ComponentManager manager = ComponentManager.getManager();
    if (classUse.getReference() != null && classUse.isReference())
    {
      int methodRefCount = classUse.sizeMethodUses();
      int fieldRefCount = classUse.sizeFieldUses();
      if (methodRefCount > 0 || fieldRefCount > 0)
      {
        if (methodRefCount > 0)
        {
          Collection methodUses = classUse.getMethodUses();
          for (Iterator it = methodUses.iterator(); it.hasNext();)
            createMethodViolationMarker(javaSource, classUse, (MethodUse)it.next());
        }
        if (fieldRefCount > 0)
        {
          Collection fieldUses = classUse.getFieldUses();
          for (Iterator it = fieldUses.iterator(); it.hasNext();)
            createFieldViolationMarker(javaSource, classUse, (FieldUse)it.next());
        }
      }
      else
      {
        createMarker(javaSource, manager.getMessage("VIOLATION_CLASS_REF", new String[] {className}), -1);
      }
    }
    if (classUse.getSubclass() != null && classUse.isSubclass())
      createMarker(javaSource, manager.getMessage("VIOLATION_CLASS_SUBCLASS", new String[] {className}), -1);
    if (classUse.getImplement() != null && classUse.isImplement())
      createMarker(javaSource, manager.getMessage("VIOLATION_CLASS_IMPLEMENT", new String[] {className}), -1);
    if (classUse.getInstantiate() != null && classUse.isInstantiate())
    {
      if (classUse.sizeLines() > 0)
        createMarker(javaSource, manager.getMessage("VIOLATION_CLASS_INSTANTIATE", new String[] {className}), classUse.getLines());
      else
        createMarker(javaSource, manager.getMessage("VIOLATION_CLASS_INSTANTIATE", new String[] {className}), -1);
    }
  }

  private void createMethodViolationMarker(IResource javaSource, ClassUse classUse, MethodUse methodUse) throws CoreException
  {
    String className = classUse.getName();
    String methodName = methodUse.getName();
    ComponentManager manager = ComponentManager.getManager();
    if (methodUse.sizeLines() > 0)
      createMarker(javaSource, manager.getMessage("VIOLATION_METHOD_REF", new String[] {methodName, className}), methodUse.getLines());
    else
      createMarker(javaSource, manager.getMessage("VIOLATION_METHOD_REF", new String[] {methodName, className}), -1);
  }

  private void createFieldViolationMarker(IResource javaSource, ClassUse classUse, FieldUse fieldUse) throws CoreException
  {
    String className = classUse.getName();
    String fieldName = fieldUse.getName();
    ComponentManager manager = ComponentManager.getManager();
    if (fieldUse.sizeLines() > 0)
      createMarker(javaSource, manager.getMessage("VIOLATION_FIELD_REF", new String[] {fieldName, className}), fieldUse.getLines());
    else
      createMarker(javaSource, manager.getMessage("VIOLATION_FIELD_REF", new String[] {fieldName, className}), -1);
  }

  private void createMarker(IResource resource, String message, Collection lines) throws CoreException
  {
    for (Iterator it = lines.iterator(); it.hasNext();)
    {
      try
      {
        createMarker(resource, message, Integer.parseInt((String)it.next()));
      }
      catch (NumberFormatException e)
      {
        createMarker(resource, message, -1);
      }
    }
  }

  private void createMarker(IResource resource, String message, int line) throws CoreException
  {
    IJobManager jobManager = Job.getJobManager();
    try
    {
      jobManager.beginRule(resource, new NullProgressMonitor());
    }
    finally
    {
      jobManager.endRule(resource);
    }
    try
    {
      IMarker marker = resource.createMarker(IMarker.PROBLEM);
      marker.setAttribute(ComponentManager.MARKER_COMPONENT_VIOLATION, true);
      marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
      marker.setAttribute(IMarker.MESSAGE, message);
      if (line != -1)
        marker.setAttribute(IMarker.LINE_NUMBER, line);
    }
    catch (Throwable t)
    {
      // do nothing here
    }
  }

  public void deleteViolationMarksers(IResource res) throws CoreException
  {
    IMarker[] markers = res.findMarkers(IMarker.PROBLEM, false, IResource.DEPTH_INFINITE);
    for (int i = 0; i < markers.length; i++)
    {
      if (markers[i].getAttribute(ComponentManager.MARKER_COMPONENT_VIOLATION) != null)
      {
        IJobManager jobManager = Job.getJobManager();
        try
        {
          jobManager.beginRule(res, new NullProgressMonitor());
        }
        finally
        {
          jobManager.endRule(res);
        }
        try
        {
          markers[i].delete();
        }
        catch (Throwable t)
        {
          // do nothing here
        }
      }
    }
  }
}
