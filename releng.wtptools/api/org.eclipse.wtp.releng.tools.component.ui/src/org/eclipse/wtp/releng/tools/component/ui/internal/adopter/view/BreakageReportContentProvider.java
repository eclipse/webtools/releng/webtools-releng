/*******************************************************************************
 * Copyright (c) 2003, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - Initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.component.ui.internal.adopter.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.wtp.releng.tools.component.adopters.BreakageReport;
import org.eclipse.wtp.releng.tools.component.adopters.ClassRef;
import org.eclipse.wtp.releng.tools.component.adopters.PluginRef;
import org.eclipse.wtp.releng.tools.component.adopters.References;
import org.eclipse.wtp.releng.tools.component.ui.internal.adopter.preference.UsageReportsPrefPage;

/**
 * View of server, their configurations and status.
 */
public class BreakageReportContentProvider implements ITreeContentProvider
{
  public Object[] getElements(Object value) 
  {
    if (value == Boolean.TRUE)
    {
      final List reports = new ArrayList();
      IContainer output = UsageReportsPrefPage.getOutput();
      if (output != null)
      {
        try
        {
          output.accept(new IResourceProxyVisitor()
          {
            public boolean visit(IResourceProxy proxy) throws CoreException
            {
              if (proxy.getName().endsWith(".breakage"))
              {
                try
                {
                  BreakageReport breakageReport = new BreakageReport();
                  breakageReport.load(((IFile)proxy.requestResource()).getContents());
                  reports.add(breakageReport);
                }
                catch (Throwable t)
                {
                  t.printStackTrace();
                }
              }
              return true;
            }
          }, IContainer.NONE);
        }
        catch (CoreException ce)
        {
          ce.printStackTrace();
        }
        return reports.toArray(new BreakageReport[0]);
      }
    }
    return new Object[0];
  }

  /*
   * @see org.eclipse.jface.viewers.IContentProvider#dispose()
   */
  public void dispose() 
  {
  }

  /*
   * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(Viewer, Object, Object)
   */
  public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
  {
  }

  /*
   * @see oorg.eclipse.jface.viewers.ITreeContentProvider#getChildren(Object parentElement)
   */
  public Object[] getChildren(Object parentElement)
  {
    if (parentElement instanceof BreakageReport)
    {
      return ((BreakageReport)parentElement).getRefs().toArray(new References[0]);
    }
    else if (parentElement instanceof References)
    {
      References ref = (References)parentElement;
      List classRefs = new ArrayList();
      for (Iterator it = ref.getPluginRefs().iterator(); it.hasNext();)
      {
        PluginRef pluginRef = (PluginRef)it.next();
        classRefs.addAll(pluginRef.getClassRefs());
      }
      return classRefs.toArray(new ClassRef[0]);
    }
    else if (parentElement instanceof ClassRef)
    {
      ClassRef classRef = (ClassRef)parentElement;
      List l = new ArrayList();
      l.addAll(classRef.getMethodRefs());
      l.addAll(classRef.getFieldRefs());
      return l.toArray();
    }
    else
    {
      return new Object[0];
    }
  }

  /*
   * @see oorg.eclipse.jface.viewers.ITreeContentProvider#getParent(Object element)
   */
  public Object getParent(Object element)
  {
    return null;
  }

  /**
   * @see oorg.eclipse.jface.viewers.ITreeContentProvider#hasChildren(Object element)
   */
  public boolean hasChildren(Object element)
  {
    return (element instanceof BreakageReport) || (element instanceof References) || (element instanceof ClassRef);
  }
}