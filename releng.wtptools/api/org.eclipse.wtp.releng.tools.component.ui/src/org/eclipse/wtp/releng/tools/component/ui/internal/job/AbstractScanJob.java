/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.job;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;

public abstract class AbstractScanJob extends AbstractModifyMarkersJob
{
  public AbstractScanJob(String name)
  {
    super(name);
  }

  protected void joinBuilds()
  {
    boolean interrupted = true;
    while (interrupted)
    {
      try
      {
        IJobManager jobManager = Job.getJobManager();
        jobManager.join(ResourcesPlugin.FAMILY_AUTO_BUILD, null);
        jobManager.join(ResourcesPlugin.FAMILY_MANUAL_BUILD, null);
        interrupted = false;
      }
      catch (InterruptedException e)
      {
        interrupted = true;
      }
    }
  }
}
