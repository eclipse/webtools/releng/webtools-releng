/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.wtp.releng.tools.component.internal.ComponentXML;
import org.eclipse.wtp.releng.tools.component.internal.Plugin;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;
import org.eclipse.wtp.releng.tools.component.ui.internal.ComponentUIPlugin;
import org.eclipse.wtp.releng.tools.component.ui.internal.WorkspaceFileLocation;

public class InitComponentManager extends Job implements IResourceProxyVisitor
{
  private List scannableComps;

  public InitComponentManager()
  {
    super(ComponentManager.getManager().getMessage("JOB_INIT_COMPONENT_MANAGER"));
  }

  public IStatus run(IProgressMonitor monitor)
  {
    init();
    return new Status(IStatus.OK, ComponentUIPlugin.ID, IStatus.OK, "", null);
  }

  private void init()
  {
    scannableComps = new ArrayList();
    IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
    for (int i = 0; i < projects.length; i++)
    {
      try
      {
        projects[i].accept(this, IResource.DEPTH_INFINITE | IResource.NONE);
      }
      catch (CoreException e)
      {
        e.printStackTrace();
      }
    }
    ComponentManager manager = ComponentManager.getManager();
    for (Iterator it = scannableComps.iterator(); it.hasNext();)
      manager.addScannableComponent((ComponentXML)it.next());
    scannableComps = null;
  }

  public boolean visit(IResourceProxy resProxy)
  {
    if (resProxy.getType() == IResource.FILE && resProxy.getName().equals(ComponentXML.CONST_COMPONENT_XML))
    {
      IFile file = (IFile)resProxy.requestResource();
      WorkspaceFileLocation location = new WorkspaceFileLocation(file);
      ComponentXML compXML = new ComponentXML();
      compXML.setLocation(location);
      try
      {
        compXML.load();
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
      ComponentManager manager = ComponentManager.getManager();
      manager.addCompRef(compXML);
      Collection plugins = compXML.getPlugins();
      for (Iterator it = plugins.iterator(); it.hasNext();)
      {
        if (manager.isWorkspacePlugin(((Plugin)it.next()).getId()))
        {
          if (scannableComps == null)
            scannableComps = new ArrayList();
          scannableComps.add(compXML);
          break;
        }
      }
    }
    return true;
  }
}
