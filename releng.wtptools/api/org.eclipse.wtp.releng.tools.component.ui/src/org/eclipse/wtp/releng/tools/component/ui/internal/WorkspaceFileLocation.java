/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.wtp.releng.tools.component.internal.FileLocation;

public class WorkspaceFileLocation extends FileLocation
{
  private IFile workspaceFile;

  public WorkspaceFileLocation(IFile workspaceFile)
  {
    super(Platform.getLocation().append(workspaceFile.getFullPath()).toFile());
    this.workspaceFile = workspaceFile;
  }

  public IFile getWorkspaceFile()
  {
    return workspaceFile;
  }
}