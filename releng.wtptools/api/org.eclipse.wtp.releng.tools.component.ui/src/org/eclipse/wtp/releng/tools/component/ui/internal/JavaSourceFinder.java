/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;

public class JavaSourceFinder implements IResourceProxyVisitor
{
  private String className;
  private IResource javaSource;

  public JavaSourceFinder(String className)
  {
    this.className = className;
    int dollarSign = className.indexOf('$');
    if (dollarSign != -1)
      this.className = className.substring(0, dollarSign);
  }

  public IResource getJavaSource()
  {
    return javaSource;
  }

  public boolean visit(IResourceProxy resProxy)
  {
    String resName = resProxy.getName();
    if (resName.endsWith(".java"))
    {
      int resNameLength = resName.length();
      int dot = className.length() - (resNameLength - 5) - 1;
      if (dot > -1 && className.charAt(dot) == '.')
      {
        if (className.endsWith(resName.substring(0, resNameLength - 5)))
        {
          javaSource = resProxy.requestResource();
        }
      }
    }
    return javaSource == null;
  }
}