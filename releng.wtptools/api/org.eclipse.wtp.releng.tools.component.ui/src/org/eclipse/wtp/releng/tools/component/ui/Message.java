/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Message
{
  private static ResourceBundle bundle;

  public static String getMessage(String key)
  {
    if (bundle == null)
    {
      try
      {
        bundle = ResourceBundle.getBundle("org.eclipse.wtp.releng.tools.component.ui.component");
      }
      catch (MissingResourceException e)
      {
        return key;
      }
    }
    return bundle.getString(key);
  }

  public static String getMessage(String key, String[] subsitutes)
  {
    return MessageFormat.format(getMessage(key), subsitutes);
  }
}