/*******************************************************************************
 * Copyright (c) 2000, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.component.ui.internal.adopter.preference;

import java.util.Iterator;
import java.util.List;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.wtp.releng.tools.component.ui.Message;

/**
 * This class provides a dialog for browsing workspace resources. See also
 * {@link DialogUtils#browseResources}.
 */
public class ContainerSelectionDialog extends Dialog
{
  private boolean multiSel;
  private Tree tree;
  private TreeViewer treeViewer;
  private IContainer[] selections;
  private List excludes;

  public ContainerSelectionDialog(Shell parent, boolean multiSel, List excludes)
  {
    super(parent);
    setShellStyle(SWT.DIALOG_TRIM | SWT.RESIZE | SWT.APPLICATION_MODAL);
    this.multiSel = multiSel;
    this.excludes = excludes;
  }

  public IContainer[] getSelections()
  {
    return selections;
  }

  protected void cancelPressed()
  {
    selections = null;
    setReturnCode(Dialog.CANCEL);
    super.cancelPressed();
  }

  protected void okPressed()
  {
    ISelection selection = treeViewer.getSelection();
    if (selection instanceof IStructuredSelection)
    {
      IStructuredSelection structuredSelection = (IStructuredSelection)selection;
      selections = new IContainer[structuredSelection.size()];
      int i = 0;
      for (Iterator it = structuredSelection.iterator(); it.hasNext();)
      {
        selections[i++] = (IContainer)it.next();
      }
    }
    setReturnCode(Dialog.OK);
    super.okPressed();
  }

  protected void configureShell(Shell shell)
  {
    super.configureShell(shell);
    shell.setText(Message.getMessage("TITLE_CONTAINER_SELECTION_DIALOG"));
  }

  protected Control createDialogArea(Composite superparent)
  {
    Composite parent = new Composite(superparent, SWT.NONE);
    GridLayout layout = new GridLayout();
    layout.numColumns = 1;
    parent.setLayout(layout);
    parent.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

    GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);
    gd.widthHint = 400;
    gd.heightHint = 300;
    gd.grabExcessVerticalSpace = true;
    gd.grabExcessHorizontalSpace = true;
    parent.setLayoutData(gd);

    if (multiSel)
      tree = new Tree(parent, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
    else
      tree = new Tree(parent, SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
    gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);
    gd.grabExcessVerticalSpace = true;
    gd.grabExcessHorizontalSpace = true;
    tree.setLayoutData(gd);

    treeViewer = new TreeViewer(tree);
    treeViewer.setContentProvider(new WorkbenchContentProvider());
    treeViewer.setLabelProvider(new DecoratingLabelProvider(new WorkbenchLabelProvider(), PlatformUI.getWorkbench().getDecoratorManager().getLabelDecorator()));
    treeViewer.addFilter
    (
      new ViewerFilter()
      {
        public boolean select(Viewer viewer, Object parentObject, Object object)
        {
          if (object instanceof IContainer && (excludes == null || !excludes.contains(((IContainer)object).getName())))
            return true;
          else
            return false;
        }
      }
    );
    treeViewer.setInput(ResourcesPlugin.getWorkspace().getRoot());

    org.eclipse.jface.dialogs.Dialog.applyDialogFont(superparent);
    return parent;
  }
}