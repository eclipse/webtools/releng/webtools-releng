/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.action;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.wtp.releng.tools.component.CommandOptionParser;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ILocationVisitor;
import org.eclipse.wtp.releng.tools.component.adopters.JavadocScanner;
import org.eclipse.wtp.releng.tools.component.adopters.JavadocVisitor;
import org.eclipse.wtp.releng.tools.component.api.ComponentAPI;
import org.eclipse.wtp.releng.tools.component.internal.Location;

public class JavadocScanner2 extends JavadocScanner
{
  private String currSrc = null;

  protected void scanJavaSources()
  {
    IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
    IPath workspace = workspaceRoot.getLocation();
    for (Iterator it = getSrc().iterator(); it.hasNext();)
    {
      final String pluginId = new File((String)it.next()).getName();
      IProject project = workspaceRoot.getProject(pluginId);
      if (project.exists())
      {
        try
        {
          IJavaProject javaProj = JavaCore.create(project);
          IClasspathEntry[] cpEntries = javaProj.getRawClasspath();
          for (int i = 0; i < cpEntries.length; i++)
          {
            if (cpEntries[i].getEntryKind() == IClasspathEntry.CPE_SOURCE)
            {
              currSrc = workspace.append(cpEntries[i].getPath()).toFile().toURL().toString().substring(6);
              ILocation srcLocation = Location.createLocation(new File(currSrc));
              srcLocation.accept(new ILocationVisitor()
              {
                public boolean accept(ILocation location)
                {
                  if (location.getName().endsWith(".java"))
                    visit(pluginId, location);
                  return true;
                }
              });
            }
          }
        }
        catch (JavaModelException jme)
        {
          jme.printStackTrace();
        }
        catch (MalformedURLException murle)
        {
          murle.printStackTrace();
        }
      }
    }
  }

  public boolean visit(String pluginId, ILocation java)
  {
    String qualifiedName = java.getAbsolutePath().substring(currSrc.length());
    char firstChar = qualifiedName.charAt(0);
    if (firstChar == '/' || firstChar == '\\')
      qualifiedName = qualifiedName.substring(1);
    if (include(qualifiedName) && isAPI(pluginId, qualifiedName))
    {
      try
      {
        ComponentAPI compAPI = getComponentAPI(pluginId);
        InputStreamReader isr = new InputStreamReader(java.getInputStream());
        CharArrayWriter caw = new CharArrayWriter();
        char[] c = new char[2048];
        for (int read = isr.read(c); read != -1; read = isr.read(c))
          caw.write(c, 0, read);
        isr.close();
        caw.close();
        if (astParser == null)
          astParser = ASTParser.newParser(AST.JLS3);
        astParser.setSource(caw.toCharArray());
        ASTNode node = astParser.createAST(null);
        node.accept(new JavadocVisitor(compAPI, pluginId));
      }
      catch (IOException e)
      {
        throw new RuntimeException(e);
      }
    }
    return true;
  }

  public static void main(String[] args)
  {
    CommandOptionParser optionParser = new CommandOptionParser(args);
    Map options = optionParser.getOptions();
    Collection src = (Collection)options.get("src");
    Collection api = (Collection)options.get("api");
    Collection outputDir = (Collection)options.get("outputDir");
    Collection includes = (Collection)options.get("includes");
    Collection excludes = (Collection)options.get("excludes");
    Collection skipAPIGen = (Collection)options.get("skipAPIGen");
    Collection html = (Collection)options.get("html");
    Collection xsl = (Collection)options.get("xsl");
    if (src == null || api == null || outputDir == null || src.isEmpty() || api.isEmpty() || outputDir.isEmpty())
    {
      printUsage();
      System.exit(-1);
    }
    JavadocScanner2 javadocScanner2 = new JavadocScanner2();
    javadocScanner2.setSrc(src);
    javadocScanner2.setApi((String)api.iterator().next());
    javadocScanner2.setOutputDir((String)outputDir.iterator().next());
    javadocScanner2.setIncludes(includes);
    javadocScanner2.setExcludes(excludes);
    javadocScanner2.setSkipAPIGen(skipAPIGen != null);
    javadocScanner2.setHtml(html != null);
    javadocScanner2.setXsl(xsl != null && !xsl.isEmpty() ? (String)xsl.iterator().next() : null);
    javadocScanner2.execute();
  }
}