/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.editor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.wtp.releng.tools.component.internal.ComponentXML;
import org.eclipse.wtp.releng.tools.component.internal.Package;
import org.eclipse.wtp.releng.tools.component.internal.Type;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;

public class APIPage extends FormPage
{
  public static final String ID = "org.eclipse.wtp.releng.tools.component.ui.internal.editor.APIPage";
  private final char KEY_DEL = 127; 

  private Button isAPI;
  private Button isExclusive;
  private Button reference;
  private Button subclass;
  private Button implement;
  private Button instantiate;
  private Tree apis;
  private TreeViewer apisViewer;

  public APIPage(FormEditor editor, String id, String title)
  {
    super(editor, id, title);
  }

  public APIPage(String id, String title)
  {
    super(id, title);
  }

  private void init()
  {
    ComponentXML compXML = ((ComponentXMLEditor)getEditor()).getComponentXML();
    apisViewer = new TreeViewer(apis);
    ComponentXMLProvider apisProvider = new ComponentXMLProvider(ComponentXMLProvider.SHOW_APIS);
    apisViewer.setContentProvider(apisProvider);
    apisViewer.setLabelProvider(apisProvider);
    apisViewer.setInput(compXML);
  }

  protected void createFormContent(IManagedForm managedForm)
  {
    ComponentManager manager = ComponentManager.getManager();
    super.createFormContent(managedForm);
    ScrolledForm form = managedForm.getForm();
    form.setText(manager.getMessage("PAGE_API"));
    Composite body = form.getBody();
    GridLayout gl = new GridLayout();
    gl.numColumns = 2;
    gl.makeColumnsEqualWidth = true;
    gl.marginWidth = 10;
    gl.verticalSpacing = 20;
    gl.horizontalSpacing = 20;
    body.setLayout(gl);
    FormToolkit toolkit = managedForm.getToolkit();
    createLeftColumn(managedForm, toolkit.createComposite(body));
    createRightColumn(managedForm, toolkit.createComposite(body));
    toolkit.paintBordersFor(body);
    init();
  }

  private void createLeftColumn(IManagedForm managedForm, Composite parent)
  {
    createAPIs(managedForm, parent);
  }

  private void createRightColumn(IManagedForm managedForm, Composite parent)
  {
    createAPIUsages(managedForm, parent);
  }

  private void createAPIs(IManagedForm managedForm, Composite parent)
  {
    GridLayout gl = new GridLayout();
    gl.numColumns = 1;
    gl.marginWidth = 1;
    gl.marginHeight = 5;
    parent.setLayout(gl);
    GridData gd = new GridData(GridData.GRAB_HORIZONTAL | GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL | GridData.FILL_VERTICAL);
    parent.setLayoutData(gd);
    ComponentManager manager = ComponentManager.getManager();
    FormToolkit toolkit = managedForm.getToolkit();
    toolkit.createLabel(parent, manager.getMessage("SECTION_DESC_APIS"));
    toolkit.createLabel(parent, manager.getMessage("LABEL_APIS"));
    Composite apiComposite = toolkit.createComposite(parent);
    GridLayout gl2 = new GridLayout();
    gl2.numColumns = 2;
    gl2.marginWidth = 1;
    gl2.marginHeight = 1;
    apiComposite.setLayout(gl2);
    apiComposite.setLayoutData(gd);
    apis = toolkit.createTree(apiComposite, SWT.MULTI);
    apis.setLayout(gl);
    apis.setLayoutData(gd);
    apis.addSelectionListener
    (
      new SelectionListener()
      {
        public void widgetSelected(SelectionEvent event)
        {
          selectAPIEvent(event);
        }
        public void widgetDefaultSelected(SelectionEvent event)
        {
          selectAPIEvent(event);
        }
      }
    );
    apis.addKeyListener
    (
      new KeyListener()
      {
        public void keyPressed(KeyEvent event)
        {
          // do nothing
        }
        public void keyReleased(KeyEvent event)
        {
          apiKeyEvent(event);
        }
      }
    );
    Menu menu = new Menu(apis);
    MenuItem addPackage = new MenuItem(menu, SWT.CASCADE);
    addPackage.setText(manager.getMessage("LABEL_ADD_PACKAGE"));
    addPackage.addSelectionListener
    (
      new SelectionListener()
      {
        public void widgetSelected(SelectionEvent event)
        {
          addPackageEvent(event);
        }

        public void widgetDefaultSelected(SelectionEvent event)
        {
          addPackageEvent(event);
        }
      }
    );
    MenuItem addType = new MenuItem(menu, SWT.CASCADE);
    addType.setText(manager.getMessage("LABEL_ADD_TYPE"));
    addType.addSelectionListener
    (
      new SelectionListener()
      {
        public void widgetSelected(SelectionEvent event)
        {
          addTypeEvent(event);
        }

        public void widgetDefaultSelected(SelectionEvent event)
        {
          addTypeEvent(event);
        }
      }
    );
    apis.setMenu(menu);
    toolkit.paintBordersFor(apiComposite);
    toolkit.paintBordersFor(parent);
  }

  private void createAPIUsages(IManagedForm managedForm, Composite parent)
  {
    GridLayout gl = new GridLayout();
    gl.numColumns = 1;
    gl.marginWidth = 1;
    gl.marginHeight = 5;
    parent.setLayout(gl);
    GridData gd = new GridData(GridData.GRAB_HORIZONTAL | GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL | GridData.FILL_VERTICAL);
    parent.setLayoutData(gd);
    ComponentManager manager = ComponentManager.getManager();
    FormToolkit toolkit = managedForm.getToolkit();
    toolkit.createLabel(parent, manager.getMessage("SECTION_DESC_API_USAGES"));
    Composite usageComposite = toolkit.createComposite(parent);
    GridLayout gl2 = new GridLayout();
    gl2.numColumns = 1;
    gl2.marginWidth = 10;
    gl2.marginHeight = 1;
    usageComposite.setLayout(gl2);
    usageComposite.setLayoutData(gd);
    toolkit.createLabel(usageComposite, manager.getMessage("LABEL_PACKAGE_USAGE"));
    SelectionListener listener = new SelectionListener()
    {
      public void widgetSelected(SelectionEvent event)
      {
        selectAPIUsageEvent(event);
      }

      public void widgetDefaultSelected(SelectionEvent event)
      {
        selectAPIUsageEvent(event);
      }
    };
    isAPI = toolkit.createButton(usageComposite, manager.getMessage("LABEL_PACKAGE_API"), SWT.CHECK);
    isAPI.setEnabled(false);
    isAPI.addSelectionListener(listener);
    isExclusive = toolkit.createButton(usageComposite, manager.getMessage("LABEL_PACKAGE_EXCLUSIVE"), SWT.CHECK);
    isExclusive.setEnabled(false);
    isExclusive.addSelectionListener(listener);
    toolkit.createLabel(usageComposite, manager.getMessage("LABEL_TYPE_USAGE"));
    reference = toolkit.createButton(usageComposite, manager.getMessage("LABEL_REFERENCE"), SWT.CHECK);
    reference.setEnabled(false);
    reference.addSelectionListener(listener);
    subclass = toolkit.createButton(usageComposite, manager.getMessage("LABEL_SUBCLASS"), SWT.CHECK);
    subclass.setEnabled(false);
    subclass.addSelectionListener(listener);
    implement = toolkit.createButton(usageComposite, manager.getMessage("LABEL_IMPLEMENT"), SWT.CHECK);
    implement.setEnabled(false);
    implement.addSelectionListener(listener);
    instantiate = toolkit.createButton(usageComposite, manager.getMessage("LABEL_INSTANTIATE"), SWT.CHECK);
    instantiate.setEnabled(false);
    instantiate.addSelectionListener(listener);
    toolkit.paintBordersFor(parent);
  }

  private void addPackageEvent(SelectionEvent event)
  {
    TreeItem[] items = apis.getItems();
    List ignoreNames = new ArrayList(items.length);
    for (int i = 0; i < items.length; i++)
      ignoreNames.add(items[i].getText());
    ComponentXMLEditor editor = (ComponentXMLEditor)getEditor();
    ComponentXML compXML = editor.getComponentXML();
    APIDialog dialog = new APIDialog(editor.getSite().getShell(), APIDialog.OPTION_PACKAGE, compXML, null, ignoreNames);
    if (dialog.open() == Dialog.OK)
    {
      editor.setDirty(true);
      apisViewer.refresh();
    }
  }

  private void addTypeEvent(SelectionEvent event)
  {
    Package pkg = null;
    TreeItem[] items = apis.getSelection();
    if (items.length > 0)
    {
      Object firstItem = items[0].getData();
      if (firstItem instanceof Package)
        pkg = (Package)firstItem;
      else if (firstItem instanceof Type)
        pkg = (Package)items[0].getParentItem().getData();
    }
    if (pkg != null)
    {
      Collection types = pkg.getTypes();
      List ignoreNames = new ArrayList(types.size());
      for (Iterator it = types.iterator(); it.hasNext();)
        ignoreNames.add(((Type)it.next()).getName());
      ComponentXMLEditor editor = (ComponentXMLEditor)getEditor();
      ComponentXML compXML = editor.getComponentXML();
      APIDialog dialog = new APIDialog(editor.getSite().getShell(), APIDialog.OPTION_TYPE, compXML, pkg, ignoreNames);
      if (dialog.open() == Dialog.OK)
      {
        editor.setDirty(true);
        apisViewer.refresh();
      }
    }
  }

  private void selectAPIEvent(SelectionEvent event)
  {
    Object firstItem = getFirstSelection();
    if (firstItem instanceof Package)
    {
      Package pkg = (Package)firstItem;
      isAPI.setEnabled(true);
      isAPI.setSelection(pkg.isApi());
      isExclusive.setEnabled(true);
      isExclusive.setSelection(pkg.isExclusive());
      reference.setEnabled(false);
      reference.setSelection(false);
      subclass.setEnabled(false);
      subclass.setSelection(false);
      implement.setEnabled(false);
      implement.setSelection(false);
      instantiate.setEnabled(false);
      instantiate.setSelection(false);
    }
    else if (firstItem instanceof Type)
    {
      Type type = (Type)firstItem;
      isAPI.setEnabled(false);
      isAPI.setSelection(false);
      isExclusive.setEnabled(false);
      isExclusive.setSelection(false);
      reference.setEnabled(true);
      reference.setSelection(type.isReference());
      subclass.setEnabled(true);
      subclass.setSelection(type.isSubclass());
      implement.setEnabled(true);
      implement.setSelection(type.isImplement());
      instantiate.setEnabled(true);
      instantiate.setSelection(type.isInstantiate());
    }
    else
    {
      isAPI.setEnabled(false);
      isAPI.setSelection(false);
      isExclusive.setEnabled(false);
      isExclusive.setSelection(false);
      reference.setEnabled(false);
      reference.setSelection(false);
      subclass.setEnabled(false);
      subclass.setSelection(false);
      implement.setEnabled(false);
      implement.setSelection(false);
      instantiate.setEnabled(false);
      instantiate.setSelection(false);
    }
  }

  private void selectAPIUsageEvent(SelectionEvent event)
  {
    if (event.widget == isAPI)
      ((Package)getFirstSelection()).setApi(isAPI.getSelection() ? null : Boolean.FALSE);
    else if (event.widget == isExclusive)
      ((Package)getFirstSelection()).setExclusive(isExclusive.getSelection() ? null : Boolean.FALSE);
    else if (event.widget == reference)
      ((Type)getFirstSelection()).setReference(reference.getSelection() ? null : Boolean.FALSE);
    else if (event.widget == subclass)
      ((Type)getFirstSelection()).setSubclass(subclass.getSelection() ? null : Boolean.FALSE);
    else if (event.widget == implement)
      ((Type)getFirstSelection()).setImplement(implement.getSelection() ? null : Boolean.FALSE);
    else if (event.widget == instantiate)
      ((Type)getFirstSelection()).setInstantiate(instantiate.getSelection() ? null : Boolean.FALSE);
    ComponentXMLEditor editor = (ComponentXMLEditor)getEditor();
    editor.setDirty(true);
  }

  private Object getFirstSelection()
  {
    TreeItem[] items = apis.getSelection();
    if (items.length > 0)
      return items[0].getData();
    else
      return null;
  }

  private void apiKeyEvent(KeyEvent event)
  {
    ComponentXML compXML = ((ComponentXMLEditor)getEditor()).getComponentXML();
    if (event.character == KEY_DEL) 
    {
      TreeItem[] items = apis.getSelection();
      for (int i = 0; i < items.length; i++)
      {
        Object item = items[i].getData();
        if (item instanceof Package)
          compXML.getPackages().remove(item);
        else if (item instanceof Type)
          for (Iterator it = compXML.getPackages().iterator(); it.hasNext();)
            if (((Package)it.next()).getTypes().remove(item))
              break;
      }
      isAPI.setEnabled(false);
      isAPI.setSelection(false);
      isExclusive.setEnabled(false);
      isExclusive.setSelection(false);
      reference.setEnabled(false);
      reference.setSelection(false);
      subclass.setEnabled(false);
      subclass.setSelection(false);
      implement.setEnabled(false);
      implement.setSelection(false);
      instantiate.setEnabled(false);
      instantiate.setSelection(false);
      apisViewer.refresh();
      ComponentXMLEditor editor = (ComponentXMLEditor)getEditor();
      editor.setDirty(true);
    }
  }
}