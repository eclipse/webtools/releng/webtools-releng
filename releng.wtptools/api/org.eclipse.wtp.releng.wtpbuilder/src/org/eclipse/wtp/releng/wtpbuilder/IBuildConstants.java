/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.wtpbuilder;

/**
 * This interfaces houses the common build constants used in the java wrapping of the build ant tasks
 */
public interface IBuildConstants {

	public static final String BASE_ARCH = "basearch"; //$NON-NLS-1$
	public static final String BASE_OS = "baseos"; //$NON-NLS-1$
	public static final String BASE_WS = "basews"; //$NON-NLS-1$
	public static final String BUILD_BRANCH = "buildBranch"; //$NON-NLS-1$
	public static final String BUILD_COMMITTERS = "build.committers"; //$NON-NLS-1$
	public static final String BUILD_HOME = "build.home"; //$NON-NLS-1$
	public static final String BUILD_ID = "buildId"; //$NON-NLS-1$
	public static final String BUILD_STREAM = "build.stream"; //$NON-NLS-1$
	public static final String BUILD_TYPE = "buildType"; //$NON-NLS-1$
	public static final String BUILD_TYPE_S = "S"; //$NON-NLS-1$
	public static final String BUILD_TYPE_I = "I"; //$NON-NLS-1$
	public static final String BUILD_TYPE_N = "N"; //$NON-NLS-1$
	public static final String BUILD_TYPE_M = "M"; //$NON-NLS-1$
	public static final String CLEAN = "clean"; //$NON-NLS-1$
	public static final String COMMITTER = "committer"; //$NON-NLS-1$
	public static final String CONFIG = "config"; //$NON-NLS-1$
	public static final String DATE = "date"; //$NON-NLS-1$
	public static final String DISPLAY = "display"; //$NON-NLS-1$
	public static final String FILENAME = "filename"; //$NON-NLS-1$
	public static final String GRAPH = "graph"; //$NON-NLS-1$
	public static final String LOGIN = "login"; //$NON-NLS-1$
	public static final String MIN_TS = "minTS"; //$NON-NLS-1$
	public static final String PUBLIC = "public"; //$NON-NLS-1$
	public static final String TIME = "time"; //$NON-NLS-1$
	public static final String TIMESTAMP = "timestamp"; //$NON-NLS-1$
	public static final String UPLOAD = "upload"; //$NON-NLS-1$
}
