/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.wtpbuilder;

/**
 * This class models and stores information about a particular build instance.
 */
public class Build implements IBuildConstants {
	
	/**
	 * Is the build a public build?
	 */
	private boolean publicBuild;

	/**
	 * Is the build a maintennace, integration, stable, or nightly?
	 */
	private String type;

	/**
	 * The id for the build
	 */
	private String id;

	/**
	 * The date of the build
	 */
	private String date;

	/**
	 * The time given to that build
	 */
	private String time;

	/**
	 * What stream was the build in, i.e., R1.5 or R2.0?
	 */
	private String stream;

	/**
	 * The constructor takes a set of information about the build instance
	 * @param aPublicBuild
	 * @param aType
	 * @param anId
	 * @param aDate
	 * @param aTime
	 * @param aStream
	 */
	public Build(boolean aPublicBuild, String aType, String anId, String aDate, String aTime, String aStream) {
		this.publicBuild = aPublicBuild;
		this.type = aType;
		this.id = anId;
		this.date = aDate;
		this.time = aTime;
		this.stream = aStream;
	}

	/**
	 * Is the build a public build?  If not it is a committer build
	 */
	public boolean isPublicBuild() {
		return publicBuild;
	}

	/**
	 * Set public build field
	 * @param publicBuild
	 */
	public void setPublicBuild(boolean publicBuild) {
		this.publicBuild = publicBuild;
	}

	/**
	 * Returns the build's date as a string
	 * @return String date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Set the build's date
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Get the uild's id as a string
	 * @return String id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set the build's id
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Return the build's time as a string
	 * @return String time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * Set the build's time
	 * @param time
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * Is the build an I,S,M,or N?
	 * @return String build type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Set the build's type of I,S,M,or N
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Return the build's stream, i.e. R1.5 or R2.0
	 * @return String stream
	 */
	public String getStream() {
		return stream;
	}

	/**
	 * Set the build's stream, i.e. R1.5 or R2.0
	 * @param stream
	 */
	public void setStream(String stream) {
		this.stream = stream;
	}

	/**
	 * Return whether or not this build instance is newer than the given build
	 * @param build
	 * @return boolean if Newer
	 */
	public boolean isNewer(Build build) {
		int thisDate = Integer.parseInt(date);
		int thatDate = Integer.parseInt(build.getDate());
		if (thisDate == thatDate) {
			int thisTime = Integer.parseInt(time);
			int thatTime = Integer.parseInt(build.getTime());
			if (thisTime == thatTime)
				return getBuildTypeWeight(type) > getBuildTypeWeight(build.getType());
			return thisTime > thatTime;
		}
		return thisDate > thatDate;
	}

	/**
	 * Return the build instance as a string
	 * @return String
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(type);
		sb.append("-"); //$NON-NLS-1$
		sb.append(id);
		sb.append("-"); //$NON-NLS-1$
		sb.append(date);
		sb.append(time);
		return sb.toString();
	}

	/**
	 * Helper method used in determining which type of build should get precedence.
	 * @param buildType
	 * @return int build type weight
	 */
	private int getBuildTypeWeight(String buildType) {
		if (BUILD_TYPE_S.equals(buildType))
			return 40;
		else if (BUILD_TYPE_I.equals(buildType))
			return 30;
		else if (BUILD_TYPE_M.equals(buildType))
			return 20;
		else if (BUILD_TYPE_N.equals(buildType))
			return 10;
		else
			return 0;
	}
}