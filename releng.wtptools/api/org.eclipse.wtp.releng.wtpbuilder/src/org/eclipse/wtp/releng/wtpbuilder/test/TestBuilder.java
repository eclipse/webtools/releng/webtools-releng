/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.wtpbuilder.test;

import org.eclipse.wtp.releng.wtpbuilder.AbstractBuilder;
import org.eclipse.wtp.releng.wtpbuilder.Build;
import org.eclipse.wtp.releng.wtpbuilder.CommandOptionParser;
import org.eclipse.wtp.releng.wtpbuilder.Main;

/**
 * This class can be used as a builder to run a junit ant task suite from a java executable.  This will cache
 * the completed builds and can be used in a scheduled task to continuously check for new scans.
 */
public class TestBuilder extends AbstractBuilder {
	
	/**
	 * This is the filename for the cache of the completed builds with scans already performed 
	 */
	private static final String TEST_SUITE_COMPLETED_BUILDS_FILE = "test_suite_completed_builds.properties"; //$NON-NLS-1$
	
	/**
	 * Print out the appropriate usage
	 */
	private static final String USAGE = "Usage: java org.eclipse.wtp.releng.wtpbuilder.test.TestBuilder -baseos <baseos> -basews <basews> -basearch <basearch> -build.home <buildHome>"; //$NON-NLS-1$
	
	/**
	 * The target snt file script location
	 */
	private static final String BUILD_SCRIPT = "/releng.wtpbuilder/distribution/wtp.tests/standaloneTest.xml"; //$NON-NLS-1$

	/**
	 * Constructor takes the cached completed builds filename as a param
	 * 
	 * @param completedBuildsFile
	 */
	public TestBuilder(String completedBuildsFile) {
		super(completedBuildsFile);
	}

	/**
	 * Override of build method to take given build, run junit tests, upload results, and clean up
	 * @return boolean success
	 */
	public boolean build(Build build) {
		System.setProperty(BUILD_TYPE, build.getType());
		System.setProperty(BUILD_ID, build.getId());
		System.setProperty(TIMESTAMP, new StringBuffer().append(build.getDate()).append(build.getTime()).toString());
		System.setProperty(BUILD_STREAM, build.getStream());
		System.setProperty(BUILD_BRANCH, build.getStream());
		if (!build.isPublicBuild())
			System.setProperty(BUILD_COMMITTERS, Boolean.TRUE.toString());
		String buildScript = new StringBuffer().append(System.getProperty(BUILD_HOME)).append(BUILD_SCRIPT).toString();
		Main.main(new String[] { "-f", buildScript }); //$NON-NLS-1$
		Main.main(new String[] { "-f", buildScript, UPLOAD }); //$NON-NLS-1$
		Main.main(new String[] { "-f", buildScript, CLEAN }); //$NON-NLS-1$
		return true;
	}

	/**
	 * Main method which parses command line arguments and creates executable instance of the JUnit Test
	 * builder
	 * @param args
	 */
	public static void main(String[] args) {
		CommandOptionParser parser = new CommandOptionParser(args);
		String baseos = parser.getOptionAsString(BASE_OS);
		String basews = parser.getOptionAsString(BASE_WS);
		String basearch = parser.getOptionAsString(BASE_ARCH);
		String login = parser.getOptionAsString(LOGIN);
		String config = parser.getOptionAsString(CONFIG);
		String minTS = parser.getOptionAsString(MIN_TS);
		String buildHome = parser.getOptionAsString(BUILD_HOME);
		if (baseos == null || basews == null || basearch == null || buildHome == null) {
			System.out.println(USAGE);
			System.exit(-1);
		}
		TestBuilder testBuilder = new TestBuilder(TEST_SUITE_COMPLETED_BUILDS_FILE);
		testBuilder.setBaseos(baseos);
		testBuilder.setBasews(basews);
		testBuilder.setBasearch(basearch);
		testBuilder.setLogin(login);
		System.setProperty(CONFIG, config);
		System.setProperty(BUILD_HOME,buildHome);
		if (minTS != null)
			testBuilder.setMinTS(Long.parseLong(minTS));
		testBuilder.main();
	}
}