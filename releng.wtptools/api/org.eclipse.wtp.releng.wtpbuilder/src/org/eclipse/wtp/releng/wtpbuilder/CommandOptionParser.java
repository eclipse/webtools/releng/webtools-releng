/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.wtpbuilder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

/**
 * This is a helper class used to parse command line arguments from java main executables 
 * into specific values.
 */
public class CommandOptionParser {
	
	/**
	 * The delimiter token used to specify a new command line argument
	 */
	private static final String delimiter = "-"; //$NON-NLS-1$

	/**
	 * A cached Map of the command line options
	 */
	private Map options;

	/**
	 * Constructor takes the comman line arguments as a string array.  Invokes parse on the options to initialize
	 * the cached map of options.
	 * @param args
	 */
	public CommandOptionParser(String[] args) {
		parse(args);
	}

	/**
	 * Get the command line argument value for the given key as a collection
	 * @param key
	 * @return Collection
	 */
	private Collection getOption(String key) {
		return (Collection) options.get(key);
	}

	/**
	 * Return the command line argument value for the given argument key as a string
	 * @param key
	 * @return String value
	 */
	public String getOptionAsString(String key) {
		Collection c = getOption(key);
		if (c == null || c.isEmpty())
			return null;
		return (String) c.iterator().next();
	}

	/**
	 * Private helper method to parse the command line arguments into a cached map of options and 
	 * option values.
	 * @param args
	 */
	private void parse(String[] args) {
		options = new HashMap();
		String option = null;
		for (int i = 0; i < args.length; i++) {
			if (args[i] != null) {
				if (args[i].startsWith(delimiter)) {
					option = args[i].substring(1);
					options.put(option, new ArrayList(1));
				} else if (option != null) {
					((List) options.get(option)).add(args[i]);
				}
			}
		}
	}
}
