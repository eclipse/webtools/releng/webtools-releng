/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.wtpbuilder.api;

import org.eclipse.wtp.releng.wtpbuilder.AbstractBuilder;
import org.eclipse.wtp.releng.wtpbuilder.Build;
import org.eclipse.wtp.releng.wtpbuilder.CommandOptionParser;
import org.eclipse.wtp.releng.wtpbuilder.Main;

/**
 * This class will control and wrap the execution of the adopter breakage report ant scan from 
 * a java executable.
 */
public class AdopterBreakageBuilder extends AbstractBuilder {
	
	/**
	 * This is the filename for the cache of the completed builds with scans already performed 
	 */
	private static final String ADOPTER_BREAKAGE_COMPLETED_BUILDS_FILE = "adopter_breakage_completed_builds.properties"; //$NON-NLS-1$
	
	/**
	 * The ant file which will run the adopter breakage scan
	 */
	private static final String BUILD_SCRIPT = "/releng.wtpbuilder/distribution/wtp.adopters/build.xml"; //$NON-NLS-1$
	
	/**
	 * Prints the appropriate usage parameters to the user
	 */
	private static final String USAGE = "Usage: java org.eclipse.wtp.releng.wtpbuilder.api.AdopterBreakageBuilder -baseos <baseos> -basews <basews> -basearch <basearch>"; //$NON-NLS-1$
	
	/**
	 * Constructor takes the cached completed builds filename as a param
	 * 
	 * @param completedBuildsFile
	 */
	public AdopterBreakageBuilder(String completedBuildsFile) {
		super(completedBuildsFile);
	}

	/**
	 * This overrides the build method to call the adopter breakage scan on the given build
	 * 
	 * @param build
	 */
	public boolean build(Build build) {
		System.setProperty(BUILD_TYPE, build.getType());
		System.setProperty(BUILD_ID, build.getId());
		System.setProperty(TIMESTAMP, new StringBuffer().append(build.getDate()).append(build.getTime()).toString());
		System.setProperty(BUILD_STREAM, build.getStream());
		System.setProperty(BUILD_BRANCH, build.getStream());
		if (!build.isPublicBuild())
			System.setProperty(BUILD_COMMITTERS, Boolean.TRUE.toString());
		String buildScript = new StringBuffer().append(System.getProperty(BUILD_HOME)).append(BUILD_SCRIPT).toString();
		Main.main(new String[] { "-f", buildScript }); //$NON-NLS-1$
		return true;
	}

	/**
	 * The main method for the Adopter Breakage Scan controls the parsing of the command line arguments
	 * and the setting up of the execution of the scan.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		CommandOptionParser parser = new CommandOptionParser(args);
		String baseos = parser.getOptionAsString(BASE_OS);
		String basews = parser.getOptionAsString(BASE_WS);
		String basearch = parser.getOptionAsString(BASE_ARCH);
		String login = parser.getOptionAsString(LOGIN);
		String minTS = parser.getOptionAsString(MIN_TS);
		String buildHome = parser.getOptionAsString(BUILD_HOME);
		if (baseos == null || basews == null || basearch == null) {
			System.out.println(USAGE);
			System.exit(-1);
		}
		AdopterBreakageBuilder adopterBreakageBuilder = new AdopterBreakageBuilder(ADOPTER_BREAKAGE_COMPLETED_BUILDS_FILE);
		adopterBreakageBuilder.setBaseos(baseos);
		adopterBreakageBuilder.setBasews(basews);
		adopterBreakageBuilder.setBasearch(basearch);
		adopterBreakageBuilder.setLogin(login);
		System.setProperty(BUILD_HOME,buildHome);
		if (minTS != null)
			adopterBreakageBuilder.setMinTS(Long.parseLong(minTS));
		adopterBreakageBuilder.main();
	}
}