/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.wtpbuilder.api;

import org.eclipse.wtp.releng.wtpbuilder.AbstractBuilder;
import org.eclipse.wtp.releng.wtpbuilder.Build;
import org.eclipse.wtp.releng.wtpbuilder.CommandOptionParser;
import org.eclipse.wtp.releng.wtpbuilder.Main;

/**
 * This is a wrapper class which can be used to run the API declared build ant tests from a java
 * executable. This can be run as a scheduled task or CRON job to automate its execution.
 */
public class APIBuilder extends AbstractBuilder {
	
	/**
	 * The name of the file cache for the builds which have already been scanned
	 */
	private static final String API_COMPLETED_BUILDS_FILE = "api_completed_builds.properties"; //$NON-NLS-1$
	
	/**
	 * Print out the proper command line usage
	 */
	private static final String USAGE = "Usage: java org.eclipse.wtp.releng.wtpbuilder.api.APIBuilder -baseos <baseos> -basews <basews> -basearch <basearch> -build.home <buildHome>"; //$NON-NLS-1$
	
	/**
	 * This is the target ant script which will be called to execute the API tests
	 */
	private static final String BUILD_SCRIPT = "/releng.wtpbuilder/distribution/wtp.api/build.xml"; //$NON-NLS-1$
	
	/**
	 * Constructor passes the filename of the complete builds cache
	 * @param completedBuildsFile
	 */
	public APIBuilder(String completedBuildsFile) {
		super(completedBuildsFile);
	}
	
	/**
	 * Override method to never run api tests on committer builds because they are long running tasks
	 * @return boolean should accept committer builds?
	 */
	protected boolean acceptCommitterBuilds() {
		return false;
	}

	/**
	 * The build method will take the given build and run the API tests for that build.
	 * @param Build
	 * @return boolean success
	 */
	public boolean build(Build build) {
		System.setProperty(BUILD_TYPE, build.getType());
		System.setProperty(BUILD_ID, build.getId());
		System.setProperty(TIMESTAMP, new StringBuffer().append(build.getDate()).append(build.getTime()).toString());
		System.setProperty(BUILD_STREAM, build.getStream());
		System.setProperty(BUILD_BRANCH, build.getStream());
		if (!build.isPublicBuild())
			System.setProperty(BUILD_COMMITTERS, Boolean.TRUE.toString());
		String buildScript = new StringBuffer().append(System.getProperty(BUILD_HOME)).append(BUILD_SCRIPT).toString();
		Main.main(new String[] {"-f", buildScript}); //$NON-NLS-1$
		Main.main(new String[] {"-f", buildScript, CLEAN}); //$NON-NLS-1$
		return true;
	}

	/**
	 * The main method will parse the command line arguments and create the executable instance of the class.
	 * @param args
	 */
	public static void main(String[] args) {
		CommandOptionParser parser = new CommandOptionParser(args);
		String baseos = parser.getOptionAsString(BASE_OS);
		String basews = parser.getOptionAsString(BASE_WS);
		String basearch = parser.getOptionAsString(BASE_ARCH);
		String login = parser.getOptionAsString(LOGIN);
		String minTS = parser.getOptionAsString(MIN_TS);
		String buildHome = parser.getOptionAsString(BUILD_HOME);
		String filename = parser.getOptionAsString(FILENAME);
		if (baseos == null || basews == null || basearch == null || buildHome == null) {
			System.out.println(USAGE);
			System.exit(-1);
		}
		APIBuilder apiBuilder;
		if (filename!=null && filename.length()>0)
			apiBuilder = new APIBuilder(filename);
		else
			apiBuilder = new APIBuilder(API_COMPLETED_BUILDS_FILE);
		apiBuilder.setBaseos(baseos);
		apiBuilder.setBasews(basews);
		apiBuilder.setBasearch(basearch);
		apiBuilder.setLogin(login);
		System.setProperty(BUILD_HOME, buildHome);
		System.setProperty(DISPLAY,"localhost:0"); //$NON-NLS-1$
		if (minTS != null)
			apiBuilder.setMinTS(Long.parseLong(minTS));
		apiBuilder.main();
	}
}