/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.wtpbuilder.perf;

import org.eclipse.wtp.releng.wtpbuilder.AbstractBuilder;
import org.eclipse.wtp.releng.wtpbuilder.Build;
import org.eclipse.wtp.releng.wtpbuilder.CommandOptionParser;
import org.eclipse.wtp.releng.wtpbuilder.Main;

/**
 * This bullder class wraps the execution of a performance ant task suite from a java execuatble.  It
 * stores the cache of scanned builds and can be used in conjunction with a scheduled task or CRON job.
 */
public class PerfBuilder extends AbstractBuilder {
	
	/**
	 * This is the filename for the cache of the completed builds with scans already performed 
	 */
	private static final String PERF_COMPLETED_BUILDS_FILE = "perf_completed_builds.properties"; //$NON-NLS-1$
	
	/**
	 * Print out the appropriate usage
	 */
	private static final String USAGE = "Usage: java org.eclipse.wtp.releng.wtpbuilder.perf.PerfBuilder -baseos <baseos> -basews <basews> -basearch <basearch> -build.home <buildHome>"; //$NON-NLS-1$
	
	/**
	 * The target ant build script file
	 */
	private static final String BUILD_SCRIPT = "/releng.wtpbuilder/distribution/wtp.perf/build.xml"; //$NON-NLS-1$
	
	/**
	 * Constructor takes the cached completed builds filename as a param
	 * 
	 * @param completedBuildsFile
	 */
	public PerfBuilder(String completedBuildsFile) {
		super(completedBuildsFile);
	}
	
	/**
	 * Override method to never run perf tests on committer builds because they are long running tasks
	 * @return boolean should accept committer builds?
	 */
	protected boolean acceptCommitterBuilds() {
		return false;
	}

	/**
	 * The build method controls the execution, graphing, uploading, and cleaning of the performance execution
	 * @return boolean success
	 */
	public boolean build(Build build) {
		System.setProperty(BUILD_TYPE, build.getType());
		System.setProperty(BUILD_ID, build.getId());
		System.setProperty(DATE, build.getDate());
		System.setProperty(TIME, build.getTime());
		System.setProperty(BUILD_STREAM, build.getStream());
		System.setProperty(BUILD_BRANCH, build.getStream());
		if (!build.isPublicBuild())
			System.setProperty(BUILD_COMMITTERS, Boolean.TRUE.toString());
		String buildScript = new StringBuffer().append(System.getProperty(BUILD_HOME)).append(BUILD_SCRIPT).toString();
		Main.main(new String[] { "-f", buildScript}); //$NON-NLS-1$
		Main.main(new String[] { "-f", buildScript, GRAPH}); //$NON-NLS-1$
		Main.main(new String[] { "-f", buildScript, UPLOAD}); //$NON-NLS-1$
		Main.main(new String[] { "-f", buildScript, CLEAN}); //$NON-NLS-1$
		return true;
	}

	/**
	 * The main method parses the command line arguments and creates the executable instance of the
	 * performance builder.
	 * @param args
	 */
	public static void main(String[] args) {
		CommandOptionParser parser = new CommandOptionParser(args);
		String baseos = parser.getOptionAsString(BASE_OS);
		String basews = parser.getOptionAsString(BASE_WS);
		String basearch = parser.getOptionAsString(BASE_ARCH);
		String login = parser.getOptionAsString(LOGIN);
		String minTS = parser.getOptionAsString(MIN_TS);
		String buildHome = parser.getOptionAsString(BUILD_HOME);
		if (baseos == null || basews == null || basearch == null || buildHome == null) {
			System.out.println(USAGE);
			System.exit(-1);
		}
		PerfBuilder perfBuilder = new PerfBuilder(PERF_COMPLETED_BUILDS_FILE);
		perfBuilder.setBaseos(baseos);
		perfBuilder.setBasews(basews);
		perfBuilder.setBasearch(basearch);
		perfBuilder.setLogin(login);
		System.setProperty(BUILD_HOME, buildHome);
		if (minTS != null)
			perfBuilder.setMinTS(Long.parseLong(minTS));
		perfBuilder.main();
	}
}