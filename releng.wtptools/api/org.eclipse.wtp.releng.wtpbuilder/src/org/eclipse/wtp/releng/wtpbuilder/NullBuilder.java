/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.wtpbuilder;

/**
 * The NullBuilder has a no op build and can be used to create a dummy cache file of all the current builds
 */
public class NullBuilder extends AbstractBuilder {
	
	/**
	 * Default constructor
	 */
	public NullBuilder() {
		super();
	}

	/**
	 * Constructor taking the name of the file cache of completed builds as a param
	 * @param completedBuildsFile
	 */
	public NullBuilder(String completedBuildsFile) {
		super(completedBuildsFile);
	}

	/**
	 * The no-op build method.
	 */
	public boolean build(Build build) {
		return true;
	}

	/**
	 * The main method grabs the command line arguments and creates the java executable instance.
	 * @param args
	 */
	public static void main(String[] args) {
		CommandOptionParser parser = new CommandOptionParser(args);
		String filename = parser.getOptionAsString(FILENAME);
		if (filename!=null && filename.length()>0)
			new NullBuilder(filename).main();
		else
			new NullBuilder().main();
	}
	
	/**
	 * Override of main method to loop through all builds
	 */
	protected void main() {
		if (baseos != null)
			System.setProperty(BASE_OS, baseos);
		if (basews != null)
			System.setProperty(BASE_WS, basews);
		if (basearch != null)
			System.setProperty(BASE_ARCH, basearch);
		if (login != null)
			System.setProperty(LOGIN, login);
		System.setProperty(CLEAN, Boolean.TRUE.toString());
		try {
			while (true) {
				String buildId;
				try {
					buildId = build();
				} catch (Throwable t) {
					t.printStackTrace();
					buildId = null;
				}
				if (buildId == null) {
					try {
						Thread.sleep(1800000);
					} catch (InterruptedException ie) {
						ie.printStackTrace();
					}
				}
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
}