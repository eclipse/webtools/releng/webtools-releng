<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<script language="javascript">
				function showComponentsOnly()
				{
					var table = document.getElementById("apiprogresstable");
					var trs = table.getElementsByTagName("tr");
					var startHidding = false;
					for (var i = 0; i &lt; trs.length; i++)
					{
						if (trs[i].id != null &amp;&amp; trs[i].id.indexOf('component::') == 0)
						{
							startHidding = true;
							var imgs = trs[i].getElementsByTagName("img");
							imgs.item(0).src = "twistclosed.gif";
						}
						else if (startHidding)
						{
							trs[i].style.display = "none";
						}
					}
				}
				function showPackagesOnly()
				{
					var table = document.getElementById("apiprogresstable");
					var trs = table.getElementsByTagName("tr");
					var startHidding = false;
					for (var i = 0; i &lt; trs.length; i++)
					{
						if (trs[i].id != null &amp;&amp; trs[i].id.indexOf('component::') == 0)
						{
							var imgs = trs[i].getElementsByTagName("img");
							imgs.item(0).src = "twistopened.gif";
						}
						else if (trs[i].id != null &amp;&amp; trs[i].id.indexOf('package::') == 0)
						{
							startHidding = true;
							trs[i].style.display = "";
							var imgs = trs[i].getElementsByTagName("img");
							imgs.item(1).src = "twistclosed.gif";
						}
						else if (startHidding)
						{
							trs[i].style.display = "none";
						}
					}
				}
				function showClassesOnly()
				{
					var table = document.getElementById("apiprogresstable");
					var trs = table.getElementsByTagName("tr");
					for (var i = 0; i &lt; trs.length; i++)
					{
						if (trs[i].id != null &amp;&amp; trs[i].id.indexOf('component::') == 0)
						{
							var imgs = trs[i].getElementsByTagName("img");
							imgs.item(0).src = "twistopened.gif";
						}
						else if (trs[i].id != null &amp;&amp; trs[i].id.indexOf('package::') == 0)
						{
							var imgs = trs[i].getElementsByTagName("img");
							imgs.item(1).src = "twistopened.gif";
						}
						trs[i].style.display = "";
					}
				}
				function toggleComponent(id)
				{
					var tr = document.getElementById(id);
					var nexttr = getNextTr(tr);
					var imgs = tr.getElementsByTagName("img");
					var twist = imgs.item(0);
					var newstyle;
					if (twist.src.indexOf('twistopened.gif') != -1)
					{
						twist.src = "twistclosed.gif";
						newstyle = "none";
					}
					else
					{
						twist.src = "twistopened.gif";
						newstyle = "";
					}
					while (nexttr != null &amp;&amp; (nexttr.id == null || nexttr.id.indexOf('component::') != 0))
					{
						nexttr.style.display = newstyle;
						if (nexttr.id != null &amp;&amp; nexttr.id.indexOf('package::') != -1)
						{
							var nextimgs = nexttr.getElementsByTagName("img");
							var nexttwist = nextimgs.item(1);
							nexttwist.src = "twistopened.gif";
						}
						nexttr = getNextTr(nexttr);
					}
				}
				function togglePackage(id)
				{
					var tr = document.getElementById(id);
					var nexttr = getNextTr(tr);
					var twist = tr.cells[0].childNodes[1].childNodes[0];
					var newstyle;
					if (twist.src.indexOf('twistopened.gif') != -1)
					{
						twist.src = "twistclosed.gif";
						newstyle = "none";
					}
					else
					{
						twist.src = "twistopened.gif";
						newstyle = "";
					}
					while (nexttr != null &amp;&amp; (nexttr.id == null || (nexttr.id.indexOf('package::') != 0 &amp;&amp; nexttr.id.indexOf('component::') != 0)))
					{
						nexttr.style.display = newstyle;
						nexttr = getNextTr(nexttr);
					}
				}
				function getNextTr(tr)
				{
					var nexttr = tr.nextSibling;
					while (nexttr != null &amp;&amp; nexttr.nodeType != 1)
						nexttr = nexttr.nextSibling;
					return nexttr;
				}
				function openSVG(svg)
				{
					window.open(svg, '_blank');
				}
			</script>
			<body>
				<table width="100%" cellspacing="5" cellpadding="2" border="0">
					<tbody>
						<tr>
							<td width="60%" align="left">
								<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">Eclipse WTP API Progress Report</font>
							</td>
							<td width="40%">
								<img width="120" hspace="50" height="86" align="middle" src="Idea.jpg"/>
							</td>
						</tr>
					</tbody>
				</table>
				<table WIDTH="100%" CELLPADDING="2" CELLSPACING="5" BORDER="0">
					<tr>
						<td BGCOLOR="#0080C0" COLSPAN="2" VALIGN="TOP" ALIGN="LEFT"><b><font face="Arial,Helvetica" color="#FFFFFF">Dependency on non-APIs types</font></b></td>
					</tr>
				</table>
				<p>
					<font face="Arial,Helvetica">
					This <a href="javascript:openSVG('component-api-violation-all.html')">document</a> shows you WTP APIs that have dependencies
					on other non-API types.
					</font>
				</p>
				<table WIDTH="100%" CELLPADDING="2" CELLSPACING="5" BORDER="0">
					<tr>
						<td BGCOLOR="#0080C0" COLSPAN="2" VALIGN="TOP" ALIGN="LEFT"><b><font face="Arial,Helvetica" color="#FFFFFF">API progress</font></b></td>
					</tr>
				</table>
				<font face="Arial,Helvetica">
				<table>
					<tr>
						<td><a href="#viewsvg">How to view SVG graphs in Mozilla Firefox</a></td>
					</tr>
					<tr>
						<td><a href="javascript:showComponentsOnly()">Show components only</a></td>
					</tr>
					<tr>
						<td><a href="javascript:showPackagesOnly()">Show packages only</a></td>
					</tr>
					<tr>
						<td><a href="javascript:showClassesOnly()">Show classes</a></td>
					</tr>
				</table>
				<table id="apiprogresstable">
					<tr>
						<th></th>
						<th>Method API count</th>
						<th>JUnit count (60%)</th>
						<th>Javadoc count (35%)</th>
						<th>Overview document (5%)</th>
						<th width="80">Score (/100)</th>
					</tr>
					<xsl:call-template name="printTotal">
						<xsl:with-param name="root" select="root"/>
						<xsl:with-param name="apicount" select="0"/>
						<xsl:with-param name="testcount" select="0"/>
						<xsl:with-param name="javadoccount" select="0"/>
						<xsl:with-param name="i" select="1"/>
					</xsl:call-template>
					<xsl:for-each select="root/api-info">
						<xsl:sort select="@file"/>
						<xsl:variable name="report" select="document(@file)"/>
						<xsl:apply-templates select="$report/component-api">
							<xsl:with-param name="overviewDoc" select="@overviewDoc"/>
						</xsl:apply-templates>
					</xsl:for-each>
				</table>
				</font>
				<table WIDTH="100%" CELLPADDING="2" CELLSPACING="5" BORDER="0">
					<tr>
						<td BGCOLOR="#0080C0" COLSPAN="2" VALIGN="TOP" ALIGN="LEFT"><b><font face="Arial,Helvetica" color="#FFFFFF">Plugins without a component</font></b></td>
					</tr>
				</table>
				<font face="Arial,Helvetica">
					<table>
						<xsl:choose>
							<xsl:when test="root/plugin-without-comp">
								<xsl:for-each select="root/plugin-without-comp">
									<xsl:sort select="@id"/>
									<tr>
										<td><xsl:value-of select="@id"/></td>
									</tr>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<tr>
									<td>None.</td>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
					</table>
				</font>
				<table WIDTH="100%" CELLPADDING="2" CELLSPACING="5" BORDER="0">
					<tr>
						<td BGCOLOR="#0080C0" COLSPAN="2" VALIGN="TOP" ALIGN="LEFT"><b><font face="Arial,Helvetica" color="#FFFFFF">Plugins declared in a component but not found in the WTP distribution</font></b></td>
					</tr>
				</table>
				<font face="Arial,Helvetica">
					<table>
						<xsl:choose>
							<xsl:when test="root/missing-plugin">
								<xsl:for-each select="root/missing-plugin">
									<xsl:sort select="@id"/>
									<tr>
										<td><xsl:value-of select="@id"/></td>
									</tr>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<tr>
									<td>None.</td>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
					</table>
				</font>
				<table WIDTH="100%" CELLPADDING="2" CELLSPACING="5" BORDER="0">
					<tr>
						<td BGCOLOR="#0080C0" COLSPAN="2" VALIGN="TOP" ALIGN="LEFT"><b><font face="Arial,Helvetica" color="#FFFFFF">Plugins declared in more than one components</font></b></td>
					</tr>
				</table>
				<font face="Arial,Helvetica">
					<table>
						<xsl:choose>
							<xsl:when test="root/dup-plugin">
								<xsl:for-each select="root/dup-plugin">
									<xsl:sort select="@id"/>
									<tr>
										<td><xsl:value-of select="@id"/></td>
									</tr>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<tr>
									<td>None.</td>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
					</table>
				</font>
				<font fact="Arial, Helvetica">
				<table WIDTH="100%" CELLPADDING="2" CELLSPACING="5" BORDER="0" id="viewsvg">
					<tr>
						<td BGCOLOR="#0080C0" COLSPAN="2" VALIGN="TOP" ALIGN="LEFT"><b><font face="Arial,Helvetica" color="#FFFFFF">How to view SVG graphs in Mozilla Firefox</font></b></td>
					</tr>
				</table>
				<p>
				This API progress report includes SVG graphs that show the test/javadoc coverage of a component over time.
				These SVG graphs display nicely in Internet Explorer, but you have to do some work to make them display in
				Mozilla Firefox.
				</p>
				<p>
				SVG is not included in the normal Firefox builds. You have to download a special SVG binary from the
				<a href="http://www.mozilla.org/projects/svg/build.html#win_gdi_dl">SVG page</a>. I downloaded the
				<a href="ftp://ftp.mozilla.org/pub/mozilla.org/firefox/nightly/contrib/latest-trunk/firefox-win32-svg-GDI.zip">Windows XP GDI+ build</a>.
				Unzip this and look for firefox.exe in the bin directory.
				</p>
				<p>
				Next you have to enable svg support. This is proclaimed in the notice:<br/>
				There has recently been an important change to SVG enabled builds. SVG enabled builds now require you to
				<a href="http://weblogs.mozillazine.org/tor/archives/2005/02/important_svg_b.html">set the 'svg.enabled' pref to 'true'</a>
				before they will render SVG. 
				</p>
				<p>
				There is probably some prefs file you can edit. However, there is a slick way to do this via the browser:
				<ol>
					<li>Open the magic URL <b>about:config</b></li>
					<li>Filter the listing with svg and look at the line that contains <b>svg.enabled</b></li>
					<li>This preference is false by default. Double click it to set it to <b>true</b></li>
				</ol>
				You're now ready to view SVG.
				</p>
				</font>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="component-api">
		<xsl:param name="overviewDoc"/>
		<xsl:variable name="apicount" select="count(package-api/class-api/method-api)"/>
		<xsl:variable name="testcount" select="count(package-api/class-api/method-api/test-coverage)"/>
		<xsl:variable name="javadoccount" select="$apicount - count(package-api/class-api/method-api/javadoc-coverage)"/>
		<xsl:variable name="testscore" select="$testcount div $apicount"/>
		<xsl:variable name="javadocscore" select="$javadoccount div $apicount"/>
		<xsl:if test="$apicount &gt; 0">
		<tr id="component::{@name}">
			<td valign="top">
				<h3>
					<a href="javascript:toggleComponent('component::{@name}')"><img border="0" src="twistopened.gif"/></a>&#160;
					<a href="javascript:openSVG('svg/{@name}.svg')"><xsl:value-of select="@name"/></a>&#160;
					<a href="javascript:openSVG('svg/{@name}.svg')"><img src="viewsvg.gif" border="0"/></a>&#160;
				</h3>
			</td>
			<td valign="top"><xsl:value-of select="$apicount"/></td>
			<td valign="top">
				<xsl:if test="$apicount != 0">
					<xsl:call-template name="printProgressBar">
						<xsl:with-param name="numerator" select="$testcount"/>
						<xsl:with-param name="denominator" select="$apicount"/>
					</xsl:call-template>
				</xsl:if>
			</td>
			<td valign="top">
				<xsl:if test="$apicount != 0">
					<xsl:call-template name="printProgressBar">
						<xsl:with-param name="numerator" select="$javadoccount"/>
						<xsl:with-param name="denominator" select="$apicount"/>
					</xsl:call-template>
				</xsl:if>
			</td>
			<td align="center" valign="top">
				<xsl:choose>
					<xsl:when test="$overviewDoc">
						<a href="{$overviewDoc}" target="_blank">Yes</a>
					</xsl:when>
					<xsl:otherwise>
						No
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td align="center" valign="top">
				<xsl:choose>
					<xsl:when test="$apicount != 0">
						<xsl:choose>
							<xsl:when test="$overviewDoc">
								<xsl:value-of select="round(($testscore * 60) + ($javadocscore * 35)) + 5"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="round(($testscore * 60) + ($javadocscore * 35))"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$overviewDoc">
								<xsl:value-of select="100"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="95"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
		<xsl:for-each select="package-api">
			<xsl:sort select="@name"/>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
		</xsl:if>
	</xsl:template>

	<xsl:template match="package-api">
		<xsl:variable name="dot" select="'.'"/>
		<xsl:variable name="apicount" select="count(class-api/method-api)"/>
		<xsl:variable name="testcount" select="count(class-api/method-api/test-coverage)"/>
		<xsl:variable name="incompletejavadoc" select="count(class-api/method-api/javadoc-coverage)"/>
		<xsl:if test="$apicount &gt; 0">
		<tr id="package::{@name}">
			<td>
				<img src="space.gif"/>
				<a href="javascript:togglePackage('package::{@name}')"><img border="0" src="twistopened.gif"/></a>&#160;
				<xsl:value-of select="@name"/>&#160;
			</td>
			<td><xsl:value-of select="$apicount"/></td>
			<td>
				<xsl:if test="$apicount != 0">
					<xsl:call-template name="printProgressBar">
						<xsl:with-param name="numerator" select="$testcount"/>
						<xsl:with-param name="denominator" select="$apicount"/>
					</xsl:call-template>
				</xsl:if>
			</td>
			<td>
				<xsl:if test="$apicount != 0">
					<xsl:call-template name="printProgressBar">
						<xsl:with-param name="numerator" select="$apicount - $incompletejavadoc"/>
						<xsl:with-param name="denominator" select="$apicount"/>
					</xsl:call-template>
				</xsl:if>
			</td>
		</tr>
		<xsl:for-each select="class-api">
			<xsl:sort select="@name"/>
			<xsl:variable name="methodAPICount" select="count(method-api)"/>
			<xsl:if test="$methodAPICount &gt; 0">
				<tr>
					<td>
						<img src="space.gif"/>
						<img src="space.gif"/>
						<xsl:call-template name="lastIndexOf">
							<xsl:with-param name="string" select="@name"/>
							<xsl:with-param name="delimiter" select="$dot"/>
						</xsl:call-template>
					</td>
					<td><xsl:value-of select="$methodAPICount"/></td>
					<td><xsl:value-of select="count(method-api/test-coverage)"/></td>
					<td><xsl:value-of select="$methodAPICount - count(method-api/javadoc-coverage)"/></td>
				</tr>
			</xsl:if>
		</xsl:for-each>
		</xsl:if>
	</xsl:template>

	<xsl:template match="class-api">
		<tr>
			<td><xsl:value-of select="@name"/></td>
			<td><xsl:value-of select="count(method-api)"/></td>
			<td><xsl:value-of select="count(method-api/test-coverage)"/></td>
		</tr>
	</xsl:template>

	<xsl:template name="printProgressBar">
		<xsl:param name="numerator"/>
		<xsl:param name="denominator"/>
		<xsl:variable name="percentComplete" select="$numerator div $denominator"/>
		<xsl:variable name="w1" select="round(100 * $percentComplete)"/>
		<xsl:variable name="w2" select="100 - $w1"/>
		<xsl:if test="$percentComplete &gt; 0.75">
			<img src="green.gif" height="15" width="{$w1}"/>
			<img src="grey.gif" height="15" width="{$w2}"/>
			<font color="green">&#160;<xsl:value-of select="$numerator"/>&#160;/&#160;<xsl:value-of select="$denominator"/>&#160;(<xsl:value-of select="$w1"/>%)&#160;</font>
		</xsl:if>
		<xsl:if test="$percentComplete &gt; 0.5">
		<xsl:if test="$percentComplete &lt;= 0.75">
			<img src="yellow.gif" height="15" width="{$w1}"/>
			<img src="grey.gif" height="15" width="{$w2}"/>
			<font color="#ffc000">&#160;<xsl:value-of select="$numerator"/>&#160;/&#160;<xsl:value-of select="$denominator"/>&#160;(<xsl:value-of select="$w1"/>%)&#160;</font>
		</xsl:if>
		</xsl:if>
		<xsl:if test="$percentComplete &gt; 0.25">
		<xsl:if test="$percentComplete &lt;= 0.5">
			<img src="orange.gif" height="15" width="{$w1}"/>
			<img src="grey.gif" height="15" width="{$w2}"/>
			<font color="orange">&#160;<xsl:value-of select="$numerator"/>&#160;/&#160;<xsl:value-of select="$denominator"/>&#160;(<xsl:value-of select="$w1"/>%)&#160;</font>
		</xsl:if>
		</xsl:if>
		<xsl:if test="$percentComplete &lt;= 0.25">
			<img src="red.gif" height="15" width="{$w1}"/>
			<img src="grey.gif" height="15" width="{$w2}"/>
			<font color="red">&#160;<xsl:value-of select="$numerator"/>&#160;/&#160;<xsl:value-of select="$denominator"/>&#160;(<xsl:value-of select="$w1"/>%)&#160;</font>
		</xsl:if>
	</xsl:template>

	<xsl:template name="lastIndexOf">
		<xsl:param name="string"/>
		<xsl:param name="delimiter"/>
		<xsl:choose>
			<xsl:when test="contains($string, $delimiter)">
				<xsl:call-template name="lastIndexOf">
					<xsl:with-param name="string" select="substring-after($string, $delimiter)"/>
					<xsl:with-param name="delimiter" select="$delimiter"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="$string"/></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="printTotal">
		<xsl:param name="root"/>
		<xsl:param name="apicount"/>
		<xsl:param name="testcount"/>
		<xsl:param name="javadoccount"/>
		<xsl:param name="i"/>
		<xsl:if test="$root/api-info[$i]">
			<xsl:variable name="report" select="document($root/api-info[$i]/@file)"/>
			<xsl:variable name="compapicount" select="count($report/component-api/package-api/class-api/method-api)"/>
			<xsl:variable name="totalapicount" select="$apicount + $compapicount"/>
			<xsl:variable name="totaltestcount" select="$testcount + count($report/component-api/package-api/class-api/method-api/test-coverage)"/>
			<xsl:variable name="totaljavadoccount" select="$javadoccount + ($compapicount - count($report/component-api/package-api/class-api/method-api/javadoc-coverage))"/>
			<xsl:choose>
				<xsl:when test="$root/api-info[$i + 1]">
					<xsl:call-template name="printTotal">
						<xsl:with-param name="root" select="$root"/>
						<xsl:with-param name="apicount" select="$totalapicount"/>
						<xsl:with-param name="testcount" select="$totaltestcount"/>
						<xsl:with-param name="javadoccount" select="$totaljavadoccount"/>
						<xsl:with-param name="i" select="$i + 1"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="printTotal2">
						<xsl:with-param name="totalapicount" select="$totalapicount"/>
						<xsl:with-param name="totaltestcount" select="$totaltestcount"/>
						<xsl:with-param name="totaljavadoccount" select="$totaljavadoccount"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<xsl:template name="printTotal2">
		<xsl:param name="totalapicount"/>
		<xsl:param name="totaltestcount"/>
		<xsl:param name="totaljavadoccount"/>
		<tr><td>&#160;</td></tr>
		<tr>
			<td valign="top">
				<h3>
					<a href="javascript:openSVG('svg/total.svg')">WTP Total</a>&#160;
					<a href="javascript:openSVG('svg/total.svg')"><img src="viewsvg.gif" border="0"/></a>&#160;
				</h3>
			</td>
			<td valign="top"><xsl:value-of select="$totalapicount"/></td>
			<td valign="top">
				<xsl:call-template name="printProgressBar">
					<xsl:with-param name="numerator" select="$totaltestcount"/>
					<xsl:with-param name="denominator" select="$totalapicount"/>
				</xsl:call-template>
			</td>
			<td valign="top">
				<xsl:call-template name="printProgressBar">
					<xsl:with-param name="numerator" select="$totaljavadoccount"/>
					<xsl:with-param name="denominator" select="$totalapicount"/>
				</xsl:call-template>
			</td>
			<td align="center" valign="top"/>
			<td align="center" valign="top"><xsl:value-of select="round(($totaltestcount div $totalapicount * 60) + ($totaljavadoccount div $totalapicount * 35)) + 5"/></td>
		</tr>
		<tr><td>&#160;</td></tr>
	</xsl:template>
</xsl:stylesheet>
