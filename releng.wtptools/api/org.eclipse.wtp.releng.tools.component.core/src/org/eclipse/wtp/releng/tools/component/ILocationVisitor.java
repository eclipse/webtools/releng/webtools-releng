/*******************************************************************************
 * Copyright (c) 2002, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component;

/**
 * An <code>ILocationVisitor</code> can be used to traverse a tree of
 * locations.
 */
public interface ILocationVisitor
{
  /**
   * Allows this visitor to investigate the given location.
   * 
   * @param location
   *          the current location in the traversal
   * @return boolean <code>true</code> if the traversal should continue into
   *         the children of the given location, <code>false</code> otherwise.
   */
  boolean accept(ILocation location);
}
