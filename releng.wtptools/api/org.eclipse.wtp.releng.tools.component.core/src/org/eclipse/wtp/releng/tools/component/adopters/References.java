/*******************************************************************************
 * Copyright (c) 2006, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.adopters;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class References
{
  private Map pluginRefs;
  private String name;
  private String contactInfo;
  private String refBuildId;
  private String includes;
  private String excludes;
  private String includePlugins;
  private String excludePlugins;

  public String getContactInfo()
  {
    return contactInfo;
  }

  public void setContactInfo(String contactInfo)
  {
    this.contactInfo = contactInfo;
  }

  public String getRefBuildId()
  {
    return refBuildId;
  }

  public void setRefBuildId(String refBuildId)
  {
    this.refBuildId = refBuildId;
  }

  public List getPluginRefs()
  {
    if (pluginRefs != null)
      return new ArrayList(pluginRefs.values());
    else
      return new ArrayList(0);
  }

  public PluginRef getPluginRef(String id)
  {
    if (pluginRefs != null)
      return (PluginRef)pluginRefs.get(id);
    else
      return null;
  }

  public void addPluginRef(PluginRef pluginRef)
  {
    if (pluginRefs == null)
      pluginRefs = new HashMap();
    pluginRefs.put(pluginRef.getId(), pluginRef);
  }

  public void removePluginRef(String id)
  {
    if (pluginRefs != null)
      pluginRefs.remove(id);
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getExcludes()
  {
    return excludes;
  }

  public void setExcludes(String excludes)
  {
    this.excludes = excludes;
  }

  public String getIncludes()
  {
    return includes;
  }

  public void setIncludes(String includes)
  {
    this.includes = includes;
  }

  public String getExcludePlugins()
  {
    return excludePlugins;
  }

  public void setExcludePlugins(String excludePlugins)
  {
    this.excludePlugins = excludePlugins;
  }

  public String getIncludePlugins()
  {
    return includePlugins;
  }

  public void setIncludePlugins(String includePlugins)
  {
    this.includePlugins = includePlugins;
  }

  public void load(InputStream is) throws ParserConfigurationException, SAXException, IOException
  {
    SAXParserFactory factory = SAXParserFactory.newInstance();
    factory.setNamespaceAware(false);
    factory.setValidating(false);
    SAXParser parser = factory.newSAXParser();
    parser.parse(new InputSource(is), new ReferencesHandler());
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<references name=\"");
    sb.append(getName());
    sb.append("\" contactInfo=\"");
    sb.append(getContactInfo());
    sb.append("\" refBuildId=\"");
    sb.append(getRefBuildId());
    sb.append("\" includes=\"");
    sb.append(getIncludes());
    sb.append("\" excludes=\"");
    sb.append(getExcludes());
    sb.append("\" includePlugins=\"");
    sb.append(getIncludePlugins());
    sb.append("\" excludePlugins=\"");
    sb.append(getExcludePlugins());
    sb.append("\">");
    for (Iterator it = getPluginRefs().iterator(); it.hasNext();)
      sb.append(it.next().toString());
    sb.append("</references>");
    return sb.toString();
  }

  public void write(OutputStream os) throws IOException
  {
    try
    {
      os.write("<references name=\"".getBytes());
      String s = getName();
      if (s != null)
        os.write(s.getBytes());
      os.write("\" contactInfo=\"".getBytes());
      s = getContactInfo();
      if (s != null)
        os.write(s.getBytes());
      os.write("\" refBuildId=\"".getBytes());
      s = getRefBuildId();
      if (s != null)
        os.write(s.getBytes());
      os.write("\" includes=\"".getBytes());
      s = getIncludes();
      if (s != null)
        os.write(s.getBytes());
      os.write("\" excludes=\"".getBytes());
      s = getExcludes();
      if (s != null)
        os.write(s.getBytes());
      os.write("\" includePlugins=\"".getBytes());
      s = getIncludePlugins();
      if (s != null)
        os.write(s.getBytes());
      os.write("\" excludePlugins=\"".getBytes());
      s = getExcludePlugins();
      if (s != null)
        os.write(s.getBytes());
      os.write("\">".getBytes());
      for (Iterator it = getPluginRefs().iterator(); it.hasNext();)
        os.write(it.next().toString().getBytes());
      os.write("</references>".getBytes());
    }
    finally
    {
      os.close();
    }
  }

  private class ReferencesHandler extends DefaultHandler
  {
    private PluginRef pluginRef;
    private ClassRef classRef;

    public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException
    {
      if (elementName.equals("references") || qName.equals("references"))
      {
        setName(attributes.getValue("name"));
        setContactInfo(attributes.getValue("contactInfo"));
        setRefBuildId(attributes.getValue("refBuildId"));
        setIncludes(attributes.getValue("includes"));
        setExcludes(attributes.getValue("excludes"));
        setIncludePlugins(attributes.getValue("includePlugins"));
        setExcludePlugins(attributes.getValue("excludePlugins"));
      }
      else if (elementName.equals("plugin") || qName.equals("plugin"))
      {
        pluginRef = new PluginRef();
        pluginRef.setId(attributes.getValue("id"));
        addPluginRef(pluginRef);
      }
      else if (elementName.equals("class") || qName.equals("class"))
      {
        classRef = new ClassRef();
        classRef.setName(attributes.getValue("name"));
        classRef.setRefCount(Integer.parseInt(attributes.getValue("ref")));
        classRef.setSubclassCount(Integer.parseInt(attributes.getValue("subclass")));
        classRef.setImplementCount(Integer.parseInt(attributes.getValue("impl")));
        classRef.setInstantiateCount(Integer.parseInt(attributes.getValue("instantiate")));
        pluginRef.addClassRef(classRef);
      }
      else if (elementName.equals("method") || qName.equals("method"))
      {
        MethodRef methodRef = new MethodRef();
        methodRef.setName(attributes.getValue("name"));
        methodRef.setDescriptor(attributes.getValue("desc"));
        methodRef.setRefCount(Integer.parseInt(attributes.getValue("ref")));
        classRef.addMethodRef(methodRef);
      }
      else if (elementName.equals("field") || qName.equals("field"))
      {
        FieldRef fieldRef = new FieldRef();
        fieldRef.setName(attributes.getValue("name"));
        fieldRef.setDescriptor(attributes.getValue("desc"));
        fieldRef.setRefCount(Integer.parseInt(attributes.getValue("ref")));
        classRef.addFieldRef(fieldRef);
      }
    }

    public void endElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException
    {
      if (elementName.equals("plugin") || qName.equals("plugin"))
      {
        pluginRef = null;
      }
      else if (elementName.equals("class") || qName.equals("class"))
      {
        classRef = null;
      }
    }
  }
}