/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ILocationVisitor;
import org.eclipse.wtp.releng.tools.component.internal.ComponentXML;

public class ComponentXMLVisitor implements ILocationVisitor
{
  private Map compXMLs;

  public ComponentXMLVisitor()
  {
    this.compXMLs = new HashMap();
  }

  public Collection getCompXMLs()
  {
    return new ArrayList(compXMLs.values());
  }

  public boolean accept(ILocation location)
  {
    String locationName = location.getName();
    if (locationName.endsWith("component.xml"))
    {
      try
      {
        ComponentXML compXML = new ComponentXML();
        compXML.setLocation(location);
        compXML.load();
        String name = compXML.getName();
        if (!compXMLs.containsKey(name))
          compXMLs.put(name, compXML);
      }
      catch (Throwable e)
      {
      }
    }
    return true;
  }
}