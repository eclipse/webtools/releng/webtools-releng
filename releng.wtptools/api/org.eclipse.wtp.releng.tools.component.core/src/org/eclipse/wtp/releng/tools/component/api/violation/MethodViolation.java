/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api.violation;

public class MethodViolation extends ViolationContainer
{
  protected String getViolationName()
  {
    return "method";
  }

  public String getName()
  {
    String name = super.getName();
    StringBuffer sb = new StringBuffer(name);
    int index = name.indexOf('<');
    while (index != -1)
    {
      sb.deleteCharAt(index);
      sb.insert(index, new char[] {'&', 'l', 't', ';'}, 0, 4);
      index = sb.toString().indexOf('<');
    }
    return sb.toString();
  }

  public void setName(String name)
  {
    StringBuffer sb = new StringBuffer(name);
    int index = name.indexOf("&lt;");
    while (index != -1)
    {
      sb.delete(index, index + 4);
      sb.insert(index, '<');
      index = sb.toString().indexOf("&lt;");
    }
    super.setName(sb.toString());
  }
}