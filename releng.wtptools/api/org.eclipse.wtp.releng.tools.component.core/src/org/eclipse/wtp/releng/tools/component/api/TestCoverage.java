/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.wtp.releng.tools.component.internal.ComponentObject;

public class TestCoverage extends ComponentObject
{
  private List tests;

  public List getTests()
  {
    List copy = new ArrayList();
    if (tests != null)
      copy.addAll(tests);
    return copy;
  }

  public void addTest(String test)
  {
    if (tests == null)
      tests = new ArrayList();
    tests.add(test);
  }

  public void addTests(List tests)
  {
    if (this.tests == null)
      this.tests = new ArrayList(tests.size());
    this.tests.addAll(tests);
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<test-coverage>");
    for (Iterator it = getTests().iterator(); it.hasNext();)
    {
      sb.append("<test");
      sb.append(toAttribute("name", it.next().toString()));
      sb.append("/>");
    }
    sb.append("</test-coverage>");
    return sb.toString();
  }
}