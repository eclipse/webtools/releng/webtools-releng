/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.adopters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.eclipse.wtp.releng.tools.component.IFileLocation;
import org.eclipse.wtp.releng.tools.component.IJavaVisitor;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ILocationChildrenIterator;
import org.eclipse.wtp.releng.tools.component.ILocationVisitor;
import org.eclipse.wtp.releng.tools.component.IZipLocation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class PDESourceVisitor implements ILocationVisitor
{
  private IJavaVisitor javaVisitor;
  private List srcs;
  private Collection excludes;

  public PDESourceVisitor()
  {
    this.javaVisitor = null;
    this.srcs = new ArrayList();
  }

  public void setJavaVisitor(IJavaVisitor javaVisitor)
  {
    this.javaVisitor = javaVisitor;
  }

  public void setExcludes(Collection excludes)
  {
    this.excludes = excludes;
  }

  public boolean accept(ILocation location)
  {
    String locationName = location.getName();
    String absPath = location.getAbsolutePath();
    if (javaVisitor == null && (locationName.endsWith("plugin.xml") || locationName.endsWith("fragment.xml")))
    {
      try
      {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(location.getInputStream());
        Element root = doc.getDocumentElement();
        NodeList exts = root.getElementsByTagName("extension");
        for (int i = 0; i < exts.getLength(); i++)
        {
          Element ext = (Element)exts.item(i);
          if ("org.eclipse.pde.core.source".equals(ext.getAttribute("point")))
          {
            NodeList locs = ext.getElementsByTagName("location");
            for (int j = 0; j < locs.getLength(); j++)
            {
              Element loc = (Element)locs.item(j);
              String path = loc.getAttribute("path");
              if (path != null && path.length() > 0)
              {
                StringBuffer sb = new StringBuffer();
                if (locationName.endsWith("plugin.xml"))
                  //sb.append(locationName.substring(0, locationName.length() - "plugin.xml".length() - 1));
                  sb.append(absPath.substring(0, absPath.length() - "plugin.xml".length() - 1));
                else
                  //sb.append(locationName.substring(0, locationName.length() - "fragment.xml".length() - 1));
                  sb.append(absPath.substring(0, absPath.length() - "fragment.xml".length() - 1));
                sb.append("/");
                sb.append(path);
                srcs.add(sb.toString());
              }
            }
          }
        }
      }
      catch (Throwable e)
      {
      }
    }
    else if (javaVisitor != null && locationName.endsWith("src.zip"))
    {
      //String pluginId = getPluginId(locationName);
      String pluginId = getPluginId(absPath);
      if (pluginId != null && (excludes == null || !excludes.contains(pluginId)))
      {
        ILocationChildrenIterator it = location.childIterator();
        for (ILocation child = it.next(); child != null; child = it.next())
        {
          if (child.getName().endsWith(".java"))
          {
            javaVisitor.visit(pluginId, child);
          }
        }
      }
    }
    if (location instanceof IZipLocation)
      return true;
    else if ((location instanceof IFileLocation) && ((IFileLocation)location).getFile().isDirectory())
      return true;
    else
      return false;
  }

  private String getPluginId(String name)
  {
    for (Iterator it = srcs.iterator(); it.hasNext();)
    {
      String src = (String)it.next();
      if (name.startsWith(src))
      {
        int startIndex = src.length() + 1;
        int endIndex = name.indexOf('/', startIndex);
        if (endIndex == -1)
          endIndex = name.indexOf('\\', startIndex);
        if (endIndex != -1)
        {
          String id = name.substring(startIndex, endIndex);
          int versionIndex = id.indexOf('_');
          if (versionIndex != -1)
            id = id.substring(0, versionIndex);
          return id;
        }
      }
    }
    return null;
  }
}