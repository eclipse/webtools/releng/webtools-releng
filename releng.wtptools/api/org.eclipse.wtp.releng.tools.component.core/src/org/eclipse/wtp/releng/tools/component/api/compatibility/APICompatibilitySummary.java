/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api.compatibility;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.eclipse.wtp.releng.tools.component.IFileLocation;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.internal.ComponentEntry;
import org.eclipse.wtp.releng.tools.component.internal.ComponentSummary;

public class APICompatibilitySummary extends ComponentSummary
{
  private static final String ROOT_TAG_NAME = "api-compatibility-summary";

  public void add(APICompatibility apiCompatibility)
  {
    APICompatibilityEntry entry = new APICompatibilityEntry();
    entry.setCompName(apiCompatibility.getName());
    entry.setNewAPICount(apiCompatibility.getNewAPIs().size());
    entry.setRemovedAPICount(apiCompatibility.getRemovedAPIs().size());
    String ref = apiCompatibility.getLocation().getAbsolutePath();
    entry.setRef(ref);
    add(entry);
  }

  public void saveAsHTML(ILocation html) throws TransformerConfigurationException, TransformerException, IOException
  {
    String user_dir = "user.dir";
    String currUserDir = System.getProperty(user_dir);
    System.setProperty(user_dir, ((IFileLocation)html).getFile().getParentFile().getAbsolutePath());
    TransformerFactory factory = TransformerFactory.newInstance();
    Transformer transformer = factory.newTransformer(new StreamSource(ClassLoader.getSystemResourceAsStream("org/eclipse/wtp/releng/tools/component/xsl/api-compatibility-summary.xsl")));
    transformer.transform(new StreamSource(new ByteArrayInputStream(getBytes(html, ROOT_TAG_NAME))), new StreamResult(new FileOutputStream(new File(html.getAbsolutePath()))));
    System.setProperty(user_dir, currUserDir);
  }

  public void save(ILocation location) throws IOException
  {
    save(location, ROOT_TAG_NAME);
  }

  private class APICompatibilityEntry extends ComponentEntry
  {
    private int newAPICount;
    private int removedAPICount;

    public APICompatibilityEntry()
    {
      newAPICount = 0;
      removedAPICount = 0;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append("<api-compatibility ");
      sb.append(toAttribute("name", getCompName()));
      sb.append(toAttribute("new-api-count", String.valueOf(newAPICount)));
      sb.append(toAttribute("removed-api-count", String.valueOf(removedAPICount)));
      sb.append(toAttribute("ref", getRef()));
      sb.append("/>");
      return sb.toString();
    }

    /**
     * @return Returns the newAPICount.
     */
    public int getNewAPICount()
    {
      return newAPICount;
    }

    /**
     * @param newAPICount The newAPICount to set.
     */
    public void setNewAPICount(int newAPICount)
    {
      this.newAPICount = newAPICount;
    }

    /**
     * @return Returns the removedAPICount.
     */
    public int getRemovedAPICount()
    {
      return removedAPICount;
    }

    /**
     * @param removedAPICount The removedAPICount to set.
     */
    public void setRemovedAPICount(int removedAPICount)
    {
      this.removedAPICount = removedAPICount;
    }
  }
}