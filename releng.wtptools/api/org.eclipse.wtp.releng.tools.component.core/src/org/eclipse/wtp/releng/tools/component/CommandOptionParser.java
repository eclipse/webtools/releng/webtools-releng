/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

/**
 * This is a helper class used to parse the command line options into a
 * map where each option can be returned or all options.
 */
public class CommandOptionParser {
  
  // Delimiter string
  private static final String delimiter = "-"; //$NON-NLS-1$
  
  // Map of options
  private Map options;

  /**
   * Default constructor
   * @param args String[]
   */
  public CommandOptionParser(String[] args) {
    parse(args);
  }

  /**
   * @return Map of all command line options and values
   */
  public Map getOptions() {
    return options;
  }

  /**
   * Return the command line values for the specified command line key.
   * 
   * @param key String
   * @return Collection of values
   */
  public Collection getOption(String key) {
    return (Collection)options.get(key);
  }

  /**
   * Get command line value as a string for specified option key.
   * 
   * @param key String
   * @return String command line value
   */
  public String getOptionAsString(String key) {
    Collection c = getOption(key);
    if (c.isEmpty())
      return null;
    return (String) c.iterator().next();
  }

  /**
   * Parse the string array of command line options and values into a map.
   * 
   * @param args String[]
   */
  private void parse(String[] args) {
    options = new HashMap();
    String option = null;
    for (int i = 0; i < args.length; i++) {
      if (args[i] != null) {
        if (args[i].startsWith(delimiter)) {
          option = args[i].substring(1);
          options.put(option, new ArrayList(1));
        } else if (option != null)
          ((List)options.get(option)).add(args[i]);
      }
    }
  }
}
