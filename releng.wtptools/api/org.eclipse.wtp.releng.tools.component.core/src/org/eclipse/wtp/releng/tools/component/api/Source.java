/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Source
{
  private String name;
  private Map classUses;

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    this.name = name;
  }

  /**
   * @return Returns the classUses.
   */
  public Collection getClassUses()
  {
    if (classUses == null)
      classUses = new HashMap(1);
    return classUses.values();
  }

  public ClassUse getClassUse(String name)
  {
    if (classUses == null)
      return null;
    else
      return (ClassUse)classUses.get(name);
  }

  public void addClassUse(ClassUse classUse)
  {
    if (classUses == null)
      classUses = new HashMap(1);
    classUses.put(classUse.getName(), classUse);
  }

  public Object removeClassUse(ClassUse classUse)
  {
    if (classUses == null)
      return null;
    else
      return classUses.remove(classUse.getName());
  }
}