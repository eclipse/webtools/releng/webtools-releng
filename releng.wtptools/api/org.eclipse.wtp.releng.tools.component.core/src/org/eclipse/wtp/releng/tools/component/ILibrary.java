/*******************************************************************************
 * Copyright (c) 2002, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component;

import java.util.Map;

public interface ILibrary
{
  public static final String EXT_CLASS = "class"; //$NON-NLS-1$
  /**
   * Answers a mapping of (qualified) type names to <code>Type</code> objects
   * which are found in this library.
   * 
   * @return Map a mapping of type names to <code>Type</code> objects.
   */
  public Map getTypes();

  public void resetTypes();

  public void accept(IClazzVisitor visitor);
}
