/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.adopters;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.wtp.releng.tools.component.internal.ComponentXML;
import org.eclipse.wtp.releng.tools.component.internal.Package;

public class Java2APIVisitor extends ASTVisitor
{
  private ComponentXML compXML;
  private String packageName;
  private boolean defaultExclusive;

  public Java2APIVisitor(ComponentXML compXML)
  {
    this(compXML, true);
  }

  public Java2APIVisitor(ComponentXML compXML, boolean defaultExclusive)
  {
    super(false);
    this.compXML = compXML;
    this.packageName = null;
    this.defaultExclusive = defaultExclusive;
  }

  public boolean visit(PackageDeclaration node)
  {
    packageName = node.getName().getFullyQualifiedName();
    return true;
  }

  public boolean visit(TypeDeclaration node)
  {
    if (packageName != null)
    {
      Package pkg = compXML.getPackage(packageName);
      if (pkg == null)
      {
        pkg = new Package();
        pkg.setName(packageName);
        pkg.setApi(Boolean.TRUE);
        if (!defaultExclusive)
          pkg.setExclusive(Boolean.FALSE);
        compXML.addPackage(pkg);
      }
    }
    return false;
  }
}