/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api.violation;

public class ParamViolation extends Violation
{
  private String type;

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  protected String getViolationName()
  {
    return "param";
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<");
    sb.append(getViolationName());
    sb.append(toAttribute("name", getName()));
    sb.append(toAttribute("type", getType()));
    sb.append("/>");
    return sb.toString();
  }
}