<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<script language="javascript">
				function showComponentsOnly()
				{
					toggleDiv("component::", false);
					toggleDiv("class::", false);
				}
				function showClassesOnly()
				{
					toggleDiv("component::", true);
					toggleDiv("class::", false);
				}
				function showAll()
				{
					toggleDiv("component::", true);
					toggleDiv("class::", true);
				}
				function toggleDiv(prefix, show)
				{
					var divs = document.getElementsByTagName("div");
					for (var i = 0; i &lt; divs.length; i++)
					{
						if (divs[i].id.indexOf(prefix) == 0)
						{
							var img = document.getElementById(divs[i].id + "::twist");
							if (show)
							{
								if (img != null)
								{
									img.src = "twistopened.gif";
								}
								divs[i].style.display = "";
							}
							else
							{
								if (img != null)
								{
									img.src = "twistclosed.gif";
								}
								divs[i].style.display = "none";
							}
						}
					}
				}
				function toggle(id)
				{
					var div = document.getElementById(id);
					var img = document.getElementById(id + "::twist");
					if (div.style.display == "")
					{
						img.src = "twistclosed.gif";
						div.style.display = "none";
					}
					else
					{
						img.src = "twistopened.gif";
						div.style.display = "";
					}
				}
			</script>
			<body>
				<table width="100%" cellspacing="5" cellpadding="2" border="0">
					<tbody>
						<tr>
							<td width="60%" align="left">
								<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">Eclipse WTP Component API Violation Report</font>
							</td>
							<td width="40%">
								<img width="120" hspace="50" height="86" align="middle" src="Idea.jpg"/>
							</td>
						</tr>
					</tbody>
				</table>
				<table WIDTH="100%" CELLPADDING="2" CELLSPACING="5" BORDER="0">
					<tr>
						<td BGCOLOR="#0080C0" COLSPAN="2" VALIGN="TOP" ALIGN="LEFT"><b><font face="Arial,Helvetica" color="#FFFFFF">Component API violation report</font></b></td>
					</tr>
				</table>
				<table border="0">
					<tr><td><a href="javascript:showComponentsOnly()">Show components only</a></td></tr>
					<tr><td><a href="javascript:showClassesOnly()">Show classes only</a></td></tr>
					<tr><td><a href="javascript:showAll()">Show all</a></td></tr>
					<tr><td>&#160;</td></tr>
				</table>
				<font face="Arial,Helvetica">
					<xsl:for-each select="component-summary/component">
						<xsl:sort select="@name"/>
						<xsl:variable name="report" select="document(@ref)/component-api-violation"/>
						<xsl:if test="$report/class">
							<xsl:apply-templates select="$report"/>
						</xsl:if>
					</xsl:for-each>
				</font>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="component-api-violation">
		<table border="0">
			<tr>
				<td>
					<a href="javascript:toggle('component::{@name}')"><img id="component::{@name}::twist" border="0" src="twistopened.gif"/></a>&#160;
					<xsl:value-of select="@name"/>&#160;
				</td>
			</tr>
			<tr>
				<td>
					<div id="component::{@name}">
						<table border="0">
							<xsl:for-each select="class">
								<xsl:sort select="@name"/>
								<xsl:apply-templates select="."/>
							</xsl:for-each>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template match="class">
		<tr>
			<td>
				<img border="0" src="space.gif"/>
				<a href="javascript:toggle('class::{@name}')"><img id="class::{@name}::twist" border="0" src="twistopened.gif"/></a>&#160;
				<xsl:value-of select="@name"/>&#160;
				<div id="class::{@name}">
					<ol>
						<xsl:if test="super">
							<li><b>Super class</b>&#160;<xsl:value-of select="super/@name"/> is not an API</li>
						</xsl:if>
						<xsl:for-each select="method">
							<xsl:sort select="@name"/>
							<li>
								<b>Method</b>&#160;<xsl:value-of select="@name"/>
								<ul>
									<xsl:for-each select="param">
										<li><b>Param type</b>&#160;<xsl:value-of select="@name"/> is not an API</li>
									</xsl:for-each>
									<xsl:if test="return">
										<li><b>Return type</b>&#160;<xsl:value-of select="return/@name"/> is not an API</li>
									</xsl:if>
									<xsl:for-each select="throw">
										<li><b>Throw type</b>&#160;<xsl:value-of select="@name"/> is not an API</li>
									</xsl:for-each>
								</ul>
							</li>
						</xsl:for-each>
						<xsl:for-each select="field">
							<xsl:sort select="@name"/>
							<li><b>Field</b>&#160;<xsl:value-of select="@name"/>: <xsl:value-of select="@type"/> is not an API</li>
						</xsl:for-each>
					</ol>
				</div>
			</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>