/*******************************************************************************
 * Copyright (c) 2002, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ILocationChildrenIterator;

/**
 * A <code>ZipEntryLocation</code> is a zip location that is contained inside
 * of a zip file.
 */
public class ZipEntryLocation extends AbstractZipLocation
{
  protected static final byte[] EMPTY_BYTE_ARRAY = new byte[0];
  protected ZipInputStreamIterator iterator;
  protected boolean exists;
  protected boolean checkedExists;

  public ZipEntryLocation(AbstractZipLocation parent, String name)
  {
    super(parent, name);
  }

  /*
   * @see com.example.location.AbstractZipLocation#getChildInputStream(java.lang.String)
   */
  protected InputStream getChildInputStream(String name)
  {
    if (iterator != null)
    {
      if (iterator.entry.getName().equals(name))
      {
        return new ByteArrayInputStream(iterator.getEntryBytes());
      }
    }
    return null;
  }

  /*
   * @see com.example.location.ILocation#getInputStream()
   */
  public InputStream getInputStream() throws IOException
  {
    AbstractZipLocation zipParent = (AbstractZipLocation)parent;
    InputStream inputStream = zipParent.getChildInputStream(name);
    if (inputStream != null)
    {
      return inputStream;
    }
    inputStream = parent.getInputStream();
    ZipInputStream parentStream = null;
    if (inputStream != null)
    {
      parentStream = getZipInputStream(inputStream);
    }
    try
    {
      ZipEntry entry = parentStream.getNextEntry();
      while (entry != null)
      {
        if (name.equals(entry.getName()))
        {
          return new ByteArrayInputStream(readAllBytes(parentStream));
        }
        entry = parentStream.getNextEntry();
      }
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    finally
    {
      if (parentStream != null)
      {
        try
        {
          parentStream.close();
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    return null;
  }

  /*
   * @see com.example.location.ILocation#hasChildren()
   */
  public boolean hasChildren()
  {
    return Location.isArchive(name);
  }

  /*
   * @see com.example.location.ILocation#childIterator()
   */
  public ILocationChildrenIterator childIterator()
  {
    InputStream in = null;
    try
    {
      in = getInputStream();
    }
    catch (IOException e)
    {
      iterator = null;
    }
    iterator = new ZipInputStreamIterator(getZipInputStream(in));
    return iterator;
  }
  /**
   * Implements the <code>ILocationChildrenIterator</code> to iterate over the
   * zip entries inside a <code>ZipInputStream</code>.
   */
  protected class ZipInputStreamIterator implements ILocationChildrenIterator
  {
    private ZipInputStream inputStream;
    public ZipEntry entry;

    public ZipInputStreamIterator(ZipInputStream inputStream)
    {
      this.inputStream = inputStream;
    }

    private void readEntry()
    {
      if (inputStream != null)
      {
        try
        {
          entry = inputStream.getNextEntry();
        }
        catch (IOException e)
        {
          e.printStackTrace();
          entry = null;
        }
        finally
        {
          if (entry == null && inputStream != null)
          {
            try
            {
              inputStream.close();
            }
            catch (IOException e)
            {
              e.printStackTrace();
            }
          }
        }
      }
      else
      {
        entry = null;
      }
    }

    public ILocation next()
    {
      readEntry();
      ILocation location = null;
      if (entry != null)
      {
        location = new ZipEntryLocation(ZipEntryLocation.this, entry.getName());
      }
      else
      {
        iterator = null;
      }
      return location;
    }

    public byte[] getEntryBytes()
    {
      if (entry != null)
      {
        try
        {
          return readAllBytes(inputStream);
        }
        catch (IOException e)
        {
          return EMPTY_BYTE_ARRAY;
        }
      }
      return EMPTY_BYTE_ARRAY;
    }
  }

  /*
   * @see com.example.location.ILocation#getAbsolutePath()
   */
  public String getAbsolutePath()
  {
    return parent.getAbsolutePath() + "/" + getName();
  }
}
