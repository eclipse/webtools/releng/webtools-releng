/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api.progress;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.eclipse.core.runtime.Platform;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.api.ClassAPI;
import org.eclipse.wtp.releng.tools.component.api.ComponentAPI;
import org.eclipse.wtp.releng.tools.component.api.FieldAPI;
import org.eclipse.wtp.releng.tools.component.api.MethodAPI;
import org.eclipse.wtp.releng.tools.component.api.PackageAPI;
import org.eclipse.wtp.releng.tools.component.internal.ComponentEntry;
import org.eclipse.wtp.releng.tools.component.internal.ComponentSummary;

public class JavadocCoverageSummary extends ComponentSummary
{
  private static final String ROOT_TAG_NAME = "component-api-javadoc-summary";

  public void add(ComponentAPI compAPI)
  {
    JavadocCoverageEntry entry = new JavadocCoverageEntry();
    int classCount = 0;
    int methodCount = 0;
    int fieldCount = 0;
    int classIncompleteJavadoc = 0;
    int methodIncompleteJavadoc = 0;
    int fieldIncompleteJavadoc = 0;
    entry.setCompName(compAPI.getName());
    for (Iterator it = compAPI.getPackageAPIs().iterator(); it.hasNext();)
    {
      for (Iterator classIt = ((PackageAPI)it.next()).getClassAPIs().iterator(); classIt.hasNext();)
      {
        ClassAPI classAPI = (ClassAPI)classIt.next();
        classCount++;
        if (classAPI.isSetJavadocCoverage())
          classIncompleteJavadoc++;
        for (Iterator methodIt = classAPI.getMethodAPIs().iterator(); methodIt.hasNext();)
        {
          MethodAPI methodAPI = (MethodAPI)methodIt.next();
          methodCount++;
          if (methodAPI.isSetJavadocCoverage())
            methodIncompleteJavadoc++;
        }
        for (Iterator fieldIt = classAPI.getFieldAPIs().iterator(); fieldIt.hasNext();)
        {
          FieldAPI fieldAPI = (FieldAPI)fieldIt.next();
          fieldCount++;
          if (fieldAPI.isSetJavadocCoverage())
            fieldIncompleteJavadoc++;
        }
      }
    }
    entry.setClassCount(classCount);
    entry.setClassJavadocCount(classCount - classIncompleteJavadoc);
    entry.setMethodCount(methodCount);
    entry.setMethodJavadocCount(methodCount - methodIncompleteJavadoc);
    entry.setFieldCount(fieldCount);
    entry.setFieldJavadocCount(fieldCount - fieldIncompleteJavadoc);
    String ref = compAPI.getLocation().getAbsolutePath();
    int i = ref.lastIndexOf('/');
    if (i != -1)
      ref = ref.substring(0, i + 1);
    entry.setRef(ref + "component-api-javadoc.html");
    add(entry);
  }

  protected void saveAsHTML(ILocation html, String xsl, String rootTagName) throws TransformerConfigurationException, TransformerException, IOException
  {
    TransformerFactory factory = TransformerFactory.newInstance();
    Transformer transformer = factory.newTransformer(new StreamSource(Platform.getBundle("org.eclipse.wtp.releng.tools.component.core").getResource(xsl).openStream()));
    transformer.transform(new StreamSource(new ByteArrayInputStream(getBytes(html, rootTagName))), new StreamResult(new FileOutputStream(new File(html.getAbsolutePath()))));
  }

  public void saveAsHTML(String xsl, ILocation html) throws TransformerConfigurationException, TransformerException, IOException
  {
    saveAsHTML(html, xsl, ROOT_TAG_NAME);
  }

  public void save(ILocation location) throws IOException
  {
    save(location, ROOT_TAG_NAME);
  }

  private class JavadocCoverageEntry extends ComponentEntry
  {
    private int classCount;
    private int methodCount;
    private int fieldCount;
    private int classJavadocCount;
    private int methodJavadocCount;
    private int fieldJavadocCount;

    public JavadocCoverageEntry()
    {
      classCount = 0;
      methodCount = 0;
      fieldCount = 0;
      classJavadocCount = 0;
      methodJavadocCount = 0;
      fieldJavadocCount = 0;
    }

    public int getClassCount()
    {
      return classCount;
    }

    public void setClassCount(int classCount)
    {
      this.classCount = classCount;
    }

    public int getClassJavadocCount()
    {
      return classJavadocCount;
    }

    public void setClassJavadocCount(int classJavadocCount)
    {
      this.classJavadocCount = classJavadocCount;
    }

    public int getFieldCount()
    {
      return fieldCount;
    }

    public void setFieldCount(int fieldCount)
    {
      this.fieldCount = fieldCount;
    }

    public int getFieldJavadocCount()
    {
      return fieldJavadocCount;
    }

    public void setFieldJavadocCount(int fieldJavadocCount)
    {
      this.fieldJavadocCount = fieldJavadocCount;
    }

    public int getMethodCount()
    {
      return methodCount;
    }

    public void setMethodCount(int methodCount)
    {
      this.methodCount = methodCount;
    }

    public int getMethodJavadocCount()
    {
      return methodJavadocCount;
    }

    public void setMethodJavadocCount(int methodJavadocCount)
    {
      this.methodJavadocCount = methodJavadocCount;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append("<component-api-javadoc ");
      sb.append(toAttribute("name", getCompName()));
      sb.append(toAttribute("class-api-count", String.valueOf(classCount)));
      sb.append(toAttribute("class-javadoc-count", String.valueOf(classJavadocCount)));
      sb.append(toAttribute("method-api-count", String.valueOf(methodCount)));
      sb.append(toAttribute("method-javadoc-count", String.valueOf(methodJavadocCount)));
      sb.append(toAttribute("field-api-count", String.valueOf(fieldCount)));
      sb.append(toAttribute("field-javadoc-count", String.valueOf(fieldJavadocCount)));
      sb.append(toAttribute("ref", getRef()));
      sb.append("/>");
      return sb.toString();
    }
  }
}