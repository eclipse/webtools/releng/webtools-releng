/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.util.Collection;
import java.util.Iterator;

public abstract class ComponentObject
{
  protected final boolean checkAccess(int access, int modifier)
  {
    return ((access & modifier) == modifier);
  }

  protected final String toAttribute(String key, String value)
  {
    StringBuffer sb = new StringBuffer();
    if (key != null && value != null)
    {
      sb.append(" ");
      sb.append(key);
      sb.append("=\"");
      sb.append(value);
      sb.append("\"");
    }
    return sb.toString();
  }

  protected final String toAttribute(String key, Collection values, String delimiter)
  {
    StringBuffer sb = new StringBuffer();
    if (key != null && values != null && values.size() > 0 && delimiter != null)
    {
      sb.append(" ");
      sb.append(key);
      sb.append("=\"");
      for (Iterator it = values.iterator(); it.hasNext();)
      {
        sb.append(it.next().toString());
        if (it.hasNext())
        {
          sb.append(delimiter);
        }
      }
      sb.append("\"");
    }
    return sb.toString();
  }
}