<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:component="http://www.eclipse.org/component"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="func">
	<xsl:output method="text" encoding="UTF-8" />

	<xsl:template match="/">
		<xsl:if test="/component-api-tc-summary">
api-count=<xsl:value-of select="component:tc-apicount(1, 0)"/>
test-coverage-count=<xsl:value-of select="component:tc-testcount(1, 0)"/>
		</xsl:if>
		<xsl:if test="/component-api-javadoc-summary">
method-api-count=<xsl:value-of select="component:javadoc-method-api(1, 0)"/>
class-javadoc-count=<xsl:value-of select="component:javadoc-class(1, 0)"/>
method-javadoc-count=<xsl:value-of select="component:javadoc-method(1, 0)"/>
		</xsl:if>
	</xsl:template>

	<func:function name="component:tc-apicount">
		<xsl:param name="i"/>
		<xsl:param name="total"/>
		<xsl:choose>
			<xsl:when test="$i &lt;= count(/component-api-tc-summary/component-api-tc)">
				<func:result select="component:tc-apicount(($i + 1), ($total + /component-api-tc-summary/component-api-tc[$i]/@api-count))"/>
			</xsl:when>
			<xsl:otherwise>
				<func:result select="$total"/>
			</xsl:otherwise>
		</xsl:choose>
	</func:function>

	<func:function name="component:tc-testcount">
		<xsl:param name="i"/>
		<xsl:param name="total"/>
		<xsl:choose>
			<xsl:when test="$i &lt;= count(/component-api-tc-summary/component-api-tc)">
				<func:result select="component:tc-testcount(($i + 1), ($total + /component-api-tc-summary/component-api-tc[$i]/@test-coverage-count))"/>
			</xsl:when>
			<xsl:otherwise>
				<func:result select="$total"/>
			</xsl:otherwise>
		</xsl:choose>
	</func:function>

	<func:function name="component:javadoc-method-api">
		<xsl:param name="i"/>
		<xsl:param name="total"/>
		<xsl:choose>
			<xsl:when test="$i &lt;= count(/component-api-javadoc-summary/component-api-javadoc)">
				<func:result select="component:javadoc-method-api(($i + 1), ($total + /component-api-javadoc-summary/component-api-javadoc[$i]/@method-api-count))"/>
			</xsl:when>
			<xsl:otherwise>
				<func:result select="$total"/>
			</xsl:otherwise>
		</xsl:choose>
	</func:function>

	<func:function name="component:javadoc-class">
		<xsl:param name="i"/>
		<xsl:param name="total"/>
		<xsl:choose>
			<xsl:when test="$i &lt;= count(/component-api-javadoc-summary/component-api-javadoc)">
				<func:result select="component:javadoc-class(($i + 1), ($total + /component-api-javadoc-summary/component-api-javadoc[$i]/@class-javadoc-count))"/>
			</xsl:when>
			<xsl:otherwise>
				<func:result select="$total"/>
			</xsl:otherwise>
		</xsl:choose>
	</func:function>

	<func:function name="component:javadoc-method">
		<xsl:param name="i"/>
		<xsl:param name="total"/>
		<xsl:choose>
			<xsl:when test="$i &lt;= count(/component-api-javadoc-summary/component-api-javadoc)">
				<func:result select="component:javadoc-method(($i + 1), ($total + /component-api-javadoc-summary/component-api-javadoc[$i]/@method-javadoc-count))"/>
			</xsl:when>
			<xsl:otherwise>
				<func:result select="$total"/>
			</xsl:otherwise>
		</xsl:choose>
	</func:function>

</xsl:stylesheet>