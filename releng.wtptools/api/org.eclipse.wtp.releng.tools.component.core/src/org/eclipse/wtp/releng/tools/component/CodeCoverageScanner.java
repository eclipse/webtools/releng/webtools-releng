/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.eclipse.wtp.releng.tools.component.api.API2ComponentAPI;
import org.eclipse.wtp.releng.tools.component.api.ClassAPI;
import org.eclipse.wtp.releng.tools.component.api.ComponentAPI;
import org.eclipse.wtp.releng.tools.component.api.MethodAPI;
import org.eclipse.wtp.releng.tools.component.api.PackageAPI;
import org.eclipse.wtp.releng.tools.component.api.TestCoverage;
import org.eclipse.wtp.releng.tools.component.images.ImagesUtil;
import org.eclipse.wtp.releng.tools.component.internal.FileLocation;
import org.eclipse.wtp.releng.tools.component.internal.Location;
import org.eclipse.wtp.releng.tools.component.xsl.XSLUtil;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class CodeCoverageScanner implements ILocationVisitor
{
  private String api;
  private Collection src;
  private String trcxml;
  private String outputDir;
  private boolean skipAPIGen;
  private Collection includes;
  private Collection excludes;
  private boolean includeAllTC;
  private boolean html;
  private String title;
  private String xsl;

  public String getOutputDir()
  {
    return outputDir;
  }

  public void setOutputDir(String outputDir)
  {
    this.outputDir = addTrailingSeperator(outputDir);
  }

  public String getApi()
  {
    return api;
  }

  public void setApi(String api)
  {
    this.api = api;
  }

  public Collection getSrc()
  {
    return src;
  }

  public void setSrc(Collection src)
  {
    this.src = src;
  }

  public String getTRCXML()
  {
    return trcxml;
  }

  public void setTRCXML(String trcxml)
  {
    this.trcxml = trcxml;
  }

  public boolean isSkipAPIGen()
  {
    return skipAPIGen;
  }

  public void setSkipAPIGen(boolean skipAPIGen)
  {
    this.skipAPIGen = skipAPIGen;
  }

  public Collection getIncludes()
  {
    return includes;
  }

  public void setIncludes(Collection includes)
  {
    this.includes = includes;
  }

  public Collection getExcludes()
  {
    return excludes;
  }

  public void setExcludes(Collection excludes)
  {
    this.excludes = excludes;
  }

  public boolean isIncludeAllTC()
  {
    return includeAllTC;
  }

  public void setIncludeAllTC(boolean includeAllTC)
  {
    this.includeAllTC = includeAllTC;
  }

  public boolean isHtml()
  {
    return html;
  }

  public void setHtml(boolean html)
  {
    this.html = html;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getXsl()
  {
    return xsl;
  }

  public void setXsl(String xsl)
  {
    this.xsl = xsl;
  }

  private API2ComponentAPI api2CompXML;

  public void execute()
  {
    // Generate api-info.xml
    api2CompXML = new API2ComponentAPI();
    api2CompXML.setApi(api);
    api2CompXML.setSrc(src);
    api2CompXML.setOutputDir(outputDir);
    api2CompXML.setIncludes(includes);
    api2CompXML.setExcludes(excludes);
    api2CompXML.setReadInterface(true);
    api2CompXML.setSkipAPIGen(skipAPIGen);
    api2CompXML.execute();

    // Visit all .trcxml files that uses APIs
    Location.createLocation(new File(trcxml)).accept(this);

    // Generate HTML
    if (isHtml())
    {
      ImagesUtil.copyAll(outputDir);
      genHTML();
    }
  }

  private void genHTML()
  {
    final StringBuffer summary = new StringBuffer();
    summary.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    summary.append("<root>");
    File f = new File(outputDir);
    f.mkdirs();
    ILocation outputLoc = Location.createLocation(new File(outputDir));
    outputLoc.accept(new ILocationVisitor()
    {
      public boolean accept(ILocation location)
      {
        if (location.getName().endsWith("api-info.xml"))
        {
          try
          {
            XSLUtil.transform
            (
              xsl != null && xsl.length() > 0 ? Location.createLocation(new File(xsl)).getInputStream() : ClassLoader.getSystemResourceAsStream("org/eclipse/wtp/releng/tools/component/xsl/api-tc.xsl"),
              location.getInputStream(),
              new FileOutputStream(((FileLocation)location.createSibling("api-tc.html")).getFile())
            );
            summary.append("<api-info file=\"");
            summary.append(location.getAbsolutePath().substring(outputDir.length()));
            summary.append("\"/>");
          }
          catch (Throwable t)
          {
            t.printStackTrace();
          }
        }
        return true;
      }
    });
    summary.append("</root>");
    try
    {
      if (isIncludeAllTC())
        System.setProperty("includeAllTC", "true");
      if (title != null)
        System.setProperty("title", title);
      XSLUtil.transform
      (
        ClassLoader.getSystemResourceAsStream("org/eclipse/wtp/releng/tools/component/xsl/api-tc-summary.xsl"),
        new ByteArrayInputStream(summary.toString().getBytes()),
        new FileOutputStream(new File(outputDir + "/api-tc-summary.html")),
        outputDir
      );
    }
    catch (Throwable e)
    {
      e.printStackTrace();
    }
  }

  public boolean accept(ILocation location)
  {
    String locationName = location.getName();
    if (locationName.endsWith(".trcxml"))
      processTRCXML(location);
    return true;
  }

  private void processTRCXML(ILocation location)
  {
    try
    {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      factory.setNamespaceAware(false);
      factory.setValidating(false);
      SAXParser parser = factory.newSAXParser();
      TRCXMLHandler trcxmlHandler = new TRCXMLHandler();
      parser.parse(new InputSource(new BufferedInputStream(location.getInputStream())), trcxmlHandler);
      final String testcaseName = getTRCXMLTestcaseName(location);
      final Map classMethodName2Count = trcxmlHandler.getClassMethodName2Count();
      ILocation outputLoc = Location.createLocation(new File(outputDir));
      outputLoc.accept(new ILocationVisitor()
      {
        public boolean accept(ILocation location)
        {
          if (location.getName().endsWith("api-info.xml"))
          {
            try
            {
              boolean dirty = false;
              ComponentAPI compAPI = new ComponentAPI();
              compAPI.setLocation(location);
              compAPI.load();
              for (Iterator it = compAPI.getPackageAPIs().iterator(); it.hasNext();)
              {
                PackageAPI pkgAPI = (PackageAPI)it.next();
                for (Iterator it2 = pkgAPI.getClassAPIs().iterator(); it2.hasNext();)
                {
                  ClassAPI classAPI = (ClassAPI)it2.next();
                  StringBuffer qualifiedClassName = new StringBuffer();
                  qualifiedClassName.append(pkgAPI.getName());
                  qualifiedClassName.append(".");
                  qualifiedClassName.append(classAPI.getName());
                  for (Iterator it3 = classAPI.getMethodAPIs().iterator(); it3.hasNext();)
                  {
                    MethodAPI methodAPI = (MethodAPI)it3.next();
                    String methodName = methodAPI.getName().replace('>', '-');
                    if (methodName.startsWith("&lt;"))
                      methodName = "-" + methodName.substring(4);
                    String signature = methodAPI.getDescriptor();
                    String classMethodName = getClassMethodName(qualifiedClassName.toString(), methodName, signature);
                    String countString = (String)classMethodName2Count.get(classMethodName);
                    // TODO
                    if (countString != null)
                    {
                      TestCoverage tc = methodAPI.getTestCoverage();
                      tc.addTest(testcaseName);
                      dirty = true;
                    }
                    else
                    {
                      List implClasses = (List)api2CompXML.getImplClasses(qualifiedClassName.toString());
                      for (Iterator it4 = implClasses.iterator(); it4.hasNext();)
                      {
                        classMethodName = getClassMethodName((String)it4.next(), methodName, signature);
                        countString = (String)classMethodName2Count.get(classMethodName);
                        // TODO
                        if (countString != null)
                        {
                          TestCoverage tc = methodAPI.getTestCoverage();
                          tc.addTest(testcaseName);
                          dirty = true;
                        }
                      }
                    }
                  }
                }
              }
              if (dirty)
              {
                compAPI.save();
              }
            }
            catch (IOException ioe)
            {
              ioe.printStackTrace();
            }
          }
          return true;
        }
      });
    }
    catch (ParserConfigurationException pce)
    {
      pce.printStackTrace();
    }
    catch (SAXException saxe)
    {
      saxe.printStackTrace();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  private String getClassMethodName(String qualifiedClasName, String methodName, String signature)
  {
    StringBuffer classMethodName = new StringBuffer();
    classMethodName.append(qualifiedClasName);
    classMethodName.append("#");
    classMethodName.append(methodName);
    classMethodName.append("#");
    classMethodName.append(signature);
    return classMethodName.toString();
  }

  private String getTRCXMLTestcaseName(ILocation location)
  {
    String locationName = location.getName();
    int i = locationName.lastIndexOf('-');
    return locationName.substring(i + 1, locationName.length() - ".trcxml".length());
  }

  protected String addTrailingSeperator(String s)
  {
    if (s != null && !s.endsWith("/") && !s.endsWith("\\"))
    {
      StringBuffer sb = new StringBuffer(s);
      sb.append('/');
      return sb.toString();
    }
    else
    {
      return s;
    }
  }

  private class TRCXMLHandler extends DefaultHandler
  {
	private static final String DOT = "."; //$NON-NLS-1$ 
	private static final String FORWARD_SLASH = "/"; //$NON-NLS-1$
    private final String CLASS_DEF = "classDef";
    private final String METHOD_DEF = "methodDef";
    private final String METHOD_COUNT = "methodCount";

    private Map classMethodName2count = new HashMap();
    private Map methodId2Name = new HashMap();
    private Map methodId2ClassId = new HashMap();
    private Map classId2Name = new HashMap();

    public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException
    {
      if (elementName.equals(CLASS_DEF) || qName.equals(CLASS_DEF))
      {
        String id = attributes.getValue("classId");
        String name = attributes.getValue("name");
        classId2Name.put(id, name.replaceAll(FORWARD_SLASH, DOT)); 
      }
      else if (elementName.equals(METHOD_DEF) || qName.equals(METHOD_DEF))
      {
        String id = attributes.getValue("methodId");
        String name = attributes.getValue("name");
        String signature = attributes.getValue("signature");
        String classIdRef = attributes.getValue("classIdRef");
        StringBuffer sb = new StringBuffer();
        sb.append(name);
        sb.append("#");
        sb.append(signature);
        methodId2Name.put(id, sb.toString());
        methodId2ClassId.put(id, classIdRef);
      }
      else if (elementName.equals(METHOD_COUNT) || qName.equals(METHOD_COUNT))
      {
        String methodIdRef = attributes.getValue("methodIdRef");
        String count = attributes.getValue("count");
        String methodName = (String)methodId2Name.get(methodIdRef);
        if (methodName != null)
        {
          String classId = (String)methodId2ClassId.get(methodIdRef);
          if (classId != null)
          {
            String className = (String)classId2Name.get(classId);
            if (className != null)
            {
              StringBuffer sb = new StringBuffer();
              sb.append(className);
              sb.append("#");
              sb.append(methodName);
              classMethodName2count.put(sb.toString(), count);
            }
          }
        }
      }
    }

    public Map getClassMethodName2Count()
    {
      return classMethodName2count;
    }
  }

  public static void main(String[] args)
  {
    CommandOptionParser optionParser = new CommandOptionParser(args);
    Map options = optionParser.getOptions();
    Collection api = (Collection)options.get("api");
    Collection src = (Collection)options.get("src");
    Collection trcxml = (Collection)options.get("trcxml");
    Collection outputDir = (Collection)options.get("outputDir");
    Collection skipAPIGen = (Collection)options.get("skipAPIGen");
    Collection includes = (Collection)options.get("includes");
    Collection excludes = (Collection)options.get("excludes");
    Collection includeAllTC = (Collection)options.get("includeAllTC");
    Collection html = (Collection)options.get("html");
    Collection title = (Collection)options.get("title");
    Collection xsl = (Collection)options.get("xsl");
    if (outputDir == null || outputDir.isEmpty())
    {
      printUsage();
      System.exit(-1);
    }
    if ((src == null || api == null || src.isEmpty() || api.isEmpty()) && skipAPIGen == null)
    {
      printUsage();
      System.exit(-1);
    }
    CodeCoverageScanner scanner = new CodeCoverageScanner();
    scanner.setApi((String)api.iterator().next());
    scanner.setSrc(src);
    scanner.setTRCXML((String)trcxml.iterator().next());
    scanner.setOutputDir((String)outputDir.iterator().next());
    scanner.setSkipAPIGen(skipAPIGen != null);
    scanner.setIncludes(includes);
    scanner.setExcludes(excludes);
    scanner.setIncludeAllTC(includeAllTC != null);
    scanner.setHtml(html != null);
    scanner.setTitle(title != null ? (String)title.iterator().next() : null);
    scanner.setXsl(xsl != null && !xsl.isEmpty() ? (String)xsl.iterator().next() : null);
    scanner.execute();
  }

  private static void printUsage()
  {
    System.out.println("Usage: java org.eclipse.wtp.releng.tools.component.codecoverage.CodeCoverage -api <api> -src <src> -use <use> -outputDir <outputDir> [-options]");
    System.out.println("");
    System.out.println("\t-api\t\t<api>\t\tlocation of your component.xml");
    System.out.println("\t-src\t\t<src>\t\tlocation of a Eclipse-based product (requires SDK build)");
    System.out.println("\t-trcxml\t\t<trcxml>\tlocation of the *.trcxml files generated by TPTP");
    System.out.println("\t-outputDir\t<outputDir>\toutput directory of component.xml files");
    System.out.println("");
    System.out.println("where options include:");
    System.out.println("");
    System.out.println("\t-skipAPIGen\t\t\tskip api-info.xml generation and use existing ones");
    System.out.println("\t-includes\t<includes>\tspace seperated packages to include");
    System.out.println("\t-excludes\t<excludes>\tspace seperated packages to exclude");
    System.out.println("\t-includeAllTC\t\t\t\tinclude plug-ins with 100% test coverage");
    System.out.println("\t-html\t\t\t\tgenerate HTML results");
    System.out.println("\t-title\t\t<title>\ttitle of the generated HTML report");
    System.out.println("\t-xsl\t\t<xsl>\t\tuse your own stylesheet. You must specify the -html option");
  }
}