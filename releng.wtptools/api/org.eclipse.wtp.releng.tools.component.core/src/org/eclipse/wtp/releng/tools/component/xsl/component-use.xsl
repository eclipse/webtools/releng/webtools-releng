<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<table border="1">
					<tr>
						<th>Name</th>
						<th>Reference</th>
						<th>Subclass</th>
						<th>Implement</th>
						<th>Instantiate</th>
					</tr>
					<xsl:for-each select="component-use/source">
						<xsl:sort select="@name"/>
						<xsl:if test="class-use">
							<xsl:apply-templates select="." />
						</xsl:if>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="source">
		<tr>
			<td><b>Source: <xsl:value-of select="@name"/></b></td>
			<td bgcolor="c8c8c8" colspan="4">&#160;</td>
		</tr>
		<xsl:for-each select="class-use">
			<xsl:sort select="@name"/>
			<xsl:apply-templates select="." />
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="class-use">
		<tr>
			<td><b>&#160;&#160;&#160;Class: <xsl:value-of select="@name"/></b></td>
			<td><xsl:value-of select="@reference"/></td>
			<td><xsl:value-of select="@subclass"/></td>
			<td><xsl:value-of select="@implement"/></td>
			<td><xsl:value-of select="@instantiate"/></td>
		</tr>
		<xsl:for-each select="method-use">
			<xsl:sort select="@name"/>
			<xsl:apply-templates select="." />
		</xsl:for-each>
		<xsl:for-each select="field-use">
			<xsl:sort select="@name"/>
			<xsl:apply-templates select="." />
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="method-use">
		<tr>
			<td>&#160;&#160;&#160;&#160;&#160;&#160;Method: <xsl:value-of select="@name"/>&#160;<xsl:value-of select="@descriptor"/></td>
			<td bgcolor="c8c8c8" colspan="4">&#160;</td>
		</tr>
	</xsl:template>
	<xsl:template match="field">
		<tr>
			<td>&#160;&#160;&#160;&#160;&#160;&#160;Field: <xsl:value-of select="@name"/></td>
			<td bgcolor="c8c8c8" colspan="4">&#160;</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>