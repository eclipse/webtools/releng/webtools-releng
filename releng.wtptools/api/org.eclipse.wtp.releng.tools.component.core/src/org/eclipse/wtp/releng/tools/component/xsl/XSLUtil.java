/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.xsl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * This is a helper utility class used in the XSL transformation of XML file formats to a 
 * compatible HTML format.
 *
 */
public class XSLUtil {

  // System property string 
  private static final String USER_DIR = "user.dir"; //$NON-NLS-1$
  
  /**
   * This forwards the transformation, passing along a new file input stream on the given file.
   * 
   * @param xsl InputStream
   * @param f File
   * @param os OutputStream
   * @param userDir String
   * @throws TransformerException
   * @throws TransformerConfigurationException
   * @throws IOException
   */
  public static void transform(InputStream xsl, File f, OutputStream os, String userDir) throws TransformerException, TransformerConfigurationException, IOException {
    transform(xsl, new FileInputStream(f), os, userDir);
  }

  /**
   * This will set the user directory to the passed in value, forward the transform, and then
   * reset the user directory back to it's original value.
   * 
   * @param xsl InputStream
   * @param data InputStream
   * @param os OutputStream
   * @param userDir String
   * @throws TransformerException
   * @throws TransformerConfigurationException
   * @throws IOException
   */
  public static void transform(InputStream xsl, InputStream data, OutputStream os, String userDir) throws TransformerException, TransformerConfigurationException, IOException {
    String currUserDir = System.getProperty(USER_DIR);
    // Override system value for user directory
    System.setProperty(USER_DIR, userDir);
    // Transform.
    transform(xsl, data, os);
    // Revert user directory value
    System.setProperty(USER_DIR, currUserDir);
  }

  /**
   * Create a new FileInputStream from the given file and perform the transform to the output stream.
   * 
   * @param xsl InputStream
   * @param f File
   * @param os OutputStream
   * @throws TransformerException
   * @throws TransformerConfigurationException
   * @throws IOException
   */
  public static void transform(InputStream xsl, File f, OutputStream os) throws TransformerException, TransformerConfigurationException, IOException {
    transform(xsl, new FileInputStream(f), os);
  }

  /**
   * This method handles the actual transformation and closes the input and output streams.
   * 
   * @param xsl InputStream
   * @param data InputStream
   * @param os OutputStream
   * @throws TransformerException
   * @throws TransformerConfigurationException
   * @throws IOException
   */
  public static void transform(InputStream xsl, InputStream data, OutputStream os) throws TransformerException, TransformerConfigurationException, IOException {
    // Grab a factory instance and create a new XSLT Transformer based on the xsl input stream
	TransformerFactory factory = TransformerFactory.newInstance();
    Transformer transformer = factory.newTransformer(new StreamSource(xsl));
    // Invoke the xsl transformation on the data input to the output
    transformer.transform(new StreamSource(data), new StreamResult(os));
    // Close the input and output stream
    os.close();
    xsl.close();
  }
}