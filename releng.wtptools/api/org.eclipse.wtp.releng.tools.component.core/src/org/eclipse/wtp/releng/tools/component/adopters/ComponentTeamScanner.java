/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.component.adopters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

/**
 * This abstract base class for scanning operations which make use of logical WTP component teams.
 * This class will manage which plugins are associated with which component team.
 */
public abstract class ComponentTeamScanner implements IComponentConstants {

	  // Instance variable for which plugins are associated with which logical component team grouping
	  private HashMap plugin2compTeam;
	  
	  // Instance variable for cache of WTP component teams
	  private List componentTeams;
	  
	  /**
	   * The ComponentTeam class manages applicable reference counts per logical grouping of 
	   * plugins.
	   */
	  public class ComponentTeam {
	    private String teamName;
	    private TreeMap extpt2RefCounts;
	    private TreeMap class2refCounts;
		private TreeMap pkg2refCounts;
		private TreeMap plugin2refCounts;

	    /**
	     * Simple Constructor
	     * @param teamName
	     */
	    public ComponentTeam(String teamName) {
	      this.teamName = teamName;
	    }
	    
	    /**
		 * @return String component team's name
		 */
	    public String getTeamName() {
	    	return teamName;
	    }
	    
	    /**
	     * @return TreeMap of Extension Point references
	     */
	    public TreeMap getExtensionPointReferenceCounts() {
	    	if (extpt2RefCounts==null)
	    		extpt2RefCounts = new TreeMap();
	    	return extpt2RefCounts;
	    }
	    
	    /**
		 * @return TreeMap of class reference counts
		 */
		public TreeMap getClassReferenceCounts() {
			if (class2refCounts==null)
				class2refCounts = new TreeMap();
			return class2refCounts;
		}
		
		/**
		 * @return TreeMap of package reference counts
		 */
		public TreeMap getPackageReferenceCounts() {
			if (pkg2refCounts==null)
				pkg2refCounts = new TreeMap();
			return pkg2refCounts;
		}
		
		/**
		 * @return TreeMap of plugin reference counts
		 */
		public TreeMap getPluginReferenceCounts() {
			if (plugin2refCounts==null)
				plugin2refCounts = new TreeMap();
			return plugin2refCounts;
		}
	  }
	  
	  /**
	   * @return HashMap of plugin keys and associated component team values
	   */
	  private HashMap getPluginComponentMap() {
		  if (plugin2compTeam==null) {
			  plugin2compTeam = new HashMap();
			  initializeComponentTeams();
		  }
		  return plugin2compTeam;
	  }
	  
	  /**
	   * Initialize the component teams list with the appropriate regular expressions to link plugins 
	   * to the known set of component teams.
	   */
	  private void initializeComponentTeams() {
		componentTeams = new ArrayList();
		
		// Create the JEE team with associated plugins
		ComponentTeam javaEE = new ComponentTeam(COMPONENT_TEAM_JAVA_EE);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_J2EE, javaEE);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_EJB, javaEE);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_SERVLET, javaEE);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_WEB, javaEE);
		componentTeams.add(javaEE);
		
		// Create the Common team with associated plugins
		ComponentTeam common = new ComponentTeam(COMPONENT_TEAM_COMMON);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_COMMAND, common);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_COMMON, common);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_VALIDATION, common);
		componentTeams.add(common);
		
		// Create the Editors team with associated plugins
		ComponentTeam editors = new ComponentTeam(COMPONENT_TEAM_EDITORS);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_JSP, editors);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_CSS, editors);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_DTD, editors);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_HTML, editors);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_JAVASCRIPT, editors);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_SSE, editors);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_XML, editors);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_XSD, editors);
		componentTeams.add(editors);
		
		// Create the RDB team with associated plugins
		ComponentTeam rdb = new ComponentTeam(COMPONENT_TEAM_RDB);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_RDB, rdb);
		componentTeams.add(rdb);
		
		// Create the Web Services team with associated plugins
		ComponentTeam webServices = new ComponentTeam(COMPONENT_TEAM_WEB_SERVICES);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_WS, webServices);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_WSDL, webServices);
		componentTeams.add(webServices);
		
		// Create the Server team with associated plugins
		ComponentTeam server = new ComponentTeam(COMPONENT_TEAM_SERVER);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_SERVER, server);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_INTERNET, server);
		componentTeams.add(server);
		
		// Create the JEM team with associated plugins
		ComponentTeam jem = new ComponentTeam(COMPONENT_TEAM_JEM);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_JEM,jem);
		componentTeams.add(jem);
		
		// Create the JSF team with associated plugins
		ComponentTeam jsf = new ComponentTeam(COMPONENT_TEAM_JSF);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_JSF,jsf);
		componentTeams.add(jsf);
		
		// Create the JPT team with associated plugins
		ComponentTeam jpt = new ComponentTeam(COMPONENT_TEAM_JPT);
		getPluginComponentMap().put(PLUGIN_EXPRESSION_JPT,jpt);
		componentTeams.add(jpt);
		
		// Add a "team" for the unknown references
		ComponentTeam unknown = new ComponentTeam(COMPONENT_TEAM_UNKNOWN);
		componentTeams.add(unknown);
	  }
	  
	  /**
	   * Helper method to find the corresponding component team for a given plugin id.  If one is
	   * not found, the unknown component is returned.
	   * 
	   * @param pluginId
	   * @return ComponentTeam for specified plugin
	   */
	  protected ComponentTeam getComponentTeam(String pluginId) {
	      // Try and match the proper component team to the referenced plugin from the 
	      // map of plugins to component teams
	      for (Iterator it3 = getPluginComponentMap().keySet().iterator(); it3.hasNext();) {
	        String regex = (String)it3.next();
	        if (pluginId.matches(regex)) {
	          return (ComponentTeam)getPluginComponentMap().get(regex);
	        }
	      }
	      // Otherwise return the unknown component
	      return getUnknownComponent();
	  }
	  
	  /**
	   * @return the unknown component where a plugin's logical component is not known
	   */
	  private ComponentTeam getUnknownComponent() {
		  for (int i=0; i<getComponentTeams().size(); i++) {
			  ComponentTeam compTeam = (ComponentTeam) getComponentTeams().get(i);
			  if (compTeam.getTeamName().equals(COMPONENT_TEAM_UNKNOWN))
				  return compTeam;
		  }
		  return null;
	  }
	  
	  /**
	   * @return the list of component teams
	   */
	  protected List getComponentTeams() {
		  if (componentTeams == null)
			  initializeComponentTeams();
		  return componentTeams;
	  }
}
