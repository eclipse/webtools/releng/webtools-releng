<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<script language="javascript">
				function twist(img)
				{
					var div = document.getElementById(img.id.substring(0, img.id.length - 4));
					if (div.style.display == "")
					{
						div.style.display = "none";
						img.src = "../twistclosed.gif";
					}
					else
					{
						div.style.display = "";
						img.src = "../twistopened.gif";
					}
				}
				var show = false;
				function allTwist()
				{
					var divs = document.getElementsByTagName("div");
					for (var i = 0; i &lt; divs.length; i++)
					{
						var img = document.getElementById(divs[i].id + '.img');
						if (!show)
						{
							divs[i].style.display = "none";
							img.src = "../twistclosed.gif";
						}
						else
						{
							divs[i].style.display = "";
							img.src = "../twistopened.gif";
						}
					}
					var a = document.getElementById("allTwistId");
					if (!show)
						a.childNodes[0].data = "Expand classes";
					else
						a.childNodes[0].data = "Collapse classes";
					show = !show;
				}
			</script>
			<body>
				<font face="Arial,Helvetica">
					<xsl:apply-templates select="api-compatibility"/>
				</font>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="api-compatibility">
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td align="left" width="60%">
					<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">API Compatibility Report for <xsl:value-of select="@name"/></font>
				</td>
				<td width="40%">
					<img src="../Idea.jpg" align="middle" height="86" hspace="50" width="120"/>
				</td>
			</tr>
		</table>
		<br/>
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td align="left">
					<a id="allTwistId" href="javascript:allTwist()">Collapse classes</a>
				</td>
			</tr>
		</table>
		<br/>
		<xsl:apply-templates select="removed-apis"/>
		<xsl:apply-templates select="new-apis"/>
	</xsl:template>

	<xsl:template match="removed-apis">
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td ALIGN="LEFT" VALIGN="TOP" COLSPAN="2" BGCOLOR="#0080C0">
					<b><font color="#FFFFFF" face="Arial,Helvetica">Removed APIs (methods: <xsl:value-of select="count(package-api/class-api/method-api)"/>, fields: <xsl:value-of select="count(package-api/class-api/field-api)"/>)</font></b>
				</td>
			</tr>
		</table>
		<br/>
		<xsl:choose>
			<xsl:when test="package-api/class-api">
				<table>
					<xsl:for-each select="package-api/class-api">
						<xsl:sort select="@name"/>
						<xsl:apply-templates select="."/>
					</xsl:for-each>
				</table>
			</xsl:when>
			<xsl:otherwise>
				None
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="new-apis">
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td ALIGN="LEFT" VALIGN="TOP" COLSPAN="2" BGCOLOR="#0080C0">
					<b><font color="#FFFFFF" face="Arial,Helvetica">New APIs (methods: <xsl:value-of select="count(package-api/class-api/method-api)"/>, fields: <xsl:value-of select="count(package-api/class-api/field-api)"/>)</font></b>
				</td>
			</tr>
		</table>
		<br/>
		<xsl:choose>
			<xsl:when test="package-api/class-api">
				<table>
					<xsl:for-each select="package-api/class-api">
						<xsl:sort select="@name"/>
						<xsl:apply-templates select="."/>
					</xsl:for-each>
				</table>
			</xsl:when>
			<xsl:otherwise>
				None
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="class-api">
		<tr>
			<td>
				<img id="{name(../..)}::{concat(../@name, @name)}.img" src="../twistopened.gif" onclick="javascript:twist(this)"/>
				<xsl:value-of select="concat(../@name, @name)"/>
				<p>
					<div id="{name(../..)}::{concat(../@name, @name)}">
						<ul>
							<xsl:for-each select="method-api">
								<li><b>Method&#160;-&#160;</b><xsl:value-of select="@name"/>&#160;&#160;&#160;<i><xsl:value-of select="@descriptor"/></i></li>
							</xsl:for-each>
						</ul>
						<ul>
							<xsl:for-each select="field-api">
								<li><b>Field&#160;-&#160;</b><xsl:value-of select="@name"/></li>
							</xsl:for-each>
						</ul>
					</div>
				</p>
			</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>
