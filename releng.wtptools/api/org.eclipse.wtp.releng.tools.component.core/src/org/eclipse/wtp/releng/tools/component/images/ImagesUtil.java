/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.images;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

public class ImagesUtil
{
  public static final String FAIL = "FAIL.gif";
  public static final String GREEN = "green.gif";
  public static final String GREY = "grey.gif";
  public static final String IDEA = "Idea.jpg";
  public static final String OK = "OK.gif";
  public static final String ORANGE = "orange.gif";
  public static final String RED = "red.gif";
  public static final String SPACE = "space.gif";
  public static final String TWIST_CLOSED = "twistclosed.gif";
  public static final String TWIST_OPENED = "twistopened.gif";
  public static final String VIEW_SVG = "viewsvg.gif";
  public static final String YELLOW = "yellow.gif";
  public static final String LINE = "line.gif";
  public static final String LINE_LAST = "line_last.gif";
  public static final String LINE_NOT_LAST = "line_notlast.gif";

  public static void copyAll(String dest)
  {
    copy(FAIL, dest);
    copy(GREEN, dest);
    copy(GREY, dest);
    copy(IDEA, dest);
    copy(OK, dest);
    copy(ORANGE, dest);
    copy(RED, dest);
    copy(SPACE, dest);
    copy(TWIST_CLOSED, dest);
    copy(TWIST_OPENED, dest);
    copy(VIEW_SVG, dest);
    copy(YELLOW, dest);
    copy(LINE, dest);
    copy(LINE_LAST, dest);
    copy(LINE_NOT_LAST, dest);
  }

  public static void copyAllFromBundle(Bundle bundle, String dest)
  {
    copyFromBundle(bundle, FAIL, dest);
    copyFromBundle(bundle, GREEN, dest);
    copyFromBundle(bundle, GREY, dest);
    copyFromBundle(bundle, IDEA, dest);
    copyFromBundle(bundle, OK, dest);
    copyFromBundle(bundle, ORANGE, dest);
    copyFromBundle(bundle, RED, dest);
    copyFromBundle(bundle, SPACE, dest);
    copyFromBundle(bundle, TWIST_CLOSED, dest);
    copyFromBundle(bundle, TWIST_OPENED, dest);
    copyFromBundle(bundle, VIEW_SVG, dest);
    copyFromBundle(bundle, YELLOW, dest);
    copyFromBundle(bundle, LINE, dest);
    copyFromBundle(bundle, LINE_LAST, dest);
    copyFromBundle(bundle, LINE_NOT_LAST, dest);
  }

  public static void copy(String imageId, String dest)
  {
    File file = new File(dest + imageId);
    if (!file.exists())
    {
      try
      {
        InputStream is = ClassLoader.getSystemResourceAsStream("org/eclipse/wtp/releng/tools/component/images/" + imageId);
        if (is != null)
        {
          BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
          copy(is, bos);
        }
        else
          copyFromBundle(Platform.getBundle("org.eclipse.wtp.releng.tools.component.core"), imageId, dest);
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
  }

  public static void copyFromBundle(Bundle bundle, String imageId, String dest)
  {
    File file = new File(dest + imageId);
    if (!file.exists())
    {
      try
      {
        InputStream is = bundle.getResource("org/eclipse/wtp/releng/tools/component/images/" + imageId).openStream();
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
        copy(is, bos);
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
  }

  private static void copy(InputStream is, OutputStream os) throws IOException
  {
    if (is != null && os != null)
    {
      try
      {
        byte[] b = new byte[2048];
        int read = is.read(b);
        while (read != -1)
        {
          os.write(b, 0, read);
          read = is.read(b);
        }
      }
      finally
      {
        try
        {
          os.close();
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
        try
        {
          is.close();
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
  }
}