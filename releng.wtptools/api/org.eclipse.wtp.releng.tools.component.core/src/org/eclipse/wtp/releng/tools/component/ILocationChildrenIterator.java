/*******************************************************************************
 * Copyright (c) 2002, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component;

/**
 * The <code>ILocationChildrenIterator</code> is a simple iterator that
 * iterates over the children of a location. A <code>null</code> is returned
 * when the end of the children list is reached.
 */

public interface ILocationChildrenIterator
{
  /**
   * Answers the next child location.
   * 
   * @return ILocation The next child location, or <code>null</code> if there
   *         are no more children.
   */
  ILocation next();
}
