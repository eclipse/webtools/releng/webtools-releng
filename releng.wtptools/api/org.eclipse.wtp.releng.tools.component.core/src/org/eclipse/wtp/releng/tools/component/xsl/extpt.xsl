<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<body>
				<table border="0" cellpadding="2" cellspacing="5" width="100%">
					<tr>
						<td align="left" width="60%">
							<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">
								Extension Point Usage Report
							</font>
						</td>
					</tr>
				</table>
				<table border="0" cellpadding="2" cellspacing="5" width="100%">
					<tr>
						<td ALIGN="LEFT" VALIGN="TOP" COLSPAN="2" BGCOLOR="#0080C0"><b><font color="#FFFFFF" face="Arial,Helvetica">
							Extension point usage summary
						</font></b></td>
					</tr>
				</table>
				<u><h5>
				<xsl:for-each select="root/team">
					<xsl:sort select="@lead"/>
					<a href="#{@lead}"><xsl:value-of select="@lead"/></a><br />
				</xsl:for-each>
				</h5></u>
				<xsl:for-each select="root/team">
					<xsl:sort select="@lead"/>
					<h2><a name="{@lead}" id="{@lead}"><xsl:value-of select="@lead"/></a></h2>
					<table id="data" border="1" width="80%">
						<tr>
							<th width="95%">Extension point</th>
							<th># of reference</th>
						</tr>
						<xsl:for-each select="extpt">
							<xsl:sort select="@ref" order="ascending" data-type="number"/>
							<tr>
								<td><xsl:value-of select="@id"/></td>
								<td><xsl:value-of select="@ref"/></td>
							</tr>
						</xsl:for-each>
					</table>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>