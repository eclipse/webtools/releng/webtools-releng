/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api.progress;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.eclipse.wtp.releng.tools.component.CommandOptionParser;

public class APIUseCoverageEmitter extends APITestCoverageEmitter
{
  public APIUseCoverageEmitter(String compAPIDir)
  {
    super(compAPIDir);
  }

  protected String getComponentSummaryXML()
  {
    return "index-comp-uc-summary.xml";
  }

  protected String getSummaryXML()
  {
    return "index-api-uc.xml";
  }

  protected String getSummaryHTML()
  {
    return "index-api-uc.html";
  }

  protected String getSummaryXSL()
  {
    return "org/eclipse/wtp/releng/tools/component/xsl/component-api-uc-summary.xsl";
  }

  protected String getCoverageXML()
  {
    return "component-api-uc.xml";
  }

  protected String getCoverageHTML()
  {
    return "component-api-uc.html";
  }

  public static void main(String[] args)
  {
    CommandOptionParser optionParser = new CommandOptionParser(args);
    Map options = optionParser.getOptions();
    List eclipseDir = (List)options.get(APIUseCoverageEmitter.OPTION_ECLIPSE_DIR);
    List testDir = (List)options.get(APIUseCoverageEmitter.OPTION_TEST_DIR);
    List compXMLDir = (List)options.get(APIUseCoverageEmitter.OPTION_COMPONENT_XML_DIR);
    List compAPIDir = (List)options.get(APIUseCoverageEmitter.OPTION_COMPONENT_API_DIR);
    List genHTML = (List)options.get(APIUseCoverageEmitter.OPTION_GEN_HTML);
    if (eclipseDir == null || testDir == null || compXMLDir == null || compAPIDir == null || eclipseDir.size() < 1 || testDir.size() < 1 || compXMLDir.size() < 1 || compAPIDir.size() < 1)
    {
      printUsage();
      System.exit(-1);
    }
    APIUseCoverageEmitter apiUseCoverageEmitter = new APIUseCoverageEmitter((String)compAPIDir.get(0));
    apiUseCoverageEmitter.setGenHTML(genHTML != null);
    apiUseCoverageEmitter.init(eclipseDir, testDir, compXMLDir);
    try
    {
      apiUseCoverageEmitter.genAPITestCoverageXML();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  private static void printUsage()
  {
    System.out.println("Usage: java org.eclipse.wtp.releng.tools.component.api.usecoverage.APIUseCoverageEmitter -eclipseDir <eclipseDir> -testDir <testDir> -compXMLDir <compXMLDir> -compAPIDir <compAPIDir> [-options]");
    System.out.println("");
    System.out.println("\t-eclipseDir\t<eclipseDir>\tspace seperated list of directories containing Eclipse plugins");
    System.out.println("\t-testDir\t<testDir>\tspace separated list of directories containing test plugins");
    System.out.println("\t-compXMLDir\t<compXMLDir>\tdirectories containing component.xml that will be checked for API test coverage");
    System.out.println("\t-compAPIDir\t<compVioDir>\toutput directory of component-api-tc.xml");
    System.out.println("");
    System.out.println("where options include:");
    System.out.println("");
    System.out.println("\t-genHTML\tgenerate test coverage report in HTML");
  }
}