<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:component="http://www.eclipse.org/component"
	xmlns:func="http://exslt.org/functions"
	extension-element-prefixes="func">

	<xsl:template match="/">
		<xsl:apply-templates select="root"/>
	</xsl:template>

	<xsl:template match="root">
		<xsl:variable name="xcount" select="component:max(count(summary), 10)"/>
		<xsl:variable name="ycount" select="10"/>
		<xsl:variable name="leftborder" select="150"/>
		<xsl:variable name="rightborder" select="50"/>
		<xsl:variable name="topborder" select="60"/>
		<xsl:variable name="bottomborder" select="100"/>
		<xsl:variable name="gridwidth" select="50"/>
		<xsl:variable name="gridheight" select="30"/>
		<xsl:variable name="svgwidth" select="$leftborder + $rightborder + ($xcount * $gridwidth)"/>
		<xsl:variable name="svgheight" select="$topborder + $bottomborder + ($ycount * $gridheight)"/>

		<svg width="{$svgwidth}" height="{$svgheight}" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve">
			<defs>
				<pattern id="grid" width="{$gridwidth}" height="{$gridheight}" patternUnits="userSpaceOnUse">
					<path d="M{$gridwidth} 0 L0 0 L0 {$gridheight}" style="fill:none;stroke:rgb(128,128,128);stroke-width:1"/>
				</pattern>
			</defs>
			<rect id="graph" x="{$leftborder}" y="{$topborder}" width="{$xcount * $gridwidth}" height="{$ycount * $gridheight}" style="stroke:rgb(128,128,128);stroke-width:1;fill:url(#grid)"/>

			<text x="{$svgwidth div 2}" y="{$topborder div 2}" style="text-anchor:middle"><xsl:value-of select="@name"/></text>
			<text x="{$leftborder div 2}" y="{$topborder + ($ycount * $gridheight div 2)}" style="text-anchor:middle">% completed</text>
			<text x="{$leftborder + ($xcount * $gridwidth div 2)}" y="{$svgheight - ($bottomborder div 2)}" style="text-anchor:middle">Date</text>

			<xsl:call-template name="printLegend">
				<xsl:with-param name="x" select="$rightborder div 2"/>
				<xsl:with-param name="y" select="$svgheight - ($bottomborder div 2)"/>
			</xsl:call-template>

			<xsl:call-template name="printyscale">
				<xsl:with-param name="svgheight" select="$svgheight"/>
				<xsl:with-param name="leftborder" select="$leftborder"/>
				<xsl:with-param name="bottomborder" select="$bottomborder"/>
				<xsl:with-param name="gridheight" select="$gridheight"/>
				<xsl:with-param name="y" select="0"/>
				<xsl:with-param name="ycount" select="$ycount"/>
			</xsl:call-template>

			<xsl:call-template name="printProgress">
				<xsl:with-param name="root" select="."/>
				<xsl:with-param name="componentname" select="@name"/>
				<xsl:with-param name="tscount" select="count(summary)"/>
				<xsl:with-param name="currIndex" select="1"/>
				<xsl:with-param name="xorigin" select="$leftborder"/>
				<xsl:with-param name="yorigin" select="$svgheight - $bottomborder"/>
				<xsl:with-param name="gridwidth" select="$gridwidth"/>
				<xsl:with-param name="percentwidth" select="$ycount * $gridheight div 100"/>
			</xsl:call-template>
		</svg>
	</xsl:template>

	<xsl:template name="printProgress">
		<xsl:param name="root"/>
		<xsl:param name="componentname"/>
		<xsl:param name="tscount"/>
		<xsl:param name="currIndex"/>
		<xsl:param name="xorigin"/>
		<xsl:param name="yorigin"/>
		<xsl:param name="gridwidth"/>
		<xsl:param name="percentwidth"/>
		<xsl:choose>
			<xsl:when test="$tscount &gt; $currIndex">
				<xsl:call-template name="printProgress2">
					<xsl:with-param name="curr-api-info" select="document(concat($root/summary[$currIndex]/@timestamp, '/api-info-summary.xml'))/api-info-summary/api-info[@name=$componentname or concat('org.eclipse.', @name)=$componentname]"/>
					<xsl:with-param name="next-api-info" select="document(concat($root/summary[$currIndex + 1]/@timestamp, '/api-info-summary.xml'))/api-info-summary/api-info[@name=$componentname or concat('org.eclipse.', @name)=$componentname]"/>
					<xsl:with-param name="currIndex" select="$currIndex"/>
					<xsl:with-param name="xorigin" select="$xorigin"/>
					<xsl:with-param name="yorigin" select="$yorigin"/>
					<xsl:with-param name="gridwidth" select="$gridwidth"/>
					<xsl:with-param name="percentwidth" select="$percentwidth"/>
					<xsl:with-param name="date" select="$root/summary[$currIndex]/@timestamp"/>
				</xsl:call-template>
				<xsl:call-template name="printProgress">
					<xsl:with-param name="root" select="$root"/>
					<xsl:with-param name="componentname" select="$componentname"/>
					<xsl:with-param name="tscount" select="$tscount"/>
					<xsl:with-param name="currIndex" select="$currIndex + 1"/>
					<xsl:with-param name="xorigin" select="$xorigin"/>
					<xsl:with-param name="yorigin" select="$yorigin"/>
					<xsl:with-param name="gridwidth" select="$gridwidth"/>
					<xsl:with-param name="percentwidth" select="$percentwidth"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="printProgress2">
					<xsl:with-param name="curr-api-info" select="document(concat($root/summary[$currIndex]/@timestamp, '/api-info-summary.xml'))/api-info-summary/api-info[@name=$componentname or concat('org.eclipse.', @name)=$componentname]"/>
					<xsl:with-param name="next-api-info" select="-1"/>
					<xsl:with-param name="currIndex" select="$currIndex"/>
					<xsl:with-param name="xorigin" select="$xorigin"/>
					<xsl:with-param name="yorigin" select="$yorigin"/>
					<xsl:with-param name="gridwidth" select="$gridwidth"/>
					<xsl:with-param name="percentwidth" select="$percentwidth"/>
					<xsl:with-param name="date" select="$root/summary[$currIndex]/@timestamp"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="printProgress2">
		<xsl:param name="curr-api-info"/>
		<xsl:param name="next-api-info"/>
		<xsl:param name="currIndex"/>
		<xsl:param name="xorigin"/>
		<xsl:param name="yorigin"/>
		<xsl:param name="gridwidth"/>
		<xsl:param name="percentwidth"/>
		<xsl:param name="date"/>
		<xsl:variable name="currapicount" select="$curr-api-info/@method-count"/>
		<xsl:variable name="currx" select="$xorigin + ($currIndex * $gridwidth)"/>
		<xsl:variable name="hundredpercent" select="$yorigin - (100 * $percentwidth)"/>
		<svg:text x="{$currx}" y="{$yorigin + 15 + (($currIndex + 1) mod 2 * 15)}" style="text-anchor:middle"><xsl:value-of select="$date"/></svg:text>
		<xsl:choose>
			<xsl:when test="$currapicount &gt; 0">
				<xsl:variable name="currtestcount" select="$curr-api-info/@method-tested"/>
				<xsl:variable name="currjavadoccount" select="$curr-api-info/@method-javadoced"/>
				<xsl:variable name="currtestpercent" select="$currtestcount div $currapicount * 100"/>
				<xsl:variable name="currjavadocpercent" select="$currjavadoccount div $currapicount * 100"/>
				<xsl:variable name="currtesty" select="$yorigin - round($currtestpercent * $percentwidth)"/>
				<xsl:variable name="currjavadocy" select="$yorigin - round($currjavadocpercent * $percentwidth)"/>

				<svg:circle cx="{$currx}" cy="{$currtesty}" r="3" fill="blue"/>
				<svg:text x="{$currx}" y="{$currtesty - 23}" style="text-anchor:middle"><xsl:value-of select="$currtestcount"/>/<xsl:value-of select="$currapicount"/></svg:text>
				<svg:text x="{$currx}" y="{$currtesty - 10}" style="text-anchor:middle">(<xsl:value-of select="round($currtestpercent)"/>%)</svg:text>

				<svg:circle cx="{$currx}" cy="{$currjavadocy}" r="3" fill="orange"/>
				<svg:text x="{$currx}" y="{$currjavadocy - 23}" style="text-anchor:middle"><xsl:value-of select="$currjavadoccount"/>/<xsl:value-of select="$currapicount"/></svg:text>
				<svg:text x="{$currx}" y="{$currjavadocy - 10}" style="text-anchor:middle">(<xsl:value-of select="round($currjavadocpercent)"/>%)</svg:text>

				<xsl:call-template name="printProgress3">
					<xsl:with-param name="next-api-info" select="$next-api-info"/>
					<xsl:with-param name="currIndex" select="$currIndex"/>
					<xsl:with-param name="xorigin" select="$xorigin"/>
					<xsl:with-param name="yorigin" select="$yorigin"/>
					<xsl:with-param name="gridwidth" select="$gridwidth"/>
					<xsl:with-param name="percentwidth" select="$percentwidth"/>
					<xsl:with-param name="currx" select="$currx"/>
					<xsl:with-param name="currtesty" select="$currtesty"/>
					<xsl:with-param name="currjavadocy" select="$currjavadocy"/>
					<xsl:with-param name="hundredpercent" select="$hundredpercent"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<svg:circle cx="{$currx}" cy="{$hundredpercent}" r="3" fill="blue"/>
				<svg:circle cx="{$currx}" cy="{$hundredpercent}" r="3" fill="orange"/>
				<svg:text x="{$currx}" y="{$hundredpercent - 8}" style="text-anchor:middle">No API</svg:text>
				<xsl:call-template name="printProgress3">
					<xsl:with-param name="next-api-info" select="$next-api-info"/>
					<xsl:with-param name="currIndex" select="$currIndex"/>
					<xsl:with-param name="xorigin" select="$xorigin"/>
					<xsl:with-param name="yorigin" select="$yorigin"/>
					<xsl:with-param name="gridwidth" select="$gridwidth"/>
					<xsl:with-param name="percentwidth" select="$percentwidth"/>
					<xsl:with-param name="currx" select="$currx"/>
					<xsl:with-param name="currtesty" select="$hundredpercent"/>
					<xsl:with-param name="currjavadocy" select="$hundredpercent"/>
					<xsl:with-param name="hundredpercent" select="$hundredpercent"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="printProgress3">
		<xsl:param name="next-api-info"/>
		<xsl:param name="currIndex"/>
		<xsl:param name="xorigin"/>
		<xsl:param name="yorigin"/>
		<xsl:param name="gridwidth"/>
		<xsl:param name="percentwidth"/>
		<xsl:param name="currx"/>
		<xsl:param name="currtesty"/>
		<xsl:param name="currjavadocy"/>
		<xsl:param name="hundredpercent"/>
		<xsl:if test="$next-api-info != -1">
			<xsl:variable name="nextapicount" select="$next-api-info/@method-count"/>
			<xsl:variable name="nextx" select="$xorigin + (($currIndex + 1) * $gridwidth)"/>
			<xsl:choose>
				<xsl:when test="$nextapicount &gt; 0">
					<xsl:variable name="nexttestcount" select="$next-api-info/@method-tested"/>
					<xsl:variable name="nexttestpercent" select="$nexttestcount div $nextapicount * 100"/>
					<xsl:variable name="nexttesty" select="$yorigin - round($nexttestpercent * $percentwidth)"/>
					<svg:line x1="{$currx}" y1="{$currtesty}" x2="{$nextx}" y2="{$nexttesty}" style="stroke:blue;stroke-width:1"/>
				</xsl:when>
				<xsl:otherwise>
					<svg:line x1="{$currx}" y1="{$currtesty}" x2="{$nextx}" y2="{$hundredpercent}" style="stroke:blue;stroke-width:1"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$next-api-info != -1">
			<xsl:variable name="nextapicount" select="$next-api-info/@method-count"/>
			<xsl:variable name="nextx" select="$xorigin + (($currIndex + 1) * $gridwidth)"/>
			<xsl:choose>
				<xsl:when test="$nextapicount &gt; 0">
					<xsl:variable name="nextjavadoccount" select="$next-api-info/@method-javadoced"/>
					<xsl:variable name="nextjavadocpercent" select="$nextjavadoccount div $nextapicount * 100"/>
					<xsl:variable name="nextjavadocy" select="$yorigin - round($nextjavadocpercent * $percentwidth)"/>
					<svg:line x1="{$currx}" y1="{$currjavadocy}" x2="{$nextx}" y2="{$nextjavadocy}" style="stroke:orange;stroke-width:1"/>
				</xsl:when>
				<xsl:otherwise>
					<svg:line x1="{$currx}" y1="{$currjavadocy}" x2="{$nextx}" y2="{$hundredpercent}" style="stroke:orange;stroke-width:1"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<func:function name="component:max">
		<xsl:param name="x"/>
		<xsl:param name="y"/>
		<xsl:choose>
			<xsl:when test="$x &gt; $y">
				<func:result select="$x"/>
			</xsl:when>
			<xsl:otherwise>
				<func:result select="$y"/>
			</xsl:otherwise>
		</xsl:choose>
	</func:function>

	<xsl:template name="printLegend">
		<xsl:param name="x"/>
		<xsl:param name="y"/>
		<svg:line x1="{$x}" y1="{$y}" x2="{$x + 50}" y2="{$y}" style="stroke:blue;stroke-width:1"/>
		<svg:text x="{$x + 55}" y="{$y}" fill="blue">JUnit</svg:text>
		<svg:line x1="{$x}" y1="{$y + 20}" x2="{$x + 50}" y2="{$y + 20}" style="stroke:orange;stroke-width:1"/>
		<svg:text x="{$x + 55}" y="{$y + 20}" fill="orange">Javadoc</svg:text>
	</xsl:template>

	<xsl:template name="printyscale">
		<xsl:param name="svgheight"/>
		<xsl:param name="leftborder"/>
		<xsl:param name="bottomborder"/>
		<xsl:param name="gridheight"/>
		<xsl:param name="y"/>
		<xsl:param name="ycount"/>
		<xsl:if test="$y &lt;= $ycount">
			<xsl:call-template name="printyscale2">
				<xsl:with-param name="svgheight" select="$svgheight"/>
				<xsl:with-param name="leftborder" select="$leftborder"/>
				<xsl:with-param name="bottomborder" select="$bottomborder"/>
				<xsl:with-param name="gridheight" select="$gridheight"/>
				<xsl:with-param name="y" select="$y"/>
				<xsl:with-param name="ycount" select="$ycount"/>
			</xsl:call-template>
			<xsl:call-template name="printyscale">
				<xsl:with-param name="svgheight" select="$svgheight"/>
				<xsl:with-param name="leftborder" select="$leftborder"/>
				<xsl:with-param name="bottomborder" select="$bottomborder"/>
				<xsl:with-param name="gridheight" select="$gridheight"/>
				<xsl:with-param name="y" select="$y + 1"/>
				<xsl:with-param name="ycount" select="$ycount"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="printyscale2">
		<xsl:param name="svgheight"/>
		<xsl:param name="leftborder"/>
		<xsl:param name="bottomborder"/>
		<xsl:param name="gridheight"/>
		<xsl:param name="y"/>
		<xsl:param name="ycount"/>
		<text x="{$leftborder - 20}" y="{$svgheight - $bottomborder - ($y * $gridheight)}"><xsl:value-of select="$y * $ycount"/></text>
	</xsl:template>

</xsl:stylesheet>
