/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MethodUse extends MethodAPI
{
  private List lines;

  /**
   * @return Returns the line.
   */
  public Collection getLines()
  {
    if (lines == null)
      return new ArrayList(0);
    else
      return new ArrayList(lines);
  }

  public void addLine(int line)
  {
    if (lines == null)
      lines = new ArrayList(1);
    lines.add(String.valueOf(line));
  }

  public void addLines(Collection l)
  {
    if (lines == null)
      lines = new ArrayList(l);
    else
      lines.addAll(l);
  }

  public int sizeLines()
  {
    return lines != null ? lines.size() : 0;
  }
}
