<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<h2><xsl:value-of select="api-compatibility/@name"/></h2>
				<xsl:if test="api-compatibility/removed-apis/class-api">
					<fieldset>
						<legend><img src="../FAIL.gif"/>&#160;Removed APIs</legend>
						<xsl:for-each select="api-compatibility/removed-apis/class-api">
							<xsl:apply-templates select="." />
						</xsl:for-each>
					</fieldset>
				</xsl:if>
				<br/>
				<xsl:if test="api-compatibility/new-apis/class-api">
					<fieldset>
						<legend>New APIs</legend>
						<xsl:for-each select="api-compatibility/new-apis/class-api">
							<xsl:apply-templates select="." />
						</xsl:for-each>
					</fieldset>
				</xsl:if>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="class-api">
		<h3><xsl:value-of select="@name"/></h3>
		<ul>
			<xsl:for-each select="method-api">
				<li><b>Method&#160;-&#160;</b><xsl:value-of select="@name"/>&#160;<i><xsl:value-of select="@descriptor"/></i></li>
			</xsl:for-each>
		</ul>
		<ul>
			<xsl:for-each select="field-api">
				<li><b>Field&#160;-&#160;</b><xsl:value-of select="@name"/>&#160;<i><xsl:value-of select="@descriptor"/></i></li>
			</xsl:for-each>
		</ul>
	</xsl:template>
</xsl:stylesheet>
