/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api.violation;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.api.ComponentUse;
import org.eclipse.wtp.releng.tools.component.api.Source;
import org.eclipse.wtp.releng.tools.component.internal.ComponentEntry;
import org.eclipse.wtp.releng.tools.component.internal.ComponentSummary;

public class ComponentViolationSummary extends ComponentSummary
{
  private static final String ROOT_TAG_NAME = "component-violation-summary";

  public void add(ComponentUse compViolation)
  {
    ComponentViolationEntry entry = new ComponentViolationEntry();
    entry.setCompName(compViolation.getName());
    int numViolations = 0;
    List sources = compViolation.getSources();
    for (Iterator it = sources.iterator(); it.hasNext();)
      numViolations += ((Source)it.next()).getClassUses().size();
    entry.setNumViolations(numViolations);
    entry.setRef(compViolation.getLocation().getAbsolutePath());
    add(entry);
  }

  public void saveAsHTML(ILocation html) throws TransformerConfigurationException, TransformerException, IOException
  {
    saveAsHTML(html, "org/eclipse/wtp/releng/tools/component/xsl/component-violation-summary.xsl", ROOT_TAG_NAME);
  }

  public void save(ILocation location) throws IOException
  {
    save(location, ROOT_TAG_NAME);
  }

  private class ComponentViolationEntry extends ComponentEntry
  {
    private int numViolations;

    protected ComponentViolationEntry()
    {
      this.numViolations = -1;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append("<component-violation ");
      sb.append(toAttribute("name", getCompName()));
      sb.append(toAttribute("count", String.valueOf(numViolations)));
      sb.append(toAttribute("ref", getRef()));
      sb.append("/>");
      return sb.toString();
    }

    /**
     * @return Returns the numViolations.
     */
    public int getNumViolations()
    {
      return numViolations;
    }

    /**
     * @param numViolations The numViolations to set.
     */
    public void setNumViolations(int numViolations)
    {
      this.numViolations = numViolations;
    }
  }
}