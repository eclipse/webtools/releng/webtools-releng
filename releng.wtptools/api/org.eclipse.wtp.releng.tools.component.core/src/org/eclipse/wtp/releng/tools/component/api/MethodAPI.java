/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.eclipse.jdt.core.Signature;
import org.eclipse.wtp.releng.tools.component.internal.ComponentObject;

public class MethodAPI extends ComponentObject
{
  private String name;
  private int access;
  private String descriptor;
  private List throwTypes;
  private TestCoverage testCoverage;
  private JavadocCoverage javadocCoverage;

  public MethodAPI()
  {
    access = -1;
  }

  /**
   * @return Returns the access.
   */
  public int getAccess()
  {
    return access;
  }

  /**
   * @param access The access to set.
   */
  public void setAccess(int access)
  {
    this.access = access;
  }

  /**
   * @return Returns the descriptor.
   */
  public String getDescriptor()
  {
    return descriptor;
  }

  /**
   * @param descriptor The descriptor to set.
   */
  public void setDescriptor(String descriptor)
  {
    this.descriptor = descriptor;
  }

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    StringBuffer sb = new StringBuffer(name);
    int index = name.indexOf('<');
    while (index != -1)
    {
      sb.deleteCharAt(index);
      sb.insert(index, new char[] {'&', 'l', 't', ';'}, 0, 4);
      index = sb.toString().indexOf('<');
    }
    return sb.toString();
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    StringBuffer sb = new StringBuffer(name);
    int index = name.indexOf("&lt;");
    while (index != -1)
    {
      sb.delete(index, index + 4);
      sb.insert(index, '<');
      index = sb.toString().indexOf("&lt;");
    }
    this.name = sb.toString();
  }

  /**
   * @return Returns the throwTypes.
   */
  public List getThrows()
  {
    if (throwTypes == null)
      throwTypes = new ArrayList(1);
    return throwTypes;
  }

  public void addThrows(Collection throwTypes)
  {
    if (throwTypes == null)
      throwTypes = new ArrayList(throwTypes);
    else
      throwTypes.addAll(throwTypes);
  }

  public int sizeThrows()
  {
    return throwTypes == null ? 0 : throwTypes.size();
  }

  public List getInputs()
  {
    String[] encodedInputs = Signature.getParameterTypes(getDescriptor());
    List decodedInputs = new ArrayList(encodedInputs.length);
    for (int i = 0; i < encodedInputs.length; i++)
      decodedInputs.add(decodeDescriptor(encodedInputs[i]));
    return decodedInputs;
  }

  public String getReturn()
  {
    return decodeDescriptor(Signature.getReturnType(getDescriptor()));
  }

  private String decodeDescriptor(String descriptor)
  {
    return decodeClassName(Signature.toString(descriptor));
  }

  private String decodeClassName(String className)
  {
    return className.replace('/', '.');
  }

  public TestCoverage getTestCoverage()
  {
    if (testCoverage == null)
      testCoverage = new TestCoverage();
    return testCoverage;
  }

  public void setTestCoverage(TestCoverage testCoverage)
  {
    this.testCoverage = testCoverage;
  }

  public int countTestcases()
  {
    return testCoverage != null ? testCoverage.getTests().size() : 0;
  }

  public JavadocCoverage getJavadocCoverage()
  {
    if (javadocCoverage == null)
      javadocCoverage = new JavadocCoverage();
    return javadocCoverage;
  }

  public void setJavadocCoverage(JavadocCoverage javadocCoverage)
  {
    this.javadocCoverage = javadocCoverage;
  }

  public boolean isSetJavadocCoverage()
  {
    return javadocCoverage != null;
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<method-api");
    sb.append(toAttribute("name", getName()));
    sb.append(toAttribute("descriptor", getDescriptor()));
    int access = getAccess();
    if (access != -1)
      sb.append(toAttribute("access", String.valueOf(access)));
    if (sizeThrows() > 0)
      sb.append(toAttribute("throws", getThrows(), " "));
    sb.append(">");
    if (testCoverage != null)
      sb.append(testCoverage.toString());
    if (javadocCoverage != null)
      sb.append(javadocCoverage.toString());
    sb.append("</method-api>");
    return sb.toString();
  }
}