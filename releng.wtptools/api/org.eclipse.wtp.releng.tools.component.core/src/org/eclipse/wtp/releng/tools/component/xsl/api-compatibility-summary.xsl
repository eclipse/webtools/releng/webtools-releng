<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<script language="javascript">
				function showComponentOnly()
				{
					toggleDiv("component::", false);
					toggleDiv("removed::", false);
					toggleDiv("new::", false);
					toggleDiv("class::", false);
				}
				function showRemovedAPIsOnly()
				{
					toggleDiv("component::", true);
					toggleDiv("removed::", true);
					toggleDiv("new::", false);
					toggleDiv("class::", false);
				}
				function showNewAPIsOnly()
				{
					toggleDiv("component::", true);
					toggleDiv("removed::", false);
					toggleDiv("new::", true);
					toggleDiv("class::", false);
				}
				function showClassesOnly()
				{
					toggleDiv("component::", true);
					toggleDiv("removed::", true);
					toggleDiv("new::", true);
					toggleDiv("class::", false);
				}
				function showAll()
				{
					toggleDiv("component::", true);
					toggleDiv("removed::", true);
					toggleDiv("new::", true);
					toggleDiv("class::", true);
				}
				function toggleDiv(prefix, show)
				{
					var divs = document.getElementsByTagName("div");
					for (var i = 0; i &lt; divs.length; i++)
					{
						if (divs[i].id.indexOf(prefix) == 0)
						{
							var img = document.getElementById(divs[i].id + "::twist");
							if (show)
							{
								if (img != null)
								{
									img.src = "twistopened.gif";
								}
								divs[i].style.display = "";
							}
							else
							{
								if (img != null)
								{
									img.src = "twistclosed.gif";
								}
								divs[i].style.display = "none";
							}
						}
					}
				}
				function toggleComponent(id)
				{
					var divRemoved = document.getElementById(id + "::removed");
					var divNew = document.getElementById(id + "::new");
					var img = document.getElementById(id + "::twist");
					if (divRemoved.style.display == "")
					{
						img.src = "twistclosed.gif";
						divRemoved.style.display = "none";
					}
					else
					{
						img.src = "twistopened.gif";
						divRemoved.style.display = "";
					}
					if (divNew.style.display == "")
					{
						img.src = "twistclosed.gif";
						divNew.style.display = "none";
					}
					else
					{
						img.src = "twistopened.gif";
						divNew.style.display = "";
					}
				}
				function toggle(id)
				{
					var div = document.getElementById(id);
					var img = document.getElementById(id + "::twist");
					if (div.style.display == "")
					{
						img.src = "twistclosed.gif";
						div.style.display = "none";
					}
					else
					{
						img.src = "twistopened.gif";
						div.style.display = "";
					}
				}
			</script>
			<body>
				<table width="100%" cellspacing="5" cellpadding="2" border="0">
					<tbody>
						<tr>
							<td width="60%" align="left">
								<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">Eclipse WTP API Compatibility Report</font>
							</td>
							<td width="40%">
								<img width="120" hspace="50" height="86" align="middle" src="Idea.jpg"/>
							</td>
						</tr>
					</tbody>
				</table>
				<table WIDTH="100%" CELLPADDING="2" CELLSPACING="5" BORDER="0">
					<tr>
						<td BGCOLOR="#0080C0" COLSPAN="2" VALIGN="TOP" ALIGN="LEFT"><b><font face="Arial,Helvetica" color="#FFFFFF">API compatibility report</font></b></td>
					</tr>
				</table>
				<table border="0">
					<tr><td><a href="javascript:showComponentOnly()">Show components only</a></td></tr>
					<tr><td><a href="javascript:showRemovedAPIsOnly()">Show removed APIs only</a></td></tr>
					<tr><td><a href="javascript:showNewAPIsOnly()">Show new APIs only</a></td></tr>
					<tr><td><a href="javascript:showClassesOnly()">Show classes only</a></td></tr>
					<tr><td><a href="javascript:showAll()">Show all</a></td></tr>
					<tr><td>&#160;</td></tr>
				</table>
				<font face="Arial,Helvetica">
					<xsl:for-each select="api-compatibility-summary/api-compatibility">
						<xsl:sort select="@name"/>
						<xsl:variable name="report" select="document(@ref)/api-compatibility"/>
						<xsl:if test="$report/new-apis/class-api or $report/removed-apis/class-api">
							<xsl:apply-templates select="$report"/>
						</xsl:if>
					</xsl:for-each>
				</font>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="api-compatibility">
		<table border="0">
			<tr>
				<td>
					<div id="component::{@name}"/>
					<a href="javascript:toggleComponent('component::{@name}')"><img id="component::{@name}::twist" border="0" src="twistopened.gif"/></a>&#160;
					<xsl:value-of select="@name"/>&#160;
					(Removed methods: <xsl:value-of select="count(removed-apis/class-api/method-api)"/>,
					Removed fields: <xsl:value-of select="count(removed-apis/class-api/field-api)"/>,
					New methods: <xsl:value-of select="count(new-apis/class-api/method-api)"/>,
					New fields: <xsl:value-of select="count(new-apis/class-api/field-api)"/>)
				</td>
			</tr>
			<tr>
				<td>
					<div id="component::{@name}::removed">
						<table border="0">
							<xsl:if test="removed-apis/class-api">
								<xsl:apply-templates select="removed-apis">
									<xsl:with-param name="name" select="@name"/>
								</xsl:apply-templates>
							</xsl:if>
						</table>
					</div>
					<div id="component::{@name}::new">
						<table border="0">
							<xsl:if test="new-apis/class-api">
								<xsl:apply-templates select="new-apis">
									<xsl:with-param name="name" select="@name"/>
								</xsl:apply-templates>
							</xsl:if>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template match="removed-apis">
		<xsl:param name="name"/>
		<tr>
			<td>
				<img border="0" src="space.gif"/>
				<a href="javascript:toggle('removed::{$name}')"><img id="removed::{$name}::twist" border="0" src="twistopened.gif"/></a>&#160;Removed APIs
			</td>
		</tr>
		<tr>
			<td>
				<div id="removed::{$name}">
					<table border="0">
						<xsl:for-each select="class-api[((@access - (@access mod 512)) mod 1024) = 512]">
							<xsl:sort select="@name"/>
							<xsl:apply-templates select=".">
								<xsl:with-param name="isInterface" select="1"/>
							</xsl:apply-templates>
						</xsl:for-each>
						<xsl:for-each select="class-api[((@access - (@access mod 512)) mod 1024) != 512]">
							<xsl:sort select="@name"/>
							<xsl:apply-templates select=".">
								<xsl:with-param name="isInterface" select="0"/>
							</xsl:apply-templates>
						</xsl:for-each>
					</table>
				</div>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="new-apis">
		<xsl:param name="name"/>
		<tr>
			<td>
				<img border="0" src="space.gif"/>
				<a href="javascript:toggle('new::{$name}')"><img id="new::{$name}::twist" border="0" src="twistopened.gif"/></a>&#160;New APIs
			</td>
		</tr>
		<tr>
			<td>
				<div id="new::{$name}">
					<table border="0">
						<xsl:for-each select="class-api[((@access - (@access mod 512)) mod 1024) = 512]">
							<xsl:sort select="@name"/>
							<xsl:apply-templates select=".">
								<xsl:with-param name="isInterface" select="1"/>
							</xsl:apply-templates>
						</xsl:for-each>
						<xsl:for-each select="class-api[((@access - (@access mod 512)) mod 1024) != 512]">
							<xsl:sort select="@name"/>
							<xsl:apply-templates select=".">
								<xsl:with-param name="isInterface" select="0"/>
							</xsl:apply-templates>
						</xsl:for-each>
					</table>
				</div>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="class-api">
		<xsl:param name="isInterface"/>
		<tr>
			<td>
				<img border="0" src="space.gif"/>
				<img border="0" src="space.gif"/>
				<a href="javascript:toggle('class::{@name}')"><img id="class::{@name}::twist" border="0" src="twistopened.gif"/></a>&#160;
				<xsl:choose>
					<xsl:when test="$isInterface = 1">
						<b>Interface</b>&#160;
					</xsl:when>
					<xsl:otherwise>
						<b>Class</b>&#160;
					</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="@name"/>
			</td>
		</tr>
		<tr>
			<td>
				<div id="class::{@name}">
					<table border="0">
						<xsl:for-each select="method-api">
							<xsl:sort select="@name"/>
							<xsl:apply-templates select="."/>
						</xsl:for-each>
						<xsl:for-each select="field-api">
							<xsl:sort select="@name"/>
							<xsl:apply-templates select="."/>
						</xsl:for-each>
					</table>
				</div>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="method-api">
		<tr>
			<td>
				<img border="0" src="space.gif"/>
				<img border="0" src="space.gif"/>
				<img border="0" src="space.gif"/>
				<b>Method</b>&#160;
				<xsl:value-of select="@name"/>&#160;
				<i><xsl:value-of select="@descriptor"/></i>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="field-api">
		<tr>
			<td>
				<img border="0" src="space.gif"/>
				<img border="0" src="space.gif"/>
				<img border="0" src="space.gif"/>
				<b>Field</b>&#160;
				<xsl:value-of select="@name"/>&#160;
				<i><xsl:value-of select="@descriptor"/></i>
			</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>
