/*******************************************************************************
 * Copyright (c) 2006, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.adopters;

/**
 * This is a base class for caching information about named references.
 */
public class NamedRef {
  // class variable for strings	
  private static final String LESS_THAN = "&lt;"; //$NON-NLS-1$
  
  // Instance variables detailing information about the reference
  private String name;
  private String descriptor;
  private int refCount = 0;
  
  /**
   * @return String descriptor
   */
  public String getDescriptor() {
    return descriptor;
  }

  /**
   * Set the descriptor.
   * @param String descriptor
   */
  public void setDescriptor(String descriptor) {
    this.descriptor = descriptor;
  }

  /**
   * @return String name
   */
  public String getName() {
    return name;
  }

  /**
   * Set the name string.
   * @param String name
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /**
   * @return int reference count
   */
  public int getRefCount() {
    return refCount;
  }

  /**
   * Set the reference count.
   * @param aRefCount int
   */
  public void setRefCount(int aRefCount) {
    refCount = aRefCount;
  }

  /**
   * Increment the current reference count by 1.
   */
  public void incRefCount() {
    this.refCount++;
  }

  /**
   * Encode all occurences of "<" with "&lt;" in the given string
   * 
   * @param s String
   * @return encoded String
   */
  protected String encode(String s) {
    int index = s.indexOf('<');
    if (index != -1) {
      StringBuffer sb = new StringBuffer(s);
      while (index != -1) {
        sb.deleteCharAt(index);
        sb.insert(index, new char[] {'&', 'l', 't', ';'}, 0, 4);
        index = sb.toString().indexOf('<');
      }
      return sb.toString();
    }
    return s;
  }

  /**
   * Decode all occurences of "&lt;" with "<" in the given string
   * 
   * @param s String
   * @return decoded String
   */
  protected String decode(String s) {
    int index = s.indexOf(LESS_THAN);
    if (index != -1) {
      StringBuffer sb = new StringBuffer(s);
      while (index != -1) {
        sb.delete(index, index + 4);
        sb.insert(index, '<');
        index = sb.toString().indexOf(LESS_THAN);
      }
      return sb.toString();
    }
    return s;
  }
}