/*******************************************************************************
 * Copyright (c) 2006, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.adopters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * The class PluginRef manages a plugin reference including all of the related class references
 * and those field and method references related to those classes.
 */
public class PluginRef {
  // Instance variables
  private String id;
  private Map classRefs;

  /**
   * Get the class references for this plugin reference.
   * @return Collection
   */
  public Collection getClassRefs() {
    if (classRefs != null)
    	return new ArrayList(classRefs.values());
    return Collections.EMPTY_LIST;  
  }

  /**
   * Get the associated class reference with the given name
   * @param className
   * @return ClassRef
   */
  public ClassRef getClassRef(String className) {
    if (classRefs != null)
    	return (ClassRef)classRefs.get(className);
    return null;  
  }

  /**
   * Add the given class reference to the plugin reference.
   * @param classRef
   */
  public void addClassRef(ClassRef classRef) {
    if (classRefs == null)
      classRefs = new HashMap();
    classRefs.put(classRef.getName(), classRef);
  }

  /**
   * Remove the associated class reference for the given class name.
   * @param className
   */
  public void removeClassRef(String className) {
    if (classRefs != null)
      classRefs.remove(className);
  }

  /**
   * @return String plugin id
   */
  public String getId() {
    return id;
  }

  /**
   * Set the plugin id
   * @param String id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return String override toString()
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("<plugin id=\""); //$NON-NLS-1$
    sb.append(getId());
    sb.append("\">"); //$NON-NLS-1$
    for (Iterator it = getClassRefs().iterator(); it.hasNext();)
      sb.append(it.next().toString());
    sb.append("</plugin>"); //$NON-NLS-1$
    return sb.toString();
  }
}