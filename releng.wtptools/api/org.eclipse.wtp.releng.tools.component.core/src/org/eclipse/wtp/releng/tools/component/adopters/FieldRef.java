/*******************************************************************************
 * Copyright (c) 2006, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.adopters;

/**
 * This encapsulates a field reference sub type of NamedRef.
 */
public class FieldRef extends NamedRef {
  
  /**
   * Override the toString method to return information specific to field references
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("<field name=\""); //$NON-NLS-1$
    sb.append(getName());
    sb.append("\" desc=\""); //$NON-NLS-1$
    sb.append(getDescriptor());
    sb.append("\" ref=\""); //$NON-NLS-1$
    sb.append(getRefCount());
    sb.append("\"/>"); //$NON-NLS-1$
    return sb.toString();
  }
}