/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api.progress;

import org.eclipse.wtp.releng.tools.component.internal.ComponentEntry;

public class APIProgressEntry extends ComponentEntry
{
  private String overviewDoc;

  public String getOverviewDoc()
  {
    return overviewDoc;
  }

  public void setOverviewDoc(String overviewDoc)
  {
    this.overviewDoc = overviewDoc;
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<component ");
    sb.append(toAttribute("name", getCompName()));
    sb.append(toAttribute("ref", getRef()));
    if (overviewDoc != null)
      sb.append(toAttribute("overviewDoc", overviewDoc));
    sb.append("/>");
    return sb.toString();
  }
}