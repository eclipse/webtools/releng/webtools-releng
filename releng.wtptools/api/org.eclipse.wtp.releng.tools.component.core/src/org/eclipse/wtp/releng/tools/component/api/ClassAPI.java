/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.wtp.releng.tools.component.internal.Type;

public class ClassAPI extends Type
{
  private String superClass;
  private List interfaces;
  private int access;
  private Map methodAPIs;
  private Map fieldAPIs;
  private JavadocCoverage javadocCoverage;

  public ClassAPI()
  {
    access = -1;
  }

  public String getSuperClass()
  {
    return superClass;
  }

  public void setSuperClass(String superClass)
  {
    this.superClass = superClass;
  }

  /**
   * @return Returns the access.
   */
  public int getAccess()
  {
    return access;
  }

  /**
   * @param access The access to set.
   */
  public void setAccess(int access)
  {
    this.access = access;
  }

  public void addInterface(String interfaz)
  {
    if (interfaces == null)
      interfaces = new ArrayList();
    interfaces.add(interfaz);
  }

  public Collection getInterfaces()
  {
    if (interfaces == null)
      return new ArrayList(0);
    else
      return new ArrayList(interfaces);
  }

  /**
   * @return Returns the fieldAPIs.
   */
  public Collection getFieldAPIs()
  {
    if (fieldAPIs == null)
      fieldAPIs = new HashMap(1);
    return fieldAPIs.values();
  }

  public FieldAPI getFieldAPI(String name)
  {
    if (fieldAPIs == null)
      return null;
    else
      return (FieldAPI)fieldAPIs.get(name);
  }

  public void addFieldAPI(FieldAPI fieldAPI)
  {
    if (fieldAPIs == null)
      fieldAPIs = new HashMap(1);
    fieldAPIs.put(fieldAPI.getName(), fieldAPI);
  }

  public void removeFieldAPI(FieldAPI fieldAPI)
  {
    fieldAPIs.remove(fieldAPI.getName());
  }

  public int sizeFieldAPIs()
  {
    return fieldAPIs != null ? fieldAPIs.size() : 0;
  }

  /**
   * @return Returns the methodAPIs.
   */
  public Collection getMethodAPIs()
  {
    if (methodAPIs == null)
      methodAPIs = new HashMap(1);
    return methodAPIs.values();
  }

  public MethodAPI getMethodAPI(String name, String descriptor)
  {
    if (methodAPIs == null)
      return null;
    StringBuffer key = new StringBuffer();
    key.append(name);
    key.append(" "); //$NON-NLS-1$
    key.append(descriptor);
    MethodAPI result = (MethodAPI)methodAPIs.get(key.toString());
    if (result != null)
    	return result;
    if (name.indexOf("<")>-1) //$NON-NLS-1$
    	return getMethodAPI(name.replaceFirst("<", "&lt;"),descriptor); //$NON-NLS-1$ //$NON-NLS-2$
    return null;
  }

  public MethodAPI getMethodAPI(String name, List inputs, String returnType)
  {
    for (Iterator it = methodAPIs.values().iterator(); it.hasNext();)
    {
      MethodAPI methodAPI = (MethodAPI)it.next();
      if (name.equals(methodAPI.getName()))
      {
        if (match(inputs, methodAPI.getInputs()))
        {
          if (methodAPI.getReturn().endsWith(returnType))
          {
            return methodAPI;
          }
        }
      }
    }
    return null;
  }

  private boolean match(List list1, List list2)
  {
    if (list1.size() == list2.size())
    {
      for (int i = 0; i < list1.size(); i++)
      {
        String x = (String)list1.get(0);
        String y = (String)list2.get(0);
        if (!y.endsWith(x))
        {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  public void addMethodAPI(MethodAPI methodAPI)
  {
    if (methodAPIs == null)
      methodAPIs = new HashMap(1);
    StringBuffer key = new StringBuffer();
    key.append(methodAPI.getName());
    key.append(" ");
    key.append(methodAPI.getDescriptor());
    methodAPIs.put(key.toString(), methodAPI);
  }

  public void removeMethodAPI(MethodAPI methodAPI)
  {
    StringBuffer key = new StringBuffer();
    key.append(methodAPI.getName());
    key.append(" ");
    key.append(methodAPI.getDescriptor());
    methodAPIs.remove(key.toString());
  }

  public int sizeMethodAPIs()
  {
    return methodAPIs != null ? methodAPIs.size() : 0;
  }

  public JavadocCoverage getJavadocCoverage()
  {
    if (javadocCoverage == null)
      javadocCoverage = new JavadocCoverage();
    return javadocCoverage;
  }

  public void setJavadocCoverage(JavadocCoverage javadocCoverage)
  {
    this.javadocCoverage = javadocCoverage;
  }

  public boolean isSetJavadocCoverage()
  {
    return javadocCoverage != null;
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<class-api");
    sb.append(toAttribute("name", getName()));
    sb.append(toAttribute("super", getSuperClass()));
    if (interfaces != null)
    {
      StringBuffer interfacesAttr = new StringBuffer();
      Iterator it = interfaces.iterator();
      boolean hasNext = it.hasNext();
      while (hasNext)
      {
        interfacesAttr.append((String)it.next());
        hasNext = it.hasNext();
        if (hasNext)
        {
          interfacesAttr.append(" ");
        }
      }
      sb.append(toAttribute("interfaces", interfacesAttr.toString()));
    }
    int access = getAccess();
    if (access != -1)
      sb.append(toAttribute("access", String.valueOf(access)));
    if (getReference() != null)
      sb.append(toAttribute("reference", String.valueOf(isReference())));
    if (getImplement() != null)
      sb.append(toAttribute("implement", String.valueOf(isImplement())));
    if (getSubclass() != null)
      sb.append(toAttribute("subclass", String.valueOf(isSubclass())));
    if (getInstantiate() != null)
      sb.append(toAttribute("instantiate", String.valueOf(isInstantiate())));
    sb.append(">");
    if (javadocCoverage != null)
      sb.append(javadocCoverage.toString());
    if (sizeMethodAPIs() > 0)
      for (Iterator it = getMethodAPIs().iterator(); it.hasNext();)
        sb.append(((MethodAPI)it.next()).toString());
    if (sizeFieldAPIs() > 0)
      for (Iterator it = getFieldAPIs().iterator(); it.hasNext();)
        sb.append(((FieldAPI)it.next()).toString());
    sb.append("</class-api>");
    return sb.toString();
  }
}