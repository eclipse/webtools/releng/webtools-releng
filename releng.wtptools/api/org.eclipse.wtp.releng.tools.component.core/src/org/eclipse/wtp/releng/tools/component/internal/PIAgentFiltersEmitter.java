/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.wtp.releng.tools.component.CommandOptionParser;
import org.eclipse.wtp.releng.tools.component.IClazz;
import org.eclipse.wtp.releng.tools.component.IClazzVisitor;
import org.eclipse.wtp.releng.tools.component.IPluginXML;

public class PIAgentFiltersEmitter extends AbstractEmitter
{
  List eclipseDirs;
  List compXMLDirs;
  String outputFile;
  Map id2Plugin;
  Map id2Fragment;
  List includes;
  List packages;

  public PIAgentFiltersEmitter(List eclipseDirs, List compXMLDirs, String outputFile)
  {
    this.eclipseDirs = eclipseDirs;
    this.compXMLDirs = compXMLDirs;
    this.outputFile = outputFile;
    this.packages = new ArrayList();
    this.id2Plugin = new HashMap();
    this.id2Fragment = new HashMap();
    for (Iterator it = eclipseDirs.iterator(); it.hasNext();)
    {
      File eclipseFile = new File(addTrailingSeperator((String)it.next()));
      if (eclipseFile.exists())
        harvestPlugins(eclipseFile, id2Plugin, id2Fragment);
    }
    linkPluginsAndFragments(id2Plugin, id2Fragment);
  }

  public void setIncludes(List includes)
  {
    this.includes = includes;
  }

  private void genFilters()
  {
    for (Iterator it = compXMLDirs.iterator(); it.hasNext();)
    {
      File file = new File(addTrailingSeperator((String)it.next()));
      if (file.exists())
      {
        getPackages(file);
      }
    }
    BufferedWriter bw = null;
    try
    {
      File file = new File(outputFile);
      file.getParentFile().mkdirs();
      bw = new BufferedWriter(new FileWriter(file));
      for (Iterator it = packages.iterator(); it.hasNext();)
      {
        bw.write((String)it.next());
        bw.write(".* * INCLUDE");
        bw.newLine();
      }
      bw.write("* * EXCLUDE");
      bw.newLine();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    finally
    {
      try
      {
        if (bw != null)
        {
          bw.close();
        }
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
  }

  private void getPackages(File file)
  {
    if (file.isDirectory())
    {
      File[] files = file.listFiles();
      for (int i = 0; i < files.length; i++)
        getPackages(files[i]);
    }
    else if (ComponentXML.CONST_COMPONENT_XML.equalsIgnoreCase(file.getName()))
    {
      try
      {
        ComponentXML compXML = new ComponentXML();
        compXML.setLocation(new FileLocation(file));
        compXML.load();
        for (Iterator it = compXML.getPackages().iterator(); it.hasNext();)
        {
          Package pkg = (Package)it.next();
          String pkgName = pkg.getName();
          if (includePackage(pkgName))
          {
            packages.add(pkgName);
          }
        }
        for (Iterator it = compXML.getPlugins().iterator(); it.hasNext();)
        {
          String pluginId = ((Plugin)it.next()).getId();
          IPluginXML plugin = (IPluginXML)id2Plugin.get(pluginId);
          if (plugin != null)
          {
            plugin.accept
            (
              new IClazzVisitor()
              {
                public boolean visit(IClazz clazz)
                {
                  String className = clazz.getName();
                  int dot = className.lastIndexOf(".");
                  if (dot != -1)
                  {
                    String classPkg = className.substring(0, dot);
                    if (!packages.contains(classPkg))
                    {
                      String[] interfaces = clazz.getInterfaces();
                      for (int i = 0; i < interfaces.length; i++)
                      {
                        dot = interfaces[i].lastIndexOf(".");
                        if (dot != -1)
                        {
                          String interfacePkg = interfaces[i].substring(0, dot);
                          if (packages.contains(interfacePkg))
                          {
                            packages.add(classPkg);
                            return true;
                          }
                        }
                      }
                    }
                  }
                  return true;
                }
              }
            );
          }
        }
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
  }

  private boolean includePackage(String pkg)
  {
    if (includes != null)
    {
      for (Iterator it = includes.iterator(); it.hasNext();)
      {
        if (pkg.startsWith((String)it.next()))
        {
          return true;
        }
      }
      return false;
    }
    return true;
  }

  public static void main(String[] args)
  {
    CommandOptionParser optionParser = new CommandOptionParser(args);
    Map options = optionParser.getOptions();
    List eclipseDirs = (List)options.get("eclipseDirs");
    List compXMLDirs = (List)options.get("compXMLDirs");
    List outputDir = (List)options.get("outputFile");
    List includes = (List)options.get("includes");
    if (eclipseDirs == null || compXMLDirs == null || outputDir == null || eclipseDirs.size() < 1 || compXMLDirs.size() < 1 || outputDir.size() < 1)
    {
      printUsage();
      System.exit(-1);
    }
    PIAgentFiltersEmitter emitter = new PIAgentFiltersEmitter(eclipseDirs, compXMLDirs, (String)outputDir.get(0));
    emitter.setIncludes(includes);
    emitter.genFilters();
  }

  private static void printUsage()
  {
    System.out.println("Usage: java org.eclipse.wtp.releng.tools.component.piagent.PIAgentFilterEmitter -compXMLDirs <compXMLDirs> -outputFile <outputFile>");
    System.out.println("");
    System.out.println("\t-eclipseDirs\t<eclipseDirs>\tspace seperated list of directories containing Eclipse plugins");
    System.out.println("\t-compXMLDirs\t<compXMLDirs>\tspace seperated list of directories containing component.xml");
    System.out.println("\t-outputFile\t<outputFile>\toutput PIAgent filters file");
    System.out.println("");
    System.out.println("\t-includes\t<includes>\tpackages to include");
  }
}