/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.wtp.releng.tools.component.IFragmentXML;
import org.eclipse.wtp.releng.tools.component.ILocation;

/**
 * A <code>Fragment</code> is a model object. Fragments are children of
 * plugins. Although they can contain libraries, these libraries must be viewed
 * as part of the parent plugin's list of libraries.
 */
public class FragmentXML extends PluginXML implements IFragmentXML
{
  public static final String CONST_FRAGMENT_XML = "fragment.xml";
  private String fragmentName;
  private PluginXML plugin;
  private String pluginName;
  private String pluginVersion;

  /**
   * Creates a new <code>Fragment</code> from the configuration file at the
   * given location.
   * 
   * @see org.eclipse.api.internalreference.Plugin#Plugin(org.eclipse.api.location.ILocation)
   */
  public FragmentXML(ILocation location)
  {
    super(location);
  }

  /**
   * Always answers an empty list.
   * 
   * @see org.eclipse.api.internalreference.Plugin#getLibraries()
   */
  public List getLibraries()
  {
    return new ArrayList(0);
  }

  /**
   * Answers the parent plugin of this fragment
   * 
   * @return Plugin the parent plugin of this fragment
   */
  public PluginXML getPlugin()
  {
    return plugin;
  }

  /**
   * Attempts to locate the containing plugin for this fragment.
   */
  public void link(Map namesToPlugins)
  {
    plugin = (PluginXML)namesToPlugins.get(getPluginUniqueIdentifier());
    if (plugin == null)
    {
      /*
       * TODO: Remove assumption that there is only one plugin with the given
       * name.
       */
      for (Iterator i = namesToPlugins.values().iterator(); i.hasNext();)
      {
        PluginXML plugin = (PluginXML)i.next();
        if (getName().equals(plugin.getName()))
        {
          setPlugin(plugin);
          return;
        }
      }
      System.err.println("Could not find plugin: " + getName());
    }
    else
    {
      setPlugin(plugin);
      return;
    }
  }

  /**
   * Sets the plugin for this fragment, and registers this fragments libraries
   * with the plugin.
   * 
   * @param plugin
   *          this fragments plugin
   */
  private void setPlugin(PluginXML plugin)
  {
    this.plugin = plugin;
    plugin.addFragment(this);
    for (Iterator i = libraries.iterator(); i.hasNext();)
    {
      Library library = (Library)i.next();
      plugin.addLibrary(library);
    }
  }

  /**
   * Answers the unique identifier of the plugin which contains this fragment.
   * 
   * @return String the unique identifier of the containing plugin, not
   *         <code>null</code>
   */
  public String getPluginUniqueIdentifier()
  {
    return getPluginName() + '_' + getPluginVersion();
  }

  /**
   * Answers the name of the plugin which contains this fragment.
   * 
   * @return String the name of the containing plugin, not <code>null</code>
   */
  public String getPluginName()
  {
    return pluginName;
  }

  /**
   * Answers the version of the plugin which contains this fragment.
   * 
   * @return String the version of the containing plugin, not <code>null</code>
   */
  public String getPluginVersion()
  {
    return pluginVersion;
  }

  /**
   * Sets the name of the plugin which contains this fragment.
   * 
   * @param pluginName
   *          the name of the containing plugin, not <code>null</code>
   */
  public void setPluginName(String pluginName)
  {
    this.pluginName = pluginName;
  }

  /**
   * Sets the version of the plugin which contains this fragment.
   * 
   * @param pluginVersion
   *          the version of the containing plugin, not <code>null</code>
   */
  public void setPluginVersion(String pluginVersion)
  {
    this.pluginVersion = pluginVersion;
  }

  public String getFragmentName()
  {
    return fragmentName;
  }

  public void setFragmentName(String fragmentName)
  {
    this.fragmentName = fragmentName;
  }

  public ILocation getLocation()
  {
    return location;
  }
}
