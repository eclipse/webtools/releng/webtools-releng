/*******************************************************************************
 * Copyright (c) 2006, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.adopters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * The ClassRef class manages a class reference.  It holds the number of field references
 * and method references as well as tracks the implementors, subclasses, and instantiators
 * of the class.  It is a sub type of NamedRef.
 */
public class ClassRef extends NamedRef {
  // Instance variables for various reference counts
  private int implementCount;
  private int subclassCount;
  private int instantiateCount;
  private Map methodRefs;
  private Map fieldRefs;

  /**
   * @return int number of implementors
   */
  public int getImplementCount() {
    return implementCount;
  }
  
  /**
   * Set the implementor count
   * @param implementCount
   */
  public void setImplementCount(int implementCount) {
    this.implementCount = implementCount;
  }
  
  /**
   * Increment the implementor count by 1
   */
  public void incImplementCount() {
    this.implementCount++;
  }
  
  /**
   * @return int instantiator count
   */
  public int getInstantiateCount() {
    return instantiateCount;
  }
  
  /**
   * Set the instantiator count
   * @param int instantiateCount
   */
  public void setInstantiateCount(int instantiateCount) {
    this.instantiateCount = instantiateCount;
  }
  
  /**
   * Increment the instantiator count by 1.
   */
  public void incInstantiateCount() {
    this.instantiateCount++;
  }
  
  /**
   * @return int subclass count
   */
  public int getSubclassCount() {
    return subclassCount;
  }
  
  /**
   * Set the subclass count.
   * @param int subclassCount
   */
  public void setSubclassCount(int subclassCount) {
    this.subclassCount = subclassCount;
  }
  
  /**
   * Increment the subclass count by 1.
   */
  public void incSubclassCount() {
    this.subclassCount++;
  }

  /**
   * @return the related method references for this class
   */
  public Collection getMethodRefs() {
    if (methodRefs != null)
      return new ArrayList(methodRefs.values());
    return Collections.EMPTY_LIST;
  }

  /**
   * @param name
   * @param descriptor
   * @return MethodRef for the given name and descriptor info
   */
  public MethodRef getMethodRef(String name, String descriptor) {
    if (methodRefs != null)
      return (MethodRef)methodRefs.get(encode(name, descriptor));
    return null;
  }

  /**
   * Add a method reference to the class reference.
   * @param methodRef
   */
  public void addMethodRef(MethodRef methodRef) {
    if (methodRefs == null)
      methodRefs = new HashMap();
    methodRefs.put(encode(methodRef.getName(), methodRef.getDescriptor()), methodRef);
  }

  /**
   * Remove method ref with given name and descriptor info
   * @param name
   * @param descriptor
   */
  public void removeMethodRef (String name, String descriptor) {
    if (methodRefs != null)
      methodRefs.remove(encode(name, descriptor));
  }

  /**
   * Get the field references for this class reference.
   * @return Collection
   */
  public Collection getFieldRefs() {
    if (fieldRefs != null)
      return new ArrayList(fieldRefs.values());
    return Collections.EMPTY_LIST;
  }

  /**
   * @param name
   * @param descriptor
   * @return FieldRef
   */
  public FieldRef getFieldRef(String name, String descriptor) {
    if (fieldRefs != null)
      return (FieldRef)fieldRefs.get(encode(name, descriptor));
    return null;
  }

  /**
   * Add the FieldRef to the Class Reference.
   * @param fieldRef
   */
  public void addFieldRef(FieldRef fieldRef) {
    if (fieldRefs == null)
      fieldRefs = new HashMap();
    fieldRefs.put(encode(fieldRef.getName(), fieldRef.getDescriptor()), fieldRef);
  }

  /**
   * Remove associated field reference.
   * @param name
   * @param descriptor
   */
  public void removeFieldRef(String name, String descriptor) {
    if (fieldRefs != null)
      fieldRefs.remove(encode(name, descriptor));
  }
  
  /**
   * Encode the given name and descriptor into a string
   * @param name
   * @param descriptor
   * @return encoded String
   */
  private String encode(String name, String descriptor) {
    StringBuffer sb = new StringBuffer();
    sb.append(name);
    sb.append('#');
    sb.append(descriptor);
    return sb.toString();
  }

  /**
   * @return String toString override
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("<class name=\""); //$NON-NLS-1$
    sb.append(getName());
    sb.append("\" ref=\""); //$NON-NLS-1$
    sb.append(getRefCount());
    sb.append("\" impl=\""); //$NON-NLS-1$
    sb.append(getImplementCount());
    sb.append("\" subclass=\""); //$NON-NLS-1$
    sb.append(getSubclassCount());
    sb.append("\" instantiate=\""); //$NON-NLS-1$
    sb.append(getInstantiateCount());
    sb.append("\">"); //$NON-NLS-1$
    for (Iterator it = getMethodRefs().iterator(); it.hasNext();)
      sb.append(it.next().toString());
    for (Iterator it = getFieldRefs().iterator(); it.hasNext();)
      sb.append(it.next().toString());
    sb.append("</class>"); //$NON-NLS-1$
    return sb.toString();
  }
}