/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

public class Plugin
{
  private String id;
  private Boolean fragment;

  /**
   * @return Returns the fragment.
   */
  public boolean isFragment()
  {
    if (fragment == null)
      return false;
    else
      return fragment.booleanValue();
  }

  public Boolean getFragment()
  {
    return fragment;
  }

  /**
   * @param fragment The fragment to set.
   */
  public void setFragment(Boolean fragment)
  {
    this.fragment = fragment;
  }

  /**
   * @return Returns the id.
   */
  public String getId()
  {
    return id;
  }

  /**
   * @param id The id to set.
   */
  public void setId(String id)
  {
    this.id = id;
  }

  public Object clone()
  {
    Plugin clone = new Plugin();
    clone.setId(getId());
    clone.setFragment(getFragment());
    return clone;
  }
}