/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api.progress;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.internal.ComponentSummary;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class APIProgressSummary extends ComponentSummary
{
  private List pluginsWithoutComp;
  private List missingPlugins;
  private List dupPlugins;

  public APIProgressSummary()
  {
    super();
  }

  public void addPluginsWithoutComp(List pluginIds)
  {
    if (pluginsWithoutComp == null)
      pluginsWithoutComp = new ArrayList();
    pluginsWithoutComp.addAll(pluginIds);
  }

  public void addMissingPlugins(List pluginIds)
  {
    if (missingPlugins == null)
      missingPlugins = new ArrayList();
    missingPlugins.addAll(pluginIds);
  }

  public void addDupPlugins(List pluginIds)
  {
    if (dupPlugins == null)
      dupPlugins = new ArrayList();
    dupPlugins.addAll(pluginIds);
  }

  public void load(ILocation location) throws ParserConfigurationException, SAXException, IOException
  {
    SAXParserFactory factory = SAXParserFactory.newInstance();
    factory.setNamespaceAware(false);
    factory.setValidating(false);
    SAXParser parser = factory.newSAXParser();
    parser.parse(new InputSource(new BufferedInputStream(location.getInputStream())), new APIProgressSummaryHandler(this));
  }

  public String toString(String rootTagName)
  {
    StringBuffer sb = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    sb.append("<");
    sb.append(rootTagName);
    sb.append(">");
    for (Iterator it = getEntries().iterator(); it.hasNext();)
      sb.append(it.next().toString());
    if (pluginsWithoutComp != null)
      for (Iterator it = pluginsWithoutComp.iterator(); it.hasNext();)
        sb.append("<plugin-without-comp").append(toAttribute("id", it.next().toString())).append("/>");
    if (missingPlugins != null)
      for (Iterator it = missingPlugins.iterator(); it.hasNext();)
        sb.append("<missing-plugin").append(toAttribute("id", it.next().toString())).append("/>");
    if (dupPlugins != null)
      for (Iterator it = dupPlugins.iterator(); it.hasNext();)
        sb.append("<dup-plugin").append(toAttribute("id", it.next().toString())).append("/>");
    sb.append("</");
    sb.append(rootTagName);
    sb.append(">");
    return sb.toString();
  }

  private String toAttribute(String key, String value)
  {
    StringBuffer sb = new StringBuffer();
    if (key != null && value != null)
    {
      sb.append(" ");
      sb.append(key);
      sb.append("=\"");
      sb.append(value);
      sb.append("\"");
    }
    return sb.toString();
  }

  private class APIProgressSummaryHandler extends DefaultHandler
  {
    private APIProgressSummary summary;
    
    public APIProgressSummaryHandler(APIProgressSummary summary)
    {
      this.summary = summary;
    }

    public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException
    {
      final String COMPONENT = "component";
      final String COMPONENT_SUMMARY = "component-summary";
      if (COMPONENT.equals(elementName) || COMPONENT.equals(qName))
      {
        String name = attributes.getValue("name");
        String ref = attributes.getValue("ref");
        if (name != null && ref != null)
        {
          APIProgressEntry entry = new APIProgressEntry();
          entry.setCompName(name);
          entry.setRef(ref);
          summary.add(entry);
        }
      }
      else if (COMPONENT_SUMMARY.equals(elementName) || COMPONENT_SUMMARY.equals(qName))
      {
        String timestamp = attributes.getValue("timestamp");
        summary.setTimestamp(timestamp);
      }
    }
  }
}