/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ComponentDepends
{
  private Boolean unrestricted;
  private Map componentRefs;

  /**
   * @return Returns the componentRefs.
   */
  public Collection getComponentRefs()
  {
    if (componentRefs == null)
      componentRefs = new HashMap(1);
    return componentRefs.values();
  }
  
  public void addComponentRef(ComponentRef compRef)
  {
    if (componentRefs == null)
      componentRefs = new HashMap(1);
    componentRefs.put(compRef.getName(), compRef);
  }

  /**
   * @return Returns the unrestricted.
   */
  public boolean isUnrestricted()
  {
    if (unrestricted == null)
      return false;
    else
      return unrestricted.booleanValue();
  }

  public Boolean getUnrestricted()
  {
    return unrestricted;
  }

  /**
   * @param unrestricted The unrestricted to set.
   */
  public void setUnrestricted(Boolean unrestricted)
  {
    this.unrestricted = unrestricted;
  }

  public Object clone()
  {
    ComponentDepends clone = new ComponentDepends();
    clone.setUnrestricted(getUnrestricted());
    for (Iterator it = getComponentRefs().iterator(); it.hasNext();)
      clone.addComponentRef((ComponentRef)((ComponentRef)it.next()).clone());
    return clone;
  }
}