<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<h2>Component API Javadoc Coverage Summary</h2>
				<table border="1">
					<tr>
						<th><h3><b>Component name</b></h3></th>
						<th><h3><b>Incomplete class javadoc</b></h3></th>
						<th><h3><b>Incomplete method javadoc</b></h3></th>
						<th><h3><b>Incomplete field javadoc</b></h3></th>
					</tr>
					<xsl:for-each select="component-api-javadoc-summary/component-api-javadoc">
						<xsl:sort select="@name"/>
						<tr>
							<td><a href="{@ref}"><xsl:value-of select="@name"/></a></td>
							<td><xsl:value-of select="@class-api-count - @class-javadoc-count"/></td>
							<td><xsl:value-of select="@method-api-count - @method-javadoc-count"/></td>
							<td><xsl:value-of select="@field-api-count - @field-javadoc-count"/></td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
