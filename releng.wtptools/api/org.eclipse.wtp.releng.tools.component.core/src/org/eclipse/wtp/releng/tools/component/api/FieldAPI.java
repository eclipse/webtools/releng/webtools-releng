/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import org.eclipse.wtp.releng.tools.component.internal.ComponentObject;

public class FieldAPI extends ComponentObject
{
  private String name;
  private int access;
  private String descriptor;
  private JavadocCoverage javadocCoverage;

  public FieldAPI()
  {
    access = -1;
  }

  /**
   * @return Returns the access.
   */
  public int getAccess()
  {
    return access;
  }

  /**
   * @param access The access to set.
   */
  public void setAccess(int access)
  {
    this.access = access;
  }

  /**
   * @return Returns the descriptor.
   */
  public String getDescriptor()
  {
    return descriptor;
  }

  /**
   * @param descriptor The descriptor to set.
   */
  public void setDescriptor(String descriptor)
  {
    this.descriptor = descriptor;
  }

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    this.name = name;
  }

  public JavadocCoverage getJavadocCoverage()
  {
    if (javadocCoverage == null)
      javadocCoverage = new JavadocCoverage();
    return javadocCoverage;
  }

  public void setJavadocCoverage(JavadocCoverage javadocCoverage)
  {
    this.javadocCoverage = javadocCoverage;
  }

  public boolean isSetJavadocCoverage()
  {
    return javadocCoverage != null;
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<field-api");
    sb.append(toAttribute("name", getName()));
    sb.append(toAttribute("descriptor", getDescriptor()));
    int access = getAccess();
    if (access != -1)
      sb.append(toAttribute("access", String.valueOf(access)));
    sb.append(">");
    if (javadocCoverage != null)
      sb.append(javadocCoverage.toString());
    sb.append("</field-api>");
    return sb.toString();
  }
}