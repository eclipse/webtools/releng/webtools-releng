/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ComponentUse extends ComponentAPI
{
  public static final String CONST_COMPONENT_USE_XML = "component-use.xml";
  public static final String CONST_COMPONENT_USE_HTML = "component-use.html";

  private static final String ELEMENT_COMPONENT_USE = "component-use";
  private static final String ELEMENT_SOURCE = "source";
  private static final String ELEMENT_CLASS_USE = "class-use";
  private static final String ELEMENT_METHOD_USE = "method-use";
  private static final String ELEMENT_FIELD_USE = "field-use";

  private static final String ATTR_ACCESS = "access";
  private static final String ATTR_LINES = "lines";
  private static final String ATTR_NAME = "name";
  private static final String ATTR_DESCRIPTOR = "descriptor";
  private static final String ATTR_REFERENCE = "reference";
  private static final String ATTR_IMPLEMENT = "implement";
  private static final String ATTR_SUBCLASS = "subclass";
  private static final String ATTR_INSTANTIATE = "instantiate";
  private static final String ATTR_THROWS = "throws";

  private List sources;

  /**
   * @return Returns the sources.
   */
  public List getSources()
  {
    if (sources == null)
      sources = new ArrayList(1);
    return sources;
  }

  public void addSource(Source source)
  {
    if (sources == null)
      sources = new ArrayList(1);
    sources.add(source);
  }

  public void load() throws IOException, FileNotFoundException
  {
    try
    {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      factory.setNamespaceAware(false);
      factory.setValidating(false);
      SAXParser parser = factory.newSAXParser();
      parser.parse(new InputSource(new BufferedInputStream(location.getInputStream())), new ComponentUseHandler(this));
    }
    catch (ParserConfigurationException pce)
    {
      pce.printStackTrace();
    }
    catch (SAXException saxe)
    {
      saxe.printStackTrace();
    }
  }

  public void saveAsHTML(ILocation html) throws TransformerConfigurationException, TransformerException, IOException
  {
    TransformerFactory factory = TransformerFactory.newInstance();
    Transformer transformer = factory.newTransformer(new StreamSource(ClassLoader.getSystemResourceAsStream("org/eclipse/wtp/releng/tools/component/xsl/component-violation.xsl")));
    transformer.transform(new StreamSource(new ByteArrayInputStream(getBytes())), new StreamResult(new FileOutputStream(new File(html.getAbsolutePath()))));
  }

  public void save() throws IOException
  {
    if (location != null)
    {
      File file = new File(location.getAbsolutePath());
      file.getParentFile().mkdirs();
      FileOutputStream fos = new FileOutputStream(file);
      fos.write(getBytes());
      fos.close();
    }
  }

  public String toString()
  {
    try
    {
      return new String(getBytes());
    }
    catch (UnsupportedEncodingException e)
    {
      e.printStackTrace();
    }
    return super.toString();
  }

  private byte[] getBytes() throws UnsupportedEncodingException
  {
    StringBuffer sb = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    sb.append("<component-use ");
    sb.append(toAttribute(ATTR_NAME, getName()));
    sb.append(">");
    for (Iterator it = getSources().iterator(); it.hasNext();)
      saveSource(sb, (Source)it.next());
    sb.append("</component-use>");
    return sb.toString().getBytes("UTF-8");
  }

  private void saveSource(StringBuffer sb, Source source)
  {
    sb.append("<source");
    sb.append(toAttribute(ATTR_NAME, source.getName()));
    sb.append(">");
    for (Iterator it = source.getClassUses().iterator(); it.hasNext();)
      saveClassUse(sb, (ClassUse)it.next());
    sb.append("</source>");
  }

  private void saveClassUse(StringBuffer sb, ClassUse classUse)
  {
    sb.append("<class-use");
    sb.append(toAttribute(ATTR_NAME, classUse.getName()));
    if (classUse.sizeLines() > 0)
      sb.append(toAttribute(ATTR_LINES, classUse.getLines(), " "));
    int access = classUse.getAccess();
    if (access != -1)
      sb.append(toAttribute(ATTR_ACCESS, String.valueOf(access)));
    if (classUse.getReference() != null)
      sb.append(toAttribute(ATTR_REFERENCE, String.valueOf(classUse.isReference())));
    if (classUse.getImplement() != null)
      sb.append(toAttribute(ATTR_IMPLEMENT, String.valueOf(classUse.isImplement())));
    if (classUse.getSubclass() != null)
      sb.append(toAttribute(ATTR_SUBCLASS, String.valueOf(classUse.isSubclass())));
    if (classUse.getInstantiate() != null)
      sb.append(toAttribute(ATTR_INSTANTIATE, String.valueOf(classUse.isInstantiate())));
    sb.append(">");
    if (classUse.sizeMethodUses() > 0)
      for (Iterator it = classUse.getMethodUses().iterator(); it.hasNext();)
        saveMethodUse(sb, (MethodUse)it.next());
    if (classUse.sizeFieldUses() > 0)
      for (Iterator it = classUse.getFieldUses().iterator(); it.hasNext();)
        saveFieldUse(sb, (FieldUse)it.next());
    sb.append("</class-use>");
  }

  protected void saveMethodUse(StringBuffer sb, MethodUse methodUse)
  {
    sb.append("<method-use");
    sb.append(toAttribute(ATTR_NAME, methodUse.getName()));
    sb.append(toAttribute(ATTR_DESCRIPTOR, methodUse.getDescriptor()));
    int access = methodUse.getAccess();
    if (access != -1)
      sb.append(toAttribute(ATTR_ACCESS, String.valueOf(access)));
    if (methodUse.sizeThrows() > 0)
      sb.append(toAttribute(ATTR_THROWS, methodUse.getThrows(), " "));
    if (methodUse.sizeLines() > 0)
      sb.append(toAttribute(ATTR_LINES, methodUse.getLines(), " "));
    sb.append("/>");
  }

  protected void saveFieldUse(StringBuffer sb, FieldUse fieldUse)
  {
    sb.append("<field-use");
    sb.append(toAttribute(ATTR_NAME, fieldUse.getName()));
    sb.append(toAttribute(ATTR_DESCRIPTOR, fieldUse.getDescriptor()));
    int access = fieldUse.getAccess();
    if (access != -1)
      sb.append(toAttribute(ATTR_ACCESS, String.valueOf(access)));
    if (fieldUse.sizeLines() > 0)
      sb.append(toAttribute(ATTR_LINES, fieldUse.getLines(), " "));
    sb.append("/>");
  }

  protected static class ComponentUseHandler extends ComponentAPIHandler
  {
    private ComponentUse compUse;
    private Source source;
    private ClassUse classUse;
    
    public ComponentUseHandler(ComponentUse compUse)
    {
      super(compUse);
      this.compUse = compUse;
    }

    public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException
    {
      if (elementName.equals(ELEMENT_SOURCE) || qName.equals(ELEMENT_SOURCE))
      {
        source = new Source();
        source.setName(attributes.getValue(ATTR_NAME));
        compUse.addSource(source);
      }
      else if (elementName.equals(ELEMENT_CLASS_USE) || qName.equals(ELEMENT_CLASS_USE))
      {
        if (source != null)
        {
          classUse = new ClassUse();
          classUse.setName(attributes.getValue(ATTR_NAME));
          String attrLines = attributes.getValue(ATTR_LINES);
          if (attrLines != null)
            classUse.addLines(toCollection(attrLines, " "));
          String attrAccess = attributes.getValue(ATTR_ACCESS);
          if (attrAccess != null)
            classUse.setAccess(Integer.parseInt(attrAccess));
          String attrRef = attributes.getValue(ATTR_REFERENCE);
          if (attrRef != null)
            classUse.setReference(Boolean.valueOf(attrRef));
          String attrImpl = attributes.getValue(ATTR_IMPLEMENT);
          if (attrImpl != null)
            classUse.setImplement(Boolean.valueOf(attrImpl));
          String attrSubclass = attributes.getValue(ATTR_SUBCLASS);
          if (attrSubclass != null)
            classUse.setSubclass(Boolean.valueOf(attrSubclass));
          String attrInstantiate = attributes.getValue(ATTR_INSTANTIATE);
          if (attrInstantiate != null)
            classUse.setInstantiate(Boolean.valueOf(attrInstantiate));
          source.addClassUse(classUse);
        }
      }
      else if (elementName.equals(ELEMENT_METHOD_USE) || qName.equals(ELEMENT_METHOD_USE))
      {
        MethodUse methodUse = new MethodUse();
        startMethod(classUse, methodUse, attributes);
        String attrLines = attributes.getValue(ATTR_LINES);
        if (attrLines != null)
          methodUse.addLines(toCollection(attrLines, " "));
      }
      else if (elementName.equals(ELEMENT_FIELD_USE) || qName.equals(ELEMENT_FIELD_USE))
      {
        FieldUse fieldUse = new FieldUse();
        startField(classUse, fieldUse, attributes);
        String attrLines = attributes.getValue(ATTR_LINES);
        if (attrLines != null)
          fieldUse.addLines(toCollection(attrLines, " "));
      }
      else if (elementName.equals(ELEMENT_COMPONENT_USE) || qName.equals(ELEMENT_COMPONENT_USE))
      {
        compUse.setName(attributes.getValue(ATTR_NAME));
      }
    }
  }
}
