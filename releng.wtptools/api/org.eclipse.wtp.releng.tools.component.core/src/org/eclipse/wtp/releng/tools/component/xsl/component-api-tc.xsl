<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<h2><xsl:value-of select="component-api/@name"/></h2>
				<xsl:for-each select="//class-api">
					<xsl:sort select="@name"/>
					<h3><xsl:value-of select="@name"/></h3>
					<table>
						<xsl:for-each select="method-api">
							<tr>
								<td>
									<xsl:choose>
										<xsl:when test="test-coverage/test">
											<img src="../OK.gif"/>
										</xsl:when>
										<xsl:otherwise>
											<img src="../FAIL.gif"/>
										</xsl:otherwise>
									</xsl:choose>
									&#160;<xsl:value-of select="@name"/>&#160;&#160;<i><xsl:value-of select="@descriptor"/></i>
									<xsl:if test="test-coverage/test">
										<ol>
											<xsl:for-each select="test-coverage/test">
												<li><xsl:value-of select="@name"/></li>
											</xsl:for-each>
										</ol>
									</xsl:if>
								</td>
							</tr>
						</xsl:for-each>
					</table>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
