<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" encoding="ISO-8859-1" indent="yes" />

	<xsl:template match="/">
		<api-info-summary>
			<xsl:for-each select="root/api-info">
				<xsl:sort select="@file"/>
				<xsl:variable name="report" select="document(@file)"/>
				<xsl:apply-templates select="$report/component-api"/>
			</xsl:for-each>
			<xsl:call-template name="total">
				<xsl:with-param name="root" select="root"/>
				<xsl:with-param name="total-class-count" select="0"/>
				<xsl:with-param name="total-method-count" select="0"/>
				<xsl:with-param name="total-field-count" select="0"/>
				<xsl:with-param name="total-method-tested" select="0"/>
				<xsl:with-param name="total-class-javadoced" select="0"/>
				<xsl:with-param name="total-method-javadoced" select="0"/>
				<xsl:with-param name="total-field-javadoced" select="0"/>
				<xsl:with-param name="i" select="1"/>
			</xsl:call-template>
		</api-info-summary>
	</xsl:template>

	<xsl:template match="component-api">
		<xsl:variable name="class-count" select="count(package-api/class-api)"/>
		<xsl:variable name="method-count" select="count(package-api/class-api/method-api)"/>
		<xsl:variable name="field-count" select="count(package-api/class-api/field-api)"/>
		<api-info
			name="{@name}"
			class-count="{$class-count}"
			method-count="{$method-count}"
			field-count="{$field-count}"
			method-tested="{count(package-api/class-api/method-api/test-coverage)}"
			class-javadoced="{$class-count - count(package-api/class-api/javadoc-coverage)}"
			method-javadoced="{$method-count - count(package-api/class-api/method-api/javadoc-coverage)}"
			field-javadoced="{$field-count - count(package-api/class-api/field-api/javadoc-coverage)}"/>
	</xsl:template>

	<xsl:template name="total">
		<xsl:param name="root"/>
		<xsl:param name="total-class-count"/>
		<xsl:param name="total-method-count"/>
		<xsl:param name="total-field-count"/>
		<xsl:param name="total-method-tested"/>
		<xsl:param name="total-class-javadoced"/>
		<xsl:param name="total-method-javadoced"/>
		<xsl:param name="total-field-javadoced"/>
		<xsl:param name="i"/>
		<xsl:if test="$root/api-info[$i]">
			<xsl:variable name="report" select="document($root/api-info[$i]/@file)"/>
			<xsl:variable name="class-count" select="count($report/component-api/package-api/class-api)"/>
			<xsl:variable name="method-count" select="count($report/component-api/package-api/class-api/method-api)"/>
			<xsl:variable name="field-count" select="count($report/component-api/package-api/class-api/field-api)"/>
			<xsl:variable name="method-tested" select="count($report/component-api/package-api/class-api/method-api/test-coverage)"/>
			<xsl:variable name="class-javadoced" select="$class-count - count($report/component-api/package-api/class-api/javadoc-coverage)"/>
			<xsl:variable name="method-javadoced" select="$method-count - count($report/component-api/package-api/class-api/method-api/javadoc-coverage)"/>
			<xsl:variable name="field-javadoced" select="$field-count - count($report/component-api/package-api/class-api/field-api/javadoc-coverage)"/>
			<xsl:choose>
				<xsl:when test="$root/api-info[$i + 1]">
					<xsl:call-template name="total">
						<xsl:with-param name="root" select="$root"/>
						<xsl:with-param name="total-class-count" select="$total-class-count + $class-count"/>
						<xsl:with-param name="total-method-count" select="$total-method-count + $method-count"/>
						<xsl:with-param name="total-field-count" select="$total-field-count + $field-count"/>
						<xsl:with-param name="total-method-tested" select="$total-method-tested + $method-tested"/>
						<xsl:with-param name="total-class-javadoced" select="$total-class-javadoced + $class-javadoced"/>
						<xsl:with-param name="total-method-javadoced" select="$total-method-javadoced + $method-javadoced"/>
						<xsl:with-param name="total-field-javadoced" select="$total-field-javadoced + $field-javadoced"/>
						<xsl:with-param name="i" select="$i + 1"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<api-info
						name="total"
						class-count="{$total-class-count + $class-count}"
						method-count="{$total-method-count + $method-count}"
						field-count="{$total-field-count + $field-count}"
						method-tested="{$total-method-tested + $method-tested}"
						class-javadoced="{$total-class-javadoced + $class-javadoced}"
						method-javadoced="{$total-method-javadoced + $method-javadoced}"
						field-javadoced="{$total-field-javadoced + $field-javadoced}"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>