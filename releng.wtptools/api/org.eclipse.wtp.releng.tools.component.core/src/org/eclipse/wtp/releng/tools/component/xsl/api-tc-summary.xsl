<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<script language="javascript">
				function sort(col)
				{
					var table = document.getElementById("data");
					for (var i = 2; i &lt; table.rows.length; i++)
					{
						for (var j = i - 1; j &gt; 0; j--)
						{
							var x = parseInt(table.rows[i].cells[col].childNodes[0].data);
							var y = parseInt(table.rows[j].cells[col].childNodes[0].data);
							if (x &lt; y)
							{
								move(table, i, j + 1);
								break;
							}
							if (j == 1)
							{
								move(table, i, 1);
								break;
							}
						}
					}
				}
				function move(table, src, target)
				{
					var a = document.createElement("a");
					var td_name = document.createElement("td");
					var td_tested = document.createElement("td");
					var td_tested_per = document.createElement("td");
					var td_not_tested = document.createElement("td");
					var td_not_tested_per = document.createElement("td");

					a.href = table.rows[src].cells[0].childNodes[0].href;
					a.appendChild(document.createTextNode(table.rows[src].cells[0].childNodes[0].childNodes[0].data));
					td_name.appendChild(a);
					td_tested.appendChild(document.createTextNode(table.rows[src].cells[1].childNodes[0].data));
					td_tested_per.appendChild(document.createTextNode(table.rows[src].cells[2].childNodes[0].data));
					td_not_tested.appendChild(document.createTextNode(table.rows[src].cells[3].childNodes[0].data));
					td_not_tested_per.appendChild(document.createTextNode(table.rows[src].cells[4].childNodes[0].data));

					table.deleteRow(src);
					var tr = table.insertRow(target);
					tr.appendChild(td_name);
					tr.appendChild(td_tested);
					tr.appendChild(td_tested_per);
					tr.appendChild(td_not_tested);
					tr.appendChild(td_not_tested_per);
				}
			</script>
			<body>
				<xsl:variable name="title" select="system-property('title')"/>
				<table border="0" cellpadding="2" cellspacing="5" width="100%">
					<tr>
						<td align="left" width="60%">
							<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">
								<xsl:choose>
									<xsl:when test="string-length($title) &gt; 0">
										<xsl:value-of select="$title"/>
									</xsl:when>
									<xsl:otherwise>
										API Test Coverage Summary
									</xsl:otherwise>
								</xsl:choose>
							</font>
						</td>
						<td width="40%">
							<img src="Idea.jpg" align="middle" height="86" hspace="50" width="120"/>
						</td>
					</tr>
				</table>
				<table border="0" cellpadding="2" cellspacing="5" width="100%">
					<tr>
						<td ALIGN="LEFT" VALIGN="TOP" COLSPAN="2" BGCOLOR="#0080C0"><b><font color="#FFFFFF" face="Arial,Helvetica">
							<xsl:choose>
								<xsl:when test="string-length($title) &gt; 0">
									<xsl:value-of select="$title"/>
								</xsl:when>
								<xsl:otherwise>
									API test coverage summary
								</xsl:otherwise>
							</xsl:choose>
						</font></b></td>
					</tr>
				</table>
				<br/>
				<p>
				Sort by:
				<select name="sortby" onchange="javascript:sort(this.selectedIndex)">
					<option value="1">Name</option>
					<option value="2">Methods tested</option>
					<option value="3">% tested</option>
					<option value="4">Methods not tested</option>
					<option value="5">% not tested</option>
				</select>
				</p>
				<table id="data" border="1" width="80%">
					<tr>
						<th width="50%">Name</th>
						<th><img src="OK.gif"/>Methods tested</th>
						<th>&#160;%&#160;</th>
						<th><img src="FAIL.gif"/>Methods not tested</th>
						<th>&#160;%&#160;</th>
					</tr>
					<xsl:for-each select="root/api-info">
						<xsl:sort select="@file"/>
						<xsl:variable name="report" select="document(@file)"/>
						<xsl:variable name="total" select="count($report/component-api/package-api/class-api/method-api)"/>
						<xsl:variable name="tested" select="count($report/component-api/package-api/class-api/method-api/test-coverage)"/>
						<xsl:variable name="untested" select="$total - $tested"/>
						<xsl:if test="system-property('includeAllTC') = 'true' or $untested &gt; 0">
							<tr>
								<td><a href="{concat(substring(@file, 0, string-length(@file) - 11), 'api-tc.html')}"><xsl:value-of select="substring(@file, 0, string-length(@file) - 12)"/></a></td>
								<td><xsl:value-of select="$tested"/></td>
								<td><xsl:value-of select="round($tested div $total * 100)"/></td>
								<td><xsl:value-of select="$untested"/></td>
								<td><xsl:value-of select="round($untested div $total * 100)"/></td>
							</tr>
						</xsl:if>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
