<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<script language="javascript">
				var showComps = false;
				var showPkgs = false;
				function twistComponents()
				{
					var divs = document.getElementsByTagName("div");
					var imgs = document.getElementsByTagName("img");
					for (var i = 0; i &lt; divs.length; i++)
					{
						if (divs[i].id != null &amp;&amp; divs[i].id.indexOf("comp::") != -1)
						{
							if (showComps)
							{
								divs[i].style.display = "";
							}
							else
							{
								divs[i].style.display = "none";
							}
						}
					}
					for (var i = 0; i &lt; imgs.length; i++)
					{
						if (imgs[i].id != null &amp;&amp; imgs[i].id.indexOf("comp::") != -1)
						{
							if (showComps)
							{
								imgs[i].src = "twistopened.gif";
							}
							else
							{
								imgs[i].src = "twistclosed.gif";
							}
						}
					}
					showComps = !showComps;
				}
				function twistPackages()
				{
					var divs = document.getElementsByTagName("div");
					var imgs = document.getElementsByTagName("img");
					for (var i = 0; i &lt; divs.length; i++)
					{
						if (divs[i].id != null &amp;&amp; divs[i].id.indexOf("comp::") != -1)
						{
							divs[i].style.display = "";
						}
						else if (divs[i].id != null &amp;&amp; divs[i].id.indexOf("pkg::") != -1)
						{
							if (showPkgs)
							{
								divs[i].style.display = "";
							}
							else
							{
								divs[i].style.display = "none";
							}
						}
					}
					for (var i = 0; i &lt; imgs.length; i++)
					{
						if (imgs[i].id != null &amp;&amp; imgs[i].id.indexOf("comp::") != -1)
						{
							imgs[i].src = "twistopened.gif";
						}
						else if (imgs[i].id != null &amp;&amp; imgs[i].id.indexOf("pkg::") != -1)
						{
							if (showPkgs)
							{
								imgs[i].src = "twistopened.gif";
							}
							else
							{
								imgs[i].src = "twistclosed.gif";
							}
						}
					}
					showPkgs = !showPkgs
					showComps = false;
				}
				function twist(id)
				{
					var div = document.getElementById(id);
					var img = document.getElementById(id + "::img");
					if (div.style.display == "")
					{
						div.style.display = "none";
						img.src = "twistclosed.gif";
					}
					else
					{
						div.style.display = "";
						img.src = "twistopened.gif";
					}
				}
				<xsl:for-each select="root/api-info">
					<xsl:variable name="doc" select="document(@file)"/>
					<xsl:for-each select="$doc/component-api/package-api/class-api">
						<xsl:variable name="fullname" select="concat(translate(../@name, '.', '_'), '_', @name)"/>
						function <xsl:value-of select="$fullname"/>()
						{
							var oNewDoc = document.open("text/html", "target=_blank");
							var sMarkup = "&lt;html&gt;&lt;body&gt;&lt;font face=Arial,Helvetica&gt;";
							sMarkup += "&lt;table border=0 width=100%&gt;&lt;tr&gt;&lt;td bgcolor=#0080C0&gt;&lt;b&gt;&lt;font color=#FFFFFF face=Arial,Helvetica&gt;Method APIs&lt;/font&gt;&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;br/&gt;";
							sMarkup += "&lt;table cellpadding=5&gt;";
							<xsl:for-each select="method-api">
								<xsl:sort select="@name"/>
								sMarkup += "&lt;tr&gt;&lt;td&gt;";
								sMarkup += "<xsl:value-of select="@name"/>";
								sMarkup += " ";
								sMarkup += "<xsl:value-of select="@descriptor"/>";
								sMarkup += "&lt;/td&gt;&lt;/tr&gt;";
							</xsl:for-each>
							sMarkup += "&lt;table&gt;";
							sMarkup += "&lt;br/&gt;";
							sMarkup += "&lt;table border=0 width=100%&gt;&lt;tr&gt;&lt;td bgcolor=#0080C0&gt;&lt;b&gt;&lt;font color=#FFFFFF face=Arial,Helvetica&gt;Field APIs&lt;/font&gt;&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;br/&gt;";
							sMarkup += "&lt;table cellpadding=5&gt;";
							<xsl:for-each select="field-api">
								<xsl:sort select="@name"/>
								sMarkup += "&lt;tr&gt;&lt;td&gt;";
								sMarkup += "<xsl:value-of select="@name"/>";
								sMarkup += " ";
								sMarkup += "<xsl:value-of select="@descriptor"/>";
								sMarkup += "&lt;/td&gt;&lt;/tr&gt;";
							</xsl:for-each>
							sMarkup += "&lt;table&gt;";
							sMarkup += "&lt;/font&gt;&lt;/body&gt;&lt;/html&gt;";
							oNewDoc.write(sMarkup);
							oNewDoc.close();
						}
					</xsl:for-each>
				</xsl:for-each>
			</script>
			<body>
				<table border="0" cellpadding="2" cellspacing="5" width="100%">
					<tr>
						<td align="left" width="60%">
							<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">API Report</font>
						</td>
						<td width="40%">
							<img src="Idea.jpg" align="middle" height="86" hspace="50" width="120"/>
						</td>
					</tr>
				</table>
				<table border="0" cellpadding="2" cellspacing="5" width="100%">
					<tr>
						<td ALIGN="LEFT" VALIGN="TOP" COLSPAN="2" BGCOLOR="#0080C0"><b><font color="#FFFFFF" face="Arial,Helvetica">APIs</font></b></td>
					</tr>
				</table>
				<font face="Arial,Helvetica">
					<table border="0">
						<tr>
							<td><a href="javascript:twistComponents()">Show/Hide components</a></td>
						</tr>
						<tr>
							<td><a href="javascript:twistPackages()">Show/Hide packages</a></td>
						</tr>
					</table>
					<xsl:for-each select="root/api-info">
						<xsl:sort select="@file"/>
						<xsl:variable name="doc" select="document(@file)"/>
						<p>
							<xsl:apply-templates select="$doc/component-api"/>
						</p>
					</xsl:for-each>
				</font>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="component-api">
		<img src="twistopened.gif" id="comp::{@name}::img" onclick="javascript:twist('comp::{@name}')"/>
		Component <xsl:value-of select="@name"/>
		<br/>
		<div id="comp::{@name}">
		<xsl:for-each select="package-api">
			<xsl:sort select="@name"/>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
		</div>
	</xsl:template>

	<xsl:template match="package-api">
		<img src="space.gif"/>
		<img src="twistopened.gif" id="pkg::{@name}::img" onclick="javascript:twist('pkg::{@name}')"/>
		Package <xsl:value-of select="@name"/>
		<br/>
		<div id="pkg::{@name}">
		<xsl:for-each select="class-api">
			<xsl:sort select="@name"/>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
		</div>
	</xsl:template>

	<xsl:template match="class-api">
		<img src="space.gif"/>
		<img src="space.gif"/>
		<xsl:variable name="fullname" select="concat(translate(../@name, '.', '_'), '_', @name)"/>
		<a href="javascript:{$fullname}()"><xsl:value-of select="@name"/></a>
		<br/>
	</xsl:template>

</xsl:stylesheet>
