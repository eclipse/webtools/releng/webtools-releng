/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.internal.ComponentObject;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ComponentAPI extends ComponentObject
{
  public static final String CONST_COMPONENT_API = "component-api.xml";
  private static final String ELEMENT_COMPONENT_API = "component-api";
  private static final String ELEMENT_PACKAGE_API = "package-api";
  private static final String ELEMENT_CLASS_API = "class-api";
  private static final String ELEMENT_METHOD_API = "method-api";
  private static final String ELEMENT_FIELD_API = "field-api";
  private static final String ELEMENT_TEST_COVERAGE = "test-coverage";
  private static final String ELEMENT_TEST = "test";
  private static final String ELEMENT_JAVADOC_COVERAGE = "javadoc-coverage";
  private static final String ELEMENT_PARAM = "param";
  private static final String ELEMENT_THROW = "throw";
  private static final String ATTR_ACCESS = "access";
  private static final String ATTR_NAME = "name";
  private static final String ATTR_SUPER = "super";
  private static final String ATTR_INTERFACES = "interfaces";
  private static final String ATTR_TIMESTAMP = "timestamp";
  private static final String ATTR_DESCRIPTOR = "descriptor";
  private static final String ATTR_REFERENCE = "reference";
  private static final String ATTR_IMPLEMENT = "implement";
  private static final String ATTR_SUBCLASS = "subclass";
  private static final String ATTR_INSTANTIATE = "instantiate";
  private static final String ATTR_THROWS = "throws";
  private static final String ATTR_SINCE = "since";
  private static final String ATTR_RETURN = "return";

  protected ILocation location;
  protected String name;
  protected String timestamp;
  private Map packageAPIs;
  private boolean reload;

  public ComponentAPI()
  {
    reload = false;
  }

  /**
   * @return Returns the location.
   */
  public ILocation getLocation()
  {
    return location;
  }

  /**
   * @param location The location to set.
   */
  public void setLocation(ILocation location)
  {
    this.location = location;
    reload = true;
  }

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    this.name = name;
  }

  public String getTimestamp()
  {
    return timestamp;
  }

  public void setTimestamp(String timestamp)
  {
    this.timestamp = timestamp;
  }

  /**
   * @return Returns the packageAPIs.
   */
  public Collection getPackageAPIs()
  {
    if (packageAPIs == null)
      packageAPIs = new HashMap(1);
    return packageAPIs.values();
  }

  public PackageAPI getPackageAPI(String name)
  {
    if (packageAPIs == null)
      return null;
    else
      return (PackageAPI)packageAPIs.get(name);
  }

  public void addPackageAPI(PackageAPI pkgAPI)
  {
    if (packageAPIs == null)
      packageAPIs = new HashMap(1);
    packageAPIs.put(pkgAPI.getName(), pkgAPI);
  }

  public void load() throws IOException, FileNotFoundException
  {
    if (reload)
    {
      try
      {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(false);
        factory.setValidating(false);
        SAXParser parser = factory.newSAXParser();
        parser.parse(new InputSource(new BufferedInputStream(location.getInputStream())), new ComponentAPIHandler(this));
      }
      catch (ParserConfigurationException pce)
      {
        pce.printStackTrace();
      }
      catch (SAXException saxe)
      {
        saxe.printStackTrace();
      }
      reload = false;
    }
  }

  public void saveAsHTML(ILocation html, String xsl) throws TransformerConfigurationException, TransformerException, IOException
  {
    TransformerFactory factory = TransformerFactory.newInstance();
    Transformer transformer = factory.newTransformer(new StreamSource(ClassLoader.getSystemResourceAsStream(xsl)));
    transformer.transform(new StreamSource(new ByteArrayInputStream(toString().getBytes())), new StreamResult(new FileOutputStream(new File(html.getAbsolutePath()))));
  }

  public void save() throws IOException
  {
    if (location != null)
    {
      File file = new File(location.getAbsolutePath());
      file.getParentFile().mkdirs();
      FileOutputStream fos = new FileOutputStream(file);
      fos.write(toString().getBytes());
      fos.close();
    }
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    sb.append("<component-api ");
    sb.append(toAttribute("name", getName()));
    sb.append(toAttribute("timestamp", getTimestamp()));
    sb.append(">");
    for (Iterator it = getPackageAPIs().iterator(); it.hasNext();)
      sb.append(it.next().toString());
    sb.append("</component-api>");
    return sb.toString();
  }

  protected static class ComponentAPIHandler extends DefaultHandler
  {
    private ComponentAPI compAPI;
    private PackageAPI packageAPI;
    private ClassAPI classAPI;
    private MethodAPI methodAPI;
    private FieldAPI fieldAPI;
    private TestCoverage testCoverage;
    private JavadocCoverage javadocCoverage;
    private boolean classJavadoc;
    private boolean methodJavadoc;
    private boolean fieldJavadoc;
    
    public ComponentAPIHandler(ComponentAPI compAPI)
    {
      this.compAPI = compAPI;
      classJavadoc = true;
      methodJavadoc = false;
      fieldJavadoc = false;
    }

    public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException
    {
      if (elementName.equals(ELEMENT_TEST_COVERAGE) || qName.equals(ELEMENT_TEST_COVERAGE))
      {
        if (methodAPI != null)
        {
          testCoverage = new TestCoverage();
          methodAPI.setTestCoverage(testCoverage);
        }
      }
      else if (elementName.equals(ELEMENT_TEST) || qName.equals(ELEMENT_TEST))
      {
        if (testCoverage != null)
        {
          String name = attributes.getValue(ATTR_NAME);
          if (name != null)
            testCoverage.addTest(name);
        }
      }
      else if (elementName.equals(ELEMENT_JAVADOC_COVERAGE) || qName.equals(ELEMENT_JAVADOC_COVERAGE))
      {
        javadocCoverage = new JavadocCoverage();
        String hasSince = attributes.getValue(ATTR_SINCE);
        if (hasSince != null)
          javadocCoverage.setHasSince(Boolean.valueOf(hasSince));
        String hasReturn = attributes.getValue(ATTR_RETURN);
        if (hasReturn != null)
          javadocCoverage.setHasReturn(Boolean.valueOf(hasReturn));
        if (classJavadoc && classAPI != null)
          classAPI.setJavadocCoverage(javadocCoverage);
        else if (methodJavadoc && methodAPI != null)
          methodAPI.setJavadocCoverage(javadocCoverage);
        else if (fieldJavadoc && fieldAPI != null)
          fieldAPI.setJavadocCoverage(javadocCoverage);
      }
      else if (elementName.equals(ELEMENT_PARAM) || qName.equals(ELEMENT_PARAM))
      {
        if (javadocCoverage != null)
        {
          String name = attributes.getValue(ATTR_NAME);
          if (name != null)
            javadocCoverage.addMissingParam(name);
        }
      }
      else if (elementName.equals(ELEMENT_THROW) || qName.equals(ELEMENT_THROW))
      {
        if (javadocCoverage != null)
        {
          String name = attributes.getValue(ATTR_NAME);
          if (name != null)
            javadocCoverage.addMissingThrow(name);
        }
      }
      else if (elementName.equals(ELEMENT_METHOD_API) || qName.equals(ELEMENT_METHOD_API))
      {
        classJavadoc = false;
        methodJavadoc = true;
        fieldJavadoc = false;
        methodAPI = new MethodAPI();
        startMethod(classAPI, methodAPI, attributes);
      }
      else if (elementName.equals(ELEMENT_FIELD_API) || qName.equals(ELEMENT_FIELD_API))
      {
        classJavadoc = false;
        methodJavadoc = false;
        fieldJavadoc = true;
        fieldAPI = new FieldAPI();
        startField(classAPI, fieldAPI, attributes);
      }
      else if (elementName.equals(ELEMENT_CLASS_API) || qName.equals(ELEMENT_CLASS_API))
      {
        classJavadoc = true;
        methodJavadoc = false;
        fieldJavadoc = false;
        if (packageAPI != null)
        {
          classAPI = new ClassAPI();
          classAPI.setName(attributes.getValue(ATTR_NAME));
          String attrSuper = attributes.getValue(ATTR_SUPER);
          if (attrSuper != null)
            classAPI.setSuperClass(attrSuper);
          String attrInterfaces = attributes.getValue(ATTR_INTERFACES);
          if (attrInterfaces != null)
          {
            StringTokenizer st = new StringTokenizer(attrInterfaces, " ");
            while (st.hasMoreTokens())
            {
              classAPI.addInterface(st.nextToken());
            }
          }
          String attrAccess = attributes.getValue(ATTR_ACCESS);
          if (attrAccess != null)
            classAPI.setAccess(Integer.parseInt(attrAccess));
          String attrRef = attributes.getValue(ATTR_REFERENCE);
          if (attrRef != null)
            classAPI.setReference(Boolean.valueOf(attrRef));
          String attrImpl = attributes.getValue(ATTR_IMPLEMENT);
          if (attrImpl != null)
            classAPI.setImplement(Boolean.valueOf(attrImpl));
          String attrSubclass = attributes.getValue(ATTR_SUBCLASS);
          if (attrSubclass != null)
            classAPI.setSubclass(Boolean.valueOf(attrSubclass));
          String attrInstantiate = attributes.getValue(ATTR_INSTANTIATE);
          if (attrInstantiate != null)
            classAPI.setInstantiate(Boolean.valueOf(attrInstantiate));
          packageAPI.addClassAPI(classAPI);
        }
      }
      else if (elementName.equals(ELEMENT_PACKAGE_API) || qName.equals(ELEMENT_PACKAGE_API))
      {
        packageAPI = new PackageAPI();
        packageAPI.setName(attributes.getValue(ATTR_NAME));
        compAPI.addPackageAPI(packageAPI);
      }
      else if (equalsLocalpart(elementName, ELEMENT_COMPONENT_API) || equalsLocalpart(qName, ELEMENT_COMPONENT_API))
      {
        compAPI.setName(attributes.getValue(ATTR_NAME));
        compAPI.setTimestamp(attributes.getValue(ATTR_TIMESTAMP));
      }
    }

    private boolean equalsLocalpart(String fullname, String localpart)
    {
      int index = fullname.indexOf(':');
      if (index != -1)
        return fullname.substring(index + 1).equals(localpart);
      else
        return fullname.equals(localpart);
    }

    protected void startMethod(ClassAPI cAPI, MethodAPI methodAPI, Attributes attributes) throws SAXException
    {
      if (cAPI != null)
      {
        methodAPI.setName(attributes.getValue(ATTR_NAME));
        methodAPI.setDescriptor(attributes.getValue(ATTR_DESCRIPTOR));
        String attrAccess = attributes.getValue(ATTR_ACCESS);
        if (attrAccess != null)
          methodAPI.setAccess(Integer.parseInt(attrAccess));
        String attrThrows = attributes.getValue(ATTR_THROWS);
        if (attrThrows != null)
          methodAPI.addThrows(toCollection(attrThrows, " "));
        cAPI.addMethodAPI(methodAPI);
      }
    }

    protected Collection toCollection(String value, String delimiter)
    {
      StringTokenizer st = new StringTokenizer(value, delimiter);
      Collection list = new ArrayList(st.countTokens());
      while (st.hasMoreTokens())
        list.add(st.nextToken());
      return list;
    }

    protected void startField(ClassAPI cAPI, FieldAPI fAPI, Attributes attributes) throws SAXException
    {
      if (cAPI != null)
      {
        fAPI.setName(attributes.getValue(ATTR_NAME));
        fAPI.setDescriptor(attributes.getValue(ATTR_DESCRIPTOR));
        String attrAccess = attributes.getValue(ATTR_ACCESS);
        if (attrAccess != null)
          fAPI.setAccess(Integer.parseInt(attrAccess));
        cAPI.addFieldAPI(fAPI);
      }
    }
  }
}