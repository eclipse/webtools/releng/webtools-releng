/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api.progress;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.api.ClassAPI;
import org.eclipse.wtp.releng.tools.component.api.ComponentAPI;
import org.eclipse.wtp.releng.tools.component.api.MethodAPI;
import org.eclipse.wtp.releng.tools.component.api.PackageAPI;
import org.eclipse.wtp.releng.tools.component.internal.ComponentEntry;
import org.eclipse.wtp.releng.tools.component.internal.ComponentSummary;

public class APITestCoverageSummary extends ComponentSummary
{
  private static final String ROOT_TAG_NAME = "component-api-tc-summary";
  private String refFileName;

  public APITestCoverageSummary(String refFileName)
  {
    this.refFileName = refFileName;
  }

  public void add(ComponentAPI compAPI)
  {
    APITestCoverageEntry entry = new APITestCoverageEntry();
    int apiCount = 0;
    int testCoverageCount = 0;
    entry.setCompName(compAPI.getName());
    for (Iterator it = compAPI.getPackageAPIs().iterator(); it.hasNext();)
    {
      PackageAPI pkgAPI = (PackageAPI)it.next();
      for (Iterator classAPIsIt = pkgAPI.getClassAPIs().iterator(); classAPIsIt.hasNext();)
      {
        ClassAPI classAPI = (ClassAPI)classAPIsIt.next();
        Collection methodAPIs = classAPI.getMethodAPIs();
        apiCount += methodAPIs.size();
        for (Iterator methodAPIsIt = methodAPIs.iterator(); methodAPIsIt.hasNext();)
        {
          MethodAPI methodAPI = (MethodAPI)methodAPIsIt.next();
          if (methodAPI.countTestcases() > 0)
            testCoverageCount++;
        }
      }
    }
    entry.setApiCount(apiCount);
    entry.setTestCoverageCount(testCoverageCount);
    String ref = compAPI.getLocation().getAbsolutePath();
    int i = ref.lastIndexOf('/');
    if (i != -1)
      ref = ref.substring(0, i + 1);
    entry.setRef(ref + refFileName);
    add(entry);
  }

  public void saveAsHTML(String xsl, ILocation html) throws TransformerConfigurationException, TransformerException, IOException
  {
    saveAsHTML(html, xsl, ROOT_TAG_NAME);
  }

  public void save(ILocation location) throws IOException
  {
    save(location, ROOT_TAG_NAME);
  }

  private class APITestCoverageEntry extends ComponentEntry
  {
    private int apiCount;
    private int testCoverageCount;

    public APITestCoverageEntry()
    {
      apiCount = 0;
      testCoverageCount = 0;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append("<component-api-tc ");
      sb.append(toAttribute("name", getCompName()));
      sb.append(toAttribute("api-count", String.valueOf(apiCount)));
      sb.append(toAttribute("test-coverage-count", String.valueOf(testCoverageCount)));
      sb.append(toAttribute("missing-coverage-count", String.valueOf(apiCount - testCoverageCount)));
      sb.append(toAttribute("ref", getRef()));
      sb.append("/>");
      return sb.toString();
    }

    /**
     * @return Returns the apiCount.
     */
    public int getApiCount()
    {
      return apiCount;
    }

    /**
     * @param apiCount The apiCount to set.
     */
    public void setApiCount(int apiCount)
    {
      this.apiCount = apiCount;
    }

    /**
     * @return Returns the testCoverageCount.
     */
    public int getTestCoverageCount()
    {
      return testCoverageCount;
    }

    /**
     * @param testCoverageCount The testCoverageCount to set.
     */
    public void setTestCoverageCount(int testCoverageCount)
    {
      this.testCoverageCount = testCoverageCount;
    }
  }
}