/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Package
{
  private String name;
  private Boolean api;
  private Boolean exclusive;
  private Map types;

  /**
   * @return Returns the api.
   */
  public boolean isApi()
  {
    if (api == null)
      return true;
    else
      return api.booleanValue();
  }

  public Boolean getApi()
  {
    return api;
  }

  /**
   * @param api The api to set.
   */
  public void setApi(Boolean api)
  {
    this.api = api;
  }

  /**
   * @return Returns the exclusive.
   */
  public boolean isExclusive()
  {
    if (exclusive == null)
      return true;
    else
      return exclusive.booleanValue();
  }

  public Boolean getExclusive()
  {
    return exclusive;
  }

  /**
   * @param exclusive The exclusive to set.
   */
  public void setExclusive(Boolean exclusive)
  {
    this.exclusive = exclusive;
  }

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    this.name = name;
  }

  /**
   * @return Returns the types.
   */
  public Collection getTypes()
  {
    if (types == null)
      types = new HashMap(1);
    return types.values();
  }

  public Type getType(String name)
  {
    if (types == null)
      return null;
    else
      return (Type)types.get(name);
  }

  public void addType(Type type)
  {
    if (types == null)
      types = new HashMap(1);
    types.put(type.getName(), type);
  }

  public Object clone()
  {
    Package clone = new Package();
    clone.setName(getName());
    clone.setApi(getApi());
    clone.setExclusive(getExclusive());
    for (Iterator it = getTypes().iterator(); it.hasNext();)
      clone.addType((Type)((Type)it.next()).clone());
    return clone;
  }
}