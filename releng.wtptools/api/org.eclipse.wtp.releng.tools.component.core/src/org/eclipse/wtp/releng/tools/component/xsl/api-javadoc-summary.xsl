<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<body>
				<table border="0" cellpadding="2" cellspacing="5" width="100%">
					<tr>
						<td align="left" width="60%">
							<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">API Javadoc Summary</font>
						</td>
						<td width="40%">
							<img src="Idea.jpg" align="middle" height="86" hspace="50" width="120"/>
						</td>
					</tr>
				</table>
				<table border="0" cellpadding="2" cellspacing="5" width="100%">
					<tr>
						<td ALIGN="LEFT" VALIGN="TOP" COLSPAN="2" BGCOLOR="#0080C0"><b><font color="#FFFFFF" face="Arial,Helvetica">API javadoc summary</font></b></td>
					</tr>
				</table>
				<br/>
				<table border="1" width="80%">
					<tr>
						<th>Name</th>
						<th>Missing javadoc count</th>
					</tr>
					<xsl:for-each select="root/api-info">
						<xsl:sort select="@file"/>
						<xsl:variable name="report" select="document(@file)"/>
						<xsl:variable name="count" select="count($report/component-api/package-api/class-api/javadoc-coverage) + count($report/component-api/package-api/class-api/method-api/javadoc-coverage) + count($report/component-api/package-api/class-api/field-api/javadoc-coverage)"/>
						<xsl:if test="$count &gt; 0">
							<tr>
								<td><a href="{concat(substring(@file, 0, string-length(@file) - 11), 'api-javadoc.html')}"><xsl:value-of select="substring(@file, 0, string-length(@file) - 12)"/></a></td>
								<td><xsl:value-of select="$count"/></td>
							</tr>
						</xsl:if>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
