<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<script language="javascript">
				function twist(img)
				{
					var div = document.getElementById(img.id.substring(0, img.id.length - 4));
					if (div.style.display == "")
					{
						div.style.display = "none";
						img.src = "../twistclosed.gif";
					}
					else
					{
						div.style.display = "";
						img.src = "../twistopened.gif";
					}
				}
				var show = false;
				function allTwist()
				{
					var divs = document.getElementsByTagName("div");
					for (var i = 0; i &lt; divs.length; i++)
					{
						if (divs[i].id != null &amp;&amp; divs[i].id.length &gt; 0 &amp;&amp; divs[i].id.indexOf("::ok::") == -1 &amp;&amp; divs[i].id.indexOf("::fail::") == -1)
						{
							var img = document.getElementById(divs[i].id + '.img');
							if (!show)
							{
								divs[i].style.display = "none";
								img.src = "../twistclosed.gif";
							}
							else
							{
								divs[i].style.display = "";
								img.src = "../twistopened.gif";
							}
						}
					}
					var a = document.getElementById("allTwistId");
					if (!show)
						a.childNodes[0].data = "Expand classes";
					else
						a.childNodes[0].data = "Collapse classes";
					show = !show;
				}
				function twistOKAndFail(id, prefix)
				{
					var input = document.getElementById(id);
					var divs = document.getElementsByTagName("div");
					for (var i = 0; i &lt; divs.length; i++)
					{
						if (divs[i].id != null &amp;&amp; divs[i].id.length &gt; 0 &amp;&amp; divs[i].id.indexOf(prefix) != -1)
						{
							if (input.checked)
							{
								divs[i].style.display = "none";
							}
							else
							{
								divs[i].style.display = "";
							}
						}
					}
				}
			</script>
			<body>
				<font face="Arial,Helvetica">
					<xsl:apply-templates select="component-api"/>
				</font>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="component-api">
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td align="left" width="60%">
					<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">API Test Coverage Report for <xsl:value-of select="@name"/></font>
				</td>
				<td width="40%">
					<img src="../Idea.jpg" align="middle" height="86" hspace="50" width="120"/>
				</td>
			</tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td ALIGN="LEFT" VALIGN="TOP" COLSPAN="2" BGCOLOR="#0080C0"><b><font color="#FFFFFF" face="Arial,Helvetica">Test Coverage (methods tested: <xsl:value-of select="count(package-api/class-api/method-api/test-coverage)"/>, methods not tested: <xsl:value-of select="count(package-api/class-api/method-api) - count(package-api/class-api/method-api/test-coverage)"/>)</font></b></td>
			</tr>
		</table>
		<br/>
		<table border="0" cellpadding="2" cellspacing="5" width="75%">
			<tr>
				<td align="left">
					<input id="okId" type="checkbox" onclick="javascript:twistOKAndFail('okId', '::ok::')"/>Hide tested methods
				</td>
				<td align="left">
					<input id="failId" type="checkbox" onclick="javascript:twistOKAndFail('failId', '::fail::')"/>Hide untested methods
				</td>
				<td align="left">
					<a id="allTwistId" href="javascript:allTwist()">Collapse classes</a>
				</td>
			</tr>
		</table>
		<br/>
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<xsl:for-each select="package-api/class-api">
				<xsl:sort select="@name"/>
				<xsl:choose>
					<xsl:when test="(count(method-api/test-coverage) = count(method-api)) or (count(method-api/test-coverage) = 0)">
						<xsl:choose>
							<xsl:when test="method-api/test-coverage/test">
								<tr>
									<td>
										<div id="::ok::class::{@name}">
											<xsl:apply-templates select="."/>
										</div>
									</td>
								</tr>
							</xsl:when>
							<xsl:otherwise>
								<tr>
									<td>
										<div id="::fail::class::{@name}">
											<xsl:apply-templates select="."/>
										</div>
									</td>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<tr>
							<td>
								<xsl:apply-templates select="."/>
							</td>
						</tr>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</table>
	</xsl:template>

	<xsl:template match="class-api">
		<xsl:if test="method-api">
			<xsl:variable name="fullname" select="concat(../@name, '.', @name)"/>
			<img id="source::{@name}.img" src="../twistopened.gif" onclick="javascript:twist(this)"/>
			<xsl:value-of select="$fullname"/>
			<p>
				<div id="source::{@name}">
					<xsl:for-each select="method-api">
						<xsl:sort select="@name"/>
						<xsl:choose>
							<xsl:when test="test-coverage/test">
								<div id="::ok::source::{@name}">
									<xsl:apply-templates select="."/>
								</div>
							</xsl:when>
							<xsl:otherwise>
								<div id="::fail::source::{@name}">
									<xsl:apply-templates select="."/>
								</div>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</div>
			</p>
		</xsl:if>
	</xsl:template>

	<xsl:template match="method-api">
		<img src="../space.gif"/>
		<xsl:choose>
			<xsl:when test="test-coverage/test">
				<img src="../OK.gif"/>
			</xsl:when>
			<xsl:otherwise>
				<img src="../FAIL.gif"/>
			</xsl:otherwise>
		</xsl:choose>
		&#160;<xsl:value-of select="@name"/>&#160;&#160;<i><xsl:value-of select="@descriptor"/></i>
		<xsl:if test="test-coverage/test">
			<ol>
				<xsl:for-each select="test-coverage/test">
					<li>
						<xsl:value-of select="@name"/>
					</li>
				</xsl:for-each>
			</ol>
		</xsl:if>
		<br/>
	</xsl:template>

</xsl:stylesheet>