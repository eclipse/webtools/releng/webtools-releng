<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<h2><xsl:value-of select="component-api/@name"/></h2>
				<xsl:for-each select="//class-api">
					<xsl:sort select="@name"/>
					<xsl:if test="javadoc-coverage | method-api/javadoc-coverage">
						<h3><xsl:value-of select="@name"/></h3>
						<xsl:if test="javadoc-coverage/@since">
							<ul>
								<li><i>missing: @since</i></li>
							</ul>
						</xsl:if>
						<table>
							<xsl:for-each select="method-api">
								<xsl:sort select="@name"/>
								<xsl:if test="javadoc-coverage">
	 								<tr>
										<td><b>Method</b> - <xsl:value-of select="@name"/></td>
									</tr>
									<tr>
										<td>
											<ul>
												<xsl:if test="javadoc-coverage/@doc">
													<li><i>missing javadoc</i></li>
												</xsl:if>
												<xsl:if test="javadoc-coverage/@since">
													<li><i>missing: @since</i></li>
												</xsl:if>
												<xsl:if test="javadoc-coverage/@return">
													<li><i>missing: @return</i></li>
												</xsl:if>
												<xsl:for-each select="javadoc-coverage/param">
													<li><i>missing: @param <xsl:value-of select="javadoc-coverage/param/@name"/></i></li>
												</xsl:for-each>
												<xsl:for-each select="javadoc-coverage/throw">
													<li><i>missing: @throws <xsl:value-of select="javadoc-coverage/throw/@name"/></i></li>
												</xsl:for-each>
											</ul>
										</td>
									</tr>
								</xsl:if>
							</xsl:for-each>
							<xsl:for-each select="field-api">
								<xsl:sort select="@name"/>
								<xsl:if test="javadoc-coverage">
	 								<tr>
										<td><b>Field </b><xsl:value-of select="@name"/> has no javadoc</td>
									</tr>
								</xsl:if>
							</xsl:for-each>
						</table>
					</xsl:if>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
