<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<xsl:for-each select="breakageReport/references">
				    <xsl:apply-templates select="." />
			    </xsl:for-each>
			    <xsl:for-each select="references">
				    <xsl:apply-templates select="." />
			    </xsl:for-each>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="references">
		<p>
		<b>Contact info: <xsl:value-of select="@contactInfo"/></b>
		</p>
		<table border="1">
			<tr>
				<th>Name</th>
				<th>Reference</th>
				<th>Subclass</th>
				<th>Implement</th>
				<th>Instantiate</th>
			</tr>
			<xsl:for-each select="plugin">
				<xsl:sort select="@id"/>
				<xsl:apply-templates select="." />
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template match="plugin">
	    <xsl:choose>
		    <xsl:when test="starts-with(@id,'org.eclipse.wst.rdb.')">
		    </xsl:when>
		    <xsl:otherwise>
				<tr>
					<td><b>Plugin : <xsl:value-of select="@id"/></b></td>
					<td bgcolor="c8c8c8" colspan="4">&#160;</td>
				</tr>
				<xsl:for-each select="class">
					<xsl:sort select="@name"/>
					<xsl:apply-templates select="." />
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="class">
		<tr>
			<td>&#160;&#160;&#160;&#160;<b>Class : <xsl:value-of select="@name"/></b></td>
			<td><xsl:value-of select="@ref"/></td>
			<td><xsl:value-of select="@subclass"/></td>
			<td><xsl:value-of select="@impl"/></td>
			<td><xsl:value-of select="@instantiate"/></td>
		</tr>
		<xsl:for-each select="method">
			<xsl:sort select="@name"/>
			<xsl:apply-templates select="." />
		</xsl:for-each>
		<xsl:for-each select="field">
			<xsl:sort select="@name"/>
			<xsl:apply-templates select="." />
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="method">
		<tr>
			<td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Method : <xsl:value-of select="@name"/>(...)</td>
			<td><xsl:value-of select="@ref"/></td>
			<td bgcolor="c8c8c8" colspan="3">&#160;</td>
		</tr>
	</xsl:template>
	<xsl:template match="field">
		<tr>
			<td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Field : <xsl:value-of select="@name"/></td>
			<td><xsl:value-of select="@ref"/></td>
			<td bgcolor="c8c8c8" colspan="3">&#160;</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>