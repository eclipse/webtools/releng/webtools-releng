/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;


public class ComponentEntry extends ComponentObject
{
  private String compName;
  private String ref;

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<component ");
    sb.append(toAttribute("name", compName));
    sb.append(toAttribute("ref", ref));
    sb.append("/>");
    return sb.toString();
  }

  /**
   * @return Returns the compName.
   */
  public String getCompName()
  {
    return compName;
  }

  /**
   * @param compName
   *          The compName to set.
   */
  public void setCompName(String compName)
  {
    this.compName = compName;
  }

  /**
   * @return Returns the ref.
   */
  public String getRef()
  {
    return ref;
  }

  /**
   * @param ref
   *          The ref to set.
   */
  public void setRef(String ref)
  {
    this.ref = ref;
  }
}