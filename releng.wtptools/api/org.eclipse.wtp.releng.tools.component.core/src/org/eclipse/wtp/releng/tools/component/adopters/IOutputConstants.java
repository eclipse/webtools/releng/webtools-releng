/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.component.adopters;

public interface IOutputConstants {

	public static final String LINE_BREAK = "\n"; //$NON-NLS-1$
	public static final String COMMA = ","; //$NON-NLS-1$
	public static final String HYPHEN = "-"; //$NON-NLS-1$
	
	public static final String XML_ROOT_BEGIN = "<root>"; //$NON-NLS-1$
	public static final String XML_ROOT_END = "</root>" ; //$NON-NLS-1$
	
	public static final String PRINT_SOURCE_LOCATION = "\t-src\t\t<src>\t\tlocation of your usage reports"; //$NON-NLS-1$
	public static final String PRINT_OUTPUT_LOCATION = "\t-output\t<output>\t\tlocation of the output file"; //$NON-NLS-1$
	public static final String PRINT_COMPONENT_XML_API_LOCATION = "\t-api\t\t<api>\t\tlocation of your component.xml"; //$NON-NLS-1$
	public static final String PRINT_USAGE_COMBINED = "Usage: java org.eclipse.wtp.releng.tools.component.adopters.CombineClass2Reference -src <src> -output <output>"; //$NON-NLS-1$
	public static final String PRINT_USAGE_COMBINED_EXT_PT = "Usage: java org.eclipse.wtp.releng.tools.component.adopters.CombineExtensionPointScans -sourceFiles <sourcFiles> -output <output>"; //$NON-NLS-1$
	public static final String PRINT_SOURCE_FILES_LOCATION = "\t-sourceFiles\t\t<sourceFiles>\t\tlocation of your extension point usage reports"; //$NON-NLS-1$
	
	public static final String PRINT_USAGE_EXTENSION_POINT = "Usage: java org.eclipse.wtp.releng.tools.component.adopters.ExtensionPointScanner -extpt -output [options]";//$NON-NLS-1$
	public static final String PRINT_EXTPT = "\t-extpt\t\t<extpt>\t\tregular expressions for filtering your extension points"; //$NON-NLS-1$
	public static final String PRINT_OUTPUT_EXTENSION_POINT = "\t-output\t\t<output>\t\toutput directory"; //$NON-NLS-1$
	public static final String OPTIONS = "where options include:"; //$NON-NLS-1$
	public static final String PRINT_INCLUDES = "\t-includes\t<includes>\tspace seperated extensions to include"; //$NON-NLS-1$
	public static final String PRINT_EXCLUDES = "\t-excludes\t<excludes>\tspace seperated extensions to exclude"; //$NON-NLS-1$
}
