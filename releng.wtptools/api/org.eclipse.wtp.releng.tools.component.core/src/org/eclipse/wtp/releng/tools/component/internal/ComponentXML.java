/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ComponentXML
{
  public static final String CONST_COMPONENT_XML = "component.xml";
  private static final String NS = "http://eclipse.org/wtp/releng/tools/component-model";
  private static final String ELEMENT_COMPONENT = "component";
  private static final String ELEMENT_PLUGIN = "plugin";
  private static final String ELEMENT_PACKAGE = "package";
  private static final String ELEMENT_TYPE = "type";
  private static final String ELEMENT_COMPONENT_DEPENDS = "component-depends";
  private static final String ELEMENT_COMPONENT_REF = "component-ref";
  private static final String ELEMENT_DESCRIPTION = "description";
  private static final String ATTR_XMLNS = "xmlns";
  private static final String ATTR_NAME = "name";
  private static final String ATTR_FRAGMENT = "fragment";
  private static final String ATTR_ID = "id";
  private static final String ATTR_API = "api";
  private static final String ATTR_EXCLUSIVE = "exclusive";
  private static final String ATTR_UNRESTRICTED = "unrestricted";
  private static final String ATTR_REFERENCE = "reference";
  private static final String ATTR_IMPLEMENT = "implement";
  private static final String ATTR_SUBCLASS = "subclass";
  private static final String ATTR_INSTANTIATE = "instantiate";
  private static final String ATTR_URL = "url";

  private ILocation location;
  private boolean loaded;
  private String name;
  private Description description;
  private Map plugins;
  private Map packages;
  private ComponentDepends componentDepends;

  public ComponentXML()
  {
    loaded = false;
  }
  /**
   * @return Returns the location.
   */
  public ILocation getLocation()
  {
    return location;
  }

  /**
   * @param location The location to set.
   */
  public void setLocation(ILocation location)
  {
    this.location = location;
    loaded = false;
  }

  public Description getDescription()
  {
    return description;
  }

  public void setDescription(Description description)
  {
    this.description = description;
  }

  /**
   * @return Returns the componentDepends.
   */
  public ComponentDepends getComponentDepends()
  {
    return componentDepends;
  }

  /**
   * @param componentDepends The componentDepends to set.
   */
  public void setComponentDepends(ComponentDepends componentDepends)
  {
    this.componentDepends = componentDepends;
  }

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    this.name = name;
  }

  /**
   * @return Returns the packages.
   */
  public Collection getPackages()
  {
    if (packages == null)
      packages = new HashMap(1);
    return packages.values();
  }

  public Package getPackage(String name)
  {
    if (packages == null)
      return null;
    else
      return (Package)packages.get(name);
  }

  public void addPackage(Package pkg)
  {
    if (packages == null)
      packages = new HashMap(1);
    packages.put(pkg.getName(), pkg);
  }

  /**
   * @return Returns the plugins.
   */
  public Collection getPlugins()
  {
    if (plugins == null)
      plugins = new HashMap(1);
    return plugins.values();
  }

  public Plugin getPlugin(String id)
  {
    if (plugins == null)
      return null;
    return (Plugin)plugins.get(id);
  }

  public void addPlugin(Plugin plugin)
  {
    if (plugins == null)
      plugins = new HashMap(1);
    plugins.put(plugin.getId(), plugin);
  }

  public void load() throws IOException, FileNotFoundException
  {
    if (!loaded)
    {
      try
      {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(false);
        factory.setValidating(false);
        SAXParser parser = factory.newSAXParser();
        parser.parse(new InputSource(new BufferedInputStream(location.getInputStream())), new ComponentXMLHandler(this));
        loaded = true;
      }
      catch (ParserConfigurationException pce)
      {
        pce.printStackTrace();
      }
      catch (SAXException saxe)
      {
        saxe.printStackTrace();
      }
    }
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    sb.append("<component ");
    saveAttribute(sb, ATTR_XMLNS, NS);
    saveAttribute(sb, ATTR_NAME, getName());
    sb.append(">");
    if (description != null)
      sb.append(description.toString());
    saveComponentDepends(sb, getComponentDepends());
    for (Iterator it = getPlugins().iterator(); it.hasNext();)
      savePlugin(sb, (Plugin)it.next());
    for (Iterator it = getPackages().iterator(); it.hasNext();)
      savePackage(sb, (Package)it.next());
    sb.append("</component>");
    return sb.toString();
  }

  public void save() throws IOException
  {
    if (location != null)
    {
      File file = new File(location.getAbsolutePath());
      file.getParentFile().mkdirs();
      FileOutputStream fos = new FileOutputStream(file);
      fos.write(toString().getBytes("UTF-8"));
      fos.close();
    }
  }

  private void saveComponentDepends(StringBuffer sb, ComponentDepends dep)
  {
    sb.append("<component-depends");
    if (dep.getUnrestricted() != null)
      saveAttribute(sb, ATTR_UNRESTRICTED, String.valueOf(dep.isUnrestricted()));
    sb.append(">");
    for (Iterator it = dep.getComponentRefs().iterator(); it.hasNext();)
      saveComponentRef(sb, (ComponentRef)it.next());
    sb.append("</component-depends>");
  }

  private void saveComponentRef(StringBuffer sb, ComponentRef ref)
  {
    sb.append("<component-ref");
    saveAttribute(sb, ATTR_NAME, ref.getName());
    sb.append("/>");
  }

  private void savePlugin(StringBuffer sb, Plugin plugin)
  {
    sb.append("<plugin");
    saveAttribute(sb, ATTR_ID, plugin.getId());
    if (plugin.getFragment() != null)
      saveAttribute(sb, ATTR_FRAGMENT, String.valueOf(plugin.isFragment()));
    sb.append("/>");
  }

  private void savePackage(StringBuffer sb, Package pkg)
  {
    sb.append("<package");
    saveAttribute(sb, ATTR_NAME, pkg.getName());
    if (pkg.getApi() != null)
      saveAttribute(sb, ATTR_API, String.valueOf(pkg.isApi()));
    if (pkg.getExclusive() != null)
      saveAttribute(sb, ATTR_EXCLUSIVE, String.valueOf(pkg.isExclusive()));
    sb.append(">");
    for (Iterator it = pkg.getTypes().iterator(); it.hasNext();)
      saveType(sb, (Type)it.next());
    sb.append("</package>");
  }

  private void saveType(StringBuffer sb, Type type)
  {
    sb.append("<type");
    saveAttribute(sb, ATTR_NAME, type.getName());
    if (type.getReference() != null)
      saveAttribute(sb, ATTR_REFERENCE, String.valueOf(type.isReference()));
    if (type.getImplement() != null)
      saveAttribute(sb, ATTR_IMPLEMENT, String.valueOf(type.isImplement()));
    if (type.getSubclass() != null)
      saveAttribute(sb, ATTR_SUBCLASS, String.valueOf(type.isSubclass()));
    if (type.getInstantiate() != null)
      saveAttribute(sb, ATTR_INSTANTIATE, String.valueOf(type.isInstantiate()));
    sb.append("/>");
  }

  private void saveAttribute(StringBuffer sb, String key, String value)
  {
    if (key != null && value != null)
    {
      sb.append(" ");
      sb.append(key);
      sb.append("=\"");
      sb.append(value);
      sb.append("\"");
    }
  }

  public Object clone()
  {
    ComponentXML clone = new ComponentXML();
    clone.setName(getName());
    clone.setLocation(getLocation());
    clone.setDescription(getDescription());
    ComponentDepends depends = getComponentDepends();
    if (depends != null)
      clone.setComponentDepends((ComponentDepends)depends.clone());
    if (plugins != null)
      for (Iterator it = getPlugins().iterator(); it.hasNext();)
        clone.addPlugin((Plugin)((Plugin)it.next()).clone());
    if (packages != null)
      for (Iterator it = getPackages().iterator(); it.hasNext();)
        clone.addPackage((Package)((Package)it.next()).clone());
    return clone;
  }

  private static class ComponentXMLHandler extends DefaultHandler
  {
    private ComponentXML compXML;
    private Package pkg;
    
    public ComponentXMLHandler(ComponentXML compXML)
    {
      this.compXML = compXML;
      this.compXML.setComponentDepends(new ComponentDepends());
      this.compXML.setName(null);
      this.compXML.getPlugins().clear();
      this.compXML.getPackages().clear();
    }

    public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException
    {
      if (equalsLocalpart(qName, ELEMENT_PLUGIN) || equalsLocalpart(elementName, ELEMENT_PLUGIN))
      {
        Plugin plugin = new Plugin();
        plugin.setId(attributes.getValue(ATTR_ID));
        String attrFragment = attributes.getValue(ATTR_FRAGMENT);
        if (attrFragment != null)
          plugin.setFragment(Boolean.valueOf(attrFragment));
        compXML.addPlugin(plugin);
      }
      else if (equalsLocalpart(qName, ELEMENT_PACKAGE) || equalsLocalpart(elementName, ELEMENT_PACKAGE))
      {
        pkg = new Package();
        pkg.setName(attributes.getValue(ATTR_NAME));
        String attrApi = attributes.getValue(ATTR_API);
        if (attrApi != null)
          pkg.setApi(Boolean.valueOf(attrApi));
        String attrExclusive = attributes.getValue(ATTR_EXCLUSIVE);
        if (attrExclusive != null)
          pkg.setExclusive(Boolean.valueOf(attrExclusive));
        compXML.addPackage(pkg);
      }
      else if (equalsLocalpart(qName, ELEMENT_TYPE) || equalsLocalpart(elementName, ELEMENT_TYPE))
      {
        if (pkg != null)
        {
          Type type = new Type();
          type.setName(attributes.getValue(ATTR_NAME));
          String attrRef = attributes.getValue(ATTR_REFERENCE);
          if (attrRef != null)
            type.setReference(Boolean.valueOf(attrRef));
          String attrImpl = attributes.getValue(ATTR_IMPLEMENT);
          if (attrImpl != null)
            type.setImplement(Boolean.valueOf(attrImpl));
          String attrSubclass = attributes.getValue(ATTR_SUBCLASS);
          if (attrSubclass != null)
            type.setSubclass(Boolean.valueOf(attrSubclass));
          String attrInstantiate = attributes.getValue(ATTR_INSTANTIATE);
          if (attrInstantiate != null)
            type.setInstantiate(Boolean.valueOf(attrInstantiate));
          pkg.addType(type);
        }
      }
      else if (equalsLocalpart(qName, ELEMENT_COMPONENT) || equalsLocalpart(elementName, ELEMENT_COMPONENT))
      {
        compXML.setName(attributes.getValue(ATTR_NAME));
      }
      else if (equalsLocalpart(qName, ELEMENT_COMPONENT_DEPENDS) || equalsLocalpart(elementName, ELEMENT_COMPONENT_DEPENDS))
      {
        String attrUnrestricted = attributes.getValue(ATTR_UNRESTRICTED);
        if (attrUnrestricted != null)
          compXML.getComponentDepends().setUnrestricted(Boolean.valueOf(attrUnrestricted));
      }
      else if (equalsLocalpart(qName, ELEMENT_COMPONENT_REF) || equalsLocalpart(elementName, ELEMENT_COMPONENT_REF))
      {
        ComponentRef compRef = new ComponentRef();
        compRef.setName(attributes.getValue(ATTR_NAME));
        compXML.getComponentDepends().addComponentRef(compRef);
      }
      else if (equalsLocalpart(qName, ELEMENT_DESCRIPTION) || equalsLocalpart(elementName, ELEMENT_DESCRIPTION))
      {
        Description desc = new Description();
        desc.setUrl(attributes.getValue(ATTR_URL));
        compXML.setDescription(desc);
      }
    }

    private boolean equalsLocalpart(String fullname, String localpart)
    {
      int index = fullname.indexOf(':');
      if (index != -1)
        return fullname.substring(index + 1).equals(localpart);
      else
        return fullname.equals(localpart);
    }
  }
}
