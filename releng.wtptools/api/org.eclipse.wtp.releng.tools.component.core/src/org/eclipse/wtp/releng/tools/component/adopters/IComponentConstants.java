/*******************************************************************************
 * Copyright (c) 2006, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.component.adopters;

public interface IComponentConstants {
	
	public static final String WTP_SCANNING_PLUGIN = "org.eclipse.wtp.releng.tools.component.core"; //$NON-NLS-1$
	
	// ComponentTeam Names
	public static final String COMPONENT_TEAM_JAVA_EE = "Java EE"; //$NON-NLS-1$
	public static final String COMPONENT_TEAM_COMMON = "Common"; //$NON-NLS-1$
	public static final String COMPONENT_TEAM_EDITORS = "XML/JSP"; //$NON-NLS-1$
	public static final String COMPONENT_TEAM_RDB = "RDB"; //$NON-NLS-1$
	public static final String COMPONENT_TEAM_WEB_SERVICES = "Web Services"; //$NON-NLS-1$
	public static final String COMPONENT_TEAM_SERVER = "Server"; //$NON-NLS-1$
	public static final String COMPONENT_TEAM_JEM = "Jem"; //$NON-NLS-1$
	public static final String COMPONENT_TEAM_JSF = "JSF"; //$NON-NLS-1$
	public static final String COMPONENT_TEAM_JPT = "JPT"; //$NON-NLS-1$
	public static final String COMPONENT_TEAM_UNKNOWN = "Unknown"; //$NON-NLS-1$
	
	// General Expressions for plugin name matching
	public static final String PLUGIN_EXPRESSION_J2EE = "org.eclipse..st.j2ee.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_EJB = "org.eclipse..st.ejb.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_SERVLET = "org.eclipse..st.servlet.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_WEB = "org.eclipse..st.web.*"; //$NON-NLS-1$
	
	public static final String PLUGIN_EXPRESSION_COMMAND = "org.eclipse..st.command.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_COMMON = "org.eclipse..st.common.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_VALIDATION = "org.eclipse..st.validation.*"; //$NON-NLS-1$
	
	public static final String PLUGIN_EXPRESSION_JSP = "org.eclipse..st.jsp.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_CSS = "org.eclipse..st.css.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_DTD = "org.eclipse..st.dtd.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_HTML = "org.eclipse..st.html.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_JAVASCRIPT = "org.eclipse..st.javascript.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_SSE = "org.eclipse..st.sse.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_XML = "org.eclipse..st.xml.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_XSD = "org.eclipse..st.xsd.*"; //$NON-NLS-1$
	
	public static final String PLUGIN_EXPRESSION_RDB = "org.eclipse..st.rdb.*"; //$NON-NLS-1$
	
	public static final String PLUGIN_EXPRESSION_WS = "org.eclipse..st.ws.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_WSDL = "org.eclipse..st.wsdl.*"; //$NON-NLS-1$
	
	public static final String PLUGIN_EXPRESSION_SERVER = "org.eclipse..st.server.*"; //$NON-NLS-1$
	public static final String PLUGIN_EXPRESSION_INTERNET = "org.eclipse..st.internet.*"; //$NON-NLS-1$
	
	public static final String PLUGIN_EXPRESSION_JEM = "org.eclipse.jem.*"; //$NON-NLS-1$
	
	public static final String PLUGIN_EXPRESSION_JSF = "org.eclipse.jst.jsf.*"; //$NON-NLS-1$
	
	public static final String PLUGIN_EXPRESSION_JPT = "org.eclipse.jpt.*"; //$NON-NLS-1$
	
}
