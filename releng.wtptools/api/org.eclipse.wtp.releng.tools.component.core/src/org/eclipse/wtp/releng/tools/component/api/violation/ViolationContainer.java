/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api.violation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class ViolationContainer extends Violation
{
  private List violations;

  public void addViolation(Violation violation)
  {
    if (violations == null)
      violations = new ArrayList();
    violations.add(violation);
  }

  public void addAllViolations(List violations)
  {
    if (this.violations == null)
      this.violations = new ArrayList();
    this.violations.addAll(violations);
  }

  public int countViolations()
  {
    if (violations == null)
      return 0;
    else
      return violations.size();
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<");
    sb.append(getViolationName());
    sb.append(toAttribute("name", getName()));
    sb.append(">");
    if (violations != null)
      for (Iterator it = violations.iterator(); it.hasNext();)
        sb.append(it.next().toString());
    sb.append("</");
    sb.append(getViolationName());
    sb.append(">");
    return sb.toString();
  }
}