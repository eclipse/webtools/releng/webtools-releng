/*******************************************************************************
 * Copyright (c) 2002, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component;

import org.eclipse.wtp.releng.tools.component.internal.PluginXML;

public interface IFragmentXML extends IPluginXML
{
  public static final String CONST_FRAGMENT_XML = "fragment.xml"; //$NON-NLS-1$

  /**
   * Answers the parent plugin of this fragment
   * 
   * @return Plugin the parent plugin of this fragment
   */
  public PluginXML getPlugin();

  /**
   * Answers the name of the plugin which contains this fragment.
   * 
   * @return String the name of the containing plugin, not <code>null</code>
   */
  public String getPluginName();

  /**
   * Answers the version of the plugin which contains this fragment.
   * 
   * @return String the version of the containing plugin, not <code>null</code>
   */
  public String getPluginVersion();

  public String getFragmentName();
}
