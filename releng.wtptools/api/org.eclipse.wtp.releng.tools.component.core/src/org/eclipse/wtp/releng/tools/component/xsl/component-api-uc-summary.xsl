<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:use="http://eclipse.org/wtp/releng/tools/component-use">
	<xsl:template match="/">
		<html>
			<body>
				<h2>Component API Use Coverage Summary</h2>
				<table border="1">
					<tr>
						<th><h3><b>Component name</b></h3></th>
						<th><h3><b>API count</b></h3></th>
						<th><h3><b><img src="OK.gif"/>Used API count</b></h3></th>
						<th><h3><b><img src="FAIL.gif"/>Unused API count</b></h3></th>
					</tr>
					<xsl:for-each select="component-api-tc-summary/component-api-tc">
						<xsl:sort select="@name"/>
						<tr>
							<td><a href="{@ref}"><xsl:value-of select="@name"/></a></td>
							<td><xsl:value-of select="@api-count"/></td>
							<td><xsl:value-of select="@test-coverage-count"/></td>
							<td><xsl:value-of select="@missing-coverage-count"/></td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
