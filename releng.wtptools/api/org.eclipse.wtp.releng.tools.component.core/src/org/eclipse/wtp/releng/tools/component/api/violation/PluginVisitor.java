/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api.violation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.wtp.releng.tools.component.IFileLocation;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ILocationChildrenIterator;
import org.eclipse.wtp.releng.tools.component.ILocationVisitor;
import org.eclipse.wtp.releng.tools.component.IZipLocation;
import org.eclipse.wtp.releng.tools.component.internal.Bundle;
import org.eclipse.wtp.releng.tools.component.internal.Location;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class PluginVisitor implements ILocationVisitor
{
  private List pluginIds;

  public PluginVisitor()
  {
    this.pluginIds = new ArrayList();
  }

  public Collection getPluginIds()
  {
    return new ArrayList(pluginIds);
  }

  public boolean accept(ILocation location)
  {
    String locationName = location.getName();
    if (locationName.endsWith("MANIFEST.MF"))
    {
      acceptManifest(location);
    }
    else if (Location.isArchive(locationName))
    {
      return acceptSingleJar(location);
    }
    else if (locationName.endsWith(".classpath"))
    {
      acceptDotClasspath(location);
    }
    if (location instanceof IZipLocation)
      return true;
    else if ((location instanceof IFileLocation) && ((IFileLocation)location).getFile().isDirectory())
      return true;
    else
      return false;
  }

  private void acceptManifest(ILocation location)
  {
    try
    {
      Manifest manifest = new Manifest(location.getInputStream());
      java.util.jar.Attributes attrs = manifest.getMainAttributes();
      String bundleNameAttr = attrs.getValue(new java.util.jar.Attributes.Name(Bundle.CONST_BUNDLE_NAME));
      if (bundleNameAttr != null)
      {
        String bundleName = (new StringTokenizer(bundleNameAttr, ";")).nextToken().trim();
        if (bundleName != null && bundleName.length() > 0 && !pluginIds.contains(bundleName))
          pluginIds.add(bundleName);
      }
    }
    catch (IOException e)
    {
    }
  }

  private boolean acceptSingleJar(ILocation location)
  {
    ILocationChildrenIterator it = location.childIterator();
    boolean pluginAdded = false;
    for (ILocation child = it.next(); child != null; child = it.next())
    {
      String name = child.getName();
      if (name.equalsIgnoreCase("META-INF/MANIFEST.MF"))
      {
        try
        {
          Manifest manifest = new Manifest(child.getInputStream());
          java.util.jar.Attributes attrs = manifest.getMainAttributes();
          String bundleNameAttr = attrs.getValue(new java.util.jar.Attributes.Name(Bundle.CONST_BUNDLE_NAME));
          if (bundleNameAttr != null)
          {
            String bundleName = (new StringTokenizer(bundleNameAttr, ";")).nextToken().trim();
            if (bundleName != null && bundleName.length() > 0 && !pluginIds.contains(bundleName))
            {
              pluginIds.add(bundleName);
              pluginAdded = true;
            }
          }
        }
        catch (IOException e)
        {
        }
      }
    }
    if (!pluginAdded && Location.getExtension(location.getName()).equalsIgnoreCase("jar"))
    {
      try
      {
        JarInputStream jis = new JarInputStream(location.getInputStream());
        Manifest manifest = jis.getManifest();
        if (manifest != null)
        {
          java.util.jar.Attributes attrs = manifest.getMainAttributes();
          String bundleNameAttr = attrs.getValue(new java.util.jar.Attributes.Name(Bundle.CONST_BUNDLE_NAME));
          if (bundleNameAttr != null)
          {
            String bundleName = (new StringTokenizer(bundleNameAttr, ";")).nextToken().trim();
            if (bundleName != null && bundleName.length() > 0 && !pluginIds.contains(bundleName))
            {
              pluginIds.add(bundleName);
              pluginAdded = true;
            }
          }
        }
      }
      catch (IOException ioe)
      {
      }
    }
    return true;
  }

  private void acceptDotClasspath(ILocation location)
  {
    try
    {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document doc = builder.parse(location.getInputStream());
      Element root = doc.getDocumentElement();
      if (root.getTagName().equals("classpath"))
      {
        NodeList cpEntries = root.getElementsByTagName("classpathentry");
        for (int i = 0; i < cpEntries.getLength(); i++)
        {
          Element cpEntry = (Element)cpEntries.item(i);
          String kind = cpEntry.getAttribute("kind");
          if (kind != null && kind.equals("output"))
          {
            String absPath = location.getAbsolutePath().replace('\\', '/');
            int j = absPath.lastIndexOf('/');
            String s = absPath.substring(0, j);
            String id = s.substring(s.lastIndexOf('/') + 1, j);
            if (id != null && id.length() > 0 && !pluginIds.contains(id))
              pluginIds.add(id);
          }
        }
      }
    }
    catch (Throwable e)
    {
    }
  }
}