/*******************************************************************************
 * Copyright (c) 2006, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.adopters;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class BreakageReport
{
  List refs;

  public void addRefs(References ref)
  {
    if (refs == null)
      refs = new ArrayList();
    refs.add(ref);
  }

  public List getRefs()
  {
    if (refs == null)
      return new ArrayList(0);
    else
      return new ArrayList(refs);
  }

  public void load(InputStream is) throws IOException, ParserConfigurationException, SAXException
  {
    byte[] b = new byte[1024];
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    int read = is.read(b);
    while (read != -1)
    {
      baos.write(b, 0, read);
      read = is.read(b);
    }
    is.close();
    String content = baos.toString();
    int start = content.indexOf("<references");
    int end = content.indexOf("</references>");
    int offset = "</references>".length();
    while (start != -1 && end != -1)
    {
      String s = content.substring(start, end + offset);
      References ref = new References();
      ref.load(new ByteArrayInputStream(s.getBytes()));
      addRefs(ref);
      content = content.substring(end + offset);
      start = content.indexOf("<references");
      end = content.indexOf("</references>");
    }
  }

  public void save(OutputStream os) throws IOException
  {
    byte[] content = toString().getBytes();
    os.write(content);
    os.close();
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<breakageReport>");
    for (Iterator it = getRefs().iterator(); it.hasNext();)
      sb.append(it.next().toString());
    sb.append("</breakageReport>");
    return sb.toString();
  }
}