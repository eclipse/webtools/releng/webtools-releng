/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.component.adopters;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IPlatformRunnable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.wtp.releng.tools.component.CommandOptionParser;
import org.eclipse.wtp.releng.tools.component.xsl.XSLUtil;

/**
 * ExtensionPointScanner is a scanning mechanism for extension point usage in an eclipse environment.
 * This class will collect all of the loaded extensions in a the running workbench, and
 * then process the extension point usage based on the command line arguments:
 * 
 * extpt: Regular expressions for filtering which extension point's extensions will be scanned.
 * output: The output directory of the usage files.
 * includes: Optional regular expression for which plugins containing extensions should be included.
 * excludes: Optional regular expression for which plugins containing extensions should be excluded.
 *
 */
public class ExtensionPointScanner extends ComponentTeamScanner implements IPlatformRunnable, IOutputConstants  {
  
  // Command Line arguments	
  public static final String ARG_FILTERS = "extpt"; //$NON-NLS-1$
  public static final String ARG_OUTPUT = "output"; //$NON-NLS-1$
  public static final String ARG_INCLUDES = "includes"; //$NON-NLS-1$
  public static final String ARG_EXCLUDES = "excludes"; //$NON-NLS-1$

  // Instance variables for command line argument values	
  private Collection filters;
  private String output;
  private Collection includes;
  private Collection excludes;
  
  // Class variables for string values
  private static final String EXTENSION_POINT_USAGE_FILE = "org/eclipse/wtp/releng/tools/component/xsl/extpt.xsl"; //$NON-NLS-1$
  private static final String HTML_OUTPUT_EXTENSION = "/extpt.html"; //$NON-NLS-1$
  
  /**
   * Default Constructor
   */
  public ExtensionPointScanner() {
    super();
  }

  /**
   * @return Collection of regular expressions defining which extension points to consider
   */
  public Collection getFilters() {
    return filters;
  }

  /**
   * Set the collection of regular expressions defining which extension points to consider.
   * @param someFilters
   */
  public void setFilters(Collection someFilters) {
    filters = someFilters;
  }

  /**
   * @return String value of the output folder location to write the html file
   */
  public String getOutput() {
    return output;
  }

  /**
   * Set the output folder location to the string value parameter
   * @param anOutput
   */
  public void setOutput(String anOutput) {
    output = anOutput;
  }

  /**
   * @return optional collection of regular expressions defining which plugin extensions to ignore
   */
  public Collection getExcludes() {
    return excludes;
  }

  /**
   * Set the optional collection of regular expressions defining which plugin extensions to ignore.
   * @param someExcludes
   */
  public void setExcludes(Collection someExcludes) {
    excludes = someExcludes;
  }

  /**
   * @return optional collection of regular expressions defining which plugin extensions to include
   */
  public Collection getIncludes() {
    return includes;
  }

  /**
   * Set the optional collection of regular expressions defining which plugin extensions to include.
   * @param someIncludes
   */
  public void setIncludes(Collection someIncludes) {
    includes = someIncludes;
  }

  /**
   * This is a helper method to determine if a plugin extension meets the inclusion and exclusion
   * critera set in the optional command line arguments "includes" and "excludes".
   * 
   * @param s
   * @return boolean should the string pattern be included?
   */
  private boolean include(String s) {
	// If the string matchs any of the excludes, return false  
    if (getExcludes()!=null && !getExcludes().isEmpty()) {
      for (Iterator it = getExcludes().iterator(); it.hasNext();) {
        if (s.matches((String)it.next()))
          return false;
      }
    }
    // If there are includes, make sure the string matchs one of them
    if (includes != null && !includes.isEmpty()) {
      for (Iterator it = includes.iterator(); it.hasNext();) {
        if (s.matches((String)it.next()))
          return true;
      }
      // If there are includes and the string does not match any, return false
      return false;
    }
    // If there are not includes, and the excludes don't match, return true
    return true;
  }

  /**
   * This is the execute method for the extension point scanning which will drive the operation
   * and ouput of the result html file based on command line arguments.  It will load all the
   * known extension points in the eclipse environment and match the references accordingly.
   */
  public void execute() {
	// Get and iterate over all known extension points in the eclipse environment. 
    IExtensionPoint[] extpts = Platform.getExtensionRegistry().getExtensionPoints();
    for (int i = 0; i < extpts.length; i++) {
      String extPtId = extpts[i].getUniqueIdentifier();
      String pluginId = extpts[i].getNamespaceIdentifier();
      if (extPtId != null) {
    	// Iterate over the known filters to see if this extension pt should be scanned for refs  
        for (Iterator it = getFilters().iterator(); it.hasNext();) {
          if (extPtId.matches((String)it.next())) {
            int extptRefCount = 0;
            // Get all extensions for the current extension point
            IExtension[] exts = extpts[i].getExtensions();
            for (int j = 0; j < exts.length; j++) {
              // If the reference should be included, increment the count	
              if (include(exts[j].getNamespaceIdentifier()))
            	  extptRefCount++;
            }
            // Get the associated component team for the extension point's plugin
            ComponentTeam compTeam = getComponentTeam(pluginId);
            // Update the extension point reference counts with the result of this extenion point scan
            compTeam.getExtensionPointReferenceCounts().put(extPtId, String.valueOf(extptRefCount));
            break;
          }
        }
      }
    }
    // Generate the output html file.
    generateHTMLFile();
  }

  /**
   * This method creates an XML formatted stream of the resulting extension point reference data
   * and then transforms it to HTML using an XSL transformation.
   *
   */
  private void generateHTMLFile() {
    try {
      // Get an output stream to write results in XML format
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      baos.write(XML_ROOT_BEGIN.getBytes());
      // For each component team, write out the extension point usage
      for (int i=0; i<getComponentTeams().size(); i++) {
    	  ComponentTeam compTeam = (ComponentTeam) getComponentTeams().get(i);
    	  writeCompTeamXML(compTeam, baos);
      }
      baos.write(XML_ROOT_END.getBytes());
      // Close the XML output stream
      baos.close();
      // Write out the byte array xml to a html file output stream for the extension pt references
      // This transform will do a XSLT operation using the file extpt.xsl
      XSLUtil.transform (
    	Platform.getBundle(WTP_SCANNING_PLUGIN).getResource(EXTENSION_POINT_USAGE_FILE).openStream(),
    	new ByteArrayInputStream(baos.toByteArray()),
        new FileOutputStream(getOutput() + HTML_OUTPUT_EXTENSION)
      );
    } catch (Throwable t) {
      t.printStackTrace();
      throw new RuntimeException(t);
    }
  }

  /**
   * This method will write out, in xml format, the component team's extension point references.
   * 
   * @param compTeam
   * @param baos
   * @throws IOException
   */
  private void writeCompTeamXML(ComponentTeam compTeam, ByteArrayOutputStream baos) throws IOException {
    baos.write("<team lead=\"".getBytes()); //$NON-NLS-1$
    baos.write(compTeam.getTeamName().getBytes());
    baos.write("\">".getBytes()); //$NON-NLS-1$
    // For each extension pt in the component team's scope, write its reference information
    for (Iterator it = compTeam.getExtensionPointReferenceCounts().keySet().iterator(); it.hasNext();) {
      String id = (String)it.next();
      String refCount = (String)compTeam.getExtensionPointReferenceCounts().get(id);
      baos.write("<extpt id=\"".getBytes()); //$NON-NLS-1$
      baos.write(id.getBytes());
      baos.write("\" ref=\"".getBytes()); //$NON-NLS-1$
      baos.write(refCount.getBytes());
      baos.write("\"/>".getBytes()); //$NON-NLS-1$
    }
    baos.write("</team>".getBytes()); //$NON-NLS-1$
  }

  /**
   * Runs this runnable calling main with the given args and returns the result.
   * @param arguments
   */
  public Object run(Object arguments) {
    String vmExtpt = System.getProperty(ARG_FILTERS);
    String vmOutput = System.getProperty(ARG_OUTPUT);
    String vmIncludes = System.getProperty(ARG_INCLUDES);
    String vmExcludes = System.getProperty(ARG_EXCLUDES);
    List args = new ArrayList();
    // extpt is required, add its value to command line argument values
    args.add(HYPHEN+ARG_FILTERS);
    args.addAll(tokenize(vmExtpt));
    // output is required, add its value to command line argument values
    args.add(HYPHEN+ARG_OUTPUT);
    args.add(vmOutput);
    // If includes are set, add the command line argument values
    if (vmIncludes != null) {
      args.add(HYPHEN+ARG_INCLUDES);
      args.addAll(tokenize(vmIncludes));
    }
    // If excludes are set, add the command line argument values
    if (vmExcludes != null) {
      args.add(HYPHEN+ARG_EXCLUDES);
      args.addAll(tokenize(vmExcludes));
    }
    // Run the main method with the current command line arguments
    try {
      main((String[])args.toArray(new String[0]));
    } catch (Throwable t) {
      t.printStackTrace();
    } 
    return IPlatformRunnable.EXIT_OK;
  }

  /**
   * Helper method to tokenize the given string into a list of tokens.
   * 
   * @param s
   * @return List of tokenized Strings
   */
  private List tokenize(String s) {
    StringTokenizer st = new StringTokenizer(s, COMMA);
    List tokens = new ArrayList(st.countTokens());
    while(st.hasMoreTokens()) {
      tokens.add(st.nextToken());
    }
    return tokens;
  }

  /**
   * This is the main method which is invoked when running the run method of this runnable.  It will
   * set up the Scanner instance, set the command line arguments, and call execute to calculate
   * the extension point reference usages based on the given argument values:
   * 
   * extpt: Regular expressions for filtering which extension point's extensions will be scanned.
   * output: The output directory of the usage files.
   * includes: Optional regular expression for which plugins containing extensions should be included.
   * excludes: Optional regular expression for which plugins containing extensions should be excluded.
   * 
   * @param args
   */
  public static void main(String[] args) {
	// Parse the command line options
    CommandOptionParser optionParser = new CommandOptionParser(args);
    Map options = optionParser.getOptions();
    Collection optionsExtpt = (Collection)options.get(ARG_FILTERS);
    Collection optionsOutput = (Collection)options.get(ARG_OUTPUT);
    Collection optionsIncludes = (Collection)options.get(ARG_INCLUDES);
    Collection optionsExcludes = (Collection)options.get(ARG_EXCLUDES);
    // If improper arguments or argument values are specified, prompt for proper usage.
    if (optionsExtpt == null || optionsOutput == null || optionsExtpt.isEmpty() || optionsOutput.isEmpty()) {
      printUsage();
      System.exit(-1);
    }
    // Create a new instance of the scanner class and set the command line arguments
    ExtensionPointScanner scanner = new ExtensionPointScanner();
    scanner.setFilters(optionsExtpt);
    scanner.setOutput((String)optionsOutput.iterator().next());
    scanner.setIncludes(optionsIncludes);
    scanner.setExcludes(optionsExcludes);
    // Invoke the execute method to run the extension point usage scan and output result file
    scanner.execute();
  }

  /**
   * This is a helper method to the user to print out an error message of the proper usage of 
   * the arguments to be passed and the location of the output files.  
   * See IOutputConstants for messages.
   */
  private static void printUsage() {
    System.out.println(PRINT_USAGE_EXTENSION_POINT);
    System.out.println(""); //$NON-NLS-1$
    System.out.println(PRINT_EXTPT);
    System.out.println(PRINT_OUTPUT_EXTENSION_POINT);
    System.out.println(""); //$NON-NLS-1$
    System.out.println(OPTIONS);
    System.out.println(""); //$NON-NLS-1$
    System.out.println(PRINT_INCLUDES);
    System.out.println(PRINT_EXCLUDES);
  }
}