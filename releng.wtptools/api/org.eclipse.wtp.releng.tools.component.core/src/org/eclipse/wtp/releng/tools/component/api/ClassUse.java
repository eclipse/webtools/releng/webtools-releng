/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ClassUse extends ClassAPI
{
  private List lines;

  public ClassUse()
  {
    setReference(Boolean.FALSE);
    setSubclass(Boolean.FALSE);
    setImplement(Boolean.FALSE);
    setInstantiate(Boolean.FALSE);
  }

  /**
   * @return Returns the lines.
   */
  public Collection getLines()
  {
    if (lines == null)
      return new ArrayList(0);
    else
      return new ArrayList(lines);
  }

  public void addLine(int line)
  {
    if (lines == null)
      lines = new ArrayList(1);
    lines.add(String.valueOf(line));
  }

  public void addLines(Collection l)
  {
    if (lines == null)
      lines = new ArrayList(l);
    else
      lines.addAll(l);
  }

  public int sizeLines()
  {
    return lines != null ? lines.size() : 0;
  }

  public Collection getMethodUses()
  {
    return super.getMethodAPIs();
  }

  public int sizeMethodUses()
  {
    return super.sizeMethodAPIs();
  }

  public Collection getFieldUses()
  {
    return super.getFieldAPIs();
  }

  public int sizeFieldUses()
  {
    return super.sizeFieldAPIs();
  }
}
