/*******************************************************************************
 * Copyright (c) 2002, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.io.File;
import org.eclipse.wtp.releng.tools.component.IFileLocation;
import org.eclipse.wtp.releng.tools.component.ILocation;

public class Bundle extends PluginXML
{
  public static final String CONST_MANIFEST_MF = "MANIFEST.MF";
  public static final String CONST_BUNDLE_NAME = "Bundle-SymbolicName";
  public static final String CONST_BUNDLE_VERSION = "Bundle-Version";
  public static final String CONST_BUNDLE_CLASSPATH = "Bundle-ClassPath";

  public Bundle(ILocation location)
  {
    super(location);
  }

  public void addLibrary(String relativePath)
  {
    File manifest = ((IFileLocation)location).getFile();
    StringBuffer sb = new StringBuffer(manifest.getParentFile().getParentFile().getAbsolutePath());
    sb.append('/');
    sb.append(relativePath);
    File jar = new File(sb.toString());
    if (!jar.exists()) {
      unresolvedLibs.add(relativePath);
      System.err.println(jar);
    }
    addLibrary(new Library(new ZipLocation(jar)));
  }
}
