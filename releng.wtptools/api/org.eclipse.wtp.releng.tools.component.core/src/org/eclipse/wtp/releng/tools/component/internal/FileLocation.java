/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.eclipse.wtp.releng.tools.component.IFileLocation;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ILocationChildrenIterator;

/**
 * A <code>FileLocation</code> is a location that represents a plain file in
 * the filesystem.
 */
public class FileLocation extends Location implements IFileLocation
{
  protected File file;

  /**
   * Creates a new <code>FileLocation</code> on the given file
   * 
   * @param file
   *          A file to wrap, not null
   */
  public FileLocation(File file)
  {
    this(null, file.getAbsolutePath());
  }

  /**
   * Creates a new <code>FileLocation</code> using the given name to create a
   * child of the parent location.
   * 
   * @param parent
   *          The parent location
   * @param name
   *          The name of the child, not <code>null</code>
   */
  public FileLocation(FileLocation parent, String name)
  {
    super(parent, name);
    if (parent != null)
    {
      file = new File(((FileLocation)parent).file, getName());
    }
    else
    {
      file = new File(getName());
    }
  }

  /*
   * @see com.example.location.ILocation#getInputStream()
   */
  public InputStream getInputStream() throws IOException
  {
    return new FileInputStream(file);
  }

  /*
   * @see com.example.location.ILocation#childIterator()
   */
  public ILocationChildrenIterator childIterator()
  {
    if (file.isDirectory())
    {
      final String[] children = file.list();
      return new ILocationChildrenIterator()
      {
        private int index = 0;

        public ILocation next()
        {
          if (index < children.length)
          {
            String child = children[index++];
            File childFile = new File(file, child);
            if (childFile.isDirectory())
            {
              return new FileLocation(FileLocation.this, child);
            }
            if (Location.isArchive(child))
            {
              return new ZipLocation(FileLocation.this, child);
            }
            else
            {
              return new FileLocation(FileLocation.this, child);
            }
          }
          return null;
        }
      };
    }
    else
    {
      return new ILocationChildrenIterator()
      {
        public ILocation next()
        {
          return null;
        }
      };
    }
  }

  /*
   * @see com.example.location.ILocation#hasChildren()
   */
  public boolean hasChildren()
  {
    if (file.isDirectory())
    {
      return true;
    }
    return false;
  }

  /**
   * Answers the file that this location wrappers.
   * 
   * @return File
   */
  public File getFile()
  {
    return file;
  }

  /*
   * @see com.example.location.ILocation#createSibling(java.lang.String)
   */
  public ILocation createSibling(String relativePath)
  {
    FileLocation parentLocation = (FileLocation)parent;
    File childFile = new File(parentLocation.file, relativePath);
    if (childFile.isDirectory())
    {
      return new FileLocation(parentLocation, relativePath);
    }
    if (Location.isArchive(relativePath))
    {
      return new ZipLocation(parentLocation, relativePath);
    }
    else
    {
      return new FileLocation(parentLocation, relativePath);
    }
  }

  /*
   * @see com.example.location.ILocation#createChild(java.lang.String)
   */
  public ILocation createChild(String relativePath)
  {
    File childFile = new File(file, relativePath);
    if (childFile.isDirectory())
    {
      return new FileLocation(this, relativePath);
    }
    if (Location.isArchive(relativePath))
    {
      return new ZipLocation(this, relativePath);
    }
    else
    {
      return new FileLocation(this, relativePath);
    }
  }

  /*
   * @see com.example.location.ILocation#getAbsolutePath()
   */
  public String getAbsolutePath()
  {
    if (parent == null)
    {
      return getName();
    }
    else
    {
      return parent.getAbsolutePath() + "/" + getName();
    }
  }
}
