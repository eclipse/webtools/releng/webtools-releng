<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:template match="/">
		<html>
			<body>
				<table border="0" cellpadding="2" cellspacing="5" width="100%">
					<tr>
						<td align="left" width="60%">
							<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">
								Code Usage Report
							</font>
						</td>
					</tr>
				</table>
				<table border="0" cellpadding="2" cellspacing="5" width="100%">
					<tr>
						<td ALIGN="LEFT" VALIGN="TOP" COLSPAN="2" BGCOLOR="#0080C0"><b><font color="#FFFFFF" face="Arial,Helvetica">
							Code usage summary
						</font></b></td>
					</tr>
				</table>
				<u><h5>
				<xsl:for-each select="root/team">
					<xsl:sort select="@lead"/>
					<a href="#{@lead}"><xsl:value-of select="@lead"/></a><br />
				</xsl:for-each>
				</h5></u>
				<xsl:for-each select="root/team">
					<xsl:sort select="@lead"/>
					<h2><a name="{@lead}" id="{@lead}"><xsl:value-of select="@lead"/></a></h2>
					<table id="data" border="1" width="80%">
						<tr>
							<th width="95%">Class name</th>
							<th>Internal code usage count</th>
							<th>API usage count</th>
							<xsl:for-each select="source">
							   <th><xsl:value-of select="@name"/> Internal Count</th>
							</xsl:for-each>
						</tr>
						<xsl:for-each select="class">
							<xsl:apply-templates select="."/>
						</xsl:for-each>
					</table>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="class">
		<tr>
			<td><b><xsl:value-of select="@name"/></b></td>
			<td><xsl:value-of select="@internal"/></td>
			<td><xsl:value-of select="@api"/></td>
			<xsl:for-each select="source">
				<xsl:sort select="@name"/>
				<xsl:apply-templates select="."/>
			</xsl:for-each>
		</tr>
		<xsl:for-each select="method">
				<xsl:sort select="@name"/>
				<xsl:apply-templates select="."/>
		</xsl:for-each>
		<xsl:for-each select="field">
				<xsl:sort select="@name"/>
				<xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="source">
		<td><xsl:value-of select="@internal"/></td>
	</xsl:template>
	
	<xsl:template match="method">
	    <tr>
	    <td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Method : <xsl:value-of select="@name"/>(...)</td>
		<td><xsl:value-of select="@internal"/></td>
		<td> &#160;</td>
		</tr>
	</xsl:template>
	
	<xsl:template match="field">
	    <tr>
	    <td>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Field : <xsl:value-of select="@name"/></td>
		<td><xsl:value-of select="@internal"/></td>
		<td> &#160;</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>