/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ILocationVisitor;
import org.eclipse.wtp.releng.tools.component.internal.FileLocation;
import org.eclipse.wtp.releng.tools.component.internal.Location;

public class ComponentAPICache
{
  private int size;
  private String compAPIDir;
  private List cachedIds = new ArrayList(size);
  private List cachedCompAPIs = new ArrayList(size);

  public ComponentAPICache(String compAPIDir)
  {
    this(20, compAPIDir);
  }

  public ComponentAPICache(int size, String compAPIDir)
  {
    this.size = size;
    this.compAPIDir = addTrailingSeperator(compAPIDir);
  }
  
  private String addTrailingSeperator(String s) {
    if (s != null && !s.endsWith("/") && !s.endsWith("\\")) { //$NON-NLS-1$ //$NON-NLS-2$
      StringBuffer sb = new StringBuffer(s);
      sb.append('/');
      return sb.toString();
    }
    return s;
  }

  public ComponentAPI getComponentAPI(String id) throws IOException
  {
    int index = cachedIds.indexOf(id);
    if (index != -1)
    {
      ComponentAPI compAPI = (ComponentAPI)cachedCompAPIs.get(index);
      if (index != 0)
      {
        cachedIds.remove(index);
        cachedCompAPIs.remove(index);
        cachedIds.add(0, id);
        cachedCompAPIs.add(0, compAPI);
      }
      return compAPI;
    }
    StringBuffer sb = new StringBuffer(compAPIDir);
    sb.append(id);
    sb.append("/api-info.xml");
    File file = new File(sb.toString());
    ComponentAPI compAPI = new ComponentAPI();
    compAPI.setName(id);
    compAPI.setLocation(new FileLocation(file));
    if (file.exists())
    {
      compAPI.load();
    }
    if (cachedCompAPIs.size() == size)
    {
      cachedIds.remove(size - 1);
      ((ComponentAPI)cachedCompAPIs.remove(size - 1)).save();
    }
    cachedIds.add(0, id);
    cachedCompAPIs.add(0, compAPI);
    return compAPI;
  }

  private Map pkg2Id;

  public ComponentAPI getComponentAPIByClassName(String className)
  {
    if (pkg2Id == null)
    {
      pkg2Id = new HashMap();
      ILocation dir = Location.createLocation(new File(compAPIDir));
      LocationVisitor visitor = new LocationVisitor();
      dir.accept(visitor);
    }
    int index = className.lastIndexOf('.');
    String pkgName = (index != -1) ? className.substring(0, index) : "";
    String localName = (index != -1) ? className.substring(index + 1) : className;
    int innerClassIndex = localName.indexOf("$");
    if (innerClassIndex != -1)
    {
      localName = localName.substring(0, innerClassIndex);
    }
    if (localName.endsWith("[]"))
    {
      localName = localName.substring(0, localName.length() - 2);
    }
    List ids = (List)pkg2Id.get(pkgName);
    if (ids != null)
    {
      for (Iterator it = ids.iterator(); it.hasNext();)
      {
        try
        {
          ComponentAPI compAPI = getComponentAPI((String)it.next());
          PackageAPI pkgAPI = compAPI.getPackageAPI(pkgName);
          if (pkgAPI.getClassAPI(localName) != null)
          {
            return compAPI;
          }
        }
        catch (IOException ioe)
        {
          ioe.printStackTrace();
        }
      }
    }
    return null;
  }

  private class LocationVisitor implements ILocationVisitor
  {
    public boolean accept(ILocation location)
    {
      String absPath = location.getAbsolutePath().replace('\\', '/');
      if (absPath.endsWith("api-info.xml"))
      {
        String id = absPath.substring(0, absPath.length() - "/api-info.xml".length());
        int i = id.lastIndexOf('/');
        if (i != -1)
        {
          id = id.substring(i + 1);
          try
          {
            ComponentAPI compAPI = new ComponentAPI();
            compAPI.setLocation(location);
            compAPI.load();
            for (Iterator it = compAPI.getPackageAPIs().iterator(); it.hasNext();)
            {
              String pkgName = ((PackageAPI)it.next()).getName();
              List ids = (List)pkg2Id.get(pkgName);
              if (ids == null)
              {
                ids = new ArrayList();
                pkg2Id.put(pkgName, ids);
              }
              ids.add(id);
            }
          }
          catch (IOException ioe)
          {
            ioe.printStackTrace();
          }
        }
      }
      return true;
    }
  }
}