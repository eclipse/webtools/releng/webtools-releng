<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<script language="javascript">
				function twist(img)
				{
					var div = document.getElementById(img.id.substring(0, img.id.length - 4));
					if (div.style.display == "")
					{
						div.style.display = "none";
						img.src = "../twistclosed.gif";
					}
					else
					{
						div.style.display = "";
						img.src = "../twistopened.gif";
					}
				}
				var show = false;
				function allTwist()
				{
					var divs = document.getElementsByTagName("div");
					for (var i = 0; i &lt; divs.length; i++)
					{
						var img = document.getElementById(divs[i].id + '.img');
						if (!show)
						{
							divs[i].style.display = "none";
							img.src = "../twistclosed.gif";
						}
						else
						{
							divs[i].style.display = "";
							img.src = "../twistopened.gif";
						}
					}
					var a = document.getElementById("allTwistId");
					if (!show)
						a.childNodes[0].data = "Expand classes";
					else
						a.childNodes[0].data = "Collapse classes";
					show = !show;
				}
			</script>
			<body>
				<font face="Arial,Helvetica">
					<xsl:apply-templates select="component-use"/>
				</font>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="component-use">
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td align="left" width="60%">
					<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">API Violation Report for <xsl:value-of select="@name"/></font>
				</td>
				<td width="40%">
					<img src="../Idea.jpg" align="middle" height="86" hspace="50" width="120"/>
				</td>
			</tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td ALIGN="LEFT" VALIGN="TOP" COLSPAN="2" BGCOLOR="#0080C0"><b><font color="#FFFFFF" face="Arial,Helvetica">API violations (total: 
				<xsl:variable name="x" select="count(source/class-use/method-use) + count(source/class-use/field-use)"/>
				<xsl:choose>
					<xsl:when test="$x &gt; 0">
						<xsl:value-of select="$x"/>)
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="count(source/class-use)"/>)
					</xsl:otherwise>
				</xsl:choose>
				</font></b></td>
			</tr>
		</table>
		<br/>
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td align="left">
					<a id="allTwistId" href="javascript:allTwist()">Collapse classes</a>
				</td>
			</tr>
		</table>
		<br/>
		<xsl:if test="source">
			<table>
				<xsl:for-each select="source">
					<xsl:sort select="@name"/>
					<xsl:apply-templates select="."/>
				</xsl:for-each>
			</table>
			<script language ="javascript">
				addComp('<xsl:value-of select="@name"/>');
			</script>
		</xsl:if>
	</xsl:template>

	<xsl:template match="source">
		<tr>
			<td>
				<img id="source::{@name}.img" src="../twistopened.gif" onclick="javascript:twist(this)"/>
				<xsl:value-of select="@name"/>
				<p>
					<div id="source::{@name}">
						<xsl:if test="class-use/@subclass = 'true'">
							<p>
							<img src="../space.gif"/>
							<b>Cannot subclass:</b><br/>
							<xsl:for-each select="class-use">
								<xsl:sort select="@name"/>
								<xsl:if test="@subclass">
									<img src="../space.gif"/>
									<img src="../space.gif"/>
									<xsl:value-of select="@name"/><br/>
								</xsl:if>
							</xsl:for-each>
							</p>
						</xsl:if>

						<xsl:if test="class-use/@implement = 'true'">
							<p>
							<img src="../space.gif"/>
							<b>Cannot implement:</b><br/>
							<xsl:for-each select="class-use">
								<xsl:sort select="@name"/>
								<xsl:if test="@implement">
									<img src="../space.gif"/>
									<img src="../space.gif"/>
									<xsl:value-of select="@name"/><br/>
								</xsl:if>
							</xsl:for-each>
							</p>
						</xsl:if>

						<xsl:if test="class-use/@instantiate = 'true'">
							<p>
							<img src="../space.gif"/>
							<b>Cannot instantiate:</b><br/>
							<xsl:for-each select="class-use">
								<xsl:sort select="@name"/>
								<xsl:if test="@instantiate">
									<xsl:for-each select="method-use">
										<xsl:if test="@name='&lt;init&gt;'">
											<img src="../space.gif"/>
											<img src="../space.gif"/>
											<xsl:value-of select="../@name"/>
											<img src="../space.gif"/>
											<i><xsl:value-of select="@lines"/></i><br/>
										</xsl:if>
									</xsl:for-each>
								</xsl:if>
							</xsl:for-each>
							</p>
						</xsl:if>

						<xsl:if test="class-use/@reference = 'true'">
							<p>
							<img src="../space.gif"/>
							<b>Cannot reference:</b><br/>
							<xsl:for-each select="class-use">
								<xsl:sort select="@name"/>
								<xsl:if test="@reference">
									<img src="../space.gif"/>
									<img src="../space.gif"/>
									<xsl:value-of select="@name"/>
									<xsl:for-each select="method-use">
										<xsl:if test="@name!='&lt;init&gt;'">
											<br/><i>
											<img src="../space.gif"/>
											<img src="../space.gif"/>
											<img src="../space.gif"/>
											<xsl:value-of select="@name"/>(...)
											<img src="../space.gif"/>
											<xsl:value-of select="@lines"/>
											</i>
										</xsl:if>
									</xsl:for-each>
									<xsl:for-each select="field-use">
										<xsl:sort select="@name"/>
										<br/><i>
										<img src="../space.gif"/>
										<img src="../space.gif"/>
										<img src="../space.gif"/>
										<xsl:value-of select="@name"/>
										<img src="../space.gif"/>
										<xsl:value-of select="@lines"/>
										</i>
									</xsl:for-each>
									<br/>
								</xsl:if>
							</xsl:for-each>
							</p>
						</xsl:if>
					</div>
				</p>
			</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>
