/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.wtp.releng.tools.component.internal.ComponentObject;

public class JavadocCoverage extends ComponentObject
{
  private Boolean hasDoc;
  private Boolean hasSince;
  private Boolean hasReturn;
  private List missingParams;
  private List missingThrows;

  public boolean hasDoc()
  {
    if (hasDoc == null)
      return true;
    else
      return hasDoc.booleanValue();
  }

  public Boolean getHasDoc()
  {
    return hasDoc;
  }

  public void setHasDoc(Boolean hasDoc)
  {
    this.hasDoc = hasDoc;
  }

  public boolean hasSince()
  {
    if (hasSince == null)
      return true;
    else
      return hasSince.booleanValue();
  }

  public Boolean getHasSince()
  {
    return hasSince;
  }

  public void setHasSince(Boolean hasSince)
  {
    this.hasSince = hasSince;
  }

  public boolean hasReturn()
  {
    if (hasReturn == null)
      return true;
    else
      return hasReturn.booleanValue();
  }

  public Boolean getHasReturn()
  {
    return hasReturn;
  }

  public void setHasReturn(Boolean hasReturn)
  {
    this.hasReturn = hasReturn;
  }

  public void addMissingParam(String param)
  {
    if (missingParams == null)
      missingParams = new ArrayList();
    missingParams.add(param);
  }

  public List getMissingParams()
  {
    List copy = new ArrayList();
    if (missingParams != null)
      copy.addAll(missingParams);
    return copy;
  }

  public void addMissingThrow(String missingThrow)
  {
    if (missingThrows == null)
      missingThrows = new ArrayList();
    missingThrows.add(missingThrow);
  }

  public List getMissingThrows()
  {
    List copy = new ArrayList();
    if (missingThrows != null)
      copy.addAll(missingThrows);
    return copy;
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<javadoc-coverage");
    if (getHasDoc() != null)
      sb.append(toAttribute("doc", String.valueOf(hasDoc())));
    if (getHasSince() != null)
      sb.append(toAttribute("since", String.valueOf(hasSince())));
    if (getHasReturn() != null)
      sb.append(toAttribute("return", String.valueOf(hasReturn())));
    sb.append(">");
    for (Iterator it = getMissingParams().iterator(); it.hasNext();)
    {
      sb.append("<param");
      sb.append(toAttribute("name", (String)it.next()));
      sb.append("/>");
    }
    for (Iterator it = getMissingThrows().iterator(); it.hasNext();)
    {
      sb.append("<throw");
      sb.append(toAttribute("name", (String)it.next()));
      sb.append("/>");
    }
    sb.append("</javadoc-coverage>");
    return sb.toString();
  }
}