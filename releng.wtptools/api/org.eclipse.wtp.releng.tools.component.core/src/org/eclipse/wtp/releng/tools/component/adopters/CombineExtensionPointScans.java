/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.component.adopters;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.wtp.releng.tools.component.CommandOptionParser;
import org.eclipse.wtp.releng.tools.component.xsl.XSLUtil;

/**
 * This class is used to combine multiple adopter extension point scan results created by
 * running the ExtensionPointScanner into one html report.  The output folder location must
 * exist before running and there are two command line parameters to be passed to the main
 * method for appropriate execution:
 *   sourceFiles - the input html files from adopter extension pt usage scans
 *   output - a string location of where the combinedExtpt.html file should go
 */
public class CombineExtensionPointScans extends ComponentTeamScanner implements IOutputConstants {

	// Command Line arguments	
	private static final String ARG_SOURCE = "sourceFiles"; //$NON-NLS-1$
	private static final String ARG_OUTPUT = "output"; //$NON-NLS-1$
	
	 // Instance variables for command line arguments
	private Collection sourceFiles;
	private String output;
	
	// Class variables for string values
	private static final String EXTENSION_POINT_USAGE_FILE = "org/eclipse/wtp/releng/tools/component/xsl/extpt.xsl"; //$NON-NLS-1$
	private static final String HTML_OUTPUT_EXTENSION = "/combinedExtPtUsage.html"; //$NON-NLS-1$
	private static final String ORG_ECLIPSE = "org.eclipse."; //$NON-NLS-1$
	private static final String BEGIN_TD = "<td>"; //$NON-NLS-1$
	private static final String END_TD = "</td>"; //$NON-NLS-1$
	
	/**
	 * This is the static main method used for launching this combinatorial class routine. The
	 * command line arguments used are:
	 *   sourceFiles - the input html files from adopter extension pt usage scans
	 *   output - a string location of where the combinedExtpt.html file should go
	 * @param args
	 */
	public static void main(String[] args) {
		// Parse the command line options
	    CommandOptionParser optionParser = new CommandOptionParser(args);
	    Map options = optionParser.getOptions();
	    Collection src = (Collection)options.get(ARG_SOURCE);
	    Collection output = (Collection)options.get(ARG_OUTPUT);
	    // If the usage is improper or arguments are not valid, prompt for proper usage
	    if (src == null || output == null) {
	      printUsage();
	      System.exit(-1);
	    }
		//Use the args to set up the CombineExtensionPointScans instance
		CombineExtensionPointScans combineExtPtUsage = new CombineExtensionPointScans();
		combineExtPtUsage.setOutput((String)output.iterator().next());
		combineExtPtUsage.setSourceFiles(src);
		combineExtPtUsage.execute();
	}
	
	/**
	 * The execute method drives the processing of this class.  It will read the files in, calculate
	 * total usage using the ComponentTeam class and then generate an XML file and translate it
	 * to HTML using XSLT.
	 */
	public void execute() {
		// Iterate over all the source extpt.html files provided by the "sourceFiles" collection arg
	    for (Iterator sourceFilesIterator = getSourceFiles().iterator(); sourceFilesIterator.hasNext();) {
	    	String file = (String)sourceFilesIterator.next();
	    	// Read and process the extension pt usages in the html file
	    	processFile(file);
	    }
	    // Generate the output combined html file
		generateHTMLFile();
	}
	
	/**
	 * This gets a FileInputStream on the file location passed in.  It will read and parse the file
	 * line by line, finding extension point usages to cache and update in the ComponentTeam map.
	 * 
	 * @param fileLocation String
	 */
	private void processFile(String fileLocation) {
		try {
			// Open input streams and buffered reader stream on the source html file
			FileInputStream fstream = new FileInputStream(fileLocation);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String currentLine;
			String extensionPointName;
			int useCount = 0;
			while ((currentLine = br.readLine()) != null) {
				// If the html current line contains "org.eclipse", assume it is an extension point
				if (currentLine.indexOf(ORG_ECLIPSE)>=0) {
					// Calculate the extension point string by removing the <td> and </td> tags
					int beginExtPtName = currentLine.indexOf(BEGIN_TD)+4;
					int endExtPtName = currentLine.indexOf(END_TD);
					extensionPointName = currentLine.substring(beginExtPtName, endExtPtName);
					// Calculate the use count by removing the last <td> and </td> tags on the line
					int beginUseCount = currentLine.lastIndexOf(BEGIN_TD)+4;
					int endUseCount = currentLine.lastIndexOf(END_TD);
					useCount = Integer.parseInt(currentLine.substring(beginUseCount, endUseCount));
					 // Get the associated component team for the extension point's plugin
		            ComponentTeam compTeam = getComponentTeam(extensionPointName);
		            // Update the extension point reference counts with the result of this extenion point scan
		            String previousCount = (String) compTeam.getExtensionPointReferenceCounts().get(extensionPointName);
		            // If this is the first time this extension pt is referenced, add it to the map
		            if (previousCount == null)
		            	compTeam.getExtensionPointReferenceCounts().put(extensionPointName, String.valueOf(useCount));
		            // Else, add the current use to the previous total use count and update map
		            else 
		            	compTeam.getExtensionPointReferenceCounts().put(extensionPointName, String.valueOf(Integer.parseInt(previousCount)+useCount));
				}
			}
			// Close the streams.
			in.close();
			fstream.close();
			br.close();
		} catch (Exception e) {
			  System.err.println(e.getMessage());
		} 
	}
	
	/**
	   * This method creates an XML formatted stream of the resulting extension point reference data
	   * and then transforms it to HTML using an XSL transformation.
	   *
	   */
	  private void generateHTMLFile() {
	    try {
	      // Get an output stream to write results in XML format
	      ByteArrayOutputStream baos = new ByteArrayOutputStream();
	      baos.write(XML_ROOT_BEGIN.getBytes());
	      // For each component team, write out the extension point usage
	      for (int i=0; i<getComponentTeams().size(); i++) {
	    	  ComponentTeam compTeam = (ComponentTeam) getComponentTeams().get(i);
	    	  writeCompTeamXML(compTeam, baos);
	      }
	      baos.write(XML_ROOT_END.getBytes());
	      // Close the XML output stream
	      baos.close();
	      // Write out the byte array xml to a html file output stream for the extension pt references
	      // This transform will do a XSLT operation using the file extpt.xsl
	      XSLUtil.transform (
	    	ClassLoader.getSystemResourceAsStream(EXTENSION_POINT_USAGE_FILE),
	    	new ByteArrayInputStream(baos.toByteArray()),
	        new FileOutputStream(getOutput() + HTML_OUTPUT_EXTENSION)
	      );
	    } catch (Throwable t) {
	      t.printStackTrace();
	      throw new RuntimeException(t);
	    }
	  }
	  
	  /**
	   * This method will write out, in xml format, the component team's extension point references.
	   * 
	   * @param compTeam
	   * @param baos
	   * @throws IOException
	   */
	  private void writeCompTeamXML(ComponentTeam compTeam, ByteArrayOutputStream baos) throws IOException {
	    baos.write("<team lead=\"".getBytes()); //$NON-NLS-1$
	    baos.write(compTeam.getTeamName().getBytes());
	    baos.write("\">".getBytes()); //$NON-NLS-1$
	    // For each extension pt in the component team's scope, write its reference information
	    for (Iterator it = compTeam.getExtensionPointReferenceCounts().keySet().iterator(); it.hasNext();) {
	      String id = (String)it.next();
	      String refCount = (String)compTeam.getExtensionPointReferenceCounts().get(id);
	      baos.write("<extpt id=\"".getBytes()); //$NON-NLS-1$
	      baos.write(id.getBytes());
	      baos.write("\" ref=\"".getBytes()); //$NON-NLS-1$
	      baos.write(refCount.getBytes());
	      baos.write("\"/>".getBytes()); //$NON-NLS-1$
	    }
	    baos.write("</team>".getBytes()); //$NON-NLS-1$
	  }

	/**
	 * @return Collection of input html files
	 */
	public Collection getSourceFiles() {
		return sourceFiles;
	}

	/**
	 * Set the collection of input html files
	 * @param sourceFiles Collection
	 */
	public void setSourceFiles(Collection sourceFiles) {
		this.sourceFiles = sourceFiles;
	}

	/**
	 * @return String output location
	 */
	public String getOutput() {
		return output;
	}

	/**
	 * Set the output location string
	 * @param output String
	 */
	public void setOutput(String output) {
		this.output = output;
	}
	
	/**
     * This is a helper method to the user to print out an error message of the proper usage of 
     * the arguments to be passed and the location of the output files.  
     * See IOutputConstants for messages.
     */
     private static void printUsage() {
    	 System.out.println(PRINT_USAGE_COMBINED_EXT_PT);
    	 System.out.println(""); //$NON-NLS-1$
    	 System.out.println(PRINT_SOURCE_FILES_LOCATION);
    	 System.out.println(PRINT_OUTPUT_LOCATION);
     }
}
