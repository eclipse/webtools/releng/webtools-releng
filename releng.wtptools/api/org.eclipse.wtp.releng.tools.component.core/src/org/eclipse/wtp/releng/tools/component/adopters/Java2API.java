/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.adopters;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.core.runtime.IPlatformRunnable;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.wtp.releng.tools.component.CommandOptionParser;
import org.eclipse.wtp.releng.tools.component.IJavaVisitor;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.internal.ComponentDepends;
import org.eclipse.wtp.releng.tools.component.internal.ComponentXML;
import org.eclipse.wtp.releng.tools.component.internal.FileLocation;
import org.eclipse.wtp.releng.tools.component.internal.Location;
import org.eclipse.wtp.releng.tools.component.internal.Plugin;

public class Java2API implements IJavaVisitor, IPlatformRunnable
{
  private String src;
  private String outputDir;
  private Collection includes;
  private Collection excludes;
  private Collection excludePlugins;
  private boolean defaultExclusive;
  private List visitedPlugins = new ArrayList();

  public String getOutputDir()
  {
    return outputDir;
  }

  public void setOutputDir(String outputDir)
  {
    this.outputDir = addTrailingSeperator(outputDir);
  }

  public String getSrc()
  {
    return src;
  }

  public void setSrc(String src)
  {
    this.src = src;
  }

  public Collection getIncludes()
  {
    return includes;
  }

  public void setIncludes(Collection includes)
  {
    this.includes = includes;
  }

  public Collection getExcludes()
  {
    return excludes;
  }

  public void setExcludes(Collection excludes)
  {
    this.excludes = excludes;
  }

  public Collection getExcludePlugins()
  {
    return excludePlugins;
  }

  public void setExcludePlugins(Collection excludePlugins)
  {
    this.excludePlugins = excludePlugins;
  }

  public boolean isDefaultExclusive()
  {
    return defaultExclusive;
  }

  public void setDefaultExclusive(boolean defaultExclusive)
  {
    this.defaultExclusive = defaultExclusive;
  }

  public List getVisitedPlugins()
  {
    return new ArrayList(visitedPlugins);
  }

  public void execute()
  {
    ILocation srcLocation = Location.createLocation(new File(src));
    PDESourceVisitor pdeSrcVisitor = new PDESourceVisitor();
    pdeSrcVisitor.setExcludes(excludePlugins);
    srcLocation.accept(pdeSrcVisitor);
    pdeSrcVisitor.setJavaVisitor(this);
    srcLocation.accept(pdeSrcVisitor);
    try
    {
      if (cachedCompXML != null)
        cachedCompXML.save();
    }
    catch (IOException e)
    {
      throw new RuntimeException(e);
    }
  }
  private ASTParser astParser;

  public boolean visit(String pluginId, ILocation java)
  {
    if (!visitedPlugins.contains(pluginId))
    {
      visitedPlugins.add(pluginId);
    }
    if (include(java.getName()))
    {
      InputStreamReader isr = null;
      CharArrayWriter caw = null;
      try
      {
        ComponentXML compXML = getComponentXML(pluginId);
        isr = new InputStreamReader(java.getInputStream());
        caw = new CharArrayWriter();
        char[] c = new char[2048];
        for (int read = isr.read(c); read != -1; read = isr.read(c))
          caw.write(c, 0, read);
        isr.close();
        caw.close();
        if (astParser == null)
          astParser = ASTParser.newParser(AST.JLS3);
        astParser.setSource(caw.toCharArray());
        ASTNode node = astParser.createAST(null);
        node.accept(new Java2APIVisitor(compXML, defaultExclusive));
      }
      catch (IOException e)
      {
        throw new RuntimeException(e);
      }
      finally
      {
        if (isr != null)
        {
          try
          {
            isr.close();
          }
          catch (IOException e)
          {
          }
        }
        if (caw != null)
          caw.close();
      }
    }
    return true;
  }

  private boolean include(String name)
  {
    name = name.replace('/', '.');
    name = name.replace('\\', '.');
    if (excludes != null && !excludes.isEmpty())
      for (Iterator it = excludes.iterator(); it.hasNext();)
        if (name.matches((String)it.next()))
          return false;
    if (includes != null && !includes.isEmpty())
    {
      for (Iterator it = includes.iterator(); it.hasNext();)
        if (name.matches((String)it.next()))
          return true;
      return false;
    }
    return true;
  }

  private ComponentXML cachedCompXML;

  private ComponentXML getComponentXML(String id) throws IOException
  {
    if (cachedCompXML != null)
    {
      if (cachedCompXML.getName().equals(id))
      {
        return cachedCompXML;
      }
      else
      {
        cachedCompXML.save();
      }
    }
    StringBuffer sb = new StringBuffer(outputDir);
    sb.append(id);
    sb.append("/component.xml");
    File file = new File(sb.toString());
    cachedCompXML = new ComponentXML();
    cachedCompXML.setName(id);
    cachedCompXML.setLocation(new FileLocation(file));
    Plugin plugin = new Plugin();
    plugin.setId(id);
    cachedCompXML.addPlugin(plugin);
    ComponentDepends compDepends = new ComponentDepends();
    compDepends.setUnrestricted(Boolean.TRUE);
    cachedCompXML.setComponentDepends(compDepends);
    if (file.exists())
      cachedCompXML.load();
    return cachedCompXML;
  }

  protected String addTrailingSeperator(String s)
  {
    if (s != null && !s.endsWith("/") && !s.endsWith("\\"))
    {
      StringBuffer sb = new StringBuffer(s);
      sb.append('/');
      return sb.toString();
    }
    else
    {
      return s;
    }
  }

  public Object run(Object arguments)
  {
    String src = System.getProperty("src");
    String outputDir = System.getProperty("outputDir");
    String includes = System.getProperty("includes");
    String excludes = System.getProperty("excludes");
    try
    {
    main(new String[]{"-src", src, "-outputDir", outputDir, "-includes", includes, "-excludes", excludes});
    }
    catch (Throwable t)
    {
      t.printStackTrace();
    }
    return IPlatformRunnable.EXIT_OK;
  }

  public static void main(String[] args)
  {
    CommandOptionParser optionParser = new CommandOptionParser(args);
    Map options = optionParser.getOptions();
    Collection src = (Collection)options.get("src");
    Collection outputDir = (Collection)options.get("outputDir");
    Collection includes = (Collection)options.get("includes");
    Collection excludes = (Collection)options.get("excludes");
    Collection excludePlugins = (Collection)options.get("excludePlugins");
    Collection defaultExclusive = (Collection)options.get("defaultExclusive");
    if (src == null || outputDir == null || src.isEmpty() || outputDir.isEmpty())
    {
      printUsage();
      System.exit(-1);
    }
    Java2API java2API = new Java2API();
    java2API.setSrc((String)src.iterator().next());
    java2API.setOutputDir((String)outputDir.iterator().next());
    java2API.setIncludes(includes);
    java2API.setExcludes(excludes);
    java2API.setExcludePlugins(excludePlugins);
    java2API.setDefaultExclusive(defaultExclusive != null);
    java2API.execute();
  }

  private static void printUsage()
  {
    System.out.println("Usage: java org.eclipse.wtp.releng.tools.component.java.Java2API -src <src> -outputDir <outputDir> [-options]");
    System.out.println("");
    System.out.println("\t-src\t\t<src>\t\tlocation of a Eclipse-based product (requires SDK build)");
    System.out.println("\t-outputDir\t<outputDir>\toutput directory of component.xml files");
    System.out.println("");
    System.out.println("where options include:");
    System.out.println("");
    System.out.println("\t-includes\t<includes>\tspace seperated packages to include");
    System.out.println("\t-excludes\t<excludes>\tspace seperated packages to exclude");
    System.out.println("\t-excludePlugins\t<excludePlugins>\tspace seperated plugins to exclude");
  }
}