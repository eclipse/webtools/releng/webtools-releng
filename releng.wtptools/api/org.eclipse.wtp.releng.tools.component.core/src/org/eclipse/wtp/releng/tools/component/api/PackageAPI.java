/*******************************************************************************
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.wtp.releng.tools.component.internal.ComponentObject;

public class PackageAPI extends ComponentObject
{
  private String name;
  private Map classAPIs;

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    this.name = name;
  }

  /**
   * @return Returns the classAPIs.
   */
  public Collection getClassAPIs()
  {
    if (classAPIs == null)
      classAPIs = new HashMap(1);
    return classAPIs.values();
  }

  public ClassAPI getClassAPI(String name)
  {
    if (classAPIs == null)
      return null;
    else
      return (ClassAPI)classAPIs.get(name);
  }

  public void addClassAPI(ClassAPI classAPI)
  {
    if (classAPIs == null)
      classAPIs = new HashMap(1);
    classAPIs.put(classAPI.getName(), classAPI);
  }

  public void removeClassAPI(ClassAPI classAPI)
  {
    if (classAPIs != null)
      classAPIs.remove(classAPI.getName());
  }

  public int sizeClassAPI()
  {
    if (classAPIs != null)
      return classAPIs.size();
    else
      return 0;
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<package-api");
    sb.append(toAttribute("name", getName()));
    sb.append(">");
    for (Iterator it = getClassAPIs().iterator(); it.hasNext();)
      sb.append(((ClassAPI)it.next()).toString());
    sb.append("</package-api>");
    return sb.toString();
  }
}