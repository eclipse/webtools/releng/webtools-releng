/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.eclipse.wtp.releng.tools.component.ILocation;

public class ComponentSummary
{
  public static final String INDEX_COMPONENT_SUMMARY_XML = "index-comp-summary.xml";
  private static final String ROOT_TAG_NAME = "component-summary";
  private String timestamp;
  private Map entries;

  public String getTimestamp()
  {
    return timestamp;
  }

  public void setTimestamp(String timestamp)
  {
    this.timestamp = timestamp;
  }

  public ComponentSummary()
  {
    entries = new HashMap(1);
  }

  public void add(ComponentEntry entry)
  {
    entries.put(entry.getCompName(), entry);
  }

  public ComponentEntry getEntry(String compName)
  {
    return (ComponentEntry)entries.get(compName);
  }

  public List getEntries()
  {
    return new ArrayList(entries.values());
  }

  protected void saveAsHTML(ILocation html, String xsl, String rootTagName) throws TransformerConfigurationException, TransformerException, IOException
  {
    TransformerFactory factory = TransformerFactory.newInstance();
    Transformer transformer = factory.newTransformer(new StreamSource(ClassLoader.getSystemResourceAsStream(xsl)));
    transformer.transform(new StreamSource(new ByteArrayInputStream(getBytes(html, rootTagName))), new StreamResult(new FileOutputStream(new File(html.getAbsolutePath()))));
  }

  public void save(ILocation location) throws IOException
  {
    save(location, ROOT_TAG_NAME);
  }

  protected void save(ILocation location, String rootTagName) throws IOException
  {
    File file = new File(location.getAbsolutePath());
    file.getParentFile().mkdirs();
    FileOutputStream fos = new FileOutputStream(file);
    fos.write(getBytes(location, rootTagName));
    fos.close();
  }

  protected byte[] getBytes(ILocation location, String rootTagName) throws UnsupportedEncodingException
  {
    String base = location.getAbsolutePath().replace('\\', '/');
    int index = base.lastIndexOf('/');
    if (index != -1)
      base = base.substring(0, index);
    for (Iterator it = entries.values().iterator(); it.hasNext();)
    {
      ComponentEntry entry = (ComponentEntry)it.next();
      String ref = entry.getRef();
      if (ref != null && ref.startsWith(base))
      {
        StringBuffer newRef = new StringBuffer(".");
        newRef.append(ref.substring(base.length()));
        entry.setRef(newRef.toString());
      }
    }
    return toString(rootTagName).getBytes();
  }

  public String toString(String rootTagName)
  {
    StringBuffer sb = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    sb.append("<");
    sb.append(rootTagName);
    if (timestamp != null)
    {
      sb.append(" timestamp=\"");
      sb.append(timestamp);
      sb.append("\"");
    }
    sb.append(">");
    for (Iterator it = entries.values().iterator(); it.hasNext();)
      sb.append(it.next().toString());
    sb.append("</");
    sb.append(rootTagName);
    sb.append(">");
    return sb.toString();
  }

  public String toString()
  {
    return toString("component-summary");
  }
}