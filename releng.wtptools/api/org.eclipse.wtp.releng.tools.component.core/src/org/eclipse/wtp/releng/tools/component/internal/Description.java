/*******************************************************************************
 * Copyright (c) 2004, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

public class Description extends ComponentObject
{
  private String url;

  public String getUrl()
  {
    return url;
  }

  public void setUrl(String url)
  {
    this.url = url;
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<description");
    sb.append(toAttribute("url", url));
    sb.append(">");
    sb.append("</description>");
    return sb.toString();
  }

  public Object clone()
  {
    Description clone = new Description();
    clone.setUrl(getUrl());
    return clone;
  }
}