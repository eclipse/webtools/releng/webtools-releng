<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<script language="javascript">
				function twist(img)
				{
					var div = document.getElementById(img.id.substring(0, img.id.length - 4));
					if (div.style.display == "")
					{
						div.style.display = "none";
						img.src = "../twistclosed.gif";
					}
					else
					{
						div.style.display = "";
						img.src = "../twistopened.gif";
					}
				}
				var show = false;
				function allTwist()
				{
					var divs = document.getElementsByTagName("div");
					for (var i = 0; i &lt; divs.length; i++)
					{
						var img = document.getElementById(divs[i].id + '.img');
						if (!show)
						{
							divs[i].style.display = "none";
							img.src = "../twistclosed.gif";
						}
						else
						{
							divs[i].style.display = "";
							img.src = "../twistopened.gif";
						}
					}
					var a = document.getElementById("allTwistId");
					if (!show)
						a.childNodes[0].data = "expand classes";
					else
						a.childNodes[0].data = "collapse classes";
					show = !show;
				}
			</script>
			<body>
				<font face="Arial,Helvetica">
					<xsl:apply-templates select="component-api"/>
				</font>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="component-api">
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td align="left" width="60%">
					<font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">API Javadoc Report for <xsl:value-of select="@name"/></font>
				</td>
				<td width="40%">
					<img src="../Idea.jpg" align="middle" height="86" hspace="50" width="120"/>
				</td>
			</tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td ALIGN="LEFT" VALIGN="TOP" COLSPAN="2" BGCOLOR="#0080C0"><b><font color="#FFFFFF" face="Arial,Helvetica">Missing Javadoc (total: <xsl:value-of select="count(package-api/class-api/javadoc-coverage) + count(package-api/class-api/method-api/javadoc-coverage) + count(package-api/class-api/field-api/javadoc-coverage)"/>)</font></b></td>
			</tr>
		</table>
		<br/>
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<tr>
				<td align="left">
					Missing javadoc are shown in <font color="red">red</font>. Click here to <a id="allTwistId" href="javascript:allTwist()">collapse classes</a>.
				</td>
			</tr>
		</table>
		<br/>
		<table border="0" cellpadding="2" cellspacing="5" width="100%">
			<xsl:for-each select="package-api/class-api">
				<xsl:sort select="@name"/>
				<xsl:apply-templates select="."/>
			</xsl:for-each>
		</table>
	</xsl:template>

	<xsl:template match="class-api">
		<xsl:if test="javadoc-coverage | method-api/javadoc-coverage | field-api/javadoc-coverage">
			<tr>
				<td>
					<xsl:variable name="fullname" select="concat(../@name, '.', @name)"/>
					<img id="source::{@name}.img" src="../twistopened.gif" onclick="javascript:twist(this)"/>
					<xsl:value-of select="$fullname"/>
					<p>
						<div id="source::{@name}">
							<font style="font-size: x-small; font-family: Courier New">
								<xsl:choose>
									<xsl:when test="javadoc-coverage/@since">
										<font color="red">@since</font>
									</xsl:when>
									<xsl:otherwise>
										@since
									</xsl:otherwise>
								</xsl:choose>
								<br/>
								class <xsl:value-of select="@name"/><br/>
								{<br/>
									<xsl:for-each select="field-api">
										<xsl:sort select="@name"/>
										<xsl:apply-templates select="."/>
									</xsl:for-each>
									<xsl:for-each select="method-api">
										<xsl:sort select="@name"/>
										<xsl:apply-templates select="."/>
									</xsl:for-each>
								}<br/>
							</font>
						</div>
					</p>
				</td>
			</tr>
		</xsl:if>
	</xsl:template>

	<xsl:template match="method-api">
		<xsl:if test="javadoc-coverage">
			<p>
			&#160;&#160;/**<br/>
			<xsl:if test="javadoc-coverage/@doc">
				&#160;&#160;&#160;* <font color="red">Missing method description</font><br/>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="javadoc-coverage/@since">
					&#160;&#160;&#160;* <font color="red">@since</font><br/>
				</xsl:when>
				<xsl:otherwise>
					&#160;&#160;&#160;* @since<br/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:for-each select="javadoc-coverage/param">
				&#160;&#160;&#160;* <font color="red">@param <xsl:value-of select="@name"/></font><br/>
			</xsl:for-each>
			<xsl:for-each select="javadoc-coverage/throw">
				&#160;&#160;&#160;* <font color="red">@throws <xsl:value-of select="@name"/></font><br/>
			</xsl:for-each>
			<xsl:choose>
				<xsl:when test="javadoc-coverage/@return">
					&#160;&#160;&#160;* <font color="red">@return</font><br/>
				</xsl:when>
				<xsl:otherwise>
					&#160;&#160;&#160;* @return<br/>
				</xsl:otherwise>
			</xsl:choose>
			&#160;&#160;&#160;*/<br/>
			<xsl:choose>
				<xsl:when test="contains(@name, '&lt;init&gt;')">
					&#160;&#160;<xsl:value-of select="../@name"/>
				</xsl:when>
				<xsl:otherwise>
					&#160;&#160;<xsl:value-of select="@name"/>
				</xsl:otherwise>
			</xsl:choose>
			(...);<br/>
			</p>
		</xsl:if>
	</xsl:template>

	<xsl:template match="field-api">
		<xsl:if test="javadoc-coverage">
			<p>
			&#160;&#160;/**<br/>
			&#160;&#160;&#160;* <font color="red">Missing field description</font><br/>
			&#160;&#160;&#160;*/<br/>
			&#160;
			<xsl:call-template name="printType">
				<xsl:with-param name="type" select="@descriptor"/>
			</xsl:call-template>
			<xsl:if test="contains(@descriptor, '[')">[]</xsl:if>
			&#160;<xsl:value-of select="@name"/>;<br/>
			</p>
		</xsl:if>
	</xsl:template>

	<xsl:template name="printType">
		<xsl:param name="type"/>
		<xsl:choose>
			<xsl:when test="contains($type, '/')">
				<xsl:variable name="type2" select="substring-after($type, '/')"/>
				<xsl:call-template name="printType">
					<xsl:with-param name="type" select="$type2"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="contains($type, ';')">
						<xsl:value-of select="substring-before($type, ';')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="string-length($type) = 1 and contains($type, 'Z')">
							boolean
						</xsl:if>
						<xsl:if test="string-length($type) = 1 and contains($type, 'B')">
							byte
						</xsl:if>
						<xsl:if test="string-length($type) = 1 and contains($type, 'C')">
							char
						</xsl:if>
						<xsl:if test="string-length($type) = 1 and contains($type, 'D')">
							double
						</xsl:if>
						<xsl:if test="string-length($type) = 1 and contains($type, 'F')">
							float
						</xsl:if>
						<xsl:if test="string-length($type) = 1 and contains($type, 'I')">
							int
						</xsl:if>
						<xsl:if test="string-length($type) = 1 and contains($type, 'J')">
							long
						</xsl:if>
						<xsl:if test="string-length($type) = 1 and contains($type, 'S')">
							short
						</xsl:if>
						<xsl:if test="string-length($type) = 1 and contains($type, 'V')">
							void
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>