/*******************************************************************************
 * Copyright (c) 2007, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.component.internal;

import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;

/**
 * This is a helper class for use with org.eclipse.jdt.core.Signature.  The Signature class does
 * not make it easy to resolve the real qualified names of type signatures, so this helper class
 * has been added for help in the scans.
 */
public class SignatureHelper {
	
	/**
	 * Constants
	 */
	public static final char C_WHITESPACE = ' ';
	public static final char C_COMMA = ',';
	
	/**
	 * This is a helper method to return the fully qualified resolved signature type name of
	 * a given type signature character array.
	 * 
	 * @param typeSignature char[]
	 * @param relativeType IType
	 * @return resolved signature type class name
	 */
	public static String getSignatureResolvedName(char[] typeSignature, IType relativeType) {
		StringBuffer buffer = new StringBuffer();
		try {
		    computeSignatureResolvedName(typeSignature, relativeType, buffer);
		} catch (JavaModelException jme) {
			jme.printStackTrace();
		}
		return buffer.toString();
	}
	
	private static void computeSignatureResolvedName(char[] typeSignature, IType relativeType, StringBuffer buffer) throws JavaModelException{
		char[] erasureType = Signature.getTypeErasure(typeSignature);
		if (erasureType != null) {
			computeSignatureResolvedNameNoNesting(erasureType, relativeType, buffer);
		} else {
			char[] variableName = Signature.getTypeVariable(typeSignature);
			if (variableName != null) {
				buffer.append(variableName);
			}
		}
		char[][] typeArgs = Signature.getTypeArguments(typeSignature);
		if (typeArgs != null && typeArgs.length > 0) {
			buffer.append(Signature.C_GENERIC_START);
			for (int i = 0; i < typeArgs.length; i++) {
				computeSignatureResolvedName(typeArgs[i], relativeType, buffer);
				if (i + 1 < typeArgs.length) {
					buffer.append(C_COMMA);
					buffer.append(C_WHITESPACE);
				}
			}
			buffer.append(Signature.C_GENERIC_END);
		}
	}
	
	private static void computeSignatureResolvedNameNoNesting(char[] typeSignature, IType relativeType, StringBuffer buffer) throws JavaModelException{
		switch (Signature.getTypeSignatureKind(typeSignature)) {
			case Signature.CLASS_TYPE_SIGNATURE:
				if (typeSignature[0] == Signature.C_RESOLVED) {
					buffer.append(Signature.toCharArray(typeSignature));
					break;
				} 
				buffer.append(Signature.toQualifiedName(relativeType.resolveType(new String(Signature.toCharArray(typeSignature)))[0]));
				break;
			case Signature.WILDCARD_TYPE_SIGNATURE :
				//Check the next char to see what the type signature is.
				if (typeSignature.length > 1 && typeSignature[1] == Signature.C_UNRESOLVED){
					//Resolve the unresolved type.
					char[] wildcardType = new char[typeSignature.length - 3];
					System.arraycopy(typeSignature, 2, wildcardType, 0, wildcardType.length);
					String resolvedType = Signature.toQualifiedName(relativeType.resolveType(new String(wildcardType))[0]);
					char[] resolvedTypeChars = resolvedType.toCharArray();
					//We actually resolved something.
					if (resolvedTypeChars.length > (typeSignature.length - 3)) {
						char[] result = new char[resolvedTypeChars.length + 3];
						result[0] = typeSignature[0];
						result[1] = Signature.C_RESOLVED;
						//Copy the resolved chars.
						System.arraycopy(resolvedTypeChars, 0, result, 2, resolvedTypeChars.length);
						result[result.length - 1] = Signature.C_SEMICOLON;
						buffer.append(Signature.toCharArray(result));
						break;
					}
				} 
			default:
				buffer.append(Signature.getSignatureSimpleName(typeSignature));
		}
	}
}
