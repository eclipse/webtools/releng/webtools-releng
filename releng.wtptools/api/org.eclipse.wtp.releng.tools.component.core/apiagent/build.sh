#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
export LD_LIBRARY_PATH=$JAVA_HOME/jre/bin/java:$JAVA_HOME/jre/bin:$PWD
gcc -c -I$JAVA_HOME/include -I$JAVA_HOME/include/linux apiagent.c
ld -shared apiagent.o -o libapiagent.so
