/*******************************************************************************
 * Copyright (c) 2010, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package aaa;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.osgi.util.ManifestElement;
import org.osgi.framework.BundleException;

/**
 * This simple utility was attached to bug
 * https://bugs.eclipse.org/bugs/show_bug.cgi?id=329376 and provides a simple
 * quick check on one form of incorrect headers.
 */
public class Application implements IApplication {

    private static final boolean DEBUG = false;
    private static final String  PATH  = "/Users/equinox/eclipse_ibuild/plugins";

    private String               rootpath;
    private int                  checkedOk;
    private int                  failedCount;
    private int                  errorCount;

    public Object start(IApplicationContext context) throws Exception {
        File root = getRoot(context);
        checkBundles(root);
        printReport();
        return IApplication.EXIT_OK;
    }

    private void checkBundles(File root) throws BundleException, IOException {
        File[] children = root.listFiles();
        for (File child : children) {
            if (child.isDirectory()) {
                processFolder(child);
            }
            else if (child.getName().endsWith(".jar") || child.getName().endsWith(".zip")) {
                processJar(child);
            }
            else {
                error("Skipping unexpected file type: " + child);
            }
        }
    }

    private void printReport() {
        System.out.println();
        System.out.println("Checked bundles under root path: " + rootpath);
        System.out.println("Failed to read manifest (may be normal): " + failedCount);
        System.out.println("Bundles with version attribute errors: " + errorCount);
        System.out.println("CheckedOk: " + checkedOk);
        System.out.println();
    }

    private File getRoot(IApplicationContext context) {
        Map args = context.getArguments();
        String appargskey = "application.args";
        String[] appargs = (String[]) args.get(appargskey);
        if (appargs.length > 0) {
            rootpath = appargs[0];
            System.out.println("Found path from application arguments: " + rootpath);
        }
        else {
            rootpath = PATH;
            System.out.println("No path was found from application arguments, so using default: " + rootpath);
        }

        File root = new File(rootpath);
        return root;
    }

    private void debug(String message) {
        if (DEBUG) {
            System.out.println(message);
        }
    }

    private void error(String message) {
        System.err.println(message);
    }

    private void processFolder(File folder) throws BundleException {
        debug("Processing: " + folder);
        File manifestFile = new File(folder, "META-INF/MANIFEST.MF");
        if (!manifestFile.exists()) {
            error("WARNING: Unable to read manifest at: " + manifestFile.getAbsolutePath());
            failedCount++;
            return;
        }
        InputStream input = null;
        try {
            input = new BufferedInputStream(new FileInputStream(manifestFile));
            processStream(input, manifestFile.getAbsolutePath());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (input != null) {
                try {
                    input.close();
                }
                catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    private void processJar(File file) throws IOException, BundleException {
        debug("Processing: " + file);
        JarFile jar = new JarFile(file);
        ZipEntry entry = jar.getEntry("META-INF/MANIFEST.MF");
        if (entry == null) {
            error("WARNING: No manifest found in: " + file.getAbsolutePath());
            failedCount++;
            return;
        }
        InputStream input = null;
        try {
            input = new BufferedInputStream(jar.getInputStream(entry));
            processStream(input, file.getAbsolutePath());
        }
        catch (IOException e) {
            if (input != null) {
                try {
                    input.close();
                }
                catch (IOException e1) {
                    System.out.println("Unexpectedly could not close stream?");
                }
            }
        }
    }

    private void processStream(InputStream input, String filename) throws IOException, BundleException {
        String[] KEYS = new String[]{"Require-Bundle", "Fragment-Host"};
        Map<String, String> manifest = getManifest(input);
        for (String key : KEYS) {
            String value = manifest.get(key);
            if (value == null) {
                continue;
            }
            ManifestElement[] elements = ManifestElement.parseHeader(key, value);
            if (elements != null) {
                for (ManifestElement element : elements) {
                    if (element != null) {
                        for (Enumeration<String> e = element.getKeys(); (e != null) && e.hasMoreElements();) {
                            String attr = e.nextElement();
                            if ("version".equals(attr)) {
                                error(filename);
                                errorCount++;
                                return;
                            }
                        }
                    }
                }
                checkedOk++;
            }
        }
    }

    private Map<String, String> getManifest(InputStream input) throws IOException, BundleException {
        return ManifestElement.parseBundleManifest(input, null);
    }

    public void stop() {
        // nothing to do
    }
}
