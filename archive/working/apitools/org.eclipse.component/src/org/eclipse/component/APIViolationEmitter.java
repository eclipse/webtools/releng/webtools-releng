/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.component;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.component.api.ApiFactory;
import org.eclipse.component.api.ApiTypes;
import org.eclipse.component.api.ClassApi;
import org.eclipse.component.api.ComponentApiType;
import org.eclipse.component.api.FieldApi;
import org.eclipse.component.api.MethodApi;
import org.eclipse.component.api.Package;
import org.eclipse.component.use.ComponentUseType;
import org.eclipse.component.use.SourceClass;
import org.eclipse.component.use.UseFactory;
import org.eclipse.component.use.UsePackage;
import org.eclipse.component.use.util.UseResourceFactoryImpl;
import org.eclipse.component.use.util.UseResourceImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class APIViolationEmitter
{
  private static final String CONST_COMPONENT_VIOLATION_XML = "component-violation.xml";
  private String compDirLoc;
  private String reportLoc;
  private Map compName2Comp;

  public APIViolationEmitter(String compDirLoc, String reportLoc)
  {
    this.compDirLoc = compDirLoc;
    this.reportLoc = reportLoc;
  }

  public void setComponents(Map compName2Comp)
  {
    this.compName2Comp = compName2Comp;
  }

  public void genAPIViolationReport() throws IOException
  {
    for (Iterator it = compName2Comp.keySet().iterator(); it.hasNext();)
      genAPIViolationReport((String)it.next());
  }

  public void genAPIViolationReport(String compName) throws IOException
  {
    /*
    Component comp = (Component)compName2Comp.get(compName);
    if (comp != null)
    {
      ComponentUseType apiViolations = UseFactory.eINSTANCE.createComponentUseType();
      ComponentUseType compUse = comp.getCompUseXML();
      EList sources = compUse.getSourceClass();
      for (Iterator sourcesIt = sources.iterator(); sourcesIt.hasNext();)
      {
        SourceClass source = (SourceClass)sourcesIt.next();
        EList classUses = source.getClassUse();
        for (Iterator classUseIt = classUses.iterator(); classUseIt.hasNext();)
        {
          ClassApi classUse = (ClassApi)classUseIt.next();
          isPublicApi(classUse);
        }
      }
      saveAPIViolations(apiViolations, new File(reportLoc + CONST_COMPONENT_VIOLATION_XML));
    }
    */
  }

  private ClassApi getAPIViolation(ClassApi classUse)
  {
    return null;
    /*
    String className = classUse.getName();
    String pkgName;
    int index = className.lastIndexOf('.');
    if (index != -1)
      pkgName = className.substring(0, index);
    else
      pkgName = "";
    for (Iterator compIt = compName2Comp.values().iterator(); compIt.hasNext();)
    {
      Component comp = (Component)compIt.next();
      ComponentApiType compApi = comp.getCompApiXml();
      ApiTypes externalApis = compApi.getExternalApis();
      EList pkgs = externalApis.getPackage();
      for (Iterator pkgsIt = pkgs.iterator(); pkgsIt.hasNext();)
      {
        Package pkg = (Package)pkgsIt.next();
        if (pkg.getName().equals(pkgName))
        {
          EList apis = pkg.getClassApi();
          for (Iterator apisIt = apis.iterator(); apisIt.hasNext();)
          {
            ClassApi classApi = (ClassApi)apisIt.next();
            if (classUse.isReference() && !classApi.isReference())
              break;
            if (classUse.isSubclass() && !classApi.isSubclass())
              break;
            if (classUse.isImplement() && !classApi.isImplement())
              break;
            if (classUse.isInstantiate() && !classApi.isInstantiate())
              break;
            EList methodUses = classUse.getMethodApi();
            for (Iterator methodUsesIt = methodUses.iterator(); methodUsesIt.hasNext();)
            {
              MethodApi methodUse = (MethodApi)methodUsesIt.next();
              isPublicApi(methodUse);
            }
            EList fieldUses = classUse.getFieldApi();
            for (Iterator fieldUsesIt = fieldUses.iterator(); fieldUsesIt.hasNext();)
            {
              FieldApi fieldUse = (FieldApi)fieldUsesIt.next();
              isPublicApi(fieldUse);
            }
            return true;
          }
        }
      }
    }
    return false;
    */
  }

  private MethodApi getAPIViolation(ClassApi classApi, MethodApi methodUse)
  {
    String name = methodUse.getName();
    List params = methodUse.getInputType();
    String returnType = methodUse.getReturnType();
    EList methodApis = classApi.getMethodApi();
    for (Iterator it = methodApis.iterator(); it.hasNext();)
    {
      MethodApi methodApi = (MethodApi)it.next();
      if (methodApi.getName().equals(name) && inputTypeEquals(methodApi.getInputType(), params) && methodApi.getReturnType().equals(returnType))
        return null;
    }
    MethodApi methodUseClone = ApiFactory.eINSTANCE.createMethodApi();
    methodUseClone.setName(name);
    methodUseClone.setInputType(params);
    methodUseClone.setReturnType(returnType);
    return methodUseClone;
  }

  private FieldApi getAPIViolation(ClassApi classApi, FieldApi fieldUse)
  {
    String name = fieldUse.getName();
    String type = fieldUse.getType();
    EList fieldApis = classApi.getFieldApi();
    for (Iterator it = fieldApis.iterator(); it.hasNext();)
    {
      FieldApi fieldApi = (FieldApi)it.next();
      if (fieldApi.getName().equals(name) && fieldApi.getType().equals(type))
        return null;
    }
    FieldApi fieldUseClone = ApiFactory.eINSTANCE.createFieldApi();
    fieldUseClone.setName(name);
    fieldUseClone.setType(type);
    return fieldUseClone;
  }

  private ClassApi newClassApi(String typeName, boolean ref, boolean subclass, boolean implement, boolean instantiate)
  {
    ClassApi classApi = ApiFactory.eINSTANCE.createClassApi();
    classApi.setName(typeName);
    classApi.setReference(ref);
    classApi.setSubclass(subclass);
    classApi.setImplement(implement);
    classApi.setInstantiate(instantiate);
    return classApi;
  }

  private boolean inputTypeEquals(List api, List use)
  {
    if (api.size() == use.size())
    {
      int size = api.size();
      for (int i = 0; i < size; i++)
        if (!api.get(i).equals(use.get(i)))
          return false;
      return true;
    }
    return false;
  }

  private void saveAPIViolations(ComponentUseType apiViolations, File file) throws IOException
  {
    ResourceSet res = new ResourceSetImpl();
    res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentviolation", new UseResourceFactoryImpl());
    res.getPackageRegistry().put(UsePackage.eNS_URI, UsePackage.eINSTANCE);
    UseResourceImpl useRes = (UseResourceImpl)res.createResource(URI.createURI("*.componentviolation"));
    org.eclipse.component.use.DocumentRoot root = UseFactory.eINSTANCE.createDocumentRoot();
    root.setComponentUse(apiViolations);
    useRes.getContents().add(root);
    useRes.save(new BufferedOutputStream(new FileOutputStream(file)), new HashMap());
  }
}
