/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TestMain
{
  public static void main(String[] args) throws IOException
  {
    String compDirLoc = "C:/Documents and Settings/chng1me.T40-92U-V46/Desktop/api-test-data/";
    String eclipseDirLoc = "D:/eclipse_v31m4/eclipse/";
    String compOutputLoc = compDirLoc + "org.eclipse.jdt/";
    String reportLoc = compDirLoc;

    List pluginIds = new ArrayList();
    pluginIds.add("org.eclipse.jdt.core");
    pluginIds.add("org.eclipse.jdt.debug.ui");
    pluginIds.add("org.eclipse.jdt.debug");
    pluginIds.add("org.eclipse.jdt.doc.isv");
    pluginIds.add("org.eclipse.jdt.doc.user");
    pluginIds.add("org.eclipse.jdt.junit.runtime");
    pluginIds.add("org.eclipse.jdt.junit");
    pluginIds.add("org.eclipse.jdt.launching");
    pluginIds.add("org.eclipse.jdt.source");
    pluginIds.add("org.eclipse.jdt.ui");
    pluginIds.add("org.eclipse.jdt");
    ComponentEmitter compEmitter = new ComponentEmitter(compOutputLoc, eclipseDirLoc, "JDT", pluginIds);
    compEmitter.genComponentXML();
    compEmitter = null;

    ComponentAPIEmitter compApiEmitter = new ComponentAPIEmitter(compDirLoc, eclipseDirLoc);
    compApiEmitter.genComponentApiXml();
    compApiEmitter.genComponentUseXML();
    Map comps = compApiEmitter.getComponents();
    compApiEmitter = null;

    APIViolationEmitter violationEmitter = new APIViolationEmitter(compDirLoc, reportLoc);
    violationEmitter.setComponents(comps);
    violationEmitter.genAPIViolationReport();
  }
}
