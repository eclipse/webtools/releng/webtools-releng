/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.component.internalreference;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import org.eclipse.component.location.ILocation;

/**
 * Creates model objects from configuration files.  Utilizes the
 * xerces parser to read the xml.
 */
public class ConfigurationFileParser {

	private static SAXParser saxParser;
	
	static {
		initializeParser();
	}

	private static void initializeParser() {
		try {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      factory.setNamespaceAware(false);
      factory.setValidating(false);
	 		factory.setFeature("http://xml.org/sax/features/string-interning", true); //$NON-NLS-1$
      saxParser = factory.newSAXParser();
		} catch (SAXException e) {
			// In case support for this feature is removed
		} catch (ParserConfigurationException pce) {
      pce.printStackTrace();  
    }
	}

	/**
	 * Creates a <code>Plugin</code> from a location file 
	 * @param location a location that points to a plugin.xml file, not <code>null</code>.
	 * @return Plugin the Plugin object representation of that file
	 */	
	public static Plugin getPlugin(ILocation location) {
		PluginHandler handler= new PluginHandler(location);
		try {
			parse(location, handler);
		} catch (IOException e) {
			System.err.println("Could not read " + location.getName() + ", skipping");
		}
		return handler.getPlugin();
	}

	/**
	 * Creates a <code>Fragment</code> from a location file 
	 * @param location a location that points to a fragment.xml file, not <code>null</code>.
	 * @return Fragment the Fragment object representation of that file
	 */
	public static Fragment getFragment(ILocation location) {
		FragmentHandler handler= new FragmentHandler(location);
		try {
			parse(location, handler);
		} catch (IOException e) {
			System.err.println("Could not read " + location.getName() + ", skipping");
		}
		return handler.getFragment();
	}	


	private static void parse(ILocation location, DefaultHandler handler) throws IOException {
		//saxParser.setContentHandler(handler);
		//saxParser.setDTDHandler(handler);
		//saxParser.setEntityResolver(handler);
		//saxParser.setErrorHandler(handler);
		InputStream in= null;
		try {
			in= location.getInputStream();
			saxParser.parse(new InputSource(in), handler);
		} catch (SAXException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private static String getNameAndVersion(Attributes attributes) {
		StringBuffer b= new StringBuffer();
		b.append(attributes.getValue("id"));
		b.append('_');
		b.append(attributes.getValue("version"));
		return b.toString();
	}

	private static class PluginHandler extends DefaultHandler {
		private Plugin plugin;
		
		public PluginHandler(ILocation location) {
			plugin= new Plugin(location);
		}
		
		public Plugin getPlugin() {
			return plugin;
		}

		public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException {
			if(elementName.equals("plugin") || qName.equals("plugin")) {
        plugin.setName(attributes.getValue("id"));
				plugin.setVersion(attributes.getValue("version"));
				return;
			}			
	
			if(elementName.equals("library") || qName.equals("library")) {
				plugin.addLibrary(attributes.getValue("name"));
			}
		}
	}

	private static class FragmentHandler extends DefaultHandler {
		private Fragment fragment;
		
		public FragmentHandler(ILocation location) {
			fragment= new Fragment(location);
		}
		
		public Fragment getFragment() {
			return fragment;
		}
		
		
		public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException {
			if(elementName.equals("fragment") || qName.equals("fragment")) { 
				fragment.setFragmentName(attributes.getValue("id"));
				fragment.setVersion(attributes.getValue("version"));
				fragment.setName(attributes.getValue("plugin-id"));
				fragment.setVersion(attributes.getValue("plugin-version"));
				return;
			}			
	
			if(elementName.equals("library") || qName.equals("library")){
				fragment.addLibrary(attributes.getValue("name"));
				return;
			}
		}
	}
}
