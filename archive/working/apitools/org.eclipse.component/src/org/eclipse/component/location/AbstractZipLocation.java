/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.component.location;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarInputStream;
import java.util.zip.ZipInputStream;

/**
 * The <code>AbstractZipLocation</code> is a location representing 
 * a zip entry or a zip file.
 */
public abstract class AbstractZipLocation extends Location {
	protected static final byte[] fBuffer= new byte[8192];
	protected static final ByteArrayOutputStream fBytesOut= new ByteArrayOutputStream(8192);

	public static final String JAR = "jar";

	/*
	 * @see com.example.location.Location#Location(com.example.location.ILocation, java.lang.String)
	 */
	public AbstractZipLocation(ILocation parent, String fullName) {
		super(parent, fullName);
	}

	/*
	 * @see com.example.location.ILocation#createChild(java.lang.String)
	 */
	public ILocation createChild(String relativePath) {
		return new ZipEntryLocation(this, relativePath);
	}

	/*
	 * @see com.example.location.ILocation#createSibling(java.lang.String)
	 */
	public ILocation createSibling(String relativePath) {
		String path= getName();
		int index= path.lastIndexOf('/');
		if (index < 0) {
			return parent.createChild(relativePath);
		} else {
			path= path.substring(0, index + 1) + relativePath;
			return parent.createChild(path);
		}
	}

	/*
	 * Get a ZipInputStream from the given stream.
	 *  
	 * @param inputStream	An input stream, or <code>null</code>.
	 * @return ZipInputStream A ZipInputStream, or a JarInputStream depending
	 * 	on the name of this location.
	 */
	protected ZipInputStream getZipInputStream(InputStream inputStream) {
		if (inputStream == null)
			return null;
		ZipInputStream zipIn= null;
		if (Location.getExtension(getName()).equalsIgnoreCase(JAR)) {
			try {
				zipIn= new JarInputStream(inputStream);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			zipIn= new ZipInputStream(inputStream);
		}
		return zipIn;
	}

	/*
	 * Reads all of the bytes from the input stream, returning the byte array.
	 * 
	 * @param in	An input stream to read.
	 * @return byte[]	The resulting bytes.
	 * @throws IOException	if the input stream was unable to be read.
	 */
	protected byte[] readAllBytes(InputStream in) throws IOException {
		fBytesOut.reset();
		for(int read= in.read(fBuffer); read != -1; read= in.read(fBuffer)) {
			fBytesOut.write(fBuffer, 0, read);
		}
		return fBytesOut.toByteArray();
	}

	/**
	 * Answers the InputStream for the child with the given name. 
	 * @param name	The child name, not <code>null</code>
	 * @return InputStream The contents of the child
	 */
	protected abstract InputStream getChildInputStream(String name);
}
