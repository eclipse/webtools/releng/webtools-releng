/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.component.location;

/**
 * An <code>ILocationVisitor</code> can be used to traverse a tree
 * of locations.
 */
public interface ILocationVisitor {
	/**
	 * Allows this visitor to investigate the given location.
	 * 
	 * @param location the current location in the traversal
	 * @return boolean <code>true</code> if the traversal should
	 * 	continue into the children of the given location, <code>false</code>
	 * 	otherwise.
	 */
	boolean accept(ILocation location);
}
