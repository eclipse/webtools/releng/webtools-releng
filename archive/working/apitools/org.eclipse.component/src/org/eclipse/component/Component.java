/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.eclipse.component.ComponentPackage;
import org.eclipse.component.ComponentType;
import org.eclipse.component.DocumentRoot;
import org.eclipse.component.api.ApiPackage;
import org.eclipse.component.api.ComponentApiType;
import org.eclipse.component.api.util.ApiResourceFactoryImpl;
import org.eclipse.component.api.util.ApiResourceImpl;
import org.eclipse.component.use.ComponentUseType;
import org.eclipse.component.use.UsePackage;
import org.eclipse.component.use.util.UseResourceFactoryImpl;
import org.eclipse.component.use.util.UseResourceImpl;
import org.eclipse.component.util.ComponentResourceFactoryImpl;
import org.eclipse.component.util.ComponentResourceImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class Component
{
  public static final String CONST_COMPONENT_XML = "component.xml";
  public static final String CONST_COMPONENT_API_XML = "component-api.xml";
  public static final String CONST_COMPONENT_USE_XML = "component-use.xml";

  private String compLoc;
  private ComponentType compXML;
  private ComponentApiType compApiXml;
  private ComponentUseType compUseXML;

  /**
   * @return Returns the compLoc.
   */
  public String getCompLoc()
  {
    return compLoc;
  }

  /**
   * @param compLoc The compLoc to set.
   */
  public void setCompLoc(String compLoc)
  {
    this.compLoc = compLoc;
  }

  /**
   * @return Returns the compXML.
   */
  public ComponentType getCompXML()
  {
    if (compXML == null)
    {
      try
      {
        String compXMLLoc = compLoc + CONST_COMPONENT_XML;
        FileInputStream fis = new FileInputStream(compXMLLoc);
        ResourceSet res = new ResourceSetImpl();
        res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentxml", new ComponentResourceFactoryImpl());
        res.getPackageRegistry().put(ComponentPackage.eNS_URI, ComponentPackage.eINSTANCE);
        ComponentResourceImpl compRes = (ComponentResourceImpl)res.createResource(URI.createURI("*.componentxml"));
        compRes.load(fis, res.getLoadOptions());
        compRes.setURI(URI.createURI(compXMLLoc));
        EList contents = compRes.getContents();
        if (contents.size() == 1 && contents.get(0) instanceof DocumentRoot)
          compXML = ((DocumentRoot)contents.get(0)).getComponent();
      }
      catch (FileNotFoundException fnfe)
      {
        fnfe.printStackTrace();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
    }
    return compXML;
  }

  /**
   * @param compXML The compXML to set.
   */
  public void setCompXML(ComponentType compXML)
  {
    this.compXML = compXML;
  }

  /**
   * @return Returns the compApiXml.
   */
  public ComponentApiType getCompApiXml()
  {
    if (compApiXml == null)
    {
      try
      {
        String compApiXmlLoc = compLoc + CONST_COMPONENT_API_XML;
        FileInputStream fis = new FileInputStream(compApiXmlLoc);
        ResourceSet res = new ResourceSetImpl();
        res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentapixml", new ApiResourceFactoryImpl());
        res.getPackageRegistry().put(ApiPackage.eNS_URI, ApiPackage.eINSTANCE);
        ApiResourceImpl apiRes = (ApiResourceImpl)res.createResource(URI.createURI("*.componentapixml"));
        apiRes.load(fis, res.getLoadOptions());
        apiRes.setURI(URI.createURI(compApiXmlLoc));
        EList contents = apiRes.getContents();
        if (contents.size() == 1 && contents.get(0) instanceof org.eclipse.component.api.DocumentRoot)
          compApiXml = ((org.eclipse.component.api.DocumentRoot)contents.get(0)).getComponentApi();
      }
      catch (FileNotFoundException fnfe)
      {
        fnfe.printStackTrace();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
    }
    return compApiXml;
  }

  /**
   * @param compApiXml The compApiXml to set.
   */
  public void setCompApiXml(ComponentApiType compApiXml)
  {
    this.compApiXml = compApiXml;
  }

  /**
   * @return Returns the compUseXML.
   */
  public ComponentUseType getCompUseXML()
  {
    if (compUseXML == null)
    {
      try
      {
        String compUseXmlLoc = compLoc + CONST_COMPONENT_USE_XML;
        FileInputStream fis = new FileInputStream(compUseXmlLoc);
        ResourceSet res = new ResourceSetImpl();
        res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentusexml", new UseResourceFactoryImpl());
        res.getPackageRegistry().put(UsePackage.eNS_URI, UsePackage.eINSTANCE);
        UseResourceImpl useRes = (UseResourceImpl)res.createResource(URI.createURI("*.componentusexml"));
        useRes.load(fis, res.getLoadOptions());
        useRes.setURI(URI.createURI(compUseXmlLoc));
        EList contents = useRes.getContents();
        if (contents.size() == 1 && contents.get(0) instanceof org.eclipse.component.use.DocumentRoot)
          compUseXML = ((org.eclipse.component.use.DocumentRoot)contents.get(0)).getComponentUse();
      }
      catch (FileNotFoundException fnfe)
      {
        fnfe.printStackTrace();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
    }
    return compUseXML;
  }

  /**
   * @param compUseXML The compUseXML to set.
   */
  public void setCompUseXML(ComponentUseType compUseXML)
  {
    this.compUseXML = compUseXML;
  }
}
