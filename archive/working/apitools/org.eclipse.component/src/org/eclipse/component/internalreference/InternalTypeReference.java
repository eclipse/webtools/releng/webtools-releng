/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.component.internalreference;

/**
 * 
 */
public class InternalTypeReference {
	private String typeName;
	private String pluginName;
	
	/**
	 * Creates a new <code>InternalTypeReference</code>.
	 * @param typeName		the name of the referenced type
	 * @param pluginName	the name of the plugin which owns the type, 
	 * 	or <code>null</code>.
	 */
	public InternalTypeReference(String typeName, String pluginName) {
		this.typeName= typeName;
		this.pluginName= pluginName;
	}

	/**
	 * @return String the name of the plugin which owns the type,
	 *  or <code>null</code>.
	 */
	public String getPluginName() {
		return pluginName;
	}

	/**
	 * @return String the name of the referenced type, not <code>null</code>.
	 */
	public String getTypeName() {
		return typeName;
	}
}
