/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.component;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.component.internalreference.ConfigurationFileParser;
import org.eclipse.component.internalreference.Fragment;
import org.eclipse.component.internalreference.Library;
import org.eclipse.component.internalreference.Plugin;
import org.eclipse.component.location.FileLocation;
import org.eclipse.component.util.ComponentResourceFactoryImpl;
import org.eclipse.component.util.ComponentResourceImpl;
import org.eclipse.component.ComponentType;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class ComponentEmitter
{
  private static final String CONST_COMPONENT_XML = "component.xml";
  private String compDirLoc;
  private String eclipseDirLoc;
  private String componentName;
  private List pluginIds;
  private Map pluginId2Plugin;
  private Map fragmentId2Fragment;

  public ComponentEmitter(String compDirLoc, String eclipseDirLoc, String componentName, List pluginIds)
  {
    this.compDirLoc = compDirLoc;
    this.eclipseDirLoc = eclipseDirLoc;
    this.pluginIds = pluginIds;
    this.componentName = componentName;
    pluginId2Plugin = new HashMap();
    fragmentId2Fragment = new HashMap();
    File eclipseDir = new File(eclipseDirLoc);
    if (eclipseDir.exists() && eclipseDir.isDirectory())
      harvestPlugins(eclipseDir);
    linkPluginsAndFragments();
  }
  
  private void harvestPlugins(File file)
  {
    if (file.isDirectory())
    {
      File[] files = file.listFiles();
      for (int i = 0; i < files.length; i++)
        harvestPlugins(files[i]);
    }
    else if (Plugin.CONST_PLUGIN_XML.equalsIgnoreCase(file.getName()))
    {
      Plugin plugin = ConfigurationFileParser.getPlugin(new FileLocation(new FileLocation(file.getParentFile()), file.getName()));
      pluginId2Plugin.put(plugin.getName(), plugin);
    }
    else if (Fragment.CONST_FRAGMENT_XML.equalsIgnoreCase(file.getName()))
    {
      Fragment fragment = ConfigurationFileParser.getFragment(new FileLocation(new FileLocation(file.getParentFile()), file.getName()));
      fragmentId2Fragment.put(fragment.getFragmentName(), fragment);
    }
  }

  private void linkPluginsAndFragments()
  {
    for (Iterator it = fragmentId2Fragment.values().iterator(); it.hasNext();)
    {
      Fragment fragment = (Fragment)it.next();
      fragment.link(pluginId2Plugin);
    }
  }

  public void genComponentXML() throws IOException
  {
    ComponentType comp = getComponentType();
    EList plugins = comp.getPlugin();
    for (Iterator it = pluginIds.iterator(); it.hasNext();)
    {
      String pluginId = (String)it.next();
      Fragment fragment = (Fragment)fragmentId2Fragment.get(pluginId);
      if (fragment != null)
        addPluginType(comp, pluginId, true);
      else
      {
        Plugin plugin = (Plugin)pluginId2Plugin.get(pluginId);
        if (plugin != null)
        {
          addPluginType(comp, pluginId, false);
          List libs = plugin.getLibraries();
          for (Iterator libsIt = libs.iterator(); libsIt.hasNext();)
          {
            Library lib = (Library)libsIt.next();
            Map types = lib.getTypes();
            for (Iterator typesIt = types.keySet().iterator(); typesIt.hasNext();)
            {
              String typeName = (String)typesIt.next();
              int index = typeName.lastIndexOf('.');
              String pkgName;
              if (index != -1)
              {
                pkgName = typeName.substring(0, index);
                if (pkgName.indexOf("internal") == -1)
                  addPackageType(comp, pkgName);
              }
            }
          }
        }
      }
    }
    saveComponent(comp, new File(compDirLoc + CONST_COMPONENT_XML));
  }

  private PluginType addPluginType(ComponentType comp, String pluginId, boolean fragment)
  {
    EList plugins = comp.getPlugin();
    for (Iterator it = plugins.iterator(); it.hasNext();)
    {
      PluginType pluginType = (PluginType)it.next();
      if (pluginType.getId().equals(pluginId))
        return pluginType;
    }
    PluginType pluginType = ComponentFactory.eINSTANCE.createPluginType();
    pluginType.setId(pluginId);
    pluginType.setFragment(fragment);
    plugins.add(pluginType);
    return pluginType;
  }

  private PackageType addPackageType(ComponentType comp, String pkgName)
  {
    EList pkgs = comp.getPackage();
    for (Iterator it = pkgs.iterator(); it.hasNext();)
    {
      PackageType pkgType = (PackageType)it.next();
      if (pkgType.getName().equals(pkgName))
        return pkgType;
    }
    PackageType pkgType = ComponentFactory.eINSTANCE.createPackageType();
    pkgType.setName(pkgName);
    pkgs.add(pkgType);
    return pkgType;
  }

  private ComponentType getComponentType()
  {
    try
    {
      String compXMLLoc = compDirLoc + CONST_COMPONENT_XML;
      File f = new File(compXMLLoc);
      if (f.exists())
      {
        FileInputStream fis = new FileInputStream(f);
        ResourceSet res = new ResourceSetImpl();
        res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentxml", new ComponentResourceFactoryImpl());
        res.getPackageRegistry().put(ComponentPackage.eNS_URI, ComponentPackage.eINSTANCE);
        ComponentResourceImpl compRes = (ComponentResourceImpl)res.createResource(URI.createURI("*.componentxml"));
        compRes.load(fis, res.getLoadOptions());
        compRes.setURI(URI.createURI(compXMLLoc));
        EList contents = compRes.getContents();
        if (contents.size() == 1 && contents.get(0) instanceof DocumentRoot)
          return ((DocumentRoot)contents.get(0)).getComponent();
      }
    }
    catch (FileNotFoundException fnfe)
    {
      fnfe.printStackTrace();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
    ComponentType comp = ComponentFactory.eINSTANCE.createComponentType();
    comp.setName(componentName);
    ComponentDependsType depends = ComponentFactory.eINSTANCE.createComponentDependsType();
    depends.setUnrestricted(false);
    comp.setComponentDepends(depends);
    return comp;
  }

  private void saveComponent(ComponentType comp, File file) throws IOException
  {
    ResourceSet res = new ResourceSetImpl();
    res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentxml", new ComponentResourceFactoryImpl());
    res.getPackageRegistry().put(ComponentPackage.eNS_URI, ComponentPackage.eINSTANCE);
    ComponentResourceImpl compRes = (ComponentResourceImpl)res.createResource(URI.createURI("*.componentxml"));
    DocumentRoot root = ComponentFactory.eINSTANCE.createDocumentRoot();
    root.setComponent(comp);
    compRes.getContents().add(root);
    compRes.save(new BufferedOutputStream(new FileOutputStream(file)), new HashMap());
  }
}
