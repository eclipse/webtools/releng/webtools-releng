/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.component.internalreference;

/**
 * An <code>IInternalRules</code> object is responsible for determining
 * if references within types are considered internal.  This interface
 * allows clients to customize of the internal reference tool to 
 * reflect their internal API rules. 
 */
public interface IInternalRules {
	/**
	 * Answers <code>true</code> if this set of rules dictates that 
	 * the given reference is a reference to an internal type.
	 * 
	 * @param referencedType	the type reference that may be internal
	 * @param type				the type from which the reference was made
	 * @param library			the library from which the reference was made
	 * @param plugin			the plugin from which the reference was made
	 * @return boolean			<code>true</code> if the reference is an internal reference
	 */
	boolean isInternal(String referencedType, Type type, Library library, Plugin plugin);
}
