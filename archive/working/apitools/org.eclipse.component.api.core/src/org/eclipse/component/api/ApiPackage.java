/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ApiPackage.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.api;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.component.api.ApiFactory
 * @generated
 */
public interface ApiPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "api";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://eclipse.org/component-api";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "api";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  ApiPackage eINSTANCE = org.eclipse.component.api.impl.ApiPackageImpl.init();

  /**
   * The meta object id for the '{@link org.eclipse.component.api.impl.ApiTypesImpl <em>Types</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.api.impl.ApiTypesImpl
   * @see org.eclipse.component.api.impl.ApiPackageImpl#getApiTypes()
   * @generated
   */
  int API_TYPES = 0;

  /**
   * The feature id for the '<em><b>Package</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int API_TYPES__PACKAGE = 0;

  /**
   * The number of structural features of the the '<em>Types</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int API_TYPES_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.eclipse.component.api.impl.ClassApiImpl <em>Class Api</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.api.impl.ClassApiImpl
   * @see org.eclipse.component.api.impl.ApiPackageImpl#getClassApi()
   * @generated
   */
  int CLASS_API = 1;

  /**
   * The feature id for the '<em><b>Method Api</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_API__METHOD_API = 0;

  /**
   * The feature id for the '<em><b>Field Api</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_API__FIELD_API = 1;

  /**
   * The feature id for the '<em><b>Implement</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_API__IMPLEMENT = 2;

  /**
   * The feature id for the '<em><b>Instantiate</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_API__INSTANTIATE = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_API__NAME = 4;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_API__REFERENCE = 5;

  /**
   * The feature id for the '<em><b>Subclass</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_API__SUBCLASS = 6;

  /**
   * The number of structural features of the the '<em>Class Api</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLASS_API_FEATURE_COUNT = 7;

  /**
   * The meta object id for the '{@link org.eclipse.component.api.impl.ComponentApiTypeImpl <em>Component Api Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.api.impl.ComponentApiTypeImpl
   * @see org.eclipse.component.api.impl.ApiPackageImpl#getComponentApiType()
   * @generated
   */
  int COMPONENT_API_TYPE = 2;

  /**
   * The feature id for the '<em><b>Internal Apis</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_API_TYPE__INTERNAL_APIS = 0;

  /**
   * The feature id for the '<em><b>External Apis</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_API_TYPE__EXTERNAL_APIS = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_API_TYPE__NAME = 2;

  /**
   * The feature id for the '<em><b>Version</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_API_TYPE__VERSION = 3;

  /**
   * The number of structural features of the the '<em>Component Api Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_API_TYPE_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.eclipse.component.api.impl.DocumentRootImpl <em>Document Root</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.api.impl.DocumentRootImpl
   * @see org.eclipse.component.api.impl.ApiPackageImpl#getDocumentRoot()
   * @generated
   */
  int DOCUMENT_ROOT = 3;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__MIXED = 0;

  /**
   * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

  /**
   * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

  /**
   * The feature id for the '<em><b>Component Api</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__COMPONENT_API = 3;

  /**
   * The number of structural features of the the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.eclipse.component.api.impl.FieldApiImpl <em>Field Api</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.api.impl.FieldApiImpl
   * @see org.eclipse.component.api.impl.ApiPackageImpl#getFieldApi()
   * @generated
   */
  int FIELD_API = 4;

  /**
   * The feature id for the '<em><b>Final</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_API__FINAL = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_API__NAME = 1;

  /**
   * The feature id for the '<em><b>Static</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_API__STATIC = 2;

  /**
   * The feature id for the '<em><b>Transient</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_API__TRANSIENT = 3;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_API__TYPE = 4;

  /**
   * The feature id for the '<em><b>Visibility</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_API__VISIBILITY = 5;

  /**
   * The feature id for the '<em><b>Volatile</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_API__VOLATILE = 6;

  /**
   * The number of structural features of the the '<em>Field Api</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_API_FEATURE_COUNT = 7;

  /**
   * The meta object id for the '{@link org.eclipse.component.api.impl.MethodApiImpl <em>Method Api</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.api.impl.MethodApiImpl
   * @see org.eclipse.component.api.impl.ApiPackageImpl#getMethodApi()
   * @generated
   */
  int METHOD_API = 5;

  /**
   * The feature id for the '<em><b>Abstract</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API__ABSTRACT = 0;

  /**
   * The feature id for the '<em><b>Exception Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API__EXCEPTION_TYPE = 1;

  /**
   * The feature id for the '<em><b>Final</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API__FINAL = 2;

  /**
   * The feature id for the '<em><b>Input Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API__INPUT_TYPE = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API__NAME = 4;

  /**
   * The feature id for the '<em><b>Native</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API__NATIVE = 5;

  /**
   * The feature id for the '<em><b>Return Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API__RETURN_TYPE = 6;

  /**
   * The feature id for the '<em><b>Static</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API__STATIC = 7;

  /**
   * The feature id for the '<em><b>Strict</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API__STRICT = 8;

  /**
   * The feature id for the '<em><b>Synchronized</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API__SYNCHRONIZED = 9;

  /**
   * The feature id for the '<em><b>Visibility</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API__VISIBILITY = 10;

  /**
   * The number of structural features of the the '<em>Method Api</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_API_FEATURE_COUNT = 11;

  /**
   * The meta object id for the '{@link org.eclipse.component.api.impl.PackageImpl <em>Package</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.api.impl.PackageImpl
   * @see org.eclipse.component.api.impl.ApiPackageImpl#getPackage()
   * @generated
   */
  int PACKAGE = 6;

  /**
   * The feature id for the '<em><b>Class Api</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE__CLASS_API = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE__NAME = 1;

  /**
   * The number of structural features of the the '<em>Package</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.eclipse.component.api.Visibility <em>Visibility</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.api.Visibility
   * @see org.eclipse.component.api.impl.ApiPackageImpl#getVisibility()
   * @generated
   */
  int VISIBILITY = 7;

  /**
   * The meta object id for the '<em>List Of Types</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.util.List
   * @see org.eclipse.component.api.impl.ApiPackageImpl#getListOfTypes()
   * @generated
   */
  int LIST_OF_TYPES = 8;

  /**
   * The meta object id for the '<em>Visibility Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.api.Visibility
   * @see org.eclipse.component.api.impl.ApiPackageImpl#getVisibilityObject()
   * @generated
   */
  int VISIBILITY_OBJECT = 9;


  /**
   * Returns the meta object for class '{@link org.eclipse.component.api.ApiTypes <em>Types</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Types</em>'.
   * @see org.eclipse.component.api.ApiTypes
   * @generated
   */
  EClass getApiTypes();

  /**
   * Returns the meta object for the containment reference list '{@link org.eclipse.component.api.ApiTypes#getPackage <em>Package</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Package</em>'.
   * @see org.eclipse.component.api.ApiTypes#getPackage()
   * @see #getApiTypes()
   * @generated
   */
  EReference getApiTypes_Package();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.api.ClassApi <em>Class Api</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Class Api</em>'.
   * @see org.eclipse.component.api.ClassApi
   * @generated
   */
  EClass getClassApi();

  /**
   * Returns the meta object for the containment reference list '{@link org.eclipse.component.api.ClassApi#getMethodApi <em>Method Api</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Method Api</em>'.
   * @see org.eclipse.component.api.ClassApi#getMethodApi()
   * @see #getClassApi()
   * @generated
   */
  EReference getClassApi_MethodApi();

  /**
   * Returns the meta object for the containment reference list '{@link org.eclipse.component.api.ClassApi#getFieldApi <em>Field Api</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Field Api</em>'.
   * @see org.eclipse.component.api.ClassApi#getFieldApi()
   * @see #getClassApi()
   * @generated
   */
  EReference getClassApi_FieldApi();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.ClassApi#isImplement <em>Implement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Implement</em>'.
   * @see org.eclipse.component.api.ClassApi#isImplement()
   * @see #getClassApi()
   * @generated
   */
  EAttribute getClassApi_Implement();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.ClassApi#isInstantiate <em>Instantiate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Instantiate</em>'.
   * @see org.eclipse.component.api.ClassApi#isInstantiate()
   * @see #getClassApi()
   * @generated
   */
  EAttribute getClassApi_Instantiate();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.ClassApi#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.eclipse.component.api.ClassApi#getName()
   * @see #getClassApi()
   * @generated
   */
  EAttribute getClassApi_Name();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.ClassApi#isReference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Reference</em>'.
   * @see org.eclipse.component.api.ClassApi#isReference()
   * @see #getClassApi()
   * @generated
   */
  EAttribute getClassApi_Reference();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.ClassApi#isSubclass <em>Subclass</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Subclass</em>'.
   * @see org.eclipse.component.api.ClassApi#isSubclass()
   * @see #getClassApi()
   * @generated
   */
  EAttribute getClassApi_Subclass();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.api.ComponentApiType <em>Component Api Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component Api Type</em>'.
   * @see org.eclipse.component.api.ComponentApiType
   * @generated
   */
  EClass getComponentApiType();

  /**
   * Returns the meta object for the containment reference '{@link org.eclipse.component.api.ComponentApiType#getInternalApis <em>Internal Apis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Internal Apis</em>'.
   * @see org.eclipse.component.api.ComponentApiType#getInternalApis()
   * @see #getComponentApiType()
   * @generated
   */
  EReference getComponentApiType_InternalApis();

  /**
   * Returns the meta object for the containment reference '{@link org.eclipse.component.api.ComponentApiType#getExternalApis <em>External Apis</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>External Apis</em>'.
   * @see org.eclipse.component.api.ComponentApiType#getExternalApis()
   * @see #getComponentApiType()
   * @generated
   */
  EReference getComponentApiType_ExternalApis();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.ComponentApiType#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.eclipse.component.api.ComponentApiType#getName()
   * @see #getComponentApiType()
   * @generated
   */
  EAttribute getComponentApiType_Name();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.ComponentApiType#getVersion <em>Version</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Version</em>'.
   * @see org.eclipse.component.api.ComponentApiType#getVersion()
   * @see #getComponentApiType()
   * @generated
   */
  EAttribute getComponentApiType_Version();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.api.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document Root</em>'.
   * @see org.eclipse.component.api.DocumentRoot
   * @generated
   */
  EClass getDocumentRoot();

  /**
   * Returns the meta object for the attribute list '{@link org.eclipse.component.api.DocumentRoot#getMixed <em>Mixed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Mixed</em>'.
   * @see org.eclipse.component.api.DocumentRoot#getMixed()
   * @see #getDocumentRoot()
   * @generated
   */
  EAttribute getDocumentRoot_Mixed();

  /**
   * Returns the meta object for the map '{@link org.eclipse.component.api.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
   * @see org.eclipse.component.api.DocumentRoot#getXMLNSPrefixMap()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XMLNSPrefixMap();

  /**
   * Returns the meta object for the map '{@link org.eclipse.component.api.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XSI Schema Location</em>'.
   * @see org.eclipse.component.api.DocumentRoot#getXSISchemaLocation()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XSISchemaLocation();

  /**
   * Returns the meta object for the containment reference '{@link org.eclipse.component.api.DocumentRoot#getComponentApi <em>Component Api</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component Api</em>'.
   * @see org.eclipse.component.api.DocumentRoot#getComponentApi()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_ComponentApi();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.api.FieldApi <em>Field Api</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Api</em>'.
   * @see org.eclipse.component.api.FieldApi
   * @generated
   */
  EClass getFieldApi();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.FieldApi#isFinal <em>Final</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Final</em>'.
   * @see org.eclipse.component.api.FieldApi#isFinal()
   * @see #getFieldApi()
   * @generated
   */
  EAttribute getFieldApi_Final();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.FieldApi#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.eclipse.component.api.FieldApi#getName()
   * @see #getFieldApi()
   * @generated
   */
  EAttribute getFieldApi_Name();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.FieldApi#isStatic <em>Static</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Static</em>'.
   * @see org.eclipse.component.api.FieldApi#isStatic()
   * @see #getFieldApi()
   * @generated
   */
  EAttribute getFieldApi_Static();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.FieldApi#isTransient <em>Transient</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Transient</em>'.
   * @see org.eclipse.component.api.FieldApi#isTransient()
   * @see #getFieldApi()
   * @generated
   */
  EAttribute getFieldApi_Transient();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.FieldApi#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see org.eclipse.component.api.FieldApi#getType()
   * @see #getFieldApi()
   * @generated
   */
  EAttribute getFieldApi_Type();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.FieldApi#getVisibility <em>Visibility</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Visibility</em>'.
   * @see org.eclipse.component.api.FieldApi#getVisibility()
   * @see #getFieldApi()
   * @generated
   */
  EAttribute getFieldApi_Visibility();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.FieldApi#isVolatile <em>Volatile</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Volatile</em>'.
   * @see org.eclipse.component.api.FieldApi#isVolatile()
   * @see #getFieldApi()
   * @generated
   */
  EAttribute getFieldApi_Volatile();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.api.MethodApi <em>Method Api</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Method Api</em>'.
   * @see org.eclipse.component.api.MethodApi
   * @generated
   */
  EClass getMethodApi();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.MethodApi#isAbstract <em>Abstract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Abstract</em>'.
   * @see org.eclipse.component.api.MethodApi#isAbstract()
   * @see #getMethodApi()
   * @generated
   */
  EAttribute getMethodApi_Abstract();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.MethodApi#getExceptionType <em>Exception Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Exception Type</em>'.
   * @see org.eclipse.component.api.MethodApi#getExceptionType()
   * @see #getMethodApi()
   * @generated
   */
  EAttribute getMethodApi_ExceptionType();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.MethodApi#isFinal <em>Final</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Final</em>'.
   * @see org.eclipse.component.api.MethodApi#isFinal()
   * @see #getMethodApi()
   * @generated
   */
  EAttribute getMethodApi_Final();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.MethodApi#getInputType <em>Input Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Input Type</em>'.
   * @see org.eclipse.component.api.MethodApi#getInputType()
   * @see #getMethodApi()
   * @generated
   */
  EAttribute getMethodApi_InputType();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.MethodApi#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.eclipse.component.api.MethodApi#getName()
   * @see #getMethodApi()
   * @generated
   */
  EAttribute getMethodApi_Name();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.MethodApi#isNative <em>Native</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Native</em>'.
   * @see org.eclipse.component.api.MethodApi#isNative()
   * @see #getMethodApi()
   * @generated
   */
  EAttribute getMethodApi_Native();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.MethodApi#getReturnType <em>Return Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Return Type</em>'.
   * @see org.eclipse.component.api.MethodApi#getReturnType()
   * @see #getMethodApi()
   * @generated
   */
  EAttribute getMethodApi_ReturnType();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.MethodApi#isStatic <em>Static</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Static</em>'.
   * @see org.eclipse.component.api.MethodApi#isStatic()
   * @see #getMethodApi()
   * @generated
   */
  EAttribute getMethodApi_Static();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.MethodApi#isStrict <em>Strict</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Strict</em>'.
   * @see org.eclipse.component.api.MethodApi#isStrict()
   * @see #getMethodApi()
   * @generated
   */
  EAttribute getMethodApi_Strict();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.MethodApi#isSynchronized <em>Synchronized</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Synchronized</em>'.
   * @see org.eclipse.component.api.MethodApi#isSynchronized()
   * @see #getMethodApi()
   * @generated
   */
  EAttribute getMethodApi_Synchronized();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.MethodApi#getVisibility <em>Visibility</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Visibility</em>'.
   * @see org.eclipse.component.api.MethodApi#getVisibility()
   * @see #getMethodApi()
   * @generated
   */
  EAttribute getMethodApi_Visibility();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.api.Package <em>Package</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Package</em>'.
   * @see org.eclipse.component.api.Package
   * @generated
   */
  EClass getPackage();

  /**
   * Returns the meta object for the containment reference list '{@link org.eclipse.component.api.Package#getClassApi <em>Class Api</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Class Api</em>'.
   * @see org.eclipse.component.api.Package#getClassApi()
   * @see #getPackage()
   * @generated
   */
  EReference getPackage_ClassApi();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.api.Package#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.eclipse.component.api.Package#getName()
   * @see #getPackage()
   * @generated
   */
  EAttribute getPackage_Name();

  /**
   * Returns the meta object for enum '{@link org.eclipse.component.api.Visibility <em>Visibility</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Visibility</em>'.
   * @see org.eclipse.component.api.Visibility
   * @generated
   */
  EEnum getVisibility();

  /**
   * Returns the meta object for data type '{@link java.util.List <em>List Of Types</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>List Of Types</em>'.
   * @see java.util.List
   * @model instanceClass="java.util.List"
   * @generated
   */
  EDataType getListOfTypes();

  /**
   * Returns the meta object for data type '{@link org.eclipse.component.api.Visibility <em>Visibility Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Visibility Object</em>'.
   * @see org.eclipse.component.api.Visibility
   * @model instanceClass="org.eclipse.component.api.Visibility"
   * @generated
   */
  EDataType getVisibilityObject();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  ApiFactory getApiFactory();

} //ApiPackage
