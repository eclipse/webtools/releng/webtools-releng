/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentApiTypeImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.api.impl;

import org.eclipse.component.api.ApiPackage;
import org.eclipse.component.api.ApiTypes;
import org.eclipse.component.api.ComponentApiType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Api Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.component.api.impl.ComponentApiTypeImpl#getInternalApis <em>Internal Apis</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.ComponentApiTypeImpl#getExternalApis <em>External Apis</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.ComponentApiTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.ComponentApiTypeImpl#getVersion <em>Version</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComponentApiTypeImpl extends EObjectImpl implements ComponentApiType
{
  /**
   * The cached value of the '{@link #getInternalApis() <em>Internal Apis</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInternalApis()
   * @generated
   * @ordered
   */
  protected ApiTypes internalApis = null;

  /**
   * The cached value of the '{@link #getExternalApis() <em>External Apis</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExternalApis()
   * @generated
   * @ordered
   */
  protected ApiTypes externalApis = null;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVersion()
   * @generated
   * @ordered
   */
  protected static final String VERSION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVersion()
   * @generated
   * @ordered
   */
  protected String version = VERSION_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComponentApiTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EClass eStaticClass()
  {
    return ApiPackage.eINSTANCE.getComponentApiType();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ApiTypes getInternalApis()
  {
    return internalApis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetInternalApis(ApiTypes newInternalApis, NotificationChain msgs)
  {
    ApiTypes oldInternalApis = internalApis;
    internalApis = newInternalApis;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApiPackage.COMPONENT_API_TYPE__INTERNAL_APIS, oldInternalApis, newInternalApis);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInternalApis(ApiTypes newInternalApis)
  {
    if (newInternalApis != internalApis)
    {
      NotificationChain msgs = null;
      if (internalApis != null)
        msgs = ((InternalEObject)internalApis).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApiPackage.COMPONENT_API_TYPE__INTERNAL_APIS, null, msgs);
      if (newInternalApis != null)
        msgs = ((InternalEObject)newInternalApis).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApiPackage.COMPONENT_API_TYPE__INTERNAL_APIS, null, msgs);
      msgs = basicSetInternalApis(newInternalApis, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.COMPONENT_API_TYPE__INTERNAL_APIS, newInternalApis, newInternalApis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ApiTypes getExternalApis()
  {
    return externalApis;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExternalApis(ApiTypes newExternalApis, NotificationChain msgs)
  {
    ApiTypes oldExternalApis = externalApis;
    externalApis = newExternalApis;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApiPackage.COMPONENT_API_TYPE__EXTERNAL_APIS, oldExternalApis, newExternalApis);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExternalApis(ApiTypes newExternalApis)
  {
    if (newExternalApis != externalApis)
    {
      NotificationChain msgs = null;
      if (externalApis != null)
        msgs = ((InternalEObject)externalApis).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApiPackage.COMPONENT_API_TYPE__EXTERNAL_APIS, null, msgs);
      if (newExternalApis != null)
        msgs = ((InternalEObject)newExternalApis).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApiPackage.COMPONENT_API_TYPE__EXTERNAL_APIS, null, msgs);
      msgs = basicSetExternalApis(newExternalApis, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.COMPONENT_API_TYPE__EXTERNAL_APIS, newExternalApis, newExternalApis));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.COMPONENT_API_TYPE__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVersion()
  {
    return version;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVersion(String newVersion)
  {
    String oldVersion = version;
    version = newVersion;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.COMPONENT_API_TYPE__VERSION, oldVersion, version));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, Class baseClass, NotificationChain msgs)
  {
    if (featureID >= 0)
    {
      switch (eDerivedStructuralFeatureID(featureID, baseClass))
      {
        case ApiPackage.COMPONENT_API_TYPE__INTERNAL_APIS:
          return basicSetInternalApis(null, msgs);
        case ApiPackage.COMPONENT_API_TYPE__EXTERNAL_APIS:
          return basicSetExternalApis(null, msgs);
        default:
          return eDynamicInverseRemove(otherEnd, featureID, baseClass, msgs);
      }
    }
    return eBasicSetContainer(null, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object eGet(EStructuralFeature eFeature, boolean resolve)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.COMPONENT_API_TYPE__INTERNAL_APIS:
        return getInternalApis();
      case ApiPackage.COMPONENT_API_TYPE__EXTERNAL_APIS:
        return getExternalApis();
      case ApiPackage.COMPONENT_API_TYPE__NAME:
        return getName();
      case ApiPackage.COMPONENT_API_TYPE__VERSION:
        return getVersion();
    }
    return eDynamicGet(eFeature, resolve);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eSet(EStructuralFeature eFeature, Object newValue)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.COMPONENT_API_TYPE__INTERNAL_APIS:
        setInternalApis((ApiTypes)newValue);
        return;
      case ApiPackage.COMPONENT_API_TYPE__EXTERNAL_APIS:
        setExternalApis((ApiTypes)newValue);
        return;
      case ApiPackage.COMPONENT_API_TYPE__NAME:
        setName((String)newValue);
        return;
      case ApiPackage.COMPONENT_API_TYPE__VERSION:
        setVersion((String)newValue);
        return;
    }
    eDynamicSet(eFeature, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eUnset(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.COMPONENT_API_TYPE__INTERNAL_APIS:
        setInternalApis((ApiTypes)null);
        return;
      case ApiPackage.COMPONENT_API_TYPE__EXTERNAL_APIS:
        setExternalApis((ApiTypes)null);
        return;
      case ApiPackage.COMPONENT_API_TYPE__NAME:
        setName(NAME_EDEFAULT);
        return;
      case ApiPackage.COMPONENT_API_TYPE__VERSION:
        setVersion(VERSION_EDEFAULT);
        return;
    }
    eDynamicUnset(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean eIsSet(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.COMPONENT_API_TYPE__INTERNAL_APIS:
        return internalApis != null;
      case ApiPackage.COMPONENT_API_TYPE__EXTERNAL_APIS:
        return externalApis != null;
      case ApiPackage.COMPONENT_API_TYPE__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case ApiPackage.COMPONENT_API_TYPE__VERSION:
        return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
    }
    return eDynamicIsSet(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", version: ");
    result.append(version);
    result.append(')');
    return result.toString();
  }

} //ComponentApiTypeImpl
