/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: FieldApi.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.api;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Api</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.component.api.FieldApi#isFinal <em>Final</em>}</li>
 *   <li>{@link org.eclipse.component.api.FieldApi#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.component.api.FieldApi#isStatic <em>Static</em>}</li>
 *   <li>{@link org.eclipse.component.api.FieldApi#isTransient <em>Transient</em>}</li>
 *   <li>{@link org.eclipse.component.api.FieldApi#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.component.api.FieldApi#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link org.eclipse.component.api.FieldApi#isVolatile <em>Volatile</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.component.api.ApiPackage#getFieldApi()
 * @model 
 * @generated
 */
public interface FieldApi extends EObject
{
  /**
   * Returns the value of the '<em><b>Final</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Final</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Final</em>' attribute.
   * @see #isSetFinal()
   * @see #unsetFinal()
   * @see #setFinal(boolean)
   * @see org.eclipse.component.api.ApiPackage#getFieldApi_Final()
   * @model unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isFinal();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.FieldApi#isFinal <em>Final</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Final</em>' attribute.
   * @see #isSetFinal()
   * @see #unsetFinal()
   * @see #isFinal()
   * @generated
   */
  void setFinal(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.FieldApi#isFinal <em>Final</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetFinal()
   * @see #isFinal()
   * @see #setFinal(boolean)
   * @generated
   */
  void unsetFinal();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.FieldApi#isFinal <em>Final</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Final</em>' attribute is set.
   * @see #unsetFinal()
   * @see #isFinal()
   * @see #setFinal(boolean)
   * @generated
   */
  boolean isSetFinal();

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.eclipse.component.api.ApiPackage#getFieldApi_Name()
   * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.FieldApi#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Static</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Static</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Static</em>' attribute.
   * @see #isSetStatic()
   * @see #unsetStatic()
   * @see #setStatic(boolean)
   * @see org.eclipse.component.api.ApiPackage#getFieldApi_Static()
   * @model unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isStatic();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.FieldApi#isStatic <em>Static</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Static</em>' attribute.
   * @see #isSetStatic()
   * @see #unsetStatic()
   * @see #isStatic()
   * @generated
   */
  void setStatic(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.FieldApi#isStatic <em>Static</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetStatic()
   * @see #isStatic()
   * @see #setStatic(boolean)
   * @generated
   */
  void unsetStatic();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.FieldApi#isStatic <em>Static</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Static</em>' attribute is set.
   * @see #unsetStatic()
   * @see #isStatic()
   * @see #setStatic(boolean)
   * @generated
   */
  boolean isSetStatic();

  /**
   * Returns the value of the '<em><b>Transient</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Transient</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Transient</em>' attribute.
   * @see #isSetTransient()
   * @see #unsetTransient()
   * @see #setTransient(boolean)
   * @see org.eclipse.component.api.ApiPackage#getFieldApi_Transient()
   * @model unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isTransient();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.FieldApi#isTransient <em>Transient</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Transient</em>' attribute.
   * @see #isSetTransient()
   * @see #unsetTransient()
   * @see #isTransient()
   * @generated
   */
  void setTransient(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.FieldApi#isTransient <em>Transient</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetTransient()
   * @see #isTransient()
   * @see #setTransient(boolean)
   * @generated
   */
  void unsetTransient();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.FieldApi#isTransient <em>Transient</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Transient</em>' attribute is set.
   * @see #unsetTransient()
   * @see #isTransient()
   * @see #setTransient(boolean)
   * @generated
   */
  boolean isSetTransient();

  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see org.eclipse.component.api.ApiPackage#getFieldApi_Type()
   * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.FieldApi#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

  /**
   * Returns the value of the '<em><b>Visibility</b></em>' attribute.
   * The default value is <code>"public"</code>.
   * The literals are from the enumeration {@link org.eclipse.component.api.Visibility}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Visibility</em>' attribute.
   * @see org.eclipse.component.api.Visibility
   * @see #isSetVisibility()
   * @see #unsetVisibility()
   * @see #setVisibility(Visibility)
   * @see org.eclipse.component.api.ApiPackage#getFieldApi_Visibility()
   * @model default="public" unique="false" unsettable="true" required="true"
   * @generated
   */
  Visibility getVisibility();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.FieldApi#getVisibility <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Visibility</em>' attribute.
   * @see org.eclipse.component.api.Visibility
   * @see #isSetVisibility()
   * @see #unsetVisibility()
   * @see #getVisibility()
   * @generated
   */
  void setVisibility(Visibility value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.FieldApi#getVisibility <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetVisibility()
   * @see #getVisibility()
   * @see #setVisibility(Visibility)
   * @generated
   */
  void unsetVisibility();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.FieldApi#getVisibility <em>Visibility</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Visibility</em>' attribute is set.
   * @see #unsetVisibility()
   * @see #getVisibility()
   * @see #setVisibility(Visibility)
   * @generated
   */
  boolean isSetVisibility();

  /**
   * Returns the value of the '<em><b>Volatile</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Volatile</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Volatile</em>' attribute.
   * @see #isSetVolatile()
   * @see #unsetVolatile()
   * @see #setVolatile(boolean)
   * @see org.eclipse.component.api.ApiPackage#getFieldApi_Volatile()
   * @model unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isVolatile();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.FieldApi#isVolatile <em>Volatile</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Volatile</em>' attribute.
   * @see #isSetVolatile()
   * @see #unsetVolatile()
   * @see #isVolatile()
   * @generated
   */
  void setVolatile(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.FieldApi#isVolatile <em>Volatile</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetVolatile()
   * @see #isVolatile()
   * @see #setVolatile(boolean)
   * @generated
   */
  void unsetVolatile();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.FieldApi#isVolatile <em>Volatile</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Volatile</em>' attribute is set.
   * @see #unsetVolatile()
   * @see #isVolatile()
   * @see #setVolatile(boolean)
   * @generated
   */
  boolean isSetVolatile();

} // FieldApi
