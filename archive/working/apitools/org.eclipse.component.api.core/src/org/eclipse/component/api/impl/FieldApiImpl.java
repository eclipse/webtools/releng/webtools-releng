/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: FieldApiImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.api.impl;

import org.eclipse.component.api.ApiPackage;
import org.eclipse.component.api.FieldApi;
import org.eclipse.component.api.Visibility;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Field Api</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.component.api.impl.FieldApiImpl#isFinal <em>Final</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.FieldApiImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.FieldApiImpl#isStatic <em>Static</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.FieldApiImpl#isTransient <em>Transient</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.FieldApiImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.FieldApiImpl#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.FieldApiImpl#isVolatile <em>Volatile</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FieldApiImpl extends EObjectImpl implements FieldApi
{
  /**
   * The default value of the '{@link #isFinal() <em>Final</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isFinal()
   * @generated
   * @ordered
   */
  protected static final boolean FINAL_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isFinal() <em>Final</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isFinal()
   * @generated
   * @ordered
   */
  protected boolean final_ = FINAL_EDEFAULT;

  /**
   * This is true if the Final attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean finalESet = false;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #isStatic() <em>Static</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStatic()
   * @generated
   * @ordered
   */
  protected static final boolean STATIC_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isStatic() <em>Static</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStatic()
   * @generated
   * @ordered
   */
  protected boolean static_ = STATIC_EDEFAULT;

  /**
   * This is true if the Static attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean staticESet = false;

  /**
   * The default value of the '{@link #isTransient() <em>Transient</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTransient()
   * @generated
   * @ordered
   */
  protected static final boolean TRANSIENT_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isTransient() <em>Transient</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTransient()
   * @generated
   * @ordered
   */
  protected boolean transient_ = TRANSIENT_EDEFAULT;

  /**
   * This is true if the Transient attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean transientESet = false;

  /**
   * The default value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected static final String TYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected String type = TYPE_EDEFAULT;

  /**
   * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVisibility()
   * @generated
   * @ordered
   */
  protected static final Visibility VISIBILITY_EDEFAULT = Visibility.PUBLIC_LITERAL;

  /**
   * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVisibility()
   * @generated
   * @ordered
   */
  protected Visibility visibility = VISIBILITY_EDEFAULT;

  /**
   * This is true if the Visibility attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean visibilityESet = false;

  /**
   * The default value of the '{@link #isVolatile() <em>Volatile</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isVolatile()
   * @generated
   * @ordered
   */
  protected static final boolean VOLATILE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isVolatile() <em>Volatile</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isVolatile()
   * @generated
   * @ordered
   */
  protected boolean volatile_ = VOLATILE_EDEFAULT;

  /**
   * This is true if the Volatile attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean volatileESet = false;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FieldApiImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EClass eStaticClass()
  {
    return ApiPackage.eINSTANCE.getFieldApi();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isFinal()
  {
    return final_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFinal(boolean newFinal)
  {
    boolean oldFinal = final_;
    final_ = newFinal;
    boolean oldFinalESet = finalESet;
    finalESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.FIELD_API__FINAL, oldFinal, final_, !oldFinalESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetFinal()
  {
    boolean oldFinal = final_;
    boolean oldFinalESet = finalESet;
    final_ = FINAL_EDEFAULT;
    finalESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.FIELD_API__FINAL, oldFinal, FINAL_EDEFAULT, oldFinalESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetFinal()
  {
    return finalESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.FIELD_API__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isStatic()
  {
    return static_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatic(boolean newStatic)
  {
    boolean oldStatic = static_;
    static_ = newStatic;
    boolean oldStaticESet = staticESet;
    staticESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.FIELD_API__STATIC, oldStatic, static_, !oldStaticESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetStatic()
  {
    boolean oldStatic = static_;
    boolean oldStaticESet = staticESet;
    static_ = STATIC_EDEFAULT;
    staticESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.FIELD_API__STATIC, oldStatic, STATIC_EDEFAULT, oldStaticESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetStatic()
  {
    return staticESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isTransient()
  {
    return transient_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTransient(boolean newTransient)
  {
    boolean oldTransient = transient_;
    transient_ = newTransient;
    boolean oldTransientESet = transientESet;
    transientESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.FIELD_API__TRANSIENT, oldTransient, transient_, !oldTransientESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetTransient()
  {
    boolean oldTransient = transient_;
    boolean oldTransientESet = transientESet;
    transient_ = TRANSIENT_EDEFAULT;
    transientESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.FIELD_API__TRANSIENT, oldTransient, TRANSIENT_EDEFAULT, oldTransientESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetTransient()
  {
    return transientESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(String newType)
  {
    String oldType = type;
    type = newType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.FIELD_API__TYPE, oldType, type));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Visibility getVisibility()
  {
    return visibility;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVisibility(Visibility newVisibility)
  {
    Visibility oldVisibility = visibility;
    visibility = newVisibility == null ? VISIBILITY_EDEFAULT : newVisibility;
    boolean oldVisibilityESet = visibilityESet;
    visibilityESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.FIELD_API__VISIBILITY, oldVisibility, visibility, !oldVisibilityESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetVisibility()
  {
    Visibility oldVisibility = visibility;
    boolean oldVisibilityESet = visibilityESet;
    visibility = VISIBILITY_EDEFAULT;
    visibilityESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.FIELD_API__VISIBILITY, oldVisibility, VISIBILITY_EDEFAULT, oldVisibilityESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetVisibility()
  {
    return visibilityESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isVolatile()
  {
    return volatile_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVolatile(boolean newVolatile)
  {
    boolean oldVolatile = volatile_;
    volatile_ = newVolatile;
    boolean oldVolatileESet = volatileESet;
    volatileESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.FIELD_API__VOLATILE, oldVolatile, volatile_, !oldVolatileESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetVolatile()
  {
    boolean oldVolatile = volatile_;
    boolean oldVolatileESet = volatileESet;
    volatile_ = VOLATILE_EDEFAULT;
    volatileESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.FIELD_API__VOLATILE, oldVolatile, VOLATILE_EDEFAULT, oldVolatileESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetVolatile()
  {
    return volatileESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object eGet(EStructuralFeature eFeature, boolean resolve)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.FIELD_API__FINAL:
        return isFinal() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.FIELD_API__NAME:
        return getName();
      case ApiPackage.FIELD_API__STATIC:
        return isStatic() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.FIELD_API__TRANSIENT:
        return isTransient() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.FIELD_API__TYPE:
        return getType();
      case ApiPackage.FIELD_API__VISIBILITY:
        return getVisibility();
      case ApiPackage.FIELD_API__VOLATILE:
        return isVolatile() ? Boolean.TRUE : Boolean.FALSE;
    }
    return eDynamicGet(eFeature, resolve);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eSet(EStructuralFeature eFeature, Object newValue)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.FIELD_API__FINAL:
        setFinal(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.FIELD_API__NAME:
        setName((String)newValue);
        return;
      case ApiPackage.FIELD_API__STATIC:
        setStatic(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.FIELD_API__TRANSIENT:
        setTransient(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.FIELD_API__TYPE:
        setType((String)newValue);
        return;
      case ApiPackage.FIELD_API__VISIBILITY:
        setVisibility((Visibility)newValue);
        return;
      case ApiPackage.FIELD_API__VOLATILE:
        setVolatile(((Boolean)newValue).booleanValue());
        return;
    }
    eDynamicSet(eFeature, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eUnset(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.FIELD_API__FINAL:
        unsetFinal();
        return;
      case ApiPackage.FIELD_API__NAME:
        setName(NAME_EDEFAULT);
        return;
      case ApiPackage.FIELD_API__STATIC:
        unsetStatic();
        return;
      case ApiPackage.FIELD_API__TRANSIENT:
        unsetTransient();
        return;
      case ApiPackage.FIELD_API__TYPE:
        setType(TYPE_EDEFAULT);
        return;
      case ApiPackage.FIELD_API__VISIBILITY:
        unsetVisibility();
        return;
      case ApiPackage.FIELD_API__VOLATILE:
        unsetVolatile();
        return;
    }
    eDynamicUnset(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean eIsSet(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.FIELD_API__FINAL:
        return isSetFinal();
      case ApiPackage.FIELD_API__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case ApiPackage.FIELD_API__STATIC:
        return isSetStatic();
      case ApiPackage.FIELD_API__TRANSIENT:
        return isSetTransient();
      case ApiPackage.FIELD_API__TYPE:
        return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
      case ApiPackage.FIELD_API__VISIBILITY:
        return isSetVisibility();
      case ApiPackage.FIELD_API__VOLATILE:
        return isSetVolatile();
    }
    return eDynamicIsSet(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (final: ");
    if (finalESet) result.append(final_); else result.append("<unset>");
    result.append(", name: ");
    result.append(name);
    result.append(", static: ");
    if (staticESet) result.append(static_); else result.append("<unset>");
    result.append(", transient: ");
    if (transientESet) result.append(transient_); else result.append("<unset>");
    result.append(", type: ");
    result.append(type);
    result.append(", visibility: ");
    if (visibilityESet) result.append(visibility); else result.append("<unset>");
    result.append(", volatile: ");
    if (volatileESet) result.append(volatile_); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //FieldApiImpl
