/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassApiImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.api.impl;

import java.util.Collection;

import org.eclipse.component.api.ApiPackage;
import org.eclipse.component.api.ClassApi;
import org.eclipse.component.api.FieldApi;
import org.eclipse.component.api.MethodApi;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Api</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.component.api.impl.ClassApiImpl#getMethodApi <em>Method Api</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.ClassApiImpl#getFieldApi <em>Field Api</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.ClassApiImpl#isImplement <em>Implement</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.ClassApiImpl#isInstantiate <em>Instantiate</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.ClassApiImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.ClassApiImpl#isReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.ClassApiImpl#isSubclass <em>Subclass</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClassApiImpl extends EObjectImpl implements ClassApi
{
  /**
   * The cached value of the '{@link #getMethodApi() <em>Method Api</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMethodApi()
   * @generated
   * @ordered
   */
  protected EList methodApi = null;

  /**
   * The cached value of the '{@link #getFieldApi() <em>Field Api</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFieldApi()
   * @generated
   * @ordered
   */
  protected EList fieldApi = null;

  /**
   * The default value of the '{@link #isImplement() <em>Implement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isImplement()
   * @generated
   * @ordered
   */
  protected static final boolean IMPLEMENT_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isImplement() <em>Implement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isImplement()
   * @generated
   * @ordered
   */
  protected boolean implement = IMPLEMENT_EDEFAULT;

  /**
   * This is true if the Implement attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean implementESet = false;

  /**
   * The default value of the '{@link #isInstantiate() <em>Instantiate</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isInstantiate()
   * @generated
   * @ordered
   */
  protected static final boolean INSTANTIATE_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isInstantiate() <em>Instantiate</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isInstantiate()
   * @generated
   * @ordered
   */
  protected boolean instantiate = INSTANTIATE_EDEFAULT;

  /**
   * This is true if the Instantiate attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean instantiateESet = false;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #isReference() <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReference()
   * @generated
   * @ordered
   */
  protected static final boolean REFERENCE_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isReference() <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReference()
   * @generated
   * @ordered
   */
  protected boolean reference = REFERENCE_EDEFAULT;

  /**
   * This is true if the Reference attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean referenceESet = false;

  /**
   * The default value of the '{@link #isSubclass() <em>Subclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSubclass()
   * @generated
   * @ordered
   */
  protected static final boolean SUBCLASS_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isSubclass() <em>Subclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSubclass()
   * @generated
   * @ordered
   */
  protected boolean subclass = SUBCLASS_EDEFAULT;

  /**
   * This is true if the Subclass attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean subclassESet = false;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ClassApiImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EClass eStaticClass()
  {
    return ApiPackage.eINSTANCE.getClassApi();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList getMethodApi()
  {
    if (methodApi == null)
    {
      methodApi = new EObjectContainmentEList(MethodApi.class, this, ApiPackage.CLASS_API__METHOD_API);
    }
    return methodApi;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList getFieldApi()
  {
    if (fieldApi == null)
    {
      fieldApi = new EObjectContainmentEList(FieldApi.class, this, ApiPackage.CLASS_API__FIELD_API);
    }
    return fieldApi;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isImplement()
  {
    return implement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setImplement(boolean newImplement)
  {
    boolean oldImplement = implement;
    implement = newImplement;
    boolean oldImplementESet = implementESet;
    implementESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.CLASS_API__IMPLEMENT, oldImplement, implement, !oldImplementESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetImplement()
  {
    boolean oldImplement = implement;
    boolean oldImplementESet = implementESet;
    implement = IMPLEMENT_EDEFAULT;
    implementESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.CLASS_API__IMPLEMENT, oldImplement, IMPLEMENT_EDEFAULT, oldImplementESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetImplement()
  {
    return implementESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isInstantiate()
  {
    return instantiate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInstantiate(boolean newInstantiate)
  {
    boolean oldInstantiate = instantiate;
    instantiate = newInstantiate;
    boolean oldInstantiateESet = instantiateESet;
    instantiateESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.CLASS_API__INSTANTIATE, oldInstantiate, instantiate, !oldInstantiateESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetInstantiate()
  {
    boolean oldInstantiate = instantiate;
    boolean oldInstantiateESet = instantiateESet;
    instantiate = INSTANTIATE_EDEFAULT;
    instantiateESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.CLASS_API__INSTANTIATE, oldInstantiate, INSTANTIATE_EDEFAULT, oldInstantiateESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetInstantiate()
  {
    return instantiateESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.CLASS_API__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isReference()
  {
    return reference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReference(boolean newReference)
  {
    boolean oldReference = reference;
    reference = newReference;
    boolean oldReferenceESet = referenceESet;
    referenceESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.CLASS_API__REFERENCE, oldReference, reference, !oldReferenceESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetReference()
  {
    boolean oldReference = reference;
    boolean oldReferenceESet = referenceESet;
    reference = REFERENCE_EDEFAULT;
    referenceESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.CLASS_API__REFERENCE, oldReference, REFERENCE_EDEFAULT, oldReferenceESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetReference()
  {
    return referenceESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSubclass()
  {
    return subclass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubclass(boolean newSubclass)
  {
    boolean oldSubclass = subclass;
    subclass = newSubclass;
    boolean oldSubclassESet = subclassESet;
    subclassESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.CLASS_API__SUBCLASS, oldSubclass, subclass, !oldSubclassESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetSubclass()
  {
    boolean oldSubclass = subclass;
    boolean oldSubclassESet = subclassESet;
    subclass = SUBCLASS_EDEFAULT;
    subclassESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.CLASS_API__SUBCLASS, oldSubclass, SUBCLASS_EDEFAULT, oldSubclassESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetSubclass()
  {
    return subclassESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, Class baseClass, NotificationChain msgs)
  {
    if (featureID >= 0)
    {
      switch (eDerivedStructuralFeatureID(featureID, baseClass))
      {
        case ApiPackage.CLASS_API__METHOD_API:
          return ((InternalEList)getMethodApi()).basicRemove(otherEnd, msgs);
        case ApiPackage.CLASS_API__FIELD_API:
          return ((InternalEList)getFieldApi()).basicRemove(otherEnd, msgs);
        default:
          return eDynamicInverseRemove(otherEnd, featureID, baseClass, msgs);
      }
    }
    return eBasicSetContainer(null, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object eGet(EStructuralFeature eFeature, boolean resolve)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.CLASS_API__METHOD_API:
        return getMethodApi();
      case ApiPackage.CLASS_API__FIELD_API:
        return getFieldApi();
      case ApiPackage.CLASS_API__IMPLEMENT:
        return isImplement() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.CLASS_API__INSTANTIATE:
        return isInstantiate() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.CLASS_API__NAME:
        return getName();
      case ApiPackage.CLASS_API__REFERENCE:
        return isReference() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.CLASS_API__SUBCLASS:
        return isSubclass() ? Boolean.TRUE : Boolean.FALSE;
    }
    return eDynamicGet(eFeature, resolve);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eSet(EStructuralFeature eFeature, Object newValue)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.CLASS_API__METHOD_API:
        getMethodApi().clear();
        getMethodApi().addAll((Collection)newValue);
        return;
      case ApiPackage.CLASS_API__FIELD_API:
        getFieldApi().clear();
        getFieldApi().addAll((Collection)newValue);
        return;
      case ApiPackage.CLASS_API__IMPLEMENT:
        setImplement(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.CLASS_API__INSTANTIATE:
        setInstantiate(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.CLASS_API__NAME:
        setName((String)newValue);
        return;
      case ApiPackage.CLASS_API__REFERENCE:
        setReference(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.CLASS_API__SUBCLASS:
        setSubclass(((Boolean)newValue).booleanValue());
        return;
    }
    eDynamicSet(eFeature, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eUnset(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.CLASS_API__METHOD_API:
        getMethodApi().clear();
        return;
      case ApiPackage.CLASS_API__FIELD_API:
        getFieldApi().clear();
        return;
      case ApiPackage.CLASS_API__IMPLEMENT:
        unsetImplement();
        return;
      case ApiPackage.CLASS_API__INSTANTIATE:
        unsetInstantiate();
        return;
      case ApiPackage.CLASS_API__NAME:
        setName(NAME_EDEFAULT);
        return;
      case ApiPackage.CLASS_API__REFERENCE:
        unsetReference();
        return;
      case ApiPackage.CLASS_API__SUBCLASS:
        unsetSubclass();
        return;
    }
    eDynamicUnset(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean eIsSet(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.CLASS_API__METHOD_API:
        return methodApi != null && !methodApi.isEmpty();
      case ApiPackage.CLASS_API__FIELD_API:
        return fieldApi != null && !fieldApi.isEmpty();
      case ApiPackage.CLASS_API__IMPLEMENT:
        return isSetImplement();
      case ApiPackage.CLASS_API__INSTANTIATE:
        return isSetInstantiate();
      case ApiPackage.CLASS_API__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case ApiPackage.CLASS_API__REFERENCE:
        return isSetReference();
      case ApiPackage.CLASS_API__SUBCLASS:
        return isSetSubclass();
    }
    return eDynamicIsSet(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (implement: ");
    if (implementESet) result.append(implement); else result.append("<unset>");
    result.append(", instantiate: ");
    if (instantiateESet) result.append(instantiate); else result.append("<unset>");
    result.append(", name: ");
    result.append(name);
    result.append(", reference: ");
    if (referenceESet) result.append(reference); else result.append("<unset>");
    result.append(", subclass: ");
    if (subclassESet) result.append(subclass); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //ClassApiImpl
