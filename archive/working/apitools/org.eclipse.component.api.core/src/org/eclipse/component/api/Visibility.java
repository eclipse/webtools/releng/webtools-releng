/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: Visibility.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.api;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Visibility</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.component.api.ApiPackage#getVisibility()
 * @model
 * @generated
 */
public final class Visibility extends AbstractEnumerator
{
  /**
   * The '<em><b>Public</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Public</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #PUBLIC_LITERAL
   * @model name="public"
   * @generated
   * @ordered
   */
  public static final int PUBLIC = 0;

  /**
   * The '<em><b>Protected</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Protected</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #PROTECTED_LITERAL
   * @model name="protected"
   * @generated
   * @ordered
   */
  public static final int PROTECTED = 1;

  /**
   * The '<em><b>Private</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Private</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #PRIVATE_LITERAL
   * @model name="private"
   * @generated
   * @ordered
   */
  public static final int PRIVATE = 2;

  /**
   * The '<em><b>Public</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #PUBLIC
   * @generated
   * @ordered
   */
  public static final Visibility PUBLIC_LITERAL = new Visibility(PUBLIC, "public");

  /**
   * The '<em><b>Protected</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #PROTECTED
   * @generated
   * @ordered
   */
  public static final Visibility PROTECTED_LITERAL = new Visibility(PROTECTED, "protected");

  /**
   * The '<em><b>Private</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #PRIVATE
   * @generated
   * @ordered
   */
  public static final Visibility PRIVATE_LITERAL = new Visibility(PRIVATE, "private");

  /**
   * An array of all the '<em><b>Visibility</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final Visibility[] VALUES_ARRAY =
    new Visibility[]
    {
      PUBLIC_LITERAL,
      PROTECTED_LITERAL,
      PRIVATE_LITERAL,
    };

  /**
   * A public read-only list of all the '<em><b>Visibility</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>Visibility</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static Visibility get(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      Visibility result = VALUES_ARRAY[i];
      if (result.toString().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>Visibility</b></em>' literal with the specified value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static Visibility get(int value)
  {
    switch (value)
    {
      case PUBLIC: return PUBLIC_LITERAL;
      case PROTECTED: return PROTECTED_LITERAL;
      case PRIVATE: return PRIVATE_LITERAL;
    }
    return null;	
  }

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private Visibility(int value, String name)
  {
    super(value, name);
  }

} //Visibility
