/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: MethodApiImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.api.impl;

import java.util.List;

import org.eclipse.component.api.ApiPackage;
import org.eclipse.component.api.MethodApi;
import org.eclipse.component.api.Visibility;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Method Api</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.component.api.impl.MethodApiImpl#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.MethodApiImpl#getExceptionType <em>Exception Type</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.MethodApiImpl#isFinal <em>Final</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.MethodApiImpl#getInputType <em>Input Type</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.MethodApiImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.MethodApiImpl#isNative <em>Native</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.MethodApiImpl#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.MethodApiImpl#isStatic <em>Static</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.MethodApiImpl#isStrict <em>Strict</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.MethodApiImpl#isSynchronized <em>Synchronized</em>}</li>
 *   <li>{@link org.eclipse.component.api.impl.MethodApiImpl#getVisibility <em>Visibility</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MethodApiImpl extends EObjectImpl implements MethodApi
{
  /**
   * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAbstract()
   * @generated
   * @ordered
   */
  protected static final boolean ABSTRACT_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAbstract()
   * @generated
   * @ordered
   */
  protected boolean abstract_ = ABSTRACT_EDEFAULT;

  /**
   * This is true if the Abstract attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean abstractESet = false;

  /**
   * The default value of the '{@link #getExceptionType() <em>Exception Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExceptionType()
   * @generated
   * @ordered
   */
  protected static final List EXCEPTION_TYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getExceptionType() <em>Exception Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExceptionType()
   * @generated
   * @ordered
   */
  protected List exceptionType = EXCEPTION_TYPE_EDEFAULT;

  /**
   * The default value of the '{@link #isFinal() <em>Final</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isFinal()
   * @generated
   * @ordered
   */
  protected static final boolean FINAL_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isFinal() <em>Final</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isFinal()
   * @generated
   * @ordered
   */
  protected boolean final_ = FINAL_EDEFAULT;

  /**
   * This is true if the Final attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean finalESet = false;

  /**
   * The default value of the '{@link #getInputType() <em>Input Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInputType()
   * @generated
   * @ordered
   */
  protected static final List INPUT_TYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getInputType() <em>Input Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInputType()
   * @generated
   * @ordered
   */
  protected List inputType = INPUT_TYPE_EDEFAULT;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #isNative() <em>Native</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNative()
   * @generated
   * @ordered
   */
  protected static final boolean NATIVE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isNative() <em>Native</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNative()
   * @generated
   * @ordered
   */
  protected boolean native_ = NATIVE_EDEFAULT;

  /**
   * This is true if the Native attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean nativeESet = false;

  /**
   * The default value of the '{@link #getReturnType() <em>Return Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReturnType()
   * @generated
   * @ordered
   */
  protected static final String RETURN_TYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getReturnType() <em>Return Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReturnType()
   * @generated
   * @ordered
   */
  protected String returnType = RETURN_TYPE_EDEFAULT;

  /**
   * The default value of the '{@link #isStatic() <em>Static</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStatic()
   * @generated
   * @ordered
   */
  protected static final boolean STATIC_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isStatic() <em>Static</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStatic()
   * @generated
   * @ordered
   */
  protected boolean static_ = STATIC_EDEFAULT;

  /**
   * This is true if the Static attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean staticESet = false;

  /**
   * The default value of the '{@link #isStrict() <em>Strict</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStrict()
   * @generated
   * @ordered
   */
  protected static final boolean STRICT_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isStrict() <em>Strict</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isStrict()
   * @generated
   * @ordered
   */
  protected boolean strict = STRICT_EDEFAULT;

  /**
   * This is true if the Strict attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean strictESet = false;

  /**
   * The default value of the '{@link #isSynchronized() <em>Synchronized</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSynchronized()
   * @generated
   * @ordered
   */
  protected static final boolean SYNCHRONIZED_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isSynchronized() <em>Synchronized</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSynchronized()
   * @generated
   * @ordered
   */
  protected boolean synchronized_ = SYNCHRONIZED_EDEFAULT;

  /**
   * This is true if the Synchronized attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean synchronizedESet = false;

  /**
   * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVisibility()
   * @generated
   * @ordered
   */
  protected static final Visibility VISIBILITY_EDEFAULT = Visibility.PUBLIC_LITERAL;

  /**
   * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVisibility()
   * @generated
   * @ordered
   */
  protected Visibility visibility = VISIBILITY_EDEFAULT;

  /**
   * This is true if the Visibility attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean visibilityESet = false;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MethodApiImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EClass eStaticClass()
  {
    return ApiPackage.eINSTANCE.getMethodApi();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isAbstract()
  {
    return abstract_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAbstract(boolean newAbstract)
  {
    boolean oldAbstract = abstract_;
    abstract_ = newAbstract;
    boolean oldAbstractESet = abstractESet;
    abstractESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.METHOD_API__ABSTRACT, oldAbstract, abstract_, !oldAbstractESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetAbstract()
  {
    boolean oldAbstract = abstract_;
    boolean oldAbstractESet = abstractESet;
    abstract_ = ABSTRACT_EDEFAULT;
    abstractESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.METHOD_API__ABSTRACT, oldAbstract, ABSTRACT_EDEFAULT, oldAbstractESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetAbstract()
  {
    return abstractESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public List getExceptionType()
  {
    return exceptionType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExceptionType(List newExceptionType)
  {
    List oldExceptionType = exceptionType;
    exceptionType = newExceptionType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.METHOD_API__EXCEPTION_TYPE, oldExceptionType, exceptionType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isFinal()
  {
    return final_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFinal(boolean newFinal)
  {
    boolean oldFinal = final_;
    final_ = newFinal;
    boolean oldFinalESet = finalESet;
    finalESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.METHOD_API__FINAL, oldFinal, final_, !oldFinalESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetFinal()
  {
    boolean oldFinal = final_;
    boolean oldFinalESet = finalESet;
    final_ = FINAL_EDEFAULT;
    finalESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.METHOD_API__FINAL, oldFinal, FINAL_EDEFAULT, oldFinalESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetFinal()
  {
    return finalESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public List getInputType()
  {
    return inputType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInputType(List newInputType)
  {
    List oldInputType = inputType;
    inputType = newInputType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.METHOD_API__INPUT_TYPE, oldInputType, inputType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.METHOD_API__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isNative()
  {
    return native_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNative(boolean newNative)
  {
    boolean oldNative = native_;
    native_ = newNative;
    boolean oldNativeESet = nativeESet;
    nativeESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.METHOD_API__NATIVE, oldNative, native_, !oldNativeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetNative()
  {
    boolean oldNative = native_;
    boolean oldNativeESet = nativeESet;
    native_ = NATIVE_EDEFAULT;
    nativeESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.METHOD_API__NATIVE, oldNative, NATIVE_EDEFAULT, oldNativeESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetNative()
  {
    return nativeESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getReturnType()
  {
    return returnType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReturnType(String newReturnType)
  {
    String oldReturnType = returnType;
    returnType = newReturnType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.METHOD_API__RETURN_TYPE, oldReturnType, returnType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isStatic()
  {
    return static_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatic(boolean newStatic)
  {
    boolean oldStatic = static_;
    static_ = newStatic;
    boolean oldStaticESet = staticESet;
    staticESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.METHOD_API__STATIC, oldStatic, static_, !oldStaticESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetStatic()
  {
    boolean oldStatic = static_;
    boolean oldStaticESet = staticESet;
    static_ = STATIC_EDEFAULT;
    staticESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.METHOD_API__STATIC, oldStatic, STATIC_EDEFAULT, oldStaticESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetStatic()
  {
    return staticESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isStrict()
  {
    return strict;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStrict(boolean newStrict)
  {
    boolean oldStrict = strict;
    strict = newStrict;
    boolean oldStrictESet = strictESet;
    strictESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.METHOD_API__STRICT, oldStrict, strict, !oldStrictESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetStrict()
  {
    boolean oldStrict = strict;
    boolean oldStrictESet = strictESet;
    strict = STRICT_EDEFAULT;
    strictESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.METHOD_API__STRICT, oldStrict, STRICT_EDEFAULT, oldStrictESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetStrict()
  {
    return strictESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSynchronized()
  {
    return synchronized_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSynchronized(boolean newSynchronized)
  {
    boolean oldSynchronized = synchronized_;
    synchronized_ = newSynchronized;
    boolean oldSynchronizedESet = synchronizedESet;
    synchronizedESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.METHOD_API__SYNCHRONIZED, oldSynchronized, synchronized_, !oldSynchronizedESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetSynchronized()
  {
    boolean oldSynchronized = synchronized_;
    boolean oldSynchronizedESet = synchronizedESet;
    synchronized_ = SYNCHRONIZED_EDEFAULT;
    synchronizedESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.METHOD_API__SYNCHRONIZED, oldSynchronized, SYNCHRONIZED_EDEFAULT, oldSynchronizedESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetSynchronized()
  {
    return synchronizedESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Visibility getVisibility()
  {
    return visibility;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVisibility(Visibility newVisibility)
  {
    Visibility oldVisibility = visibility;
    visibility = newVisibility == null ? VISIBILITY_EDEFAULT : newVisibility;
    boolean oldVisibilityESet = visibilityESet;
    visibilityESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ApiPackage.METHOD_API__VISIBILITY, oldVisibility, visibility, !oldVisibilityESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetVisibility()
  {
    Visibility oldVisibility = visibility;
    boolean oldVisibilityESet = visibilityESet;
    visibility = VISIBILITY_EDEFAULT;
    visibilityESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ApiPackage.METHOD_API__VISIBILITY, oldVisibility, VISIBILITY_EDEFAULT, oldVisibilityESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetVisibility()
  {
    return visibilityESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object eGet(EStructuralFeature eFeature, boolean resolve)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.METHOD_API__ABSTRACT:
        return isAbstract() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.METHOD_API__EXCEPTION_TYPE:
        return getExceptionType();
      case ApiPackage.METHOD_API__FINAL:
        return isFinal() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.METHOD_API__INPUT_TYPE:
        return getInputType();
      case ApiPackage.METHOD_API__NAME:
        return getName();
      case ApiPackage.METHOD_API__NATIVE:
        return isNative() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.METHOD_API__RETURN_TYPE:
        return getReturnType();
      case ApiPackage.METHOD_API__STATIC:
        return isStatic() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.METHOD_API__STRICT:
        return isStrict() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.METHOD_API__SYNCHRONIZED:
        return isSynchronized() ? Boolean.TRUE : Boolean.FALSE;
      case ApiPackage.METHOD_API__VISIBILITY:
        return getVisibility();
    }
    return eDynamicGet(eFeature, resolve);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eSet(EStructuralFeature eFeature, Object newValue)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.METHOD_API__ABSTRACT:
        setAbstract(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.METHOD_API__EXCEPTION_TYPE:
        setExceptionType((List)newValue);
        return;
      case ApiPackage.METHOD_API__FINAL:
        setFinal(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.METHOD_API__INPUT_TYPE:
        setInputType((List)newValue);
        return;
      case ApiPackage.METHOD_API__NAME:
        setName((String)newValue);
        return;
      case ApiPackage.METHOD_API__NATIVE:
        setNative(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.METHOD_API__RETURN_TYPE:
        setReturnType((String)newValue);
        return;
      case ApiPackage.METHOD_API__STATIC:
        setStatic(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.METHOD_API__STRICT:
        setStrict(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.METHOD_API__SYNCHRONIZED:
        setSynchronized(((Boolean)newValue).booleanValue());
        return;
      case ApiPackage.METHOD_API__VISIBILITY:
        setVisibility((Visibility)newValue);
        return;
    }
    eDynamicSet(eFeature, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eUnset(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.METHOD_API__ABSTRACT:
        unsetAbstract();
        return;
      case ApiPackage.METHOD_API__EXCEPTION_TYPE:
        setExceptionType(EXCEPTION_TYPE_EDEFAULT);
        return;
      case ApiPackage.METHOD_API__FINAL:
        unsetFinal();
        return;
      case ApiPackage.METHOD_API__INPUT_TYPE:
        setInputType(INPUT_TYPE_EDEFAULT);
        return;
      case ApiPackage.METHOD_API__NAME:
        setName(NAME_EDEFAULT);
        return;
      case ApiPackage.METHOD_API__NATIVE:
        unsetNative();
        return;
      case ApiPackage.METHOD_API__RETURN_TYPE:
        setReturnType(RETURN_TYPE_EDEFAULT);
        return;
      case ApiPackage.METHOD_API__STATIC:
        unsetStatic();
        return;
      case ApiPackage.METHOD_API__STRICT:
        unsetStrict();
        return;
      case ApiPackage.METHOD_API__SYNCHRONIZED:
        unsetSynchronized();
        return;
      case ApiPackage.METHOD_API__VISIBILITY:
        unsetVisibility();
        return;
    }
    eDynamicUnset(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean eIsSet(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ApiPackage.METHOD_API__ABSTRACT:
        return isSetAbstract();
      case ApiPackage.METHOD_API__EXCEPTION_TYPE:
        return EXCEPTION_TYPE_EDEFAULT == null ? exceptionType != null : !EXCEPTION_TYPE_EDEFAULT.equals(exceptionType);
      case ApiPackage.METHOD_API__FINAL:
        return isSetFinal();
      case ApiPackage.METHOD_API__INPUT_TYPE:
        return INPUT_TYPE_EDEFAULT == null ? inputType != null : !INPUT_TYPE_EDEFAULT.equals(inputType);
      case ApiPackage.METHOD_API__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case ApiPackage.METHOD_API__NATIVE:
        return isSetNative();
      case ApiPackage.METHOD_API__RETURN_TYPE:
        return RETURN_TYPE_EDEFAULT == null ? returnType != null : !RETURN_TYPE_EDEFAULT.equals(returnType);
      case ApiPackage.METHOD_API__STATIC:
        return isSetStatic();
      case ApiPackage.METHOD_API__STRICT:
        return isSetStrict();
      case ApiPackage.METHOD_API__SYNCHRONIZED:
        return isSetSynchronized();
      case ApiPackage.METHOD_API__VISIBILITY:
        return isSetVisibility();
    }
    return eDynamicIsSet(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (abstract: ");
    if (abstractESet) result.append(abstract_); else result.append("<unset>");
    result.append(", exceptionType: ");
    result.append(exceptionType);
    result.append(", final: ");
    if (finalESet) result.append(final_); else result.append("<unset>");
    result.append(", inputType: ");
    result.append(inputType);
    result.append(", name: ");
    result.append(name);
    result.append(", native: ");
    if (nativeESet) result.append(native_); else result.append("<unset>");
    result.append(", returnType: ");
    result.append(returnType);
    result.append(", static: ");
    if (staticESet) result.append(static_); else result.append("<unset>");
    result.append(", strict: ");
    if (strictESet) result.append(strict); else result.append("<unset>");
    result.append(", synchronized: ");
    if (synchronizedESet) result.append(synchronized_); else result.append("<unset>");
    result.append(", visibility: ");
    if (visibilityESet) result.append(visibility); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //MethodApiImpl
