/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ApiFactoryImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.api.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.component.api.ApiFactory;
import org.eclipse.component.api.ApiPackage;
import org.eclipse.component.api.ApiTypes;
import org.eclipse.component.api.ClassApi;
import org.eclipse.component.api.ComponentApiType;
import org.eclipse.component.api.DocumentRoot;
import org.eclipse.component.api.FieldApi;
import org.eclipse.component.api.MethodApi;
import org.eclipse.component.api.Visibility;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApiFactoryImpl extends EFactoryImpl implements ApiFactory
{
  /**
   * Creates and instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ApiFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case ApiPackage.API_TYPES: return createApiTypes();
      case ApiPackage.CLASS_API: return createClassApi();
      case ApiPackage.COMPONENT_API_TYPE: return createComponentApiType();
      case ApiPackage.DOCUMENT_ROOT: return createDocumentRoot();
      case ApiPackage.FIELD_API: return createFieldApi();
      case ApiPackage.METHOD_API: return createMethodApi();
      case ApiPackage.PACKAGE: return createPackage();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case ApiPackage.VISIBILITY:
      {
        Visibility result = Visibility.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
      }
      case ApiPackage.LIST_OF_TYPES:
        return createListOfTypesFromString(eDataType, initialValue);
      case ApiPackage.VISIBILITY_OBJECT:
        return createVisibilityObjectFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case ApiPackage.VISIBILITY:
        return instanceValue == null ? null : instanceValue.toString();
      case ApiPackage.LIST_OF_TYPES:
        return convertListOfTypesToString(eDataType, instanceValue);
      case ApiPackage.VISIBILITY_OBJECT:
        return convertVisibilityObjectToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ApiTypes createApiTypes()
  {
    ApiTypesImpl apiTypes = new ApiTypesImpl();
    return apiTypes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClassApi createClassApi()
  {
    ClassApiImpl classApi = new ClassApiImpl();
    return classApi;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentApiType createComponentApiType()
  {
    ComponentApiTypeImpl componentApiType = new ComponentApiTypeImpl();
    return componentApiType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentRoot createDocumentRoot()
  {
    DocumentRootImpl documentRoot = new DocumentRootImpl();
    return documentRoot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldApi createFieldApi()
  {
    FieldApiImpl fieldApi = new FieldApiImpl();
    return fieldApi;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodApi createMethodApi()
  {
    MethodApiImpl methodApi = new MethodApiImpl();
    return methodApi;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public org.eclipse.component.api.Package createPackage()
  {
    PackageImpl package_ = new PackageImpl();
    return package_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public List createListOfTypesFromString(EDataType eDataType, String initialValue)
  {
    if (initialValue == null) return null;
    List result = new ArrayList();
    for (StringTokenizer stringTokenizer = new StringTokenizer(initialValue); stringTokenizer.hasMoreTokens(); )
    {
      String item = stringTokenizer.nextToken();
      result.add(XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.eINSTANCE.getString(), item));
    }
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertListOfTypesToString(EDataType eDataType, Object instanceValue)
  {
    if (instanceValue == null) return null;
    List list = (List)instanceValue;
    if (list.isEmpty()) return "";
    StringBuffer result = new StringBuffer();
    for (Iterator i = list.iterator(); i.hasNext(); )
    {
      result.append(XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.eINSTANCE.getString(), i.next()));
      result.append(' ');
    }
    return result.substring(0, result.length() - 1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Visibility createVisibilityObjectFromString(EDataType eDataType, String initialValue)
  {
    return (Visibility)ApiFactory.eINSTANCE.createFromString(ApiPackage.eINSTANCE.getVisibility(), initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertVisibilityObjectToString(EDataType eDataType, Object instanceValue)
  {
    return ApiFactory.eINSTANCE.convertToString(ApiPackage.eINSTANCE.getVisibility(), instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ApiPackage getApiPackage()
  {
    return (ApiPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  public static ApiPackage getPackage()
  {
    return ApiPackage.eINSTANCE;
  }

} //ApiFactoryImpl
