/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ApiPackageImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.api.impl;

import java.util.List;

import org.eclipse.component.api.ApiFactory;
import org.eclipse.component.api.ApiPackage;
import org.eclipse.component.api.ApiTypes;
import org.eclipse.component.api.ClassApi;
import org.eclipse.component.api.ComponentApiType;
import org.eclipse.component.api.DocumentRoot;
import org.eclipse.component.api.FieldApi;
import org.eclipse.component.api.MethodApi;
import org.eclipse.component.api.Visibility;

import org.eclipse.component.api.util.ApiValidator;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.eclipse.emf.ecore.xml.type.impl.XMLTypePackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApiPackageImpl extends EPackageImpl implements ApiPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass apiTypesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass classApiEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentApiTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass documentRootEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldApiEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass methodApiEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass packageEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum visibilityEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType listOfTypesEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType visibilityObjectEDataType = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.eclipse.component.api.ApiPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private ApiPackageImpl()
  {
    super(eNS_URI, ApiFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this
   * model, and for any others upon which it depends.  Simple
   * dependencies are satisfied by calling this method on all
   * dependent packages before doing anything else.  This method drives
   * initialization for interdependent packages directly, in parallel
   * with this package, itself.
   * <p>Of this package and its interdependencies, all packages which
   * have not yet been registered by their URI values are first created
   * and registered.  The packages are then initialized in two steps:
   * meta-model objects for all of the packages are created before any
   * are initialized, since one package's meta-model objects may refer to
   * those of another.
   * <p>Invocation of this method will not affect any packages that have
   * already been initialized.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static ApiPackage init()
  {
    if (isInited) return (ApiPackage)EPackage.Registry.INSTANCE.getEPackage(ApiPackage.eNS_URI);

    // Obtain or create and register package
    ApiPackageImpl theApiPackage = (ApiPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof ApiPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new ApiPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    XMLTypePackageImpl.init();

    // Create package meta-data objects
    theApiPackage.createPackageContents();

    // Initialize created meta-data
    theApiPackage.initializePackageContents();

    // Register package validator
    EValidator.Registry.INSTANCE.put
      (theApiPackage, 
       new EValidator.Descriptor()
       {
         public EValidator getEValidator()
         {
           return ApiValidator.INSTANCE;
         }
       });

    // Mark meta-data to indicate it can't be changed
    theApiPackage.freeze();

    return theApiPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getApiTypes()
  {
    return apiTypesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getApiTypes_Package()
  {
    return (EReference)apiTypesEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getClassApi()
  {
    return classApiEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getClassApi_MethodApi()
  {
    return (EReference)classApiEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getClassApi_FieldApi()
  {
    return (EReference)classApiEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getClassApi_Implement()
  {
    return (EAttribute)classApiEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getClassApi_Instantiate()
  {
    return (EAttribute)classApiEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getClassApi_Name()
  {
    return (EAttribute)classApiEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getClassApi_Reference()
  {
    return (EAttribute)classApiEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getClassApi_Subclass()
  {
    return (EAttribute)classApiEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentApiType()
  {
    return componentApiTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentApiType_InternalApis()
  {
    return (EReference)componentApiTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentApiType_ExternalApis()
  {
    return (EReference)componentApiTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getComponentApiType_Name()
  {
    return (EAttribute)componentApiTypeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getComponentApiType_Version()
  {
    return (EAttribute)componentApiTypeEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDocumentRoot()
  {
    return documentRootEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDocumentRoot_Mixed()
  {
    return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XMLNSPrefixMap()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XSISchemaLocation()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_ComponentApi()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldApi()
  {
    return fieldApiEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFieldApi_Final()
  {
    return (EAttribute)fieldApiEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFieldApi_Name()
  {
    return (EAttribute)fieldApiEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFieldApi_Static()
  {
    return (EAttribute)fieldApiEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFieldApi_Transient()
  {
    return (EAttribute)fieldApiEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFieldApi_Type()
  {
    return (EAttribute)fieldApiEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFieldApi_Visibility()
  {
    return (EAttribute)fieldApiEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFieldApi_Volatile()
  {
    return (EAttribute)fieldApiEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMethodApi()
  {
    return methodApiEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodApi_Abstract()
  {
    return (EAttribute)methodApiEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodApi_ExceptionType()
  {
    return (EAttribute)methodApiEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodApi_Final()
  {
    return (EAttribute)methodApiEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodApi_InputType()
  {
    return (EAttribute)methodApiEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodApi_Name()
  {
    return (EAttribute)methodApiEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodApi_Native()
  {
    return (EAttribute)methodApiEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodApi_ReturnType()
  {
    return (EAttribute)methodApiEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodApi_Static()
  {
    return (EAttribute)methodApiEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodApi_Strict()
  {
    return (EAttribute)methodApiEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodApi_Synchronized()
  {
    return (EAttribute)methodApiEClass.getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodApi_Visibility()
  {
    return (EAttribute)methodApiEClass.getEStructuralFeatures().get(10);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPackage()
  {
    return packageEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPackage_ClassApi()
  {
    return (EReference)packageEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPackage_Name()
  {
    return (EAttribute)packageEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getVisibility()
  {
    return visibilityEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getListOfTypes()
  {
    return listOfTypesEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getVisibilityObject()
  {
    return visibilityObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ApiFactory getApiFactory()
  {
    return (ApiFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    apiTypesEClass = createEClass(API_TYPES);
    createEReference(apiTypesEClass, API_TYPES__PACKAGE);

    classApiEClass = createEClass(CLASS_API);
    createEReference(classApiEClass, CLASS_API__METHOD_API);
    createEReference(classApiEClass, CLASS_API__FIELD_API);
    createEAttribute(classApiEClass, CLASS_API__IMPLEMENT);
    createEAttribute(classApiEClass, CLASS_API__INSTANTIATE);
    createEAttribute(classApiEClass, CLASS_API__NAME);
    createEAttribute(classApiEClass, CLASS_API__REFERENCE);
    createEAttribute(classApiEClass, CLASS_API__SUBCLASS);

    componentApiTypeEClass = createEClass(COMPONENT_API_TYPE);
    createEReference(componentApiTypeEClass, COMPONENT_API_TYPE__INTERNAL_APIS);
    createEReference(componentApiTypeEClass, COMPONENT_API_TYPE__EXTERNAL_APIS);
    createEAttribute(componentApiTypeEClass, COMPONENT_API_TYPE__NAME);
    createEAttribute(componentApiTypeEClass, COMPONENT_API_TYPE__VERSION);

    documentRootEClass = createEClass(DOCUMENT_ROOT);
    createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
    createEReference(documentRootEClass, DOCUMENT_ROOT__COMPONENT_API);

    fieldApiEClass = createEClass(FIELD_API);
    createEAttribute(fieldApiEClass, FIELD_API__FINAL);
    createEAttribute(fieldApiEClass, FIELD_API__NAME);
    createEAttribute(fieldApiEClass, FIELD_API__STATIC);
    createEAttribute(fieldApiEClass, FIELD_API__TRANSIENT);
    createEAttribute(fieldApiEClass, FIELD_API__TYPE);
    createEAttribute(fieldApiEClass, FIELD_API__VISIBILITY);
    createEAttribute(fieldApiEClass, FIELD_API__VOLATILE);

    methodApiEClass = createEClass(METHOD_API);
    createEAttribute(methodApiEClass, METHOD_API__ABSTRACT);
    createEAttribute(methodApiEClass, METHOD_API__EXCEPTION_TYPE);
    createEAttribute(methodApiEClass, METHOD_API__FINAL);
    createEAttribute(methodApiEClass, METHOD_API__INPUT_TYPE);
    createEAttribute(methodApiEClass, METHOD_API__NAME);
    createEAttribute(methodApiEClass, METHOD_API__NATIVE);
    createEAttribute(methodApiEClass, METHOD_API__RETURN_TYPE);
    createEAttribute(methodApiEClass, METHOD_API__STATIC);
    createEAttribute(methodApiEClass, METHOD_API__STRICT);
    createEAttribute(methodApiEClass, METHOD_API__SYNCHRONIZED);
    createEAttribute(methodApiEClass, METHOD_API__VISIBILITY);

    packageEClass = createEClass(PACKAGE);
    createEReference(packageEClass, PACKAGE__CLASS_API);
    createEAttribute(packageEClass, PACKAGE__NAME);

    // Create enums
    visibilityEEnum = createEEnum(VISIBILITY);

    // Create data types
    listOfTypesEDataType = createEDataType(LIST_OF_TYPES);
    visibilityObjectEDataType = createEDataType(VISIBILITY_OBJECT);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    XMLTypePackageImpl theXMLTypePackage = (XMLTypePackageImpl)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

    // Add supertypes to classes

    // Initialize classes and features; add operations and parameters
    initEClass(apiTypesEClass, ApiTypes.class, "ApiTypes", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getApiTypes_Package(), this.getPackage(), null, "package", null, 1, -1, ApiTypes.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(classApiEClass, ClassApi.class, "ClassApi", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getClassApi_MethodApi(), this.getMethodApi(), null, "methodApi", null, 1, -1, ClassApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getClassApi_FieldApi(), this.getFieldApi(), null, "fieldApi", null, 1, -1, ClassApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getClassApi_Implement(), theXMLTypePackage.getBoolean(), "implement", "true", 0, 1, ClassApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getClassApi_Instantiate(), theXMLTypePackage.getBoolean(), "instantiate", "true", 0, 1, ClassApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getClassApi_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, ClassApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getClassApi_Reference(), theXMLTypePackage.getBoolean(), "reference", "true", 0, 1, ClassApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getClassApi_Subclass(), theXMLTypePackage.getBoolean(), "subclass", "true", 0, 1, ClassApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(componentApiTypeEClass, ComponentApiType.class, "ComponentApiType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getComponentApiType_InternalApis(), this.getApiTypes(), null, "internalApis", null, 1, 1, ComponentApiType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getComponentApiType_ExternalApis(), this.getApiTypes(), null, "externalApis", null, 1, 1, ComponentApiType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getComponentApiType_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, ComponentApiType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getComponentApiType_Version(), theXMLTypePackage.getString(), "version", null, 1, 1, ComponentApiType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_ComponentApi(), this.getComponentApiType(), null, "componentApi", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

    initEClass(fieldApiEClass, FieldApi.class, "FieldApi", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFieldApi_Final(), theXMLTypePackage.getBoolean(), "final", null, 0, 1, FieldApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getFieldApi_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, FieldApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getFieldApi_Static(), theXMLTypePackage.getBoolean(), "static", null, 0, 1, FieldApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getFieldApi_Transient(), theXMLTypePackage.getBoolean(), "transient", null, 0, 1, FieldApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getFieldApi_Type(), theXMLTypePackage.getString(), "type", null, 1, 1, FieldApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getFieldApi_Visibility(), this.getVisibility(), "visibility", "public", 1, 1, FieldApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getFieldApi_Volatile(), theXMLTypePackage.getBoolean(), "volatile", null, 0, 1, FieldApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(methodApiEClass, MethodApi.class, "MethodApi", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getMethodApi_Abstract(), theXMLTypePackage.getBoolean(), "abstract", null, 0, 1, MethodApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodApi_ExceptionType(), this.getListOfTypes(), "exceptionType", null, 0, 1, MethodApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodApi_Final(), theXMLTypePackage.getBoolean(), "final", null, 0, 1, MethodApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodApi_InputType(), this.getListOfTypes(), "inputType", null, 0, 1, MethodApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodApi_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, MethodApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodApi_Native(), theXMLTypePackage.getBoolean(), "native", null, 0, 1, MethodApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodApi_ReturnType(), theXMLTypePackage.getString(), "returnType", null, 0, 1, MethodApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodApi_Static(), theXMLTypePackage.getBoolean(), "static", null, 0, 1, MethodApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodApi_Strict(), theXMLTypePackage.getBoolean(), "strict", null, 0, 1, MethodApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodApi_Synchronized(), theXMLTypePackage.getBoolean(), "synchronized", null, 0, 1, MethodApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodApi_Visibility(), this.getVisibility(), "visibility", "public", 1, 1, MethodApi.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(packageEClass, org.eclipse.component.api.Package.class, "Package", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getPackage_ClassApi(), this.getClassApi(), null, "classApi", null, 1, -1, org.eclipse.component.api.Package.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPackage_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, org.eclipse.component.api.Package.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Initialize enums and add enum literals
    initEEnum(visibilityEEnum, Visibility.class, "Visibility");
    addEEnumLiteral(visibilityEEnum, Visibility.PUBLIC_LITERAL);
    addEEnumLiteral(visibilityEEnum, Visibility.PROTECTED_LITERAL);
    addEEnumLiteral(visibilityEEnum, Visibility.PRIVATE_LITERAL);

    // Initialize data types
    initEDataType(listOfTypesEDataType, List.class, "ListOfTypes", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
    initEDataType(visibilityObjectEDataType, Visibility.class, "VisibilityObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);

    // Create resource
    createResource(eNS_URI);

    // Create annotations
    // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
    createExtendedMetaDataAnnotations();
  }

  /**
   * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createExtendedMetaDataAnnotations()
  {
    String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";		
    addAnnotation
      (apiTypesEClass, 
       source, 
       new String[] 
       {
       "name", "api-types",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getApiTypes_Package(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "package"
       });		
    addAnnotation
      (classApiEClass, 
       source, 
       new String[] 
       {
       "name", "class-api",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getClassApi_MethodApi(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "method-api"
       });		
    addAnnotation
      (getClassApi_FieldApi(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "field-api"
       });		
    addAnnotation
      (getClassApi_Implement(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "implement"
       });		
    addAnnotation
      (getClassApi_Instantiate(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "instantiate"
       });		
    addAnnotation
      (getClassApi_Name(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "name"
       });		
    addAnnotation
      (getClassApi_Reference(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "reference"
       });		
    addAnnotation
      (getClassApi_Subclass(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "subclass"
       });		
    addAnnotation
      (componentApiTypeEClass, 
       source, 
       new String[] 
       {
       "name", "component-api_._type",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getComponentApiType_InternalApis(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "internal-apis"
       });		
    addAnnotation
      (getComponentApiType_ExternalApis(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "external-apis"
       });			
    addAnnotation
      (getComponentApiType_Name(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "name"
       });		
    addAnnotation
      (getComponentApiType_Version(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "version"
       });		
    addAnnotation
      (documentRootEClass, 
       source, 
       new String[] 
       {
       "name", "",
       "kind", "mixed"
       });		
    addAnnotation
      (getDocumentRoot_Mixed(), 
       source, 
       new String[] 
       {
       "kind", "elementWildcard",
       "name", ":mixed"
       });		
    addAnnotation
      (getDocumentRoot_XMLNSPrefixMap(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "xmlns:prefix"
       });		
    addAnnotation
      (getDocumentRoot_XSISchemaLocation(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "xsi:schemaLocation"
       });		
    addAnnotation
      (getDocumentRoot_ComponentApi(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "component-api",
       "namespace", "##targetNamespace"
       });		
    addAnnotation
      (fieldApiEClass, 
       source, 
       new String[] 
       {
       "name", "field-api",
       "kind", "empty"
       });		
    addAnnotation
      (getFieldApi_Final(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "final"
       });		
    addAnnotation
      (getFieldApi_Name(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "name"
       });		
    addAnnotation
      (getFieldApi_Static(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "static"
       });		
    addAnnotation
      (getFieldApi_Transient(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "transient"
       });		
    addAnnotation
      (getFieldApi_Type(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "type"
       });		
    addAnnotation
      (getFieldApi_Visibility(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "visibility"
       });		
    addAnnotation
      (getFieldApi_Volatile(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "volatile"
       });		
    addAnnotation
      (listOfTypesEDataType, 
       source, 
       new String[] 
       {
       "name", "list-of-types",
       "itemType", "http://www.eclipse.org/emf/2003/XMLType#string"
       });		
    addAnnotation
      (methodApiEClass, 
       source, 
       new String[] 
       {
       "name", "method-api",
       "kind", "empty"
       });		
    addAnnotation
      (getMethodApi_Abstract(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "abstract"
       });		
    addAnnotation
      (getMethodApi_ExceptionType(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "exception-type"
       });		
    addAnnotation
      (getMethodApi_Final(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "final"
       });		
    addAnnotation
      (getMethodApi_InputType(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "input-type"
       });		
    addAnnotation
      (getMethodApi_Name(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "name"
       });		
    addAnnotation
      (getMethodApi_Native(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "native"
       });		
    addAnnotation
      (getMethodApi_ReturnType(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "return-type"
       });		
    addAnnotation
      (getMethodApi_Static(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "static"
       });		
    addAnnotation
      (getMethodApi_Strict(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "strict"
       });		
    addAnnotation
      (getMethodApi_Synchronized(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "synchronized"
       });		
    addAnnotation
      (getMethodApi_Visibility(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "visibility"
       });		
    addAnnotation
      (packageEClass, 
       source, 
       new String[] 
       {
       "name", "package",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getPackage_ClassApi(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "class-api"
       });		
    addAnnotation
      (getPackage_Name(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "name"
       });		
    addAnnotation
      (visibilityEEnum, 
       source, 
       new String[] 
       {
       "name", "visibility"
       });		
    addAnnotation
      (visibilityObjectEDataType, 
       source, 
       new String[] 
       {
       "name", "visibility:Object",
       "baseType", "visibility"
       });
  }

} //ApiPackageImpl
