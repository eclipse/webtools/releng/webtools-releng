/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: MethodApi.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.api;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method Api</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.component.api.MethodApi#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link org.eclipse.component.api.MethodApi#getExceptionType <em>Exception Type</em>}</li>
 *   <li>{@link org.eclipse.component.api.MethodApi#isFinal <em>Final</em>}</li>
 *   <li>{@link org.eclipse.component.api.MethodApi#getInputType <em>Input Type</em>}</li>
 *   <li>{@link org.eclipse.component.api.MethodApi#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.component.api.MethodApi#isNative <em>Native</em>}</li>
 *   <li>{@link org.eclipse.component.api.MethodApi#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link org.eclipse.component.api.MethodApi#isStatic <em>Static</em>}</li>
 *   <li>{@link org.eclipse.component.api.MethodApi#isStrict <em>Strict</em>}</li>
 *   <li>{@link org.eclipse.component.api.MethodApi#isSynchronized <em>Synchronized</em>}</li>
 *   <li>{@link org.eclipse.component.api.MethodApi#getVisibility <em>Visibility</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.component.api.ApiPackage#getMethodApi()
 * @model 
 * @generated
 */
public interface MethodApi extends EObject
{
  /**
   * Returns the value of the '<em><b>Abstract</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Abstract</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Abstract</em>' attribute.
   * @see #isSetAbstract()
   * @see #unsetAbstract()
   * @see #setAbstract(boolean)
   * @see org.eclipse.component.api.ApiPackage#getMethodApi_Abstract()
   * @model unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isAbstract();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.MethodApi#isAbstract <em>Abstract</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Abstract</em>' attribute.
   * @see #isSetAbstract()
   * @see #unsetAbstract()
   * @see #isAbstract()
   * @generated
   */
  void setAbstract(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.MethodApi#isAbstract <em>Abstract</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetAbstract()
   * @see #isAbstract()
   * @see #setAbstract(boolean)
   * @generated
   */
  void unsetAbstract();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.MethodApi#isAbstract <em>Abstract</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Abstract</em>' attribute is set.
   * @see #unsetAbstract()
   * @see #isAbstract()
   * @see #setAbstract(boolean)
   * @generated
   */
  boolean isSetAbstract();

  /**
   * Returns the value of the '<em><b>Exception Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Exception Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exception Type</em>' attribute.
   * @see #setExceptionType(List)
   * @see org.eclipse.component.api.ApiPackage#getMethodApi_ExceptionType()
   * @model unique="false" dataType="org.eclipse.component.api.ListOfTypes" many="false"
   * @generated
   */
  List getExceptionType();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.MethodApi#getExceptionType <em>Exception Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Exception Type</em>' attribute.
   * @see #getExceptionType()
   * @generated
   */
  void setExceptionType(List value);

  /**
   * Returns the value of the '<em><b>Final</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Final</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Final</em>' attribute.
   * @see #isSetFinal()
   * @see #unsetFinal()
   * @see #setFinal(boolean)
   * @see org.eclipse.component.api.ApiPackage#getMethodApi_Final()
   * @model unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isFinal();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.MethodApi#isFinal <em>Final</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Final</em>' attribute.
   * @see #isSetFinal()
   * @see #unsetFinal()
   * @see #isFinal()
   * @generated
   */
  void setFinal(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.MethodApi#isFinal <em>Final</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetFinal()
   * @see #isFinal()
   * @see #setFinal(boolean)
   * @generated
   */
  void unsetFinal();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.MethodApi#isFinal <em>Final</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Final</em>' attribute is set.
   * @see #unsetFinal()
   * @see #isFinal()
   * @see #setFinal(boolean)
   * @generated
   */
  boolean isSetFinal();

  /**
   * Returns the value of the '<em><b>Input Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Input Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Input Type</em>' attribute.
   * @see #setInputType(List)
   * @see org.eclipse.component.api.ApiPackage#getMethodApi_InputType()
   * @model unique="false" dataType="org.eclipse.component.api.ListOfTypes" many="false"
   * @generated
   */
  List getInputType();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.MethodApi#getInputType <em>Input Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Input Type</em>' attribute.
   * @see #getInputType()
   * @generated
   */
  void setInputType(List value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.eclipse.component.api.ApiPackage#getMethodApi_Name()
   * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.MethodApi#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Native</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Native</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Native</em>' attribute.
   * @see #isSetNative()
   * @see #unsetNative()
   * @see #setNative(boolean)
   * @see org.eclipse.component.api.ApiPackage#getMethodApi_Native()
   * @model unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isNative();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.MethodApi#isNative <em>Native</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Native</em>' attribute.
   * @see #isSetNative()
   * @see #unsetNative()
   * @see #isNative()
   * @generated
   */
  void setNative(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.MethodApi#isNative <em>Native</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetNative()
   * @see #isNative()
   * @see #setNative(boolean)
   * @generated
   */
  void unsetNative();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.MethodApi#isNative <em>Native</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Native</em>' attribute is set.
   * @see #unsetNative()
   * @see #isNative()
   * @see #setNative(boolean)
   * @generated
   */
  boolean isSetNative();

  /**
   * Returns the value of the '<em><b>Return Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Return Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Return Type</em>' attribute.
   * @see #setReturnType(String)
   * @see org.eclipse.component.api.ApiPackage#getMethodApi_ReturnType()
   * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String"
   * @generated
   */
  String getReturnType();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.MethodApi#getReturnType <em>Return Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Return Type</em>' attribute.
   * @see #getReturnType()
   * @generated
   */
  void setReturnType(String value);

  /**
   * Returns the value of the '<em><b>Static</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Static</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Static</em>' attribute.
   * @see #isSetStatic()
   * @see #unsetStatic()
   * @see #setStatic(boolean)
   * @see org.eclipse.component.api.ApiPackage#getMethodApi_Static()
   * @model unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isStatic();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.MethodApi#isStatic <em>Static</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Static</em>' attribute.
   * @see #isSetStatic()
   * @see #unsetStatic()
   * @see #isStatic()
   * @generated
   */
  void setStatic(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.MethodApi#isStatic <em>Static</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetStatic()
   * @see #isStatic()
   * @see #setStatic(boolean)
   * @generated
   */
  void unsetStatic();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.MethodApi#isStatic <em>Static</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Static</em>' attribute is set.
   * @see #unsetStatic()
   * @see #isStatic()
   * @see #setStatic(boolean)
   * @generated
   */
  boolean isSetStatic();

  /**
   * Returns the value of the '<em><b>Strict</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Strict</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Strict</em>' attribute.
   * @see #isSetStrict()
   * @see #unsetStrict()
   * @see #setStrict(boolean)
   * @see org.eclipse.component.api.ApiPackage#getMethodApi_Strict()
   * @model unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isStrict();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.MethodApi#isStrict <em>Strict</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Strict</em>' attribute.
   * @see #isSetStrict()
   * @see #unsetStrict()
   * @see #isStrict()
   * @generated
   */
  void setStrict(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.MethodApi#isStrict <em>Strict</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetStrict()
   * @see #isStrict()
   * @see #setStrict(boolean)
   * @generated
   */
  void unsetStrict();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.MethodApi#isStrict <em>Strict</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Strict</em>' attribute is set.
   * @see #unsetStrict()
   * @see #isStrict()
   * @see #setStrict(boolean)
   * @generated
   */
  boolean isSetStrict();

  /**
   * Returns the value of the '<em><b>Synchronized</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Synchronized</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Synchronized</em>' attribute.
   * @see #isSetSynchronized()
   * @see #unsetSynchronized()
   * @see #setSynchronized(boolean)
   * @see org.eclipse.component.api.ApiPackage#getMethodApi_Synchronized()
   * @model unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isSynchronized();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.MethodApi#isSynchronized <em>Synchronized</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Synchronized</em>' attribute.
   * @see #isSetSynchronized()
   * @see #unsetSynchronized()
   * @see #isSynchronized()
   * @generated
   */
  void setSynchronized(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.MethodApi#isSynchronized <em>Synchronized</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetSynchronized()
   * @see #isSynchronized()
   * @see #setSynchronized(boolean)
   * @generated
   */
  void unsetSynchronized();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.MethodApi#isSynchronized <em>Synchronized</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Synchronized</em>' attribute is set.
   * @see #unsetSynchronized()
   * @see #isSynchronized()
   * @see #setSynchronized(boolean)
   * @generated
   */
  boolean isSetSynchronized();

  /**
   * Returns the value of the '<em><b>Visibility</b></em>' attribute.
   * The default value is <code>"public"</code>.
   * The literals are from the enumeration {@link org.eclipse.component.api.Visibility}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Visibility</em>' attribute.
   * @see org.eclipse.component.api.Visibility
   * @see #isSetVisibility()
   * @see #unsetVisibility()
   * @see #setVisibility(Visibility)
   * @see org.eclipse.component.api.ApiPackage#getMethodApi_Visibility()
   * @model default="public" unique="false" unsettable="true" required="true"
   * @generated
   */
  Visibility getVisibility();

  /**
   * Sets the value of the '{@link org.eclipse.component.api.MethodApi#getVisibility <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Visibility</em>' attribute.
   * @see org.eclipse.component.api.Visibility
   * @see #isSetVisibility()
   * @see #unsetVisibility()
   * @see #getVisibility()
   * @generated
   */
  void setVisibility(Visibility value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.api.MethodApi#getVisibility <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetVisibility()
   * @see #getVisibility()
   * @see #setVisibility(Visibility)
   * @generated
   */
  void unsetVisibility();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.api.MethodApi#getVisibility <em>Visibility</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Visibility</em>' attribute is set.
   * @see #unsetVisibility()
   * @see #getVisibility()
   * @see #setVisibility(Visibility)
   * @generated
   */
  boolean isSetVisibility();

} // MethodApi
