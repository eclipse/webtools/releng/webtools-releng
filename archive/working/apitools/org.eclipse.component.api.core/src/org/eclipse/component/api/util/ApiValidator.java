/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ApiValidator.java,v 1.1 2005/01/20 12:03:04 ryman Exp $
 */
package org.eclipse.component.api.util;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.component.api.ApiPackage;
import org.eclipse.component.api.ApiTypes;
import org.eclipse.component.api.ClassApi;
import org.eclipse.component.api.ComponentApiType;
import org.eclipse.component.api.DocumentRoot;
import org.eclipse.component.api.FieldApi;
import org.eclipse.component.api.MethodApi;
import org.eclipse.component.api.Visibility;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.eclipse.emf.ecore.xml.type.util.XMLTypeValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.component.api.ApiPackage
 * @generated
 */
public class ApiValidator extends EObjectValidator
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final ApiValidator INSTANCE = new ApiValidator();

  /**
   * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.common.util.Diagnostic#getSource()
   * @see org.eclipse.emf.common.util.Diagnostic#getCode()
   * @generated
   */
  public static final String DIAGNOSTIC_SOURCE = "org.eclipse.component.api";

  /**
   * A constant with a fixed name that can be used as the base value for additional hand written constants.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

  /**
   * The cached base package validator.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected XMLTypeValidator xmlTypeValidator;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ApiValidator()
  {
    xmlTypeValidator = XMLTypeValidator.INSTANCE;
  }

  /**
   * Returns the package of this validator switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EPackage getEPackage()
  {
    return ApiPackage.eINSTANCE;
  }

  /**
   * Calls <code>validateXXX</code> for the corresonding classifier of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map context)
  {
    switch (classifierID)
    {
      case ApiPackage.API_TYPES:
        return validateApiTypes((ApiTypes)value, diagnostics, context);
      case ApiPackage.CLASS_API:
        return validateClassApi((ClassApi)value, diagnostics, context);
      case ApiPackage.COMPONENT_API_TYPE:
        return validateComponentApiType((ComponentApiType)value, diagnostics, context);
      case ApiPackage.DOCUMENT_ROOT:
        return validateDocumentRoot((DocumentRoot)value, diagnostics, context);
      case ApiPackage.FIELD_API:
        return validateFieldApi((FieldApi)value, diagnostics, context);
      case ApiPackage.METHOD_API:
        return validateMethodApi((MethodApi)value, diagnostics, context);
      case ApiPackage.PACKAGE:
        return validatePackage((org.eclipse.component.api.Package)value, diagnostics, context);
      case ApiPackage.VISIBILITY:
        return validateVisibility((Object)value, diagnostics, context);
      case ApiPackage.LIST_OF_TYPES:
        return validateListOfTypes((List)value, diagnostics, context);
      case ApiPackage.VISIBILITY_OBJECT:
        return validateVisibilityObject((Visibility)value, diagnostics, context);
      default: 
        return true;
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateApiTypes(ApiTypes apiTypes, DiagnosticChain diagnostics, Map context)
  {
    return validate_EveryDefaultConstraint(apiTypes, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateClassApi(ClassApi classApi, DiagnosticChain diagnostics, Map context)
  {
    return validate_EveryDefaultConstraint(classApi, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateComponentApiType(ComponentApiType componentApiType, DiagnosticChain diagnostics, Map context)
  {
    return validate_EveryDefaultConstraint(componentApiType, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateDocumentRoot(DocumentRoot documentRoot, DiagnosticChain diagnostics, Map context)
  {
    return validate_EveryDefaultConstraint(documentRoot, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateFieldApi(FieldApi fieldApi, DiagnosticChain diagnostics, Map context)
  {
    return validate_EveryDefaultConstraint(fieldApi, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateMethodApi(MethodApi methodApi, DiagnosticChain diagnostics, Map context)
  {
    return validate_EveryDefaultConstraint(methodApi, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validatePackage(org.eclipse.component.api.Package package_, DiagnosticChain diagnostics, Map context)
  {
    return validate_EveryDefaultConstraint(package_, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateVisibility(Object visibility, DiagnosticChain diagnostics, Map context)
  {
    return true;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateListOfTypes(List listOfTypes, DiagnosticChain diagnostics, Map context)
  {
    boolean result = validateListOfTypes_ItemType(listOfTypes, diagnostics, context);
    return result;
  }

  /**
   * Validates the ItemType constraint of '<em>List Of Types</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateListOfTypes_ItemType(List listOfTypes, DiagnosticChain diagnostics, Map context)
  {
    boolean result = true;
    for (Iterator i = listOfTypes.iterator(); i.hasNext() && (result || diagnostics != null); )
    {
      Object item = i.next();
      if (XMLTypePackage.eINSTANCE.getString().isInstance(item))
      {
        result &= xmlTypeValidator.validateString((String)item, diagnostics, context);
      }
      else
      {
        result = false;
        reportDataValueTypeViolation(XMLTypePackage.eINSTANCE.getString(), item, diagnostics, context);
      }
    }
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateVisibilityObject(Visibility visibilityObject, DiagnosticChain diagnostics, Map context)
  {
    return true;
  }

} //ApiValidator
