/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.util;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

public class CommandOptionParser
{
  private String delimiter = "-";
  private Map options;

  public CommandOptionParser(String[] args)
  {
    parse(args);
  }

  public Map getOptions()
  {
    return options;
  }

  private void parse(String[] args)
  {
    options = new HashMap();
    String option = null;
    for (int i = 0; i < args.length; i++)
    {
      if (args[i] != null)
      {
        if (args[i].startsWith(delimiter))
        {
          option = args[i].substring(1);
          options.put(option, new ArrayList(1));
        }
        else if (option != null)
        {
          ((List)options.get(option)).add(args[i]);
        }
      }
    }
  }
}
