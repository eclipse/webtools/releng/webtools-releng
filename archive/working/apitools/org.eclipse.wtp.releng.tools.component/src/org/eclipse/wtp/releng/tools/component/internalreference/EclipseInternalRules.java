/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internalreference;

import java.util.Iterator;
import java.util.Set;

/**
 * An example implementation of the <code>IInteranlRules</code> interface
 * which uses the eclipse.org API rules to dictate which classes are internal
 * and which classes are not.
 */
public class EclipseInternalRules implements IInternalRules {
	public static final String INTERNAL= ".internal.";

	/**
	 * See http://eclipse.org/eclipse/development/java-api-evolution.html
	 * 
	 * @see com.example.internalreference.IInternalRules#isInternal(java.lang.String, com.example.internalreference.Type, com.example.internalreference.Library, com.example.internalreference.Plugin)
	 */
	public boolean isInternal(String referencedType, Type type, Library library, Plugin plugin) {
		if (referencedType.indexOf(INTERNAL) > -1) {
			if (plugin.containsType(referencedType)) {
				return false;
			}
			Set fragments= plugin.getFragments();
			for (Iterator i = fragments.iterator(); i.hasNext();) {
				Fragment fragment = (Fragment) i.next();
				if (fragment.containsType(referencedType)) {
					return false;
				}
			}
			if (plugin instanceof Fragment) {
				Fragment fragment = (Fragment) plugin;
				if (fragment.getPlugin().containsType(referencedType)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

}
