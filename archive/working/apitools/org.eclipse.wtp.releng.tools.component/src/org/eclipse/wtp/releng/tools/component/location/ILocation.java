/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.location;

import java.io.IOException;
import java.io.InputStream;

/**
 * An <code>ILocation</code> object is an abstraction on the idea
 * of a file.  ILocations can refer to files that are directly located
 * in the filesystem, or they can refer to files that are housed in
 * zip/jar files.  Using an <code>ILocation</code>, clients can traverse
 * zip/jar files like they are directories.
 * <p>
 * Clients can create <code>ILocation</code> objects using 
 * com.example.location.Location.createLocation(File file)
 */
public interface ILocation {
	/**
	 * @return ILocation This location's parent, or <code>null</code>
	 * 	if this location was created without a parent.
	 */
	ILocation getParent();
	/**
	 * @return String The name of this location.
	 */
	String getName();
	/**
	 * @return String The absolute path of this location, this path 
	 *  may not be usable by new File(String) to create files because
	 * 	it may refer to structures inside of zip/jar files. 
	 */
	String getAbsolutePath();
	/**
	 * Answers the <code>InputStream</code>
	 * @return InputStream
	 * @throws IOException
	 */
	InputStream getInputStream() throws IOException;
	/**
	 * @return ILocationChildrenIterator which iterates over the children
	 * 	of this location.
	 */
	ILocationChildrenIterator childIterator();
	/**
	 * @return boolean <code>true</code> if this location has children.
	 */
	boolean hasChildren();
	/**
	 * Method accept.
	 * @param visitor
	 */
	void accept(ILocationVisitor visitor);
	/**
	 * Method createChild.
	 * @param relativePath
	 * @return ILocation
	 */
	ILocation createChild(String relativePath);
	/**
	 * Method createSibling.
	 * @param relativePath
	 * @return ILocation
	 */
	ILocation createSibling(String relativePath);
}
