/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import org.eclipse.wtp.releng.tools.component.internalreference.Fragment;
import org.eclipse.wtp.releng.tools.component.internalreference.Library;
import org.eclipse.wtp.releng.tools.component.internalreference.Plugin;
import org.eclipse.wtp.releng.tools.component.model.util.ModelResourceFactoryImpl;
import org.eclipse.wtp.releng.tools.component.model.util.ModelResourceImpl;
import org.eclipse.wtp.releng.tools.component.util.EmitterUtils;
import org.eclipse.wtp.releng.tools.component.model.ComponentDependsType;
import org.eclipse.wtp.releng.tools.component.model.ComponentType;
import org.eclipse.wtp.releng.tools.component.model.DocumentRoot;
import org.eclipse.wtp.releng.tools.component.model.ModelFactory;
import org.eclipse.wtp.releng.tools.component.model.ModelPackage;
import org.eclipse.wtp.releng.tools.component.model.PackageType;
import org.eclipse.wtp.releng.tools.component.model.PluginType;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class ComponentEmitter
{
  private String compDirLoc;
  private String propsLoc;
  private Map pluginId2Plugin;
  private Map fragmentId2Fragment;

  public ComponentEmitter(String[] eclipseDirLocs, String compDirLoc, String propsLoc)
  {
    this.compDirLoc = EmitterUtils.addTrailingSeperator(compDirLoc);
    this.propsLoc = propsLoc;
    pluginId2Plugin = new HashMap();
    fragmentId2Fragment = new HashMap();
    for (int i = 0; i < eclipseDirLocs.length; i++)
    {
      File eclipseDir = new File(EmitterUtils.addTrailingSeperator(eclipseDirLocs[i]));
      if (eclipseDir.exists() && eclipseDir.isDirectory())
        EmitterUtils.harvestPlugins(eclipseDir, pluginId2Plugin, fragmentId2Fragment);
    }
    EmitterUtils.linkPluginsAndFragments(pluginId2Plugin, fragmentId2Fragment);
  }

  public void genComponentXML() throws IOException
  {
    if (propsLoc != null)
    {
      Properties props = new Properties();
      props.load(new FileInputStream(propsLoc));
      for (Iterator it = props.keySet().iterator(); it.hasNext();)
      {
        String compName = (String)it.next();
        StringTokenizer pluginIds = new StringTokenizer((String)props.get(compName), " ");
        genComponentXML(compName, pluginIds);
      }
    }
    else
    {
      for (Iterator it = pluginId2Plugin.keySet().iterator(); it.hasNext();)
      {
        String pluginId = (String)it.next();
        genComponentXML(pluginId, new StringTokenizer(pluginId, " "));
      }
    }
  }

  public void genComponentXML(String compName, StringTokenizer pluginIds) throws IOException
  {
    ComponentType comp = getComponentType(compName);
    EList plugins = comp.getPlugin();
    while (pluginIds.hasMoreTokens())
    {
      String pluginId = pluginIds.nextToken();
      Fragment fragment = (Fragment)fragmentId2Fragment.get(pluginId);
      if (fragment != null)
        addPluginType(comp, pluginId, true);
      else
      {
        Plugin plugin = (Plugin)pluginId2Plugin.get(pluginId);
        if (plugin != null)
        {
          addPluginType(comp, pluginId, false);
          List libs = plugin.getLibraries();
          for (Iterator libsIt = libs.iterator(); libsIt.hasNext();)
          {
            Library lib = (Library)libsIt.next();
            Map types = lib.getTypes();
            for (Iterator typesIt = types.keySet().iterator(); typesIt.hasNext();)
            {
              String typeName = (String)typesIt.next();
              int index = typeName.lastIndexOf('.');
              String pkgName;
              if (index != -1)
              {
                pkgName = typeName.substring(0, index);
                if (pkgName.indexOf("internal") == -1)
                  addPackageType(comp, pkgName);
              }
            }
            lib.reset();
          }
        }
      }
    }
    System.out.println("Writing component.xml for " + compName);
    saveComponent(comp, new File(getComponentXMLLoc(compName)));
  }

  private PluginType addPluginType(ComponentType comp, String pluginId, boolean fragment)
  {
    EList plugins = comp.getPlugin();
    for (Iterator it = plugins.iterator(); it.hasNext();)
    {
      PluginType pluginType = (PluginType)it.next();
      if (pluginType.getId().equals(pluginId))
        return pluginType;
    }
    PluginType pluginType = ModelFactory.eINSTANCE.createPluginType();
    pluginType.setId(pluginId);
    pluginType.setFragment(fragment);
    plugins.add(pluginType);
    return pluginType;
  }

  private PackageType addPackageType(ComponentType comp, String pkgName)
  {
    EList pkgs = comp.getPackage();
    for (Iterator it = pkgs.iterator(); it.hasNext();)
    {
      PackageType pkgType = (PackageType)it.next();
      if (pkgType.getName().equals(pkgName))
        return pkgType;
    }
    PackageType pkgType = ModelFactory.eINSTANCE.createPackageType();
    pkgType.setName(pkgName);
    pkgs.add(pkgType);
    return pkgType;
  }

  private String getComponentXMLLoc(String compName)
  {
    StringBuffer sb = new StringBuffer(compDirLoc);
    sb.append(compName);
    sb.append('/');
    sb.append(Component.CONST_COMPONENT_XML);
    return sb.toString();
  }

  private ComponentType getComponentType(String compName)
  {
    try
    {
      String compXMLLoc = getComponentXMLLoc(compName);
      File f = new File(compXMLLoc);
      if (f.exists())
      {
        FileInputStream fis = new FileInputStream(f);
        ResourceSet res = new ResourceSetImpl();
        res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentxml", new ModelResourceFactoryImpl());
        res.getPackageRegistry().put(ModelPackage.eNS_URI, ModelPackage.eINSTANCE);
        ModelResourceImpl compRes = (ModelResourceImpl)res.createResource(URI.createURI("*.componentxml"));
        compRes.load(fis, res.getLoadOptions());
        compRes.setURI(URI.createURI(compXMLLoc));
        EList contents = compRes.getContents();
        if (contents.size() == 1 && contents.get(0) instanceof DocumentRoot)
          return ((DocumentRoot)contents.get(0)).getComponent();
      }
    }
    catch (FileNotFoundException fnfe)
    {
      fnfe.printStackTrace();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
    ComponentType comp = ModelFactory.eINSTANCE.createComponentType();
    comp.setName(compName);
    ComponentDependsType depends = ModelFactory.eINSTANCE.createComponentDependsType();
    depends.setUnrestricted(true);
    comp.setComponentDepends(depends);
    return comp;
  }

  private void saveComponent(ComponentType comp, File file) throws IOException
  {
    file.getParentFile().mkdirs();
    ResourceSet res = new ResourceSetImpl();
    res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentxml", new ModelResourceFactoryImpl());
    res.getPackageRegistry().put(ModelPackage.eNS_URI, ModelPackage.eINSTANCE);
    ModelResourceImpl compRes = (ModelResourceImpl)res.createResource(URI.createURI("*.componentxml"));
    DocumentRoot root = ModelFactory.eINSTANCE.createDocumentRoot();
    root.setComponent(comp);
    compRes.getContents().add(root);
    compRes.save(new BufferedOutputStream(new FileOutputStream(file)), new HashMap());
  }
}
