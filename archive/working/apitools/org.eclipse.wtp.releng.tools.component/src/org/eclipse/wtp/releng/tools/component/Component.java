/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.eclipse.wtp.releng.tools.component.model.ComponentType;
import org.eclipse.wtp.releng.tools.component.model.DocumentRoot;
import org.eclipse.wtp.releng.tools.component.model.ModelPackage;
import org.eclipse.wtp.releng.tools.component.model.util.ModelResourceFactoryImpl;
import org.eclipse.wtp.releng.tools.component.model.util.ModelResourceImpl;
import org.eclipse.wtp.releng.tools.component.api.ApiPackage;
import org.eclipse.wtp.releng.tools.component.api.ComponentApiType;
import org.eclipse.wtp.releng.tools.component.api.util.ApiResourceFactoryImpl;
import org.eclipse.wtp.releng.tools.component.api.util.ApiResourceImpl;
import org.eclipse.wtp.releng.tools.component.use.ComponentUseType;
import org.eclipse.wtp.releng.tools.component.use.UsePackage;
import org.eclipse.wtp.releng.tools.component.use.util.UseResourceFactoryImpl;
import org.eclipse.wtp.releng.tools.component.use.util.UseResourceImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class Component
{
  public static final String CONST_COMPONENT_XML = "component.xml";
  public static final String CONST_COMPONENT_API_XML = "component-api.xml";
  public static final String CONST_COMPONENT_USE_XML = "component-use.xml";
  public static final String CONST_COMPONENT_VIOLATION_XML = "component-violation.xml";

  private String compLoc;
  private String compApiLoc;
  private ComponentType compXML;
  private ComponentApiType compApiXml;
  private ComponentUseType compUseXML;
  private ComponentUseType compViolationXML;

  /**
   * @return Returns the compLoc.
   */
  public String getCompLoc()
  {
    return compLoc;
  }

  /**
   * @param compLoc The compLoc to set.
   */
  public void setCompLoc(String compLoc)
  {
    this.compLoc = compLoc;
  }

  /**
   * @return Returns the compApiLoc.
   */
  public String getCompApiLoc()
  {
    return compApiLoc;
  }

  /**
   * @param compApiLoc The compApiLoc to set.
   */
  public void setCompApiLoc(String compApiLoc)
  {
    this.compApiLoc = compApiLoc;
  }

  /**
   * @return Returns the compXML.
   */
  public ComponentType getCompXML()
  {
    if (compXML == null)
    {
      try
      {
        String compXMLLoc = compLoc + CONST_COMPONENT_XML;
        FileInputStream fis = new FileInputStream(compXMLLoc);
        ResourceSet res = new ResourceSetImpl();
        res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentxml", new ModelResourceFactoryImpl());
        res.getPackageRegistry().put(ModelPackage.eNS_URI, ModelPackage.eINSTANCE);
        ModelResourceImpl compRes = (ModelResourceImpl)res.createResource(URI.createURI("*.componentxml"));
        compRes.load(fis, res.getLoadOptions());
        compRes.setURI(URI.createURI(compXMLLoc));
        EList contents = compRes.getContents();
        if (contents.size() == 1 && contents.get(0) instanceof DocumentRoot)
          compXML = ((DocumentRoot)contents.get(0)).getComponent();
      }
      catch (FileNotFoundException fnfe)
      {
        fnfe.printStackTrace();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
    }
    return compXML;
  }

  /**
   * @param compXML The compXML to set.
   */
  public void setCompXML(ComponentType compXML)
  {
    this.compXML = compXML;
  }

  /**
   * @return Returns the compApiXml.
   */
  public ComponentApiType getCompApiXml()
  {
    if (compApiXml == null)
    {
      try
      {
        String compApiXmlLoc = compApiLoc + CONST_COMPONENT_API_XML;
        FileInputStream fis = new FileInputStream(compApiXmlLoc);
        ResourceSet res = new ResourceSetImpl();
        res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentapixml", new ApiResourceFactoryImpl());
        res.getPackageRegistry().put(ApiPackage.eNS_URI, ApiPackage.eINSTANCE);
        ApiResourceImpl apiRes = (ApiResourceImpl)res.createResource(URI.createURI("*.componentapixml"));
        apiRes.load(fis, res.getLoadOptions());
        apiRes.setURI(URI.createURI(compApiXmlLoc));
        EList contents = apiRes.getContents();
        if (contents.size() == 1 && contents.get(0) instanceof org.eclipse.wtp.releng.tools.component.api.DocumentRoot)
          compApiXml = ((org.eclipse.wtp.releng.tools.component.api.DocumentRoot)contents.get(0)).getComponentApi();
      }
      catch (FileNotFoundException fnfe)
      {
        fnfe.printStackTrace();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
    }
    return compApiXml;
  }

  /**
   * @param compApiXml The compApiXml to set.
   */
  public void setCompApiXml(ComponentApiType compApiXml)
  {
    this.compApiXml = compApiXml;
  }

  /**
   * @return Returns the compUseXML.
   */
  public ComponentUseType getCompUseXML()
  {
    if (compUseXML == null)
    {
      try
      {
        String compUseXmlLoc = compApiLoc + CONST_COMPONENT_USE_XML;
        FileInputStream fis = new FileInputStream(compUseXmlLoc);
        ResourceSet res = new ResourceSetImpl();
        res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentusexml", new UseResourceFactoryImpl());
        res.getPackageRegistry().put(UsePackage.eNS_URI, UsePackage.eINSTANCE);
        UseResourceImpl useRes = (UseResourceImpl)res.createResource(URI.createURI("*.componentusexml"));
        useRes.load(fis, res.getLoadOptions());
        useRes.setURI(URI.createURI(compUseXmlLoc));
        EList contents = useRes.getContents();
        if (contents.size() == 1 && contents.get(0) instanceof org.eclipse.wtp.releng.tools.component.use.DocumentRoot)
          compUseXML = ((org.eclipse.wtp.releng.tools.component.use.DocumentRoot)contents.get(0)).getComponentUse();
      }
      catch (FileNotFoundException fnfe)
      {
        fnfe.printStackTrace();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
    }
    return compUseXML;
  }

  /**
   * @param compUseXML The compUseXML to set.
   */
  public void setCompUseXML(ComponentUseType compUseXML)
  {
    this.compUseXML = compUseXML;
  }

  /**
   * @return Returns the compViolationXML.
   */
  public ComponentUseType getCompViolationXML()
  {
    if (compViolationXML == null)
    {
      try
      {
        String compViolationXmlLoc = compApiLoc + CONST_COMPONENT_VIOLATION_XML;
        FileInputStream fis = new FileInputStream(compViolationXmlLoc);
        ResourceSet res = new ResourceSetImpl();
        res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentviolationxml", new UseResourceFactoryImpl());
        res.getPackageRegistry().put(UsePackage.eNS_URI, UsePackage.eINSTANCE);
        UseResourceImpl violationRes = (UseResourceImpl)res.createResource(URI.createURI("*.componentviolationxml"));
        violationRes.load(fis, res.getLoadOptions());
        violationRes.setURI(URI.createURI(compViolationXmlLoc));
        EList contents = violationRes.getContents();
        if (contents.size() == 1 && contents.get(0) instanceof org.eclipse.wtp.releng.tools.component.use.DocumentRoot)
          compViolationXML = ((org.eclipse.wtp.releng.tools.component.use.DocumentRoot)contents.get(0)).getComponentUse();
      }
      catch (FileNotFoundException fnfe)
      {
        fnfe.printStackTrace();
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
    }
    return compViolationXML;
  }

  /**
   * @param compViolationXML The compViolationXML to set.
   */
  public void setCompViolationXML(ComponentUseType compViolationXML)
  {
    this.compViolationXML = compViolationXML;
  }
}
