/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internalreference;

import java.io.IOException;
import java.io.InputStream;
import java.util.StringTokenizer;
import java.util.jar.Manifest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.eclipse.wtp.releng.tools.component.location.IFileLocation;
import org.eclipse.wtp.releng.tools.component.location.ILocation;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Creates model objects from configuration files.  Utilizes the
 * xerces parser to read the xml.
 */
public class ConfigurationFileParser {

	private static SAXParser saxParser;
	
	static {
		initializeParser();
	}

	private static void initializeParser() {
		try {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      factory.setNamespaceAware(false);
      factory.setValidating(false);
	 		factory.setFeature("http://xml.org/sax/features/string-interning", true); //$NON-NLS-1$
      saxParser = factory.newSAXParser();
		} catch (SAXException e) {
			// In case support for this feature is removed
		} catch (ParserConfigurationException pce) {
      pce.printStackTrace();  
    }
	}

	/**
	 * Creates a <code>Plugin</code> from a location file 
	 * @param location a location that points to a plugin.xml file, not <code>null</code>.
	 * @return Plugin the Plugin object representation of that file
	 */	
	public static Plugin getPlugin(ILocation location) {
		PluginHandler handler= new PluginHandler(location);
		try {
			parse(location, handler);
		} catch (IOException e) {
			System.err.println("Could not read " + location.getName() + ", skipping");
		}
		return handler.getPlugin();
	}

	/**
	 * Creates a <code>Fragment</code> from a location file 
	 * @param location a location that points to a fragment.xml file, not <code>null</code>.
	 * @return Fragment the Fragment object representation of that file
	 */
	public static Fragment getFragment(ILocation location) {
		FragmentHandler handler= new FragmentHandler(location);
		try {
			parse(location, handler);
		} catch (IOException e) {
			System.err.println("Could not read " + location.getName() + ", skipping");
		}
		return handler.getFragment();
	}	

  /**
   * Creates a <code>Bundle</code> from a location file 
   * @param location a location that points to a MANIFEST.MF file, not <code>null</code>.
   * @return Bundle the Bundle object representation of that file
   */ 
  public static Bundle getBundle(ILocation location)
  {
    Bundle bundle = new Bundle(location);
    try
    {
      Manifest manifest = new Manifest(location.getInputStream());
      java.util.jar.Attributes attrs = manifest.getMainAttributes();
      String bundleName = attrs.getValue(new java.util.jar.Attributes.Name(Bundle.CONST_BUNDLE_NAME));
      if (bundleName != null)
        bundle.setName((new StringTokenizer(bundleName, "; ")).nextToken());
      bundle.setVersion(attrs.getValue(new java.util.jar.Attributes.Name(Bundle.CONST_BUNDLE_VERSION)));
      String classpathString = attrs.getValue(new java.util.jar.Attributes.Name(Bundle.CONST_BUNDLE_CLASSPATH));
      if (classpathString != null && classpathString.length() > 0)
      {
        StringTokenizer classpath = new StringTokenizer(classpathString, ",");
        while (classpath.hasMoreTokens())
          bundle.addLibrary(classpath.nextToken());
      }
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
    return bundle;
  }

	private static void parse(ILocation location, DefaultHandler handler) throws IOException {
		//saxParser.setContentHandler(handler);
		//saxParser.setDTDHandler(handler);
		//saxParser.setEntityResolver(handler);
		//saxParser.setErrorHandler(handler);
		InputStream in= null;
		try {
			in= location.getInputStream();
			saxParser.parse(new InputSource(in), handler);
		} catch (SAXException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static class PluginHandler extends DefaultHandler {
		private Plugin plugin;
		
		public PluginHandler(ILocation location) {
			plugin= new Plugin(location);
		}
		
		public Plugin getPlugin() {
			return plugin;
		}

		public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException {
			if(elementName.equals("plugin") || qName.equals("plugin")) {
        String name = attributes.getValue("id");
        String version = attributes.getValue("version");
        if (name == null)
        {
          String s = ((IFileLocation)plugin.getLocation().getParent()).getFile().getName();
          int index = s.indexOf('_');
          if (index != -1)
          {
            name = s.substring(0, index);
            version = s.substring(index + 1, s.length());
          }
          else
            name = s;
        }
        plugin.setName(name);
				plugin.setVersion(version);
				return;
			}			
	
			if(elementName.equals("library") || qName.equals("library")) {
				plugin.addLibrary(attributes.getValue("name"));
			}
		}
	}

	private static class FragmentHandler extends DefaultHandler {
		private Fragment fragment;
		
		public FragmentHandler(ILocation location) {
			fragment= new Fragment(location);
		}
		
		public Fragment getFragment() {
			return fragment;
		}
		
		
		public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException {
			if(elementName.equals("fragment") || qName.equals("fragment")) { 
				fragment.setFragmentName(attributes.getValue("id"));
				fragment.setVersion(attributes.getValue("version"));
				fragment.setName(attributes.getValue("plugin-id"));
				fragment.setVersion(attributes.getValue("plugin-version"));
				return;
			}			
	
			if(elementName.equals("library") || qName.equals("library")){
				fragment.addLibrary(attributes.getValue("name"));
				return;
			}
		}
	}
}
