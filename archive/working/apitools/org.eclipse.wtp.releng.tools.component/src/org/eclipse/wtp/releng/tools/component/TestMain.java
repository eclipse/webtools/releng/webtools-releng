/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component;

import java.io.IOException;

public class TestMain
{
  public static void main(String[] args) throws IOException
  {
    wtpMain(args);
    //radMain(args);
  }

  public static void wtpMain(String[] args) throws IOException
  {
    String propsDirLoc = "D:/wtp_v10m2/workspace_api/org.eclipse.wtp.releng.tools.component/data/";
    String compDirLoc = "C:/Documents and Settings/chng1me.T40-92U-V46/Desktop/api-test-data/component-xmls-for-wtp/";
    String compApiDirLoc = "C:/Documents and Settings/chng1me.T40-92U-V46/Desktop/api-test-data/api-xmls-for-wtp/";
    String eclipseDirLoc = "D:/wtp_v10m2/eclipse/";

    /*
    String eclipsePropsLoc = propsDirLoc + "eclipse.properties";
    String eclipseCompDirLoc = compDirLoc + "eclipse-components/";
    ComponentEmitter eclipseCompEmitter = new ComponentEmitter(new String[] {eclipseDirLoc}, eclipseCompDirLoc, eclipsePropsLoc);
    eclipseCompEmitter.genComponentXML();
    eclipseCompEmitter = null;
    */

    /*
    String wtpPropsLoc = propsDirLoc + "wtp.properties";
    String wtpCompDirLoc = compDirLoc + "wtp-components/";
    ComponentEmitter wtpCompEmitter = new ComponentEmitter(new String[] {eclipseDirLoc}, wtpCompDirLoc, wtpPropsLoc);
    wtpCompEmitter.genComponentXML();
    wtpCompEmitter = null;
    */

    /*
    String emfPropsLoc = propsDirLoc + "emf.properties";
    String emfCompDirLoc = compDirLoc + "emf-components/";
    ComponentEmitter emfCompEmitter = new ComponentEmitter(new String[] {eclipseDirLoc}, emfCompDirLoc, emfPropsLoc);
    emfCompEmitter.genComponentXML();
    emfCompEmitter = null;
    */

    /*
    String gefPropsLoc = propsDirLoc + "gef.properties";
    String gefCompDirLoc = compDirLoc + "gef-components/";
    ComponentEmitter gefCompEmitter = new ComponentEmitter(new String[] {eclipseDirLoc}, gefCompDirLoc, gefPropsLoc);
    gefCompEmitter.genComponentXML();
    gefCompEmitter = null;
    */

    /*
    String vePropsLoc = propsDirLoc + "ve.properties";
    String veCompDirLoc = compDirLoc + "ve-components/";
    ComponentEmitter veCompEmitter = new ComponentEmitter(new String[] {eclipseDirLoc}, veCompDirLoc, vePropsLoc);
    veCompEmitter.genComponentXML();
    veCompEmitter = null;
    */

    String[] compApiEmitterArgs = new String[]
    {
      "-eclipseDir",
      //eclipseDirLoc,
      "D:/wtp_v10m2/_builds/build/plugins/",
      "D:/wtp_v10m2/_builds/root/eclipse/",
      "-compDir",
      compDirLoc,
      "-compAPIDir",
      compApiDirLoc,
      "-exclude",
      "java.",
      "javax.",
      "org.w3c.",
      "org.xml.",
      "org.apache.",
      "sun."
    };
    ComponentAPIEmitter.main(compApiEmitterArgs);

    String[] apiViolationEmitterArgs = new String[]
    {
      "-compDir",
      compDirLoc,
      "-compAPIDir",
      compApiDirLoc,
      "-xsl",
      "D:/wtp_v10m2/workspace_api/org.eclipse.wtp.releng.tools.component/xsl/component-violation.xsl"
    };
    APIViolationEmitter.main(apiViolationEmitterArgs);
  }

  public static void radMain(String[] args) throws IOException
  {
    String compDirLoc = "C:/Documents and Settings/chng1me.T40-92U-V46/Desktop/api-test-data/component-xmls-for-rad/";
    String compApiDirLoc = "C:/Documents and Settings/chng1me.T40-92U-V46/Desktop/api-test-data/api-xmls-for-rad/";
    String[] radDirLocs = new String[]
    {
      "D:/rad_v600/csdev/",
      "D:/rad_v600/csdevrpt_shared/",
      "D:/rad_v600/eclipse/",
      "D:/rad_v600/rad/",
      "D:/rad_v600/rad_prod/",
      "D:/rad_v600/radrsm_shared/",
      "D:/rad_v600/rwd/",
      "D:/rad_v600/rwdrpt_shared/",
      "D:/rad_v600/sdpisv/",
      "D:/rad_v600/updater/"
    };

    /*
    ComponentEmitter radCompEmitter = new ComponentEmitter(radDirLocs, compDirLoc, null);
    radCompEmitter.genComponentXML();
    radCompEmitter = null;
    */

    String[] compApiEmitterArgs = new String[]
    {
      "-eclipseDir",
      "D:/rad_v600/csdev/",
      "D:/rad_v600/csdevrpt_shared/",
      "D:/rad_v600/eclipse/",
      "D:/rad_v600/rad/",
      "D:/rad_v600/rad_prod/",
      "D:/rad_v600/radrsm_shared/",
      "D:/rad_v600/rwd/",
      "D:/rad_v600/rwdrpt_shared/",
      "D:/rad_v600/sdpisv/",
      "D:/rad_v600/updater/",
      "-compDir",
      compDirLoc,
      "-compAPIDir",
      compApiDirLoc,
      "-include",
      "org.eclipse.",
      "-exclude",
      "java.",
      "javax.",
      "org.w3c.",
      "org.xml.",
      "org.apache.",
      "sun."
    };                                         
    ComponentAPIEmitter.main(compApiEmitterArgs);

    String[] apiViolationEmitterArgs = new String[]
    {
      "-compDir",
      "D:/rad_v600/csdev/",
      "D:/rad_v600/csdevrpt_shared/",
      "D:/rad_v600/eclipse/",
      "D:/rad_v600/rad/",
      "D:/rad_v600/rad_prod/",
      "D:/rad_v600/radrsm_shared/",
      "D:/rad_v600/rwd/",
      "D:/rad_v600/rwdrpt_shared/",
      "D:/rad_v600/sdpisv/",
      "D:/rad_v600/updater/",
      "-compAPIDir",
      compApiDirLoc,
      "-xsl",
      "D:/wtp_v10m2/workspace_api/org.eclipse.wtp.releng.tools.component/xsl/component-violation.xsl"
    };
    APIViolationEmitter.main(apiViolationEmitterArgs);
  }
}