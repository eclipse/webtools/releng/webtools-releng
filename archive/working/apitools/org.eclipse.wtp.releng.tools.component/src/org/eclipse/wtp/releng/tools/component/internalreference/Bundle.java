/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internalreference;

import java.io.File;
import org.eclipse.wtp.releng.tools.component.location.IFileLocation;
import org.eclipse.wtp.releng.tools.component.location.ILocation;
import org.eclipse.wtp.releng.tools.component.location.ZipLocation;

public class Bundle extends Plugin
{
  public static final String CONST_MANIFEST_MF = "MANIFEST.MF";
  public static final String CONST_BUNDLE_NAME = "Bundle-SymbolicName";
  public static final String CONST_BUNDLE_VERSION = "Bundle-Version";
  public static final String CONST_BUNDLE_CLASSPATH = "Bundle-ClassPath";

  public Bundle(ILocation location)
  {
    super(location);
  }

  public void addLibrary(String relativePath)
  {
    File manifest = ((IFileLocation)location).getFile();
    StringBuffer sb = new StringBuffer(manifest.getParentFile().getParentFile().getAbsolutePath());
    sb.append('/');
    sb.append(relativePath);
    File jar = new File(sb.toString());
    if (!jar.exists()) {
      unresolvedLibs.add(relativePath);
      System.err.println(jar);
    }
    addLibrary(new Library(new ZipLocation(jar)));
  }
}
