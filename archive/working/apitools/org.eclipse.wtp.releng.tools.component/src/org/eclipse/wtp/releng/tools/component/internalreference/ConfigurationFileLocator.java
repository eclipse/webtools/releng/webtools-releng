/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internalreference;

import java.util.Map;
import java.util.TreeMap;
import org.eclipse.wtp.releng.tools.component.location.ILocation;
import org.eclipse.wtp.releng.tools.component.location.ILocationVisitor;

/**
 * Traverses an <code>ILocation</code> tree looking for plugin.xml
 * and fragment.xml files.  These files are read and the libraries are
 * extracted.
 */
public class ConfigurationFileLocator implements ILocationVisitor {
	private static final String PLUGIN = "plugin.xml";
	private static final String FRAGMENT = "fragment.xml";
	
	private Map plugins;
	private Map fragments;
	
	public ConfigurationFileLocator() {
		plugins= new TreeMap();
		fragments= new TreeMap();
	}

	/**
	 * Answers a map of plugin objects.
	 * @return Map a map of plugin unique identifier to plugin object, 
	 * 	not <code>null</code>
	 */
	public Map getPlugins() {
		return plugins;
	}
	
	/**
	 * Answers a map of fragment objects.
	 * @return Map a map of fragment unique identifier to fragment object, 
	 * 	not <code>null</code>
	 */
	public Map getFragments() {
		return fragments;
	}
	
	/**
	 * Seeks plugin.xml and fragment.xml files.
	 * 
	 * @see org.eclipse.api.location.ILocationVisitor#accept(org.eclipse.api.location.ILocation)
	 */
	public boolean accept(ILocation location) {
		if(isPlugin(location)) {
			Plugin plugin= ConfigurationFileParser.getPlugin(location);
			plugins.put(plugin.getUniqueIdentifier(), plugin);
			return false;
		}
		
		if (isFragment(location)) {
			Fragment fragment= ConfigurationFileParser.getFragment(location);
			fragments.put(fragment.getUniqueIdentifier(), fragment);
			return false;
		}
		return true;
	}

	private boolean isPlugin(ILocation location) {
		return getLastSegment(location.getName()).toLowerCase().equals(PLUGIN);
	}

	private boolean isFragment(ILocation location) {
		return getLastSegment(location.getName()).toLowerCase().equals(FRAGMENT);
	}
	
	private String getLastSegment(String path) {
		int index= path.lastIndexOf('/');
		if (index < 0) {
			return path;
		}
		return path.substring(index + 1);
	}
}
