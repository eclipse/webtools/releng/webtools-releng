/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internalreference;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.wtp.releng.tools.component.location.ILocation;
import org.eclipse.wtp.releng.tools.component.location.ILocationVisitor;
import org.eclipse.wtp.releng.tools.component.location.Location;

/**
 * A <code>Library</code> is a model object.  Libraries contain
 * types.  Type names are unique in libraries, but two libraries
 * could contain two types with the same name.
 */
public class Library {
	public static final String CLASS= "class";
	
	private ILocation location;
	private Map typeNamesToTypes;
		
	/**
	 * Creates a new <code>Library</code> on the given location.
	 * @param location
	 */
	public Library(ILocation location) {
		this.location= location;
		typeNamesToTypes= null;
	}

  private void init()
  {
    if (typeNamesToTypes == null)
    {
      typeNamesToTypes = new HashMap();
      long time= System.currentTimeMillis();
      typeNamesToTypes= new HashMap();
      location.accept(new ILocationVisitor() {
        public boolean accept(ILocation location) {
          if (isClassFile(location)) {
            Type type= new Type(location); 
            typeNamesToTypes.put(type.getName(), type);
          }
          return true;
        }
      });   
      time= System.currentTimeMillis() - time;
      //System.out.println("Read " +  location.getAbsolutePath() + " in " + time + "ms");
    }
  }

	/**
	 * Answers a mapping of (qualified) type names to <code>Type</code>
	 * objects which are found in this library.
	 * @return Map a mapping of type names to <code>Type</code> objects.
	 */
	public Map getTypes() {
    init();
		return typeNamesToTypes;
	}
	
	/**
	 * Answers <code>true</code> if and only if this library contains
	 * a type with the given (qualified) name.
	 * @param typeName	the name of the type to be found
	 * @return boolean	<code>true</code> if this library has a type
	 * 	with the given name.
	 */
	public boolean containsType(String typeName) {
		return getTypes().containsKey(typeName);
	}

	/**
	 * Answers <code>true</code> if the name of the location ends in
	 * ".class".
	 *  
	 * @param location a location that could be a class file, not <code>null</code>.
	 * @return boolean <code>true</code> if the given location represents
	 * 	a class file. 
	 */
	private boolean isClassFile(ILocation location) {
		return Location.getExtension(location.getName()).equals(CLASS);
	}
	
	/**
	 * Answers the location for this library.
	 * @return ILocation a location, not <code>null</code>.
	 */
	public ILocation getLocation() {
		return location;
	}

  public void reset()
  {
    typeNamesToTypes = null;
  }
}
