/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internalreference;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * The <code>ReportGenerator</code> creates reports on the internal
 * references which were found.
 */
public class ReportGenerator {
	private File fOutputDir;
	
	public ReportGenerator(File outputDir) {
		fOutputDir= outputDir;
	}
	
	/**
	 * Creates an HTML report detailing the internal references found
	 * 
	 * @param outputFile the directory to write to
	 * @throws IOException if a write error occurs.
	 */
	public void writeInternalReferencesAsHTML (Map pluginNamesToInternalReferences, Map pluginNamesToPlugins) {
		PrintWriter index;
		try {
			index =
				new PrintWriter(
					new BufferedWriter(
						new FileWriter(new File(fOutputDir, "index.html"))));		
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		openHtml(index, "Internal Type References");
		openList(index, "Plugins with types that reference other plugin's internals:");
		for (Iterator i = pluginNamesToInternalReferences.keySet().iterator(); i.hasNext();) {
			String plugin = (String) i.next();
			Map typeNamesToInternalReferences= (Map)pluginNamesToInternalReferences.get(plugin);
			writePlugin (index, plugin, typeNamesToInternalReferences, pluginNamesToPlugins);
		}
		closeList(index);
		closeHtml(index);
	}
	
	/*
	 * Creates a heading for an html file
	 * @param writer destination
	 * @param title the page title
	 */
	private void openHtml(PrintWriter writer, String title)  {
		writer.println("<HTML>");
		writer.println("  <HEAD><TITLE>");
		writer.println(title);
		writer.println("</TITLE></HEAD>");
		writer.println("<BODY>");
	}

	/*
	 * Starts a list in HTML
	 * @param writer destination
	 * @param description a title for the list
	 */
	private void openList(PrintWriter writer, String description) {
		writer.println(description);
		writer.println("  <UL>");
	}
	
	/*
	 * Creates the necessary tags to close an HTML file.
	 * @param writer destination
	 */
	private void closeHtml(PrintWriter writer) {
		writer.println("</BODY>");
		writer.close();
	}

	/*
	 * Closes an HTML list.
	 * @param writer destination
	 */
	private void closeList(PrintWriter writer) {
		writer.println("  </UL>");
	}
	
	/*
	 * Writes the list of internal references in a type 
	 * @param writer	destination
	 * @param typeName	the name of the type
	 * @param internalReferences	the list of internal references in the type
	 */
	private void writeType (PrintWriter writer, String typeName, Set internalReferences) {
		int referencedClassesCount= internalReferences.size();
		writer.print("<LI><B>");
		writer.print(typeName);
		writer.print("</B> (");
		writer.print(referencedClassesCount);
		writer.print((referencedClassesCount == 1) ? " reference" : " references");
		writer.println(")<BR>");
		writer.println("References:");
		writer.println("<UL>");
		for (Iterator i = internalReferences.iterator(); i.hasNext();) {
			InternalTypeReference reference = (InternalTypeReference)i.next();
			writer.print("<LI>");
			writer.print(reference.getTypeName());
			writer.print(" (from <B>");
			writer.print(reference.getPluginName());
			writer.println("</B>)</LI>");
		}
		writer.println("</UL></LI>");
	}

	/*
	 * Writes the list of types with internal references in the plugin
	 * @param indexWriter the writer on the index.html file
	 * @param plugin	the plugin to write
	 * @param typeNamesToInternalReferences a map of type names to internal references in this plugin
	 * @param pluginNamesToPlugins a map of plugin names to plugin objects
	 */
	private void writePlugin (PrintWriter indexWriter, String plugin, Map typeNamesToInternalReferences, Map pluginNamesToPlugins) {
		String pluginHtmlFile= plugin + ".html";
		PrintWriter writer= null;
		try {
			writer =
				new PrintWriter(
					new BufferedWriter(
						new FileWriter(new File(fOutputDir, pluginHtmlFile))));
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		openHtml(writer, "Internal references in " + plugin);

		Set typeNames= typeNamesToInternalReferences.keySet();
		int typeCount= typeNames.size();
		indexWriter.print("<LI><B><A HREF=\"");
		indexWriter.print(pluginHtmlFile);
		indexWriter.print("\">");
		indexWriter.print(plugin);
		indexWriter.print("</A></B> (");
		indexWriter.print(typeCount);
		indexWriter.print((typeCount == 1) ? " type" : " types");
		indexWriter.println(")</LI>");
		
		Set referencedPlugins= getReferencedPlugins(typeNamesToInternalReferences, pluginNamesToPlugins);
		openList(writer, "<B>" + plugin + "</B> contains references to internal types in these plugins:");
		for (Iterator i = referencedPlugins.iterator(); i.hasNext();) {
			String pluginName = (String) i.next();
			writer.print("<LI><B>");
			writer.print(pluginName);
			writer.println("</B></LI>");
		}
		closeList(writer);
		
		openList(writer, "Types in <B>"+ plugin +"</B> with internal references to other plugins.");
		for (Iterator i = typeNames.iterator(); i.hasNext();) {
			String typeName = (String) i.next();
			Set internalReferences = (Set)typeNamesToInternalReferences.get(typeName);
			writeType (writer, typeName, internalReferences);
		}
		closeList(writer);
		closeHtml(writer);
	}

	/**
	 * Answers the set of plugins which are referenced by other the map
	 * of internal references.
	 * 
	 * @param typeNamesToInternalReferences map of type name to collection of internal references
	 * @param pluginNamesToPlugins map of plugin names to plugin objects
	 * @return Set plugins which are referenced or an empty set, not <code>null</code>.
	 */
	private Set getReferencedPlugins(Map typeNamesToInternalReferences, Map pluginNamesToPlugins) {
		Set referencedPlugins= new TreeSet();
		for (Iterator i = typeNamesToInternalReferences.values().iterator(); i.hasNext();) {
			Collection referencedTypes = (Collection) i.next();
			for (Iterator j = referencedTypes.iterator(); j.hasNext();) {
				InternalTypeReference typeReference= (InternalTypeReference) j.next();
				referencedPlugins.add(typeReference.getPluginName());
			}
		}
		return referencedPlugins;
	}
}
