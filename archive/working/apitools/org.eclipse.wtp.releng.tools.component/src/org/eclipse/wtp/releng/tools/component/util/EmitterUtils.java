/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.util;

import java.io.File;
import java.util.Iterator;
import java.util.Map;
import org.eclipse.wtp.releng.tools.component.Component;
import org.eclipse.wtp.releng.tools.component.internalreference.Bundle;
import org.eclipse.wtp.releng.tools.component.internalreference.ConfigurationFileParser;
import org.eclipse.wtp.releng.tools.component.internalreference.Fragment;
import org.eclipse.wtp.releng.tools.component.internalreference.Plugin;
import org.eclipse.wtp.releng.tools.component.location.FileLocation;
import org.eclipse.wtp.releng.tools.component.model.ComponentType;

public class EmitterUtils
{
  private EmitterUtils()
  {
  }

  public static void harvestComponents(File file, String compApiLoc, Map compName2Comp)
  {
    harvestComponents(file, compApiLoc, file.getAbsolutePath() + '/', compName2Comp);
  }

  private static void harvestComponents(File file, String compApiLoc, String baseLoc, Map compName2Comp)
  {
    if (file.isDirectory())
    {
      File[] files = file.listFiles();
      for (int i = 0; i < files.length; i++)
        harvestComponents(files[i], compApiLoc, baseLoc, compName2Comp);
    }
    else if (Component.CONST_COMPONENT_XML.equalsIgnoreCase(file.getName()))
    {
      String path = file.getAbsolutePath();
      Component comp = new Component();
      String compLoc = path.substring(0, path.length() - Component.CONST_COMPONENT_XML.length());
      comp.setCompLoc(compLoc);
      comp.setCompApiLoc(compApiLoc + compLoc.substring(baseLoc.length(), compLoc.length()));
      ComponentType compType = comp.getCompXML();
      if (compType != null)
        compName2Comp.put(compType.getName(), comp);
    }
  }
  
  public static void harvestPlugins(File file, Map pluginId2Plugin, Map fragmentId2Fragment)
  {
    if (file.isDirectory())
    {
      File[] files = file.listFiles();
      for (int i = 0; i < files.length; i++)
        harvestPlugins(files[i], pluginId2Plugin, fragmentId2Fragment);
    }
    else if (Plugin.CONST_PLUGIN_XML.equalsIgnoreCase(file.getName()))
    {
      Plugin plugin = ConfigurationFileParser.getPlugin(new FileLocation(new FileLocation(file.getParentFile()), file.getName()));
      String pluginName = plugin.getName();
      if (pluginName != null && !pluginId2Plugin.containsKey(pluginName))
        pluginId2Plugin.put(pluginName, plugin);
    }
    else if (Fragment.CONST_FRAGMENT_XML.equalsIgnoreCase(file.getName()))
    {
      Fragment fragment = ConfigurationFileParser.getFragment(new FileLocation(new FileLocation(file.getParentFile()), file.getName()));
      String fragmentName = fragment.getFragmentName();
      if (fragmentName != null)
        fragmentId2Fragment.put(fragmentName, fragment);
    }
    else if (Bundle.CONST_MANIFEST_MF.equalsIgnoreCase(file.getName()))
    {
      Plugin plugin = ConfigurationFileParser.getBundle(new FileLocation(new FileLocation(file.getParentFile()), file.getName()));
      String pluginName = plugin.getName();
      if (pluginName != null)
        pluginId2Plugin.put(pluginName, plugin);
    }
  }

  public static void linkPluginsAndFragments(Map pluginId2Plugin, Map fragmentId2Fragment)
  {
    for (Iterator it = fragmentId2Fragment.values().iterator(); it.hasNext();)
    {
      Fragment fragment = (Fragment)it.next();
      fragment.link(pluginId2Plugin);
    }
  }

  public static String addTrailingSeperator(String s)
  {
    if (s != null && !s.endsWith("/") && !s.endsWith("\\"))
    {
      StringBuffer sb = new StringBuffer(s);
      sb.append('/');
      return sb.toString();
    }
    else
      return s;
  }
}