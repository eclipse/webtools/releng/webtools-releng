/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internalreference;

import java.util.Comparator;

/**
 * A <code>InternalReferenceComparator</code> compares
 * <code>InternalTypeReference</code> objects based off of the
 * plugin name and the name of the referenced type.
 */
public class InternalReferenceComparator implements Comparator {

	/*
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Object o1, Object o2) {
		InternalTypeReference i1= (InternalTypeReference)o1;
		InternalTypeReference i2= (InternalTypeReference)o2;
		int compare= i1.getPluginName().compareTo(i2.getPluginName());
		if (compare != 0) {
			return compare;
		}
		return i1.getTypeName().compareTo(i1.getTypeName());
	}

}
