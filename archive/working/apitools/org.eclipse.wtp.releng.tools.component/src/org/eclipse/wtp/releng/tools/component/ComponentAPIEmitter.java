/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/
package org.eclipse.wtp.releng.tools.component;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import org.eclipse.wtp.releng.tools.component.internalreference.Library;
import org.eclipse.wtp.releng.tools.component.internalreference.Plugin;
import org.eclipse.wtp.releng.tools.component.internalreference.Type;
import org.eclipse.wtp.releng.tools.component.use.ComponentUseType;
import org.eclipse.wtp.releng.tools.component.use.SourceClass;
import org.eclipse.wtp.releng.tools.component.use.UseFactory;
import org.eclipse.wtp.releng.tools.component.use.UsePackage;
import org.eclipse.wtp.releng.tools.component.use.util.UseResourceFactoryImpl;
import org.eclipse.wtp.releng.tools.component.use.util.UseResourceImpl;
import org.eclipse.wtp.releng.tools.component.util.CommandOptionParser;
import org.eclipse.wtp.releng.tools.component.util.EmitterUtils;
import org.eclipse.wtp.releng.tools.component.model.ComponentDependsType;
import org.eclipse.wtp.releng.tools.component.model.ComponentRefType;
import org.eclipse.wtp.releng.tools.component.model.ComponentType;
import org.eclipse.wtp.releng.tools.component.model.PackageType;
import org.eclipse.wtp.releng.tools.component.model.PluginType;
import org.eclipse.wtp.releng.tools.component.model.TypeType;
import org.eclipse.wtp.releng.tools.component.api.ApiFactory;
import org.eclipse.wtp.releng.tools.component.api.ApiPackage;
import org.eclipse.wtp.releng.tools.component.api.ApiTypes;
import org.eclipse.wtp.releng.tools.component.api.ClassApi;
import org.eclipse.wtp.releng.tools.component.api.ComponentApiType;
import org.eclipse.wtp.releng.tools.component.api.FieldApi;
import org.eclipse.wtp.releng.tools.component.api.MethodApi;
import org.eclipse.wtp.releng.tools.component.api.Package;
import org.eclipse.wtp.releng.tools.component.api.Visibility;
import org.eclipse.wtp.releng.tools.component.api.util.ApiResourceFactoryImpl;
import org.eclipse.wtp.releng.tools.component.api.util.ApiResourceImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.core.util.IConstantPoolConstant;
import org.eclipse.jdt.core.util.IConstantPoolEntry;
import org.eclipse.jdt.core.util.IExceptionAttribute;
import org.eclipse.jdt.core.util.IFieldInfo;
import org.eclipse.jdt.core.util.IMethodInfo;
import org.eclipse.jdt.core.util.IModifierConstants;

public class ComponentAPIEmitter
{
  public static final String OPTION_ECLIPSE_DIR = "eclipseDir";
  public static final String OPTION_COMPONENT_DIR = "compDir";
  public static final String OPTION_COMPONENT_API_DIR = "compAPIDir";
  public static final String OPTION_INCLUDE = "include";
  public static final String OPTION_EXCLUDE = "exclude";

  private String compDirLoc;
  private Map compName2Comp;
  private Map pluginId2Plugin;
  private Map fragmentId2Fragment;
  private List classUseIncludes;
  private List classUseExcludes;

  public ComponentAPIEmitter(List eclipseDirLocs, String compDirLoc, String compApiDirLoc)
  {
    this.compDirLoc = EmitterUtils.addTrailingSeperator(compDirLoc);
    compName2Comp = new HashMap();
    pluginId2Plugin = new HashMap();
    fragmentId2Fragment = new HashMap();
    File compDir = new File(this.compDirLoc);
    if (compDir.exists() && compDir.isDirectory())
      EmitterUtils.harvestComponents(compDir, EmitterUtils.addTrailingSeperator(compApiDirLoc), compName2Comp);
    for (Iterator it = eclipseDirLocs.iterator(); it.hasNext();)
    {
      File eclipseDir = new File(EmitterUtils.addTrailingSeperator((String)it.next()));
      if (eclipseDir.exists() && eclipseDir.isDirectory())
        EmitterUtils.harvestPlugins(eclipseDir, pluginId2Plugin, fragmentId2Fragment);
    }
    EmitterUtils.linkPluginsAndFragments(pluginId2Plugin, fragmentId2Fragment);
  }

  public List getClassUseIncludes()
  {
    return classUseIncludes;
  }

  public void setClassUseIncludes(List includes)
  {
    this.classUseIncludes = includes;
  }

  public List getClassUseExcludes()
  {
    return classUseExcludes;
  }

  public void setClassUseExcludes(List excludes)
  {
    this.classUseExcludes = excludes;
  }

  public void genComponentApiXml() throws IOException
  {
    for (Iterator it = compName2Comp.keySet().iterator(); it.hasNext();)
      genComponentApiXml((String)it.next());
  }

  public void genComponentApiXml(String compName) throws IOException
  {
    Component comp = (Component)compName2Comp.get(compName);
    if (comp != null)
    {
      ComponentApiType compApi = newComponentApiType();
      ComponentType compType = comp.getCompXML();
      EList plugins = compType.getPlugin();
      for (Iterator pluginsIt = plugins.iterator(); pluginsIt.hasNext();)
      {
        PluginType pluginType = (PluginType)pluginsIt.next();
        Plugin plugin = (Plugin)pluginId2Plugin.get(pluginType.getId());
        if (plugin != null)
        {
          List libs = plugin.getLibraries();
          for (Iterator libsIt = libs.iterator(); libsIt.hasNext();)
          {
            Library lib = (Library)libsIt.next();
            Map types = lib.getTypes();
            for (Iterator typesIt = types.keySet().iterator(); typesIt.hasNext();)
            {
              String typeName = (String)typesIt.next();
              Type type = (Type)types.get(typeName);
              ClassApi classApi = addClassApi(compType, type, compApi);
              if (classApi != null)
              {
                IMethodInfo[] methodInfos = type.getMethodInfo();
                for (int i = 0; i < methodInfos.length; i++)
                {
                  String methodName = new String(methodInfos[i].getName());
                  int accessFlag = methodInfos[i].getAccessFlags();
                  Boolean visibility;
                  if (isBit(accessFlag, IModifierConstants.ACC_PUBLIC))
                    visibility = Boolean.TRUE;
                  else if (isBit(accessFlag, IModifierConstants.ACC_PRIVATE))
                    visibility = null;
                  else
                    visibility = Boolean.FALSE;
                  if (visibility != null)
                  {
                    Boolean isStatic = isBit(accessFlag, IModifierConstants.ACC_STATIC) ? Boolean.TRUE : null;
                    Boolean isFinal = isBit(accessFlag, IModifierConstants.ACC_FINAL) ? Boolean.TRUE : null;
                    Boolean isSynchronized = isBit(accessFlag, IModifierConstants.ACC_SYNCHRONIZED) ? Boolean.TRUE : null;
                    Boolean isNative = isBit(accessFlag, IModifierConstants.ACC_NATIVE) ? Boolean.TRUE : null;
                    Boolean isAbstract = isBit(accessFlag, IModifierConstants.ACC_ABSTRACT) ? Boolean.TRUE : null;
                    Boolean isStrict = isBit(accessFlag, IModifierConstants.ACC_STRICT) ? Boolean.TRUE : null;
                    char[][] paramChars = Signature.getParameterTypes(methodInfos[i].getDescriptor());
                    String[] params = new String[paramChars.length];
                    for (int j = 0; j < params.length; j++)
                      params[j] = descriptor2Signature(paramChars[j]);
                    String returnType = descriptor2Signature(Signature.getReturnType(methodInfos[i].getDescriptor()));
                    IExceptionAttribute exAttr = methodInfos[i].getExceptionAttribute();
                    String[] exs = new String[0];
                    if (exAttr != null)
                    {
                      char[][] exChars = exAttr.getExceptionNames();
                      exs = new String[exChars.length];
                      for (int j = 0; j < exs.length; j++)
                        exs[j] = toClassName(new String(exChars[j]));
                    }
                    addMethodApi(classApi, newMethodApi(methodName, visibility.booleanValue(), isStatic, isFinal, isSynchronized, isNative, isAbstract, isStrict, params, returnType, exs));
                  }
                }
                IFieldInfo[] fieldInfos = type.getFieldInfo();
                for (int i = 0; i < fieldInfos.length; i++)
                {
                  int accessFlag = fieldInfos[i].getAccessFlags();
                  Boolean visibility;
                  if (isBit(accessFlag, IModifierConstants.ACC_PUBLIC))
                    visibility = Boolean.TRUE;
                  else if (isBit(accessFlag, IModifierConstants.ACC_PRIVATE))
                    visibility = null;
                  else
                    visibility = Boolean.FALSE;
                  if (visibility != null)
                  {
                    String fieldName = new String(fieldInfos[i].getName());
                    Boolean isStatic = isBit(accessFlag, IModifierConstants.ACC_STATIC) ? Boolean.TRUE : null;
                    Boolean isFinal = isBit(accessFlag, IModifierConstants.ACC_FINAL) ? Boolean.TRUE : null;
                    Boolean isVolatile = isBit(accessFlag, IModifierConstants.ACC_VOLATILE) ? Boolean.TRUE : null;
                    Boolean isTransient = isBit(accessFlag, IModifierConstants.ACC_TRANSIENT) ? Boolean.TRUE : null;
                    String fieldType = descriptor2Signature(fieldInfos[i].getDescriptor());
                    addFieldApi(classApi, newFieldApi(fieldName, visibility.booleanValue(), isStatic, isFinal, isVolatile, isTransient, fieldType));
                  }
                }
              }
              type.reset();
            }
            lib.reset();
          }
        }
      }
      System.out.println("Writing component-api.xml for " + compName);
      saveComponentApi(compApi, new File(comp.getCompApiLoc() + Component.CONST_COMPONENT_API_XML));
      // comp.setCompApiXml(compApi);
    }
  }

  public void genComponentUseXML() throws IOException
  {
    for (Iterator it = compName2Comp.keySet().iterator(); it.hasNext();)
      genComponentUseXML((String)it.next());
  }

  public void genComponentUseXML(String compName) throws IOException
  {
    Component comp = (Component)compName2Comp.get(compName);
    if (comp != null)
    {
      ComponentUseType compUse = newComponentUseType();
      ComponentType compType = comp.getCompXML();
      EList plugins = compType.getPlugin();
      for (Iterator pluginsIt = plugins.iterator(); pluginsIt.hasNext();)
      {
        PluginType pluginType = (PluginType)pluginsIt.next();
        Plugin plugin = (Plugin)pluginId2Plugin.get(pluginType.getId());
        if (plugin != null)
        {
          List libs = plugin.getLibraries();
          for (Iterator libsIt = libs.iterator(); libsIt.hasNext();)
          {
            Library lib = (Library)libsIt.next();
            Map types = lib.getTypes();
            for (Iterator typesIt = types.keySet().iterator(); typesIt.hasNext();)
            {
              String typeName = (String)typesIt.next();
              Type type = (Type)types.get(typeName);
              SourceClass sourceClass = newSourceClass(typeName);
              addSourceClass(compUse, sourceClass);
              // method references
              IConstantPoolEntry[] methodRefs = type.getConstantPoolEntries(IConstantPoolConstant.CONSTANT_Methodref);
              IConstantPoolEntry[] intMethodRefs = type.getConstantPoolEntries(IConstantPoolConstant.CONSTANT_InterfaceMethodref);
              IConstantPoolEntry[] allMethodRefs = new IConstantPoolEntry[methodRefs.length + intMethodRefs.length];
              System.arraycopy(methodRefs, 0, allMethodRefs, 0, methodRefs.length);
              System.arraycopy(intMethodRefs, 0, allMethodRefs, methodRefs.length, intMethodRefs.length);
              for (int i = 0; i < allMethodRefs.length; i++)
              {
                String className = toClassName(new String(allMethodRefs[i].getClassName()));
                if (isReportClassApi(typeName, className))
                {
                  String methodName = new String(allMethodRefs[i].getMethodName());
                  if (isConstructor(methodName))
                    addUniqueClassApi(sourceClass, className, null, null, null, Boolean.TRUE);
                  else
                  {
                    char[] methodDescriptor = allMethodRefs[i].getMethodDescriptor();
                    char[][] paramChars = Signature.getParameterTypes(methodDescriptor);
                    String[] params = new String[paramChars.length];
                    for (int j = 0; j < params.length; j++)
                      params[j] = descriptor2Signature(paramChars[j]);
                    String returnType = descriptor2Signature(Signature.getReturnType(methodDescriptor));
                     // String containingClass = getContainingClass(compType, type, methodName, methodDescriptor);
                     // if (containingClass == null)
                     //   // TODO: Flag this as a violation because this type does not contain this method
                     //   containingClass = className;
                    String containingClass = className;
                    ClassApi classApi = addUniqueClassApi(sourceClass, containingClass, Boolean.TRUE, null, null, null);
                    MethodApi methodApi = newMethodApi(methodName, params, returnType);
                    classApi.getMethodApi().add(methodApi);
                  }
                }
              }
              // field references
              IConstantPoolEntry[] fieldRefs = type.getConstantPoolEntries(IConstantPoolConstant.CONSTANT_Fieldref);
              for (int i = 0; i < fieldRefs.length; i++)
              {
                String className = toClassName(new String(fieldRefs[i].getClassName()));
                if (isReportClassApi(typeName, className))
                {
                  String fieldType = descriptor2Signature(fieldRefs[i].getFieldDescriptor());
                  String fieldName = new String(fieldRefs[i].getFieldName());
                  ClassApi classApi = addUniqueClassApi(sourceClass, className, Boolean.TRUE, null, null, null);
                  FieldApi fieldApi = newFieldApi(fieldName, fieldType);
                  classApi.getFieldApi().add(fieldApi);
                }
              }
              // use: reference
              Set refTypes = type.getReferencedTypes();
              for (Iterator refTypesIt = refTypes.iterator(); refTypesIt.hasNext();)
              {
                String className = (String)refTypesIt.next();
                if (isReportClassApi(typeName, className))
                  addUniqueClassApi(sourceClass, className, Boolean.TRUE, null, null, null);
              }
              // use: subclass
              if (!type.isInterface())
              {
                String superClass = type.getSuperClass();
                if (superClass != null && isReportClassApi(typeName, superClass))
                  addUniqueClassApi(sourceClass, superClass, null, Boolean.TRUE, null, null);
              }
              // use: implement
              String[] interfaces = type.getInterfaces();
              for (int i = 0; i < interfaces.length; i++)
                addUniqueClassApi(sourceClass, interfaces[i], null, null, Boolean.TRUE, null);
              // use: instantiate
              // reset type
              type.reset();
            }
            // reset lib
            lib.reset();
          }
        }
      }
      System.out.println("Writing component-use.xml for " + compName);
      saveComponentUse(compUse, new File(comp.getCompApiLoc() + Component.CONST_COMPONENT_USE_XML));
      // comp.setCompUseXML(compUse);
    }
  }

  private boolean isReportClassApi(String sourceClassName, String classUseName)
  {
    if (sourceClassName != null && sourceClassName.equals(classUseName))
      return false;
    if (classUseExcludes != null)
      for (Iterator it = classUseExcludes.iterator(); it.hasNext();)
        if (classUseName.startsWith((String)it.next()))
          return false;
    if (classUseIncludes != null && classUseIncludes.size() > 0)
    {
      for (Iterator it = classUseIncludes.iterator(); it.hasNext();)
        if (classUseName.startsWith((String)it.next()))
          return true;
      return false;
    }
    return true;
  }

  private boolean isConstructor(String methodName)
  {
    return methodName.equals("<init>");
  }

  private String getContainingClass(ComponentType compType, Type type, String methodName, char[] methodDescriptor)
  {
    IMethodInfo[] methodInfos = type.getMethodInfo();
    for (int i = 0; i < methodInfos.length; i++)
      if (methodName.equals(methodInfos[i].getName()) && methodDescriptor.equals(new String(methodInfos[i].getDescriptor())))
        return type.getName();
    String superClass = type.getSuperClass();
    List compRefs = new Vector();
    ComponentDependsType depends = compType.getComponentDepends();
    if (depends != null)
    {
      EList compRefTypes = depends.getComponentRef();
      for (Iterator it = compRefTypes.iterator(); it.hasNext();)
      {
        ComponentRefType compRefType = (ComponentRefType)it.next();
        Component comp = (Component)compName2Comp.get(compRefType.getName());
        if (comp != null)
          compRefs.add(comp);
      }
    }
    for (Iterator compRefsIt = compRefs.iterator(); compRefsIt.hasNext();)
    {
      Component compRef = (Component)compRefsIt.next();
      ComponentType compRefType = compRef.getCompXML();
      if (compRefType != null)
      {
        EList plugins = compRefType.getPlugin();
        for (Iterator pluginsIt = plugins.iterator(); pluginsIt.hasNext();)
        {
          PluginType pluginType = (PluginType)pluginsIt.next();
          Plugin plugin = (Plugin)pluginId2Plugin.get(pluginType.getId());
          if (plugin != null)
          {
            List libs = plugin.getLibraries();
            for (Iterator libsIt = libs.iterator(); libsIt.hasNext();)
            {
              Library lib = (Library)libsIt.next();
              Map types = lib.getTypes();
              Type refType = (Type)types.get(superClass);
              if (refType != null)
              {
                String containingClass = getContainingClass(compType, refType, methodName, methodDescriptor);
                // reset type
                refType.reset();
                // reset lib
                lib.reset();
                return containingClass;
              }
              // reset lib
              lib.reset();
            }
          }
        }
      }
    }
    return null;
  }

  private ClassApi addClassApi(ComponentType comp, Type type, ComponentApiType compApi)
  {
    String typeName = type.getName();
    if (isReportClassApi(null, typeName))
    {
      int index = typeName.lastIndexOf('.');
      String pkgName;
      String localName;
      if (index != -1)
      {
        pkgName = typeName.substring(0, index);
        localName = typeName.substring(index + 1);
      }
      else
      {
        pkgName = "";
        localName = typeName;
      }
      ClassApi internalClassApi = newClassApi(typeName, null, null, null, null);
      addClassApi(compApi, true, pkgName, internalClassApi);
      if (!isBit(type.getAccessFlags(), IModifierConstants.ACC_PRIVATE))
      {
        boolean isInterface = type.isInterface();
        PackageType pkgType = findPackageType(comp, pkgName);
        if (pkgType != null)
        {
          TypeType typeType = findTypeType(pkgType, localName);
          if (typeType != null)
          {
            boolean ref = typeType.isReference();
            boolean subclass = typeType.isSubclass() && !isInterface;
            boolean implement = typeType.isImplement() && isInterface;
            boolean instantiate = typeType.isInstantiate() && !isInterface;
            if (ref || subclass || implement || instantiate)
            {
              // at least one public usage
              ClassApi externalClassApi = newClassApi(typeName, new Boolean(ref), new Boolean(subclass), new Boolean(implement), new Boolean(instantiate));
              addClassApi(compApi, false, pkgName, externalClassApi);
              return externalClassApi;
            }
            else
            {
              // all usage are set to false, in another word, it is an internal
              // API
              return null;
            }
          }
          else if (pkgType.isApi())
          {
            // api == true means, by default, a non-listed type is an external
            // API
            ClassApi externalClassApi = newClassApi(typeName, Boolean.TRUE, new Boolean(!isInterface), new Boolean(isInterface), new Boolean(!isInterface));
            addClassApi(compApi, false, pkgName, externalClassApi);
            return externalClassApi;
          }
          else
          {
            // api == false means, by default, a non-listed type is an internal
            // API
            return null;
          }
        }
        else
        {
          // package not defined in component.xml means it is an internal API
          return null;
        }
      }
    }
    return null;
  }

  private void addClassApi(ComponentApiType comp, boolean isInternal, String pkgName, ClassApi classApi)
  {
    ApiTypes apis = (isInternal) ? comp.getInternalApis() : comp.getExternalApis();
    EList pkgs = apis.getPackage();
    Package pkg = null;
    for (Iterator it = pkgs.iterator(); it.hasNext();)
    {
      Package nextPkg = (Package)it.next();
      if (nextPkg.getName().equals(pkgName))
      {
        pkg = nextPkg;
        break;
      }
    }
    if (pkg == null)
    {
      pkg = newPackage(pkgName);
      pkgs.add(pkg);
    }
    pkg.getClassApi().add(classApi);
  }

  private void addMethodApi(ClassApi classApi, MethodApi methodApi)
  {
    classApi.getMethodApi().add(methodApi);
  }

  private void addFieldApi(ClassApi classApi, FieldApi fieldApi)
  {
    classApi.getFieldApi().add(fieldApi);
  }

  private void addSourceClass(ComponentUseType compUse, SourceClass sourceClass)
  {
    compUse.getSourceClass().add(sourceClass);
  }

  private ClassApi addUniqueClassApi(SourceClass sourceClass, String className, Boolean ref, Boolean subclass, Boolean implement, Boolean instantiate)
  {
    EList classApis = sourceClass.getClassUse();
    for (Iterator it = classApis.iterator(); it.hasNext();)
    {
      ClassApi classApi = (ClassApi)it.next();
      if (!classApi.getName().equals(className))
        continue;
      if (ref != null && (ref.booleanValue() != classApi.isReference()))
        continue;
      if (subclass != null && (subclass.booleanValue() != classApi.isSubclass()))
        continue;
      if (implement != null && (implement.booleanValue() != classApi.isImplement()))
        continue;
      if (instantiate != null && (instantiate.booleanValue() != classApi.isInstantiate()))
        continue;
      return classApi;
    }
    ClassApi classApi = newClassApi(className, ref, subclass, implement, instantiate);
    classApis.add(classApi);
    return classApi;
  }

  private ComponentApiType newComponentApiType()
  {
    ComponentApiType comp = ApiFactory.eINSTANCE.createComponentApiType();
    ApiTypes internalApis = ApiFactory.eINSTANCE.createApiTypes();
    ApiTypes externalApis = ApiFactory.eINSTANCE.createApiTypes();
    comp.setInternalApis(internalApis);
    comp.setExternalApis(externalApis);
    return comp;
  }

  private Package newPackage(String pkgName)
  {
    Package pkg = ApiFactory.eINSTANCE.createPackage();
    pkg.setName(pkgName);
    return pkg;
  }

  private ClassApi newClassApi(String typeName, Boolean ref, Boolean subclass, Boolean implement, Boolean instantiate)
  {
    ClassApi classApi = ApiFactory.eINSTANCE.createClassApi();
    classApi.setName(typeName);
    if (ref != null)
      classApi.setReference(ref.booleanValue());
    if (subclass != null)
      classApi.setSubclass(subclass.booleanValue());
    if (implement != null)
      classApi.setImplement(implement.booleanValue());
    if (instantiate != null)
      classApi.setInstantiate(instantiate.booleanValue());
    return classApi;
  }

  private MethodApi newMethodApi(String methodName, boolean isPublic, Boolean isStatic, Boolean isFinal, Boolean isSynchronized, Boolean isNative, Boolean isAbstract, Boolean isStrict, String[] params, String returnType, String[] exceptions)
  {
    MethodApi methodApi = ApiFactory.eINSTANCE.createMethodApi();
    methodApi.setName(methodName);
    if (isPublic)
      methodApi.setVisibility(Visibility.PUBLIC_LITERAL);
    else
      methodApi.setVisibility(Visibility.PROTECTED_LITERAL);
    if (isStatic != null)
      methodApi.setStatic(isStatic.booleanValue());
    if (isFinal != null)
      methodApi.setFinal(isFinal.booleanValue());
    if (isSynchronized != null)
      methodApi.setSynchronized(isSynchronized.booleanValue());
    if (isNative != null)
      methodApi.setNative(isNative.booleanValue());
    if (isAbstract != null)
      methodApi.setAbstract(isAbstract.booleanValue());
    if (isStrict != null)
      methodApi.setStrict(isStrict.booleanValue());
    List inputType = new Vector();
    for (int i = 0; i < params.length; i++)
      inputType.add(params[i]);
    methodApi.setInputType(inputType);
    methodApi.setReturnType(returnType);
    List exList = new Vector();
    for (int i = 0; i < exceptions.length; i++)
      exList.add(exceptions[i]);
    methodApi.setExceptionType(exList);
    return methodApi;
  }

  private MethodApi newMethodApi(String methodName, String[] params, String returnType)
  {
    MethodApi methodApi = ApiFactory.eINSTANCE.createMethodApi();
    methodApi.setName(methodName);
    List inputType = new Vector();
    for (int i = 0; i < params.length; i++)
      inputType.add(params[i]);
    methodApi.setInputType(inputType);
    methodApi.setReturnType(returnType);
    return methodApi;
  }

  private FieldApi newFieldApi(String fieldName, boolean isPublic, Boolean isStatic, Boolean isFinal, Boolean isVolatile, Boolean isTransient, String type)
  {
    FieldApi fieldApi = ApiFactory.eINSTANCE.createFieldApi();
    fieldApi.setName(fieldName);
    if (isPublic)
      fieldApi.setVisibility(Visibility.PUBLIC_LITERAL);
    else
      fieldApi.setVisibility(Visibility.PROTECTED_LITERAL);
    if (isStatic != null)
      fieldApi.setStatic(isStatic.booleanValue());
    if (isFinal != null)
      fieldApi.setFinal(isFinal.booleanValue());
    if (isVolatile != null)
      fieldApi.setVolatile(isVolatile.booleanValue());
    if (isTransient != null)
      fieldApi.setTransient(isTransient.booleanValue());
    fieldApi.setType(type);
    return fieldApi;
  }

  private FieldApi newFieldApi(String fieldName, String type)
  {
    FieldApi fieldApi = ApiFactory.eINSTANCE.createFieldApi();
    fieldApi.setName(fieldName);
    fieldApi.setType(type);
    return fieldApi;
  }

  private ComponentUseType newComponentUseType()
  {
    ComponentUseType compUse = UseFactory.eINSTANCE.createComponentUseType();
    return compUse;
  }

  private SourceClass newSourceClass(String className)
  {
    SourceClass source = UseFactory.eINSTANCE.createSourceClass();
    source.setName(className);
    return source;
  }

  private PackageType findPackageType(ComponentType comp, String pkgName)
  {
    if (pkgName != null)
    {
      EList pkgs = comp.getPackage();
      for (Iterator it = pkgs.iterator(); it.hasNext();)
      {
        PackageType pkg = (PackageType)it.next();
        if (pkgName.equals(pkg.getName()))
          return pkg;
      }
    }
    return null;
  }

  private TypeType findTypeType(PackageType pkg, String typeName)
  {
    if (typeName != null)
    {
      EList types = pkg.getType();
      for (Iterator it = types.iterator(); it.hasNext();)
      {
        TypeType type = (TypeType)it.next();
        if (typeName.equals(type.getName()))
          return type;
      }
    }
    return null;
  }

  private boolean isBit(int flag, int bit)
  {
    return ((flag & bit) == bit);
  }

  private String descriptor2Signature(char[] descriptor)
  {
    return toClassName(Signature.toString(new String(descriptor)));
  }

  private String toClassName(String className)
  {
    return className.replace('/', '.');
  }

  private void saveComponentApi(ComponentApiType componentApi, File file) throws IOException
  {
    file.getParentFile().mkdirs();
    ResourceSet res = new ResourceSetImpl();
    res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentapi", new ApiResourceFactoryImpl());
    res.getPackageRegistry().put(ApiPackage.eNS_URI, ApiPackage.eINSTANCE);
    ApiResourceImpl apiRes = (ApiResourceImpl)res.createResource(URI.createURI("*.componentapi"));
    org.eclipse.wtp.releng.tools.component.api.DocumentRoot root = ApiFactory.eINSTANCE.createDocumentRoot();
    root.setComponentApi(componentApi);
    apiRes.getContents().add(root);
    apiRes.save(new BufferedOutputStream(new FileOutputStream(file)), new HashMap());
  }

  private void saveComponentUse(ComponentUseType componentUse, File file) throws IOException
  {
    file.getParentFile().mkdirs();
    ResourceSet res = new ResourceSetImpl();
    res.getResourceFactoryRegistry().getExtensionToFactoryMap().put("componentuse", new UseResourceFactoryImpl());
    res.getPackageRegistry().put(UsePackage.eNS_URI, UsePackage.eINSTANCE);
    UseResourceImpl useRes = (UseResourceImpl)res.createResource(URI.createURI("*.componentuse"));
    org.eclipse.wtp.releng.tools.component.use.DocumentRoot root = UseFactory.eINSTANCE.createDocumentRoot();
    root.setComponentUse(componentUse);
    useRes.getContents().add(root);
    useRes.save(new BufferedOutputStream(new FileOutputStream(file)), new HashMap());
  }

  public Map getComponents()
  {
    return compName2Comp;
  }

  public static void main(String[] args)
  {
    CommandOptionParser optionParser = new CommandOptionParser(args);
    Map options = optionParser.getOptions();
    List eclipseDir = (List)options.get(ComponentAPIEmitter.OPTION_ECLIPSE_DIR);
    List compDir = (List)options.get(ComponentAPIEmitter.OPTION_COMPONENT_DIR);
    List compAPIDir = (List)options.get(ComponentAPIEmitter.OPTION_COMPONENT_API_DIR);
    List includes = (List)options.get(ComponentAPIEmitter.OPTION_INCLUDE);
    List excludes = (List)options.get(ComponentAPIEmitter.OPTION_EXCLUDE);
    if (eclipseDir == null || compDir == null || compAPIDir == null || eclipseDir.size() < 1 || compDir.size() < 1 || compAPIDir.size() < 1)
    {
      printUsage();
      System.exit(-1);
    }
    ComponentAPIEmitter compApiEmitter = new ComponentAPIEmitter(eclipseDir, (String)compDir.get(0), (String)compAPIDir.get(0));
    compApiEmitter.setClassUseIncludes(includes);
    compApiEmitter.setClassUseExcludes(excludes);
    try
    {
      compApiEmitter.genComponentApiXml();
      compApiEmitter.genComponentUseXML();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  private static void printUsage()
  {
    System.out.println("Usage: java -eclipseDir <eclipseDir> -compDir <compDir> -compAPIDir <compAPIDir> -include <include> -exclude <exclude> org.eclipse.wtp.releng.tools.component.ComponentAPIEmitter");
    System.out.println("");
    System.out.println("\t-eclipseDir\tspace seperated list of directories containing Eclipse plugins");
    System.out.println("\t-compDir\tdirectory containing component.xml");
    System.out.println("\t-compAPIDir\toutput directory of component-api.xml and component-use.xml");
    System.out.println("\t-include\tspace seperated packages to include");
    System.out.println("\t-exclude\tspace seperated packages to exclude");
  }
}