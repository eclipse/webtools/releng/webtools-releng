/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internalreference;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.eclipse.wtp.releng.tools.component.location.IFileLocation;
import org.eclipse.wtp.releng.tools.component.location.ILocation;

/**
 * A <code>Plugin</code> is a model object.  A plugin
 * contains libraries, which in turn contain types.  Plugins
 * can also have children fragments, which contribute more libraries
 * to the plugins list of libraries.
 */
public class Plugin {
	
  public static final String CONST_PLUGIN_XML = "plugin.xml";
	protected String name;
	protected String version;
	protected ILocation location;
	protected List libraries;
	protected Set fragments;
  protected List unresolvedLibs;

	/**
	 * Creates a plugin on the given location
	 * @param location a configuration file location, not <code>null</code>.
	 */
	public Plugin(ILocation location){
		this.location= location;
		libraries= new ArrayList(1);
		fragments= new HashSet(1);
    unresolvedLibs = new ArrayList(1);
	}

	/**
	 * Adds a library at the given path.  The path is relative to the
	 * plugin directory, which is always the directory in which
	 * the configuration file is found.
	 * 
	 * @param relativePath a path relative to the plugin directory indicating
	 * 	where the library can be found
	 */
	public void addLibrary(String relativePath) {
		ILocation libraryLocation= location.createSibling(relativePath);
		if (libraryLocation instanceof IFileLocation) {
      IFileLocation fileLocation = (IFileLocation) libraryLocation;
			File file= fileLocation.getFile();
			if (!file.exists()) {
        unresolvedLibs.add(relativePath);
				System.err.println(file);
			}
		}
		addLibrary(new Library(libraryLocation));
	}
	
	/*
	 * Adds the library to the list of libraries.
	 * @param library a library, not <code>null</code>.
	 */
	protected void addLibrary(Library library) {
		libraries.add(library);
	}

	/**
	 * Answers the libraries that are declared in this plugin.
	 * @return List libraries in this plugin
	 */
	public List getLibraries() {
		return libraries;
	}

	/**
	 * Answers the name of this plugin.  Plugin names do not contain
	 * the version identifier, for example, org.eclipse.core.resources.
	 * @return String the name of the plugin, not <code>null</code>.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the plugin
	 * @param name a plugin name, not <code>null</code>.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Answers the version identifier for the plugin.  A version identifier
	 * is a '.' delimited set of numbers, for example, 2.0.1.5.
	 * @return String the plugin version, not <code>null</code>.
	 */
	public String getVersion() {
		return version;
	}
	
	/**
	 * Sets the plugin version.
	 * @param version a plugin version, not <code>null</code>.
	 */
	public void setVersion(String version) {
		this.version= version;
	}
	
	/**
	 * Answers <code>true</code> if and only if this plugin
	 * contains a library which contains a type with the given
	 * (qualified) name.
	 * @param typeName the name of the type, not <code>null</code>.
	 * @return boolean <code>true</code> if this plugin contains the 
	 * 	named type.
	 */
	public boolean containsType(String typeName) {
		for (Iterator i = libraries.iterator(); i.hasNext();) {
			Library library = (Library) i.next();
			if (library.containsType(typeName)) {
				return true;
			}	
		}
		return false;
	}
	
	/**
	 * Adds a fragment to this plugin's list of fragments
	 * @param fragment a fragment, not <code>null</code>.
	 */
	public void addFragment(Fragment fragment) {
		fragments.add(fragment);
    int size = unresolvedLibs.size();
    for (int i = 0; i < size; i++)
    {
      String libPath = (String)unresolvedLibs.get(i);
      int begin = libPath.indexOf('$');
      int end;
      if (begin != -1)
        end = libPath.indexOf('$', begin + 1);
      else
        end = -1;
      if (begin != -1 && end != -1)
      {
        String s = fragment.getFragmentName().substring(getName().length() + 1);
        StringBuffer sb = new StringBuffer();
        sb.append(libPath.substring(0, begin));
        sb.append(libPath.substring(begin + 1, end));
        sb.append('/');
        sb.append(s);
        sb.append(libPath.substring(end + 1, libPath.length()));
        libPath = sb.toString();
      }
      ILocation loc = (ILocation)fragment.getLocation().createSibling(libPath);
      if (loc instanceof IFileLocation)
      {
        if (((IFileLocation)loc).getFile().exists())
        {
          addLibrary(new Library(loc));
          unresolvedLibs.remove(i);
          i--;
          size--;
        }
      }
    }
	}
	
	/**
	 * Answers the fragments of this plugin
	 * @return Set the set of fragments, not <code>null</code>.
	 */
	public Set getFragments() {
		return fragments;
	}
	
	/**
	 * The unique identifier is a concatination of the plugin name,
	 * and '_' and the plugin version.
	 *  
	 * @return String the unique identifier of the plugin.  
	 */
	public String getUniqueIdentifier() {
		return getName() + "_" + getVersion();
	}

  public ILocation getLocation()
  {
    return location;
  }
}
