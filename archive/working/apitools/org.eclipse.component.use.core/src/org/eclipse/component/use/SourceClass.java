/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: SourceClass.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.use;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.component.use.SourceClass#getClassUse <em>Class Use</em>}</li>
 *   <li>{@link org.eclipse.component.use.SourceClass#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.component.use.UsePackage#getSourceClass()
 * @model 
 * @generated
 */
public interface SourceClass extends EObject
{
  /**
   * Returns the value of the '<em><b>Class Use</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.component.api.ClassApi}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Class Use</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Class Use</em>' containment reference list.
   * @see org.eclipse.component.use.UsePackage#getSourceClass_ClassUse()
   * @model type="org.eclipse.component.api.ClassApi" containment="true" resolveProxies="false" required="true"
   * @generated
   */
  EList getClassUse();

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.eclipse.component.use.UsePackage#getSourceClass_Name()
   * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.eclipse.component.use.SourceClass#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // SourceClass
