/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ILocationChildrenIterator;
import org.eclipse.wtp.releng.tools.component.ILocationVisitor;

public class URLLocation implements ILocation
{
  protected URL url;

  public URLLocation(URL url)
  {
    this.url = url;
  }

  public void accept(ILocationVisitor visitor)
  {
    visitor.accept(this);
  }

  public ILocation getParent()
  {
    return null;
  }

  public String getName()
  {
    return url.getFile();
  }

  public String getAbsolutePath()
  {
    return url.toString();
  }

  public InputStream getInputStream() throws IOException
  {
    return url.openStream();
  }

  public ILocationChildrenIterator childIterator()
  {
    throw new UnsupportedOperationException();
  }

  public boolean hasChildren()
  {
    return false;
  }

  public ILocation createChild(String relativePath)
  {
    throw new UnsupportedOperationException();
  }

  public ILocation createSibling(String relativePath)
  {
    throw new UnsupportedOperationException();
  }
}
