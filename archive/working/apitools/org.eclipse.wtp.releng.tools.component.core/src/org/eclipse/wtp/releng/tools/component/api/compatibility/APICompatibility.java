/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.api.compatibility;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.api.ClassAPI;

public class APICompatibility
{
  public static final String CONST_API_COMPATIBILITY_XML = "api-compatibility.xml";
  private static final String ELEMENT_API_COMPATIBILITY = "api-compatibility";
  private static final String ELEMENT_NEW_APIS = "new-apis";
  private static final String ELEMENT_REMOVED_APIS = "removed-apis";
  private static final String ATTR_XMLNS = "xmlns";
  private static final String ATTR_NAME = "name";

  protected ILocation location;
  protected String name;
  private List newAPIs;
  private List removedAPIs;

  public APICompatibility()
  {
    newAPIs = new ArrayList();
    removedAPIs = new ArrayList(0);
  }

  /**
   * @return Returns the location.
   */
  public ILocation getLocation()
  {
    return location;
  }

  /**
   * @param location The location to set.
   */
  public void setLocation(ILocation location)
  {
    this.location = location;
  }

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    this.name = name;
  }

  public void addNewAPI(ClassAPI classAPI)
  {
    newAPIs.add(classAPI);
  }

  public List getNewAPIs()
  {
    return new ArrayList(newAPIs);
  }

  public void addRemovedAPI(ClassAPI classAPI)
  {
    removedAPIs.add(classAPI);
  }

  public List getRemovedAPIs()
  {
    return new ArrayList(removedAPIs);
  }

  public void saveAsHTML(ILocation html, String xsl) throws TransformerConfigurationException, TransformerException, IOException
  {
    TransformerFactory factory = TransformerFactory.newInstance();
    Transformer transformer = factory.newTransformer(new StreamSource(ClassLoader.getSystemResourceAsStream(xsl)));
    transformer.transform(new StreamSource(new ByteArrayInputStream(getBytes())), new StreamResult(new FileOutputStream(new File(html.getAbsolutePath()))));
  }

  public void save() throws IOException
  {
    if (location != null)
    {
      File file = new File(location.getAbsolutePath());
      file.getParentFile().mkdirs();
      FileOutputStream fos = new FileOutputStream(file);
      fos.write(getBytes());
      fos.close();
    }
  }

  private byte[] getBytes() throws UnsupportedEncodingException
  {
    StringBuffer sb = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    sb.append("<api-compatibility ");
    saveAttribute(sb, ATTR_NAME, getName());
    sb.append(">");
    sb.append("<new-apis>");
    for (Iterator it = getNewAPIs().iterator(); it.hasNext();)
      sb.append(((ClassAPI)it.next()).toString());
    sb.append("</new-apis>");
    sb.append("<removed-apis>");
    for (Iterator it = getRemovedAPIs().iterator(); it.hasNext();)
      sb.append(((ClassAPI)it.next()).toString());
    sb.append("</removed-apis>");
    sb.append("</api-compatibility>");
    return sb.toString().getBytes("UTF-8");
  }

  protected void saveAttribute(StringBuffer sb, String key, String value)
  {
    if (key != null && value != null)
    {
      sb.append(" ");
      sb.append(key);
      sb.append("=\"");
      sb.append(value);
      sb.append("\"");
    }
  }
}
