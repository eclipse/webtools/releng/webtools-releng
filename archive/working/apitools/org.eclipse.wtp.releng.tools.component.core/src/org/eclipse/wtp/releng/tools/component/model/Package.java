/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Package
{
  private String name;
  private Boolean api;
  private Boolean exclusive;
  private List types;

  /**
   * @return Returns the api.
   */
  public boolean isApi()
  {
    if (api == null)
      return true;
    else
      return api.booleanValue();
  }

  public Boolean getApi()
  {
    return api;
  }

  /**
   * @param api The api to set.
   */
  public void setApi(Boolean api)
  {
    this.api = api;
  }

  /**
   * @return Returns the exclusive.
   */
  public boolean isExclusive()
  {
    if (exclusive == null)
      return true;
    else
      return exclusive.booleanValue();
  }

  public Boolean getExclusive()
  {
    return exclusive;
  }

  /**
   * @param exclusive The exclusive to set.
   */
  public void setExclusive(Boolean exclusive)
  {
    this.exclusive = exclusive;
  }

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    this.name = name;
  }

  /**
   * @return Returns the types.
   */
  public List getTypes()
  {
    if (types == null)
      types = new ArrayList(1);
    return types;
  }

  public Object clone()
  {
    Package clone = new Package();
    clone.setName(getName());
    clone.setApi(getApi());
    clone.setExclusive(getExclusive());
    List typesClone = clone.getTypes();
    for (Iterator it = getTypes().iterator(); it.hasNext();)
      typesClone.add(((Type)it.next()).clone());
    return clone;
  }
}