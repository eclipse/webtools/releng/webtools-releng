/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.eclipse.wtp.releng.tools.component.IClazz;
import org.eclipse.wtp.releng.tools.component.IClazzVisitor;
import org.eclipse.wtp.releng.tools.component.ILibrary;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ILocationVisitor;

/**
 * A <code>Library</code> is a model object. Libraries contain types. Type
 * names are unique in libraries, but two libraries could contain two types with
 * the same name.
 */
public class Library implements ILibrary
{
  private ILocation location;
  private Map typeNamesToTypes;

  /**
   * Creates a new <code>Library</code> on the given location.
   * 
   * @param location
   */
  public Library(ILocation location)
  {
    this.location = location;
    typeNamesToTypes = null;
  }

  private void init()
  {
    if (typeNamesToTypes == null)
    {
      typeNamesToTypes = new HashMap();
      location.accept(new ILocationVisitor()
      {
        public boolean accept(ILocation location)
        {
          if (Location.getExtension(location.getName()).equals(EXT_CLASS))
          {
            Clazz clazz = new Clazz(location);
            typeNamesToTypes.put(clazz.getName(), clazz);
            clazz.resetClazz();
          }
          return true;
        }
      });
    }
  }

  /**
   * Answers a mapping of (qualified) type names to <code>Type</code> objects
   * which are found in this library.
   * 
   * @return Map a mapping of type names to <code>Type</code> objects.
   */
  public Map getTypes()
  {
    init();
    return typeNamesToTypes;
  }

  public void resetTypes()
  {
    typeNamesToTypes = null;
  }

  public void accept(IClazzVisitor visitor)
  {
    for (Iterator it = getTypes().values().iterator(); it.hasNext();)
    {
      IClazz clazz = (IClazz)it.next();
      visitor.visit(clazz);
      clazz.resetClazz();
    }
  }
}
