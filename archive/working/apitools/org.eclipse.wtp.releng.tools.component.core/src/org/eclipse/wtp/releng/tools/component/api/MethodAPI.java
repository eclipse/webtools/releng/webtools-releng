/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.core.util.IModifierConstants;

public class MethodAPI
{
  protected static final String ATTR_ACCESS = "access";
  protected static final String ATTR_NAME = "name";
  protected static final String ATTR_DESCRIPTOR = "descriptor";
  protected static final String ATTR_THROWS = "throws";

  private String name;
  private int access;
  private String descriptor;
  private List throwTypes;

  public MethodAPI()
  {
    access = -1;
  }

  /**
   * @return Returns the access.
   */
  public int getAccess()
  {
    return access;
  }

  /**
   * @param access The access to set.
   */
  public void setAccess(int access)
  {
    this.access = access;
  }

  /**
   * @return Returns the descriptor.
   */
  public String getDescriptor()
  {
    return descriptor;
  }

  /**
   * @param descriptor The descriptor to set.
   */
  public void setDescriptor(String descriptor)
  {
    this.descriptor = descriptor;
  }

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    StringBuffer sb = new StringBuffer(name);
    int index = name.indexOf('<');
    while (index != -1)
    {
      sb.deleteCharAt(index);
      sb.insert(index, new char[] {'&', 'l', 't', ';'}, 0, 4);
      index = sb.toString().indexOf('<');
    }
    return sb.toString();
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    StringBuffer sb = new StringBuffer(name);
    int index = name.indexOf("&lt;");
    while (index != -1)
    {
      sb.delete(index, index + 4);
      sb.insert(index, '<');
      index = sb.toString().indexOf("&lt;");
    }
    this.name = sb.toString();
  }

  /**
   * @return Returns the throwTypes.
   */
  public List getThrows()
  {
    if (throwTypes == null)
      throwTypes = new ArrayList(0);
    return throwTypes;
  }

  public int sizeThrows()
  {
    return throwTypes == null ? 0 : throwTypes.size();
  }

  public boolean isPublic()
  {
    return checkAccess(IModifierConstants.ACC_PUBLIC);
  }

  public boolean isProtected()
  {
    return checkAccess(IModifierConstants.ACC_PROTECTED);
  }

  public boolean isPrivate()
  {
    return checkAccess(IModifierConstants.ACC_PRIVATE);
  }

  public boolean isStatic()
  {
    return checkAccess(IModifierConstants.ACC_STATIC);
  }

  public boolean isFinal()
  {
    return checkAccess(IModifierConstants.ACC_FINAL);
  }

  public boolean isSynchronize()
  {
    return checkAccess(IModifierConstants.ACC_SYNCHRONIZED);
  }

  public boolean isNative()
  {
    return checkAccess(IModifierConstants.ACC_NATIVE);
  }

  public boolean isAbstract()
  {
    return checkAccess(IModifierConstants.ACC_ABSTRACT);
  }

  public boolean isStrict()
  {
    return checkAccess(IModifierConstants.ACC_STRICT);
  }

  public List getInputs()
  {
    String[] encodedInputs = Signature.getParameterTypes(getDescriptor());
    List decodedInputs = new ArrayList(encodedInputs.length);
    for (int i = 0; i < encodedInputs.length; i++)
      decodedInputs.add(decodeDescriptor(encodedInputs[i]));
    return decodedInputs;
  }

  public String getReturn()
  {
    return decodeDescriptor(Signature.getReturnType(getDescriptor()));
  }

  private boolean checkAccess(int modifier)
  {
    return ((access & modifier) == modifier);
  }

  private String decodeDescriptor(String descriptor)
  {
    return decodeClassName(Signature.toString(descriptor));
  }

  private String decodeClassName(String className)
  {
    return className.replace('/', '.');
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<method-api");
    saveAttribute(sb, ATTR_NAME, getName());
    saveAttribute(sb, ATTR_DESCRIPTOR, getDescriptor());
    int access = getAccess();
    if (access != -1)
      saveAttribute(sb, ATTR_ACCESS, String.valueOf(access));
    if (sizeThrows() > 0)
      saveAttribute(sb, ATTR_THROWS, getThrows(), " ");
    sb.append("/>");
    return sb.toString();
  }

  protected void saveAttribute(StringBuffer sb, String key, String value)
  {
    if (key != null && value != null)
    {
      sb.append(" ");
      sb.append(key);
      sb.append("=\"");
      sb.append(value);
      sb.append("\"");
    }
  }

  protected void saveAttribute(StringBuffer sb, String key, List values, String delimiter)
  {
    if (key != null && values != null && values.size() > 0)
    {
      sb.append(" ");
      sb.append(key);
      sb.append("=\"");
      for (Iterator it = values.iterator(); it.hasNext();)
      {
        sb.append(it.next().toString());
        if (it.hasNext())
          sb.append(delimiter);
      }
      sb.append("\"");
    }
  }
}