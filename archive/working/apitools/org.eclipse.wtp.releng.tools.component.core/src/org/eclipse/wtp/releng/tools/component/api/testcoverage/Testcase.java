/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.api.testcoverage;

public class Testcase
{
  private String className;

  /**
   * @return Returns the className.
   */
  public String getClassName()
  {
    return className;
  }

  /**
   * @param className The className to set.
   */
  public void setClassName(String className)
  {
    this.className = className;
  }
}
