/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component;

import java.io.IOException;
import org.eclipse.wtp.releng.tools.component.api.compatibility.APICompatibilityEmitter;
import org.eclipse.wtp.releng.tools.component.api.testcoverage.APITestCoverageEmitter;
import org.eclipse.wtp.releng.tools.component.api.ComponentAPIEmitter;
import org.eclipse.wtp.releng.tools.component.model.ComponentEmitter;
import org.eclipse.wtp.releng.tools.component.violation.ComponentViolationEmitter;

public class TestMain
{
  public static void main(String[] args) throws IOException
  {
    wtpMain(args);
    //radMain(args);
  }

  public static void wtpMain(String[] args) throws IOException
  {
    String propsDir = "D:/wtp_v10m2/workspace_api/z/data/";
    String compXMLDir = "C:/Documents and Settings/chng1me.T40-92U-V46/Desktop/api-test-data/component-xmls-for-wtp/";
    String compAPIDir = "C:/Documents and Settings/chng1me.T40-92U-V46/Desktop/api-test-data/api-xmls-for-wtp/";
    String compAPIDir2 = "C:/Documents and Settings/chng1me.T40-92U-V46/Desktop/api-test-data/api-xmls-for-wtp.2/";
    String eclipseDir = "D:/wtp_v10m2/eclipse/";
    String testDir = "D:/wtp_v10m2/tests/";

    String[] compEmitterArgs = new String[]
    {
      "-eclipseDir",
      eclipseDir,
      "-props",
      propsDir + "eclipse.properties",
      "-compXMLDir",
      compXMLDir + "eclipse-components/"
    };
    //ComponentEmitter.main(compEmitterArgs);

    compEmitterArgs[3] = propsDir + "wtp.properties";
    compEmitterArgs[5] = compXMLDir + "wtp-components/";
    //ComponentEmitter.main(compEmitterArgs);

    compEmitterArgs[3] = propsDir + "emf.properties";
    compEmitterArgs[5] = compXMLDir + "emf-components/";
    //ComponentEmitter.main(compEmitterArgs);

    compEmitterArgs[3] = propsDir + "gef.properties";
    compEmitterArgs[5] = compXMLDir + "gef-components/";
    //ComponentEmitter.main(compEmitterArgs);

    compEmitterArgs[3] = propsDir + "ve.properties";
    compEmitterArgs[5] = compXMLDir + "ve-components/";
    //ComponentEmitter.main(compEmitterArgs);

    String[] compViolationEmitterArgs = new String[]
    {
      "-eclipseDir",
      eclipseDir,
      "-testDir",
      testDir,
      "-compXMLDir",
      compXMLDir + "wtp-components/",
      "-compRefDir",
      compXMLDir + "depends/",
      "-compAPIDir",
      compAPIDir,
      "-compUseDir",
      compAPIDir,
      "-compVioDir",
      compAPIDir,
      "-outputDir",
      compAPIDir,
      "-currAPIIndex",
      compAPIDir + "index-comp-summary.xml",
      "-refAPIIndex",
      compAPIDir2 + "index-comp-summary.xml",
      "-exclude",
      "java.",
      "javax.",
      "org.w3c.",
      "org.xml.",
      "org.apache.",
      "sun.",
      "-genHTML",
      "-genAPI",
      "-genUsage",
      "-debug"
    };
    //ComponentAPIEmitter.main(compViolationEmitterArgs);
    ComponentViolationEmitter.main(compViolationEmitterArgs);
    APITestCoverageEmitter.main(compViolationEmitterArgs);
    APICompatibilityEmitter.main(compViolationEmitterArgs);
  }

  public static void radMain(String[] args) throws IOException
  {
    String compXMLDir = "C:/Documents and Settings/chng1me.T40-92U-V46/Desktop/api-test-data/component-xmls-for-rad/";
    String compAPIDir = "C:/Documents and Settings/chng1me.T40-92U-V46/Desktop/api-test-data/api-xmls-for-rad/";
    String compAPIDir2 = "C:/Documents and Settings/chng1me.T40-92U-V46/Desktop/api-test-data/api-xmls-for-rad/";
    String testDir = "C:/temp/";

    String[] compEmitterArgs = new String[]
    {
      "-eclipseDir",
      "D:/rad_v600/csdev/",
      "D:/rad_v600/csdevrpt_shared/",
      "D:/rad_v600/eclipse/",
      "D:/rad_v600/rad/",
      "D:/rad_v600/rad_prod/",
      "D:/rad_v600/radrsm_shared/",
      "D:/rad_v600/rwd/",
      "D:/rad_v600/rwdrpt_shared/",
      "D:/rad_v600/sdpisv/",
      "D:/rad_v600/updater/",
      "-compXMLDir",
      compXMLDir
    };
    //ComponentEmitter.main(compEmitterArgs);

    String[] compViolationEmitterArgs = new String[]
    {
      "-eclipseDir",
      "D:/rad_v600/csdev/",
      "D:/rad_v600/csdevrpt_shared/",
      "D:/rad_v600/eclipse/",
      "D:/rad_v600/rad/",
      "D:/rad_v600/rad_prod/",
      "D:/rad_v600/radrsm_shared/",
      "D:/rad_v600/rwd/",
      "D:/rad_v600/rwdrpt_shared/",
      "D:/rad_v600/sdpisv/",
      "D:/rad_v600/updater/",
      "-testDir",
      testDir,
      "-compXMLDir",
      compXMLDir,
      "-compAPIDir",
      compAPIDir,
      "-compUseDir",
      compAPIDir,
      "-compVioDir",
      compAPIDir,
      "-outputDir",
      compAPIDir,
      "-currAPIIndex",
      compAPIDir + "index-comp-summary.xml",
      "-refAPIIndex",
      compAPIDir2 + "index-comp-summary.xml",
      "-include",
      "org.eclipse.",
      "-exclude",
      "java.",
      "javax.",
      "org.w3c.",
      "org.xml.",
      "org.apache.",
      "sun.",
      "-genHTML",
      "-genAPI",
      "-genUsage",
      "-debug",
    };
    //ComponentAPIEmitter.main(compViolationEmitterArgs);
    //ComponentViolationEmitter.main(compViolationEmitterArgs);
    //APITestCoverageEmitter.main(compViolationEmitterArgs);
    //APICompatibilityEmitter.main(compViolationEmitterArgs);
  }
}