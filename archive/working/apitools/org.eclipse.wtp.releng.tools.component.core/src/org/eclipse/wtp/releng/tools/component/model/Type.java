/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.model;

public class Type
{
  private String name;
  private Boolean reference;
  private Boolean implement;
  private Boolean subclass;
  private Boolean instantiate;

  /**
   * @return Returns the implement.
   */
  public boolean isImplement()
  {
    if (implement == null)
      return true;
    else
      return implement.booleanValue();
  }

  public Boolean getImplement()
  {
    return implement;
  }

  /**
   * @param implement The implement to set.
   */
  public void setImplement(Boolean implement)
  {
    this.implement = implement;
  }

  /**
   * @return Returns the instantiate.
   */
  public boolean isInstantiate()
  {
    if (instantiate == null)
      return true;
    else
      return instantiate.booleanValue();
  }

  public Boolean getInstantiate()
  {
    return instantiate;
  }

  /**
   * @param instantiate The instantiate to set.
   */
  public void setInstantiate(Boolean instantiate)
  {
    this.instantiate = instantiate;
  }

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    this.name = name;
  }

  /**
   * @return Returns the reference.
   */
  public boolean isReference()
  {
    if (reference == null)
      return true;
    else
      return reference.booleanValue();
  }

  public Boolean getReference()
  {
    return reference;
  }

  /**
   * @param reference The reference to set.
   */
  public void setReference(Boolean reference)
  {
    this.reference = reference;
  }

  /**
   * @return Returns the subclass.
   */
  public boolean isSubclass()
  {
    if (subclass == null)
      return true;
    else
      return subclass.booleanValue();
  }

  public Boolean getSubclass()
  {
    return subclass;
  }

  /**
   * @param subclass The subclass to set.
   */
  public void setSubclass(Boolean subclass)
  {
    this.subclass = subclass;
  }

  public Object clone()
  {
    Type clone = new Type();
    clone.setName(getName());
    clone.setReference(getReference());
    clone.setSubclass(getSubclass());
    clone.setImplement(getImplement());
    clone.setInstantiate(getInstantiate());
    return clone;
  }
}