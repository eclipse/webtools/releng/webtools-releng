<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:use="http://eclipse.org/wtp/releng/tools/component-use">
	<xsl:template match="/">
		<html>
			<body>
				<h2>API Compatibility Summary</h2>
				<table border="1">
					<tr>
						<th><h3><b>Component name</b></h3></th>
						<th><h3><b>New API count</b></h3></th>
						<th><h3><b><img src="FAIL.gif"/>Removed API count</b></h3></th>
					</tr>
					<xsl:for-each select="api-compatibility-summary/api-compatibility">
						<xsl:sort select="@name"/>
						<tr>
							<td><a href="{@ref}"><xsl:value-of select="@name"/></a></td>
							<td><xsl:value-of select="@new-api-count"/></td>
							<td><xsl:value-of select="@removed-api-count"/></td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
