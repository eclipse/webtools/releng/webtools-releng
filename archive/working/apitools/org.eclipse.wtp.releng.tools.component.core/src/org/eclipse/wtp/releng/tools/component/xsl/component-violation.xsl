<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:use="http://eclipse.org/wtp/releng/tools/component-use">
	<xsl:template match="/">
		<html>
			<body>
				<h2><xsl:value-of select="use:component-use/@name"/></h2>
				<xsl:for-each select="use:component-use/source">
					<xsl:sort select="@name"/>
					<h3><xsl:value-of select="@name"/></h3>
					<ul>
						<xsl:for-each select="class-use">
							<xsl:if test="@reference">
								<li><p>
									<b>Reference&#160;-&#160;</b><xsl:value-of select="@name"/>
									<ul>
										<xsl:for-each select="method-use">
											<li><b>Method <xsl:if test="@lines"><i>(lines: <xsl:value-of select="@lines"/>)</i></xsl:if>&#160;-&#160;</b><xsl:value-of select="@name"/>&#160;<i><xsl:value-of select="@descriptor"/></i></li>
										</xsl:for-each>
									</ul>
									<ul>
										<xsl:for-each select="field-use">
											<li><b>Field <xsl:if test="@lines"><i>(lines: <xsl:value-of select="@lines"/>)</i></xsl:if>&#160;-&#160;</b><xsl:value-of select="@name"/>&#160;-&#160;<i><xsl:value-of select="@descriptor"/></i></li>
										</xsl:for-each>
									</ul>
								</p></li>
							</xsl:if>
						</xsl:for-each>
					</ul>
					<ul>
						<xsl:for-each select="class-use">
							<xsl:if test="@subclass">
								<li><p>
									<b>Subclass&#160;-&#160;</b><xsl:value-of select="@name"/>
								</p></li>
							</xsl:if>
						</xsl:for-each>
					</ul>
					<ul>
						<xsl:for-each select="class-use">
							<xsl:if test="@implement">
								<li><p>
									<b>Implement&#160;-&#160;</b><xsl:value-of select="@name"/>
								</p></li>
							</xsl:if>
						</xsl:for-each>
					</ul>
					<ul>
						<xsl:for-each select="class-use">
							<xsl:if test="@instantiate">
								<li><p>
									<b>Instantiate <xsl:if test="@lines"><i>(lines: <xsl:value-of select="@lines"/>)</i></xsl:if>&#160;-&#160;</b><xsl:value-of select="@name"/>
								</p></li>
							</xsl:if>
						</xsl:for-each>
					</ul>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
