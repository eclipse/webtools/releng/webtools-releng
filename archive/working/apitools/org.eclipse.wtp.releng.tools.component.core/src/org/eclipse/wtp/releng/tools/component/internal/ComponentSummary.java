/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.eclipse.wtp.releng.tools.component.ILocation;

public class ComponentSummary
{
  public static final String INDEX_COMPONENT_SUMMARY_XML = "index-comp-summary.xml";
  private static final String ROOT_TAG_NAME = "component-summary";
  private List entries;

  public ComponentSummary()
  {
    entries = new ArrayList(1);
  }

  public void add(ComponentEntry entry)
  {
    entries.add(entry);
  }

  public List getEntries()
  {
    return new ArrayList(entries);
  }

  protected void saveAsHTML(ILocation html, String xsl, String rootTagName) throws TransformerConfigurationException, TransformerException, IOException
  {
    TransformerFactory factory = TransformerFactory.newInstance();
    Transformer transformer = factory.newTransformer(new StreamSource(ClassLoader.getSystemResourceAsStream(xsl)));
    transformer.transform(new StreamSource(new ByteArrayInputStream(getBytes(html, rootTagName))), new StreamResult(new FileOutputStream(new File(html.getAbsolutePath()))));
  }

  public void save(ILocation location) throws IOException
  {
    save(location, ROOT_TAG_NAME);
  }

  protected void save(ILocation location, String rootTagName) throws IOException
  {
    File file = new File(location.getAbsolutePath());
    file.getParentFile().mkdirs();
    FileOutputStream fos = new FileOutputStream(file);
    fos.write(getBytes(location, rootTagName));
    fos.close();
  }

  protected byte[] getBytes(ILocation location, String rootTagName) throws UnsupportedEncodingException
  {
    String base = location.getAbsolutePath().replace('\\', '/');
    int index = base.lastIndexOf('/');
    if (index != -1)
      base = base.substring(0, index);
    StringBuffer sb = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    sb.append("<");
    sb.append(rootTagName);
    sb.append(">");
    for (Iterator it = entries.iterator(); it.hasNext();)
    {
      ComponentEntry entry = (ComponentEntry)it.next();
      String ref = entry.getRef();
      if (ref != null && ref.startsWith(base))
      {
        StringBuffer newRef = new StringBuffer(".");
        newRef.append(ref.substring(base.length()));
        entry.setRef(newRef.toString());
      }
      entry.save(sb);
    }
    sb.append("</");
    sb.append(rootTagName);
    sb.append(">");
    return sb.toString().getBytes("UTF-8");
  }
}
