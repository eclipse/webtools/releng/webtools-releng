/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component;

import java.util.List;
import java.util.Set;
import org.eclipse.jdt.core.util.IConstantPoolEntry;
import org.eclipse.jdt.core.util.IFieldInfo;
import org.eclipse.jdt.core.util.IMethodInfo;

public interface IClazz
{
  public Set getReferencedTypes();

  public List getMethodRefs(List includes, List excludes, boolean genLineInfo);

  public void resetMethodRefs();

  public List getFieldRefs(List includes, List excludes, boolean genLineInfo);

  public void resetFieldRefs();

  public String getName();

  public String getSuperClass();

  public String[] getInterfaces();

  public IFieldInfo[] getFieldInfo();

  public IMethodInfo[] getMethodInfo();

  public IConstantPoolEntry[] getConstantPoolEntries(int kind);

  public boolean isInterface();

  public int getAccessFlags();

  public void resetClazz();
}
