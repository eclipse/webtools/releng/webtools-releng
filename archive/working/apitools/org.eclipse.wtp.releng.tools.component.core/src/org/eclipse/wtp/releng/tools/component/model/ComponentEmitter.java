/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import org.eclipse.wtp.releng.tools.component.ILibrary;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.IPluginXML;
import org.eclipse.wtp.releng.tools.component.internal.AbstractEmitter;
import org.eclipse.wtp.releng.tools.component.internal.FileLocation;
import org.eclipse.wtp.releng.tools.component.internal.FragmentXML;
import org.eclipse.wtp.releng.tools.component.util.CommandOptionParser;

public class ComponentEmitter extends AbstractEmitter
{
  public static final String OPTION_ECLIPSE_DIR = "eclipseDir";
  public static final String OPTION_COMPONENT_XML_DIR = "compXMLDir";
  public static final String OPTION_PROPS = "props";

  private String compXMLDir;
  private String props;
  private Map pluginId2Plugin;
  private Map fragmentId2Fragment;

  public ComponentEmitter(String compXMLDir, String props)
  {
    this.compXMLDir = addTrailingSeperator(compXMLDir);
    this.props = props;
  }

  public void init(List eclipseDirs)
  {
    pluginId2Plugin = new HashMap();
    fragmentId2Fragment = new HashMap();
    for (Iterator it = eclipseDirs.iterator(); it.hasNext();)
    {
      File eclipseFile = new File(addTrailingSeperator((String)it.next()));
      if (eclipseFile.exists())
        harvestPlugins(eclipseFile, pluginId2Plugin, fragmentId2Fragment);
    }
    linkPluginsAndFragments(pluginId2Plugin, fragmentId2Fragment);
  }

  public void genComponentXML() throws IOException
  {
    if (props != null)
    {
      Properties properties = new Properties();
      properties.load(new FileInputStream(props));
      for (Iterator it = properties.keySet().iterator(); it.hasNext();)
      {
        String compName = (String)it.next();
        StringTokenizer pluginIds = new StringTokenizer((String)properties.get(compName), " ");
        genComponentXML(compName, pluginIds);
      }
    }
    else
    {
      for (Iterator it = pluginId2Plugin.keySet().iterator(); it.hasNext();)
      {
        String pluginId = (String)it.next();
        genComponentXML(pluginId, new StringTokenizer(pluginId, " "));
      }
    }
  }

  public ComponentXML genComponentXML(String compName, StringTokenizer pluginIds) throws IOException
  {
    ILocation location = null;
    if (compXMLDir != null)
    {
      StringBuffer sb = new StringBuffer(compXMLDir);
      sb.append(compName);
      sb.append('/');
      sb.append(ComponentXML.CONST_COMPONENT_XML);
      location = new FileLocation(new File(sb.toString()));
    }
    ComponentXML compXML = newComponentXML(compName, location);
    List plugins = compXML.getPlugins();
    while (pluginIds.hasMoreTokens())
    {
      String pluginId = pluginIds.nextToken();
      FragmentXML fragment = (FragmentXML)fragmentId2Fragment.get(pluginId);
      if (fragment != null)
        addPlugin(compXML, pluginId, Boolean.TRUE);
      else
      {
        IPluginXML pluginXML = (IPluginXML)pluginId2Plugin.get(pluginId);
        if (pluginXML != null)
        {
          addPlugin(compXML, pluginId, Boolean.FALSE);
          List libs = pluginXML.getLibraries();
          for (Iterator libsIt = libs.iterator(); libsIt.hasNext();)
          {
            ILibrary lib = (ILibrary)libsIt.next();
            Map classes = lib.getTypes();
            for (Iterator classesIt = classes.keySet().iterator(); classesIt.hasNext();)
            {
              String className = (String)classesIt.next();
              int index = className.lastIndexOf('.');
              String pkgName;
              if (index != -1)
              {
                pkgName = className.substring(0, index);
                if (pkgName.indexOf("internal") == -1)
                  addPackage(compXML, pkgName);
              }
            }
            lib.resetTypes();
          }
        }
      }
    }
    if (compXMLDir != null)
    {
      System.out.println("Writing component.xml for " + compName);
      compXML.save();
    }
    return compXML;
  }

  private ComponentXML newComponentXML(String compName, ILocation location)
  {
    ComponentXML compXML = new ComponentXML();
    compXML.setName(compName);
    compXML.setLocation(location);
    ComponentDepends depends = new ComponentDepends();
    depends.setUnrestricted(Boolean.TRUE);
    compXML.setComponentDepends(depends);
    return compXML;
  }

  private Plugin addPlugin(ComponentXML compXML, String pluginId, Boolean fragment)
  {
    List plugins = compXML.getPlugins();
    for (Iterator it = plugins.iterator(); it.hasNext();)
    {
      Plugin plugin = (Plugin)it.next();
      if (plugin.getId().equals(pluginId))
        return plugin;
    }
    Plugin plugin = new Plugin();
    plugin.setId(pluginId);
    plugin.setFragment(fragment);
    plugins.add(plugin);
    return plugin;
  }

  private Package addPackage(ComponentXML compXML, String pkgName)
  {
    List pkgs = compXML.getPackages();
    for (Iterator it = pkgs.iterator(); it.hasNext();)
    {
      Package pkg = (Package)it.next();
      if (pkg.getName().equals(pkgName))
        return pkg;
    }
    Package pkg = new Package();
    pkg.setName(pkgName);
    pkgs.add(pkg);
    return pkg;
  }

  public static void main(String[] args)
  {
    CommandOptionParser optionParser = new CommandOptionParser(args);
    Map options = optionParser.getOptions();
    List eclipseDir = (List)options.get(ComponentEmitter.OPTION_ECLIPSE_DIR);
    List compXMLDir = (List)options.get(ComponentEmitter.OPTION_COMPONENT_XML_DIR);
    List props = (List)options.get(ComponentEmitter.OPTION_PROPS);
    if (eclipseDir == null || compXMLDir == null || eclipseDir.size() < 1 || compXMLDir.size() < 1)
    {
      printUsage();
      System.exit(-1);
    }
    ComponentEmitter compEmitter = new ComponentEmitter((String)compXMLDir.get(0), props != null ? (String)props.get(0) : null);
    compEmitter.init(eclipseDir);
    try
    {
      compEmitter.genComponentXML();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  private static void printUsage()
  {
    System.out.println("Usage: java org.eclipse.wtp.releng.tools.component.model.ComponentEmitter -eclipseDir <eclipseDir> -compXMLDir <compXMLDir> [-options]");
    System.out.println("");
    System.out.println("\t-eclipseDir\tspace seperated list of directories containing Eclipse plugins");
    System.out.println("\t-compXMLDir\tdirectories containing component.xml that will be checked for API violations");
    System.out.println("");
    System.out.println("where options include:");
    System.out.println("\t-props\t<props>\tuse this properties file to define components");
  }
}
