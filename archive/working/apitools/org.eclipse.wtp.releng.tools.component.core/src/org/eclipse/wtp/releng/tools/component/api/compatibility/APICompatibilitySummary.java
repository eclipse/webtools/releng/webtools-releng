/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.api.compatibility;

import java.io.IOException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.internal.ComponentEntry;
import org.eclipse.wtp.releng.tools.component.internal.ComponentSummary;

public class APICompatibilitySummary extends ComponentSummary
{
  private static final String ROOT_TAG_NAME = "api-compatibility-summary";

  public void add(APICompatibility apiCompatibility)
  {
    APICompatibilityEntry entry = new APICompatibilityEntry();
    entry.setCompName(apiCompatibility.getName());
    entry.setNewAPICount(apiCompatibility.getNewAPIs().size());
    entry.setRemovedAPICount(apiCompatibility.getRemovedAPIs().size());
    String ref = apiCompatibility.getLocation().getAbsolutePath();
    if (ref.endsWith(".xml"))
      ref = ref.substring(0, ref.length() - 4) + ".html";
    entry.setRef(ref);
    add(entry);
  }

  public void saveAsHTML(ILocation html) throws TransformerConfigurationException, TransformerException, IOException
  {
    saveAsHTML(html, "org/eclipse/wtp/releng/tools/component/xsl/api-compatibility-summary.xsl", ROOT_TAG_NAME);
  }

  public void save(ILocation location) throws IOException
  {
    save(location, ROOT_TAG_NAME);
  }

  private class APICompatibilityEntry extends ComponentEntry
  {
    private static final String ATTR_NEW_API_COUNT = "new-api-count";
    private static final String ATTR_REMOVED_API_COUNT = "removed-api-count";
    private int newAPICount;
    private int removedAPICount;

    public APICompatibilityEntry()
    {
      newAPICount = 0;
      removedAPICount = 0;
    }

    public void save(StringBuffer sb)
    {
      sb.append("<api-compatibility ");
      saveAttribute(sb, ATTR_NAME, getCompName());
      saveAttribute(sb, ATTR_NEW_API_COUNT, String.valueOf(newAPICount));
      saveAttribute(sb, ATTR_REMOVED_API_COUNT, String.valueOf(removedAPICount));
      saveAttribute(sb, ATTR_REF, getRef());
      sb.append("/>");
    }

    /**
     * @return Returns the newAPICount.
     */
    public int getNewAPICount()
    {
      return newAPICount;
    }

    /**
     * @param newAPICount The newAPICount to set.
     */
    public void setNewAPICount(int newAPICount)
    {
      this.newAPICount = newAPICount;
    }

    /**
     * @return Returns the removedAPICount.
     */
    public int getRemovedAPICount()
    {
      return removedAPICount;
    }

    /**
     * @param removedAPICount The removedAPICount to set.
     */
    public void setRemovedAPICount(int removedAPICount)
    {
      this.removedAPICount = removedAPICount;
    }
  }
}
