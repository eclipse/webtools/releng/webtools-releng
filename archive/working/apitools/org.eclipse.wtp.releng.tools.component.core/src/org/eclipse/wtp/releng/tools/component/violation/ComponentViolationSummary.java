/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.violation;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.internal.ComponentEntry;
import org.eclipse.wtp.releng.tools.component.internal.ComponentSummary;
import org.eclipse.wtp.releng.tools.component.use.ComponentUse;
import org.eclipse.wtp.releng.tools.component.use.Source;

public class ComponentViolationSummary extends ComponentSummary
{
  private static final String ROOT_TAG_NAME = "component-violation-summary";

  public void add(ComponentUse compViolation)
  {
    ComponentViolationEntry entry = new ComponentViolationEntry();
    entry.setCompName(compViolation.getName());
    int numViolations = 0;
    List sources = compViolation.getSources();
    for (Iterator it = sources.iterator(); it.hasNext();)
      numViolations += ((Source)it.next()).getClassUses().size();
    entry.setNumViolations(numViolations);
    entry.setRef(compViolation.getLocation().getAbsolutePath());
    add(entry);
  }

  public void saveAsHTML(ILocation html) throws TransformerConfigurationException, TransformerException, IOException
  {
    saveAsHTML(html, "org/eclipse/wtp/releng/tools/component/xsl/component-violation-summary.xsl", ROOT_TAG_NAME);
  }

  public void save(ILocation location) throws IOException
  {
    save(location, ROOT_TAG_NAME);
  }

  private class ComponentViolationEntry extends ComponentEntry
  {
    private static final String ATTR_COUNT = "count";
    private int numViolations;

    protected ComponentViolationEntry()
    {
      this.numViolations = -1;
    }

    protected void save(StringBuffer sb)
    {
      sb.append("<component-violation ");
      saveAttribute(sb, ATTR_NAME, getCompName());
      saveAttribute(sb, ATTR_COUNT, String.valueOf(numViolations));
      saveAttribute(sb, ATTR_REF, getRef());
      sb.append("/>");
    }

    /**
     * @return Returns the numViolations.
     */
    public int getNumViolations()
    {
      return numViolations;
    }

    /**
     * @param numViolations The numViolations to set.
     */
    public void setNumViolations(int numViolations)
    {
      this.numViolations = numViolations;
    }
  }
}