/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.model;

public class Plugin
{
  private String id;
  private Boolean fragment;

  /**
   * @return Returns the fragment.
   */
  public boolean isFragment()
  {
    if (fragment == null)
      return false;
    else
      return fragment.booleanValue();
  }

  public Boolean getFragment()
  {
    return fragment;
  }

  /**
   * @param fragment The fragment to set.
   */
  public void setFragment(Boolean fragment)
  {
    this.fragment = fragment;
  }

  /**
   * @return Returns the id.
   */
  public String getId()
  {
    return id;
  }

  /**
   * @param id The id to set.
   */
  public void setId(String id)
  {
    this.id = id;
  }

  public Object clone()
  {
    Plugin clone = new Plugin();
    clone.setId(getId());
    clone.setFragment(getFragment());
    return clone;
  }
}