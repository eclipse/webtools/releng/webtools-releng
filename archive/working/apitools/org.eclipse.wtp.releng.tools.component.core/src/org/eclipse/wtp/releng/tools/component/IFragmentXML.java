/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component;

import org.eclipse.wtp.releng.tools.component.internal.PluginXML;

public interface IFragmentXML extends IPluginXML
{
  public static final String CONST_FRAGMENT_XML = "fragment.xml";

  /**
   * Answers the parent plugin of this fragment
   * 
   * @return Plugin the parent plugin of this fragment
   */
  public PluginXML getPlugin();

  /**
   * Answers the name of the plugin which contains this fragment.
   * 
   * @return String the name of the containing plugin, not <code>null</code>
   */
  public String getPluginName();

  /**
   * Answers the version of the plugin which contains this fragment.
   * 
   * @return String the version of the containing plugin, not <code>null</code>
   */
  public String getPluginVersion();

  public String getFragmentName();
}
