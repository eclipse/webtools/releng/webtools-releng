/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.api.testcoverage;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.api.APIs;
import org.eclipse.wtp.releng.tools.component.api.ClassAPI;
import org.eclipse.wtp.releng.tools.component.api.ComponentAPI;
import org.eclipse.wtp.releng.tools.component.api.PackageAPI;
import org.eclipse.wtp.releng.tools.component.internal.ComponentEntry;
import org.eclipse.wtp.releng.tools.component.internal.ComponentSummary;

public class APITestCoverageSummary extends ComponentSummary
{
  private static final String ROOT_TAG_NAME = "component-api-tc-summary";

  public void add(ComponentAPI compAPI)
  {
    APITestCoverageEntry entry = new APITestCoverageEntry();
    int apiCount = 0;
    int testCoverageCount = 0;
    entry.setCompName(compAPI.getName());
    APIs apis = compAPI.getExternalAPIs();
    List pkgAPIs = apis.getPackageAPIs();
    for (Iterator it = pkgAPIs.iterator(); it.hasNext();)
    {
      PackageAPI pkgAPI = (PackageAPI)it.next();
      List classAPIs = pkgAPI.getClassAPIs();
      for (Iterator classAPIsIt = classAPIs.iterator(); classAPIsIt.hasNext();)
      {
        ClassAPI classAPI = (ClassAPI)classAPIsIt.next();
        List methodAPIs = classAPI.getMethodAPIs();
        apiCount += methodAPIs.size();
        for (Iterator methodAPIsIt = methodAPIs.iterator(); methodAPIsIt.hasNext();)
        {
          MethodAPITestCoverage methodAPI = (MethodAPITestCoverage)methodAPIsIt.next();
          if (methodAPI.countTestcases() > 0)
            testCoverageCount++;
        }
      }
    }
    entry.setApiCount(apiCount);
    entry.setTestCoverageCount(testCoverageCount);
    entry.setRef(compAPI.getLocation().getAbsolutePath());
    add(entry);
  }

  public void saveAsHTML(ILocation html) throws TransformerConfigurationException, TransformerException, IOException
  {
    saveAsHTML(html, "org/eclipse/wtp/releng/tools/component/xsl/component-api-tc-summary.xsl", ROOT_TAG_NAME);
  }

  public void save(ILocation location) throws IOException
  {
    save(location, ROOT_TAG_NAME);
  }

  private class APITestCoverageEntry extends ComponentEntry
  {
    private static final String ATTR_API_COUNT= "api-count";
    private static final String ATTR_TEST_COVERAGE_COUNT = "test-coverage-count";
    private static final String ATTR_MISSING_COVERAGE_COUNT = "missing-coverage-count";
    private int apiCount;
    private int testCoverageCount;

    public APITestCoverageEntry()
    {
      apiCount = 0;
      testCoverageCount = 0;
    }

    public void save(StringBuffer sb)
    {
      sb.append("<component-api-tc ");
      saveAttribute(sb, ATTR_NAME, getCompName());
      saveAttribute(sb, ATTR_API_COUNT, String.valueOf(apiCount));
      saveAttribute(sb, ATTR_TEST_COVERAGE_COUNT, String.valueOf(testCoverageCount));
      saveAttribute(sb, ATTR_MISSING_COVERAGE_COUNT, String.valueOf(apiCount - testCoverageCount));
      saveAttribute(sb, ATTR_REF, getRef());
      sb.append("/>");
    }

    /**
     * @return Returns the apiCount.
     */
    public int getApiCount()
    {
      return apiCount;
    }

    /**
     * @param apiCount The apiCount to set.
     */
    public void setApiCount(int apiCount)
    {
      this.apiCount = apiCount;
    }

    /**
     * @return Returns the testCoverageCount.
     */
    public int getTestCoverageCount()
    {
      return testCoverageCount;
    }

    /**
     * @param testCoverageCount The testCoverageCount to set.
     */
    public void setTestCoverageCount(int testCoverageCount)
    {
      this.testCoverageCount = testCoverageCount;
    }
  }
}