/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.eclipse.wtp.releng.tools.component.model.Type;

public class ClassAPI extends Type
{
  protected static final String ATTR_ACCESS = "access";
  protected static final String ATTR_NAME = "name";
  protected static final String ATTR_REFERENCE = "reference";
  protected static final String ATTR_IMPLEMENT = "implement";
  protected static final String ATTR_SUBCLASS = "subclass";
  protected static final String ATTR_INSTANTIATE = "instantiate";

  private int access;
  private List methodAPIs;
  private List fieldAPIs;

  public ClassAPI()
  {
    access = -1;
  }

  /**
   * @return Returns the access.
   */
  public int getAccess()
  {
    return access;
  }

  /**
   * @param access The access to set.
   */
  public void setAccess(int access)
  {
    this.access = access;
  }

  /**
   * @return Returns the fieldAPIs.
   */
  public List getFieldAPIs()
  {
    if (fieldAPIs == null)
      fieldAPIs = new ArrayList(1);
    return fieldAPIs;
  }

  public int sizeFieldAPIs()
  {
    return fieldAPIs != null ? fieldAPIs.size() : 0;
  }

  /**
   * @return Returns the methodAPIs.
   */
  public List getMethodAPIs()
  {
    if (methodAPIs == null)
      methodAPIs = new ArrayList(1);
    return methodAPIs;
  }

  public int sizeMethodAPIs()
  {
    return methodAPIs != null ? methodAPIs.size() : 0;
  }

  private boolean checkAccess(int modifier)
  {
    return ((access & modifier) == modifier);
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<class-api");
    saveAttribute(sb, ATTR_NAME, getName());
    int access = getAccess();
    if (access != -1)
      saveAttribute(sb, ATTR_ACCESS, String.valueOf(access));
    if (getReference() != null)
      saveAttribute(sb, ATTR_REFERENCE, String.valueOf(isReference()));
    if (getImplement() != null)
      saveAttribute(sb, ATTR_IMPLEMENT, String.valueOf(isImplement()));
    if (getSubclass() != null)
      saveAttribute(sb, ATTR_SUBCLASS, String.valueOf(isSubclass()));
    if (getInstantiate() != null)
      saveAttribute(sb, ATTR_INSTANTIATE, String.valueOf(isInstantiate()));
    sb.append(">");
    if (sizeMethodAPIs() > 0)
      for (Iterator it = getMethodAPIs().iterator(); it.hasNext();)
        sb.append(((MethodAPI)it.next()).toString());
    if (sizeFieldAPIs() > 0)
      for (Iterator it = getFieldAPIs().iterator(); it.hasNext();)
        sb.append(((FieldAPI)it.next()).toString());
    sb.append("</class-api>");
    return sb.toString();
  }

  protected void saveAttribute(StringBuffer sb, String key, String value)
  {
    if (key != null && value != null)
    {
      sb.append(" ");
      sb.append(key);
      sb.append("=\"");
      sb.append(value);
      sb.append("\"");
    }
  }
}