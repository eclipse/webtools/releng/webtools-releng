/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.api;

import org.eclipse.jdt.core.util.IModifierConstants;

public class FieldAPI
{
  protected static final String ATTR_ACCESS = "access";
  protected static final String ATTR_NAME = "name";
  protected static final String ATTR_DESCRIPTOR = "descriptor";

  private String name;
  private int access;
  private String descriptor;

  public FieldAPI()
  {
    access = -1;
  }

  /**
   * @return Returns the access.
   */
  public int getAccess()
  {
    return access;
  }

  /**
   * @param access The access to set.
   */
  public void setAccess(int access)
  {
    this.access = access;
  }

  /**
   * @return Returns the descriptor.
   */
  public String getDescriptor()
  {
    return descriptor;
  }

  /**
   * @param descriptor The descriptor to set.
   */
  public void setDescriptor(String descriptor)
  {
    this.descriptor = descriptor;
  }

  /**
   * @return Returns the name.
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name The name to set.
   */
  public void setName(String name)
  {
    this.name = name;
  }

  public boolean isPublic()
  {
    return checkAccess(IModifierConstants.ACC_PUBLIC);
  }

  public boolean isProtected()
  {
    return checkAccess(IModifierConstants.ACC_PROTECTED);
  }

  public boolean isPrivate()
  {
    return checkAccess(IModifierConstants.ACC_PRIVATE);
  }

  public boolean isStatic()
  {
    return checkAccess(IModifierConstants.ACC_STATIC);
  }

  public boolean isFinal()
  {
    return checkAccess(IModifierConstants.ACC_FINAL);
  }

  public boolean isVolatile()
  {
    return checkAccess(IModifierConstants.ACC_VOLATILE);
  }

  public boolean isTransient()
  {
    return checkAccess(IModifierConstants.ACC_TRANSIENT);
  }

  private boolean checkAccess(int modifier)
  {
    return ((access & modifier) == modifier);
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<field-api");
    saveAttribute(sb, ATTR_NAME, getName());
    saveAttribute(sb, ATTR_DESCRIPTOR, getDescriptor());
    int access = getAccess();
    if (access != -1)
      saveAttribute(sb, ATTR_ACCESS, String.valueOf(access));
    sb.append("/>");
    return sb.toString();
  }

  protected void saveAttribute(StringBuffer sb, String key, String value)
  {
    if (key != null && value != null)
    {
      sb.append(" ");
      sb.append(key);
      sb.append("=\"");
      sb.append(value);
      sb.append("\"");
    }
  }
}