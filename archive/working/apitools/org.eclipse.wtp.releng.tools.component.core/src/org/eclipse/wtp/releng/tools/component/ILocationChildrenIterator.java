/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/
package org.eclipse.wtp.releng.tools.component;

/**
 * The <code>ILocationChildrenIterator</code> is a simple iterator that
 * iterates over the children of a location. A <code>null</code> is returned
 * when the end of the children list is reached.
 */

public interface ILocationChildrenIterator
{
  /**
   * Answers the next child location.
   * 
   * @return ILocation The next child location, or <code>null</code> if there
   *         are no more children.
   */
  ILocation next();
}
