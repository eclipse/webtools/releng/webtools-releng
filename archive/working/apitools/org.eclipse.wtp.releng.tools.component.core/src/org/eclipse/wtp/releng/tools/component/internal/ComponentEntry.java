/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/
package org.eclipse.wtp.releng.tools.component.internal;

public class ComponentEntry
{
  protected static final String ATTR_NAME = "name";
  protected static final String ATTR_REF = "ref";
  private String compName;
  private String ref;

  protected void save(StringBuffer sb)
  {
    sb.append("<component ");
    saveAttribute(sb, ATTR_NAME, compName);
    saveAttribute(sb, ATTR_REF, ref);
    sb.append("/>");
  }

  protected void saveAttribute(StringBuffer sb, String key, String value)
  {
    if (key != null && value != null)
    {
      sb.append(" ");
      sb.append(key);
      sb.append("=\"");
      sb.append(value);
      sb.append("\"");
    }
  }

  /**
   * @return Returns the compName.
   */
  public String getCompName()
  {
    return compName;
  }

  /**
   * @param compName
   *          The compName to set.
   */
  public void setCompName(String compName)
  {
    this.compName = compName;
  }

  /**
   * @return Returns the ref.
   */
  public String getRef()
  {
    return ref;
  }

  /**
   * @param ref
   *          The ref to set.
   */
  public void setRef(String ref)
  {
    this.ref = ref;
  }
}
