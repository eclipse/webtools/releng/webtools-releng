/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.use;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.wtp.releng.tools.component.api.FieldAPI;

public class FieldUse extends FieldAPI
{
  private List lines;

  /**
   * @return Returns the line.
   */
  public List getLines()
  {
    if (lines == null)
      lines = new ArrayList(1);
    return lines;
  }

  public int sizeLines()
  {
    return lines != null ? lines.size() : 0;
  }
}
