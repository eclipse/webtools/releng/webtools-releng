/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.api.testcoverage;

import java.util.Iterator;
import java.util.List;
import org.eclipse.wtp.releng.tools.component.api.MethodAPI;

public class MethodAPITestCoverage extends MethodAPI
{
  private static final String ATTR_CLASSNAME = "classname";
  private List testcases;

  public MethodAPITestCoverage()
  {
  }

  /**
   * @return Returns the testcases.
   */
  public List getTestcases()
  {
    return testcases;
  }

  /**
   * @param testcases The testcases to set.
   */
  public void setTestcases(List testcases)
  {
    this.testcases = testcases;
  }

  public int countTestcases()
  {
    return testcases != null ? testcases.size() : 0;
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<method-api");
    saveAttribute(sb, ATTR_NAME, getName());
    saveAttribute(sb, ATTR_DESCRIPTOR, getDescriptor());
    int access = getAccess();
    if (access != -1)
      saveAttribute(sb, ATTR_ACCESS, String.valueOf(access));
    if (sizeThrows() > 0)
      saveAttribute(sb, ATTR_THROWS, getThrows(), " ");
    sb.append(">");
    if (testcases != null && testcases.size() > 0)
      for (Iterator it = testcases.iterator(); it.hasNext();)
        saveTestcase(sb, (Testcase)it.next());
    sb.append("</method-api>");
    return sb.toString();
  }

  private void saveTestcase(StringBuffer sb, Testcase testcase)
  {
    sb.append("<testcase");
    if (testcases.size() > 0)
      saveAttribute(sb, ATTR_CLASSNAME, testcase.getClassName());
    sb.append("/>");
  }
}
