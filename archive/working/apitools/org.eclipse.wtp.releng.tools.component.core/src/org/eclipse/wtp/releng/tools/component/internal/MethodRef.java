/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.jdt.core.util.IConstantPoolEntry;

public class MethodRef
{
  private IConstantPoolEntry poolEntry;
  private List lines;

  public MethodRef()
  {
    lines = new ArrayList(1);
  }

  /**
   * @return Returns the lines.
   */
  public List getLines()
  {
    return lines;
  }

  /**
   * @param lines
   *          The lines to set.
   */
  public void setLines(List lines)
  {
    this.lines = lines;
  }

  public void addLine(int line)
  {
    String s = String.valueOf(line);
    if (!lines.contains(s))
      lines.add(s);
  }

  /**
   * @return Returns the poolEntry.
   */
  public IConstantPoolEntry getPoolEntry()
  {
    return poolEntry;
  }

  /**
   * @param poolEntry
   *          The poolEntry to set.
   */
  public void setPoolEntry(IConstantPoolEntry poolEntry)
  {
    this.poolEntry = poolEntry;
  }

  private String decodeClassName(String className)
  {
    return className.replace('/', '.');
  }

  public String getClassName()
  {
    return decodeClassName(new String(poolEntry.getClassName()));
  }

  public String getMethodName()
  {
    return new String(poolEntry.getMethodName());
  }

  public String getMethodDescriptor()
  {
    return new String(poolEntry.getMethodDescriptor());
  }

  public boolean equals(String cName, String mName, String descriptor)
  {
    return cName != null && mName != null && descriptor != null && cName.equals(getClassName()) && mName.equals(getMethodName()) && descriptor.equals(getMethodDescriptor());
  }
}
