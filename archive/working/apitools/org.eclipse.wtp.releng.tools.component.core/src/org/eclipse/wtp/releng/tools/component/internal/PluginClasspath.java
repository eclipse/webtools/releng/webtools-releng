/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.eclipse.wtp.releng.tools.component.IClazzVisitor;
import org.eclipse.wtp.releng.tools.component.IFileLocation;
import org.eclipse.wtp.releng.tools.component.ILibrary;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ILocationChildrenIterator;
import org.eclipse.wtp.releng.tools.component.IPluginXML;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class PluginClasspath implements IPluginXML
{
  public static final String CONST_DOT_CLASSPATH = ".classpath";
  private IFileLocation dotClasspath;
  private List libraries;
  private String name;
  private String version;
  private String path;

  public PluginClasspath(IFileLocation dotClasspath)
  {
    this.dotClasspath = dotClasspath;
  }

  private void init()
  {
    SAXParser saxParser = null;
    try
    {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      factory.setNamespaceAware(false);
      factory.setValidating(false);
      saxParser = factory.newSAXParser();
    }
    catch (ParserConfigurationException e)
    {
      e.printStackTrace();
    }
    catch (SAXException e)
    {
      e.printStackTrace();
    }
    if (saxParser != null)
    {
      InputStream is = null;
      try
      {
        is = dotClasspath.getInputStream();
        saxParser.parse(new InputSource(is), new DotClasspathHandler());
      }
      catch (SAXException e)
      {
        e.printStackTrace();
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
      finally
      {
        if (is != null)
        {
          try
          {
            is.close();
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
      }
      is = null;
      if (path != null)
      {
        FileLocation bin = new FileLocation(new FileLocation(dotClasspath.getFile().getParentFile()), path);
        ILocationChildrenIterator it = bin.childIterator();
        boolean hasClassFiles = false;
        ILocation loc = it.next();
        while (loc != null)
        {
          if (loc.getName().endsWith(".class"))
          {
            hasClassFiles = true;
            break;
          }
          loc = it.next();
        }
        if (hasClassFiles)
        {
          try
          {
            ILocation location = new FileLocation(new FileLocation(dotClasspath.getFile().getParentFile()), IPluginXML.CONST_PLUGIN_XML);
            is = location.getInputStream();
            saxParser.parse(new InputSource(is), new PluginHandler());
          }
          catch (SAXException e)
          {
            e.printStackTrace();
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          finally
          {
            if (is != null)
            {
              try
              {
                is.close();
              }
              catch (IOException e)
              {
                e.printStackTrace();
              }
            }
          }
        }
      }
    }
  }

  /**
   * Answers the libraries that are declared in this plugin.
   * 
   * @return List libraries in this plugin
   */
  public List getLibraries()
  {
    if (libraries == null)
    {
      libraries = new ArrayList(1);
      ILocation location = new FileLocation(new FileLocation(dotClasspath.getFile().getParentFile()), path);
      libraries.add(new Library(location));
    }
    return libraries;
  }

  /**
   * Answers the name of this plugin. Plugin names do not contain the version
   * identifier, for example, org.eclipse.core.resources.
   * 
   * @return String the name of the plugin, not <code>null</code>.
   */
  public String getName()
  {
    if (name == null)
      init();
    return name;
  }

  /**
   * Answers the version identifier for the plugin. A version identifier is a
   * '.' delimited set of numbers, for example, 2.0.1.5.
   * 
   * @return String the plugin version, not <code>null</code>.
   */
  public String getVersion()
  {
    if (version == null)
      init();
    return version;
  }

  /**
   * The unique identifier is a concatination of the plugin name, and '_' and
   * the plugin version.
   * 
   * @return String the unique identifier of the plugin.
   */
  public String getUniqueIdentifier()
  {
    return getName() + "_" + getVersion();
  }

  public void accept(IClazzVisitor visitor)
  {
    for (Iterator it = getLibraries().iterator(); it.hasNext();)
    {
      ILibrary lib = (ILibrary)it.next();
      lib.accept(visitor);
      lib.resetTypes();
    }
  }

  private class PluginHandler extends DefaultHandler
  {
    public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException
    {
      if (elementName.equals("plugin") || qName.equals("plugin"))
      {
        name = attributes.getValue("id");
        version = attributes.getValue("version");
      }
    }
  }

  private class DotClasspathHandler extends DefaultHandler
  {
    public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException
    {
      if (elementName.equals("classpathentry") || qName.equals("classpathentry"))
      {
        String kind = attributes.getValue("kind");
        if (kind != null && kind.equals("output"))
        {
          path = attributes.getValue("path");
        }
      }
    }
  }
}