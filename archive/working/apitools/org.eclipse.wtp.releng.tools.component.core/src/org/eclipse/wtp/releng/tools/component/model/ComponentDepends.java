/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
�*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ComponentDepends
{
  private Boolean unrestricted;
  private List componentRefs;

  /**
   * @return Returns the componentRefs.
   */
  public List getComponentRefs()
  {
    if (componentRefs == null)
      componentRefs = new ArrayList(1);
    return componentRefs;
  }

  /**
   * @return Returns the unrestricted.
   */
  public boolean isUnrestricted()
  {
    if (unrestricted == null)
      return false;
    else
      return unrestricted.booleanValue();
  }

  public Boolean getUnrestricted()
  {
    return unrestricted;
  }

  /**
   * @param unrestricted The unrestricted to set.
   */
  public void setUnrestricted(Boolean unrestricted)
  {
    this.unrestricted = unrestricted;
  }

  public Object clone()
  {
    ComponentDepends clone = new ComponentDepends();
    clone.setUnrestricted(getUnrestricted());
    List refsClone = clone.getComponentRefs();
    for (Iterator it = getComponentRefs().iterator(); it.hasNext();)
      refsClone.add(((ComponentRef)it.next()).clone());
    return clone;
  }
}