/**********************************************************************
 * Copyright (c) 2002, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.internal;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import org.eclipse.wtp.releng.tools.component.IFileLocation;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ILocationChildrenIterator;

/**
 * 
 */
public class ZipLocation extends AbstractZipLocation implements IFileLocation
{
  private ZipFile zipFile;

  public ZipLocation(File file)
  {
    this(null, file.getAbsolutePath());
  }

  public ZipLocation(ILocation parent, String name)
  {
    super(parent, name);
    File file = getFile();
    if (file.exists())
    {
      try
      {
        zipFile = new ZipFile(file);
      }
      catch (ZipException e)
      {
        System.err.println("Could not open " + file);
      }
      catch (IOException e)
      {
        System.err.println("Could not open " + file);
      }
    }
    else
    {
      System.err.println(file + " does not exist, skipping");
    }
  }

  /*
   * @see com.example.location.ILocation#getInputStream()
   */
  public InputStream getInputStream() throws IOException
  {
    return new FileInputStream(getFile());
  }

  /*
   * Method getFile. @return File
   */
  public File getFile()
  {
    if (parent == null)
    {
      return new File(name);
    }
    else
    {
      return new File(((FileLocation)parent).getFile(), name);
    }
  }

  /*
   * @see com.example.location.ILocation#childIterator()
   */
  public ILocationChildrenIterator childIterator()
  {
    return new ZipFileIterator(zipFile);
  }

  /*
   * @see com.example.location.ILocation#hasChildren()
   */
  public boolean hasChildren()
  {
    return true;
  }

  /*
   * @see com.example.location.AbstractZipLocation#getChildInputStream(java.lang.String)
   */
  protected InputStream getChildInputStream(String name)
  {
    if (zipFile == null)
      return null;
    ZipEntry entry = zipFile.getEntry(name);
    if (entry != null)
    {
      try
      {
        return zipFile.getInputStream(entry);
      }
      catch (IOException e)
      {
        e.printStackTrace();
        return null;
      }
    }
    return null;
  }
  /**
   * Implements the <code>ILocationChildrenIterator</code> to iterate over the
   * entries of a <code>ZipFile</code>.
   */
  protected class ZipFileIterator implements ILocationChildrenIterator
  {
    private ZipFile zipFile;
    private Enumeration entries;

    public ZipFileIterator(ZipFile zip)
    {
      zipFile = zip;
      if (zipFile != null)
      {
        entries = zipFile.entries();
      }
    }

    public ILocation next()
    {
      if (zipFile == null)
        return null;
      if (entries == null)
        return null;
      if (entries.hasMoreElements())
      {
        ZipEntry entry = (ZipEntry)entries.nextElement();
        String name = entry.getName();
        return new ZipEntryLocation(ZipLocation.this, name);
      }
      else
      {
        return null;
      }
    }
  }

  /*
   * @see com.example.location.ILocation#getAbsolutePath()
   */
  public String getAbsolutePath()
  {
    if (parent == null)
    {
      return getName();
    }
    else
    {
      return parent.getAbsolutePath() + "/" + getName();
    }
  }
}
