/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.editor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wtp.releng.tools.component.model.ComponentXML;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;

public class ComponentRefDialog extends Dialog implements ITreeContentProvider
{
  private List ignoreNames;
  private Tree compRefs;
  private String[] compNames;

  public ComponentRefDialog(Shell shell, List ignoreNames)
  {
    super(shell);
    this.ignoreNames = (ignoreNames != null) ? ignoreNames : new ArrayList(0);
  }

  protected void configureShell(Shell shell)
  {
    super.configureShell(shell);
    shell.setText(ComponentManager.getManager().getMessage("DIALOG_TITLE_ADD_COMPONENT_REFS"));
  }

  protected Control createDialogArea(Composite parent)
  {
    Composite composite = (Composite)super.createDialogArea(parent);
    GridLayout gl = new GridLayout();
    gl.marginWidth = 5;
    gl.marginHeight = 5;
    GridData gd = new GridData(GridData.GRAB_HORIZONTAL | GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL | GridData.FILL_VERTICAL);
    gd.widthHint = 250;
    gd.heightHint = 250;
    composite.setLayout(gl);
    composite.setLayoutData(gd);
    compRefs = new Tree(composite, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
    compRefs.setLayout(gl);
    compRefs.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL | GridData.FILL_VERTICAL));
    TreeViewer compRefsViewer = new TreeViewer(compRefs);
    compRefsViewer.setContentProvider(this);
    compRefsViewer.setLabelProvider(new LabelProvider());
    compRefsViewer.setInput(new byte[0]);
    return composite;
  }

  protected void okPressed()
  {
    TreeItem[] items = compRefs.getSelection();
    compNames = new String[items.length];
    for (int i = 0; i < compNames.length; i++)
      compNames[i] = items[i].getText();
    super.okPressed();
  }

  protected void cancelPressed()
  {
    compNames = new String[0];
    super.cancelPressed();
  }

  public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
  {
    // do nothing
  }

  public void dispose()
  {
    // do nothing
  }

  public boolean hasChildren(Object element)
  {
    return false;
  }

  public Object[] getChildren(Object parentElement)
  {
    return new Object[0];
  }

  public Object getParent(Object element)
  {
    return null;
  }

  public Object[] getElements(Object inputElement)
  {
    ComponentManager manager = ComponentManager.getManager();
    List compXMLs = manager.getComponentXMLs();
    List compNames = new ArrayList(compXMLs.size());
    for (Iterator it = compXMLs.iterator(); it.hasNext();)
    {
      String compName = ((ComponentXML)it.next()).getName();
      if (!ignoreNames.contains(compName))
        compNames.add(compName);
    }
    return compNames.toArray(new String[0]);
  }

  public String[] getCompNames()
  {
    return compNames;
  }
}
