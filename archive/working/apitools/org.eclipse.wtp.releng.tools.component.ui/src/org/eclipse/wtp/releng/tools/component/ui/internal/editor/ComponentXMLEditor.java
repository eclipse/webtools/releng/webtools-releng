/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.editor;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.wtp.releng.tools.component.model.ComponentXML;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;

public class ComponentXMLEditor extends FormEditor
{
  private IFile file;
  private ComponentXML compXML;
  private boolean dirty;

  public ComponentXMLEditor()
  {
    dirty = false;
  }

  public void addPages()
  {
    try
    {
      ComponentManager manager = ComponentManager.getManager();
      addPage(new ComponentPage(this, ComponentPage.ID, manager.getMessage("PAGE_COMPONENT")));
      addPage(new APIPage(this, APIPage.ID, manager.getMessage("PAGE_API")));
    }
    catch (PartInitException e)
    {
      e.printStackTrace();
    }
  }

  public void init(IEditorSite site, IEditorInput input) throws PartInitException
  {
    super.init(site, input);
    ComponentManager manager = ComponentManager.getManager();
    if (input instanceof FileEditorInput)
    {
      file = ((FileEditorInput)input).getFile();
      ComponentXML c = manager.getComponentXML(file);
      if (c != null)
        compXML = (ComponentXML)c.clone();
      else
        throw new PartInitException(manager.getMessage("ERROR_MSG_FAIL_TO_LOAD_COMPONENT", new String[] {file.getFullPath().toString()}));
    }
    else
      throw new PartInitException(manager.getMessage("ERROR_MSG_INVALID_COMPONENT_XML"));
  }

  public ComponentXML getComponentXML()
  {
    return compXML;
  }

  public void setDirty(boolean dirty)
  {
    this.dirty = dirty;
    editorDirtyStateChanged();
  }

  public boolean isDirty()
  {
    return dirty;
  }

  public void doSave(IProgressMonitor monitor)
  {
    try
    {
      String content = compXML.toString();
      file.setContents(new ByteArrayInputStream(content.getBytes("UTF-8")), true, true, new NullProgressMonitor());
      setDirty(false);
    }
    catch (CoreException e)
    {
      e.printStackTrace();
    }
    catch (UnsupportedEncodingException e)
    {
      e.printStackTrace();
    }
  }

  public void doSaveAs()
  {
    // do nothing
  }

  public boolean isSaveAsAllowed()
  {
    return false;
  }
}
