/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal;

import java.io.IOException;
import org.eclipse.wtp.releng.tools.component.internal.Clazz;

public class BundleClazz extends Clazz
{
  private byte[] content;

  public BundleClazz(byte[] content)
  {
    super(null);
    this.content = content;
  }

  protected byte[] getInputBytes() throws IOException
  {
    return content;
  }
}
