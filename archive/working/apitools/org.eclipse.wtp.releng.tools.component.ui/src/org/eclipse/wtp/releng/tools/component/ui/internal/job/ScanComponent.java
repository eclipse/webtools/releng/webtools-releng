/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;
import org.eclipse.wtp.releng.tools.component.ui.internal.ComponentUIPlugin;
import org.eclipse.wtp.releng.tools.component.ui.internal.JavaSourceFinder;
import org.eclipse.wtp.releng.tools.component.ui.internal.ScannableComponent;
import org.eclipse.wtp.releng.tools.component.use.ClassUse;
import org.eclipse.wtp.releng.tools.component.use.Source;

public class ScanComponent extends AbstractScanJob
{
  private ScannableComponent scannableComponent;
  private boolean force;
  private List sources;

  public ScanComponent(ScannableComponent scannableComponent, boolean force)
  {
    super(ComponentManager.getManager().getMessage("JOB_SCAN_COMPONENT", new String[] {scannableComponent.getCompXML().getName()}));
    this.scannableComponent = scannableComponent;
    this.force = force;
    this.sources = null;
  }

  public ScannableComponent getScannableComponent()
  {
    return scannableComponent;
  }

  public List getSources()
  {
    return sources;
  }

  public IStatus run(IProgressMonitor monitor)
  {
    IJobManager jobManager = Platform.getJobManager();
    IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
    try
    {
      jobManager.beginRule(root, new NullProgressMonitor());
    }
    finally
    {
      jobManager.endRule(root);
    }
    try
    {
      sources = scannableComponent.scan(force);
    }
    catch (IOException e)
    {
      return new Status(IStatus.ERROR, ComponentUIPlugin.ID, IStatus.ERROR, "", e);
    }
    if (sources != null)
    {
      List innerClasses = new ArrayList();
      for (Iterator sourcesIt = sources.iterator(); sourcesIt.hasNext();)
      {
        Source source = (Source)sourcesIt.next();
        String sourceName = source.getName();
        if (sourceName.indexOf('$') == -1)
          createMarker(sourceName, source.getClassUses(), false);
        else
          innerClasses.add(source);
      }
      for (Iterator it = innerClasses.iterator(); it.hasNext();)
      {
        Source source = (Source)it.next();
        createMarker(source.getName(), source.getClassUses(), true);
      }
    }
    return new Status(IStatus.OK, ComponentUIPlugin.ID, IStatus.OK, "", null);
  }

  private void createMarker(String sourceName, List classUses, boolean isInnerClass)
  {
    List projects = scannableComponent.getProjects();
    for (Iterator projectsIt = projects.iterator(); projectsIt.hasNext();)
    {
      JavaSourceFinder finder = new JavaSourceFinder(sourceName);
      try
      {
        ((IProject)projectsIt.next()).accept(finder, IResource.DEPTH_INFINITE | IResource.NONE);
        IResource javaSource = finder.getJavaSource();
        if (javaSource != null)
        {
          if (!isInnerClass)
            deleteViolationMarksers(javaSource);
          for (Iterator it = classUses.iterator(); it.hasNext();)
          {
            createClassViolationMarker(javaSource, (ClassUse)it.next());
          }
        }
      }
      catch (CoreException e)
      {
        // should never happen
        e.printStackTrace();
      }
    }
  }
}