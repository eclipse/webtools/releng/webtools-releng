/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.editor;

import java.util.List;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.wtp.releng.tools.component.model.ComponentDepends;
import org.eclipse.wtp.releng.tools.component.model.ComponentRef;
import org.eclipse.wtp.releng.tools.component.model.ComponentXML;
import org.eclipse.wtp.releng.tools.component.model.Package;
import org.eclipse.wtp.releng.tools.component.model.Plugin;
import org.eclipse.wtp.releng.tools.component.model.Type;

public class ComponentXMLProvider extends LabelProvider implements ITreeContentProvider, ILabelProvider
{
  public static final int SHOW_COMPONENT_REFS = 0x0;
  public static final int SHOW_PLUGINS = 0x1;
  public static final int SHOW_APIS = 0x2;
  private int show;

  public ComponentXMLProvider(int show)
  {
    this.show = show;
  }

  // ITreeContentProvider

  public boolean hasChildren(Object element)
  {
    if (element instanceof Package)
    {
      return ((Package)element).getTypes().size() > 0;
    }
    return false;
  }

  public Object[] getChildren(Object parentElement)
  {
    if (parentElement instanceof Package)
    {
      List types = ((Package)parentElement).getTypes();
      return types.toArray(new Type[0]);
    }
    return new Object[0];
  }

  public Object[] getElements(Object inputElement)
  {
    if (show == SHOW_COMPONENT_REFS && inputElement instanceof ComponentDepends)
    {
      List compRefs = ((ComponentDepends)inputElement).getComponentRefs();
      return compRefs.toArray(new ComponentRef[0]);
    }
    else if (show == SHOW_PLUGINS && inputElement instanceof ComponentXML)
    {
      List plugins = ((ComponentXML)inputElement).getPlugins();
      return plugins.toArray(new Plugin[0]);
    }
    else if (show == SHOW_APIS && inputElement instanceof ComponentXML)
    {
      List packages = ((ComponentXML)inputElement).getPackages();
      return packages.toArray(new Package[0]);
    }
    return new Object[0];
  }

  public Object getParent(Object element)
  {
    return null;
  }

  public void dispose()
  {
    // do nothing
  }

  public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
  {
    // do nothing
  }

  // ILabelProvider

  public String getText(Object element)
  {
    if (show == SHOW_COMPONENT_REFS && element instanceof ComponentRef)
    {
      return ((ComponentRef)element).getName();
    }
    else if (show == SHOW_PLUGINS && element instanceof Plugin)
    {
      return ((Plugin)element).getId();
    }
    else if (show == SHOW_APIS)
    {
      if (element instanceof Package)
      {
        return ((Package)element).getName();
      }
      else if (element instanceof Type)
      {
        return ((Type)element).getName();
      }
    }
    return element.toString();
  }
}
