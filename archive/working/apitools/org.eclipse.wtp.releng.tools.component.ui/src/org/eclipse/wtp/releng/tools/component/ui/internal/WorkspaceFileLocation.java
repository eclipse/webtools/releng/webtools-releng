/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.wtp.releng.tools.component.internal.FileLocation;

public class WorkspaceFileLocation extends FileLocation
{
  private IFile workspaceFile;

  public WorkspaceFileLocation(IFile workspaceFile)
  {
    super(Platform.getLocation().append(workspaceFile.getFullPath()).toFile());
    this.workspaceFile = workspaceFile;
  }

  public IFile getWorkspaceFile()
  {
    return workspaceFile;
  }
}