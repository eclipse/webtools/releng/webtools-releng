/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.action;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;
import org.eclipse.wtp.releng.tools.component.ui.internal.ScannableComponent;
import org.eclipse.wtp.releng.tools.component.ui.internal.WorkspaceFileLocation;
import org.eclipse.wtp.releng.tools.component.ui.internal.job.ScanComponent;
import org.eclipse.wtp.releng.tools.component.use.Source;

public class ScanAPI extends Action implements IActionDelegate
{
  private ScanComponent job;

  public void run()
  {
    IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
    if (window != null)
    {
      IStructuredSelection selection = (IStructuredSelection)window.getSelectionService().getSelection();
      if (selection != null && !selection.isEmpty())
      {
        Object firstElement = selection.getFirstElement();
        if (firstElement != null && firstElement instanceof IFile)
        {
          ComponentManager manager = ComponentManager.getManager();
          ScannableComponent scannableComp = getScannableComponent((IFile)firstElement);
          if (scannableComp != null)
          {
            job = new ScanComponent(scannableComp, true);
            job.schedule();
            ProgressMonitorDialog dialog = new ProgressMonitorDialog(window.getShell());
            try
            {
              dialog.run(false, false, new IRunnableWithProgress()
              {
                public void run(IProgressMonitor monitor)
                {
                  boolean interrupted = true;
                  while (interrupted)
                  {
                    try
                    {
                      job.join();
                      interrupted = false;
                    }
                    catch (InterruptedException e)
                    {
                      interrupted = true;
                    }
                  }
                }
              });
            }
            catch (InterruptedException e)
            {
              e.printStackTrace();
            }
            catch (InvocationTargetException e)
            {
              e.printStackTrace();
            }
            if (hasAPIViolation(job.getSources()))
              MessageDialog.openError(window.getShell(), manager.getMessage("TITLE_HAS_API_VIOLATIONS"), manager.getMessage("ERROR_MSG_HAS_API_VIOLATIONS"));
            else
              MessageDialog.openInformation(window.getShell(), manager.getMessage("TITLE_HAS_API_VIOLATIONS"), manager.getMessage("INFO_MSG_NO_API_VIOLATIONS"));
          }
          else
            MessageDialog.openError(window.getShell(), manager.getMessage("TITLE_NO_WORKSPACE_PLUGINS"), manager.getMessage("ERROR_MSG_NO_WORKSPACE_PLUGINS"));
        }
      }
    }
  }

  private ScannableComponent getScannableComponent(IFile compXML)
  {
    for (Iterator it = ComponentManager.getManager().getScannableComponents().values().iterator(); it.hasNext();)
    {
      ScannableComponent scannableComp = (ScannableComponent)it.next();
      String scannableCompLoc = scannableComp.getCompXML().getLocation().getAbsolutePath();
      String compLoc = new WorkspaceFileLocation(compXML).getAbsolutePath();
      if (scannableCompLoc.equals(compLoc))
        return scannableComp;
    }
    return null;
  }

  private boolean hasAPIViolation(List sources)
  {
    if (sources != null)
      for (int i = 0; i < sources.size(); i++)
        if (((Source)sources.get(i)).getClassUses().size() > 0)
          return true;
    return false;
  }

  public void run(IAction action)
  {
    run();
  }

  public void selectionChanged(IAction action, ISelection selection)
  {
  }
}
