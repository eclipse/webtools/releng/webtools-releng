/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.editor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wtp.releng.tools.component.ILibrary;
import org.eclipse.wtp.releng.tools.component.IPluginXML;
import org.eclipse.wtp.releng.tools.component.model.ComponentXML;
import org.eclipse.wtp.releng.tools.component.model.Package;
import org.eclipse.wtp.releng.tools.component.model.Plugin;
import org.eclipse.wtp.releng.tools.component.model.Type;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;

public class APIDialog extends Dialog implements ITreeContentProvider
{
  public static final int OPTION_PACKAGE = 0x0;
  public static final int OPTION_TYPE = 0x1;
  private int option;
  private ComponentXML compXML;
  private Package pkg;
  private List ignoreNames;
  private Tree tree;

  public APIDialog(Shell shell, int option, ComponentXML compXML, Package pkg, List ignoreNames)
  {
    super(shell);
    this.option = option;
    this.compXML = compXML;
    this.pkg = pkg;
    this.ignoreNames = ignoreNames;
  }

  protected void configureShell(Shell shell)
  {
    super.configureShell(shell);
    if (option == OPTION_PACKAGE)
      shell.setText(ComponentManager.getManager().getMessage("DIALOG_TITLE_ADD_PACKAGES"));
    else
      shell.setText(ComponentManager.getManager().getMessage("DIALOG_TITLE_ADD_TYPES"));
  }

  protected Control createDialogArea(Composite parent)
  {
    Composite composite = (Composite)super.createDialogArea(parent);
    GridLayout gl = new GridLayout();
    gl.marginWidth = 5;
    gl.marginHeight = 5;
    GridData gd = new GridData(GridData.GRAB_HORIZONTAL | GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL | GridData.FILL_VERTICAL);
    gd.widthHint = 300;
    gd.heightHint = 300;
    composite.setLayout(gl);
    composite.setLayoutData(gd);
    tree = new Tree(composite, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
    tree.setLayout(gl);
    tree.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.FILL_HORIZONTAL | GridData.GRAB_VERTICAL | GridData.FILL_VERTICAL));
    TreeViewer apisViewer = new TreeViewer(tree);
    apisViewer.setContentProvider(this);
    apisViewer.setLabelProvider(new LabelProvider());
    apisViewer.setInput(compXML);
    return composite;
  }

  protected void okPressed()
  {
    if (option == OPTION_PACKAGE)
    {
      List pkgs = compXML.getPackages();
      TreeItem[] items = tree.getSelection();
      for (int i = 0; i < items.length; i++)
      {
        Package pkg = new Package();
        pkg.setName(items[i].getText());
        pkgs.add(pkg);
      }
    }
    else
    {
      List types = pkg.getTypes();
      TreeItem[] items = tree.getSelection();
      for (int i = 0; i < items.length; i++)
      {
        Type type = new Type();
        type.setName(items[i].getText());
        types.add(type);
      }
    }
    super.okPressed();
  }

  protected void cancelPressed()
  {
    super.cancelPressed();
  }

  public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
  {
    // do nothing
  }

  public void dispose()
  {
    // do nothing
  }

  public boolean hasChildren(Object element)
  {
    return false;
  }

  public Object[] getChildren(Object parentElement)
  {
    return new Object[0];
  }

  public Object getParent(Object element)
  {
    return null;
  }

  public Object[] getElements(Object inputElement)
  {
    List nameList = new ArrayList();
    if (inputElement instanceof ComponentXML)
    {
      List plugins = ((ComponentXML)inputElement).getPlugins();
      for (Iterator pluginsIt = plugins.iterator(); pluginsIt.hasNext();)
      {
        String pluginId = ((Plugin)pluginsIt.next()).getId();
        IPluginXML pluginXML = ComponentManager.getManager().getPlugin(pluginId);
        if (pluginXML != null)
        {
          List libs = pluginXML.getLibraries();
          for (Iterator libsIt = libs.iterator(); libsIt.hasNext();)
          {
            ILibrary lib = (ILibrary)libsIt.next();
            Map types = lib.getTypes();
            for (Iterator typesIt = types.keySet().iterator(); typesIt.hasNext();)
            {
              String typeName = (String)typesIt.next();
              if (typeName.lastIndexOf('$') == -1 && typeName.indexOf(".internal.") == -1)
              {
                int dot = typeName.lastIndexOf('.');
                String name = null;
                if (option == OPTION_PACKAGE && dot != -1)
                {
                  name = typeName.substring(0, dot);
                }
                else
                {
                  String pkgName = pkg.getName();
                  if (dot != -1 && dot == pkgName.length() && typeName.startsWith(pkgName))
                  {
                    name = typeName.substring(dot + 1);
                  }
                }
                if (name != null && !ignoreNames.contains(name) && !nameList.contains(name))
                  nameList.add(name);
              }
            }
          }
        }
      }
    }
    return nameList.toArray(new String[0]);
  }
}