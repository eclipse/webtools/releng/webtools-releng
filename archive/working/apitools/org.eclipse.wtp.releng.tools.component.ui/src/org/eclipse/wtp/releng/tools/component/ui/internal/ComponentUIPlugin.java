/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.plugin.*;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;
import org.osgi.framework.BundleContext;
import java.util.*;

/**
 * The main plugin class to be used in the desktop.
 */
public class ComponentUIPlugin extends AbstractUIPlugin implements IStartup
{
  public static final String ID = "org.eclipse.wtp.releng.tools.component.ui";
  // The shared instance.
  private static ComponentUIPlugin plugin;
  // Resource bundle.
  private ResourceBundle resourceBundle;

  /**
   * The constructor.
   */
  public ComponentUIPlugin()
  {
    super();
    plugin = this;
  }

  public void earlyStartup()
  {
    ComponentManager manager = ComponentManager.getManager();
    manager.init();
    ResourcesPlugin.getWorkspace().addResourceChangeListener(manager, IResourceChangeEvent.POST_CHANGE);
  }

  /**
   * This method is called upon plug-in activation
   */
  public void start(BundleContext context) throws Exception
  {
    super.start(context);
  }

  /**
   * This method is called when the plug-in is stopped
   */
  public void stop(BundleContext context) throws Exception
  {
    super.stop(context);
    plugin = null;
    resourceBundle = null;
  }

  /**
   * Returns the shared instance.
   */
  public static ComponentUIPlugin getDefault()
  {
    return plugin;
  }

  /**
   * Returns the string from the plugin's resource bundle, or 'key' if not
   * found.
   */
  public static String getResourceString(String key)
  {
    ResourceBundle bundle = ComponentUIPlugin.getDefault().getResourceBundle();
    try
    {
      return (bundle != null) ? bundle.getString(key) : key;
    }
    catch (MissingResourceException e)
    {
      return key;
    }
  }

  /**
   * Returns the plugin's resource bundle,
   */
  public ResourceBundle getResourceBundle()
  {
    try
    {
      if (resourceBundle == null)
        resourceBundle = ResourceBundle.getBundle("org.eclipse.wtp.releng.tools.component.ui.internal.ComponentUIPluginResources");
    }
    catch (MissingResourceException x)
    {
      resourceBundle = null;
    }
    return resourceBundle;
  }
}
