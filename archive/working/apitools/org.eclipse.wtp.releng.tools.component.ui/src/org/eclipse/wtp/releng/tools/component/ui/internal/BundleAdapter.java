/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.eclipse.osgi.util.ManifestElement;
import org.eclipse.wtp.releng.tools.component.IClazzVisitor;
import org.eclipse.wtp.releng.tools.component.ILibrary;
import org.eclipse.wtp.releng.tools.component.IPluginXML;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;

public class BundleAdapter implements IPluginXML
{
  private Bundle bundle;
  private List libraries;

  public BundleAdapter(Bundle bundle)
  {
    this.bundle = bundle;
  }

  /**
   * Answers the libraries that are declared in this plugin.
   * 
   * @return List libraries in this plugin
   */
  public List getLibraries()
  {
    if (libraries == null)
    {
      libraries = new ArrayList(1);
      String classpathHeader = (String)bundle.getHeaders().get(Constants.BUNDLE_CLASSPATH);
      if (classpathHeader != null)
      {
        try
        {
          ManifestElement[] classpaths = ManifestElement.parseHeader(Constants.BUNDLE_CLASSPATH, classpathHeader);
          if (classpaths != null)
            for (int i = 0; i < classpaths.length; i++)
              libraries.add(new BundleLibrary(bundle, classpaths[i]));
        }
        catch (BundleException e)
        {
          e.printStackTrace();
        }
      }
    }
    return libraries;
  }

  /**
   * Answers the name of this plugin. Plugin names do not contain the version
   * identifier, for example, org.eclipse.core.resources.
   * 
   * @return String the name of the plugin, not <code>null</code>.
   */
  public String getName()
  {
    return bundle.getSymbolicName();
  }

  /**
   * Answers the version identifier for the plugin. A version identifier is a
   * '.' delimited set of numbers, for example, 2.0.1.5.
   * 
   * @return String the plugin version, not <code>null</code>.
   */
  public String getVersion()
  {
    return (String)bundle.getHeaders().get(Constants.BUNDLE_VERSION);
  }

  /**
   * The unique identifier is a concatination of the plugin name, and '_' and
   * the plugin version.
   * 
   * @return String the unique identifier of the plugin.
   */
  public String getUniqueIdentifier()
  {
    return getName() + "_" + getVersion();
  }

  public void accept(IClazzVisitor visitor)
  {
    for (Iterator it = getLibraries().iterator(); it.hasNext();)
    {
      ILibrary lib = (ILibrary)it.next();
      lib.accept(visitor);
      lib.resetTypes();
    }
  }
}
