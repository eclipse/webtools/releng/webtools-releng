/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.job;

import java.util.Iterator;
import java.util.List;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.wtp.releng.tools.component.ILocation;
import org.eclipse.wtp.releng.tools.component.ui.ComponentManager;
import org.eclipse.wtp.releng.tools.component.ui.internal.ComponentUIPlugin;
import org.eclipse.wtp.releng.tools.component.ui.internal.ScannableComponent;
import org.eclipse.wtp.releng.tools.component.ui.internal.WorkspaceFileLocation;

public class RemoveComponent extends AbstractModifyMarkersJob
{
  private IFile file;

  public RemoveComponent(IFile file)
  {
    super(ComponentManager.getManager().getMessage("JOB_REMOVE_COMPONENT", new String[] {file.getFullPath().toString()}));
    this.file = file;
  }

  public IStatus run(IProgressMonitor monitor)
  {
    IStatus status = new Status(IStatus.OK, ComponentUIPlugin.ID, IStatus.OK, "", null);
    ComponentManager manager = ComponentManager.getManager();
    ILocation location = new WorkspaceFileLocation(file);
    ScannableComponent scannableComponent = manager.removeScannableComponent(location);
    if (scannableComponent != null)
    {
      List projects = scannableComponent.getProjects();
      for (Iterator it = projects.iterator(); it.hasNext();)
      {
        try
        {
          deleteViolationMarksers((IProject)it.next());
        }
        catch (CoreException e)
        {
          status = new Status(IStatus.ERROR, ComponentUIPlugin.ID, IStatus.ERROR, "", e);
        }
      }
    }
    manager.removeCompRef(location);
    return status;
  }
}