/**********************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 �*
 * Contributors:
 *    IBM - Initial API and implementation
 **********************************************************************/

package org.eclipse.wtp.releng.tools.component.ui.internal.job;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.jobs.IJobManager;

public abstract class AbstractScanJob extends AbstractModifyMarkersJob
{
  public AbstractScanJob(String name)
  {
    super(name);
  }

  protected void joinBuilds()
  {
    boolean interrupted = true;
    while (interrupted)
    {
      try
      {
        IJobManager jobManager = Platform.getJobManager();
        jobManager.join(ResourcesPlugin.FAMILY_AUTO_BUILD, null);
        jobManager.join(ResourcesPlugin.FAMILY_MANUAL_BUILD, null);
        interrupted = false;
      }
      catch (InterruptedException e)
      {
        interrupted = true;
      }
    }
  }
}
