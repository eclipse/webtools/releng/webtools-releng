/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentPackageImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.impl;

import org.eclipse.component.ComponentDependsType;
import org.eclipse.component.ComponentFactory;
import org.eclipse.component.ComponentPackage;
import org.eclipse.component.ComponentRefType;
import org.eclipse.component.ComponentType;
import org.eclipse.component.DocumentRoot;
import org.eclipse.component.PackageType;
import org.eclipse.component.PluginType;
import org.eclipse.component.TypeType;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.eclipse.emf.ecore.xml.type.impl.XMLTypePackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ComponentPackageImpl extends EPackageImpl implements ComponentPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentDependsTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentRefTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass documentRootEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass packageTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass pluginTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeTypeEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.eclipse.component.ComponentPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private ComponentPackageImpl()
  {
    super(eNS_URI, ComponentFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this
   * model, and for any others upon which it depends.  Simple
   * dependencies are satisfied by calling this method on all
   * dependent packages before doing anything else.  This method drives
   * initialization for interdependent packages directly, in parallel
   * with this package, itself.
   * <p>Of this package and its interdependencies, all packages which
   * have not yet been registered by their URI values are first created
   * and registered.  The packages are then initialized in two steps:
   * meta-model objects for all of the packages are created before any
   * are initialized, since one package's meta-model objects may refer to
   * those of another.
   * <p>Invocation of this method will not affect any packages that have
   * already been initialized.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static ComponentPackage init()
  {
    if (isInited) return (ComponentPackage)EPackage.Registry.INSTANCE.getEPackage(ComponentPackage.eNS_URI);

    // Obtain or create and register package
    ComponentPackageImpl theComponentPackage = (ComponentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof ComponentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new ComponentPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    XMLTypePackageImpl.init();

    // Create package meta-data objects
    theComponentPackage.createPackageContents();

    // Initialize created meta-data
    theComponentPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theComponentPackage.freeze();

    return theComponentPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentDependsType()
  {
    return componentDependsTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDependsType_ComponentRef()
  {
    return (EReference)componentDependsTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getComponentDependsType_Unrestricted()
  {
    return (EAttribute)componentDependsTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentRefType()
  {
    return componentRefTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getComponentRefType_Name()
  {
    return (EAttribute)componentRefTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentType()
  {
    return componentTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentType_Plugin()
  {
    return (EReference)componentTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentType_Package()
  {
    return (EReference)componentTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentType_ComponentDepends()
  {
    return (EReference)componentTypeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getComponentType_Name()
  {
    return (EAttribute)componentTypeEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDocumentRoot()
  {
    return documentRootEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDocumentRoot_Mixed()
  {
    return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XMLNSPrefixMap()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XSISchemaLocation()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_Component()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_ComponentDepends()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_ComponentRef()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_Package()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_Plugin()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_Type()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPackageType()
  {
    return packageTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPackageType_Type()
  {
    return (EReference)packageTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPackageType_Api()
  {
    return (EAttribute)packageTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPackageType_Exclusive()
  {
    return (EAttribute)packageTypeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPackageType_Name()
  {
    return (EAttribute)packageTypeEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPluginType()
  {
    return pluginTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPluginType_Fragment()
  {
    return (EAttribute)pluginTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPluginType_Id()
  {
    return (EAttribute)pluginTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeType()
  {
    return typeTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTypeType_Implement()
  {
    return (EAttribute)typeTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTypeType_Instantiate()
  {
    return (EAttribute)typeTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTypeType_Name()
  {
    return (EAttribute)typeTypeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTypeType_Reference()
  {
    return (EAttribute)typeTypeEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTypeType_Subclass()
  {
    return (EAttribute)typeTypeEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentFactory getComponentFactory()
  {
    return (ComponentFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    componentDependsTypeEClass = createEClass(COMPONENT_DEPENDS_TYPE);
    createEReference(componentDependsTypeEClass, COMPONENT_DEPENDS_TYPE__COMPONENT_REF);
    createEAttribute(componentDependsTypeEClass, COMPONENT_DEPENDS_TYPE__UNRESTRICTED);

    componentRefTypeEClass = createEClass(COMPONENT_REF_TYPE);
    createEAttribute(componentRefTypeEClass, COMPONENT_REF_TYPE__NAME);

    componentTypeEClass = createEClass(COMPONENT_TYPE);
    createEReference(componentTypeEClass, COMPONENT_TYPE__PLUGIN);
    createEReference(componentTypeEClass, COMPONENT_TYPE__PACKAGE);
    createEReference(componentTypeEClass, COMPONENT_TYPE__COMPONENT_DEPENDS);
    createEAttribute(componentTypeEClass, COMPONENT_TYPE__NAME);

    documentRootEClass = createEClass(DOCUMENT_ROOT);
    createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
    createEReference(documentRootEClass, DOCUMENT_ROOT__COMPONENT);
    createEReference(documentRootEClass, DOCUMENT_ROOT__COMPONENT_DEPENDS);
    createEReference(documentRootEClass, DOCUMENT_ROOT__COMPONENT_REF);
    createEReference(documentRootEClass, DOCUMENT_ROOT__PACKAGE);
    createEReference(documentRootEClass, DOCUMENT_ROOT__PLUGIN);
    createEReference(documentRootEClass, DOCUMENT_ROOT__TYPE);

    packageTypeEClass = createEClass(PACKAGE_TYPE);
    createEReference(packageTypeEClass, PACKAGE_TYPE__TYPE);
    createEAttribute(packageTypeEClass, PACKAGE_TYPE__API);
    createEAttribute(packageTypeEClass, PACKAGE_TYPE__EXCLUSIVE);
    createEAttribute(packageTypeEClass, PACKAGE_TYPE__NAME);

    pluginTypeEClass = createEClass(PLUGIN_TYPE);
    createEAttribute(pluginTypeEClass, PLUGIN_TYPE__FRAGMENT);
    createEAttribute(pluginTypeEClass, PLUGIN_TYPE__ID);

    typeTypeEClass = createEClass(TYPE_TYPE);
    createEAttribute(typeTypeEClass, TYPE_TYPE__IMPLEMENT);
    createEAttribute(typeTypeEClass, TYPE_TYPE__INSTANTIATE);
    createEAttribute(typeTypeEClass, TYPE_TYPE__NAME);
    createEAttribute(typeTypeEClass, TYPE_TYPE__REFERENCE);
    createEAttribute(typeTypeEClass, TYPE_TYPE__SUBCLASS);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    XMLTypePackageImpl theXMLTypePackage = (XMLTypePackageImpl)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

    // Add supertypes to classes

    // Initialize classes and features; add operations and parameters
    initEClass(componentDependsTypeEClass, ComponentDependsType.class, "ComponentDependsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getComponentDependsType_ComponentRef(), this.getComponentRefType(), null, "componentRef", null, 0, -1, ComponentDependsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getComponentDependsType_Unrestricted(), theXMLTypePackage.getBoolean(), "unrestricted", "false", 0, 1, ComponentDependsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(componentRefTypeEClass, ComponentRefType.class, "ComponentRefType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getComponentRefType_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, ComponentRefType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(componentTypeEClass, ComponentType.class, "ComponentType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getComponentType_Plugin(), this.getPluginType(), null, "plugin", null, 0, -1, ComponentType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getComponentType_Package(), this.getPackageType(), null, "package", null, 0, -1, ComponentType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getComponentType_ComponentDepends(), this.getComponentDependsType(), null, "componentDepends", null, 1, 1, ComponentType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getComponentType_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, ComponentType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_Component(), this.getComponentType(), null, "component", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_ComponentDepends(), this.getComponentDependsType(), null, "componentDepends", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_ComponentRef(), this.getComponentRefType(), null, "componentRef", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_Package(), this.getPackageType(), null, "package", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_Plugin(), this.getPluginType(), null, "plugin", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_Type(), this.getTypeType(), null, "type", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

    initEClass(packageTypeEClass, PackageType.class, "PackageType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getPackageType_Type(), this.getTypeType(), null, "type", null, 0, -1, PackageType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPackageType_Api(), theXMLTypePackage.getBoolean(), "api", "true", 0, 1, PackageType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPackageType_Exclusive(), theXMLTypePackage.getBoolean(), "exclusive", "true", 0, 1, PackageType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPackageType_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, PackageType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(pluginTypeEClass, PluginType.class, "PluginType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getPluginType_Fragment(), theXMLTypePackage.getBoolean(), "fragment", "false", 0, 1, PluginType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPluginType_Id(), theXMLTypePackage.getString(), "id", null, 1, 1, PluginType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(typeTypeEClass, TypeType.class, "TypeType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getTypeType_Implement(), theXMLTypePackage.getBoolean(), "implement", "true", 0, 1, TypeType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTypeType_Instantiate(), theXMLTypePackage.getBoolean(), "instantiate", "true", 0, 1, TypeType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTypeType_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, TypeType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTypeType_Reference(), theXMLTypePackage.getBoolean(), "reference", "true", 0, 1, TypeType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTypeType_Subclass(), theXMLTypePackage.getBoolean(), "subclass", "true", 0, 1, TypeType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);

    // Create annotations
    // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
    createExtendedMetaDataAnnotations();
  }

  /**
   * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createExtendedMetaDataAnnotations()
  {
    String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";			
    addAnnotation
      (componentDependsTypeEClass, 
       source, 
       new String[] 
       {
       "name", "component-depends_._type",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getComponentDependsType_ComponentRef(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "component-ref",
       "namespace", "##targetNamespace"
       });			
    addAnnotation
      (getComponentDependsType_Unrestricted(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "unrestricted"
       });		
    addAnnotation
      (componentRefTypeEClass, 
       source, 
       new String[] 
       {
       "name", "component-ref_._type",
       "kind", "empty"
       });			
    addAnnotation
      (getComponentRefType_Name(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "name"
       });		
    addAnnotation
      (componentTypeEClass, 
       source, 
       new String[] 
       {
       "name", "component_._type",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getComponentType_Plugin(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "plugin",
       "namespace", "##targetNamespace"
       });		
    addAnnotation
      (getComponentType_Package(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "package",
       "namespace", "##targetNamespace"
       });		
    addAnnotation
      (getComponentType_ComponentDepends(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "component-depends",
       "namespace", "##targetNamespace"
       });			
    addAnnotation
      (getComponentType_Name(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "name"
       });		
    addAnnotation
      (documentRootEClass, 
       source, 
       new String[] 
       {
       "name", "",
       "kind", "mixed"
       });		
    addAnnotation
      (getDocumentRoot_Mixed(), 
       source, 
       new String[] 
       {
       "kind", "elementWildcard",
       "name", ":mixed"
       });		
    addAnnotation
      (getDocumentRoot_XMLNSPrefixMap(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "xmlns:prefix"
       });		
    addAnnotation
      (getDocumentRoot_XSISchemaLocation(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "xsi:schemaLocation"
       });			
    addAnnotation
      (getDocumentRoot_Component(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "component",
       "namespace", "##targetNamespace"
       });		
    addAnnotation
      (getDocumentRoot_ComponentDepends(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "component-depends",
       "namespace", "##targetNamespace"
       });		
    addAnnotation
      (getDocumentRoot_ComponentRef(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "component-ref",
       "namespace", "##targetNamespace"
       });			
    addAnnotation
      (getDocumentRoot_Package(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "package",
       "namespace", "##targetNamespace"
       });			
    addAnnotation
      (getDocumentRoot_Plugin(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "plugin",
       "namespace", "##targetNamespace"
       });			
    addAnnotation
      (getDocumentRoot_Type(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "type",
       "namespace", "##targetNamespace"
       });		
    addAnnotation
      (packageTypeEClass, 
       source, 
       new String[] 
       {
       "name", "package_._type",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getPackageType_Type(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "type",
       "namespace", "##targetNamespace"
       });			
    addAnnotation
      (getPackageType_Api(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "api"
       });			
    addAnnotation
      (getPackageType_Exclusive(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "exclusive"
       });			
    addAnnotation
      (getPackageType_Name(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "name"
       });		
    addAnnotation
      (pluginTypeEClass, 
       source, 
       new String[] 
       {
       "name", "plugin_._type",
       "kind", "empty"
       });			
    addAnnotation
      (getPluginType_Fragment(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "fragment"
       });			
    addAnnotation
      (getPluginType_Id(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "id"
       });		
    addAnnotation
      (typeTypeEClass, 
       source, 
       new String[] 
       {
       "name", "type_._type",
       "kind", "empty"
       });			
    addAnnotation
      (getTypeType_Implement(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "implement"
       });			
    addAnnotation
      (getTypeType_Instantiate(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "instantiate"
       });			
    addAnnotation
      (getTypeType_Name(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "name"
       });			
    addAnnotation
      (getTypeType_Reference(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "reference"
       });			
    addAnnotation
      (getTypeType_Subclass(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "subclass"
       });
  }

} //ComponentPackageImpl
