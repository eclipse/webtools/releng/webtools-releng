/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentType.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.component.ComponentType#getPlugin <em>Plugin</em>}</li>
 *   <li>{@link org.eclipse.component.ComponentType#getPackage <em>Package</em>}</li>
 *   <li>{@link org.eclipse.component.ComponentType#getComponentDepends <em>Component Depends</em>}</li>
 *   <li>{@link org.eclipse.component.ComponentType#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.component.ComponentPackage#getComponentType()
 * @model 
 * @generated
 */
public interface ComponentType extends EObject
{
  /**
   * Returns the value of the '<em><b>Plugin</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.component.PluginType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Plugin</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Plugin</em>' containment reference list.
   * @see org.eclipse.component.ComponentPackage#getComponentType_Plugin()
   * @model type="org.eclipse.component.PluginType" containment="true" resolveProxies="false"
   * @generated
   */
  EList getPlugin();

  /**
   * Returns the value of the '<em><b>Package</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.component.PackageType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Package</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Package</em>' containment reference list.
   * @see org.eclipse.component.ComponentPackage#getComponentType_Package()
   * @model type="org.eclipse.component.PackageType" containment="true" resolveProxies="false"
   * @generated
   */
  EList getPackage();

  /**
   * Returns the value of the '<em><b>Component Depends</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Component Depends</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Component Depends</em>' containment reference.
   * @see #setComponentDepends(ComponentDependsType)
   * @see org.eclipse.component.ComponentPackage#getComponentType_ComponentDepends()
   * @model containment="true" resolveProxies="false" required="true"
   * @generated
   */
  ComponentDependsType getComponentDepends();

  /**
   * Sets the value of the '{@link org.eclipse.component.ComponentType#getComponentDepends <em>Component Depends</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Component Depends</em>' containment reference.
   * @see #getComponentDepends()
   * @generated
   */
  void setComponentDepends(ComponentDependsType value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						The component name; e.g. "Eclipse Platform
   * 						Generic Workbench"; note that this name is used
   * 						to refer to the component and distinguish it
   * 						from other components (but otherwise has no
   * 						official status in Eclipse ontology)
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.eclipse.component.ComponentPackage#getComponentType_Name()
   * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.eclipse.component.ComponentType#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // ComponentType
