/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: TypeType.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.component.TypeType#isImplement <em>Implement</em>}</li>
 *   <li>{@link org.eclipse.component.TypeType#isInstantiate <em>Instantiate</em>}</li>
 *   <li>{@link org.eclipse.component.TypeType#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.component.TypeType#isReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.component.TypeType#isSubclass <em>Subclass</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.component.ComponentPackage#getTypeType()
 * @model 
 * @generated
 */
public interface TypeType extends EObject
{
  /**
   * Returns the value of the '<em><b>Implement</b></em>' attribute.
   * The default value is <code>"true"</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						States whether other components are expected to
   * 						declare a class that implements this interface
   * 						(default: true); specify "false" for an
   * 						interface that other components are not supposed
   * 						to implement directly; this attribute is ignored
   * 						for classes, enumerations, and annotation types,
   * 						none of which can be meaningfully implemented.
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Implement</em>' attribute.
   * @see #isSetImplement()
   * @see #unsetImplement()
   * @see #setImplement(boolean)
   * @see org.eclipse.component.ComponentPackage#getTypeType_Implement()
   * @model default="true" unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isImplement();

  /**
   * Sets the value of the '{@link org.eclipse.component.TypeType#isImplement <em>Implement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Implement</em>' attribute.
   * @see #isSetImplement()
   * @see #unsetImplement()
   * @see #isImplement()
   * @generated
   */
  void setImplement(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.TypeType#isImplement <em>Implement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetImplement()
   * @see #isImplement()
   * @see #setImplement(boolean)
   * @generated
   */
  void unsetImplement();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.TypeType#isImplement <em>Implement</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Implement</em>' attribute is set.
   * @see #unsetImplement()
   * @see #isImplement()
   * @see #setImplement(boolean)
   * @generated
   */
  boolean isSetImplement();

  /**
   * Returns the value of the '<em><b>Instantiate</b></em>' attribute.
   * The default value is <code>"true"</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						States whether other components are expected to
   * 						create instances of this class or annotation
   * 						type (default: true); specify "false" for a type
   * 						that other components are not supposed to
   * 						instantiate directly; this attribute is ignored
   * 						for interfaces and enumerations, neither of
   * 						which can be meaningfully instantiated; this
   * 						attribute is moot for classes that are declared
   * 						final (or ones with no generally accessible
   * 						constructors), since the Java compiler and JRE
   * 						will block outside attempts to instantiate.
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Instantiate</em>' attribute.
   * @see #isSetInstantiate()
   * @see #unsetInstantiate()
   * @see #setInstantiate(boolean)
   * @see org.eclipse.component.ComponentPackage#getTypeType_Instantiate()
   * @model default="true" unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isInstantiate();

  /**
   * Sets the value of the '{@link org.eclipse.component.TypeType#isInstantiate <em>Instantiate</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Instantiate</em>' attribute.
   * @see #isSetInstantiate()
   * @see #unsetInstantiate()
   * @see #isInstantiate()
   * @generated
   */
  void setInstantiate(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.TypeType#isInstantiate <em>Instantiate</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetInstantiate()
   * @see #isInstantiate()
   * @see #setInstantiate(boolean)
   * @generated
   */
  void unsetInstantiate();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.TypeType#isInstantiate <em>Instantiate</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Instantiate</em>' attribute is set.
   * @see #unsetInstantiate()
   * @see #isInstantiate()
   * @see #setInstantiate(boolean)
   * @generated
   */
  boolean isSetInstantiate();

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						Simple name of a top-level Java class,
   * 						interface, enumeration, or annotation type;
   * 						e.g., "String", "IResource".
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.eclipse.component.ComponentPackage#getTypeType_Name()
   * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.eclipse.component.TypeType#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Reference</b></em>' attribute.
   * The default value is <code>"true"</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						States whether other components are expected to
   * 						reference this type by name (default: true);
   * 						specify "false" to indicate that the type is
   * 						internal.
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Reference</em>' attribute.
   * @see #isSetReference()
   * @see #unsetReference()
   * @see #setReference(boolean)
   * @see org.eclipse.component.ComponentPackage#getTypeType_Reference()
   * @model default="true" unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isReference();

  /**
   * Sets the value of the '{@link org.eclipse.component.TypeType#isReference <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reference</em>' attribute.
   * @see #isSetReference()
   * @see #unsetReference()
   * @see #isReference()
   * @generated
   */
  void setReference(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.TypeType#isReference <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetReference()
   * @see #isReference()
   * @see #setReference(boolean)
   * @generated
   */
  void unsetReference();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.TypeType#isReference <em>Reference</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Reference</em>' attribute is set.
   * @see #unsetReference()
   * @see #isReference()
   * @see #setReference(boolean)
   * @generated
   */
  boolean isSetReference();

  /**
   * Returns the value of the '<em><b>Subclass</b></em>' attribute.
   * The default value is <code>"true"</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						States whether other components are expected to
   * 						declare a class that directly subclasses this
   * 						class (default: true); specify "false" for a
   * 						class that other components are not supposed to
   * 						subclass directly; this attribute is ignored for
   * 						interfaces, enumerations, and annotation types,
   * 						none of which can be meaningfully subclassed.
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Subclass</em>' attribute.
   * @see #isSetSubclass()
   * @see #unsetSubclass()
   * @see #setSubclass(boolean)
   * @see org.eclipse.component.ComponentPackage#getTypeType_Subclass()
   * @model default="true" unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isSubclass();

  /**
   * Sets the value of the '{@link org.eclipse.component.TypeType#isSubclass <em>Subclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Subclass</em>' attribute.
   * @see #isSetSubclass()
   * @see #unsetSubclass()
   * @see #isSubclass()
   * @generated
   */
  void setSubclass(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.TypeType#isSubclass <em>Subclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetSubclass()
   * @see #isSubclass()
   * @see #setSubclass(boolean)
   * @generated
   */
  void unsetSubclass();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.TypeType#isSubclass <em>Subclass</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Subclass</em>' attribute is set.
   * @see #unsetSubclass()
   * @see #isSubclass()
   * @see #setSubclass(boolean)
   * @generated
   */
  boolean isSetSubclass();

} // TypeType
