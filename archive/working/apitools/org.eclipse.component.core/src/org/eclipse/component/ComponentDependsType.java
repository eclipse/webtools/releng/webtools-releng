/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentDependsType.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Depends Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.component.ComponentDependsType#getComponentRef <em>Component Ref</em>}</li>
 *   <li>{@link org.eclipse.component.ComponentDependsType#isUnrestricted <em>Unrestricted</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.component.ComponentPackage#getComponentDependsType()
 * @model 
 * @generated
 */
public interface ComponentDependsType extends EObject
{
  /**
   * Returns the value of the '<em><b>Component Ref</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.component.ComponentRefType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Component Ref</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Component Ref</em>' containment reference list.
   * @see org.eclipse.component.ComponentPackage#getComponentDependsType_ComponentRef()
   * @model type="org.eclipse.component.ComponentRefType" containment="true" resolveProxies="false"
   * @generated
   */
  EList getComponentRef();

  /**
   * Returns the value of the '<em><b>Unrestricted</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						States whether this component is allowed to
   * 						depend on arbitrary other components, or just
   * 						the ones explicitly named by the
   * 						&lt;component-ref&gt; children
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Unrestricted</em>' attribute.
   * @see #isSetUnrestricted()
   * @see #unsetUnrestricted()
   * @see #setUnrestricted(boolean)
   * @see org.eclipse.component.ComponentPackage#getComponentDependsType_Unrestricted()
   * @model default="false" unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isUnrestricted();

  /**
   * Sets the value of the '{@link org.eclipse.component.ComponentDependsType#isUnrestricted <em>Unrestricted</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Unrestricted</em>' attribute.
   * @see #isSetUnrestricted()
   * @see #unsetUnrestricted()
   * @see #isUnrestricted()
   * @generated
   */
  void setUnrestricted(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.ComponentDependsType#isUnrestricted <em>Unrestricted</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetUnrestricted()
   * @see #isUnrestricted()
   * @see #setUnrestricted(boolean)
   * @generated
   */
  void unsetUnrestricted();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.ComponentDependsType#isUnrestricted <em>Unrestricted</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Unrestricted</em>' attribute is set.
   * @see #unsetUnrestricted()
   * @see #isUnrestricted()
   * @see #setUnrestricted(boolean)
   * @generated
   */
  boolean isSetUnrestricted();

} // ComponentDependsType
