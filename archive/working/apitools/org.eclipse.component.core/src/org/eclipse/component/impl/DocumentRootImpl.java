/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: DocumentRootImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.impl;

import java.util.Collection;

import org.eclipse.component.ComponentDependsType;
import org.eclipse.component.ComponentPackage;
import org.eclipse.component.ComponentRefType;
import org.eclipse.component.ComponentType;
import org.eclipse.component.DocumentRoot;
import org.eclipse.component.PackageType;
import org.eclipse.component.PluginType;
import org.eclipse.component.TypeType;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.component.impl.DocumentRootImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link org.eclipse.component.impl.DocumentRootImpl#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link org.eclipse.component.impl.DocumentRootImpl#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link org.eclipse.component.impl.DocumentRootImpl#getComponent <em>Component</em>}</li>
 *   <li>{@link org.eclipse.component.impl.DocumentRootImpl#getComponentDepends <em>Component Depends</em>}</li>
 *   <li>{@link org.eclipse.component.impl.DocumentRootImpl#getComponentRef <em>Component Ref</em>}</li>
 *   <li>{@link org.eclipse.component.impl.DocumentRootImpl#getPackage <em>Package</em>}</li>
 *   <li>{@link org.eclipse.component.impl.DocumentRootImpl#getPlugin <em>Plugin</em>}</li>
 *   <li>{@link org.eclipse.component.impl.DocumentRootImpl#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DocumentRootImpl extends EObjectImpl implements DocumentRoot
{
  /**
   * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMixed()
   * @generated
   * @ordered
   */
  protected FeatureMap mixed = null;

  /**
   * The cached value of the '{@link #getXMLNSPrefixMap() <em>XMLNS Prefix Map</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getXMLNSPrefixMap()
   * @generated
   * @ordered
   */
  protected EMap xMLNSPrefixMap = null;

  /**
   * The cached value of the '{@link #getXSISchemaLocation() <em>XSI Schema Location</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getXSISchemaLocation()
   * @generated
   * @ordered
   */
  protected EMap xSISchemaLocation = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DocumentRootImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EClass eStaticClass()
  {
    return ComponentPackage.eINSTANCE.getDocumentRoot();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FeatureMap getMixed()
  {
    if (mixed == null)
    {
      mixed = new BasicFeatureMap(this, ComponentPackage.DOCUMENT_ROOT__MIXED);
    }
    return mixed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap getXMLNSPrefixMap()
  {
    if (xMLNSPrefixMap == null)
    {
      xMLNSPrefixMap = new EcoreEMap(EcorePackage.eINSTANCE.getEStringToStringMapEntry(), EStringToStringMapEntryImpl.class, this, ComponentPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
    }
    return xMLNSPrefixMap;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap getXSISchemaLocation()
  {
    if (xSISchemaLocation == null)
    {
      xSISchemaLocation = new EcoreEMap(EcorePackage.eINSTANCE.getEStringToStringMapEntry(), EStringToStringMapEntryImpl.class, this, ComponentPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
    }
    return xSISchemaLocation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentType getComponent()
  {
    return (ComponentType)getMixed().get(ComponentPackage.eINSTANCE.getDocumentRoot_Component(), true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetComponent(ComponentType newComponent, NotificationChain msgs)
  {
    return ((FeatureMap.Internal)getMixed()).basicAdd(ComponentPackage.eINSTANCE.getDocumentRoot_Component(), newComponent, null);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComponent(ComponentType newComponent)
  {
    ((FeatureMap.Internal)getMixed()).set(ComponentPackage.eINSTANCE.getDocumentRoot_Component(), newComponent);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentDependsType getComponentDepends()
  {
    return (ComponentDependsType)getMixed().get(ComponentPackage.eINSTANCE.getDocumentRoot_ComponentDepends(), true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetComponentDepends(ComponentDependsType newComponentDepends, NotificationChain msgs)
  {
    return ((FeatureMap.Internal)getMixed()).basicAdd(ComponentPackage.eINSTANCE.getDocumentRoot_ComponentDepends(), newComponentDepends, null);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComponentDepends(ComponentDependsType newComponentDepends)
  {
    ((FeatureMap.Internal)getMixed()).set(ComponentPackage.eINSTANCE.getDocumentRoot_ComponentDepends(), newComponentDepends);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentRefType getComponentRef()
  {
    return (ComponentRefType)getMixed().get(ComponentPackage.eINSTANCE.getDocumentRoot_ComponentRef(), true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetComponentRef(ComponentRefType newComponentRef, NotificationChain msgs)
  {
    return ((FeatureMap.Internal)getMixed()).basicAdd(ComponentPackage.eINSTANCE.getDocumentRoot_ComponentRef(), newComponentRef, null);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComponentRef(ComponentRefType newComponentRef)
  {
    ((FeatureMap.Internal)getMixed()).set(ComponentPackage.eINSTANCE.getDocumentRoot_ComponentRef(), newComponentRef);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PackageType getPackage()
  {
    return (PackageType)getMixed().get(ComponentPackage.eINSTANCE.getDocumentRoot_Package(), true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPackage(PackageType newPackage, NotificationChain msgs)
  {
    return ((FeatureMap.Internal)getMixed()).basicAdd(ComponentPackage.eINSTANCE.getDocumentRoot_Package(), newPackage, null);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPackage(PackageType newPackage)
  {
    ((FeatureMap.Internal)getMixed()).set(ComponentPackage.eINSTANCE.getDocumentRoot_Package(), newPackage);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PluginType getPlugin()
  {
    return (PluginType)getMixed().get(ComponentPackage.eINSTANCE.getDocumentRoot_Plugin(), true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPlugin(PluginType newPlugin, NotificationChain msgs)
  {
    return ((FeatureMap.Internal)getMixed()).basicAdd(ComponentPackage.eINSTANCE.getDocumentRoot_Plugin(), newPlugin, null);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPlugin(PluginType newPlugin)
  {
    ((FeatureMap.Internal)getMixed()).set(ComponentPackage.eINSTANCE.getDocumentRoot_Plugin(), newPlugin);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeType getType()
  {
    return (TypeType)getMixed().get(ComponentPackage.eINSTANCE.getDocumentRoot_Type(), true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(TypeType newType, NotificationChain msgs)
  {
    return ((FeatureMap.Internal)getMixed()).basicAdd(ComponentPackage.eINSTANCE.getDocumentRoot_Type(), newType, null);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(TypeType newType)
  {
    ((FeatureMap.Internal)getMixed()).set(ComponentPackage.eINSTANCE.getDocumentRoot_Type(), newType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, Class baseClass, NotificationChain msgs)
  {
    if (featureID >= 0)
    {
      switch (eDerivedStructuralFeatureID(featureID, baseClass))
      {
        case ComponentPackage.DOCUMENT_ROOT__MIXED:
          return ((InternalEList)getMixed()).basicRemove(otherEnd, msgs);
        case ComponentPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
          return ((InternalEList)getXMLNSPrefixMap()).basicRemove(otherEnd, msgs);
        case ComponentPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
          return ((InternalEList)getXSISchemaLocation()).basicRemove(otherEnd, msgs);
        case ComponentPackage.DOCUMENT_ROOT__COMPONENT:
          return basicSetComponent(null, msgs);
        case ComponentPackage.DOCUMENT_ROOT__COMPONENT_DEPENDS:
          return basicSetComponentDepends(null, msgs);
        case ComponentPackage.DOCUMENT_ROOT__COMPONENT_REF:
          return basicSetComponentRef(null, msgs);
        case ComponentPackage.DOCUMENT_ROOT__PACKAGE:
          return basicSetPackage(null, msgs);
        case ComponentPackage.DOCUMENT_ROOT__PLUGIN:
          return basicSetPlugin(null, msgs);
        case ComponentPackage.DOCUMENT_ROOT__TYPE:
          return basicSetType(null, msgs);
        default:
          return eDynamicInverseRemove(otherEnd, featureID, baseClass, msgs);
      }
    }
    return eBasicSetContainer(null, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object eGet(EStructuralFeature eFeature, boolean resolve)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.DOCUMENT_ROOT__MIXED:
        return getMixed();
      case ComponentPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
        return getXMLNSPrefixMap();
      case ComponentPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
        return getXSISchemaLocation();
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT:
        return getComponent();
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT_DEPENDS:
        return getComponentDepends();
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT_REF:
        return getComponentRef();
      case ComponentPackage.DOCUMENT_ROOT__PACKAGE:
        return getPackage();
      case ComponentPackage.DOCUMENT_ROOT__PLUGIN:
        return getPlugin();
      case ComponentPackage.DOCUMENT_ROOT__TYPE:
        return getType();
    }
    return eDynamicGet(eFeature, resolve);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eSet(EStructuralFeature eFeature, Object newValue)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.DOCUMENT_ROOT__MIXED:
        getMixed().clear();
        getMixed().addAll((Collection)newValue);
        return;
      case ComponentPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
        getXMLNSPrefixMap().clear();
        getXMLNSPrefixMap().addAll((Collection)newValue);
        return;
      case ComponentPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
        getXSISchemaLocation().clear();
        getXSISchemaLocation().addAll((Collection)newValue);
        return;
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT:
        setComponent((ComponentType)newValue);
        return;
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT_DEPENDS:
        setComponentDepends((ComponentDependsType)newValue);
        return;
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT_REF:
        setComponentRef((ComponentRefType)newValue);
        return;
      case ComponentPackage.DOCUMENT_ROOT__PACKAGE:
        setPackage((PackageType)newValue);
        return;
      case ComponentPackage.DOCUMENT_ROOT__PLUGIN:
        setPlugin((PluginType)newValue);
        return;
      case ComponentPackage.DOCUMENT_ROOT__TYPE:
        setType((TypeType)newValue);
        return;
    }
    eDynamicSet(eFeature, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eUnset(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.DOCUMENT_ROOT__MIXED:
        getMixed().clear();
        return;
      case ComponentPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
        getXMLNSPrefixMap().clear();
        return;
      case ComponentPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
        getXSISchemaLocation().clear();
        return;
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT:
        setComponent((ComponentType)null);
        return;
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT_DEPENDS:
        setComponentDepends((ComponentDependsType)null);
        return;
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT_REF:
        setComponentRef((ComponentRefType)null);
        return;
      case ComponentPackage.DOCUMENT_ROOT__PACKAGE:
        setPackage((PackageType)null);
        return;
      case ComponentPackage.DOCUMENT_ROOT__PLUGIN:
        setPlugin((PluginType)null);
        return;
      case ComponentPackage.DOCUMENT_ROOT__TYPE:
        setType((TypeType)null);
        return;
    }
    eDynamicUnset(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean eIsSet(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.DOCUMENT_ROOT__MIXED:
        return mixed != null && !mixed.isEmpty();
      case ComponentPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
        return xMLNSPrefixMap != null && !xMLNSPrefixMap.isEmpty();
      case ComponentPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
        return xSISchemaLocation != null && !xSISchemaLocation.isEmpty();
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT:
        return getComponent() != null;
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT_DEPENDS:
        return getComponentDepends() != null;
      case ComponentPackage.DOCUMENT_ROOT__COMPONENT_REF:
        return getComponentRef() != null;
      case ComponentPackage.DOCUMENT_ROOT__PACKAGE:
        return getPackage() != null;
      case ComponentPackage.DOCUMENT_ROOT__PLUGIN:
        return getPlugin() != null;
      case ComponentPackage.DOCUMENT_ROOT__TYPE:
        return getType() != null;
    }
    return eDynamicIsSet(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (mixed: ");
    result.append(mixed);
    result.append(')');
    return result.toString();
  }

} //DocumentRootImpl
