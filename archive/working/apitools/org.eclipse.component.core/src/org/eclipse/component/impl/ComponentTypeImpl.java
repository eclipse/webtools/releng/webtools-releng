/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentTypeImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.impl;

import java.util.Collection;

import org.eclipse.component.ComponentDependsType;
import org.eclipse.component.ComponentPackage;
import org.eclipse.component.ComponentType;
import org.eclipse.component.PackageType;
import org.eclipse.component.PluginType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.component.impl.ComponentTypeImpl#getPlugin <em>Plugin</em>}</li>
 *   <li>{@link org.eclipse.component.impl.ComponentTypeImpl#getPackage <em>Package</em>}</li>
 *   <li>{@link org.eclipse.component.impl.ComponentTypeImpl#getComponentDepends <em>Component Depends</em>}</li>
 *   <li>{@link org.eclipse.component.impl.ComponentTypeImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComponentTypeImpl extends EObjectImpl implements ComponentType
{
  /**
   * The cached value of the '{@link #getPlugin() <em>Plugin</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPlugin()
   * @generated
   * @ordered
   */
  protected EList plugin = null;

  /**
   * The cached value of the '{@link #getPackage() <em>Package</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPackage()
   * @generated
   * @ordered
   */
  protected EList package_ = null;

  /**
   * The cached value of the '{@link #getComponentDepends() <em>Component Depends</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComponentDepends()
   * @generated
   * @ordered
   */
  protected ComponentDependsType componentDepends = null;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComponentTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EClass eStaticClass()
  {
    return ComponentPackage.eINSTANCE.getComponentType();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList getPlugin()
  {
    if (plugin == null)
    {
      plugin = new EObjectContainmentEList(PluginType.class, this, ComponentPackage.COMPONENT_TYPE__PLUGIN);
    }
    return plugin;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList getPackage()
  {
    if (package_ == null)
    {
      package_ = new EObjectContainmentEList(PackageType.class, this, ComponentPackage.COMPONENT_TYPE__PACKAGE);
    }
    return package_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentDependsType getComponentDepends()
  {
    return componentDepends;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetComponentDepends(ComponentDependsType newComponentDepends, NotificationChain msgs)
  {
    ComponentDependsType oldComponentDepends = componentDepends;
    componentDepends = newComponentDepends;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ComponentPackage.COMPONENT_TYPE__COMPONENT_DEPENDS, oldComponentDepends, newComponentDepends);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComponentDepends(ComponentDependsType newComponentDepends)
  {
    if (newComponentDepends != componentDepends)
    {
      NotificationChain msgs = null;
      if (componentDepends != null)
        msgs = ((InternalEObject)componentDepends).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ComponentPackage.COMPONENT_TYPE__COMPONENT_DEPENDS, null, msgs);
      if (newComponentDepends != null)
        msgs = ((InternalEObject)newComponentDepends).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ComponentPackage.COMPONENT_TYPE__COMPONENT_DEPENDS, null, msgs);
      msgs = basicSetComponentDepends(newComponentDepends, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComponentPackage.COMPONENT_TYPE__COMPONENT_DEPENDS, newComponentDepends, newComponentDepends));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComponentPackage.COMPONENT_TYPE__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, Class baseClass, NotificationChain msgs)
  {
    if (featureID >= 0)
    {
      switch (eDerivedStructuralFeatureID(featureID, baseClass))
      {
        case ComponentPackage.COMPONENT_TYPE__PLUGIN:
          return ((InternalEList)getPlugin()).basicRemove(otherEnd, msgs);
        case ComponentPackage.COMPONENT_TYPE__PACKAGE:
          return ((InternalEList)getPackage()).basicRemove(otherEnd, msgs);
        case ComponentPackage.COMPONENT_TYPE__COMPONENT_DEPENDS:
          return basicSetComponentDepends(null, msgs);
        default:
          return eDynamicInverseRemove(otherEnd, featureID, baseClass, msgs);
      }
    }
    return eBasicSetContainer(null, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object eGet(EStructuralFeature eFeature, boolean resolve)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.COMPONENT_TYPE__PLUGIN:
        return getPlugin();
      case ComponentPackage.COMPONENT_TYPE__PACKAGE:
        return getPackage();
      case ComponentPackage.COMPONENT_TYPE__COMPONENT_DEPENDS:
        return getComponentDepends();
      case ComponentPackage.COMPONENT_TYPE__NAME:
        return getName();
    }
    return eDynamicGet(eFeature, resolve);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eSet(EStructuralFeature eFeature, Object newValue)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.COMPONENT_TYPE__PLUGIN:
        getPlugin().clear();
        getPlugin().addAll((Collection)newValue);
        return;
      case ComponentPackage.COMPONENT_TYPE__PACKAGE:
        getPackage().clear();
        getPackage().addAll((Collection)newValue);
        return;
      case ComponentPackage.COMPONENT_TYPE__COMPONENT_DEPENDS:
        setComponentDepends((ComponentDependsType)newValue);
        return;
      case ComponentPackage.COMPONENT_TYPE__NAME:
        setName((String)newValue);
        return;
    }
    eDynamicSet(eFeature, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eUnset(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.COMPONENT_TYPE__PLUGIN:
        getPlugin().clear();
        return;
      case ComponentPackage.COMPONENT_TYPE__PACKAGE:
        getPackage().clear();
        return;
      case ComponentPackage.COMPONENT_TYPE__COMPONENT_DEPENDS:
        setComponentDepends((ComponentDependsType)null);
        return;
      case ComponentPackage.COMPONENT_TYPE__NAME:
        setName(NAME_EDEFAULT);
        return;
    }
    eDynamicUnset(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean eIsSet(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.COMPONENT_TYPE__PLUGIN:
        return plugin != null && !plugin.isEmpty();
      case ComponentPackage.COMPONENT_TYPE__PACKAGE:
        return package_ != null && !package_.isEmpty();
      case ComponentPackage.COMPONENT_TYPE__COMPONENT_DEPENDS:
        return componentDepends != null;
      case ComponentPackage.COMPONENT_TYPE__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
    }
    return eDynamicIsSet(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //ComponentTypeImpl
