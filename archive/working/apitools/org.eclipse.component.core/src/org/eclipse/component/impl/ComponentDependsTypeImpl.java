/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentDependsTypeImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.impl;

import java.util.Collection;

import org.eclipse.component.ComponentDependsType;
import org.eclipse.component.ComponentPackage;
import org.eclipse.component.ComponentRefType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Depends Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.component.impl.ComponentDependsTypeImpl#getComponentRef <em>Component Ref</em>}</li>
 *   <li>{@link org.eclipse.component.impl.ComponentDependsTypeImpl#isUnrestricted <em>Unrestricted</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComponentDependsTypeImpl extends EObjectImpl implements ComponentDependsType
{
  /**
   * The cached value of the '{@link #getComponentRef() <em>Component Ref</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComponentRef()
   * @generated
   * @ordered
   */
  protected EList componentRef = null;

  /**
   * The default value of the '{@link #isUnrestricted() <em>Unrestricted</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isUnrestricted()
   * @generated
   * @ordered
   */
  protected static final boolean UNRESTRICTED_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isUnrestricted() <em>Unrestricted</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isUnrestricted()
   * @generated
   * @ordered
   */
  protected boolean unrestricted = UNRESTRICTED_EDEFAULT;

  /**
   * This is true if the Unrestricted attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean unrestrictedESet = false;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComponentDependsTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EClass eStaticClass()
  {
    return ComponentPackage.eINSTANCE.getComponentDependsType();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList getComponentRef()
  {
    if (componentRef == null)
    {
      componentRef = new EObjectContainmentEList(ComponentRefType.class, this, ComponentPackage.COMPONENT_DEPENDS_TYPE__COMPONENT_REF);
    }
    return componentRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isUnrestricted()
  {
    return unrestricted;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUnrestricted(boolean newUnrestricted)
  {
    boolean oldUnrestricted = unrestricted;
    unrestricted = newUnrestricted;
    boolean oldUnrestrictedESet = unrestrictedESet;
    unrestrictedESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComponentPackage.COMPONENT_DEPENDS_TYPE__UNRESTRICTED, oldUnrestricted, unrestricted, !oldUnrestrictedESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetUnrestricted()
  {
    boolean oldUnrestricted = unrestricted;
    boolean oldUnrestrictedESet = unrestrictedESet;
    unrestricted = UNRESTRICTED_EDEFAULT;
    unrestrictedESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ComponentPackage.COMPONENT_DEPENDS_TYPE__UNRESTRICTED, oldUnrestricted, UNRESTRICTED_EDEFAULT, oldUnrestrictedESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetUnrestricted()
  {
    return unrestrictedESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, Class baseClass, NotificationChain msgs)
  {
    if (featureID >= 0)
    {
      switch (eDerivedStructuralFeatureID(featureID, baseClass))
      {
        case ComponentPackage.COMPONENT_DEPENDS_TYPE__COMPONENT_REF:
          return ((InternalEList)getComponentRef()).basicRemove(otherEnd, msgs);
        default:
          return eDynamicInverseRemove(otherEnd, featureID, baseClass, msgs);
      }
    }
    return eBasicSetContainer(null, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object eGet(EStructuralFeature eFeature, boolean resolve)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.COMPONENT_DEPENDS_TYPE__COMPONENT_REF:
        return getComponentRef();
      case ComponentPackage.COMPONENT_DEPENDS_TYPE__UNRESTRICTED:
        return isUnrestricted() ? Boolean.TRUE : Boolean.FALSE;
    }
    return eDynamicGet(eFeature, resolve);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eSet(EStructuralFeature eFeature, Object newValue)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.COMPONENT_DEPENDS_TYPE__COMPONENT_REF:
        getComponentRef().clear();
        getComponentRef().addAll((Collection)newValue);
        return;
      case ComponentPackage.COMPONENT_DEPENDS_TYPE__UNRESTRICTED:
        setUnrestricted(((Boolean)newValue).booleanValue());
        return;
    }
    eDynamicSet(eFeature, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eUnset(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.COMPONENT_DEPENDS_TYPE__COMPONENT_REF:
        getComponentRef().clear();
        return;
      case ComponentPackage.COMPONENT_DEPENDS_TYPE__UNRESTRICTED:
        unsetUnrestricted();
        return;
    }
    eDynamicUnset(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean eIsSet(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.COMPONENT_DEPENDS_TYPE__COMPONENT_REF:
        return componentRef != null && !componentRef.isEmpty();
      case ComponentPackage.COMPONENT_DEPENDS_TYPE__UNRESTRICTED:
        return isSetUnrestricted();
    }
    return eDynamicIsSet(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (unrestricted: ");
    if (unrestrictedESet) result.append(unrestricted); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //ComponentDependsTypeImpl
