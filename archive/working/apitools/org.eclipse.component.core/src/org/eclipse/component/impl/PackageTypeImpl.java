/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: PackageTypeImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.impl;

import java.util.Collection;

import org.eclipse.component.ComponentPackage;
import org.eclipse.component.PackageType;
import org.eclipse.component.TypeType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Package Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.component.impl.PackageTypeImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.component.impl.PackageTypeImpl#isApi <em>Api</em>}</li>
 *   <li>{@link org.eclipse.component.impl.PackageTypeImpl#isExclusive <em>Exclusive</em>}</li>
 *   <li>{@link org.eclipse.component.impl.PackageTypeImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PackageTypeImpl extends EObjectImpl implements PackageType
{
  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected EList type = null;

  /**
   * The default value of the '{@link #isApi() <em>Api</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isApi()
   * @generated
   * @ordered
   */
  protected static final boolean API_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isApi() <em>Api</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isApi()
   * @generated
   * @ordered
   */
  protected boolean api = API_EDEFAULT;

  /**
   * This is true if the Api attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean apiESet = false;

  /**
   * The default value of the '{@link #isExclusive() <em>Exclusive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isExclusive()
   * @generated
   * @ordered
   */
  protected static final boolean EXCLUSIVE_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isExclusive() <em>Exclusive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isExclusive()
   * @generated
   * @ordered
   */
  protected boolean exclusive = EXCLUSIVE_EDEFAULT;

  /**
   * This is true if the Exclusive attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean exclusiveESet = false;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PackageTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EClass eStaticClass()
  {
    return ComponentPackage.eINSTANCE.getPackageType();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList getType()
  {
    if (type == null)
    {
      type = new EObjectContainmentEList(TypeType.class, this, ComponentPackage.PACKAGE_TYPE__TYPE);
    }
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isApi()
  {
    return api;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setApi(boolean newApi)
  {
    boolean oldApi = api;
    api = newApi;
    boolean oldApiESet = apiESet;
    apiESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComponentPackage.PACKAGE_TYPE__API, oldApi, api, !oldApiESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetApi()
  {
    boolean oldApi = api;
    boolean oldApiESet = apiESet;
    api = API_EDEFAULT;
    apiESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ComponentPackage.PACKAGE_TYPE__API, oldApi, API_EDEFAULT, oldApiESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetApi()
  {
    return apiESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isExclusive()
  {
    return exclusive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExclusive(boolean newExclusive)
  {
    boolean oldExclusive = exclusive;
    exclusive = newExclusive;
    boolean oldExclusiveESet = exclusiveESet;
    exclusiveESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComponentPackage.PACKAGE_TYPE__EXCLUSIVE, oldExclusive, exclusive, !oldExclusiveESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetExclusive()
  {
    boolean oldExclusive = exclusive;
    boolean oldExclusiveESet = exclusiveESet;
    exclusive = EXCLUSIVE_EDEFAULT;
    exclusiveESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ComponentPackage.PACKAGE_TYPE__EXCLUSIVE, oldExclusive, EXCLUSIVE_EDEFAULT, oldExclusiveESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetExclusive()
  {
    return exclusiveESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComponentPackage.PACKAGE_TYPE__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, Class baseClass, NotificationChain msgs)
  {
    if (featureID >= 0)
    {
      switch (eDerivedStructuralFeatureID(featureID, baseClass))
      {
        case ComponentPackage.PACKAGE_TYPE__TYPE:
          return ((InternalEList)getType()).basicRemove(otherEnd, msgs);
        default:
          return eDynamicInverseRemove(otherEnd, featureID, baseClass, msgs);
      }
    }
    return eBasicSetContainer(null, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object eGet(EStructuralFeature eFeature, boolean resolve)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.PACKAGE_TYPE__TYPE:
        return getType();
      case ComponentPackage.PACKAGE_TYPE__API:
        return isApi() ? Boolean.TRUE : Boolean.FALSE;
      case ComponentPackage.PACKAGE_TYPE__EXCLUSIVE:
        return isExclusive() ? Boolean.TRUE : Boolean.FALSE;
      case ComponentPackage.PACKAGE_TYPE__NAME:
        return getName();
    }
    return eDynamicGet(eFeature, resolve);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eSet(EStructuralFeature eFeature, Object newValue)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.PACKAGE_TYPE__TYPE:
        getType().clear();
        getType().addAll((Collection)newValue);
        return;
      case ComponentPackage.PACKAGE_TYPE__API:
        setApi(((Boolean)newValue).booleanValue());
        return;
      case ComponentPackage.PACKAGE_TYPE__EXCLUSIVE:
        setExclusive(((Boolean)newValue).booleanValue());
        return;
      case ComponentPackage.PACKAGE_TYPE__NAME:
        setName((String)newValue);
        return;
    }
    eDynamicSet(eFeature, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eUnset(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.PACKAGE_TYPE__TYPE:
        getType().clear();
        return;
      case ComponentPackage.PACKAGE_TYPE__API:
        unsetApi();
        return;
      case ComponentPackage.PACKAGE_TYPE__EXCLUSIVE:
        unsetExclusive();
        return;
      case ComponentPackage.PACKAGE_TYPE__NAME:
        setName(NAME_EDEFAULT);
        return;
    }
    eDynamicUnset(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean eIsSet(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.PACKAGE_TYPE__TYPE:
        return type != null && !type.isEmpty();
      case ComponentPackage.PACKAGE_TYPE__API:
        return isSetApi();
      case ComponentPackage.PACKAGE_TYPE__EXCLUSIVE:
        return isSetExclusive();
      case ComponentPackage.PACKAGE_TYPE__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
    }
    return eDynamicIsSet(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (api: ");
    if (apiESet) result.append(api); else result.append("<unset>");
    result.append(", exclusive: ");
    if (exclusiveESet) result.append(exclusive); else result.append("<unset>");
    result.append(", name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //PackageTypeImpl
