/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: TypeTypeImpl.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component.impl;

import org.eclipse.component.ComponentPackage;
import org.eclipse.component.TypeType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.component.impl.TypeTypeImpl#isImplement <em>Implement</em>}</li>
 *   <li>{@link org.eclipse.component.impl.TypeTypeImpl#isInstantiate <em>Instantiate</em>}</li>
 *   <li>{@link org.eclipse.component.impl.TypeTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.component.impl.TypeTypeImpl#isReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.component.impl.TypeTypeImpl#isSubclass <em>Subclass</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TypeTypeImpl extends EObjectImpl implements TypeType
{
  /**
   * The default value of the '{@link #isImplement() <em>Implement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isImplement()
   * @generated
   * @ordered
   */
  protected static final boolean IMPLEMENT_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isImplement() <em>Implement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isImplement()
   * @generated
   * @ordered
   */
  protected boolean implement = IMPLEMENT_EDEFAULT;

  /**
   * This is true if the Implement attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean implementESet = false;

  /**
   * The default value of the '{@link #isInstantiate() <em>Instantiate</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isInstantiate()
   * @generated
   * @ordered
   */
  protected static final boolean INSTANTIATE_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isInstantiate() <em>Instantiate</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isInstantiate()
   * @generated
   * @ordered
   */
  protected boolean instantiate = INSTANTIATE_EDEFAULT;

  /**
   * This is true if the Instantiate attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean instantiateESet = false;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #isReference() <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReference()
   * @generated
   * @ordered
   */
  protected static final boolean REFERENCE_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isReference() <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReference()
   * @generated
   * @ordered
   */
  protected boolean reference = REFERENCE_EDEFAULT;

  /**
   * This is true if the Reference attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean referenceESet = false;

  /**
   * The default value of the '{@link #isSubclass() <em>Subclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSubclass()
   * @generated
   * @ordered
   */
  protected static final boolean SUBCLASS_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isSubclass() <em>Subclass</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSubclass()
   * @generated
   * @ordered
   */
  protected boolean subclass = SUBCLASS_EDEFAULT;

  /**
   * This is true if the Subclass attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean subclassESet = false;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TypeTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EClass eStaticClass()
  {
    return ComponentPackage.eINSTANCE.getTypeType();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isImplement()
  {
    return implement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setImplement(boolean newImplement)
  {
    boolean oldImplement = implement;
    implement = newImplement;
    boolean oldImplementESet = implementESet;
    implementESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComponentPackage.TYPE_TYPE__IMPLEMENT, oldImplement, implement, !oldImplementESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetImplement()
  {
    boolean oldImplement = implement;
    boolean oldImplementESet = implementESet;
    implement = IMPLEMENT_EDEFAULT;
    implementESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ComponentPackage.TYPE_TYPE__IMPLEMENT, oldImplement, IMPLEMENT_EDEFAULT, oldImplementESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetImplement()
  {
    return implementESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isInstantiate()
  {
    return instantiate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInstantiate(boolean newInstantiate)
  {
    boolean oldInstantiate = instantiate;
    instantiate = newInstantiate;
    boolean oldInstantiateESet = instantiateESet;
    instantiateESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComponentPackage.TYPE_TYPE__INSTANTIATE, oldInstantiate, instantiate, !oldInstantiateESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetInstantiate()
  {
    boolean oldInstantiate = instantiate;
    boolean oldInstantiateESet = instantiateESet;
    instantiate = INSTANTIATE_EDEFAULT;
    instantiateESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ComponentPackage.TYPE_TYPE__INSTANTIATE, oldInstantiate, INSTANTIATE_EDEFAULT, oldInstantiateESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetInstantiate()
  {
    return instantiateESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComponentPackage.TYPE_TYPE__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isReference()
  {
    return reference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReference(boolean newReference)
  {
    boolean oldReference = reference;
    reference = newReference;
    boolean oldReferenceESet = referenceESet;
    referenceESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComponentPackage.TYPE_TYPE__REFERENCE, oldReference, reference, !oldReferenceESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetReference()
  {
    boolean oldReference = reference;
    boolean oldReferenceESet = referenceESet;
    reference = REFERENCE_EDEFAULT;
    referenceESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ComponentPackage.TYPE_TYPE__REFERENCE, oldReference, REFERENCE_EDEFAULT, oldReferenceESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetReference()
  {
    return referenceESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSubclass()
  {
    return subclass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubclass(boolean newSubclass)
  {
    boolean oldSubclass = subclass;
    subclass = newSubclass;
    boolean oldSubclassESet = subclassESet;
    subclassESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComponentPackage.TYPE_TYPE__SUBCLASS, oldSubclass, subclass, !oldSubclassESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetSubclass()
  {
    boolean oldSubclass = subclass;
    boolean oldSubclassESet = subclassESet;
    subclass = SUBCLASS_EDEFAULT;
    subclassESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, ComponentPackage.TYPE_TYPE__SUBCLASS, oldSubclass, SUBCLASS_EDEFAULT, oldSubclassESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetSubclass()
  {
    return subclassESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Object eGet(EStructuralFeature eFeature, boolean resolve)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.TYPE_TYPE__IMPLEMENT:
        return isImplement() ? Boolean.TRUE : Boolean.FALSE;
      case ComponentPackage.TYPE_TYPE__INSTANTIATE:
        return isInstantiate() ? Boolean.TRUE : Boolean.FALSE;
      case ComponentPackage.TYPE_TYPE__NAME:
        return getName();
      case ComponentPackage.TYPE_TYPE__REFERENCE:
        return isReference() ? Boolean.TRUE : Boolean.FALSE;
      case ComponentPackage.TYPE_TYPE__SUBCLASS:
        return isSubclass() ? Boolean.TRUE : Boolean.FALSE;
    }
    return eDynamicGet(eFeature, resolve);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eSet(EStructuralFeature eFeature, Object newValue)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.TYPE_TYPE__IMPLEMENT:
        setImplement(((Boolean)newValue).booleanValue());
        return;
      case ComponentPackage.TYPE_TYPE__INSTANTIATE:
        setInstantiate(((Boolean)newValue).booleanValue());
        return;
      case ComponentPackage.TYPE_TYPE__NAME:
        setName((String)newValue);
        return;
      case ComponentPackage.TYPE_TYPE__REFERENCE:
        setReference(((Boolean)newValue).booleanValue());
        return;
      case ComponentPackage.TYPE_TYPE__SUBCLASS:
        setSubclass(((Boolean)newValue).booleanValue());
        return;
    }
    eDynamicSet(eFeature, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void eUnset(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.TYPE_TYPE__IMPLEMENT:
        unsetImplement();
        return;
      case ComponentPackage.TYPE_TYPE__INSTANTIATE:
        unsetInstantiate();
        return;
      case ComponentPackage.TYPE_TYPE__NAME:
        setName(NAME_EDEFAULT);
        return;
      case ComponentPackage.TYPE_TYPE__REFERENCE:
        unsetReference();
        return;
      case ComponentPackage.TYPE_TYPE__SUBCLASS:
        unsetSubclass();
        return;
    }
    eDynamicUnset(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean eIsSet(EStructuralFeature eFeature)
  {
    switch (eDerivedStructuralFeatureID(eFeature))
    {
      case ComponentPackage.TYPE_TYPE__IMPLEMENT:
        return isSetImplement();
      case ComponentPackage.TYPE_TYPE__INSTANTIATE:
        return isSetInstantiate();
      case ComponentPackage.TYPE_TYPE__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case ComponentPackage.TYPE_TYPE__REFERENCE:
        return isSetReference();
      case ComponentPackage.TYPE_TYPE__SUBCLASS:
        return isSetSubclass();
    }
    return eDynamicIsSet(eFeature);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (implement: ");
    if (implementESet) result.append(implement); else result.append("<unset>");
    result.append(", instantiate: ");
    if (instantiateESet) result.append(instantiate); else result.append("<unset>");
    result.append(", name: ");
    result.append(name);
    result.append(", reference: ");
    if (referenceESet) result.append(reference); else result.append("<unset>");
    result.append(", subclass: ");
    if (subclassESet) result.append(subclass); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //TypeTypeImpl
