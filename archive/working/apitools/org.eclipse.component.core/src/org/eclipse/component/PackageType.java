/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: PackageType.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Package Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.component.PackageType#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.component.PackageType#isApi <em>Api</em>}</li>
 *   <li>{@link org.eclipse.component.PackageType#isExclusive <em>Exclusive</em>}</li>
 *   <li>{@link org.eclipse.component.PackageType#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.component.ComponentPackage#getPackageType()
 * @model 
 * @generated
 */
public interface PackageType extends EObject
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.component.TypeType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference list.
   * @see org.eclipse.component.ComponentPackage#getPackageType_Type()
   * @model type="org.eclipse.component.TypeType" containment="true" resolveProxies="false"
   * @generated
   */
  EList getType();

  /**
   * Returns the value of the '<em><b>Api</b></em>' attribute.
   * The default value is <code>"true"</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						States whether this package is reserved for
   * 						exclusive use by this component (default: true);
   * 						specify "false" in (rare) cases where a multiple
   * 						components declared types in the same package.
   * 						Package sharing is only by mutual consent; all
   * 						components involved must explicitly declare the
   * 						package as exclusive="false" (even if it has no
   * 						API types).
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Api</em>' attribute.
   * @see #isSetApi()
   * @see #unsetApi()
   * @see #setApi(boolean)
   * @see org.eclipse.component.ComponentPackage#getPackageType_Api()
   * @model default="true" unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isApi();

  /**
   * Sets the value of the '{@link org.eclipse.component.PackageType#isApi <em>Api</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Api</em>' attribute.
   * @see #isSetApi()
   * @see #unsetApi()
   * @see #isApi()
   * @generated
   */
  void setApi(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.PackageType#isApi <em>Api</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetApi()
   * @see #isApi()
   * @see #setApi(boolean)
   * @generated
   */
  void unsetApi();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.PackageType#isApi <em>Api</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Api</em>' attribute is set.
   * @see #unsetApi()
   * @see #isApi()
   * @see #setApi(boolean)
   * @generated
   */
  boolean isSetApi();

  /**
   * Returns the value of the '<em><b>Exclusive</b></em>' attribute.
   * The default value is <code>"true"</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						States whether top-level types in this package
   * 						are API types by default (default: true);
   * 						specify "false" in order to explicitly list API
   * 						types found in the package.
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Exclusive</em>' attribute.
   * @see #isSetExclusive()
   * @see #unsetExclusive()
   * @see #setExclusive(boolean)
   * @see org.eclipse.component.ComponentPackage#getPackageType_Exclusive()
   * @model default="true" unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isExclusive();

  /**
   * Sets the value of the '{@link org.eclipse.component.PackageType#isExclusive <em>Exclusive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Exclusive</em>' attribute.
   * @see #isSetExclusive()
   * @see #unsetExclusive()
   * @see #isExclusive()
   * @generated
   */
  void setExclusive(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.PackageType#isExclusive <em>Exclusive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetExclusive()
   * @see #isExclusive()
   * @see #setExclusive(boolean)
   * @generated
   */
  void unsetExclusive();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.PackageType#isExclusive <em>Exclusive</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Exclusive</em>' attribute is set.
   * @see #unsetExclusive()
   * @see #isExclusive()
   * @see #setExclusive(boolean)
   * @generated
   */
  boolean isSetExclusive();

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						Java package name; e.g., "javax.swing",
   * 						"org.eclipse.ui".
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.eclipse.component.ComponentPackage#getPackageType_Name()
   * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.eclipse.component.PackageType#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // PackageType
