/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentPackage.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * 
 * 			Each component is described via a component description
 * 			file.
 * 		
 * <!-- end-model-doc -->
 * @see org.eclipse.component.ComponentFactory
 * @generated
 */
public interface ComponentPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "component";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://eclipse.org/component";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "component";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  ComponentPackage eINSTANCE = org.eclipse.component.impl.ComponentPackageImpl.init();

  /**
   * The meta object id for the '{@link org.eclipse.component.impl.ComponentDependsTypeImpl <em>Depends Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.impl.ComponentDependsTypeImpl
   * @see org.eclipse.component.impl.ComponentPackageImpl#getComponentDependsType()
   * @generated
   */
  int COMPONENT_DEPENDS_TYPE = 0;

  /**
   * The feature id for the '<em><b>Component Ref</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DEPENDS_TYPE__COMPONENT_REF = 0;

  /**
   * The feature id for the '<em><b>Unrestricted</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DEPENDS_TYPE__UNRESTRICTED = 1;

  /**
   * The number of structural features of the the '<em>Depends Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_DEPENDS_TYPE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.eclipse.component.impl.ComponentRefTypeImpl <em>Ref Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.impl.ComponentRefTypeImpl
   * @see org.eclipse.component.impl.ComponentPackageImpl#getComponentRefType()
   * @generated
   */
  int COMPONENT_REF_TYPE = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REF_TYPE__NAME = 0;

  /**
   * The number of structural features of the the '<em>Ref Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_REF_TYPE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.eclipse.component.impl.ComponentTypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.impl.ComponentTypeImpl
   * @see org.eclipse.component.impl.ComponentPackageImpl#getComponentType()
   * @generated
   */
  int COMPONENT_TYPE = 2;

  /**
   * The feature id for the '<em><b>Plugin</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_TYPE__PLUGIN = 0;

  /**
   * The feature id for the '<em><b>Package</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_TYPE__PACKAGE = 1;

  /**
   * The feature id for the '<em><b>Component Depends</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_TYPE__COMPONENT_DEPENDS = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_TYPE__NAME = 3;

  /**
   * The number of structural features of the the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_TYPE_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.eclipse.component.impl.DocumentRootImpl <em>Document Root</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.impl.DocumentRootImpl
   * @see org.eclipse.component.impl.ComponentPackageImpl#getDocumentRoot()
   * @generated
   */
  int DOCUMENT_ROOT = 3;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__MIXED = 0;

  /**
   * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

  /**
   * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

  /**
   * The feature id for the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__COMPONENT = 3;

  /**
   * The feature id for the '<em><b>Component Depends</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__COMPONENT_DEPENDS = 4;

  /**
   * The feature id for the '<em><b>Component Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__COMPONENT_REF = 5;

  /**
   * The feature id for the '<em><b>Package</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__PACKAGE = 6;

  /**
   * The feature id for the '<em><b>Plugin</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__PLUGIN = 7;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__TYPE = 8;

  /**
   * The number of structural features of the the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_FEATURE_COUNT = 9;

  /**
   * The meta object id for the '{@link org.eclipse.component.impl.PackageTypeImpl <em>Package Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.impl.PackageTypeImpl
   * @see org.eclipse.component.impl.ComponentPackageImpl#getPackageType()
   * @generated
   */
  int PACKAGE_TYPE = 4;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_TYPE__TYPE = 0;

  /**
   * The feature id for the '<em><b>Api</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_TYPE__API = 1;

  /**
   * The feature id for the '<em><b>Exclusive</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_TYPE__EXCLUSIVE = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_TYPE__NAME = 3;

  /**
   * The number of structural features of the the '<em>Package Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_TYPE_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.eclipse.component.impl.PluginTypeImpl <em>Plugin Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.impl.PluginTypeImpl
   * @see org.eclipse.component.impl.ComponentPackageImpl#getPluginType()
   * @generated
   */
  int PLUGIN_TYPE = 5;

  /**
   * The feature id for the '<em><b>Fragment</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLUGIN_TYPE__FRAGMENT = 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLUGIN_TYPE__ID = 1;

  /**
   * The number of structural features of the the '<em>Plugin Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLUGIN_TYPE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.eclipse.component.impl.TypeTypeImpl <em>Type Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.component.impl.TypeTypeImpl
   * @see org.eclipse.component.impl.ComponentPackageImpl#getTypeType()
   * @generated
   */
  int TYPE_TYPE = 6;

  /**
   * The feature id for the '<em><b>Implement</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_TYPE__IMPLEMENT = 0;

  /**
   * The feature id for the '<em><b>Instantiate</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_TYPE__INSTANTIATE = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_TYPE__NAME = 2;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_TYPE__REFERENCE = 3;

  /**
   * The feature id for the '<em><b>Subclass</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_TYPE__SUBCLASS = 4;

  /**
   * The number of structural features of the the '<em>Type Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_TYPE_FEATURE_COUNT = 5;


  /**
   * Returns the meta object for class '{@link org.eclipse.component.ComponentDependsType <em>Depends Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Depends Type</em>'.
   * @see org.eclipse.component.ComponentDependsType
   * @generated
   */
  EClass getComponentDependsType();

  /**
   * Returns the meta object for the containment reference list '{@link org.eclipse.component.ComponentDependsType#getComponentRef <em>Component Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Component Ref</em>'.
   * @see org.eclipse.component.ComponentDependsType#getComponentRef()
   * @see #getComponentDependsType()
   * @generated
   */
  EReference getComponentDependsType_ComponentRef();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.ComponentDependsType#isUnrestricted <em>Unrestricted</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Unrestricted</em>'.
   * @see org.eclipse.component.ComponentDependsType#isUnrestricted()
   * @see #getComponentDependsType()
   * @generated
   */
  EAttribute getComponentDependsType_Unrestricted();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.ComponentRefType <em>Ref Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ref Type</em>'.
   * @see org.eclipse.component.ComponentRefType
   * @generated
   */
  EClass getComponentRefType();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.ComponentRefType#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.eclipse.component.ComponentRefType#getName()
   * @see #getComponentRefType()
   * @generated
   */
  EAttribute getComponentRefType_Name();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.ComponentType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see org.eclipse.component.ComponentType
   * @generated
   */
  EClass getComponentType();

  /**
   * Returns the meta object for the containment reference list '{@link org.eclipse.component.ComponentType#getPlugin <em>Plugin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Plugin</em>'.
   * @see org.eclipse.component.ComponentType#getPlugin()
   * @see #getComponentType()
   * @generated
   */
  EReference getComponentType_Plugin();

  /**
   * Returns the meta object for the containment reference list '{@link org.eclipse.component.ComponentType#getPackage <em>Package</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Package</em>'.
   * @see org.eclipse.component.ComponentType#getPackage()
   * @see #getComponentType()
   * @generated
   */
  EReference getComponentType_Package();

  /**
   * Returns the meta object for the containment reference '{@link org.eclipse.component.ComponentType#getComponentDepends <em>Component Depends</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component Depends</em>'.
   * @see org.eclipse.component.ComponentType#getComponentDepends()
   * @see #getComponentType()
   * @generated
   */
  EReference getComponentType_ComponentDepends();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.ComponentType#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.eclipse.component.ComponentType#getName()
   * @see #getComponentType()
   * @generated
   */
  EAttribute getComponentType_Name();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document Root</em>'.
   * @see org.eclipse.component.DocumentRoot
   * @generated
   */
  EClass getDocumentRoot();

  /**
   * Returns the meta object for the attribute list '{@link org.eclipse.component.DocumentRoot#getMixed <em>Mixed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Mixed</em>'.
   * @see org.eclipse.component.DocumentRoot#getMixed()
   * @see #getDocumentRoot()
   * @generated
   */
  EAttribute getDocumentRoot_Mixed();

  /**
   * Returns the meta object for the map '{@link org.eclipse.component.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
   * @see org.eclipse.component.DocumentRoot#getXMLNSPrefixMap()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XMLNSPrefixMap();

  /**
   * Returns the meta object for the map '{@link org.eclipse.component.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XSI Schema Location</em>'.
   * @see org.eclipse.component.DocumentRoot#getXSISchemaLocation()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XSISchemaLocation();

  /**
   * Returns the meta object for the containment reference '{@link org.eclipse.component.DocumentRoot#getComponent <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component</em>'.
   * @see org.eclipse.component.DocumentRoot#getComponent()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_Component();

  /**
   * Returns the meta object for the containment reference '{@link org.eclipse.component.DocumentRoot#getComponentDepends <em>Component Depends</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component Depends</em>'.
   * @see org.eclipse.component.DocumentRoot#getComponentDepends()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_ComponentDepends();

  /**
   * Returns the meta object for the containment reference '{@link org.eclipse.component.DocumentRoot#getComponentRef <em>Component Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Component Ref</em>'.
   * @see org.eclipse.component.DocumentRoot#getComponentRef()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_ComponentRef();

  /**
   * Returns the meta object for the containment reference '{@link org.eclipse.component.DocumentRoot#getPackage <em>Package</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Package</em>'.
   * @see org.eclipse.component.DocumentRoot#getPackage()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_Package();

  /**
   * Returns the meta object for the containment reference '{@link org.eclipse.component.DocumentRoot#getPlugin <em>Plugin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Plugin</em>'.
   * @see org.eclipse.component.DocumentRoot#getPlugin()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_Plugin();

  /**
   * Returns the meta object for the containment reference '{@link org.eclipse.component.DocumentRoot#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.eclipse.component.DocumentRoot#getType()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_Type();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.PackageType <em>Package Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Package Type</em>'.
   * @see org.eclipse.component.PackageType
   * @generated
   */
  EClass getPackageType();

  /**
   * Returns the meta object for the containment reference list '{@link org.eclipse.component.PackageType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Type</em>'.
   * @see org.eclipse.component.PackageType#getType()
   * @see #getPackageType()
   * @generated
   */
  EReference getPackageType_Type();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.PackageType#isApi <em>Api</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Api</em>'.
   * @see org.eclipse.component.PackageType#isApi()
   * @see #getPackageType()
   * @generated
   */
  EAttribute getPackageType_Api();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.PackageType#isExclusive <em>Exclusive</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Exclusive</em>'.
   * @see org.eclipse.component.PackageType#isExclusive()
   * @see #getPackageType()
   * @generated
   */
  EAttribute getPackageType_Exclusive();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.PackageType#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.eclipse.component.PackageType#getName()
   * @see #getPackageType()
   * @generated
   */
  EAttribute getPackageType_Name();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.PluginType <em>Plugin Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Plugin Type</em>'.
   * @see org.eclipse.component.PluginType
   * @generated
   */
  EClass getPluginType();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.PluginType#isFragment <em>Fragment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Fragment</em>'.
   * @see org.eclipse.component.PluginType#isFragment()
   * @see #getPluginType()
   * @generated
   */
  EAttribute getPluginType_Fragment();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.PluginType#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see org.eclipse.component.PluginType#getId()
   * @see #getPluginType()
   * @generated
   */
  EAttribute getPluginType_Id();

  /**
   * Returns the meta object for class '{@link org.eclipse.component.TypeType <em>Type Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Type</em>'.
   * @see org.eclipse.component.TypeType
   * @generated
   */
  EClass getTypeType();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.TypeType#isImplement <em>Implement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Implement</em>'.
   * @see org.eclipse.component.TypeType#isImplement()
   * @see #getTypeType()
   * @generated
   */
  EAttribute getTypeType_Implement();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.TypeType#isInstantiate <em>Instantiate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Instantiate</em>'.
   * @see org.eclipse.component.TypeType#isInstantiate()
   * @see #getTypeType()
   * @generated
   */
  EAttribute getTypeType_Instantiate();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.TypeType#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.eclipse.component.TypeType#getName()
   * @see #getTypeType()
   * @generated
   */
  EAttribute getTypeType_Name();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.TypeType#isReference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Reference</em>'.
   * @see org.eclipse.component.TypeType#isReference()
   * @see #getTypeType()
   * @generated
   */
  EAttribute getTypeType_Reference();

  /**
   * Returns the meta object for the attribute '{@link org.eclipse.component.TypeType#isSubclass <em>Subclass</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Subclass</em>'.
   * @see org.eclipse.component.TypeType#isSubclass()
   * @see #getTypeType()
   * @generated
   */
  EAttribute getTypeType_Subclass();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  ComponentFactory getComponentFactory();

} //ComponentPackage
