/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: DocumentRoot.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.component.DocumentRoot#getMixed <em>Mixed</em>}</li>
 *   <li>{@link org.eclipse.component.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link org.eclipse.component.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link org.eclipse.component.DocumentRoot#getComponent <em>Component</em>}</li>
 *   <li>{@link org.eclipse.component.DocumentRoot#getComponentDepends <em>Component Depends</em>}</li>
 *   <li>{@link org.eclipse.component.DocumentRoot#getComponentRef <em>Component Ref</em>}</li>
 *   <li>{@link org.eclipse.component.DocumentRoot#getPackage <em>Package</em>}</li>
 *   <li>{@link org.eclipse.component.DocumentRoot#getPlugin <em>Plugin</em>}</li>
 *   <li>{@link org.eclipse.component.DocumentRoot#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.component.ComponentPackage#getDocumentRoot()
 * @model 
 * @generated
 */
public interface DocumentRoot extends EObject
{
  /**
   * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mixed</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mixed</em>' attribute list.
   * @see org.eclipse.component.ComponentPackage#getDocumentRoot_Mixed()
   * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   * @generated
   */
  FeatureMap getMixed();

  /**
   * Returns the value of the '<em><b>XMLNS Prefix Map</b></em>' map.
   * The key is of type {@link java.lang.String},
   * and the value is of type {@link java.lang.String},
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>XMLNS Prefix Map</em>' map isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>XMLNS Prefix Map</em>' map.
   * @see org.eclipse.component.ComponentPackage#getDocumentRoot_XMLNSPrefixMap()
   * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry" keyType="java.lang.String" valueType="java.lang.String" transient="true"
   * @generated
   */
  EMap getXMLNSPrefixMap();

  /**
   * Returns the value of the '<em><b>XSI Schema Location</b></em>' map.
   * The key is of type {@link java.lang.String},
   * and the value is of type {@link java.lang.String},
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>XSI Schema Location</em>' map isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>XSI Schema Location</em>' map.
   * @see org.eclipse.component.ComponentPackage#getDocumentRoot_XSISchemaLocation()
   * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry" keyType="java.lang.String" valueType="java.lang.String" transient="true"
   * @generated
   */
  EMap getXSISchemaLocation();

  /**
   * Returns the value of the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 				Provides information about a component. Child elements
   * 				of this element describe the set of plug-ins and
   * 				fragments making up the component, and provide
   * 				information about the Java packages and types in the
   * 				component's code.
   * 			
   * <!-- end-model-doc -->
   * @return the value of the '<em>Component</em>' containment reference.
   * @see #setComponent(ComponentType)
   * @see org.eclipse.component.ComponentPackage#getDocumentRoot_Component()
   * @model containment="true" resolveProxies="false" transient="true" volatile="true" derived="true"
   * @generated
   */
  ComponentType getComponent();

  /**
   * Sets the value of the '{@link org.eclipse.component.DocumentRoot#getComponent <em>Component</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Component</em>' containment reference.
   * @see #getComponent()
   * @generated
   */
  void setComponent(ComponentType value);

  /**
   * Returns the value of the '<em><b>Component Depends</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Component Depends</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Component Depends</em>' containment reference.
   * @see #setComponentDepends(ComponentDependsType)
   * @see org.eclipse.component.ComponentPackage#getDocumentRoot_ComponentDepends()
   * @model containment="true" resolveProxies="false" transient="true" volatile="true" derived="true"
   * @generated
   */
  ComponentDependsType getComponentDepends();

  /**
   * Sets the value of the '{@link org.eclipse.component.DocumentRoot#getComponentDepends <em>Component Depends</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Component Depends</em>' containment reference.
   * @see #getComponentDepends()
   * @generated
   */
  void setComponentDepends(ComponentDependsType value);

  /**
   * Returns the value of the '<em><b>Component Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Component Ref</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Component Ref</em>' containment reference.
   * @see #setComponentRef(ComponentRefType)
   * @see org.eclipse.component.ComponentPackage#getDocumentRoot_ComponentRef()
   * @model containment="true" resolveProxies="false" transient="true" volatile="true" derived="true"
   * @generated
   */
  ComponentRefType getComponentRef();

  /**
   * Sets the value of the '{@link org.eclipse.component.DocumentRoot#getComponentRef <em>Component Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Component Ref</em>' containment reference.
   * @see #getComponentRef()
   * @generated
   */
  void setComponentRef(ComponentRefType value);

  /**
   * Returns the value of the '<em><b>Package</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 				Provides information about a package as used by the
   * 				component. In the unusual case where a package is shared
   * 				with other components, the &lt;package&gt; element is
   * 				understood to apply only to the types the component
   * 				actually declares, and has no bearing on the types
   * 				declared in the same package in any other component. The
   * 				list of packages may be incomplete; if the component
   * 				contains code in a package not mentioned in the list,
   * 				the package is considered to be internal (equivalent to
   * 				being explicitly described as &lt;package name="..."
   * 				api="false" /&gt;). The children of the &lt;package&gt;
   * 				element provide information about specific types in the
   * 				package.
   * 			
   * <!-- end-model-doc -->
   * @return the value of the '<em>Package</em>' containment reference.
   * @see #setPackage(PackageType)
   * @see org.eclipse.component.ComponentPackage#getDocumentRoot_Package()
   * @model containment="true" resolveProxies="false" transient="true" volatile="true" derived="true"
   * @generated
   */
  PackageType getPackage();

  /**
   * Sets the value of the '{@link org.eclipse.component.DocumentRoot#getPackage <em>Package</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Package</em>' containment reference.
   * @see #getPackage()
   * @generated
   */
  void setPackage(PackageType value);

  /**
   * Returns the value of the '<em><b>Plugin</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 				Identifies a plug-in or plug-in fragment that is part of
   * 				the component. The list of plug-ins must be complete;
   * 				that is, a component contains a plug-in (or fragment) if
   * 				and only if a &lt;plugin&gt; element occurs as a child
   * 				of the &lt;component&gt; element.
   * 			
   * <!-- end-model-doc -->
   * @return the value of the '<em>Plugin</em>' containment reference.
   * @see #setPlugin(PluginType)
   * @see org.eclipse.component.ComponentPackage#getDocumentRoot_Plugin()
   * @model containment="true" resolveProxies="false" transient="true" volatile="true" derived="true"
   * @generated
   */
  PluginType getPlugin();

  /**
   * Sets the value of the '{@link org.eclipse.component.DocumentRoot#getPlugin <em>Plugin</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Plugin</em>' containment reference.
   * @see #getPlugin()
   * @generated
   */
  void setPlugin(PluginType value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 				Provides information about a top-level type in a
   * 				package. (Note: We could extend the schema in the future
   * 				to allow &lt;type&gt; elements to provide analogous
   * 				information about their members. We could also extend
   * 				the &lt;component&gt; element to allow aspects other
   * 				than code API to be described.)
   * 			
   * <!-- end-model-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(TypeType)
   * @see org.eclipse.component.ComponentPackage#getDocumentRoot_Type()
   * @model containment="true" resolveProxies="false" transient="true" volatile="true" derived="true"
   * @generated
   */
  TypeType getType();

  /**
   * Sets the value of the '{@link org.eclipse.component.DocumentRoot#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(TypeType value);

} // DocumentRoot
