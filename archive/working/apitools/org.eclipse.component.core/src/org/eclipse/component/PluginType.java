/*******************************************************************************
 * Copyright (c) 2005, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id: PluginType.java,v 1.1 2005/01/20 12:03:03 ryman Exp $
 */
package org.eclipse.component;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plugin Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.component.PluginType#isFragment <em>Fragment</em>}</li>
 *   <li>{@link org.eclipse.component.PluginType#getId <em>Id</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.component.ComponentPackage#getPluginType()
 * @model 
 * @generated
 */
public interface PluginType extends EObject
{
  /**
   * Returns the value of the '<em><b>Fragment</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						State whether this is a plug-in fragment as
   * 						opposed to a plug-in (default: plug-in).
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Fragment</em>' attribute.
   * @see #isSetFragment()
   * @see #unsetFragment()
   * @see #setFragment(boolean)
   * @see org.eclipse.component.ComponentPackage#getPluginType_Fragment()
   * @model default="false" unique="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
   * @generated
   */
  boolean isFragment();

  /**
   * Sets the value of the '{@link org.eclipse.component.PluginType#isFragment <em>Fragment</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fragment</em>' attribute.
   * @see #isSetFragment()
   * @see #unsetFragment()
   * @see #isFragment()
   * @generated
   */
  void setFragment(boolean value);

  /**
   * Unsets the value of the '{@link org.eclipse.component.PluginType#isFragment <em>Fragment</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetFragment()
   * @see #isFragment()
   * @see #setFragment(boolean)
   * @generated
   */
  void unsetFragment();

  /**
   * Returns whether the value of the '{@link org.eclipse.component.PluginType#isFragment <em>Fragment</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Fragment</em>' attribute is set.
   * @see #unsetFragment()
   * @see #isFragment()
   * @see #setFragment(boolean)
   * @generated
   */
  boolean isSetFragment();

  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						The plug-in id or plug-in fragment id; e.g.,
   * 						"org.eclipse.core.resources"; note that in the
   * 						case of a fragment, this is the id of fragment
   * 						itself.
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see org.eclipse.component.ComponentPackage#getPluginType_Id()
   * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link org.eclipse.component.PluginType#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

} // PluginType
