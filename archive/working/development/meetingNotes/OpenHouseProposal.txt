5/10/2005 - draft 

These meetings will be audio and eMeeting format, with
the component leads presenting and leaving plenty of time for questions.

Part of this purpose of these meetings is to have a "friendly forum" for 
information sharing -- to educate and listen to "interested parties"
in WTP (and also Eclipse Members who may be trying to build on top of WTP, etc.). I'm sure 
there could be problem issues raised, and in true open forum spirit, we'll try to openly listen 
to all forms of comments, and capture them in "issue" bugzillas (but these meetings
are not to resolve any problems or do "on line design").

The target audience is technical developers building on WTP 
(not end users just using it, and not "product managers" wanting 
to plan their product cycles, etc., there will be other 
avenues for those auidences).

Another purpose of these meeting is to prepare steadily towards our 
EMO required Release Review. The following link contains an EMO 
review outline. Ours is expected to be around July 6th, with prep on June 21 and 22. 
So, as you think of your target audience, think of it mostly as Tim Wagner, so
he can do a good job of accurately representing your component during these reviews :)
See
http://www.eclipse.org/org/processes/release-review.html


I suggest your charts/presentations cover 3 main areas:
(the detail is just recommendations, each component/presentor may do what's right
for them, that causes least amount of new work, just for this presentation).

- End-user feature/function ... what can end users accomplish.
   Highlight "seamless Eclipse Integration" where appropriate.
   List standards met or supported or standards deviated from, if appropriate.
   Give links to tutorials, end-user type documentation, if any.
   (Screen shots always pretty ... let's avoid risk of live demos over shared presentation link :)

 -Quality -- what's current status/counts of your bug reports, maybe trends?
	(I'm sure these will change a lot, so any meaningful summarizing
	queries you come up with could be saved and the query included in summary.)
	You might include a few positive quotes from newsgroup, if you receive some
	antidotal evidence of goodness (you can skip the negative ones :)

- API
	give links to its overview documentation.
	give links to design documents.
	Name primary classes/interfaces/packages as "places to start" if someone were
	to be casually exploring API. 
	Show picture of primary plugins involved and their dependancies (inside and outside WTP). 
	Give brief mention to "provisional APIs" ... since this indicates
	a "statement of direction" that we intend to cover in release 2 -- but for which
	there is no implied support (no need to dwell on future, that will be subject of other
	calls, other planning cycles, but should mention why is provisional, instead of API).



5/19 - openhouse 2, 3
5/26 - openhouse 1
6/2  - openhouse 4
6/9  - openhouse 5
6/16 - openhouse 6

7/1 -  M5 done, begin "end-game" phase
6/21,22  project release review prep
7/6    project release review 





1. ========================== Craig Salter to present (and/or organize others)

    - Common Component
          o Extensible Navigator --> internalProvisional, moving to base 3.2
          o Tabbed Property View --> internalProvisional, moving to base 3.2
          o Snippets View --> internalProvisional. Belongs here in WTP. 
                Needs more work to better integrate with Eclipse Templates (if possible)
          o Extensible URI Resolver --> internalProvisional. 
  
          o Validation Framework Component --> internalProvisional. 

		  o common frameworks  - ready for Review
				dataoperations - IDataModel - proposed API
				datawizards - provisional?
	
		  o Command Framework Component --> internal provisional


	      o common.componentcore (needed for flexible projects) - Ready for Review, but
                pure resource part rightfully belongs in base.


2. ========================== Tim DeBoer to present (and/or organize others).

WST Server Subsystem

    - WST Server Component - - Ready For Review.
    - Internet Component
        	Internet Monitor - internal provisional, pending post 1.0 review with TPTP
        	Browser - moved to base Eclipse in 3.1
        	URL Proxy Preferences --> moved to base in 3.1

JST Server Subsystem

    - JST Server Component - Ready For Review.

3. =========================== Der-Ping to present (and/or organize others).


Database Subsystem

    - RDB/SQL - internal, not API due to probable DTP project merge

4. ========================== David Williams to present (and/or organize others).


XML Subsystem

    - XML Component - No R1 API. some internal provisional.
    - Schema Component - No R1 API.  - some internal provisional.
    - DTD Component No R1 API. (only minimal ever planned). 
    - SSE Component No R1 API. some internal provisional.

Web Resources Subsystem

    - HTML Component. No R1 API. some internal provisional.
    - CSS Component. No R1 API. some internal provisional.
    - JavaScript Component. No R1 API. some internal provisional.
    [- Web Project Component. no R1 api, HTML ("static") Web Project]

From: J2EE Web Subsystem (WAR)

      - JSP Component - No R1 API. Some internal provisional.



5. ========================== Chris Brealey to present (and/or organize others).

Web Services Subsystem

    - WS Component ... some internal provisional
    - WSDL Component  ... Ready For Review. WSDL Spec Model API (see "EMF Models" notes).
    - WSI Component ... ? some API (provisional?) from WSTV project ?


6. =========================== Chuck Bridgham to present (and/or organize others).
J2EE Web Subsystem (WAR)

    - J2EE Core Web Model Component [Issue: not currently packaged this way]
    - Servlet Component/J2EEWebProject  ...  some API, create web compentent API
    [- JSP Component ... some internal provisional .. need better design documents]
    - WS Web Component  (JAXRPC)



J2EE Enterprise Subsystem (EARs, EJBJar, EJBClientJar, RARs)

    - J2EE Core Enterprise Model Component [Issue: not currently packaged this way]
    - J2EE Component .. .Ready For Review: J2EE Spec Model API (see "EMF Models" notes).
    - EJB Component 
    - WS Component 
