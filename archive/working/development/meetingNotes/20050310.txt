3/10 meeting

Attendees:
David Williams, IBM
Ted Bashor, BEA
Chuck Bridgham, IBM
Naci Dai, Eteration
Kevin Haaland, IBM

notes provided by David Williams

On the issue of "provisional APIs" .... I'll document here the position the WTP Architecture Group which discussed
this issue in last meeting. Initially, we were unanimously agreed anything that's not API should not be identified
as "proposed API" via package names but instead through other forms of JavaDoc and "proposal documents" for
"future releases". But, as we continued to discuss areas we would (likely) recommend that not be API in release 1,
it became apparent there could easily be some cases where it might improve review and feedback for iterative
design -- so, we would not object to "internal.provisional" in package names as long as 1) the correct expectation
is set that there is no guarantee of future compatibility (possibly not even in maintenance stream/fixes), so
clients have to be prepared to respond, if they use it, as they would any other "internal" use, and 2) the use of
"internal.provisional" should not be used lightly ... component leads would be expected to have some degree
justification  for using it (such as the best justification would be: "known clients willing to use 'internal'
classes, desired future API function, but planned to be moving to a different project next release" or similar).
As Arthur said, its use it optional, and there are other ways of "giving hints" as to future proposed API
functionality.

I think this is all consistent with the sum total of previous comments posted to wtp-dev (its meant to be), and
I'm just trying to publically document Architecture Groups concerns and discussion of it.


= = = = = = = =

Architecture and Design Issues the Architecture Group is proposing as some of the most important to resolve for
Release 1. I'll open bugzilla "issues" for the most important of these, marked with [arch] in summary to help
track and soliciti community feedback.

1. making sure API commitments are solid, platform quality. .
Components leads need to provide easy to find, "central place" for reading about their design overviews, proposed
APIs, etc., including a description and justification for their plugin structure (its felt there's probably still
too many separate plugins, but we are not sure of what reasons are). We'll schedule brief "reviews" with component
leads, but the "live" review will by necessity need to be brief, so still expect most of work done prior to Arch.
group's review and much of work done by community and clients of the API.
To be Scheduled soon.


2. Flexible project model
Such a "large change" to the traditional "Eclipse Way" of doing projects may be hard to "get right" the first
time. That is, needs lots of review and iteration and "client use".
Especially there may be release 2 issues with base Eclipse, that is, if base Eclipse provides "more support" in
release 3.2, would all our flexible project API still be correct?
should it be API in Release 1 of WTP or a "provisional API"?
Chuck and Naci to review status/recommendations with Architecture Group on or near 4/21.
Ted to provide interested party from BEA.


3. Server APIs
is it adequate? Getting correct "add on" feedback?
Ted will propose "publish" participation, similar to "deploy" participation
Do we need scheduled review? Or, just have people open their bugzilla's if see issues?
(I'll pick the later for now, since this component has gotten lots of review).

4. Operation (command) Framework.
Two (or three) "command" and/or "operation" frameworks in WTP ... which is best? Is the Eclipse base new
"operation" framework (not to be confused with Command framework) a suitable replacement for all of ours?

Chris Brealey
Chuck Bridgham
see and post to ui.dev mailing list
[Michael Van Meekeren and Doug Polick interested parties from base Eclipse]
Ted Bashor interested from BEA

Kevin says platform's goal is to provide the one-and-only operation framework ....
so, no API in WTP!? We in WTP may not want to move in 3.1, but should he recommends WTP at least
review base support by 3.1, and move to that in 3.2 and not introduce competing API
Chuck is not sure what he and Chris call "command/operation/wizard" frameworks are related to "Operations
framework" in base.
Suggest Chris/Chuck to report/review with Architecture Group on or before 4/21.
Sounds unlikely to be API in WTP 1.0.

5. EditModel.
There's an "EditModel" and "ResourceSet" (EMF) currently in VE, used by WTP, but rightfully belongs in EMF. I'm
wondering if the bases new "Logical Resources" substitutes for this or if we just need to work on getting in EMF?
Chuck reports similar but "more than" logical resources ... so he'll work with Ed to "beef up" EMF resource Set to
EditModel Chuck Bridgham to work with Ed Merks likely NOT to be WTP API (but maybe EMF API) Chuck to report/review
on or before 4/21

6. Common Undo. There's a new Common Undo Manager in base Eclipse ... we in WTP need to see if we can move to
this, in place of
our current common undo manager (that comes from a utility package in EMF). Is there a need for EMF to move too?
(Common, btw, means
one per resource or resource set, not just one implementation. The base Eclipse has, basically, one per editor,
plus new work going on).
Team:
Nitin Dahyabhai
EMF - Ed Merks
Would it help if Ed can move his command stack "utility" to common framework, so no changes needed in WTP this
release?
Suggest Nitin to report/review on or before 4/21

7. Extensible Navigator. Should our WTP Extensible Navigator be promoted to WTP API? Work is slated for Eclipse
3.2 to provide in similar functionality in base Eclipse, but will it be the same? Sufficient? Should it be
internal in anticipation of changing in 1.1?
Team:
Michael Elder
Michael Van Meekeren
Mr. Elder to present/review with Arch. Team by 4/7
likely NOT API in WTP, but Ted (BEA) concerned about that since they will likely have to use in client
(That is, they can probably manage risk of "using internal" but want to be sure its clear what is proposed for
future API so their use of internal is at least in right direction and they can provide valid feedback).


8. Tabbed Property Sheet?  Is there a reason for this to be in base (its not strictly a "web function", though
required by WTP)? Reason to be WTP API?
Craig to report/review on 3/24
Probably not API, should probably move to base, but need time, need clear statement of intent.
Craig to send documentation pointers to Kevin (and CC Arch. group) and Kevin will see if someone from base can at
least review for suitability.


9.  URI Resolver -- how compares with base resolving schemes? How compares with EMF's URI Converter?
see https://bugs.eclipse.org/bugs/show_bug.cgi?id=87465
Team:
Craig Salter
Nitin Dahyabhai
BEA - Gary Horton (interest in future "schema paths" similar to "class paths").
Suggest Craig to report/review on 4/7.

10.  XMLCatalog -- need to confirm its consistent with OASIS specs, XML/DOM3?
see https://bugs.eclipse.org/bugs/show_bug.cgi?id=87465
Team:
Craig Salter
BEA - ... Ted to get back with a name of interested parties in BEA.
Suggest Craig to report/review on 4/7

11. Browser framework should be moved to base -- work in progress (but everyone's overloaded).
Tim DeBoer
Dejan Glozic
Suggest Tim to report/review on 3/24


12. Internet proxy settings should be moved to base, since effects VM properties (and danger of conflicts if not
just one provided at a low level).
Chris Brealey
have Chris contact Kevin with documentation pointers (and CC Arch. group), he'll see if can be made part of
"update" component,
or, at minimum, reviewed by base team for 3.2 item.
Suggest Chris to report/review on 4/7


= = = = =
Following are already thought to be "future release" issues


13. Validation framework is important and useful, but needs to reconcile with similar function in TPTP, move
to base in future version? (initial indication indicates not move to base, at least anytime soon).
I suggest we make this a "future release" item, but put team in place now to be sure requirements, basic API is
evolvable.
Interested Parties:
David Williams (IBM, WTP/SSE)
Ted Bashor (BEA, WTP)
Vijay Bhadriraju (IBM, WTP/J2EE)
Alex Iskold (IBM, TPTP)
DW to report/review on 6/2


14. Do we have or need a consistent logging strategy? Doesn't need our attention now, but maybe next release.
Kevin recommends we (WTP)  see and comment on bug 83550.

15. Ted mentioned need for "crash/bug reporting" back to central (or predesignated) spot. I think some of this is
in TPTP, but not sure how accessible. See/comment on bug 83550. Definitely a "base provided function" ... only
question for WTP is if its transparent, or some degree of support we'd have to build in.

16. Data collection strategy? (e.g. most frequently used actions, preferences changed, etc). More of a future
concern, but may be able to use base support transparently using instrumentation plugin Eclipse Foundation is
looking at "how to" do (signed jars to protect receipt of data, receiving feedback at central site, etc).















