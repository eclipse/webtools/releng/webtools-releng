Agenda/Notes for 4/7 meeting

Invitees: David, Kevin, Naci, Ted, Chuck
Special Guests: Chris Brealey, Craig Salter, Michael Elder

Attendees
Invitees: David, Ted, Chuck
Special Guests: Chris Brealey, Craig Salter, Michael Elder


Discussed these architecture topics

88017 cbrealey@ca.ibm.com david_williams@us.ibm.com [arch] Should Internet proxy settings move to base? 

	Action: Chris will move to Eclipse 3.2 enhancement request
			Chris has moved 88017 to 3.2 base feature request. 
	
	Action: Craig brought up internet caching in general is platform issue.
		    Chris or Craig  will open feature request for Eclipse 3.2

88014 csalter@ca.ibm.com david_williams@us.ibm.com [arch] Should URI Resolver be WTP API 

	Craig proposes to make internal.provisional for 1.0
	need to clarify (document) use cases handled, use cases not, use cases desired

88015 csalter@ca.ibm.com david_williams@us.ibm.com [arch] Is XML Catalog ready for API? 

	plan is to eventually support a subset of OASIS spec
		such as, save our catalog in OASIS format
		Craig will document our degree of (or lack of) compliance
	still planning to have catlog extension point ... but still some 
	    issues to settle on for proposed API.
	need good tutorial/links on 3rd party DTDs/Schemas
	Craig has submited some DTDs and Schemas for re-distribution


88012 mdelder@us.ibm.com david_williams@us.ibm.com [arch] Should extensible navigator be API? (and brief status of "flexible project" API) 

	Long discussion on current status and plans. 
	General concern about the try{}finally{} required pattern and some
		possible improvements
	Need to revisit with Kevin issue of if the resource-level "flexible project" 
	API should be in base Eclipse. (Might effect if marked 'provisional' in WTP 1.0, 
	or at least need to make sure we have migration path open to all). 
	
	

Also, 

Should WTP do any work on "jarred plugins" or manifest-only plugins?
<not discussed, but component leads generally feel like "maybe", but not 
P1 item>

Update or questions from Ted and Chuck on Architecture Overview document
<offline, Chuck reported no pretty pictures yet ... but should be soon, 
haven't talked to Ted about status yet.>

