/*******************************************************************************
 * Copyright (c) 2006-2011 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Original code by provided by Craig Slater, attached to bug 109788
 *     https://bugs.eclipse.org/bugs/show_bug.cgi?id=109788
 *     Package names changed to "org.eclipse.wtp" namespace, and other 
 *     minor modification made by David Williams, 2011 
 *******************************************************************************/
package org.eclipse.wtp.releng.www.protocol.http.test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class HandlerTest {


    public static void main(String args[]) {
        System.setProperty("java.protocol.handler.pkgs", "org.eclipse.wtp.releng.www.protocol");
        String userDir = System.getProperty("user.dir", "");
        System.setProperty("urlMapLocation", userDir + "/map.txt");

        try {
            String uri = "http://www.hello.com/bar.xsd";
            testuri(uri);
            uri = "http://impossiblejunkysdflkjURL";
            testuri(uri);
            uri = "http://www.ibm.com";
            testuri(uri);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void testuri(String uri) throws MalformedURLException, IOException {
        System.out.println();
        System.out.println();
        System.out.println("   testing uri: " + uri);
        try {
            URL url = new URL(uri);
            java.io.InputStream inputStream = url.openStream();
            InputStreamReader reader = new InputStreamReader(inputStream);
            char input[] = new char[100];
            for (int length = reader.read(input); length > 0; length = reader.read(input)) {
                for (int j = 0; j < length; j++) {
                    System.out.print(input[j]);
                }
            }
            System.out.println();
        }
        /* We catch all exceptions here, since some test URL can timeout, perhaps have malformed input, etc., 
         * but even in those cases, we want the test to continue to the next test case.
         */
        catch (Exception e) {
            e.printStackTrace();
        }

    }
}
