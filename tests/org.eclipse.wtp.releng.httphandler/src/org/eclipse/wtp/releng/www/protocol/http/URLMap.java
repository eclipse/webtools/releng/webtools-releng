/*******************************************************************************
 * Copyright (c) 2006-2011 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Original code by provided by Craig Slater, attached to bug 109788
 *     https://bugs.eclipse.org/bugs/show_bug.cgi?id=109788
 *     Package names changed to "org.eclipse.wtp" namespace, and other 
 *     minor modification made by David Williams, 2011 
 *******************************************************************************/
package org.eclipse.wtp.releng.www.protocol.http;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

public class URLMap extends Hashtable {

    public URLMap() {
    }

    public synchronized void load(InputStream inStream) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(inStream, "8859_1"));
        do {
            String line;
            int len;
            int keyStart;
            char firstChar;
            do {
                do {
                    do {
                        line = in.readLine();
                        if (line == null) {
                            return;
                        }
                    }
                    while (line.length() <= 0);
                    len = line.length();
                    for (keyStart = 0; keyStart < len; keyStart++) {
                        if (" \t\r\n\f".indexOf(line.charAt(keyStart)) == -1) {
                            break;
                        }
                    }

                }
                while (keyStart == len);
                firstChar = line.charAt(keyStart);
            }
            while ((firstChar == '#') || (firstChar == '!'));
            while (continueLine(line)) {
                String nextLine = in.readLine();
                if (nextLine == null) {
                    nextLine = "";
                }
                String loppedLine = line.substring(0, len - 1);
                int startIndex;
                for (startIndex = 0; startIndex < nextLine.length(); startIndex++) {
                    if (" \t\r\n\f".indexOf(nextLine.charAt(startIndex)) == -1) {
                        break;
                    }
                }

                nextLine = nextLine.substring(startIndex, nextLine.length());
                line = new String(loppedLine + nextLine);
                len = line.length();
            }
            int separatorIndex;
            for (separatorIndex = keyStart; separatorIndex < len; separatorIndex++) {
                char currentChar = line.charAt(separatorIndex);
                if (currentChar == '\\') {
                    separatorIndex++;
                    continue;
                }
                if ("= \t\r\n\f".indexOf(currentChar) != -1) {
                    break;
                }
            }

            int valueIndex;
            for (valueIndex = separatorIndex; valueIndex < len; valueIndex++) {
                if (" \t\r\n\f".indexOf(line.charAt(valueIndex)) == -1) {
                    break;
                }
            }

            if ((valueIndex < len) && ("=".indexOf(line.charAt(valueIndex)) != -1)) {
                valueIndex++;
            }
            for (; valueIndex < len; valueIndex++) {
                if (" \t\r\n\f".indexOf(line.charAt(valueIndex)) == -1) {
                    break;
                }
            }

            String key = line.substring(keyStart, separatorIndex);
            String value = separatorIndex >= len ? "" : line.substring(valueIndex, len);
            key = loadConvert(key);
            value = loadConvert(value);
            put(key, value);
        }
        while (true);
    }

    private boolean continueLine(String line) {
        int slashCount = 0;
        for (int index = line.length() - 1; (index >= 0) && (line.charAt(index--) == '\\');) {
            slashCount++;
        }

        return (slashCount % 2) == 1;
    }

    private String loadConvert(String theString) {
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);
        for (int x = 0; x < len;) {
            char aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if (aChar == 'u') {
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = theString.charAt(x++);
                        switch (aChar) {
                            case 48 : // '0'
                            case 49 : // '1'
                            case 50 : // '2'
                            case 51 : // '3'
                            case 52 : // '4'
                            case 53 : // '5'
                            case 54 : // '6'
                            case 55 : // '7'
                            case 56 : // '8'
                            case 57 : // '9'
                                value = ((value << 4) + aChar) - 48;
                                break;

                            case 97 : // 'a'
                            case 98 : // 'b'
                            case 99 : // 'c'
                            case 100 : // 'd'
                            case 101 : // 'e'
                            case 102 : // 'f'
                                value = ((value << 4) + 10 + aChar) - 97;
                                break;

                            case 65 : // 'A'
                            case 66 : // 'B'
                            case 67 : // 'C'
                            case 68 : // 'D'
                            case 69 : // 'E'
                            case 70 : // 'F'
                                value = ((value << 4) + 10 + aChar) - 65;
                                break;

                            case 58 : // ':'
                            case 59 : // ';'
                            case 60 : // '<'
                            case 61 : // '='
                            case 62 : // '>'
                            case 63 : // '?'
                            case 64 : // '@'
                            case 71 : // 'G'
                            case 72 : // 'H'
                            case 73 : // 'I'
                            case 74 : // 'J'
                            case 75 : // 'K'
                            case 76 : // 'L'
                            case 77 : // 'M'
                            case 78 : // 'N'
                            case 79 : // 'O'
                            case 80 : // 'P'
                            case 81 : // 'Q'
                            case 82 : // 'R'
                            case 83 : // 'S'
                            case 84 : // 'T'
                            case 85 : // 'U'
                            case 86 : // 'V'
                            case 87 : // 'W'
                            case 88 : // 'X'
                            case 89 : // 'Y'
                            case 90 : // 'Z'
                            case 91 : // '['
                            case 92 : // '\\'
                            case 93 : // ']'
                            case 94 : // '^'
                            case 95 : // '_'
                            case 96 : // '`'
                            default :
                                throw new IllegalArgumentException("Malformed \\uxxxx encoding.");
                        }
                    }

                    outBuffer.append((char) value);
                }
                else {
                    if (aChar == 't') {
                        aChar = '\t';
                    }
                    else if (aChar == 'r') {
                        aChar = '\r';
                    }
                    else if (aChar == 'n') {
                        aChar = '\n';
                    }
                    else if (aChar == 'f') {
                        aChar = '\f';
                    }
                    outBuffer.append(aChar);
                }
            }
            else {
                outBuffer.append(aChar);
            }
        }

        return outBuffer.toString();
    }

    private String saveConvert(String theString, boolean escapeSpace) {
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len * 2);
        for (int x = 0; x < len; x++) {
            char aChar = theString.charAt(x);
            switch (aChar) {
                case 32 : // ' '
                    if ((x == 0) || escapeSpace) {
                        outBuffer.append('\\');
                    }
                    outBuffer.append(' ');
                    break;

                case 92 : // '\\'
                    outBuffer.append('\\');
                    outBuffer.append('\\');
                    break;

                case 9 : // '\t'
                    outBuffer.append('\\');
                    outBuffer.append('t');
                    break;

                case 10 : // '\n'
                    outBuffer.append('\\');
                    outBuffer.append('n');
                    break;

                case 13 : // '\r'
                    outBuffer.append('\\');
                    outBuffer.append('r');
                    break;

                case 12 : // '\f'
                    outBuffer.append('\\');
                    outBuffer.append('f');
                    break;

                default :
                    if ((aChar < ' ') || (aChar > '~')) {
                        outBuffer.append('\\');
                        outBuffer.append('u');
                        outBuffer.append(toHex((aChar >> 12) & 15));
                        outBuffer.append(toHex((aChar >> 8) & 15));
                        outBuffer.append(toHex((aChar >> 4) & 15));
                        outBuffer.append(toHex(aChar & 15));
                        break;
                    }
                    if ("= \t\r\n\f".indexOf(aChar) != -1) {
                        outBuffer.append('\\');
                    }
                    outBuffer.append(aChar);
                    break;
            }
        }

        return outBuffer.toString();
    }


    public synchronized void store(OutputStream out, String header) throws IOException {
        BufferedWriter awriter = new BufferedWriter(new OutputStreamWriter(out, "8859_1"));
        if (header != null) {
            writeln(awriter, "#" + header);
        }
        writeln(awriter, "#" + (new Date()).toString());
        String key;
        String val;
        for (Enumeration e = keys(); e.hasMoreElements(); writeln(awriter, key + "=" + val)) {
            key = (String) e.nextElement();
            val = (String) get(key);
            key = saveConvert(key, true);
            val = saveConvert(val, false);
        }

        awriter.flush();
    }

    private static void writeln(BufferedWriter bw, String s) throws IOException {
        bw.write(s);
        bw.newLine();
    }

    public String getProperty(String key) {
        Object oval = super.get(key);
        String sval = (oval instanceof String) ? (String) oval : null;
        return sval;
    }

    public String getProperty(String key, String defaultValue) {
        String val = getProperty(key);
        return val != null ? val : defaultValue;
    }

    public Enumeration propertyNames() {
        Hashtable h = new Hashtable();
        enumerate(h);
        return h.keys();
    }

    public void list(PrintStream out) {
        out.println("-- listing properties --");
        Hashtable h = new Hashtable();
        enumerate(h);
        String key;
        String val;
        for (Enumeration e = h.keys(); e.hasMoreElements(); out.println(key + "=" + val)) {
            key = (String) e.nextElement();
            val = (String) h.get(key);
            if (val.length() > 40) {
                val = val.substring(0, 37) + "...";
            }
        }

    }

    public void list(PrintWriter out) {
        out.println("-- listing properties --");
        Hashtable h = new Hashtable();
        enumerate(h);
        String key;
        String val;
        for (Enumeration e = h.keys(); e.hasMoreElements(); out.println(key + "=" + val)) {
            key = (String) e.nextElement();
            val = (String) h.get(key);
            if (val.length() > 40) {
                val = val.substring(0, 37) + "...";
            }
        }

    }

    private synchronized void enumerate(Hashtable h) {
        String key;
        for (Enumeration e = keys(); e.hasMoreElements(); h.put(key, get(key))) {
            key = (String) e.nextElement();
        }

    }

    private static char toHex(int nibble) {
        return hexDigit[nibble & 15];
    }

    private static final String keyValueSeparators       = "= \t\r\n\f";
    private static final String strictKeyValueSeparators = "=";
    private static final String specialSaveChars         = "= \t\r\n\f";
    private static final String whiteSpaceChars          = " \t\r\n\f";
    private static final char   hexDigit[]               = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

}
