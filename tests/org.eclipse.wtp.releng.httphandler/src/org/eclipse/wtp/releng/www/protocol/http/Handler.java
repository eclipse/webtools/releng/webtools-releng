/*******************************************************************************
 * Copyright (c) 2006-2011 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Original code by provided by Craig Slater, attached to bug 109788
 *     https://bugs.eclipse.org/bugs/show_bug.cgi?id=109788
 *     Package names changed to "org.eclipse.wtp" namespace, and other 
 *     minor modification made by David Williams, 2011 
 *******************************************************************************/
package org.eclipse.wtp.releng.www.protocol.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;


public class Handler extends sun.net.www.protocol.http.Handler {
    URLMap           map;
    long             mapTimeStamp;
    String           outputLocation;
    FileOutputStream outputStream;
    PrintWriter      printWriter;
    private String   inputLocation;

    public Handler() {
        map = null;
        mapTimeStamp = 0L;
        outputLocation = null;
        outputStream = null;
        printWriter = null;
        try {
            outputLocation = System.getProperty("urlLogLocation");
            inputLocation = System.getProperty("urlMapLocation");
            initPrintWriter();
            printWriter.println("--------------------------------------");
            printWriter.println("HTTP Connection Tracer is enabled");
            printWriter.println("For more information about HTTP Connection Tracer, see  http://wiki.eclipse.org/WTP/HTTP_Connection_Tracer_Diagnostic_Utility");
            if (outputLocation != null) {
                printWriter.println("urlLogLocation: " + outputLocation);
            }
            else {
                printWriter.println("urlLogLocation: " + "System.out");
            }
            if (inputLocation != null) {
                printWriter.println("urlMapLocation: " + inputLocation);
            }
            else {
                printWriter.println("urlMapLocation: " + "(no location provided)");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            releasePrintWriter();
        }
    }

    protected void initPrintWriter() {
        try {
            printWriter = null;
            if (outputLocation != null) {
                outputStream = new FileOutputStream(outputLocation, true);
                printWriter = new PrintWriter(outputStream);
            }
            else {
                printWriter = new PrintWriter(System.out);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void releasePrintWriter() {
        try {
            printWriter.flush();
            if (outputStream != null) {
                outputStream.close();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void reloadMapIfRequired() {

        if (inputLocation != null) {
            FileInputStream inputStream = null;
            try {
                File file = new File(inputLocation);
                if (file.lastModified() > mapTimeStamp) {
                    mapTimeStamp = file.lastModified();
                    map = new URLMap();
                    inputStream = new FileInputStream(inputLocation);
                    map.load(inputStream);
                }
            }
            catch (Exception e) {
                try {
                    initPrintWriter();
                    printWriter.println("Error loading URL map file");
                }
                finally {
                    releasePrintWriter();
                }
            }
            finally {
                try {
                    inputStream.close();
                }
                catch (Exception e) {
                }
            }
        }
    }

    protected void parseURL(URL url, String specification, int start, int limit) {
        super.parseURL(url, specification, start, limit);
    }

    protected URLConnection openConnection(URL url) throws IOException {
        URLConnection result = null;
        String mappedValue = null;
        try {
            reloadMapIfRequired();
            if (map != null) {
                String key = url.toExternalForm();
                mappedValue = map.getProperty(key);
                if ((mappedValue != null) && !mappedValue.startsWith("http")) {
                    result = (new URL(mappedValue)).openConnection();
                }
            }
        }
        catch (Exception exception) {
        }
        finally {
            try {
                initPrintWriter();
                printWriter.println("---------------------------------------------");
                printWriter.println("URL requested : " + url.toExternalForm());
                printWriter.println("URL mapped    : " + (mappedValue == null ? "(not mapped)" : mappedValue));
                throw new Exception("stack at time of request");
            }
            catch (Exception e) {
                printWriter.println("STACK TRACE  :");
                e.printStackTrace(printWriter);
            }
            finally {
                releasePrintWriter();
            }
        }
        if (result == null) {
            result = super.openConnection(url);
        }
        return result;
    }


}
