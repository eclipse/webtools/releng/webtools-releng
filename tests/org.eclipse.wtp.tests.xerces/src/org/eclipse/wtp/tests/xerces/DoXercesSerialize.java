/*******************************************************************************
 * Copyright (c) 2001, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.tests.xerces;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Test Xerces Serialization See
 * http://xerces.apache.org/xerces2-j/faq-general.html#faq-6
 */
public class DoXercesSerialize {

	/**
	 * boolean for XHTML vs HTML
	 */
	boolean doXHTML = true;

	String xmlString = "<ul>\r\n" + "<li>\r\n" + "test1\r\n" + "</li>\r\n"
			+ "<li>\r\n" + "test2\r\n" + "</li>\r\n" + "</ul>";

	private static class InternalErrorHandler implements ErrorHandler {
		public void error(SAXParseException e) {
			System.out.println("SAX PARSE ERROR: " + e);
		}

		public void fatalError(SAXParseException e) {
			System.out.println("SAX PARSE FATAL ERROR: " + e);
		}

		public void warning(SAXParseException e) {
			System.out.println("SAX PARSE WARNING: " + e);
		}
	}

	private Document getTestDocument() throws IOException,
			ParserConfigurationException, SAXException {
		StringReader stringReader = new StringReader(xmlString);

		Document document = null;

		document = getDocument(new InputSource(stringReader),
				new InternalErrorHandler());
		return document;
	}

	public void doLSwrite() throws IllegalAccessException, ClassCastException,
			ClassNotFoundException, InstantiationException, IOException,
			ParserConfigurationException, SAXException {

		DOMImplementationRegistry registry = DOMImplementationRegistry
				.newInstance();

		DOMImplementationLS impl = (DOMImplementationLS) registry
				.getDOMImplementation("LS");
		
		LSSerializer writer = impl.createLSSerializer();
		LSOutput output = impl.createLSOutput();
		output.setByteStream(System.out);

		
		Document document = getTestDocument();
		writer.write(document, output);

	}

	public void doSerialize() throws TransformerFactoryConfigurationError,
			TransformerException, IOException, ParserConfigurationException,
			SAXException {

		// System.setProperty("javax.xml.transform.TransformerFactory",
		// "org.apache.xalan.xsltc.trax.TransformerFactoryImpl");

		// Create an "identity" transformer - copies input to output
		Transformer t = TransformerFactory.newInstance().newTransformer();

		// Constants.main(new String[] {});

		if (doXHTML) {
			// for "XHTML" serialization, use the output method "xml"
			// and set publicId as shown
			// t.setOutputProperty(OutputKeys.METHOD, "xml");
			t.setOutputProperty("method", "xml");

			t.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
					"-//W3C//DTD XHTML 1.0 Transitional//EN");

			t.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
					"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd");
		} else {
			// For "HTML" serialization, use
			t.setOutputProperty("method", "html");
		}
		Document document = getTestDocument();
		// Serialize DOM tree
		// ClassLoader previousClassLoader = Thread.currentThread()
		// .getContextClassLoader();
		// ClassLoader keyClassLoader =
		// Platform.getBundle("org.apache.xerces").getClass().getClassLoader();
		// //getClass().getClassLoader();
		// //
		// Platform.getBundle("org.apache.xerces").getClass().getClassLoader();
		// System.out.println("previous classloader: " + previousClassLoader);
		// System.out.println(" key classloader: " + keyClassLoader);
		// try {
		// Thread.currentThread().setContextClassLoader(keyClassLoader);
		t.transform(new DOMSource(document), new StreamResult(System.out));
		// } finally {
		// Thread.currentThread().setContextClassLoader(previousClassLoader);
		// }
	}

	/**
	 * Builds a document using Xerces.
	 * 
	 * @param inputSource
	 *            the contents to parse.
	 * @param errorHandler
	 *            the handled used by the parser.
	 * @return a document.
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	private Document getDocument(InputSource inputSource,
			ErrorHandler errorHandler) throws IOException,
			ParserConfigurationException, SAXException {

		Document document = null;

		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
				.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
		documentBuilderFactory.setValidating(false);

		DocumentBuilder documentBuilder = null;
		documentBuilder = documentBuilderFactory.newDocumentBuilder();

		documentBuilder.setErrorHandler(errorHandler);

		document = documentBuilder.parse(inputSource);

		return document;
	}

}
