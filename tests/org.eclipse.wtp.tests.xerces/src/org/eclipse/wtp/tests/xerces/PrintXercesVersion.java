/*******************************************************************************
 * Copyright (c) 2004, 2025 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     
 *******************************************************************************/

package org.eclipse.wtp.tests.xerces;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

public class PrintXercesVersion implements IApplication {
	/**
	 * Specifying on the command line gives more info, presumably since some
	 * classes already loaded by the time it get's to here.
	 */
	static {
		System.setProperty("jaxp.debug", "1");
	}

	public static void main(String[] args) {

		String msg = null;
		if (args != null && args.length > 0) {
			msg = args[0];
		}
		else {
			msg = "XML Related Version Info from main";
		}

		new PrintXercesVersion().printHeaderInfo(msg);
	}

	public void testPrintVersionInfo() {
		printHeaderInfo("XML Related Version Info from test method");
	}

	private void printHeaderInfo(String test) {
		System.out.println();
		System.out.println("------");
		System.out.println();
		System.out.println("Test: " + test);
		System.out.println();
		System.out.println("java.vendor: " + System.getProperty("java.vendor"));
		System.out.println("java.version: " + System.getProperty("java.version"));
		System.out.println();
		System.out.println("Xerces Impl Version: " + org.apache.xerces.impl.Version.getVersion());
// System.out.println("Serializer Version: "
// + org.apache.xml.serializer.Version.getVersion());
		try {
			System.out.println("Resolver Version: " + org.apache.xml.resolver.Version.getVersion());
		}
		catch (NoClassDefFoundError e) {
			System.out.println("Resolver Version: " + "NoClassDefFoundError: " + e.getMessage());

		}
//		System.out.println("XMLCommons Version: " + org.apache.xmlcommons.Version.getVersion());
		System.out.println();
		System.out.println("------");
	}

	public Object start(IApplicationContext context) throws Exception {
		main(new String[]{"XML Related Version Info from application"});
		return IApplication.EXIT_OK;
	}

	public void stop() {
		// nothing to do
	}
}
