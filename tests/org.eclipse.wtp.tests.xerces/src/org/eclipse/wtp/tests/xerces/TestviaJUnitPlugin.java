/*******************************************************************************
 * Copyright (c) 2004, 2025 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     
 *******************************************************************************/

package org.eclipse.wtp.tests.xerces;

import junit.framework.TestCase;

public class TestviaJUnitPlugin extends TestCase {
	/**
	 * Specifying on the command line gives more info, presumably since some
	 * classes already loaded by the time it get's to here.
	 */
	static {
		System.setProperty("jaxp.debug", "1");
	}

	public void testDoDOMAndSerialization() throws Throwable {
		printHeaderInfo("Test Transformer Serialization");
		try {
			DoXercesSerialize testXerces = new DoXercesSerialize();
			testXerces.doSerialize();
			// consider passed if no exception thrown
			assertTrue(true);
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void testDoLSSerialization() throws Throwable {
		printHeaderInfo("Test LSSerialization");
		try {
			DoXercesSerialize testXerces = new DoXercesSerialize();
			testXerces.doLSwrite();
			// consider passed if no exception thrown
			assertTrue(true);
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void printHeaderInfo(String test) {
		System.out.println();
		System.out.println("------");
		System.out.println();
		System.out.println("Test: " + test);
		System.out.println();
		System.out.println("java.vendor: " + System.getProperty("java.vendor"));
		System.out.println("java.version: " + System.getProperty("java.version"));
		System.out.println();
		String transformerSystemProperty = System.getProperty("javax.xml.transform.TransformerFactory");
		if (transformerSystemProperty != null && transformerSystemProperty.length() > 0) {
			System.out.println("javax.xml.transform.TransformerFactory set as system property: " + transformerSystemProperty);
			System.out.println();
		}
		System.out.println("Xerces Version: "
				+ org.apache.xerces.impl.Version.getVersion());
// System.out.println("Serializer Version: "
//				+ org.apache.xml.serializer.Version.getVersion());
		try {
			System.out.println("Resolver Version: "
					+ org.apache.xml.resolver.Version.getVersion());
		} catch (NoClassDefFoundError e) {
			System.out.println("Resolver Version: " + "NoClassDefFoundError: "
					+ e.getMessage());

		}
//		System.out.println("XMLCommons Version: "
//				+ org.apache.xmlcommons.Version.getVersion());
		System.out.println();
		System.out.println("------");
	}
}
