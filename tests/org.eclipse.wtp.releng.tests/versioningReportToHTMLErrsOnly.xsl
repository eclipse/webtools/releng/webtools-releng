<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output
        method="html"
        indent="yes"/>
    <xsl:template
        match="/">
        <html>
            <body>
                <xsl:apply-templates
                    select="versioningReport"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template
        match="versioningReport">
        <h2>Versioning Report</h2>
        <xsl:apply-templates
            select="versioningReportData"/>
    </xsl:template>
    <xsl:template
        name="errors"
        match="versioningReportData">
        <p>
            For
            <xsl:value-of
                select="@current"/>
            using
            <xsl:value-of
                select="@id"/>
            as the reference.
        </p>
        <p>Remember this listing is subject to inaccuracies
            based on bugs in the scripts that generate it, or, more
            likely in the exact way or time the reference data was
            obtained. In other words, the code is correct and self
            documenting, and this report is intended to provide a quick
            sneek peek only.</p>
        <p>Errors (decreases in version numbers)</p>
        <ul>
            <li>
                <a
                    href="#bundleerrors">Bundles with versioning errors</a>
            </li>
            <li>
                <a
                    href="#featureerrors">Features with versioning errors</a>
            </li>
        </ul>
        <p>Bundles</p>
        <ul>
            <li>
                <a
                    href="#bundlequalifier">Bundles with qualifier-only increases
                </a>
            </li>
        </ul>
        <p>Features</p>
        <ul>
            <li>
                <a
                    href="#feauturequalifier">Features with qualifier-only increases
                </a>
            </li>
        </ul>
        <h3
            id="bundleerrors">Bundles with versioning errors</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'error' and @codePackageType ='bundle'">
                    <dt
                        style="font-weight: bold; color: crimson">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace;">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace;">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="featureerrors">Features with versioning errors</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'error' and @codePackageType ='feature'">
                    <dt
                        style="font-weight: bold; color: crimson">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace;">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace;">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="bundlequalifier">Bundles with qualifier-only increases</h3>
        <p>There may be occasions these do not always indicate a
            true error (such as fixing a spelling mistake in a comment,
            but it is clearer if qualifier-only increases are not
            allowed, since usually it indicates that someone has changed
            the code, but forgotten to increase the service or minor
            version.</p>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'increase' and @field='qualifier' and @codePackageType ='bundle'">
                    <dt
                        style="font-weight: bold; color: crimson">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="feauturequalifier">Features with qualifier-only increases</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'increase' and @field='qualifier' and @codePackageType ='feature'">
                    <dt
                        style="font-weight: bold; color: crimson">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
    </xsl:template>
</xsl:stylesheet>