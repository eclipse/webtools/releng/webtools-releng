<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright (c) 2006, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->

<project
    name="testsuite"
    default="run"
    basedir=".">
    <!--
        ===================================================== There
        should be no need to change what's above. (if there is, let us
        know if the script can be improved.)
        =====================================================
    -->
    <!--
        Every suite needs to specify three thing: plugin-name, classname
        - that provides the suite to test, and testType - either
        core-test, or ui-test. There is an optional extraVMargs property
        that some tests need to use in order to specify extra,
        non-standard properties to the VM when it runs. If not need, no
        need to specify it.
    -->
    <property
        name="plugin-name"
        value="org.eclipse.wtp.releng.tests"/>
    <property
        name="classname"
        value="org.eclipse.wtp.releng.tests.TestBuild"/>
    <property
        name="testType"
        value="core-test"/>
    <!--
        some test suites need to add special vm arguments when they run.
        This is just an example. <property name="extraVMargs"
        value="-DjsfRuntimeJarsDirectoryV1.1=${testDir}/${jsf1.1Dir}" />
        <echo message="extraVMargs ${extraVMargs}" />
    -->
    <!--
        There should be no need to change what's below. (if there is, let us
        know if the script can be improved.)
    -->
    <!--
        There are some properties that must be provided by caller.
    -->
    <fail
        message="Error: missing property. eclipse-home must be provided"
        unless="eclipse-home"/>
    <fail
        message="Error: missing property. buildDirectory must be provided"
        unless="buildDirectory"/>
    <fail
        message="Error: missing property. buildLabel must be provided"
        unless="buildLabel"/>
    <fail
        message="Error: missing property. library-file must be provided"
        unless="library-file"/>


    <echo
        message="basedir: ${basedir}"/>
    <echo
        message="eclipse-home: ${eclipse-home}"/>
    <echo
        message="buildDirectory: ${buildDirectory}"/>
    <echo
        message="buildLabel: ${buildLabel}"/>
    <echo
        message="plugin-name: ${plugin-name}"/>
    <echo
        message="classname: ${classname}"/>
    <echo
        message="testType ${testType}"/>
    <echo
        message="buildId ${buildId}"/>

    <property
        name="workspace"
        value="${eclipse-home}/junitworkspaces/${plugin-name}"/>
    <!--
        This init target holds all initialization code that needs to be
        done for all tests that are to be run. Initialization for
        individual tests should be done within the body of the suite
        target. Here it's a good idea to delete things that might have
        been created if the test has already been ran, just in case the
        test is being re-ran.
    -->
    <target
        name="init">
        <tstamp/>
        <delete>
            <fileset
                dir="${eclipse-home}"
                includes="${plugin-name}.*xml"/>
        </delete>
        <!--
            make the workspace directory, in case path doesn't exist yet
            but delete to make sure fresh contents, if it does exist
        -->
        <delete
            dir="${workspace}"
            quiet="true"/>
        <mkdir
            dir="${workspace}"/>

    </target>
    <!--
        This suite target defines the tests that need to be run.
    -->
    <target
        name="suite">
        <ant
            target="${testType}"
            antfile="${library-file}"
            dir="${eclipse-home}">
            <property
                name="data-dir"
                value="${workspace}"/>
            <property
                name="plugin-name"
                value="${plugin-name}"/>
            <property
                name="classname"
                value="${classname}"/>
            <property
                name="extraVMargs"
                value="-DrelengTestsPropertiesFile=${keyCfgFile} -DrepoURLToTest=file://${buildDirectory}/${buildLabel}/repository/ -DcomparatorLogLocation=${buildDirectory}/${buildLabel}  -DcomparatorTestSummaryLogDir=${buildDirectory}/${buildLabel}/testResults -DcomparatorFilterRules=${buildDirectory}/${buildLabel}/comparatorfilter.properties"/>
        </ant>
        <condition
            property="dotlogavailable">
            <available
                file="${workspace}/.metadata/.log"/>
        </condition>
    </target>
    <!--
        This clean target holds code to cleanup the testing environment
        after after all of the tests have been run. You can use this
        target to delete temporary files that have been created, if
        required. But, it's usually best to leave it all, in case you'd
        like to inspect it after a failed run. It will be deleted
        eventually by the next clean build.
    -->
    <target
        name="cleanup">
    </target>
    <!--
        This run target is the master target that get's all the work
        done. Most activity is actually done in the 'depends' tasks, but
        any actions that need to happen after all the tests have been
        run should go here, such as copying the results to a common
        'save' directory.
    -->
    <target
        name="run"
        depends="init,suite,copylogs,copyxsl,cleanup">
        <echo
            message="unit test ran: ${ant.file}"/>
        <echo
            message="library-file: ${library-file}"/>
    </target>
    <target
        name="copylogs"
        if="dotlogavailable">
        <copy
            failonerror="false"
            file="${workspace}/.metadata/.log"
            tofile="${buildDirectory}/${buildLabel}/testResults/consolelogs/${plugin-name}.consolelog.txt"/>
    </target>
    <target
        name="copyxsl"
        depends="checkReport"
        if="reportAvailable">
        <!-- copy our particular xsl files to web directory for prosperity-->
        <copy
            todir="${buildDirectory}/${buildLabel}/"
            flatten="true">
            <fileset
                dir="${testRoot}">
                <include
                    name="**/org.eclipse.wtp.releng.tests*/versioningReportToHTML.xsl"/>
            </fileset>
        </copy>
    	 <!-- and use them to create basic HTML display, if there is a report -->
        <xslt
            basedir="${buildDirectory}/${buildLabel}/"
            includes="versioningReport_${buildLabel}.xml"
            destdir="${buildDirectory}/${buildLabel}/"
            style="versioningReportToHTML.xsl"
            force="true"/>
        <echo
            file="${buildDirectory}/${buildLabel}/versioningReportName.php">
           <![CDATA[<?php $versionReportFilename="versioningReport_${buildLabel}" ?>]]>
        </echo>
    </target>

    <target
        name="checkReport">
        <available
            file="${buildDirectory}/${buildLabel}/versioningReport_${buildLabel}.xml"
            property="reportAvailable"/>
    </target>
</project>