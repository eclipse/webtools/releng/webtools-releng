<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output
        method="html"
        indent="yes"/>
    <xsl:template
        match="/">
        <html>
            <body>
                <xsl:apply-templates
                    select="versioningReport"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template
        match="versioningReport">
        <h2>Versioning Report</h2>
        <xsl:apply-templates
            select="versioningReportData"/>
    </xsl:template>
    <xsl:template
        name="errors"
        match="versioningReportData">
        <p>
            For
            <xsl:value-of
                select="@current"/>
            using
            <xsl:value-of
                select="@id"/>
            as the reference.
        </p>
        <p>Remember this listing is subject to inaccuracies based on
            bugs in the scripts that generate it, or, more likely in the
            exact way or time the reference data was obtained. In other
            words, the code is correct and self documenting, and this
            report is intended to provide a quick sneek peek only.</p>
        <p>Errors (decreases in version numbers)
        <ul>
            <li>
                <a
                    href="#bundleerrors">Bundles with versioning errors</a>
            </li>
            <li>
                <a
                    href="#featureerrors">Features with versioning errors</a>
            </li>
        </ul>
        </p>
        <p>Errors (qualifier-only increases)
        <ul>
            <li>
                <a
                    href="#bundlequalifier">Bundles with qualifier-only increases</a>
            </li>
            <li>
                <a
                    href="#feauturequalifier">Features with qualifier-only increases</a>
            </li>
        </ul>
        </p>
        <p>Bundles</p>
        <ul>
            <li>
                <a
                    href="#bundlemajor">Bundles with major version increase</a>
            </li>
            <li>
                <a
                    href="#bundleminor">Bundles with minor version increase</a>
            </li>
            <li>
                <a
                    href="#bundleservice">Bundles with service version increase</a>
            </li>
        </ul>
        <p>Features</p>
        <ul>
            <li>
                <a
                    href="#featuremajor">Features with major version increase</a>
            </li>
            <li>
                <a
                    href="#featureminor">Features with minor version increase</a>
            </li>
            <li>
                <a
                    href="#featureservice">Features with service version increase</a>
            </li>
        </ul>
        <p>New</p>
        <ul>
            <li>
                <a
                    href="#bundlesnew">New bundles in current but not in reference</a>
            </li>
            <li>
                <a
                    href="#featuresnew">New features in current but not in reference</a>
            </li>
        </ul>
        <p>Gone</p>
        <ul>
            <li>
                <a
                    href="#bundlesgone">Bundles in reference but not in current</a>
            </li>
            <li>
                <a
                    href="#featuresgone">Features in reference but not in current</a>
            </li>
        </ul>
        <p>No Change</p>
        <ul>
            <li>
                <a
                    href="#bundlenochange">Bundles with no change</a>
            </li>
            <li>
                <a
                    href="#featurenochange">Features with no change</a>
            </li>
        </ul>
        <h3
            id="bundleerrors">Bundles with versioning errors</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'error' and @codePackageType ='bundle'">
                    <dt
                        style="font-weight: bold; color: crimson">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace;">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace;">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="featureerrors">Features with versioning errors</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'error' and @codePackageType ='feature'">
                    <dt
                        style="font-weight: bold; color: crimson">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace;">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace;">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="bundlequalifier">Bundles with qualifier-only increases</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'increase' and @field='qualifier' and @codePackageType ='bundle'">
                    <dt
                        style="font-weight: bold; color: crimson">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="feauturequalifier">Features with qualifier-only increases</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'increase' and @field='qualifier' and @codePackageType ='feature'">
                    <dt
                        style="font-weight: bold; color: crimson">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="bundlemajor">Bundles with major version increase</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'increase' and @field='major' and @codePackageType ='bundle'">
                    <dt
                        style="font-weight: bold; color: blue">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="bundleminor">Bundles with minor version increase</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'increase' and @field='minor' and @codePackageType ='bundle'">
                    <dt
                        style="font-weight: bold; color: blue">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="bundleservice">Bundles with service version increase</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'increase' and @field='service' and @codePackageType ='bundle'">
                    <dt
                        style="font-weight: bold; color: blue">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="featuremajor">Features with major version increase</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'increase' and @field='major' and @codePackageType ='feature'">
                    <dt
                        style="font-weight: bold; color: blue">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="featureminor">Features with minor version increase</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'increase' and @field='minor' and @codePackageType ='feature'">
                    <dt
                        style="font-weight: bold; color: blue">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="featureservice">Features with service version increase</h3>
        <dl>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'increase' and @field='service' and @codePackageType ='feature'">
                    <dt
                        style="font-weight: bold; color: blue">
                        <xsl:value-of
                            select="entry"/>
                    </dt>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="current"/>
                        (current)
                    </dd>
                    <dd
                        style="font-family: monospace">
                        <xsl:value-of
                            select="reference"/>
                        (reference)
                    </dd>
                </xsl:if>
            </xsl:for-each>
        </dl>
        <h3
            id="bundlesnew">New bundles in current but not in reference</h3>
        <table
            border="1">
            <tr>
                <th>bundle</th>
                <th>version</th>
            </tr>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'extraInReference' and @codePackageType ='bundle'">
                    <tr>
                        <td>
                            <xsl:value-of
                                select="entry"/>
                        </td>
                        <td>
                            <xsl:value-of
                                select="reference"/>
                        </td>
                    </tr>
                </xsl:if>
            </xsl:for-each>
        </table>
        <h3
            id="featuresnew">New features in current but not in reference</h3>
        <table
            border="1">
            <tr>
                <th>feature</th>
                <th>version</th>
            </tr>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'extraInReference' and @codePackageType ='feature'">
                    <tr>
                        <td>
                            <xsl:value-of
                                select="entry"/>
                        </td>
                        <td>
                            <xsl:value-of
                                select="reference"/>
                        </td>
                    </tr>
                </xsl:if>
            </xsl:for-each>
        </table>
        <h3
            id="bundlesgone">Bundles in reference but not in current</h3>
        <table
            border="1">
            <tr>
                <th>bundle</th>
                <th>version</th>
            </tr>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'extraInCurrent' and @codePackageType ='bundle'">
                    <tr>
                        <td>
                            <xsl:value-of
                                select="entry"/>
                        </td>
                        <td>
                            <xsl:value-of
                                select="reference"/>
                        </td>
                    </tr>
                </xsl:if>
            </xsl:for-each>
        </table>
        <h3
            id="featuresgone">Features in reference but not in current</h3>
        <table
            border="1">
            <tr>
                <th>feature</th>
                <th>version</th>
            </tr>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'extraInCurrent' and @codePackageType ='feature'">
                    <tr>
                        <td>
                            <xsl:value-of
                                select="entry"/>
                        </td>
                        <td>
                            <xsl:value-of
                                select="reference"/>
                        </td>
                    </tr>
                </xsl:if>
            </xsl:for-each>
        </table>
        <h3
            id="featurenochange">Features with no change</h3>
        <table
            border="1">
            <tr>
                <th>feature</th>
                <th>version</th>
            </tr>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'noChange' and @codePackageType ='feature'">
                    <tr>
                        <td>
                            <xsl:value-of
                                select="entry"/>
                        </td>
                        <td>
                            <xsl:value-of
                                select="current"/>
                        </td>
                    </tr>
                </xsl:if>
            </xsl:for-each>
        </table>
        <h3
            id="bundlenochange">Bundles with no change</h3>
        <table
            border="1">
            <tr>
                <th>bundle</th>
                <th>version</th>
            </tr>
            <xsl:for-each
                select="versioningCompare">
                <xsl:if
                    test="@kind = 'noChange' and @codePackageType ='bundle'">
                    <tr>
                        <td>
                            <xsl:value-of
                                select="entry"/>
                        </td>
                        <td>
                            <xsl:value-of
                                select="current"/>
                        </td>
                    </tr>
                </xsl:if>
            </xsl:for-each>
        </table>
    </xsl:template>
</xsl:stylesheet>