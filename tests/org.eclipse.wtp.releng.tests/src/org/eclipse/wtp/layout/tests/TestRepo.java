/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.layout.tests;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.equinox.internal.p2.core.helpers.ServiceHelper;
import org.eclipse.equinox.internal.p2.metadata.License;
import org.eclipse.equinox.p2.core.IProvisioningAgent;
import org.eclipse.equinox.p2.core.ProvisionException;
import org.eclipse.equinox.p2.metadata.ICopyright;
import org.eclipse.equinox.p2.metadata.IInstallableUnit;
import org.eclipse.equinox.p2.metadata.ILicense;
import org.eclipse.equinox.p2.query.IQueryResult;
import org.eclipse.equinox.p2.query.QueryUtil;
import org.eclipse.equinox.p2.repository.metadata.IMetadataRepository;
import org.eclipse.equinox.p2.repository.metadata.IMetadataRepositoryManager;
import org.eclipse.wtp.releng.tests.TestActivator;

/**
 * Tests that licenses in the helios repository are consistent with the
 * platform feature license.
 * 
 * This was based on test attached to bug 306627
 * https://bugs.eclipse.org/bugs/show_bug.cgi?id=306627
 */
public class TestRepo extends TestCase {
    private static final String NBSP                   = " &nbsp; ";
    static final String         JUNIT_REPORT_OUTPUT    = "junit-report-output";
    private static final String EXPECTED_PROVIDER_NAME = "Eclipse Web Tools Platform";
    static final String         defaultURLToTest       = "http://download.eclipse.org/releases/staging";

    public static Test suite() {
        System.out.println("suite for layout");
        return new TestSuite(TestRepo.class);
    }

    private static final String  EOL               = System.getProperty("line.separator", "\n");
    private static final boolean DEBUG             = false;
    private static final String  OLD_PROVIDER_NAME = "Eclipse.org";
    private String               repoURLToTest;

    public void testLicenses() throws URISyntaxException, ProvisionException, OperationCanceledException, IOException {
        String repoURL = getrepoURLToTest();
        // String referenceFeature = System.getProperty("referenceFeature");
        // System.out.println("referenceFeature: " + referenceFeature);
        // if (referenceFeature == null) {
        // referenceFeature = defaultReferenceFeature;
        // }
        // System.out.println("referenceFeature: " + referenceFeature);
        URI repoLocation = new URI(repoURL);
        IMetadataRepository repo = getMetadataRepositoryManager().loadRepository(repoLocation, null);
        if (repo == null) {
            System.out.println("no repository found at " + repoLocation.toString());
        }
        else {

            IQueryResult<IInstallableUnit> allFeatures = repo.query(QueryUtil.createIUGroupQuery(), null);
            // IQueryResult<IInstallableUnit> platform =
            // allFeatures.query(QueryUtil.createIUQuery(referenceFeature),
            // null);
            assertFalse(allFeatures.isEmpty());
            // if (platform == null) {
            // System.out.println("did not find reference feature: " +
            // referenceFeature);
            // System.out.println("list of existing features: ");
            // Iterator<IInstallableUnit> itFeatures = allFeatures.iterator();
            // while (itFeatures.hasNext()) {
            // System.out.println(itFeatures.next());
            // }
            // }
            // assertNotNull(platform);
            // assertFalse(platform.isEmpty());
            // IInstallableUnit platformFeature = platform.iterator().next();
            // ILicense platformLicense =
            // platformFeature.getLicenses(null).iterator().next();

            Properties properties = new Properties();
            InputStream inStream = this.getClass().getResourceAsStream("standard.properties");
            properties.load(inStream);
            String body = properties.getProperty("license");
            ILicense standardLicense = new License(null, body, null);

            List<IInstallableUnit> noLicense = new ArrayList<IInstallableUnit>();
            List<IInstallableUnit> extraLicense = new ArrayList<IInstallableUnit>();
            List<IInstallableUnit> goodLicense = new ArrayList<IInstallableUnit>();
            List<IInstallableUnit> badLicense = new ArrayList<IInstallableUnit>();
            checkLicenses(standardLicense, allFeatures, goodLicense, badLicense, noLicense, extraLicense);

            printReport(goodLicense, badLicense, noLicense, extraLicense);
        }
    }

    private void printReport(List<IInstallableUnit> goodLicense, List<IInstallableUnit> badLicense, List<IInstallableUnit> noLicense, List<IInstallableUnit> extraLicense) {
        String SPACER = "<br />=======================";

        FileWriter outfileWriter = null;
        File outfile = null;
        String testDirName = System.getProperty(JUNIT_REPORT_OUTPUT);
        try {
            outfile = new File(testDirName, "licenseConsistency.html");
            outfileWriter = new FileWriter(outfile);

            println(outfileWriter, "<br /><br />Summary:" + SPACER);
            println(outfileWriter, "Features with conforming license: " + goodLicense.size());
            println(outfileWriter, "Features with different license: " + badLicense.size());
            println(outfileWriter, "Features with no license: " + noLicense.size());
            println(outfileWriter, "Features with extra licenses: " + extraLicense.size());
            println(outfileWriter, "=======================");

            println(outfileWriter, "<br /><br />Details:" + SPACER);

            println(outfileWriter, "Features with no license:" + SPACER);
            for (IInstallableUnit unit : sort(noLicense)) {
                println(outfileWriter, unit.getId());
            }

            println(outfileWriter, "<br /><br />Features with different license:" + SPACER);
            for (IInstallableUnit unit : sort(badLicense)) {
                println(outfileWriter, unit.getId());
            }

            println(outfileWriter, "<br /><br />Features with matching license:" + SPACER);
            for (IInstallableUnit unit : sort(goodLicense)) {
                println(outfileWriter, unit.getId());
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (outfileWriter != null) {
                try {
                    outfileWriter.close();
                }
                catch (IOException e) {
                    // weirdness
                    e.printStackTrace();
                }
            }
        }
        if ((badLicense.size() > 0) || (extraLicense.size() > 0) || (noLicense.size() > 0)) {
            fail("Errors in license consistency. For list, see " + outfile.getAbsolutePath());
        }
    }

    private void println(FileWriter out, String string) throws IOException {
        out.write("<li>" + string + "</li>" + EOL);

    }

    private List<IInstallableUnit> sort(List<IInstallableUnit> noLicense) {
        Collections.sort(noLicense);
        return noLicense;
    }

    private void checkLicenses(ILicense platformLicense, IQueryResult<IInstallableUnit> allFeatures, List<IInstallableUnit> goodLicense, List<IInstallableUnit> badLicense, List<IInstallableUnit> noLicense, List<IInstallableUnit> extraLicense) {
        for (IInstallableUnit feature : allFeatures.toUnmodifiableSet()) {
            // ignore groups that are not features
            if (!feature.getId().endsWith(".feature.group")) {
                continue;
            }
            Collection<ILicense> licenses = feature.getLicenses(null);
            if (licenses.isEmpty()) {
                noLicense.add(feature);
                continue;
            }
            if (licenses.size() != 1) {
                extraLicense.add(feature);
                continue;
            }
            ILicense featureLicense = licenses.iterator().next();
            if (!platformLicense.getUUID().equals(featureLicense.getUUID())) {
                badLicense.add(feature);
                continue;
            }
            goodLicense.add(feature);
        }
    }

    private void checkProviderNames(IQueryResult<IInstallableUnit> allIUs) throws IOException {
        FileWriter outfileWriter = null;
        File outfile = null;
        List<IInstallableUnit> incorrectProviderName = new ArrayList<IInstallableUnit>();
        List<IInstallableUnit> correctProviderName = new ArrayList<IInstallableUnit>();
        List<IInstallableUnit> incorrectOldProviderName = new ArrayList<IInstallableUnit>();
        List<IInstallableUnit> probablyCorrectProviderName = new ArrayList<IInstallableUnit>();
        String testDirName = System.getProperty(JUNIT_REPORT_OUTPUT);
        try {
            outfile = new File(testDirName, "providerNames.html");
            outfileWriter = new FileWriter(outfile);
            System.out.println("output: " + outfile.getAbsolutePath());
            for (IInstallableUnit iu : allIUs.toUnmodifiableSet()) {
                try {
                    // ignore categories
                    boolean isCategory = "true".equals(iu.getProperty("org.eclipse.equinox.p2.type.category"));
                    // TODO: should we exclude fragments?
                    boolean isFragment = "true".equals(iu.getProperty("org.eclipse.equinox.p2.type.fragment"));
                    /*
                     * TODO: what are these special things? What ever they
                     * are, they have no provider name. config.a.jre is
                     * identified as a fragment
                     * (org.eclipse.equinox.p2.type.fragment). a.jre has no
                     * properties.
                     */
                    boolean isSpecial = "a.jre".equals(iu.getId()) || "config.a.jre".equals(iu.getId());
                    if (!isCategory && !isSpecial) {
                        String providerName = iu.getProperty(IInstallableUnit.PROP_PROVIDER, null);
                        if (EXPECTED_PROVIDER_NAME.equals(providerName)) {
                            correctProviderName.add(iu);
                        }
                        else if (OLD_PROVIDER_NAME.equals(providerName)) {
                            incorrectOldProviderName.add(iu);
                        }
                        else if ((providerName != null) && providerName.startsWith("Eclipse")) {
                            probablyCorrectProviderName.add(iu);
                        }
                        else {
                            incorrectProviderName.add(iu);
                        }
                        // experiment to find configs and categories
                        if (DEBUG) {
                            if (providerName == null) {
                                printProperties(outfileWriter, iu);
                            }
                        }
                    }

                }
                catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }

            outfileWriter.write("<h1>Provider names used in repository</h1>" + EOL);
            outfileWriter.write("<p>Repository ('repoURLToTest'): " + getrepoURLToTest() + "</p>" + EOL);
            outfileWriter.write("<p>Note, there are several problems with this test, e.g. see <a href=\"https://bugs.eclipse.org/bugs/show_bug.cgi?id=309566\">bug 309566</a>. So provided here as FYI for now.</p>" + EOL);
            outfileWriter.write("<h2>Major missing, or possibly incorrect provider name</h2>" + EOL);
            printLinesProvider(outfileWriter, incorrectProviderName);
            outfileWriter.write("<h2>Minor problem of using old style provider name (todo: filter out non-wtp ones)</h2>" + EOL);
            printLinesProvider(outfileWriter, incorrectOldProviderName);
            outfileWriter.write("<h2>Probably correct provider name</h2>" + EOL);
            printLinesProvider(outfileWriter, probablyCorrectProviderName);
            outfileWriter.write("<h2>Using correct provider name</h2>" + EOL);
            printLinesProvider(outfileWriter, correctProviderName);

            // don't count as error until bug 309566 fixed
            // https://bugs.eclipse.org/bugs/show_bug.cgi?id=309566
//			if (incorrectProviderName.size() > 0) {
//				fail("Errors in consistent, correct provider name. For list, see " + outfile.getAbsolutePath());
//			}
        }
        finally {
            if (outfileWriter != null) {
                try {
                    outfileWriter.close();
                }
                catch (IOException e) {
                    // would be weird
                    e.printStackTrace();
                }
            }
        }
    }

    private void printLinesProvider(FileWriter out, List<IInstallableUnit> iuList) throws IOException {
        Comparator<? super IInstallableUnit> comparatorProviderName = new Comparator<IInstallableUnit>() {

            public int compare(IInstallableUnit iu1, IInstallableUnit iu2) {
                // neither iu should be null ... but, just to cover all cases
                if ((iu1 == null) && (iu2 == null)) {
                    return 0;
                }
                else if ((iu1 == null) || (iu2 == null)) {
                    return 1;
                }
                else {
                    String p1 = iu1.getProperty(IInstallableUnit.PROP_PROVIDER, null);
                    String p2 = iu2.getProperty(IInstallableUnit.PROP_PROVIDER, null);
                    if (p1 == null) {
                        p1 = "null";
                    }
                    if (p2 == null) {
                        p2 = "null";
                    }
                    int result = p1.compareTo(p2);
                    if (result == 0) {
                        // if provider names are equal, use id as secondary
                        // sort key
                        result = iu1.getId().compareTo(iu2.getId());
                    }
                    return result;
                }
            }

        };
        Collections.sort(iuList, comparatorProviderName);
        out.write("<p>Count: " + iuList.size() + EOL);
        out.write("<ol>" + EOL);

        for (IInstallableUnit iu : iuList) {
            printLineListItem(out, iu);
        }
        out.write("</ol>" + EOL);
    }

    private void printLinesCopyright(FileWriter out, List<IInstallableUnit> iuList) throws IOException {
        Collections.sort(iuList);
        out.write("<p>Count: " + iuList.size() + EOL);
        out.write("<ol>" + EOL);

        for (IInstallableUnit iu : iuList) {
            printLineCopyright(out, iu);
        }
        out.write("</ol>" + EOL);
    }

    private void printLineListItem(FileWriter outfileWriter, IInstallableUnit iu) throws IOException {
        String providerName = iu.getProperty(IInstallableUnit.PROP_PROVIDER, null);
        String iuId = iu.getId();
        String iuVersion = iu.getVersion().toString();
        println(outfileWriter, providerName + NBSP + iuId + NBSP + iuVersion + NBSP);
    }

    private void printLineCopyright(FileWriter outfileWriter, IInstallableUnit iu) throws IOException {

        String copyright = null;
        ICopyright copyrightIu = iu.getCopyright(null);
        if (copyrightIu != null) {
            copyright = copyrightIu.getBody();
        }
        String iuId = iu.getId();
        String iuVersion = iu.getVersion().toString();
        println(outfileWriter, copyright + NBSP + iuId + NBSP + iuVersion + NBSP);
    }

    private void printProperties(FileWriter outFileWriter, IInstallableUnit iu) throws IOException {
        Map<String, String> properties = iu.getProperties();
        Set keys = properties.keySet();
        for (Object key : keys) {
            String value = properties.get(key);
            println(outFileWriter, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + key + " : " + value);
        }

    }

    public void testProviderNames() throws URISyntaxException, ProvisionException, OperationCanceledException, IOException {
        IQueryResult<IInstallableUnit> allIUs = getAllIUs();
        checkProviderNames(allIUs);
    }

    public void testCopyrights() throws URISyntaxException, ProvisionException, OperationCanceledException, IOException {
        IQueryResult<IInstallableUnit> allIUs = getAllIUs();
        checkCopyrights(allIUs);
    }

    private IQueryResult<IInstallableUnit> getAllIUs() throws URISyntaxException, ProvisionException {
        String repoURL = getrepoURLToTest();
        URI repoLocation = new URI(repoURL);
        IMetadataRepository repo = getMetadataRepositoryManager().loadRepository(repoLocation, null);
        assertNotNull("no repository found at " + repoLocation.toString(), repo);
        IQueryResult<IInstallableUnit> allIUs = repo.query(QueryUtil.createIUAnyQuery(), null);
        assertFalse(allIUs.isEmpty());
        return allIUs;
    }

    private String getrepoURLToTest() {
        if (repoURLToTest == null) {
            repoURLToTest = System.getProperty("repoURLToTest");

            if (repoURLToTest == null) {
                repoURLToTest = defaultURLToTest;
                System.out.println("the 'repoURLToTest' property was not defined, will use default");
            }
            System.out.println("repoURLToTest: " + repoURLToTest);
        }
        return repoURLToTest;
    }

    private void checkCopyrights(IQueryResult<IInstallableUnit> allIUs) throws IOException {
        FileWriter outfileWriter = null;
        File outfile = null;
        List<IInstallableUnit> correctcopyright = new ArrayList<IInstallableUnit>();
        List<IInstallableUnit> nocopyright = new ArrayList<IInstallableUnit>();
        List<IInstallableUnit> incorrectcopyright = new ArrayList<IInstallableUnit>();
        String testDirName = System.getProperty(JUNIT_REPORT_OUTPUT);
        try {
            outfile = new File(testDirName, "copyrights.html");
            outfileWriter = new FileWriter(outfile);
            System.out.println("output: " + outfile.getAbsolutePath());
            for (IInstallableUnit iu : allIUs.toUnmodifiableSet()) {
                // ignore categories
                boolean isCategory = "true".equals(iu.getProperty("org.eclipse.equinox.p2.type.category"));
                // TODO: should we exclude fragments?
                boolean isFragment = "true".equals(iu.getProperty("org.eclipse.equinox.p2.type.fragment"));
                // TODO: what are these special things? What ever they are,
                // they have no provider name.
                // config.a.jre is identified as a fragment
                // (org.eclipse.equinox.p2.type.fragment).
                // a.jre has no properties.
                boolean isSpecial = "a.jre".equals(iu.getId()) || "config.a.jre".equals(iu.getId());
                if (!isCategory && !isSpecial) {
                    ICopyright copyright = iu.getCopyright(null);
                    if (copyright == null) {
                        nocopyright.add(iu);
                    }
                    /*
                     * This heuristic may need to be improved. Note that case
                     * is important, as a common error is to have the property
                     * 'copyright' but no text.
                     */
                    else if (copyright.getBody().trim().startsWith("Copyright")) {
                        correctcopyright.add(iu);
                    }
                    else {
                        incorrectcopyright.add(iu);
                    }

                }
            }

            outfileWriter.write("<h1>Copyrights used in latest build</h1>" + EOL);
            outfileWriter.write("<p>This (simple) test is base on heuristic that a valid copyright is not null and starts with 'Copyright'. </p>" + EOL);
            outfileWriter.write("<h2>key only? or incorrect copyright?</h2>" + EOL);
            printLinesCopyright(outfileWriter, incorrectcopyright);
            outfileWriter.write("<h2>No copyright. (which is ok and expected for bundle IUs, as far as is known)</h2>" + EOL);
            printLinesCopyright(outfileWriter, nocopyright);
            outfileWriter.write("<h2>Apparently using copyright, based on heuristic.</h2>" + EOL);
            printLinesCopyright(outfileWriter, correctcopyright);

            // note we don't treat missing ones as errors. I'm not sure it is
            // _required_ for all bundles?
            if (incorrectcopyright.size() > 0) {
                fail("Errors in copyright statement? For list, see " + outfile.getAbsolutePath());
            }
        }
        finally {
            if (outfileWriter != null) {
                try {
                    outfileWriter.close();
                }
                catch (IOException e) {
                    // would be weird
                    e.printStackTrace();
                }
            }
        }
    }

    protected static IMetadataRepositoryManager getMetadataRepositoryManager() {
        return (IMetadataRepositoryManager) getAgent().getService(IMetadataRepositoryManager.SERVICE_NAME);
    }

    protected static IProvisioningAgent getAgent() {
        // get the global agent for the currently running system
        return (IProvisioningAgent) ServiceHelper.getService(TestActivator.getContext(), IProvisioningAgent.SERVICE_NAME);
    }
}
