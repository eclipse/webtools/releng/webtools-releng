/*******************************************************************************
 * Copyright (c) 2010, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tests;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;

public class ComparatorLog {


    public static final String              COMPARATOR_UNEXPECTED_MESSAGES_LOG_TXT = "comparatorUnexpectedMessages.log.txt";
    public static final String              COMPARATOR_EXCLUDED_MESSAGES_LOG_TXT   = "comparatorExcludedMessages.log.txt";
    public static final String              COMPARATOR_ALL_MESSAGES_LOG_TXT        = "comparatorAllMessages.log.txt";
    private String                          comparatorLogFile;
    private String                          summaryLogDir;
    private String                          overallsummary;
    private ArrayList<ComparatorLogMessage> allMessages;
    private ArrayList<ComparatorLogMessage> excludedMessages;
    private ArrayList<ComparatorLogMessage> significantMessages;
    private ArrayList<RuleData>             comparatorRules;
    private ComparatorRules                 comparatorRulesInstance                = new ComparatorRules();
    /**
     * Used to tell us where the comparator.log file is. Example:
     * -DabsolutionComparatorLogLocation=
     * 'D:\\builds\\workspaces\\workspaceWTPHead\\org.eclipse.wtp.releng.tests
     * \ \ c o m p a r a t o r . l o g '
     */
    private final static String             COMPARATOR_LOG_PARAM                   = "comparatorLogLocation";

    /**
     * Used to tell us where to put summary files. Example: -DsummaryLogDir=
     * 'D:\\builds\\workspaces\\workspaceWTPHead\\org.eclipse.wtp.releng.tests
     * \ \ c o m p a r a t o r . l o g '
     */
    private final static String             SUMMARY_LOG_DIR_PARAM                  = "comparatorTestSummaryLogDir";
    /*
     * Utility constants
     */
    private static final String             LINE_SEPARATOR_PROPERTY_NAME           = "line.separator";
    private static final String             PATH_SEPARATOR_PROPERTY_NAME           = "path.separator";
    private static String                   EOL                                    = System.getProperty(LINE_SEPARATOR_PROPERTY_NAME);
    private static String                   PATH_SEPERATOR                         = System.getProperty(PATH_SEPARATOR_PROPERTY_NAME);

    public static void main(String[] args) {
        try {


            ComparatorLog comparatorLog = new ComparatorLog();
            int n = comparatorLog.getNumberOfUnexpectedMessages();
            System.out.println("Number of unexpected messages from comparator: " + n);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    // defaults purely for local tests
    private String defaultComparatorLogFile = "D:\\builds\\workspaces\\workspaceWTPHead\\org.eclipse.wtp.releng.tests\\comparator.log";
    private String defaultsummaryLogDir     = System.getProperty("user.dir");


    public ComparatorLog() {
        super();
    }

    public ComparatorLog(String comparatorLogLocation, String summaryLogDir) {
        super();
        this.comparatorLogFile = comparatorLogLocation;
        this.summaryLogDir = summaryLogDir;
    }

    private void dofilterMessages(ArrayList<ComparatorLogMessage> allMessages, ArrayList<ComparatorLogMessage> excludedMessages, ArrayList<ComparatorLogMessage> significantMessages) throws IOException {
        for (int i = 0; i < allMessages.size(); i++) {

            ComparatorLogMessage comparatorLogMessage = allMessages.get(i);
            if (matchesExcludRule(comparatorLogMessage)) {
                excludedMessages.add(comparatorLogMessage);
            }
            else {
                significantMessages.add(comparatorLogMessage);
            }
        }

    }

    private void filterMessages() throws IOException {

        allMessages = getMessages();
        excludedMessages = new ArrayList<ComparatorLogMessage>();
        significantMessages = new ArrayList<ComparatorLogMessage>();

        dofilterMessages(allMessages, excludedMessages, significantMessages);
    }

    private ArrayList<RuleData> getComparatorExcludeRules() throws IOException {
        if (comparatorRules == null) {

            comparatorRules = getComparatorRulesInstance().getDataRules();

        }
        return comparatorRules;
    }

    private ComparatorRules getComparatorRulesInstance() {

        return comparatorRulesInstance;
    }

    public String getComparatorLogFile() {
        if (comparatorLogFile == null) {
            comparatorLogFile = System.getProperty(COMPARATOR_LOG_PARAM);

            if ((comparatorLogFile == null) || (comparatorLogFile.length() <= 0)) {
                /*
                 * none provided, no good defaults for production, but we have
                 * a value for quick tests purely for local tests. Or, could
                 * FAIL here, for easier debugging.
                 */
                // comparatorLogFile = defaultComparatorLogFile;
                throw new IllegalArgumentException("Comparator: found no value for log location in " + COMPARATOR_LOG_PARAM);
            }
            else {
                if (!comparatorLogFile.endsWith("/")) {
                    comparatorLogFile = comparatorLogFile + "/";
                }
                System.out.println("Comparator log location: " + COMPARATOR_LOG_PARAM + " specified " + comparatorLogFile);
            }
        }
        return comparatorLogFile;
    }

    public String getComparatorOutputSummaryDir() {
        if (summaryLogDir == null) {
            summaryLogDir = System.getProperty(SUMMARY_LOG_DIR_PARAM);
            if ((summaryLogDir == null) || (summaryLogDir.length() <= 0)) {
                /*
                 * none provided, no good defaults for production, but we have
                 * a value for quick tests purely for local tests. Or, could
                 * FAIL here, for better debugging?
                 */
                summaryLogDir = defaultsummaryLogDir;

            }
            if (summaryLogDir != null) {
                File tempDir = new File(summaryLogDir);
                if (!tempDir.exists()) {
                    tempDir.mkdirs();
                    System.out.println("Comparator log: created output directory at " + tempDir);
                }
                else {
                    System.out.println("Comparator log: using output directory at " + tempDir);
                }
            }
        }
        return summaryLogDir;
    }

    /*
     * Note: returns null, if at EOF, as "readline" does. 
     */
    private String getLine(BufferedReader logFile) throws IOException {
        String line = logFile.readLine();
        if (line != null) {
            line = line.trim();
            // unclear why/when the comparator prints out an extra time stamp header, but 
            // if we encounter one, we'll skip it. 
            // TODO: consider if recursize call would be more appropriate here
            if (line.startsWith("org.eclipse.equinox.p2.repository.tools.jar.comparator")) {
                line = logFile.readLine();
                if (line != null) {
                    line.trim();
                }
            }
        }
        return line;
    }

    private BufferedReader getLogFile() throws FileNotFoundException {

        BufferedReader reader = null;
        String logFileLocationAndName = null;
        try {
            // assume (force) comparatorLogFile to end with slash
            logFileLocationAndName = getComparatorLogFile() + "comparator.log";
            File logFile = new File(logFileLocationAndName);
            Reader log = new FileReader(logFile);
            reader = new BufferedReader(log);
        }
        catch (FileNotFoundException e) {
            throw new FileNotFoundException("Comparator log not found where expected at " + logFileLocationAndName);
        }
        return reader;


    }

    private BufferedReader getLogFileTests() throws FileNotFoundException {

        BufferedReader reader = null;
        String logFileLocationAndName = null;
        try {
            // assume (force) comparatorLogFile to end with slash
            logFileLocationAndName = getComparatorLogFile() + "comparator-unittest.log";
            File logFile = new File(logFileLocationAndName);
            Reader log = new FileReader(logFile);
            reader = new BufferedReader(log);
        }
        catch (FileNotFoundException e) {
            throw new FileNotFoundException("Comparator log not found where expected at " + logFileLocationAndName);
        }
        return reader;


    }

    private ArrayList<ComparatorLogMessage> getMessages() throws IOException {
        ArrayList<ComparatorLogMessage> loglist = new ArrayList<ComparatorLogMessage>();

        // first get regular "code" messages
        BufferedReader logFile = getLogFile();
        getMessageObjects(loglist, logFile);
        logFile.close();
        // now get test bundle messages
        BufferedReader logFileTests = getLogFileTests();
        getMessageObjects(loglist, logFileTests);
        logFileTests.close();


        return loglist;
    }

    private void getMessageObjects(ArrayList<ComparatorLogMessage> loglist, BufferedReader logFile) throws IOException {
        String line = null;
        // set summary for this whole ComparatorLog
        setSummary(logFile.readLine());
        boolean nextstarted = false;
        // remaining lines come in triplets
        while (logFile.ready()) {
            ComparatorLogMessage item = new ComparatorLogMessage();
            // if first line is null, skip rest, we must be at end. If not
            // at first of triplet, convert nulls to empy strings.
            if (!nextstarted) {
                line = getLine(logFile);
            }
            nextstarted = false;
            if (line != null) {
                line = line.trim();
                // something is likely wrong with input, or program, if this line is empty
                // but for now, we'll just avoid setting anything (instead of throwing error). 
                if (line.length() > 0) {
                    item.setSummary(line);


                    // continue until next keywords of "canonical:" or "optimized:" or EOF
                    line = getLine(logFile);
                    item.setComparison(line);

                    line = getLine(logFile);
                    if (line.trim().startsWith("Difference found")) {
                        //TODO: allow mulitiply line 'comparaison' ... perhaps hava "addComparison" line? 
                        line = getLine(logFile);
                    }
                    item.setReason(line);
                    // Some items have 4 or 5 lines
                    boolean foundnext = false;
                    do {
                        line = getLine(logFile);
                        if ((line != null) && (line.startsWith("canonical:") || line.startsWith("optimized:"))) {
                            nextstarted = true;
                            foundnext = true;
                        }
                        else {
                            if ((line != null) && (line.length() > 0)) {
                                item.setExtraDetail(line);
                            }
                            else {
                                // assume if line is null, that we are at EOF
                                foundnext = true;
                            }
                        }
                    }
                    while (!foundnext);


                    loglist.add(item);
                }
            }
        }
    }

    public int getNumberOfUnexpectedMessages() throws IOException {
        filterMessages();
        printReportFilesAll();
        printReportFilesExcluded();
        printReportFilesSignificant();
        return significantMessages.size();
    }

    private String getSummary() {
        return this.overallsummary;
    }

    private boolean matchesExcludRule(ComparatorLogMessage comparatorLogMessage) throws IOException {

        boolean result = false;
        ArrayList<RuleData> excludeRules = getComparatorExcludeRules();

        for (Iterator iterator = excludeRules.iterator(); iterator.hasNext();) {
            RuleData ruleData = (RuleData) iterator.next();

            Matcher matcherSummary = ruleData.getSummary().matcher(comparatorLogMessage.getSummary());
            Matcher matcherComparison = ruleData.getComparison().matcher(comparatorLogMessage.getComparison());
            Matcher matcherReason = ruleData.getReason().matcher(comparatorLogMessage.getReason());
            boolean matchesSummmary = matcherSummary.matches();
            boolean matchesComparison = matcherComparison.matches();
            boolean matchesReason = matcherReason.matches();
            result = matchesSummmary && matchesComparison && matchesReason;
            // TODO: we could track (count) how many messages excluded for which reasons
            // stop iterating, if we find one reason to exculde
            if (result) {
                break;
            }
        }
        return result;


    }

    private void print(ComparatorLogMessage comparatorLogMessage, Writer output) throws IOException {
        output.write(comparatorLogMessage.getSummary() + EOL);
        output.write(comparatorLogMessage.getComparison() + EOL);
        output.write(comparatorLogMessage.getReason() + EOL);
        List<String> extra = comparatorLogMessage.getExtraDetail();
        if (extra != null) {
            for (Iterator iterator = extra.iterator(); iterator.hasNext();) {
                String extraDetail = (String) iterator.next();
                output.write(extraDetail + EOL);
            }

        }

    }

    /*
     * ArrayList<ComparatorLogMessage> allMessages,
     * ArrayList<ComparatorLogMessage> excludedMessages,
     * ArrayList<ComparatorLogMessage> significantMessages
     */
    private void printReportFilesAll() throws IOException {

        BufferedWriter output = null;
        try {
            File outfile = new File(getComparatorOutputSummaryDir(), COMPARATOR_ALL_MESSAGES_LOG_TXT);
            FileWriter out = new FileWriter(outfile);
            output = new BufferedWriter(out);
            output.write(EOL + EOL + "All Comparator Messages" + EOL + EOL);
            output.write(EOL + COMPARATOR_LOG_PARAM + ": " + getComparatorLogFile() + EOL);
            output.write(EOL + SUMMARY_LOG_DIR_PARAM + ": " + getComparatorOutputSummaryDir() + EOL);
            output.write(EOL + "comparatorFilterRules" + ": " + getComparatorRulesInstance().getFilterPropertyFileLocation() + EOL);
            output.write(EOL);
            output.write(getSummary() + EOL + EOL);

            int count = 1;
            for (Iterator iterator = allMessages.iterator(); iterator.hasNext();) {
                ComparatorLogMessage msg = (ComparatorLogMessage) iterator.next();
                output.write(EOL + "Message " + count++ + EOL);
                print(msg, output);
            }
        }
        finally {
            if (output != null) {
                output.close();
            }
        }
    }

    /*
     * ArrayList<ComparatorLogMessage> allMessages,
     * ArrayList<ComparatorLogMessage> excludedMessages,
     * ArrayList<ComparatorLogMessage> significantMessages
     */
    private void printReportFilesExcluded() throws IOException {

        BufferedWriter output = null;
        try {
            File outfile = new File(getComparatorOutputSummaryDir(), COMPARATOR_EXCLUDED_MESSAGES_LOG_TXT);
            FileWriter out = new FileWriter(outfile);
            output = new BufferedWriter(out);
            output.write(EOL + EOL + "Non-significant (expected) Comparator Messages" + EOL);
            output.write(EOL + "Using filter properties from: " + getComparatorRulesInstance().getFilterPropertyFileLocation() + EOL);
            output.write(EOL + getSummary() + EOL + EOL);

            int count = 1;
            for (Iterator iterator = excludedMessages.iterator(); iterator.hasNext();) {
                ComparatorLogMessage msg = (ComparatorLogMessage) iterator.next();
                output.write(EOL + "Message " + count++ + EOL);
                print(msg, output);
            }
        }
        finally {
            if (output != null) {
                output.close();
            }
        }
    }

    /*
     * ArrayList<ComparatorLogMessage> allMessages,
     * ArrayList<ComparatorLogMessage> excludedMessages,
     * ArrayList<ComparatorLogMessage> significantMessages
     */
    private void printReportFilesSignificant() throws IOException {

        BufferedWriter output = null;
        try {
            File outfile = new File(getComparatorOutputSummaryDir(), COMPARATOR_UNEXPECTED_MESSAGES_LOG_TXT);
            FileWriter out = new FileWriter(outfile);
            output = new BufferedWriter(out);
            output.write(EOL + EOL + "Significant (Unexpected) Comparator Messages" + EOL + EOL);

            output.write(getSummary() + EOL + EOL);

            int count = 1;
            for (Iterator iterator = significantMessages.iterator(); iterator.hasNext();) {
                ComparatorLogMessage msg = (ComparatorLogMessage) iterator.next();
                output.write(EOL + "Message " + count++ + EOL);
                print(msg, output);

            }
        }
        finally {
            if (output != null) {
                output.close();
            }
        }
    }

    public void setComparatorLogFile(String comparatorLogFile) {
        this.comparatorLogFile = comparatorLogFile;
    }

    public void setComparatorOutputSummaryDir(String comparatorOutputSummaryDir) {
        this.summaryLogDir = comparatorOutputSummaryDir;
    }

    private void setSummary(String summary) {
        this.overallsummary = summary;
    }


}
