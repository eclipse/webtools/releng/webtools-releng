/*******************************************************************************
 * Copyright (c) 2010, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;

public class ComparatorRules {

    private String              filterPropertyFileLocation        = null;
    private final static String COMPARATOR_FILTER_RULES           = "comparatorFilterRules";
    String                      defaultfilterPropertyFileLocation = "/home/davidw/builds/workspaces/wtpHead/releng/maps/comparatorfilter.properties";

    public static void main(String[] args) {

        try {
            ComparatorRules comparatorRules = new ComparatorRules();
            ArrayList<RuleData> testlist = comparatorRules.getDataRules();
            for (Iterator iterator = testlist.iterator(); iterator.hasNext();) {
                RuleData ruleData = (RuleData) iterator.next();
                System.out.println(ruleData);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Properties getRuleProperties() throws IOException {
        Properties filterProperties = null;

        String fileLocationAndName = getFilterPropertyFileLocation();
        File file = new File(fileLocationAndName);

        InputStream inStream = new FileInputStream(file);

        filterProperties = new Properties();
        filterProperties.load(inStream);

        inStream.close();

        return filterProperties;
    }

    public ArrayList<RuleData> getDataRules() throws IOException {

        ArrayList<RuleData> ruleList = new ArrayList<RuleData>();
        Properties filterProperties = getRuleProperties();

        Enumeration filterKeys = filterProperties.propertyNames();

        while (filterKeys.hasMoreElements()) {
            String key = (String) filterKeys.nextElement();

            String rulename = getRulename(key);
            /*
             * Once we have name, get all three parts, removing them, as we
             * go, to avoid multiple processing
             */
            String summarykey = "comparator." + rulename + ".summary";
            String summary = (String) filterProperties.remove(summarykey);
            String comparisonkey = "comparator." + rulename + ".comparison";
            String comparison = (String) filterProperties.remove(comparisonkey);
            String reasonkey = "comparator." + rulename + ".reason";
            String reason = (String) filterProperties.remove(reasonkey);

            /*
             * if all three values are null ... then we're "looping" based on
             * difference beween the enumeration that we already got, vs the
             * properties, which we've modified ... seems like a hack,
             * somehow?
             */
            if (!((summary == null) && (comparison == null) && (reason == null))) {
                ruleList.add(new RuleData(rulename, summary, comparison, reason));
            }
        }

        return ruleList;

    }

    /*
     * Returns middle portion of three part key as the name of the rule
     */
    private String getRulename(String key) {
        StringTokenizer namefinder = new StringTokenizer(key, ".");
        if (namefinder.countTokens() != 3) {
            {
                throw new IllegalStateException("Invalid property found in comparator rule property file: key without 3 parts");
            }
        }
        String namespace = namefinder.nextToken();
        if (!namespace.equals("comparator")) {
            throw new IllegalStateException("Invalid property found in comparator rule property file: not of 'comparator' namespace");
        }
        String rulename = namefinder.nextToken();
        // sanity check on format of property files
        String ruletype = namefinder.nextToken();
        if (!(ruletype.equals("summary") || ruletype.equals("comparison") || ruletype.equals("reason"))) {
            throw new IllegalStateException("Invalid property found in comparator rule property file: unrecognized rule type");
        }

        return rulename;
    }

    public String getFilterPropertyFileLocation() {

        if (filterPropertyFileLocation == null) {
            filterPropertyFileLocation = System.getProperty(COMPARATOR_FILTER_RULES);
            if ((filterPropertyFileLocation == null) || (filterPropertyFileLocation.length() <= 0)) {
                /*
                 * none provided, no good defaults for production, but we have
                 * a value for quick tests purely for local tests. Or, could
                 * FAIL here, for better debugging.
                 */
                filterPropertyFileLocation = defaultfilterPropertyFileLocation;

            }
        }
        return filterPropertyFileLocation;
    }

    public void setFilterPropertyFileLocation(String filterPropertyFileLocation) {
        this.filterPropertyFileLocation = filterPropertyFileLocation;
    }
}
