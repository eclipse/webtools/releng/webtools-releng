/*******************************************************************************
 * Copyright (c) 2010, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tests;

import java.util.regex.Pattern;


public class RuleData {



    private static final Pattern MATCH_ANY = Pattern.compile(".*");



    public RuleData(String name, String summary, String comparison, String reason) {
        this();
        this.name = name;
        setSummary(summary);
        setComparison(comparison);
        setReason(reason);
    }

    public RuleData() {
        super();

    }


    private String  name;
    private Pattern summary;
    private Pattern comparison;
    private Pattern reason;



    public Pattern getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        if ((summary == null) || summary.equals("")) {
            this.summary = MATCH_ANY;
        }
        else {
            this.summary = Pattern.compile(summary);
        }

    }

    public Pattern getComparison() {
        return comparison;
    }

    public void setComparison(String comparison) {
        if ((comparison == null) || comparison.equals("")) {
            this.comparison = MATCH_ANY;
        }
        else {
            this.comparison = Pattern.compile(comparison);
        }
    }

    public Pattern getReason() {
        return reason;
    }

    public void setReason(String reason) {
        if ((reason == null) || reason.equals("")) {
            this.reason = MATCH_ANY;
        }
        else {
            this.reason = Pattern.compile(reason);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RuleData [name=" + name + ", summary=" + summary.pattern() + ", comparison=" + comparison.pattern() + ", reason=" + reason.pattern() + "]";
    }



}
