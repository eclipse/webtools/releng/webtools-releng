/*******************************************************************************
 * Copyright (c) 2006, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tests;

import org.eclipse.wtp.layout.tests.TestLayout;
import org.eclipse.wtp.layout.tests.TestRepo;

import junit.framework.Test;
import junit.framework.TestSuite;

public class TestBuild extends TestSuite {
	public static Test suite() {
		return new TestBuild();
	}

	public TestBuild() {
		super("Build Test Suite");
		addTest(new TestSuite(BuildTests.class, "Build Tests"));
		addTest(TestLayout.suite());
		addTest(TestRepo.suite());
	}
}	