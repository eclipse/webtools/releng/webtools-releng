/*******************************************************************************
 * Copyright (c) 2010, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tests;

import java.util.ArrayList;
import java.util.List;

public class ComparatorLogMessage {
    private String       summary;
    private String       comparison;
    private String       reason;
    private List<String> extraDetail;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getComparison() {
        return comparison;
    }

    public void setComparison(String comparison) {
        this.comparison = comparison;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<String> getExtraDetail() {
        return extraDetail;
    }

    public void setExtraDetail(String extraDetail) {
        if (this.extraDetail == null) {
            this.extraDetail = new ArrayList();
        }
        this.extraDetail.add(extraDetail);
    }
}
