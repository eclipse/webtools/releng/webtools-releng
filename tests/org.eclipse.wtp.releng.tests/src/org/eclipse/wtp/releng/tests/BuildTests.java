/*******************************************************************************
 * Copyright (c) 2000, 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import junit.framework.TestCase;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wtp.releng.tools.versionchecker.VersionLister;

public class BuildTests extends TestCase {



    private static final String LIST_TO_REFERENCE_FILE                 = "-listToReferenceFile";
    private static final String TEST_TO_REFERENCE                      = "-testToReference";
    private static final String COUNT_QUALIFIER_INCREASE_ONLY_AS_ERROR = "-countQualifierIncreaseOnlyAsError";

    public class FileSuffixFilter implements FilenameFilter {

        private String suffix;

        public FileSuffixFilter(String suffix) {
            this.suffix = suffix;
        }

        /**
         * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
         */
        public boolean accept(File dir, String name) {
            int lastDot = name.lastIndexOf('.');
            if (lastDot == -1) {
                return false;
            }
            return name.substring(lastDot).equals(suffix);
        }

    }

    public class WSTFileNameFilter implements FilenameFilter {
        String namespaceOfInterest = "org.eclipse.wst";

        public boolean accept(File dir, String name) {
            return dir.getName().startsWith(namespaceOfInterest) || name.startsWith(namespaceOfInterest);
        }
    }

    public class WTPFileNameFilter implements FilenameFilter {
        String namespaceOfInterest = "org.eclipse.wtp";

        public boolean accept(File dir, String name) {
            return dir.getName().startsWith(namespaceOfInterest) || name.startsWith(namespaceOfInterest);
        }
    }

    public class JSTFileNameFilter implements FilenameFilter {
        String namespaceOfInterest = "org.eclipse.jst";

        public boolean accept(File dir, String name) {
            return dir.getName().startsWith(namespaceOfInterest) || name.startsWith(namespaceOfInterest);
        }
    }

    public class JPTFileNameFilter implements FilenameFilter {
        String namespaceOfInterest = "org.eclipse.jpt";

        public boolean accept(File dir, String name) {
            return dir.getName().startsWith(namespaceOfInterest) || name.startsWith(namespaceOfInterest);
        }
    }

    private static final String[] REQUIRED_FEATURE_FILES            = {"epl-2.0.html", "feature.properties", "feature.xml", "license.html"};
    private static final String   REQUIRED_FEATURE_SUFFIX           = ".jpg";

    private static final String[] REQUIRED_PLUGIN_FILES             = {"about.html", "plugin.properties", "plugin.xml"};
    private static final String   REQUIRED_PLUGIN_SUFFIX            = ".jar";

    private static final String[] REQUIRED_FEATURE_PLUGIN_FILES     = {"about.html", "about.ini", "about.mappings", "about.properties", "plugin.properties", "plugin.xml"};
    private static final String   REQUIRED_FEATURE_PLUGIN_SUFFIX    = ".gif";

    private static final String[] REQUIRED_FRAGMENT_FILES           = {"fragment.xml"};
    private static final String   REQUIRED_FRAGMENT_SUFFIX          = "";

    private static final String[] REQUIRED_SWT_FRAGMENT_FILES       = {"fragment.properties"};
    private static final String   REQUIRED_SWT_FRAGMENT_SUFFIX      = "";

    private static final String[] REQUIRED_SOURCE_FILES             = {"about.html"};
    private static final String   REQUIRED_SOURCE_SUFFIX            = ".zip";

    private static final String[] REQUIRED_BUNDLE_FILES             = {"about.html"};
    private static final String   REQUIRED_BUNDLE_MANIFEST          = "MANIFEST.MF";
    private static final String   REQUIRED_BUNDLE_SUFFIX            = ".jar";

    private static final String[] SUFFIX_EXEMPT_LIST                = {"org.eclipse.swt", "org.apache.ant"};
    private static final int      EXPECTED_NUMBER_OF_TOTAL_FEATURES = 155;
    private static final int      EXPECTED_NUMBER_OF_TOTAL_PLUGINS  = 581;
    private static final int      EXPECTED_NUMBER_OF_WTP_FEATURES   = 1;
    private static final int      EXPECTED_NUMBER_OF_WTP_PLUGINS    = 5;
    private static final int      EXPECTED_NUMBER_OF_WST_FEATURES   = 43;
    private static final int      EXPECTED_NUMBER_OF_WST_PLUGINS    = 140;
    private static final int      EXPECTED_NUMBER_OF_JST_FEATURES   = 35;
    private static final int      EXPECTED_NUMBER_OF_JST_PLUGINS    = 127;
    private static final int      EXPECTED_NUMBER_OF_JPT_FEATURES   = 6;
    private static final int      EXPECTED_NUMBER_OF_JPT_PLUGINS    = 12;

    FilenameFilter                wtpFilenameFilter                 = new WTPFileNameFilter();
    FilenameFilter                wstFilenameFilter                 = new WSTFileNameFilter();
    FilenameFilter                jstFilenameFilter                 = new JSTFileNameFilter();
    FilenameFilter                jptFilenameFilter                 = new JPTFileNameFilter();

    /**
     * Constructor for EmptyDirectoriesTest.
     * 
     * @param arg0
     */
    public BuildTests(String arg0) {
        super(arg0);
    }


    public void _testFeatureFiles() {
        List result = new ArrayList();
        String installDir = Platform.getInstallLocation().getURL().getPath();
        File featureDir = new File(installDir, "features");
        File[] features = featureDir.listFiles();

        for (int i = 0; i < features.length; i++) {
            File aFeature = features[i];
            if (!testDirectory(aFeature, REQUIRED_FEATURE_FILES, REQUIRED_FEATURE_SUFFIX)) {
                result.add(aFeature.getPath());
            }

        }

        String aString = "";
        if (result.size() > 0) {

            Iterator iter = result.iterator();
            while (iter.hasNext()) {
                String element = (String) iter.next();
                aString = aString + element + "; ";
            }
        }
        assertTrue("Feature directory missing required files: " + aString, result.size() == 0);
    }


    public void __testNumberOfTOTALFeatures() {

        String installDir = Platform.getInstallLocation().getURL().getPath();
        File featureDir = new File(installDir, "features");
        File[] features = featureDir.listFiles();
        int nFeatures = features.length;
        // assertEquals("there were more or less features than expected: ",
        // EXPECTED_NUMBER_OF_TOTAL_FEATURES, nFeatures);
        if (nFeatures != EXPECTED_NUMBER_OF_TOTAL_FEATURES) {
            System.out.println("there were more or less TOTAL features than expected: Found: " + nFeatures + "   Expected: " + EXPECTED_NUMBER_OF_TOTAL_FEATURES);
        }

    }

    public void __testNumberOfWTPFeatures() {

        String installDir = Platform.getInstallLocation().getURL().getPath();
        File featureDir = new File(installDir, "features");
        File[] features = featureDir.listFiles(wtpFilenameFilter);
        int nFeatures = features.length;
        assertEquals("there were more or less WTP features than expected: ", EXPECTED_NUMBER_OF_WTP_FEATURES, nFeatures);
    }

    public void __testNumberOfWSTFeatures() {

        String installDir = Platform.getInstallLocation().getURL().getPath();
        File featureDir = new File(installDir, "features");
        File[] features = featureDir.listFiles(wstFilenameFilter);
        int nFeatures = features.length;
        assertEquals("there were more or less WST features than expected: ", EXPECTED_NUMBER_OF_WST_FEATURES, nFeatures);
    }

    public void __testNumberOfJSTFeatures() {

        String installDir = Platform.getInstallLocation().getURL().getPath();
        File featureDir = new File(installDir, "features");
        File[] features = featureDir.listFiles(jstFilenameFilter);
        int nFeatures = features.length;
        assertEquals("there were more or less JST features than expected: ", EXPECTED_NUMBER_OF_JST_FEATURES, nFeatures);
    }

    public void __testNumberOfJPTFeatures() {

        String installDir = Platform.getInstallLocation().getURL().getPath();
        File featureDir = new File(installDir, "features");
        File[] features = featureDir.listFiles(jptFilenameFilter);
        int nFeatures = features.length;
        assertEquals("there were more or less JPT features than expected: ", EXPECTED_NUMBER_OF_JPT_FEATURES, nFeatures);
    }

    public void _testPluginFiles() {
        List result = new ArrayList();
        String installDir = Platform.getInstallLocation().getURL().getPath();
        File pluginDir = new File(installDir, "plugins");
        File[] plugins = pluginDir.listFiles();


        System.out.println("nPlugins: " + plugins.length);


        for (int i = 0; i < plugins.length; i++) {
            File aPlugin = plugins[i];
            if (aPlugin.getName().indexOf("test") == -1) {
                if (!testPluginFile(aPlugin)) {
                    result.add(aPlugin.getPath());
                }
            }
        }

        String aString = "";
        if (result.size() > 0) {

            Iterator iter = result.iterator();
            while (iter.hasNext()) {
                String element = (String) iter.next();
                aString = aString + element + "; ";
            }
        }
        assertTrue("Plugin directory missing required files: " + aString, result.size() == 0);
    }


    public void __testNumberOfTOTALPlugins() {
        String installDir = Platform.getInstallLocation().getURL().getPath();
        File pluginDir = new File(installDir, "plugins");
        File[] plugins = pluginDir.listFiles();
        int nPlugins = plugins.length;
        if (nPlugins != EXPECTED_NUMBER_OF_TOTAL_PLUGINS) {
            System.out.println("there were more or less TOTAL plugins than expected: Found: " + nPlugins + "   Expected: " + EXPECTED_NUMBER_OF_TOTAL_PLUGINS);
        }
    }

    public void __testNumberOfWTPPlugins() {
        String installDir = Platform.getInstallLocation().getURL().getPath();
        File pluginDir = new File(installDir, "plugins");
        File[] plugins = pluginDir.listFiles(wtpFilenameFilter);
        int nPlugins = plugins.length;
        assertEquals("there were more or less WTP plugins than expected: ", EXPECTED_NUMBER_OF_WTP_PLUGINS, nPlugins);
    }

    public void __testNumberOfWSTPlugins() {
        String installDir = Platform.getInstallLocation().getURL().getPath();
        File pluginDir = new File(installDir, "plugins");
        File[] plugins = pluginDir.listFiles(wstFilenameFilter);
        int nPlugins = plugins.length;
        assertEquals("there were more or less WST plugins than expected: ", EXPECTED_NUMBER_OF_WST_PLUGINS, nPlugins);
    }

    public void __testNumberOfJSTPlugins() {
        String installDir = Platform.getInstallLocation().getURL().getPath();
        File pluginDir = new File(installDir, "plugins");
        File[] plugins = pluginDir.listFiles(jstFilenameFilter);
        int nPlugins = plugins.length;
        assertEquals("there were more or less JST plugins than expected: ", EXPECTED_NUMBER_OF_JST_PLUGINS, nPlugins);
    }

    public void __testNumberOfJPTPlugins() {
        String installDir = Platform.getInstallLocation().getURL().getPath();
        File pluginDir = new File(installDir, "plugins");
        File[] plugins = pluginDir.listFiles(jptFilenameFilter);
        int nPlugins = plugins.length;
        boolean matchedExpectations = ((nPlugins == EXPECTED_NUMBER_OF_JPT_PLUGINS) || (nPlugins == (EXPECTED_NUMBER_OF_JPT_PLUGINS - 1)));
        assertTrue("there were more or less JPT plugins than expected (+ or - one): " + Integer.toString(EXPECTED_NUMBER_OF_JPT_PLUGINS) + " vs " + Integer.toString(nPlugins), matchedExpectations);
    }

    private boolean testPluginFile(File aPlugin) {

        // Are we a doc plugin?
        if (testDirectory(aPlugin, REQUIRED_PLUGIN_FILES, ".zip")) {
            return true;
        }

        // Are we a feature plugin?
        if (testDirectory(aPlugin, REQUIRED_FEATURE_PLUGIN_FILES, REQUIRED_FEATURE_PLUGIN_SUFFIX)) {
            return true;
        }

        // Are we a regular plugin
        if (testDirectory(aPlugin, REQUIRED_PLUGIN_FILES, REQUIRED_PLUGIN_SUFFIX)) {
            return true;
        }

        // Are we a source plugin
        if (testSourcePlugin(aPlugin)) {
            return true;
        }

        // Are we a fragment
        if ((testDirectory(aPlugin, REQUIRED_FRAGMENT_FILES, REQUIRED_FRAGMENT_SUFFIX)) || (testBundleDirectory(aPlugin, REQUIRED_BUNDLE_FILES, REQUIRED_BUNDLE_MANIFEST, REQUIRED_FRAGMENT_SUFFIX))) {
            return true;
        }

        // Are we an swt fragment
        if (testDirectory(aPlugin, REQUIRED_SWT_FRAGMENT_FILES, REQUIRED_SWT_FRAGMENT_SUFFIX)) {
            return true;
        }

        // Are we a bundle?
        if (testBundleDirectory(aPlugin, REQUIRED_BUNDLE_FILES, REQUIRED_BUNDLE_MANIFEST, REQUIRED_BUNDLE_SUFFIX)) {
            return true;
        }

        // No then we are bad
        return false;
    }

    private boolean testPluginJar(File aDirectory, String[] requiredFiles) {
        ArrayList list = new ArrayList();
        try {
            ZipFile jarredPlugin = new ZipFile(aDirectory);
            Enumeration _enum = jarredPlugin.entries();
            while (_enum.hasMoreElements()) {
                list.add(_enum.nextElement().toString());
            }
        }
        catch (ZipException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        if (!list.containsAll(Arrays.asList(requiredFiles))) {
            return false;
        }
        return true;
    }

    private boolean testDirectory(File aDirectory, String[] requiredFiles, String requiredSuffix) {
        if (aDirectory.getName().endsWith(".jar")) {
            return testPluginJar(aDirectory, requiredFiles);
        }
        else {
            if (!Arrays.asList(aDirectory.list()).containsAll(Arrays.asList(requiredFiles))) {
                return false;
            }

            int index = aDirectory.getName().indexOf('_');
            if (index == -1) {
                index = aDirectory.getName().length();
            }

            // String plainName = aDirectory.getName().substring(0, index);

            /*
             * if (requiredSuffix.equals("") ||
             * Arrays.asList(SUFFIX_EXEMPT_LIST).contains(plainName)) { return
             * true; } else if (aDirectory .listFiles(new
             * FileSuffixFilter(requiredSuffix)).length == 0) { return false;
             * }
             */
        }
        return true;
    }


    private boolean testBundleDirectory(File aDirectory, String[] requiredFiles, String manifestFile, String requiredSuffix) {
        if (aDirectory.getName().endsWith(".jar")) {
            return testPluginJar(aDirectory, requiredFiles);
        }
        else {
            if (!Arrays.asList(aDirectory.list()).containsAll(Arrays.asList(requiredFiles))) {
                return false;
            }

            int index = aDirectory.getName().indexOf('_');
            if (index == -1) {
                index = aDirectory.getName().length();
            }

            String plainName = aDirectory.getName().substring(0, index);

            File metaDir = new File(aDirectory, "META-INF");

            String[] metaFiles = metaDir.list();
            if (metaFiles == null) {
                return (false);
            }
            else {
                for (int i = 0; i < metaFiles.length; i++) {
                    String filename = metaFiles[i];
                    if (filename == manifestFile) {
                        return true;
                    }
                }
            }

            if (!metaDir.exists()) {
                return false;
            }

            if (requiredSuffix.equals("") || Arrays.asList(SUFFIX_EXEMPT_LIST).contains(plainName)) {
                return true;
            }
            else if (aDirectory.listFiles(new FileSuffixFilter(requiredSuffix)).length == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Return true if the receiver is a source plugin, false otherwise A
     * separate method because this is a little tricky.
     * 
     * @param aPlugin
     * @return boolean
     */
    private boolean testSourcePlugin(File aPlugin) {
        if (!testDirectory(aPlugin, REQUIRED_PLUGIN_FILES, "")) {
            return false;
        }

        File sourceDir = new File(aPlugin, "src");
        File[] sourceDirs = sourceDir.listFiles();
        if (sourceDirs == null) {
            return false;
        }

        for (int i = 0; i < sourceDirs.length; i++) {
            File aSourceDir = sourceDirs[i];
            if (!testDirectory(aSourceDir, REQUIRED_SOURCE_FILES, REQUIRED_SOURCE_SUFFIX)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Compares the feature and plug-in versions contained in this Eclipse
     * configuration, against a known previous Eclipse configuration.
     * 
     * @throws Exception
     */
    public void testVersionCompare() throws Exception {

        // TODO: version lister is optional. I should put explicit check here
        // to see if
        // it is not available, and exist with informational print out.

        VersionLister versionLister = new VersionLister();
        String[] args = getArgs();

        Object result = versionLister.run(args);
        boolean testok = (IApplication.EXIT_OK.equals(result));
        assertTrue("There is an error in versioning. See full listing for details.", testok);

    }


    private String[] getArgs() {
        /* provide some defaults, in case property file not provided */
        String qualifierArg = COUNT_QUALIFIER_INCREASE_ONLY_AS_ERROR;
        String testToReferenceArg = TEST_TO_REFERENCE;
        String referenceIDArg = "";
        String referenceURLArg = "";
        String listToReferenceFileArg = LIST_TO_REFERENCE_FILE;

        String propertyFileName = System.getProperty("relengTestsPropertiesFile", "relengTests.properties");
        System.out.println("relengTestsPropertiesFile: " + propertyFileName);
        Properties testProperties = new Properties();
        File propFile = new File(propertyFileName);
        String fullPath = propFile.getAbsolutePath();
        System.out.println("fullPath: " + fullPath);
        if (propFile.exists()) {
            InputStream propertyStream;
            try {
                propertyStream = new FileInputStream(propFile);
                testProperties.load(propertyStream);
                qualifierArg = testProperties.getProperty("qualifierArgValue", "");
                testToReferenceArg = testProperties.getProperty("testToReferenceArgValue", "");
                referenceIDArg = testProperties.getProperty("referenceIDArgValue", "");
                referenceURLArg = testProperties.getProperty("referenceURLArgValue", "");
                listToReferenceFileArg = testProperties.getProperty("listToReferenceFileArgValue", "");
            }
            catch (FileNotFoundException e) {
                // just use defaults
                System.out.println(e.getMessage());
            }
            catch (IOException e) {
                // just use defaults
                System.out.println(e.getMessage());
            }
        }
        else {
            System.out.println("relengTestsPropertiesFile does not exist");
        }

        ArrayList argsList = new ArrayList();
        if (validValue(qualifierArg)) {
            argsList.add(qualifierArg);
        }
        if (validValue(testToReferenceArg)) {
            argsList.add(testToReferenceArg);
        }
        if (validValue(referenceIDArg)) {
            argsList.add(referenceIDArg);
        }
        if (validValue(referenceURLArg)) {
            argsList.add(referenceURLArg);
        }
        if (validValue(listToReferenceFileArg)) {
            argsList.add(listToReferenceFileArg);
        }
        String[] args = (String[]) argsList.toArray(new String[]{});

        return args;
    }

    private boolean validValue(String arg) {
        boolean result = false;
        if ((arg != null) && (arg.length() > 0)) {
            result = true;
        }
        return result;
    }


    /**
     * This method does not really test anything, by itself (yet) it just uses
     * a URL connection to access the internet just to make sure it is logged.
     * 
     * @throws Exception
     */
    public void testHTTPSetup() throws Exception {
        try {
            URL url = new URL("http://one.nonesensehostjusttotest.org");
            url.openConnection();
        }
        catch (IOException e) {
            // ignore, we don't care
        }
    }


    /**
     * No assert here ... we just want a print out to log to double check
     * memory settings.
     * 
     * @throws Exception
     */
    public void testMemory() throws Exception {
        long onemeg = 1024 * 1024;
        // maximum memory
        long maxMem = Runtime.getRuntime().maxMemory();
        System.out.printf("%1$20s \t%2$15d MB%n", "Max Memory: ", (maxMem / onemeg));

        // total amount of memory reserved
        long totalMem = Runtime.getRuntime().totalMemory();
        System.out.printf("%1$20s \t%2$15d MB%n", "Total Memory: ", (totalMem / onemeg));

        // free memory
        long freeMem = Runtime.getRuntime().freeMemory();
        System.out.printf("%1$20s \t%2$15d MB%n", "Free Memory: ", (freeMem / onemeg));

    }

    public void testProperties() throws Exception {
        dumpAllProperties();
    }

    public void testLogging() throws Exception {

        String symbolicName = "org.eclipse.wtp.releng.tests";
        ILog log = Platform.getLog(Platform.getBundle(symbolicName));
        IStatus status = new Status(0, symbolicName, "test message to confirm log messages get recorded as expected");
        log.log(status);

    }

    public void testEnv() throws Exception {
        dumpAllEnvVariables();
    }

    void dumpAllEnvVariables() {
        System.out.println();
        System.out.println(" = = = All Environment Variables = = = ");
        System.out.println();
        System.out.println("Environment Variables");
        Map<String, String> envMap = System.getenv();

        for (String key : envMap.keySet()) {

            System.out.print(key);
            for (int i = 0; i < (30 - key.length()); i++) {
                System.out.print(" ");
            }
            System.out.println("->" + envMap.get(key));
        }
        System.out.println();
        System.out.println(" = = = All Environment Variables = = = ");
        System.out.println();

    }

    void dumpAllProperties() {
        System.out.println();
        System.out.println(" = = = Dump All Properties = = = ");
        System.out.println();
        java.util.Enumeration enumeration = System.getProperties().keys();
        String key = null;
        while (enumeration.hasMoreElements()) {
            key = (String) enumeration.nextElement();
            System.out.print(key);
            for (int i = 0; i < (30 - key.length()); i++) {
                System.out.print(" ");
            }
            System.out.println("->" + System.getProperty(key));
        }
        System.out.println();
        System.out.println("= = = Dump All Properties = = = ");
        System.out.println();
    }

    /*
     * While VM dependent, this should be left "last" in the class, so last
     * one invoked. If it causes a crash, at least others will have ran (at
     * least on some machines/vms).
     */
    public void testDisplay() throws Exception {

        // TODO: swt is optional. I should put an explict check here, if that
        // plugin is
        // available.

        Device.DEBUG = true;
        Display display = new Display();
        Shell shell = new Shell(display);

        System.out.println();
        System.out.println(">>>Attempt creating SWT.MOZILLA-style Browser");
        try {
            new Browser(shell, SWT.MOZILLA);
            System.out.println(">>>succeeded creating SWT.MOZILLA-style Browser");

        }
        catch (Error e) {
            // System.out.println(">>>This failed with the following error:");
            // e.printStackTrace();
            System.out.println(">>>Failed creating SWT.MOZILLA-style Browser");
            System.out.println();
            System.out.println(">>>Attempt creating SWT.NONE-style Browser");
            try {
                new Browser(shell, SWT.NONE);
                System.out.println(">>>succeeded creating SWT.NONE-style Browser");
            }
            catch (Error e2) {
                System.out.println(">>>failed creating SWT.NONE-style Browser");
                System.out.println();
                System.out.println(">>>Both attempts methods failed, ending with the following error:");
                e2.printStackTrace();
            }
        }
        display.dispose();
        System.out.println();

    }

    public void testComparatorLog() throws IOException {
        ComparatorLog comparatorLog = new ComparatorLog();
        assertEquals("There were unexpected Comparator messages. See " + ComparatorLog.COMPARATOR_UNEXPECTED_MESSAGES_LOG_TXT, 0, comparatorLog.getNumberOfUnexpectedMessages());
    }

}
