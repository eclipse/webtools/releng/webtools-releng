@rem ***************************************************************************
@rem Copyright (c) 2008, 2019 IBM Corporation and others.
@rem This program and the accompanying materials
@rem are made available under the terms of the Eclipse Public License 2.0
@rem which accompanies this distribution, and is available at
@rem https://www.eclipse.org/legal/epl-2.0/
@rem
@rem SPDX-License-Identifier: EPL-2.0
@rem
@rem Contributors:
@rem     IBM Corporation - initial API and implementation
@rem ***************************************************************************
@echo off
rem This is a sample batch file, for windows, that can be execute the version checker application
rem This file needs to be extracted and copied ot the root of the Eclipse installation (i.e. the
rem same directory as to where 'eclipse.exe' is located.

setlocal

rem specify dev workspace and JRE to use to run Eclipse

        rem the development workspace isn't really imortant, or used for anything
                set devworkspace=%1
                if %devworkspace%. == . set devworkspace=./workspace

        rem change/set the JRE to one on your system

                rem Sun's 1.5 JRE
                rem set devJRE=\JDKs\jdk1.5.0_11\bin\java.exe

                rem Sun's 1.6 JRE
                rem set devJRE=\JDKs\jdk6_06\bin\java.exe

                rem IBM's 1.5
                rem set devJRE=\JDKs\ibm-java2-sdk-50-win-i386\bin\java.exe

                rem IBM's 1.6 -- has problems on multi processors?
                set devJRE=\JDKs\ibm-jdk-6-sr2\bin\java.exe

rem set options for the VM
rem     add to eclipse.ini ?
set devArgs=-showversion -Xms256m -Xmx512m

rem display (echo) basic information about environment
echo dev:          %0
echo.
echo devworkspace: %devworkspace%
echo.
echo devJRE:       %devJRE%
%devJRE% -version
echo.

rem     list only (that is, only create version report based on current "stack"
rem     set versionListerCommands=-listToReferenceFile

rem     list current and also test/compare to named reference
        set versionListerCommands=countQualifierIncreaseOnlyAsError -testToReference  WTP-3.0.1-Plus-P20080911211753  -listToReferenceFile

rem     Note: the "noexit" option makes it easier to see output information, but then must type 'exit' command in OSGi console
start eclipse -noexit -nosplash -consolelog -console -debug -data %devworkspace%  -vm %devJRE% -application org.eclipse.wtp.releng.versionchecker.listVersions %versionListerCommands% -vmargs %devArgs%


endlocal
rem pause
