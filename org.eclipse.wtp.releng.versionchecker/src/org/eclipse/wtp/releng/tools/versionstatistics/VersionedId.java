/*******************************************************************************
 * Copyright (c) 2010, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.versionstatistics;

import org.osgi.framework.Version;


/**
 * A simple data object, to old an id and version.
 */
public class VersionedId {
    private final String  id;
    private final Version version;

    /**
     * Creates and returns a new {@link VersionedId} from the given string
     * specification. The specification must be of the form "id/version", or
     * just "id" if the version is absent <p> This factory method can be used
     * to reconstruct a {@link VersionedId} instance from the string
     * representation produced by a previous invocation of {@link #toString()}
     * .
     * 
     * @param spec
     *            the specification for the versioned id to create
     * @return the parsed versioned id
     * @throws IllegalArgumentException
     *             If <code>spec</code> is improperly formatted.
     */
    public static VersionedId parse(String spec) {
        FullJarNameParser parser = new FullJarNameParser();
        parser.parse(spec);
        String id = parser.getProjectString();
        String version = parser.getVersionString();
        return new VersionedId(id, version);
    }

    /**
     * Creates a new versioned id with the given id and version.
     * 
     * @param id
     *            The identifier
     * @param version
     *            The version
     * @throws IllegalArgumentException
     *             If <code>version</code> is improperly formatted.
     */
    public VersionedId(String id, String version) {
        this.id = id;
        this.version = Version.parseVersion(version);
    }

    /**
     * Creates a new versioned id with the given id and version.
     * 
     * @param id
     *            The identifier
     * @param version
     *            The version
     */
    public VersionedId(String id, Version version) {
        this.id = id;
        this.version = (version == null) ? Version.emptyVersion : version;
    }

    /**
     * @return the ID of this VersionedID
     */
    public String getId() {
        return id;
    }

    /**
     * @return the Version of this VersionedID
     */
    public Version getVersion() {
        return version;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VersionedId)) {
            return false;
        }

        VersionedId vname = (VersionedId) obj;
        return id.equals(vname.id) && version.equals(vname.version);
    }

    public int hashCode() {
        return id.hashCode() * 31 + version.hashCode();
    }
}
