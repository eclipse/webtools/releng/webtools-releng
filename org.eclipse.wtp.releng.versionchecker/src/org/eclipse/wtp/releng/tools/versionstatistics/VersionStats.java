/*******************************************************************************
 * Copyright (c) 2010, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools.versionstatistics;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.osgi.framework.Version;

public class VersionStats {

    private static final boolean DEBUG = false;
    private static final String  EOL   = System.getProperty("line.separator");



    public static void main(String[] args) {
        try {
            new VersionStats().getStats();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int       longest;
    private String    longestDir;
    private String    longestQualifier;
    private int       longestQualifierLength;
    private String    longestQualifierFeature;
    private int       longestSuffixLength;
    private String    longestSuffix;
    private String    longestSuffixFeature;
    private SortedMap sortedFeatureDirMap = new TreeMap();
    private SortedMap sortedQualifierMap  = new TreeMap();

    private void getStats() throws IOException {
        String filename = "features.outrad.txt";

        readdata(filename);
        report();

    }

    private void readdata(String filename) throws IOException {
        FileReader featureFile = new FileReader(filename);
        BufferedReader featureReader = new BufferedReader(featureFile);
        try {
            while (featureReader.ready()) {
                String line = featureReader.readLine();
                line = line.trim();
                if (line.length() > 0) {
                    String featuredir = line;
                    if (featuredir.endsWith(".jar")) {
                        featuredir = line.substring(0, line.length() - ".jar".length());
                    }
                    if (DEBUG) {
                        System.out.println(line);
                        System.out.println(featuredir);
                    }
                    checkIfLongest(featuredir);
                    countFeature(featuredir);

                    VersionedId versionId = computeVersionId(featuredir);
                    countQualifiers(versionId);
                    checkIfLongestQualifer(versionId);
                    checkIfLongestSuffix(versionId);
                }
            }
        }
        finally {
            if (featureReader != null) {
                featureReader.close();
            }
        }
    }

    private void report() {
        System.out.println();
        System.out.println("Longest feature directory: " + EOL + longestDir + " (" + longest + ") ");
        System.out.println();
        System.out.println("Longest feature Qualifier: " + EOL + longestQualifierFeature + " (" + longestQualifier + ")  " + " (" + longestQualifierLength + ") ");
        System.out.println();
        System.out.println("Longest feature suffix: " + EOL + longestSuffixFeature + " (" + longestSuffix + ")  " + " (" + longestSuffixLength + ") ");

        histogramFeatureDir();

        histogramQualifier();
    }

    private void histogramQualifier() {
        System.out.println(EOL + EOL + " qualifier length\tcount" + EOL);
        Set<Integer> qkeys = sortedQualifierMap.keySet();

        for (Iterator iterator = qkeys.iterator(); iterator.hasNext();) {
            Integer aLength = (Integer) iterator.next();
            List<VersionedId> collection = (List<VersionedId>) sortedQualifierMap.get(aLength);
            int count = collection.size();
            System.out.println("\t" + aLength + "\t" + count);
            if (aLength >= 28) {
                for (Iterator iterator2 = collection.iterator(); iterator2.hasNext();) {
                    VersionedId VersionedId = (VersionedId) iterator2.next();
                    System.out.println("   " + VersionedId.getId());

                }
            }

        }
    }

    private void histogramFeatureDir() {
        System.out.println(EOL + EOL + " directory length\tcount" + EOL);
        Set<Integer> keys = sortedFeatureDirMap.keySet();

        for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
            Integer aLength = (Integer) iterator.next();
            List<String> collection = (List<String>) sortedFeatureDirMap.get(aLength);
            int count = collection.size();
            System.out.println("\t" + aLength + "\t" + count);

        }
    }

    private void checkIfLongestSuffix(VersionedId version) {
        String qualifier = version.getVersion().getQualifier();
        String suffix = guessSuffix(qualifier);
        if ((suffix != null) && (suffix.length() > longestSuffixLength)) {
            longestSuffixLength = suffix.length();
            longestSuffix = suffix;
            longestSuffixFeature = version.getId();
        }


    }

    private String guessSuffix(String qualifier) {
        int pos = qualifier.indexOf('-');
        String suffix = null;
        if (pos > -1) {
            /*
             * If qualifier had -dddd- or =dddd<end> then assume its just the time in cvs tag 
             */
            suffix = qualifier.substring(pos + 1);
            if (suffix.matches("\\d\\d\\d\\d-.*")) {
                suffix = suffix.substring(5);
            }
            else if (suffix.matches("\\d\\d\\d\\d$")) {
                // technically null (no suffix)
                suffix = suffix.substring(4);
            }
        }
        return suffix;
    }

    private void checkIfLongestQualifer(VersionedId versionId) {
        // versionId.getId(), versionId.getVersion()
        Version version = versionId.getVersion();
        if (version.getQualifier().length() > longestQualifierLength) {
            longestQualifierLength = version.getQualifier().length();
            longestQualifier = version.getQualifier();
            longestQualifierFeature = versionId.getId();
        }

    }

    private void checkIfLongest(String featuredir) {
        if (featuredir.length() > longest) {
            longest = featuredir.length();
            longestDir = featuredir;
        }

    }

    private void countFeature(String featuredir) {
        Integer length = new Integer(featuredir.length());
        List<String> collection = (List<String>) sortedFeatureDirMap.get(length);
        if (collection == null) {
            collection = new ArrayList<String>();
            sortedFeatureDirMap.put(length, collection);
        }
        collection.add(featuredir);


    }

    private void countQualifiers(VersionedId versionId) {
        String qualifier = versionId.getVersion().getQualifier();
        String suffix = guessSuffix(qualifier);
        Integer alength = new Integer(0);
        if (suffix != null) {
            alength = new Integer(suffix.length());
        }
        List<VersionedId> qcollection = (List<VersionedId>) sortedQualifierMap.get(alength);
        if (qcollection == null) {
            qcollection = new ArrayList<VersionedId>();
            sortedQualifierMap.put(alength, qcollection);
        }
        qcollection.add(versionId);
    }

    public VersionedId computeVersionId(String spec) {
        FullJarNameParser parser = new FullJarNameParser();
        parser.parse(spec);
        String id = parser.getProjectString();
        String versionString = parser.getVersionString();
        Version version = new Version(versionString);
        return new VersionedId(id, version);
    }

}
