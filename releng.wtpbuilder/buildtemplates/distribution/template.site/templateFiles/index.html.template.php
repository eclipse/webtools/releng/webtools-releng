<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<!-- generated from releng.wtpbuilder/buildtemplates/distribution/template.site/templateFiles/index.html.template.php -->
<!-- DEPRECATED. See webtools.releng.aggregator/wtp-parent/templateFiles/index.html.template.php -->

<?php

//ini_set("display_errors", "true");
//error_reporting (E_ALL);


$buildBranch="@buildBranch@";
$build="@build@";
$buildtype="@buildtype@";
$builddate="@date@";

$eclipseMirrorPrefixuri="@eclipse.mirror.prefixuri@";
$eclipseFSpathPrefix="@eclipse.fspath.prefix@";

$eclipsefilelinux="@eclipsefilelinux@";
$eclipsefilewindows="@eclipsefilewindows@";
$eclipsefilemacosx="@eclipsefilemacos@";

$eclipseURL="@eclipseURL@";
$eclipseFile="@eclipseFile@";
$eclipseBuildURL="@eclipseBuildURL@";
$eclipseBuildHome="@eclipseBuildHome@";
$eclipseName="@eclipseName@";
$eclipseDescription="@eclipseDescription@";

$eclipseplatformMirrorPrefixuri="@eclipseplatform.mirror.prefixuri@";
$eclipseplatformFSpathPrefix="@eclipse.fspath.prefix@";

$eclipseplatformfilelinux="@eclipseplatformfilelinux@";
$eclipseplatformfilewindows="@eclipseplatformfilewindows@";
$eclipseplatformfilemacosx="@eclipseplatformfilemacos@";

$eclipseplatformURL="@eclipseplatformURL@";
$eclipseplatformFile="@eclipseplatformFile@";
$eclipseplatformBuildURL="@eclipseplatformBuildURL@";
$eclipseplatformBuildHome="@eclipseplatformBuildHome@";
$eclipseplatformName="@eclipseplatformName@";
$eclipseplatformDescription="@eclipseplatformDescription@";

$testURL="@testURL@";
$testFile="@testFile@";

$wstURL="@wstURL@";
$wstFile="@wstFile@";
$wstMirrorPrefixuri="@wst.mirror.prefixuri@";
$wstBuildHome="@wstBuildHome@";
$wstName="@wstName@";
$wstDescription="@wstDescription@";

$jstURL="@jstURL@";
$jstFile="@jstFile@";
$jstMirrorPrefixuri="@jst.mirror.prefixuri@";
$jstBuildHome="@jstBuildHome@";
$jstName="@jstName@";
$jstDescription="@jstDescription@";

$wtpURL="@wtpURL@";
$wtpFile="@wtpFile@";
$wtpMirrorPrefixuri="@wtp.mirror.prefixuri@";
$wtpBuildHome="@wtpBuildHome@";
$wtpName="@wtpName@";
$wtpDescription="@wtpDescription@";

$gefURL="@gefURL@";
$gefFile="@gefFile@";
$gefMirrorPrefixuri="@gef.mirror.prefixuri@";
$gefBuildHome="@gefBuildHome@";
$gefName="@gefName@";
$gefDescription="@gefDescription@";

$emfURL="@emfURL@";
$emfFile="@emfFile@";
$emfMirrorPrefixuri="@emf.mirror.prefixuri@";
$emfBuildHome="@emfBuildHome@";
$emfName="@emfName@";
$emfDescription="@emfDescription@";

$emfsourceURL="@emfsourceURL@";
$emfsourceFile="@emfsourceFile@";
$emfsourceMirrorPrefixuri="@emfsource.mirror.prefixuri@";
$emfsourceBuildHome="@emfsourceBuildHome@";
$emfsourceName="@emfsourceName@";
$emfsourceDescription="@emfsourceDescription@";

$emfxsdURL="@emfxsdURL@";
$emfxsdFile="@emfxsdFile@";
$emfxsdMirrorPrefixuri="@emfxsd.mirror.prefixuri@";
$emfxsdBuildHome="@emfxsdBuildHome@";
$emfxsdName="@emfxsdName@";
$emfxsdDescription="@emfxsdDescription@";

$emfxsdsourceURL="@emfxsdsourceURL@";
$emfxsdsourceFile="@emfxsdsourceFile@";
$emfxsdsourceMirrorPrefixuri="@emfxsdsource.mirror.prefixuri@";
$emfxsdsourceBuildHome="@emfxsdsourceBuildHome@";
$emfxsdsourceName="@emfxsdsourceName@";
$emfxsdsourceDescription="@emfxsdsourceDescription@";

$emftransactionURL="@emftransactionURL@";
$emftransactionFile="@emftransactionFile@";
$emftransactionMirrorPrefixuri="@emftransaction.mirror.prefixuri@";
$emftransactionBuildHome="@emftransactionBuildHome@";
$emftransactionName="@emftransactionName@";
$emftransactionDescription="@emftransactionDescription@";

$emfvalidationURL="@emfvalidationURL@";
$emfvalidationFile="@emfvalidationFile@";
$emfvalidationMirrorPrefixuri="@emfvalidation.mirror.prefixuri@";
$emfvalidationBuildHome="@emfvalidationBuildHome@";
$emfvalidationName="@emfvalidationName@";
$emfvalidationDescription="@emfvalidationDescription@";

$graphitiURL="@graphitiURL@";
$graphitiFile="@graphitiFile@";
$graphitiMirrorPrefixuri="@graphiti.mirror.prefixuri@";
$graphitiMirrorZipPrefixuri="@graphiti.mirror.zip.prefixuri@";
$graphitiBuildHome="@graphitiBuildHome@";
$graphitiName="@graphitiName@";
$graphitiDescription="@graphitiDescription@";

$emfvalidationsourceURL="@emfvalidationsourceURL@";
$emfvalidationsourceFile="@emfvalidationsourceFile@";
$emfvalidationsourceMirrorPrefixuri="@emfvalidationsource.mirror.prefixuri@";
$emfvalidationsourceBuildHome="@emfvalidationsourceBuildHome@";
$emfvalidationsourceName="@emfvalidationsourceName@";
$emfvalidationsourceDescription="@emfvalidationsourceDescription@";

$dtpURL="@dtpURL@";
$dtpFile="@dtpFile@";
$dtpMirrorPrefixuri="@dtp.mirror.prefixuri@";
$dtpBuildHome="@dtpBuildHome@";
$dtpName="@dtpName@";
$dtpDescription="@dtpDescription@";

$dltkURL="@dltkURL@";
$dltkFile="@dltkFile@";
$dltkMirrorPrefixuri="@dltk.mirror.prefixuri@";
$dltkBuildHome="@dltkBuildHome@";
$dltkName="@dltkName@";
$dltkDescription="@dltkDescription@";

$eclipserelengFile="@eclipserelengFile@";
$eclipserelengURL="@eclipserelengURL@";
$orbitthirdpartyzipFile="@orbitthirdpartyzipFile@";
$orbitthirdpartyzipURL="@orbitthirdpartyzipURL@";
$orbitthirdpartyzipBuildHome="@orbitthirdpartyzipBuildHome@";
$orbitthirdpartyzipMirrorPrefixuri="@orbitthirdpartyzip.mirror.prefixuri@";


$prereq_eclipse="@prereq.eclipse@";
$prereq_eclipseplatform="@prereq.eclipseplatform@";
$prereq_emf="@prereq.emf@";
$prereq_emfxsd="@prereq.emfxsd@";
$prereq_emftransaction="@prereq.emftransaction@";
$prereq_emfvalidation="@prereq.emfvalidation@";
$prereq_graphiti="@prereq.graphiti@";
$prereq_gef="@prereq.gef@";
$prereq_dtp="@prereq.dtp@";
$prereq_wst="@prereq.wst@";
$prereq_jst="@prereq.jst@";
$prereq_wtp="@prereq.wtp@";
$prereq_dltk="@prereq.dltk@";


include("miscUtil.php");




$debugScript = false;
$debugFunctions = false;

$defaultMirrorScript="";
$defaultWTPMirrorPrefix="./";

$eclipseMirrorScript="http://www.eclipse.org/downloads/download.php?file=";

// TODO: improve so this hard coding isn't required.
// This depends on the declare script changing webtools/committers to webtools/downloads
// And, the logic is such that if it is not mirrored, this URI is not used at all, just
// a relative reference only
$eclipseWTPMirrorPrefix="/webtools/committers/drops/$buildBranch/$build/";


$mirrorScript=$defaultMirrorScript;
$downloadprefix=$defaultWTPMirrorPrefix;


$keytestMirrorString=$eclipseMirrorScript . "$eclipseWTPMirrorPrefix/@zipprefix@-sdk-$build.zip";
if (isMirrored($keytestMirrorString) ) {
     $mirrorScript=$eclipseMirrorScript;
     $downloadprefix="${mirrorScript}${eclipseWTPMirrorPrefix}";
}

if ($debugScript)  {
     echo "inferred platform: " . getPlatform();
}


// our summary results handling requires php 5 (for simple xml file loading)
// so, if not php 5, just don't display any summary results
// This was found to be required, since some mirror our whole site (e.g. IBM)
// and not all mirrors use PHP 5
$displayTestSummary=false;
if (phpversion() >= 5) {

     $code_totalBundles=0;
     $code_totalErrors=0;
     $code_totalWarnings=0;
     $code_totalforbiddenAccessWarningCount=0;
     $code_totaldiscouragedAccessWarningCount=0;

     $test_totalBundles=0;
     $test_totalErrors=0;
     $test_totalWarnings=0;
     $test_totalforbiddenAccessWarningCount=0;
     $test_totaldiscouragedAccessWarningCount=0;


     $displayTestSummary=true;
     // expecting grandTotalErrors and grandTotalTests
     $filename = "unitTestsSummary.xml";
     if (file_exists($filename)) {
          $prefix = "unitTests_";
          $unitTestsSummary = simplexml_load_file($filename);
          foreach ($unitTestsSummary->summaryItem as $summaryItem) {
               $name = $summaryItem->name;
               $value = $summaryItem->value;
               $code= "\$" . $prefix . $name . " = " . $value . ";";
               //echo "<br />code: " . $code;
               eval($code);
          }
     }

     $filename = "compilelogsSummary.xml";
     if (file_exists($filename)) {
          $prefix = "code_";
          $compileSummary = simplexml_load_file($filename);
          foreach ($compileSummary->summaryItem as $summaryItem) {
               $name = $summaryItem->name;
               $value = $summaryItem->value;
               $code= "\$" . $prefix . $name . " = " . $value . ";";
               //echo "<br />code: " . $code;
               eval($code);
          }
     }

     $filename = "testcompilelogsSummary.xml";
     if (file_exists($filename)) {
          $prefix = "test_";
          $compileSummary = simplexml_load_file($filename);
          foreach ($compileSummary->summaryItem as $summaryItem) {
               $name = $summaryItem->name;
               $value = $summaryItem->value;
               $code= "\$" . $prefix . $name . " = " . $value . ";";
               //echo "<br />code: " . $code;
               eval($code);
          }
     }
}


$incubating="@incubating@";



if (isset($incubating) && ($incubating == "true")) {
     echo '<title>WTP Incubator Downloads</title>';
}
else {
     echo '<title>WTP Downloads</title>';
}

?>


</head>

<body>




<?php

// tiny banner to remind when looking at "local" machine results
$serverName = $_SERVER["SERVER_NAME"];

if (!stristr($serverName, "eclipse.org") && !stristr($serverName,"you.are.at.eclipsecon.org")) {
    echo '<center>
          <p>
          Reminder: this is <font color="#FF0000">' . 
    $serverName .
        '</font>
          See also
          <a href="http://download.eclipse.org/webtools/downloads" target="_top">the live public Eclipse site</a>.
          </p>
          <hr />
          </center>';

}
?>




<?php if ("true" === $incubating) {
    echo "<table BORDER=0 CELLSPACING=2 CELLPADDING=2 WIDTH=\"100%\">";
    echo "     <tr>";
    echo "          <td ALIGN=left><font face=\"'Bitstream Vera',Helvetica,Arial\" size=\"+2\"><b><?php echo \"$type\";?>";
    echo "        Build: $build</b></font></td>";
    echo "          <td align=\"right\" rowspan=\"3\"><a";
    echo "               href=\"http://www.eclipse.org/projects/what-is-incubation.php\"><img";
    echo "               src=\"http://www.eclipse.org/images/egg-incubation.png\"";
    echo "               alt=\"Incubation\" align=\"middle\" border=\"0\"></a></td>";


    echo "     <tr valign=\"top\">";
    echo "          <td><font size=\"-1\">" . $builddate . "</font></td>";
    echo "     </tr>";
    echo "     <tr valign=\"top\">";
    echo "          <td>";
    echo "          <p>The Eclipse Web Tools Platform (WTP) Incubator Project provides";
    echo "          tools for development that are just getting started, or are";
    echo "          experimental in some fashion.</p>";
    echo "          </td>";
    echo "     </tr>";
    echo "</table>";

} else {

    echo "<table BORDER=0 CELLSPACING=2 CELLPADDING=2 WIDTH=\"100%\">";
    echo "     <tr>";
    echo "          <td ALIGN=left><font face=\"'Bitstream Vera',Helvetica,Arial\" size=\"+2\"><b>$type";
    echo "          Build: " . $build . "</b></font></td>";

    echo "     <tr valign=\"top\">";
    echo "          <td><font size=\"-1\">" . $builddate . "</font></td>";
    echo "     </tr>";
    echo "     <tr valign=\"top\">";
    echo "          <td>";
    echo "          <p>The Eclipse Web Tools Platform Project provides tools for Web";
    echo "               Development, and is a platform for adopters making add-on tools for";
    echo "               Web Development.</p>";
    echo "          </td>";
    echo "     </tr>";
    echo "</table>";

} ?>


	<table border=0 cellspacing=2 cellpadding=2 width="100%">
		<tr>
			<td align="left" valign="top" bgcolor="#0080C0"><font
				face="'Bitstream Vera',Helvetica,Arial" color="#FFFFFF">All-in-one
					Packages</font>
			</td>
		</tr>
		<tr>
			<td>
				<p>
					For most uses, we recommend web-developers download the
					"all-in-one" package, <a href="http://www.eclipse.org/downloads/">Eclipse
						IDE for Java EE Developers</a>, from the main Eclipse download
					site.
				</p></td>
		</tr>
	</table>


	<!-- ***********  Required Prerequisites **************  -->
	<table border=0 cellspacing=2 cellpadding=2 width="100%">
		<tr>
			<td align="left" valign="top" bgcolor="#0080C0"><font
				face="'Bitstream Vera',Helvetica,Arial" color="#FFFFFF">Prerequisites
					and Handy Extras</font>
			</td>
		</tr>
		<tr>
			<td>
				<p>These are the prerequisites to build or run these packages. All
					are not necessarily required, but instead some subset. Also listed
					are some frequently needed links for committer-required packages
					when creating new development environments, or targets to run
					against.</p>
				<p>
					Note that WTP requires Java 5 or higher (and, for some things,
					actually requires a JDK rather than only a JRE) even though many
					other <a href="http://www.eclipse.org/downloads/">Eclipse Projects</a>
					can run with <a
						href="http://www.eclipse.org/downloads/moreinfo/jre.php">other JRE
						levels</a>.
				</p>
				<p></p></td>
		</tr>
		<tr>
			<td>
				<table border=0 cellspacing=1 cellpadding=1 width="90%"
					align="center">
						



					<?php
					if ("true" === $prereq_eclipseplatform) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td width=\"40%\">Eclipse Platform</td>";


					    //customize page depending on user's browser/platform, if we can detect it
					    $usersPlatform = getPlatform();
					    // assume windows by default, since likely most frequent, even for cases where
					    // platform is "unknown". I've noticed Opera reports 'unknown' :(
					    $recommendedFile=$eclipseplatformfilewindows;
					    if (strcmp($usersPlatform,"linux")== 0) {
					        $recommendedFile=$eclipseplatformfilelinux;
					    } else if (strcmp($usersPlatform,"mac") == 0) {
					        $recommendedFile=$eclipseplatformfilemacosx;
					    }

					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $eclipseMirrorPrefixuri, $eclipseURL, $recommendedFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $eclipseplatformBuildURL . "\">appropriate platform</a>";
					    echo " or <a href=\"" . $eclipseplatformBuildHome . "\">equivalent</a></td>";


					    echo " </tr>";
					}
					?>
						



					<?php
					if ("true" === $prereq_eclipse) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td width=\"40%\">Eclipse SDK (Platform, JDT)</td>";


					    //customize page depending on user's browser/platform, if we can detect it
					    $usersPlatform = getPlatform();
					    // assume windows by default, since likely most frequent, even for cases where
					    // platform is "unknown". I've noticed Opera reports 'unknown' :(
					    $recommendedFile=$eclipsefilewindows;
					    if (strcmp($usersPlatform,"linux")== 0) {
					        $recommendedFile=$eclipsefilelinux;
					    } else if (strcmp($usersPlatform,"mac") == 0) {
					        $recommendedFile=$eclipsefilemacosx;
					    }

					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $eclipseMirrorPrefixuri, $eclipseURL, $recommendedFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $eclipseBuildURL . "\">appropriate platform</a>";
					    echo " or <a href=\"" . $eclipseBuildHome . "\">equivalent</a></td>";


					    echo " </tr>";
					}
					?>
						



					<?php
					if ("true" === $prereq_emf) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td>";

					    echo $emfName . "&nbsp;" . $emfDescription ;

					    echo "</td>";
					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $emfMirrorPrefixuri, $emfURL, $emfFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $emfBuildHome . "\">equivalent</a></td>";
					    echo " </tr>";
					}
					?>
						




					<?php
					if ("true" === $prereq_emfxsd) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td>";

					    echo $emfxsdName . "&nbsp;" . $emfxsdDescription ;

					    echo "</td>";
					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $emfxsdMirrorPrefixuri, $emfxsdURL, $emfxsdFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $emfxsdBuildHome . "\">equivalent</a></td>";
					    echo " </tr>";
					}
					?>
						



					<?php
					if ("true" === $prereq_emftransaction) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td>";

					    echo $emftransactionName  . "&nbsp;" . $emftransactionDescription;

					    echo "</td>";
					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $emftransactionMirrorPrefixuri, $emftransactionURL, $emftransactionFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $emftransactionBuildHome . "\">equivalent</a></td>";

					    echo " </tr>";
					}
					?>
						



					<?php
					if ("true" === $prereq_emfvalidation) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td>";

					    echo $emfvalidationName  . "&nbsp;" . $emfvalidationDescription;

					    echo "</td>";
					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $emfvalidationMirrorPrefixuri, $emfvalidationURL, $emfvalidationFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $emfvalidationBuildHome . "\">equivalent</a></td>";

					    echo " </tr>";
					}
					?>
						



					<?php
					if ("true" === $prereq_graphiti) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td>";

					    echo $graphitiName  . "&nbsp;" . $graphitiDescription;

					    echo "</td>";
					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $graphitiMirrorZipPrefixuri, $graphitiURL, $graphitiFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $graphitiBuildHome . "\">equivalent</a></td>";

					    echo " </tr>";
					}
					?>
						



					<?php
					if ("true" === $prereq_gef) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td>";

					    echo $gefName  . "&nbsp;" . $gefDescription;

					    echo "</td>";
					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $gefMirrorPrefixuri, $gefURL, $gefFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $gefBuildHome . "\">equivalent</a></td>";

					    echo " </tr>";
					}
					?>
						



					<?php
					if ("true" === $prereq_dtp) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td>";

					    echo $dtpName . "&nbsp;" . $dtpDescription;

					    echo "</td>";
					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $dtpMirrorPrefixuri, $dtpURL, $dtpFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $dtpBuildHome . "\">equivalent</a></td>";

					    echo " </tr>";
					}
					?>
						



					<?php
					if ("true" === $prereq_wst) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td>";

					    echo $wstName . "&nbsp;" . $wstDescription;

					    echo "</td>";
					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $wstMirrorPrefixuri, $wstURL, $wstFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $wstBuildHome . "\">equivalent</a></td>";

					    echo " </tr>";
					}
					?>
						



					<?php
					if ("true" === $prereq_jst) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td>";

					    echo $jstName . "&nbsp;" . $jstDescription;

					    echo "</td>";
					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $jstMirrorPrefixuri, $jstURL, $jstFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $jstBuildHome . "\">equivalent</a></td>";

					    echo " </tr>";
					}
					?>
						


					<?php
					if ("true" === $prereq_wtp) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td>";

					    echo $wtpName . "&nbsp;" . $wtpDescription;

					    echo "</td>";
					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $wtpMirrorPrefixuri, $wtpURL, $wtpFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $wtpBuildHome . "\">equivalent</a></td>";

					    echo " </tr>";
					}
					?>
						


					<?php
					if ("true" === $prereq_dltk) {
					    echo "<tr valign=\"top\">";
					    echo "<td width=\"10%\"></td>";
					    echo "<td>";

					    echo $dltkName . "&nbsp;" . $dltkDescription;

					    echo "</td>";
					    echo "<td align=\"right\">";

					    echo getPrereqReferenceOrName($eclipseMirrorScript, $dltkMirrorPrefixuri, $dltkURL, $dltkFile, $eclipseFSpathPrefix);
					    echo " or <a href=\"" . $dltkBuildHome . "\">equivalent</a></td>";

					    echo " </tr>";
					}
					?>
					<tr valign="middle">
						<td width="10%"></td>
						<td colspan="2">
							<hr /></td>
					</tr>
					
					







               <?php
               if ("true" === $prereq_emf) {
                    echo "<tr valign=\"top\">";
                    echo "<td width=\"10%\"></td>";
                    echo "<td>";
                    echo $emfsourceName . "&nbsp;" . $emfsourceDescription;
                    echo "</td> ";
                    echo "<td align=\"right\">";
                    echo getPrereqReferenceOrName($eclipseMirrorScript, $emfsourceMirrorPrefixuri, $emfsourceURL, $emfsourceFile, $eclipseFSpathPrefix);
                    echo " or <a href=\"" . $emfsourceBuildHome . "\">equivalent</a></td>";
                    echo "</tr>";
               }
               ?>
               <?php
               if ("true" === $prereq_emfxsd) {
                    echo "<tr valign=\"top\">";
                    echo "<td width=\"10%\"></td>";
                    echo "<td>";
                    echo $emfxsdsourceName . "&nbsp;" . $emfxsdsourceDescription;
                    echo "</td> ";
                    echo "<td align=\"right\">";
                    echo getPrereqReferenceOrName($eclipseMirrorScript, $emfxsdsourceMirrorPrefixuri, $emfxsdsourceURL, $emfxsdsourceFile, $eclipseFSpathPrefix);
                    echo " or <a href=\"" . $emfxsdsourceBuildHome . "\">equivalent</a></td>";
                    echo "</tr>";
               }
               ?>

               <?php
               if ("true" === $prereq_emftransaction) {
                    echo "<tr valign=\"top\">";
                    echo "<td width=\"10%\"></td>";
                    echo "<td>";
                    echo $emftransactionsourceName . "&nbsp;" . $emftransactionsourceDescription;
                    echo "</td> ";
                    echo "<td align=\"right\">";
                    echo getPrereqReferenceOrName($eclipseMirrorScript, $emftransactionsourceMirrorPrefixuri, $emftransactionsourceURL, $emftransactionsourceFile, $eclipseFSpathPrefix);
                    echo " or <a href=\"" . $emftransactionsourceBuildHome . "\">equivalent</a></td>";
                    echo "</tr>";
               }
               ?>

               <?php
               if ("true" === $prereq_emfvalidation) {
                    echo "<tr valign=\"top\">";
                    echo "<td width=\"10%\"></td>";
                    echo "<td>";
                    echo $emfvalidationsourceName . "&nbsp;" . $emfvalidationsourceDescription;
                    echo "</td> ";
                    echo "<td align=\"right\">";
                    echo getPrereqReferenceOrName($eclipseMirrorScript, $emfvalidationsourceMirrorPrefixuri, $emfvalidationsourceURL, $emfvalidationsourceFile, $eclipseFSpathPrefix);
                    echo " or <a href=\"" . $emfvalidationsourceBuildHome . "\">equivalent</a></td>";
                    echo "</tr>";
               }
               ?>

               <?php
               if ("true" === $prereq_graphiti) {
                    echo "<tr valign=\"top\">";
                    echo "<td width=\"10%\"></td>";
                    echo "<td>";
                    echo $graphitisourceName . "&nbsp;" . $graphitisourceDescription;
                    echo "</td> ";
                    echo "<td align=\"right\">";
                    echo getPrereqReferenceOrName($eclipseMirrorScript, $graphitisourceMirrorPrefixuri, $graphitisourceURL, $graphitisourceFile, $eclipseFSpathPrefix);
                    echo " or <a href=\"" . $graphitisourceBuildHome . "\">equivalent</a></td>";
                    echo "</tr>";
               }
               ?>

               <?php
               if (true) {
                    echo "<tr valign=\"top\">";
                    echo "<td width=\"10%\"></td>";
                    echo "<td>";
                    echo "Eclipse Test Framework (required only for Automated JUnit tests)";
                    echo "</td> ";
                    echo "<td align=\"right\">";

                    echo getPrereqReferenceOrName($eclipseMirrorScript, $eclipseMirrorPrefixuri, $testURL, $testFile, $eclipseFSpathPrefix);
                    echo " or <a href=\"" . $eclipseBuildHome . "\">equivalent</a></td>";
                    echo "</tr>";
               }
               ?>


               <tr valign="top">
                    <td width="10%"></td>
                    <td>Eclipse releng tool (required only for committers to more easily
                    "release" code to a build)</td>
                    <td align="right"><?php
                    echo getPrereqReferenceOrName($eclipseMirrorScript, $eclipseMirrorPrefixuri, $eclipserelengURL, $eclipserelengFile, $eclipseFSpathPrefix);
                    echo " or <a href=\"" . $eclipseBuildHome . "\">equivalent</a></td>";
                    ?>

               </tr>

               <tr valign="top">
                    <td width="10%"></td>
                    <td>Third Party code from Orbit. Not required and is currently much
                    more than needed for WTP, but some committers like using to create a
                    PDE target.</td>
                    <td align="right"><?php
                    echo getPrereqReferenceOrName($eclipseMirrorScript, $orbitthirdpartyzipMirrorPrefixuri, $orbitthirdpartyzipURL, $orbitthirdpartyzipFile,$eclipseFSpathPrefix);
                    echo " or <a href=\"" . $orbitthirdpartyzipBuildHome . "\">equivalent</a></td>";
                    ?>

               </tr>

          </table></td>
		</tr>
	</table>


	<!-- ***********  P2 Zips **************  -->
	



                    <?php

                    $bellwether_name="repos";
                    if (file_exists($bellwether_name)) {
                         // if-then-include section
                         // include this whole section if (and only if) the bellwether file exists.
                         // For example, it may not exist on builds for "old" streams.
                         ?>

<table border=0 cellspacing=2 cellpadding=2 width="100%">
     <tr>
          <td align=left valign=top colspan="2" bgcolor="#0080C0"><font
               face="'Bitstream Vera',Helvetica,Arial" color="#FFFFFF">p2 repos (update sites)</font></td>
     </tr>

     <tr>
          <td align="left" valign="top" colspan="5">
          <p>These are zips of p2 repos, which should be
          downloaded and installed into a development environment or PDE target.
          Do not simply unzip these - you should use Eclipse / p2 to perform an installation instead.</p>
          </td>
     </tr>

     <tr>
          <td>
          <table border=0 cellspacing=2 cellpadding=2 width="90%" align="center">


          <?php

          $shortname="@shortname@";
          $zipfilename=$bellwether_name."/".$shortname."-buildrepo-".$build;
          displayp2repoarchives($zipfilename, $downloadprefix, $shortname         , "Minimal", "Executable code");
          $zipfilename=$bellwether_name."/".$shortname."_sdk-buildrepo-".$build;
          displayp2repoarchives($zipfilename, $downloadprefix, $shortname." sdk"  , "SDK", "Executable code and source");
          $zipfilename=$bellwether_name."/".$shortname."_tests-buildrepo-".$build;
          displayp2repoarchives($zipfilename, $downloadprefix, $shortname." tests", "Tests", "Unit tests");

          ?>
          </table>

     </tr>
</table>

          <?php } ?>


<!-- ***********  Traditional Zips **************  -->
<table border=0 cellspacing=2 cellpadding=2 width="100%">
     <tr>
          <td align=left valign=top colspan="5" bgcolor="#0080C0"><font
               face="'Bitstream Vera',Helvetica,Arial" color="#FFFFFF"> Traditional
          Zip Files</font></td>
     </tr>

     <tr>
          <td align="left" valign="top" colspan="5">
          <p>@longdescription@ Note: These traditional zip files should be
          considered deprecated, in favor of using the archived P2 repositories.
          The tradtional zip files will one day disappear.</p>
          </td>
     </tr>
     <tr>
          <td>
          <table border=0 cellspacing=2 cellpadding=2 width="90%" align="center">


               <tr>
                    <td align="left" valign="top" width="10%"><b>Minimal</b></td>
                    <td align="left" valign="top">
                    <p>Executable code only.</p>
                    </td>
                    <?php
                    $zipfilename="@zipprefix@-${build}";
                    $filename=$zipfilename.".zip";
                    $zipfilesize=fileSizeForDisplay($filename);
                    $fileShortDescription="$shortname";
                    displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);
                    ?>
               </tr>

               <tr>
                    <td align="left" valign="top" width="10%"><b>SDK</b></td>
                    <td align="left" valign="top">
                    <p>Executable code and source code.</p>
                    </td>
                    <?php
                    $zipfilename="@zipprefix@-sdk-${build}";
                    $filename=$zipfilename.".zip";
                    $zipfilesize=fileSizeForDisplay($filename);
                    $fileShortDescription="$shortname sdk";
                    displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);
                    ?>
               </tr>
               <tr>
                    <td align="left" valign="top" width="10%"><b>Tests</b></td>
                    <td align="left" valign="top">
                    <p>Unit tests.</p>
                    </td>
                    <?php
                    $zipfilename="@zipprefix@-tests-${build}";
                    $filename=$zipfilename.".zip";
                    $zipfilesize=fileSizeForDisplay($filename);
                    $fileShortDescription="$shortname tests";
                    displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);
                    ?>
               </tr>
          </table>
          </td>


     <tr>

</table>







<!-- ***********  Build Status **************  -->
<table border=0 cellspacing=2 cellpadding=2 width="100%">
     <tr>
          <td align=left valign=top bgcolor="#0080C0"><font
               face="'Bitstream Vera',Helvetica,Arial" color="#FFFFFF">Status, tests
          and other interesting details</font></td>
     </tr>
     <tr>
          <td>
          <table border=0 cellspacing=2 cellpadding=2 width="90%" align="center">

               <tr>
                    <td><!-- 
                    Its silly to always display 'Build notes', but hard to 
                    compute if required or not. 
                    <a href="buildNotes.php">Build notes</a> <br /> 
                    --> <a href="directory.txt">map files</a> <br />
                    <?php

                    if ($displayTestSummary) {


                         if (isset($unitTests_grandTotalErrors)) {
                              $errorColor="green";
                              if ($unitTests_grandTotalErrors > 0) {
                                   $errorColor="red";
                              }
                              echo "<a href=\"testResults.php\">Unit test results</a>&nbsp;";
                              echo "<img src=\"junit_err.gif\"/><font color=\"" . $errorColor . "\">" . $unitTests_grandTotalErrors . "</font>&nbsp;&nbsp;Total: " . $unitTests_grandTotalTests;
                         }
                         else {
                              // we hardly ever "pend" anymore ... abscense usually signifies no tests or a build error.
                              echo "<br /><font color=\"orange\">Unit tests don't exists, are pending, or there's a build error.</font>";
                              //&nbsp;&nbsp;<img src=\"pending.gif\"/>";
                         }

                         echo "<br />";
                         echo "<a href=\"compileResults.php\">Compile logs: Code Bundles</a>";

                         echo "&nbsp;&nbsp;($code_totalBundles)&nbsp;&nbsp;";
                         echo "<img src=\"compile_err.gif\"/><font color=red>$code_totalErrors</font>&nbsp;";
                         echo "<img src=\"compile_warn.gif\"/><font color=orange>$code_totalWarnings</font>&nbsp;";
                         echo "<img src=\"access_err.gif\"/><font color=red>$code_totalforbiddenAccessWarningCount</font>&nbsp;";
                         echo "<img src=\"access_warn.gif\"/><font color=orange>$code_totaldiscouragedAccessWarningCount</font>&nbsp;";

                         echo "<br />";
                         echo "<a href=\"testCompileResults.php\">Compile logs: Test Bundles</a>";

                         echo "&nbsp;&nbsp;($test_totalBundles)&nbsp;&nbsp;";
                         echo "<img src=\"compile_err.gif\"/><font color=red>$test_totalErrors</font>&nbsp;";
                         echo "<img src=\"compile_warn.gif\"/><font color=orange>$test_totalWarnings</font>&nbsp;";
                         echo "<img src=\"access_err.gif\"/><font color=red>$test_totalforbiddenAccessWarningCount</font>&nbsp;";
                         echo "<img src=\"access_warn.gif\"/><font color=orange>$test_totaldiscouragedAccessWarningCount</font>&nbsp;";
                    }

                    ?> <br />

                    <?php
                    if (file_exists("versioningReportName.php")) {
                         include "versioningReportName.php";
                         $fname="${versionReportFilename}.html";
                         if (file_exists($fname)) {
                              echo "<br /> <a href='$fname'>Versioning Information</a>";
                         }
}
?> <?php
echo "<br />";
if (file_exists("./apiresults/api-progress.html"))
{
     echo "<br /> <a href=\"apiresults/api-progress.html\">API Progress Report</a>";
}
if (file_exists("./apiresults/api-info-summary.html"))
{
     echo "<br /> <a href=\"apiresults/api-info-summary.html\">APIs Defined by Each Component</a>";
}
if (file_exists("./apiresults/api-ref-compatibility.html"))
{
     echo "<br /> <a href=\"apiresults/api-ref-compatibility.html\">Adopter Breakage Report</a>";
}
if (file_exists("./apiresults/api-violation-summary.html"))
{
     echo "<br /> <a href=\"apiresults/api-violation-summary.html\">API Violations</a>";
}
if (file_exists("./apiresults/component-api-violation-all.html"))
{
     echo "<br /> <a href=\"apiresults/component-api-violation-all.html\">Non-API dependencies</a>";
}
if (file_exists("./apiresults/api-tc-summary.html"))
{
     echo "<br /> <a href=\"apiresults/api-tc-summary.html\">API Test Coverage</a>";
}
if (file_exists("./apiresults/api-javadoc-summary.html"))
{
     echo "<br /> <a href=\"apiresults/api-javadoc-summary.html\">API Javadoc Coverage</a>";
}
if (file_exists("./apiresults/api-tc-summary.html"))
{
     echo "<br /><br /> <a href=\"apiresults/full_test_coverage/api-tc-summary.html\">Test Coverage for All Classes and Methods</a>";
}
?> <?php
if (file_exists("./perfresults/graph/performance.php"))
{
     echo "<br />";
     echo "<br /> <a href=\"perfresults/graph/performance.php\">Performance Results</a>";
     echo "<br />";
}
?></td>
               </tr>
          </table>
          </td>
     </tr>
</table>



<!-- footer -->
<center>
<hr>
<p>All downloads are provided under the terms and conditions of the <a
     href="http://www.eclipse.org/legal/notice.html">Eclipse.org Software
User Agreement</a> unless otherwise specified.</p>

<p>If you have problems downloading the drops, contact the <font
     face="'Bitstream Vera',Helvetica,Arial" size="-1"><a
     href="mailto:webmaster@eclipse.org">webmaster</a></font>.</p>

</center>
<!-- end footer -->


</body>
</html>
