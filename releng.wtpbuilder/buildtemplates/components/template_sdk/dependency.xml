<?xml version="1.0"?>
   
<!--
    Copyright (c) 2009, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->
 <!--
        ======================================================================
        Properties that must be passed to this script: base.install.dir
        dependencyTargets local.cache.dir dependency.properties
        ======================================================================
    -->
<project
    name="test"
    default="get">
    <property environment="env"/>
    <!-- required to get proper value of dropinsFolder -->
    <property
        name="keyCfgFile"
        value="${env.PROJECT_BUILDERS}/${projectname}/${env.RELENG}/maps/build.cfg"/>
    <echo
        level="debug"
        message="keyCfgFile: ${keyCfgFile}"/>
    <property file="${keyCfgFile}"/>
    <condition property="getprereq.eclipse">
        <istrue value="@prereq.eclipse@"/>
    </condition>
    <condition property="getprereq.eclipseplatform">
        <istrue value="@prereq.eclipseplatform@"/>
    </condition>
    <condition property="getprereq.emf">
        <istrue value="@prereq.emf@"/>
    </condition>
    <condition property="getprereq.emfxsd">
        <istrue value="@prereq.emfxsd@"/>
    </condition>
    <condition property="getprereq.emftransaction">
        <istrue value="@prereq.emftransaction@"/>
    </condition>
    <condition property="getprereq.emfvalidation">
        <istrue value="@prereq.emfvalidation@"/>
    </condition>
    <condition property="getprereq.graphiti">
        <istrue value="@prereq.graphiti@"/>
    </condition>
    <condition property="getprereq.gef">
        <istrue value="@prereq.gef@"/>
    </condition>
    <condition property="getprereq.dtp">
        <istrue value="@prereq.dtp@"/>
    </condition>
    <condition property="getprereq.wst">
        <istrue value="@prereq.wst@"/>
    </condition>
    <condition property="getprereq.jst">
        <istrue value="@prereq.jst@"/>
    </condition>
    <condition property="getprereq.wtp">
        <istrue value="@prereq.wtp@"/>
    </condition>
    <condition property="getprereq.dltk">
        <istrue value="@prereq.dltk@"/>
    </condition>
    <target name="get">
        <!--
            read in this properties, just so we can make sure our
            requested pre-reqs are defined, which we do later with
            statements such if="${groupId}.url"
        -->
        <property file="${dependency.properties}"/>
        <antcall target="prereq.eclipse"/>
        <antcall target="prereq.eclipseplatform"/>
        <antcall target="prereq.emf"/>
        <antcall target="prereq.emfxsd"/>
        <antcall target="prereq.emftransaction"/>
        <antcall target="prereq.emfvalidation"/>
        <antcall target="prereq.graphiti"/>
        <antcall target="prereq.gef"/>
        <antcall target="prereq.dtp"/>
        <antcall target="prereq.wst"/>
        <antcall target="prereq.jst"/>
        <antcall target="prereq.wtp"/>
        <antcall target="prereq.dltk"/>
    </target>
    <target
        name="prereq.eclipse"
        if="getprereq.eclipse">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="eclipse"/>
        </antcall>
    </target>
    <target
        name="prereq.eclipseplatform"
        if="getprereq.eclipseplatform">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="eclipseplatform"/>
        </antcall>
    </target>
    <target
        name="prereq.emf"
        if="getprereq.emf">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="emf"/>
        </antcall>
    </target>
    <target
        name="prereq.emfxsd"
        if="getprereq.emfxsd">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="emfxsd"/>
        </antcall>
    </target>
    <target
        name="prereq.emftransaction"
        if="getprereq.emftransaction">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="emftransaction"/>
        </antcall>
    </target>
    <target
        name="prereq.emfvalidation"
        if="getprereq.emfvalidation">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="emfvalidation"/>
        </antcall>
    </target>
    <target
        name="prereq.graphiti"
        if="getprereq.graphiti">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="graphiti"/>
        </antcall>
    </target>
    <target
        name="prereq.gef"
        if="getprereq.gef">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="gef"/>
        </antcall>
    </target>
    <target
        name="prereq.dtp"
        if="getprereq.dtp">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="dtp"/>
        </antcall>
    </target>
    <target
        name="prereq.wst"
        if="getprereq.wst">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="wst"/>
        </antcall>
    </target>
    <target
        name="prereq.jst"
        if="getprereq.jst">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="jst"/>
        </antcall>
    </target>
    <target
        name="prereq.wtp"
        if="getprereq.wtp">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="wtp"/>
        </antcall>
    </target>
    <target
        name="prereq.dltk"
        if="getprereq.dltk">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="dltk"/>
        </antcall>
    </target>
    <target
        name="getAndInstall"
        if="${groupId}.url">
        <ant
            antfile="${dependencyTargets}"
            target="checkDependency">
            <property
                name="groupId"
                value="${groupId}"/>
        </ant>
        <ant
            antfile="${dependencyTargets}"
            target="installDependency">
            <property
                name="groupId"
                value="${groupId}"/>
            <property
                name="install.destination"
                value="${base.install.dir}"/>
        </ant>
    </target>
</project>