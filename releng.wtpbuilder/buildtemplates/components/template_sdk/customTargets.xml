<!--
    Copyright (c) 2009, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->
<project
    name="PDECustomTargets"
    default="noDefault"
    basedir=".">
    <!--
        = = = standard properties pattern = = = Note to be
        cross-platform, "environment variables" are only appropriate for
        some variables, e.g. ones we set, since properties are case
        sensitive, even if the environment variables on your operating
        system are not, e.g. it will be ${env.Path} not ${env.PATH} on
        Windows
    -->
    <property environment="env"/>
    <!--
        Let users override standard properties, if desired. If
        directory, file, or some properties do not exist, then standard
        properties will be used.
    -->
    <property file="${env.LOCAL_BUILD_PROPERTIES_DIR}/${ant.project.name}.properties"/>

    <!-- let this project specify properties, if not already set -->
    <property file="build.properties"/>
    <fail
        message="dependency.properties must be specified by distribution script"
        unless="dependency.properties"/>
    <fail
        unless="buildDirectory"
        message="buildDirectory must be set"/>
    <echo message="buildDirectory: ${buildDirectory}"/>
    <!--
        Run a given ${target} on all elements being built Add on <ant>
        task for each top level element being built.
    -->
    <target name="allElements">
        <echo
            level="info"
            message="Target:${target} "/>
        <echo
            level="debug"
            message="basedir: ${basedir}"/>
        <echo
            level="info"
            message="component: ${component}"/>
        <echo
            level="debug"
            message="buildDirectory: ${buildDirectory}"/>
        <echo
            level="debug"
            message="baseLocation: ${baseLocation}"/>
        <echo
            level="debug"
            message="generic target: ${genericTargets}"/>
        <echo message="compilerArg: ${compilerArg}"/>
        <echo message="individualSourceBundles: ${individualSourceBundles}"/>
        <ant
            antfile="${genericTargets}"
            target="${target}">
            <property
                name="type"
                value="feature"/>
            <property
                name="id"
                value="@featurename@@sdksuffix@"/>
        </ant>
    </target>
    <!--
        Targets to assemble the built elements for particular
        configurations These generally call the generated assemble
        scripts (named in ${assembleScriptName}) but may also add pre
        and post processing Add one target for each root element and
        each configuration
    -->
    <target name="assemble.@featurename@@sdksuffix@">
        <property
            name="archiveName"
            value="@zipprefix@-sdk-${buildLabel}.zip"/>
        <ant
            antfile="${assembleScriptName}"
            dir="${buildDirectory}"/>
    </target>
    <target name="prePackage"/>
    <target name="postPackage">
        <property
            name="archiveName"
            value="@zipprefix@-sdk-${buildLabel}.zip"/>
        <ant
            antfile="${wtp.builder.home}/scripts/build/buildutilities2.xml"
            target="unpackUpdateJarsAndCreateZippedPackages">
            <property
                name="buildDirectory"
                value="${buildDirectory}"/>
            <property
                name="buildLabel"
                value="${buildLabel}"/>
            <property
                name="archiveName"
                value="${archiveName}"/>
        </ant>
    </target>

    <!--
        Check out map files from repository
        -->
    <target name="getMapFiles">
        <ant
            antfile="${wtp.builder.home}/scripts/build/maptasks.xml"
            target="maputil">
            <property
                name="mapcomponent"
                value="${env.RELENG}"/>
            <property
                name="mapTag"
                value="${mapVersionTag}"/>
        </ant>
        <ant
            antfile="${wtp.builder.home}/scripts/build/maptasks.xml"
            target="maputil">
            <property
                name="mapcomponent"
                value="${env.@relengmapsenv@}"/>
            <property
                name="mapTag"
                value="${mapVersionTag}"/>
        </ant>
    </target>


    <!-- 
        Steps to do before setup
    -->
    <target name="preSetup"></target>
    <!--
        Steps to do after setup but before starting the build proper
    -->
    <target name="postSetup">
        <dirname
            file="${ant.file}"
            property="component.dir"/>
        <echo
            level="debug"
            message="ant.file: ${ant.file}"/>
        <echo
            level="debug"
            message="component.dir: ${component.dir}"/>

        <!-- Get the dependencies -->
        <antcall target="getDependencies"/>
    </target>
    <!--
        =====================================================================
    -->
    <!-- Get the dependencies based on releng maps -->
    <!--
        =====================================================================
    -->
    <target name="getDependencies">
        <ant
            antfile="${component.dir}/dependency.xml"
            target="get">
            <property
                name="base.install.dir"
                value="${buildRoot}"/>
        </ant>
    </target>

    <!--
      Steps to do before fetching the build elements
    -->
    <target name="preFetch"></target>

    <!-- 
      Steps to do after fetching the build elements
    -->
    <target name="postFetch"></target>

    <!--
      Steps to do before generating the build scripts.
    -->
    <target name="preGenerate"></target>

    <!--
      Steps to do after generating the build scripts.
    -->
    <target name="postGenerate">

        <ant antfile="${wtp.builder.home}/scripts/build/customizeAccessRules.xml"/>
    </target>
    <!--
        Steps to do before running the build.xmls for the elements being
        built.
    -->
    <target name="preProcess">
        <replace
            dir="${buildDirectory}/plugins"
            value="${timestamp}"
            token="@build@">
            <include name="**/about.mappings"/>
        </replace>
    </target>
    <!--
        Steps to do after running the build.xmls for the elements being
        built.
    -->
    <target name="postProcess">
        <condition property="logsAvailable">
            <istrue value="${javacVerbose}"/>
        </condition>
        <antcall target="gatherLogs"/>
    </target>


    <!--
      Steps to do before running assemble. 
        -->
    <target name="preAssemble"></target>

    <!--
      Steps to do after  running assemble.
    -->
    <target name="postAssemble"></target>

    <!--
      Steps to do after the build is done.
    -->
    <target name="postBuild"></target>

    <!--
      Steps to do to test the build results
    -->
    <target name="test"></target>

    <!--
      Steps to do to publish the build results
    -->
    <target name="publish"></target>

    <!--
      Helper targets
    -->
    <target
        name="gatherLogs"
        if="logsAvailable">
        <mkdir dir="${buildDirectory}/${buildLabel}/compilelogs"/>
        <echo
            level="debug"
            message="logExtension: ${logExtension}"/>
        <antcall target="allElements">
            <param
                name="target"
                value="gatherLogs"/>
            <param
                name="logExtension"
                value="${logExtension}"/>
        </antcall>
        <unzip
            dest="${buildDirectory}/${buildLabel}/compilelogs"
            overwrite="true">
            <fileset dir="${buildDirectory}/features/@featurename@@sdksuffix@">
                <include name="**/*.log.zip"/>
            </fileset>
        </unzip>
        <!--
            TODO: it is unclear if the above "gather" and unzip do
            anything constructive. I think not and should be removed
            (after experimental run, of course.
        -->
        <!--
            workaround: see
            https://bugs.eclipse.org/bugs/show_bug.cgi?id=156043 and
            https://bugs.eclipse.org/bugs/show_bug.cgi?id=172361 and
            https://bugs.eclipse.org/bugs/show_bug.cgi?id=294070
        -->
        <copy
            todir="${buildDirectory}/${buildLabel}/compilelogs/plugins"
            overwrite="true">
            <!--
                we want all @dot.xml files, but assumes a certain
                structure, to include directory for name of plugin
            -->
            <fileset dir="${buildDirectory}/features/">
                <include name="**/feature.temp.folder/"/>
                <exclude name="**/*tests*feature*/feature.temp.folder/"/>
            </fileset>
            <compositemapper>
                <mapper
                    type="regexp"
                    from="(.*)/plugins/(.*)/@dot.xml"
                    to="/\2/@dot.xml"/>
                <mapper
                    type="regexp"
                    from="(.*)/plugins/(.*)/(.*)bin.xml"
                    to="/\2/\3bin.xml"/>
            </compositemapper>
        </copy>
    </target>
    <target
        name="clean"
        unless="noclean">
        <antcall target="allElements">
            <param
                name="target"
                value="cleanElement"/>
        </antcall>
    </target>

    <!--
      Default target 
    -->
    <target name="noDefault">
        <echo
            level="error"
            message="You must specify a target when invoking this file"/>
    </target>
</project>