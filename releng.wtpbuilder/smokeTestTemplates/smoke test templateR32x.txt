== Smoke Test Promotion votes ==

Instructions: 

Use a red X (fail) icon ([[Image:Fail.gif]]) to signify that the 
build should not be promoted, that a respin is required. In that case, please 
provide bug number and be sure those that need to fix something are aware of it, 
and can arrange for a respin. Once the issue has been fixed, and confirmed, 
change the red X to a green check mark with the build Id where is was verified, 
so those doing the promotion know its ready to go. 

If a smoke test does not pass but the build can still be 
promoted, use a green check mark (ok) icon ([[Image:Checkmark.gif]]) but then 
document in the comments bug numbers, how the smoke test fails, workarounds, 
or cautions about the build.  

If a build can be promoted and no issues were found, simple use 
the green check mark and include the initials or names of who performed the test.

Remember the significant column for promoting a build is the left-most 
project column. If some component within a project has not (or can not) 
be tested, the Project Lead should either make sure someone does the test, 
or make a judgment call on if the build (from their project's point of view) 
can be promoted or not. If the smoke test could not be performed, a note
should be made in the comments section that it wasn't performed, why, and 
why that should prevent promotion.  

Keep in mind the threshold for promoting a build 
gets higher as we near a milestone or near a release.

{| border="3" cellpadding="5"
|+ This Week's Results
|Project ||Vote ||Initials Comments

|-
| Java EE
| align="center" | [[Image:Questionmark.gif]]
|
|-
|JSF
| align="center" | [[Image:Questionmark.gif]]
|
|-
|Dali
| align="center" |[[Image:Questionmark.gif]]
|
|-
|Server
| align="center" |[[Image:Questionmark.gif]]
|
|-
|Web Services, WSDL
| align="center" |[[Image:Questionmark.gif]]
|[[Image:Questionmark.gif]]Web Services

[[Image:Questionmark.gif]]WSDL

[[Image:Questionmark.gif]]JAXWS
|-
| JSDT
| align="center" | [[Image:Questionmark.gif]]
|
|-
|Source Editing
| align="center" |[[Image:Questionmark.gif]]
|[[Image:Questionmark.gif]]XML + JSP

[[Image:Questionmark.gif]]XSD

|-
|Release Engineering
| align="center" |[[Image:Questionmark.gif]]
|
|}


 Smoke Test ok to promote vote = [[Image:Checkmark.gif]]
 Smoke Test do not promote vote = [[Image:Fail.gif]]
 Smoke Test Pending = [[Image:Questionmark.gif]]


=====[[WTP Smoke Test Results | Back to the WTP Smoke Test Results Main Page]]=====
