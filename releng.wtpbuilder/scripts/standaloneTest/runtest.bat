@rem ***************************************************************************
@rem Copyright (c) 2009, 2019 IBM Corporation and others.
@rem This program and the accompanying materials
@rem are made available under the terms of the Eclipse Public License 2.0
@rem which accompanies this distribution, and is available at
@rem https://www.eclipse.org/legal/epl-2.0/
@rem
@rem SPDX-License-Identifier: EPL-2.0
@rem
@rem Contributors:
@rem     IBM Corporation - initial API and implementation
@rem ***************************************************************************

REM Example script to run standalone test on Windows
REM Running performance or junit tests require releng data to be present (i.e. call with -checkout first)
@echo off
SETLOCAL


REM These "home" directories must be accurate for your machine.
set BUILD_HOME=D:\wtp
set ANT_HOME=D:\ant
set JAVA_HOME=D:\java
set JAVA_6_HOME=D:\java

REM standard for windows. Linux or other platforms would need to change these.
set BASEOS=win32
set BASEWS=win32
set BASEARCH=x86

REM These 'set' commands should rarely, if ever have to be changed
REM Some are not actually used, but values are provided
REM to prevent spurious "warnings" in logs.
REM In other cases, the directory must exist before this script runs.
set PATH=%ANT_HOME%\bin;%JAVA_HOME%\bin;%PATH%
set BASE_BUILDERS=%BUILD_HOME%\basebuilders
set LOCAL_PREREQS_CACHE=%BUILD_HOME%\prereqsCache
set LOCAL_BUILD_PROPERTIES_DIR=%BUILD_HOME%\localBuildProperties
set RELENG=webtools.maps/releng
set RELENG_BUILDER=webtools.releng/releng.wtpbuilder
set DISPLAY=localhost:0
set BASE_BUILDERS=%BUILD_HOME%/basebuilders
set PROJECT_BUILDERS=%BUILD_HOME%/projectBuilders
set PROJECT_PROJECTS=%BUILD_HOME%/projects
set PROJECT_TESTS=%BUILD_HOME%/tests
set RECOMMENDED_TMP_DIR=%BUILD_HOME%/temp


rem   Note:
rem   'ext' protocol typically requires
rem   environment variable CVS_RSH to be set to ssh
rem   So, we just always do it. Can't imagine it'd hurt
rem   if protocol was pserver, but could be removed in that
rem   case, if desired.
rem   if you have write access you should change the write user and protocol

set CVS_RSH=ssh
set BUILD_CVS_READ_PROTOCOL=pserver
set BUILD_CVS_READ_USER=anonymous
set BUILD_CVS_WRITE_PROTOCOL=pserver
set BUILD_CVS_WRITE_USER=anonymous
set BUILD_CVS_SERVER=dev.eclipse.org

set BUILD_CVS_ROOT=/cvsroot/webtools





REM the following comments apply to the -D variables provided to the ant command
REM projectname:      what ever the project would is called in cruisecontrol (even though CC isn't used here)
REM build.committers: should be true if you want to download and reproduce a build from 'committers' area.
REM                   If working from a build on 'downloads' area, omit the variable (do not just set to false).
REM buildType:        I, S, or R
REM buildId:          Basically the "middle" part of a download zip or build name, such as the 3.2I in wtp-sdk-I-3.2I-20090722042933.zip
REM timestamp         Basically the "end" part of a download zip or build name, such as 20090722042933 in wtp-sdk-I-3.2I-20090722042933.zip
REM build.stream      (not sure if needed)
REM buildBranch       Build branch used in various scripts ... usually the "middle" part of projectname, such as R3.2 in wtp-R3.2-I
REM testTarget        The specific test suite(s) to run. These target names are from the test.xml file. Omit or use 'all' to run all tests.
REM testBundle		  The specific plugin to test, such as org.eclipse.jst.jsp. Will run the selected tests only on that bundle.  
REM -f antfile        The name or path should not have to be changed, but the "releng.wtpbuilder" project must already be checked out from CVS
REM                   (or exported from Eclipse) into a directory named 'webtools.releng' which is immediately under the BUILD_HOME directory.
REM                   It is assume this batch file, then, is in the BUILD_HOME directory.

REM check to see what the user chose. Default is help. 

if x%1==x-funcSuite goto funcSuite
if x%1==x-perfSuite goto perfSuite
if x%1==x-functional goto func
if x%1==x-checkout goto chk
if x%1==x-performance goto perf
goto hlp

:chk
echo Running standalonecheckout.xml
call ant ^
      -Dprojectname=wtp-R3.2.3 ^
      -DbuildType=R ^
      -DbuildId=3.2.3 ^
      -Dtimestamp=20110217214612 ^
      -DmapVersionTag=R3_2_3 ^
      -Dbuild.stream=R3.2.3 ^
      -DbuildBranch=R3.2.3 ^
      -f standalonecheckout.xml

goto exit


:func

echo Running standaloneTest.xml
call ant ^
      -Dprojectname=wtp-R3.2.3 ^
      -DbuildType=R ^
      -DbuildId=3.2.3 ^
      -Dtimestamp=20110217214612 ^
      -Dbuild.stream=R3.2.3 ^
      -DbuildBranch=R3.2.3 ^
     -f standaloneTest.xml 
goto exit

:funcSuite

echo Example Running standaloneTest.xml and running only one suite
call ant  ^
      -Dprojectname=wtp-M3.2.3 ^
      -DbuildType=M ^
      -DbuildId=3.2.3 ^
      -Dtimestamp=20110121005429 ^
      -Dbuild.stream=R3.2.3 ^
      -DbuildBranch=R3.2.3 ^
      -DtestBundle=org.eclipse.wst.jsdt.web.ui.tests ^
     -f standaloneTest.xml 

goto exit

:perf
echo Running standalonePerfTest.xml
call ant ^
      -Dprojectname=wtp-R3.2.3 ^
      -DbuildType=R ^
      -DbuildId=3.2.3 ^
      -Dtimestamp=20110217214612 ^
      -Dbuild.stream=R3.2.3 ^
      -DbuildBranch=R3.2.3 ^
     -f standalonePerfTest.xml  

     
goto exit

:perfSuite
rem For a complete list of available test suites see the 'all' target in eclipsePerfTests.xml
rem testBundle can be one suite (-DtestBundle=antui) or multiple (-DtestBundle="jdtcompilertool antui")
echo Running standalonePerfTest.xml
call ant ^
      -Dprojectname=wtp-R3.2.3 ^
      -DbuildType=R ^
      -DbuildId=3.2.3 ^
      -Dtimestamp=20110217214612 ^
      -Dbuild.stream=R3.2.3 ^
      -DbuildBranch=R3.2.3 ^
      -DtestBundle=antui ^
     -f standalonePerfTest.xml  
     
goto exit

:hlp
echo Use -performance to run all performance tests, -perfSuite to run specific performance suites, -funtional for function tests, -funcSuite to run a specific junit test, -checkout to (re)fetch releng data, or anything else for this message
:exit
ENDLOCAL
