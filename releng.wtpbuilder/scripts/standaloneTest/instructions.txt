
[Note to readers: please improve these instructions as you find things.]

The files in this folder are for installing and running unit tests and performance tests. 

Your machine must have the following software installed and update 
the "runtest" scripts to point to their installation: 
  ant 
  java 6

Your machine must have the following software installed and available to 
be executed from the path: 
  cvs 
  unzip 
  
To setup this framework, ensure the following environment variables in runtest.bat are set according to the machine:
BUILD_HOME  -The top level directory that will contain everything that this framework creates and uses
ANT_HOME	-Location of Ant installation
JAVA_6_HOME	-Location of Java 6 installation
JAVA_HOME	-Location of Java installation (can be the same as JAVA_6_HOME)
BASEOS		-machine Operating System
BASEWS		-machine Windowing System
BASEARCH	-machine Architecture

Run "runtest.bat -help" for info on how to run the tests. 
