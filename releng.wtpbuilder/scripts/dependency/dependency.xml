<?xml version="1.0"?>
<!--
    Copyright (c) 2009, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->

<!--
    ======================================================================
    Properties that must be passed to this script: base.install.dir
    dependencyTargets local.cache.dir dependency.properties

    ======================================================================
-->
<project
    name="test"
    default="get">
    <property environment="env"/>
     <!-- required to get proper value of branch specific values -->
    <property
        name="keyCfgFile"
        value="${env.PROJECT_BUILDERS}/${projectname}/${env.RELENG}/maps/build.cfg"/>
    <echo
        level="debug"
        message="keyCfgFile: ${keyCfgFile}"/>
    <fail message="Required property file does not exist: ${keyCfgFile}">
        <condition>
            <not>
                <available file="${keyCfgFile}"/>
            </not>
        </condition>
    </fail>
    <property file="${keyCfgFile}"/>

    <property
        name="dependencyTargets"
        value="${wtp.builder.home}/scripts/dependency/build.xml"/>
    <target name="get">

        <property
            name="installWorkingDirectory"
            value="${buildDirectory}"/>
        <fail message="Required property file does not exist: ${dependency.properties}">
            <condition>
                <not>
                    <available file="${dependency.properties}"/>
                </not>
            </condition>
        </fail>
        <fail message="Required property file does not exist: ${tobeinstalled.properties}">
            <condition>
                <not>
                    <available file="${tobeinstalled.properties}"/>
                </not>
            </condition>
        </fail>
        <property file="${dependency.properties}"/>
        <property file="${tobeinstalled.properties}"/>


        <echo level="debug" message="pde.builder.path: ${pde.builder.path}"/>
        <echo level="debug" message="eclipsebuilder.id: ${eclipsebuilder.id}"/>

        <property
            name="dependencyTargets"
            value="${wtp.builder.home}/scripts/dependency/build.xml"/>
        <echo level="debug" message="dependencyTargets: ${dependencyTargets}"/>
        <echo level="debug" message="base.install.dir: ${base.install.dir}"/>
        <echo level="debug" message="tobeinstalled.properties: ${tobeinstalled.properties}"/>
        <echo level="debug" message="installWorkingDirectory: ${installWorkingDirectory}"/>

        <antcall target="prereq.eclipse"/>
        <antcall target="prereq.e4x"/>
        <antcall target="prereq.eclipseplatform"/>
        <antcall target="prereq.eclipsetestframework"/>
        <antcall target="prereq.emf"/>
        <antcall target="prereq.emfxsd"/>
        <antcall target="prereq.emfandxsd"/>
        <antcall target="prereq.gef"/>
        <antcall target="prereq.dtp"/>
        <antcall target="prereq.wst"/>
        <antcall target="prereq.jst"/>
    	<antcall target="prereq.emfvalidation"/>
        <antcall target="prereq.emftransaction"/>
        <antcall target="prereq.graphiti"/>
        <antcall target="prereq.wtp"/>
        <antcall target="prereq.wtptests"/>
        <antcall target="prereq.dltk"/>
        <antcall target="prereq.testlibraries"/>
        <antcall target="prereq.wtpandtests"/>
        <antcall target="prereq.dalitests"/>
    </target>

<!--  Note: this target is called from "install base builder" routines, not "install prereqs" like most targets here -->
    <target name="prereq.eclipsebuilder">
        <echo message="eclipse builder being installed to: ${eclipse.builder.base.install.dir}"/>
        <mkdir dir="${eclipse.builder.base.install.dir}"/>
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="eclipsebuilder"/>
            <param
                name="base.install.dir"
                value="${eclipse.builder.base.install.dir}"/>
        </antcall>
    </target>
	<!--  Note: this target is called from "install base builder" routines, not "install prereqs" like most targets here -->
	    <target name="prereq.egit">
	        <echo message="EGit being installed to: ${eclipse.builder.base.install.dir}"/>
	        <antcall target="getAndInstallRepoFromURL">
	            <param
	                name="groupId"
	                value="egit"/>
	            <param
	                name="base.install.dir"
	                value="${eclipse.builder.base.install.dir}"/>
	        </antcall>
	    </target>
	<!--  Note: this target is called from "install base builder" routines, not "install prereqs" like most targets here -->
	    <target name="prereq.gitfetchfactory">
	        <echo message="Git fetch factory being installed to: ${eclipse.builder.base.install.dir}"/>
	        <antcall target="getAndInstallRepoFromURL">
	            <param
	                name="groupId"
	                value="gitFetchFactory"/>
	            <param
	                name="base.install.dir"
	                value="${eclipse.builder.base.install.dir}"/>
	        </antcall>
	    </target>
    <!--  Note: this target is called from "install base builder" routines, not "install prereqs" like most targets here -->
    <target name="prereq.wtpbuildtools">
        <echo level="debug" message="wtp build tools installing to: ${pde.builder.path}"/>
         <!-- TODO could check if ${eclipse.builder.base.install.dir} exists, since would be error if not -->

        <antcall target="getAndInstallRepo">
            <param
                name="groupId"
                value="wtpBuildTools"/>
            <param
                name="base.install.dir"
                value="${eclipse.builder.base.install.dir}"/>
        </antcall>
    </target>

    <target
        name="prereq.eclipse"
        if="getprereq.eclipse">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="eclipse"/>
        </antcall>
    </target>
    <target
        name="prereq.e4x"
        if="getprereq.e4x">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="e4x"/>
        </antcall>
    </target>
    <target
        name="prereq.eclipseplatform"
        if="getprereq.eclipseplatform">
        <!-- first task here is the normal one, but it is skipped if featues-to-installed is defined -->
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="eclipseplatform"/>
        </antcall>
        <antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="eclipseplatform"/>
        </antcall>

    </target>
    <target
        name="prereq.eclipsetestframework"
        if="getprereq.eclipsetestframework">
        <antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="eclipseTestFramework"/>
        </antcall>
    </target>
    <target
        name="prereq.emf"
        if="getprereq.emf">
        <antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="emf"/>
        </antcall>
    </target>
    <target
        name="prereq.emfxsd"
        if="getprereq.emfxsd">
        <antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="emfxsd"/>
        </antcall>
    </target>
    <target
        name="prereq.emfandxsd"
        if="getprereq.emfandxsd">
        <antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="emfandxsd"/>
        </antcall>
    </target>
    <target
        name="prereq.emfvalidation"
        if="getprereq.emfvalidation">
    	<antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="emfvalidation"/>
        </antcall>
    </target>
    <target
        name="prereq.emftransaction"
        if="getprereq.emftransaction">
    	<antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="emftransaction"/>
        </antcall>
    </target>
    <target
        name="prereq.graphiti"
        if="getprereq.graphiti">
    	<antcall target="getAndInstallRepoFromURL">
            <param
                name="groupId"
                value="graphiti"/>
        </antcall>
    </target>
    <target
        name="prereq.gef"
        if="getprereq.gef">
        <antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="gef"/>
        </antcall>
    </target>
    <target
        name="prereq.dtp"
        if="getprereq.dtp">
        <antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="dtp"/>
        </antcall>
    </target>
    <target
        name="prereq.wst"
        if="getprereq.wst">
        <antcall target="getAndInstallDropins">
            <param
                name="groupId"
                value="wst"/>
        </antcall>
    </target>
    <target
        name="prereq.jst"
        if="getprereq.jst">
        <antcall target="getAndInstallDropins">
            <param
                name="groupId"
                value="jst"/>
        </antcall>
    </target>
    <target
        name="prereq.wtp"
        if="getprereq.wtp">
        <antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="wtp"/>
        </antcall>
    </target>
    <target
        name="prereq.wtptests"
        if="getprereq.wtptests">
        <antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="wtptests"/>
        </antcall>
    </target>
    <target
        name="prereq.wtpandtests"
        if="getprereq.wtpandtests">
        <antcall target="getAndInstallRepo">
            <param
                name="groupId"
                value="wtpandtests"/>
        </antcall>
    </target>
    <target
        name="prereq.dltk"
        if="getprereq.dltk">
        <antcall target="getAndInstallDropins">
            <param
                name="groupId"
                value="dltk"/>
        </antcall>
    </target>
    <target
        name="prereq.dalitests"
        if="getprereq.dalitests">
        <antcall target="getAndInstallFramework">
            <param
                name="groupId"
                value="dalitests"/>
        </antcall>
    </target>



    <target
        name="prereq.testlibraries"
        if="getprereq.testlibraries">
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="tomcat.5"/>
        </antcall>
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="jonas.4"/>
        </antcall>
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="oagis.release"/>
        </antcall>
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="oagis.wsdl"/>
        </antcall>
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="jsf.1_1"/>
        </antcall>
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="emftransaction"/>
        </antcall>
        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="emfvalidation"/>
        </antcall>

        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="graphiti"/>
        </antcall>

        <antcall target="getAndInstall">
            <param
                name="groupId"
                value="eclipselink"/>
        </antcall>
    </target>








    <target
        name="getAndInstall"
        if="${groupId}.url"
        unless="${groupId}.tobeinstalledfeaturegroups">
        <ant
            antfile="${dependencyTargets}"
            target="checkDependency">
            <property
                name="groupId"
                value="${groupId}"/>
        </ant>
        <ant
            antfile="${dependencyTargets}"
            target="installDependency">
            <property
                name="groupId"
                value="${groupId}"/>
            <property
                name="install.destination"
                value="${base.install.dir}"/>
        </ant>
    </target>
    <target
        name="getAndInstallRepo"
        if="${groupId}.url">
        <ant
            antfile="${dependencyTargets}"
            target="checkDependency">
            <property
                name="groupId"
                value="${groupId}"/>
        </ant>
        <ant
            antfile="${dependencyTargets}"
            target="installRepo">
            <property
                name="groupId"
                value="${groupId}"/>
            <property
                name="install.destination"
                value="${base.install.dir}"/>
        </ant>
    </target>
	<target
	        name="getAndInstallRepoFromURL"
	        if="${groupId}.url">
	        <ant
	            antfile="${dependencyTargets}"
	            target="checkDependency">
	            <property
	                name="groupId"
	                value="${groupId}"/>
	        </ant>
	        <ant
	            antfile="${dependencyTargets}"
	            target="installRepoFromURL">
	            <property
	                name="groupId"
	                value="${groupId}"/>
	            <property
	                name="install.destination"
	                value="${base.install.dir}"/>
	        </ant>
	    </target>
     <!-- use getAndInstallFramework to decide based on whether or not 'tobeinstalledfeaturegroups' exists -->
    <target
        name="getAndInstallFramework"
        depends="tryRepo"
        unless="isRepo">
        <antcall target="getAndInstallDropins">
            <param
                name="groupId"
                value="${groupId}"/>
        </antcall>
    </target>
    <target
        name="tryRepo"
        depends="checkIfRepo"
        if="isRepo">
        <antcall target="getAndInstallRepo">
            <param
                name="groupId"
                value="${groupId}"/>
        </antcall>
    </target>
    <target name="checkIfRepo">
        <condition property="isRepo">
            <isset property="${groupId}.tobeinstalledfeaturegroups"/>
        </condition>
    </target>


    <target
        name="getAndInstallDropins"
        if="${groupId}.url">
        <ant
            antfile="${dependencyTargets}"
            target="checkDependency">
            <property
                name="groupId"
                value="${groupId}"/>
        </ant>

                  <!-- note: changed to put prereq into specific subfolder of dropins, to avoid 
                       possibility of "overwriting" one jar with another 

                                   <property
                                       name="install.destination"
                                       value="${base.install.dir}${dropinsFolder}"/>
                       -->
        <echo level="debug" message="dropinsFolder: ${dropinsFolder} "/>
        <condition
            property="install.destination"
            value="${base.install.dir}${dropinsFolder}/${groupId}"
            else="${base.install.dir}">
            <isset property="dropinsFolder"/>
        </condition>
        <ant
            antfile="${dependencyTargets}"
            target="installDependency">
            <property
                name="groupId"
                value="${groupId}"/>
            <property
                name="install.destination"
                value="${install.destination}"/>



        </ant>
    </target>

</project>

