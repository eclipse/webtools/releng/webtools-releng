<?xml version="1.0"?>
<!--
    Copyright (c) 2005, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->

<project
    name="test"
    default="get">
    <description>get and install dependencies</description>


    <!-- Note to be cross-platform, "environment variables" are only appropriate for
        some variables, e.g. ones we set, since properties are case sensitive, even if
        the environment variables on your operating system are not, e.g. it will
        be ${env.Path} not ${env.PATH} on Windows -->
    <property environment="env"/>

    <!-- 
        Note: if baseos, basews, and basearch are "passed in" to this 
        file, then they take priority, and retain their value. Otherwise, 
        use the current environment. Normally, the current environment is 
        desired, but sometimes, such as for creating the "all in ones", it may 
        be desired to "get" packages for other combinations.
    -->
    <property
        name="baseos"
        value="${env.BASEOS}"/>
    <property
        name="basews"
        value="${env.BASEWS}"/>
    <property
        name="basearch"
        value="${env.BASEARCH}"/>

    <!-- ================================= 
        target: get              
        ================================= -->
    <property file="@dependencyProperties@"/>


    <!-- - - - - - - - - - - - - - - - - - 
        target: init                      
        - - - - - - - - - - - - - - - - - -->
    <target name="init">
        <property
            name="dependency.name"
            value="${@dependencyGroupId@name}"/>
        <property
            name="dependency.description"
            value="${@dependencyGroupId@descriptions}"/>
        <property
            name="dependency.url"
            value="${@dependencyGroupId@url}"/>

        <property
            name="dependency.tobeinstalledfeaturegroups"
            value="${@dependencyGroupId@tobeinstalledfeaturegroups}"/>

        <condition
            property="dependency.repo"
            value="${@dependencyGroupId@repo}">
            <isset property="@dependencyGroupId@repo"/>
        </condition>


        <property
            name="local.cache.dir"
            value="."/>

        <condition
            property="dependency.file"
            value="${@dependencyGroupId@file}">
            <and>
                <isset property="@dependencyGroupId@file"/>
                <not>
                    <or>
                        <isset property="@dependencyGroupId@file.linux-gtk-x86_64"/>
                        <isset property="@dependencyGroupId@file.linux-gtk-ppc64"/>
                        <isset property="@dependencyGroupId@file.win32-win32-x86_64"/>
                        <isset property="@dependencyGroupId@file.macosx-cocoa-x86_64"/>
                    </or>
                </not>
            </and>
        </condition>
        <condition
            property="dependency.file"
            value="${@dependencyGroupId@file.win32-win32-x86_64}">
            <and>
                <isset property="@dependencyGroupId@file.win32-win32-x86_64"/>
                <equals
                    arg1="win32"
                    arg2="${baseos}"/>
                <equals
                    arg1="win32"
                    arg2="${basews}"/>
                <equals
                    arg1="x86_64"
                    arg2="${basearch}"/>
            </and>
        </condition>
        <condition
            property="dependency.file"
            value="${@dependencyGroupId@file.linux-gtk-x86_64}">
            <and>
                <isset property="@dependencyGroupId@file.linux-gtk-x86_64"/>
                <equals
                    arg1="linux"
                    arg2="${baseos}"/>
                <equals
                    arg1="gtk"
                    arg2="${basews}"/>
                <equals
                    arg1="x86_64"
                    arg2="${basearch}"/>
            </and>
        </condition>
        <condition
            property="dependency.file"
            value="${@dependencyGroupId@file.linux-gtk-ppc64}">
            <and>
                <isset property="@dependencyGroupId@file.linux-gtk-ppc64"/>
                <equals
                    arg1="linux"
                    arg2="${baseos}"/>
                <equals
                    arg1="gtk"
                    arg2="${basews}"/>
                <equals
                    arg1="ppc64"
                    arg2="${basearch}"/>
            </and>
        </condition>
        <condition
            property="dependency.file"
            value="${@dependencyGroupId@file.macosx-cocoa-x86_64}">
            <and>
                <isset property="@dependencyGroupId@file.macosx-cocoa-x86_64"/>
                <equals
                    arg1="macosx"
                    arg2="${baseos}"/>
                <equals
                    arg1="cocoa"
                    arg2="${basews}"/>
                <equals
                    arg1="x86_64"
                    arg2="${basearch}"/>
            </and>
        </condition>
        <condition
            property="dependency.autodownload"
            value="${@dependencyGroupId@autodownload}">
            <isset property="@dependencyGroupId@autodownload"/>
        </condition>
        <condition
            property="dependency.releng.url"
            value="${@dependencyGroupId@releng.url}">
            <isset property="@dependencyGroupId@releng.url"/>
        </condition>
    </target>


    <target
        name="get"
        depends="init">

        <echo
            level="debug"
            message="Getting ${dependency.file} or ${dependency.repo}"/>
        <echo
            level="debug"
            message="Url: ${dependency.url}"/>

        <ant
            antfile="@dependencyDir@/build.xml"
            target="default"/>

    </target>


    <!-- ================================= 
        target: install              
        ================================= -->
    <target
        name="install"
        depends="init">
        <echo
            level="debug"
            message="Installing ${dependency.file}"/>
        <echo
            level="debug"
            message="destination: ${install.destination}"/>
        <antcall target="cleanInstall"/>
        <property file="${installWorkingDirectory}/installmanifest.properties"/>
        <antcall target="callInstall"/>
        <echo
            level="debug"
            message="installmanifest.properties: ${installWorkingDirectory}/installmanifest.properties"/>
        <echo
            file="${installWorkingDirectory}/installmanifest.properties"
            append="true">
            @dependencyGroupId@${dependency.file}.installed=true

            @dependencyGroupId@dir=${local.cache.dir}
            @dependencyGroupId@file=${dependency.file}

        </echo>
    </target>

    <target
        name="installRepo"
        depends="init">
        <echo
            level="debug"
            message="Installing ${dependency.repo}"/>
        <echo
            level="debug"
            message="destination: ${install.destination}"/>
        <antcall target="cleanInstall"/>
        <property file="${installWorkingDirectory}/installmanifest.properties"/>
    <!-- 
              We first unzip the repo, then use p2 directory app to install into the eclispe
              location.
              TODO: could we just make p2runnable in "transformed" repository? for build case.
              -->         
              <!--
                   We use the exec method for unzip, so we won't fail if a 
                   prereq can not be unzipped for some reason. See 
                   https://bugs.eclipse.org/bugs/show_bug.cgi?id=283968 
              -->

        <property
            name="tobeinstalledrepo"
            value="${installWorkingDirectory}/temprepos/${groupId}"/>
        <echo
            level="debug"
            message="install repo for groupId: ${groupId}"/>
        <echo
            level="debug"
            message="tobeinstalledrepo:  ${tobeinstalledrepo}"/>
        <echo
            level="debug"
            message="tobeInstalledfeaturegroups:  ${dependency.tobeinstalledfeaturegroups}"/>

        <delete failonerror="false" quiet="true">
            <dirset dir="${tobeinstalledrepo}"/>
        </delete>
        <mkdir dir="${tobeinstalledrepo}"/>

        <exec
            dir="."
            executable="unzip">
            <arg line="-o -qq ${local.cache.dir}/${dependency.repo} -d ${tobeinstalledrepo}"/>
        </exec>


<!-- note we are deliberately running the eclipse we are installing into -->
        <java
            taskname="p2Director Install"
            fork="true"
            resultProperty="p2DirectorInstallResult"
            failonerror="true"
            jvm="${env.JAVA_7_HOME}/bin/java"
            dir="${install.destination}"
            classname="org.eclipse.equinox.launcher.Main">
            <classpath>
                <fileset dir="${install.destination}/eclipse/plugins">
                    <include name="org.eclipse.equinox.launcher_*.jar"/>
                </fileset>
            </classpath>
            <jvmarg value="-Djava.io.tmpdir=${env.RECOMMENDED_TMP_DIR}"/>
            <arg value="-nosplash"/>
            <!--  <arg value="-debug"/> -->
            <arg value="-consolelog"/>
            <arg value="-data"/>
            <arg value="${installWorkingDirectory}/p2DirectorInstall"/>
            <arg value="-application"/>
            <arg value="org.eclipse.equinox.p2.director"/>

            <arg value="-repository"/>
            <arg value="file:/${tobeinstalledrepo}"/>


            <arg value="-installIU"/>
            <arg value="${dependency.tobeinstalledfeaturegroups}"/>

            <arg value="-list"/>


            <!-- make sure our forked env has a DISPLAY? -->
            <env
                key="DISPLAY"
                value="${env.DISPLAY}"/>

            <arg value="-vmArgs"/>

        </java>
        <echo
            level="debug"
            message="p2DirectorInstallResult: ${p2DirectorInstallResult}"/>





    </target>


    <target
        name="cleanInstall"
        if="clean">
        <available
            file="${installWorkingDirectory}/installmanifest.properties"
            property="installmanifest.exists"/>
        <antcall target="cleanInstall2"/>
    </target>

    <target
        name="cleanInstall2"
        if="installmanifest.exists">
        <replace
            file="${installWorkingDirectory}/installmanifest.properties"
            token="@dependencyGroupId@${dependency.file}.installed=true"/>
    </target>

    <target
        name="callInstall"
        unless="@dependencyGroupId@${dependency.file}.installed">
        <ant
            antfile="@dependencyDir@/build.xml"
            target="install"/>
    </target>

    <target
        name="installRepoFromURL"
        depends="init">
    	<echo message="Installing ${dependency.repo}"/>
    	<echo message="destination: ${install.destination}"/>
        <antcall target="cleanInstall"/>
        <property file="${installWorkingDirectory}/installmanifest.properties"/>
    	<echo message="install repo for groupId: ${groupId}"/>
        <echo message="URL: ${dependency.url}"/>
        <echo message="tobeInstalledfeaturegroups:  ${dependency.tobeinstalledfeaturegroups}"/>
<!-- note we are deliberately running the eclipse we are installing into -->
        <java
            taskname="p2Director Install"
            fork="true"
            resultProperty="p2DirectorInstallResult"
            failonerror="true"
            jvm="${env.JAVA_7_HOME}/bin/java"
            dir="${install.destination}"
            classname="org.eclipse.equinox.launcher.Main">
            <classpath>
                <fileset dir="${install.destination}/eclipse/plugins">
                    <include name="org.eclipse.equinox.launcher_*.jar"/>
                </fileset>
            </classpath>
            <jvmarg value="-Djava.io.tmpdir=${env.RECOMMENDED_TMP_DIR}"/>
            <arg value="-nosplash"/>
            <arg value="-debug"/>
            <arg value="-consolelog"/>
            <arg value="-data"/>
            <arg value="${installWorkingDirectory}/p2DirectorInstall"/>
            <arg value="-application"/>
            <arg value="org.eclipse.equinox.p2.director"/>

            <arg value="-repository"/>
            <arg value="${dependency.url}"/>


            <arg value="-installIU"/>
            <arg value="${dependency.tobeinstalledfeaturegroups}"/>

            <arg value="-list"/>


            <!-- make sure our forked env has a DISPLAY? -->
            <env
                key="DISPLAY"
                value="${env.DISPLAY}"/>

            <arg value="-vmArgs"/>

        </java>
    	<echo message="p2DirectorInstallResult: ${p2DirectorInstallResult}"/>
    </target>
</project>