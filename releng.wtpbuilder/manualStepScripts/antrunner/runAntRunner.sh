#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2010, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
# specify devworkspace and JRE to use to runEclipse


devworkspace=~/workspace-antRunner

BUILDFILE=$1

if [ -e $BUILDFILE ]
then
    BUILDFILESTR=" -file $BUILDFILE"
    shift
fi

extraArgs="$@"

echo "BUILDFILE: $BUILDFILE"
echo "extraArgs: ${extraArgs}"

# we MUST use java 5, when using process/pack artifacts!
JAVA_5_HOME=/shared/webtools/apps/ibm-java2-i386-50
JAVA_6_HOME=/shared/webtools/apps/ibm-java-i386-60
JAVA_7_HOME=/shared/webtools/apps/ibm-java-i386-70
export JAVA_HOME=${JAVA_5_HOME}
devJRE=$JAVA_HOME/jre/bin/java

ibmDevArgs="-Xms128M -Xmx256M -Dosgi.ws=gtk -Dosgi.os=linux -Dosgi.arch=x86 ${extraArgs}"

devArgs=$ibmDevArgs

echo dev:          $0
echo
echo devworkspace: $devworkspace
echo
echo devJRE:       $devJRE
$devJRE -version
echo

ECLIPSE_INSTALL=/shared/webtools/apps/eclipse361/eclipse

$ECLIPSE_INSTALL/eclipse  --launcher.suppressErrors  -nosplash -debug -consolelog -console -data $devworkspace -application org.eclipse.ant.core.antRunner $BUILDFILESTR -vm $devJRE -vmargs $devArgs


