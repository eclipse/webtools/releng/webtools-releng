
The files in this directory are used to varying degrees when promoting or releasing builds. 
Over time, they should be more fully integrated into the automatic scripts, but are put in cvs here, 
for now, to have a safe copy. 

'antrunnner' directory normally exported to the 'apps' 
directory on /shared/webtools location, and executed from there. 

At the time of this writing, there are three steps required 
when a build is promoted, before officially released (ideally, before its promoted, so 
the work is done on "committer" copy).  

In all cases, each script needs to have file locations or urls hand edited first. 

1. 
the build's repository artifacts need to be packed (pack200) by using something like 
./runAntRunner.sh  process-artifacts-322.xml

2. 
add download stat flags, and mirror URL property
./addRepoProperties-322.sh

3. 
And finally, for final repo, be sure to update the composite repo so it points to the right place. 

