#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2010, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

APP_NAME=org.eclipse.equinox.p2.director

OTHER_ARGS="-metadataRepository http://download.eclipse.org/webtools/releng/repository/ -artifactRepository http://download.eclipse.org/webtools/releng/repository/ -installIU org.eclipse.wtp.releng.tools.feature.feature.group"

devworkspace=~/workspace-addRepoProperties

JAVA_5_HOME=/shared/webtools/apps/ibm-java2-i386-50
JAVA_6_HOME=/shared/webtools/apps/ibm-java-i386-60
JAVA_7_HOME=/shared/webtools/apps/ibm-java-i386-70
export JAVA_HOME=${JAVA_7_HOME}
devJRE=$JAVA_HOME/jre/bin/java

ibmDevArgs="-Xms128M -Xmx256M -Dosgi.ws=gtk -Dosgi.os=linux -Dosgi.arch=x86" 

devArgs=$ibmDevArgs

echo "dev:          " $0
echo
echo "devworkspace: " $devworkspace
echo
echo "devJRE:       " $devJRE
echo "OTHER_ARGS:   " ${OTHER_ARGS}
echo
$devJRE -version
echo

ECLIPSE_INSTALL=/shared/webtools/apps/eclipse361/eclipse

$ECLIPSE_INSTALL/eclipse -nosplash -console -data $devworkspace -application ${APP_NAME} ${OTHER_ARGS} -vm $devJRE -vmargs $devArgs

