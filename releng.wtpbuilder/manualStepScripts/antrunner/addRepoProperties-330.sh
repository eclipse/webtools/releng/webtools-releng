#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2010, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# this script is no longer used, routinely. 
# 'add properties' is now done as part of the promote script, in releng.control.

APP_NAME=org.eclipse.wtp.releng.tools.addRepoProperties

devworkspace=./workspace

JAVA_5_HOME=/shared/webtools/apps/ibm-java2-i386-50
JAVA_6_HOME=/shared/webtools/apps/ibm-java-i386-60
export JAVA_HOME=${JAVA_6_HOME}
devJRE=$JAVA_HOME/jre/bin/java

ibmDevArgs="-Xms128M -Xmx256M -Dosgi.ws=gtk -Dosgi.os=linux -Dosgi.arch=x86" 

REPO="/shared/webtools/committers/wtp-R3.3.0-I/20101209114749/S-3.3.0M4-20101209114749/repository"
MIRRORURL="/webtools/downloads/drops/R3.3.0/S-3.3.0M4-20101209114749/repository/"

if [ ! -z $MIRRORURL ] 
then 
   MIRRORURL_ARG="http://www.eclipse.org/downloads/download.php?format=xml&file=${MIRRORURL}&protocol=http"
else
    MIRRORURL_ARG=""
fi

# remember, the '&' should NOT be unescaped here ... the p2 api (or underlying xml) will escape it. 
devArgs="$ibmDevArgs \
-Dp2MirrorsURL=${MIRRORURL_ARG} \
-DartifactRepoDirectory=${REPO}  \
-Dp2StatsURI=http://download.eclipse.org/stats/webtools/repository/indigo -DstatsArtifactsSuffix=_indigo_M4 -DstatsTrackedArtifacts=org.eclipse.wst.jsdt.feature,org.eclipse.wst.xml_ui.feature,org.eclipse.wst.web_ui.feature,org.eclipse.jst.enterprise_ui.feature"


echo "dev:          " $0
echo
echo "devworkspace: " $devworkspace
echo
echo "devJRE:       " $devJRE
echo
echo "devArgs:      " $devArgs
echo
echo "APP_NAME:     " $APP_NAME
$devJRE -version
echo

ECLIPSE_INSTALL=/shared/webtools/apps/eclipse361/eclipse

$ECLIPSE_INSTALL/eclipse  -debug -nosplash -consolelog -console -data $devworkspace --launcher.suppressErrors -application ${APP_NAME} ${OTHER_ARGS} -vm $devJRE -vmargs $devArgs

