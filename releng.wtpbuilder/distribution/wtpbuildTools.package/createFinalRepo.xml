<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright (c) 2011, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->

<!--
       create final build repo.
       Eventually may want to "move" this to each component, in "post build" step? 
       See http://aniefer.blogspot.com/2009/08/versioning-p2-slides-from-eclipsecon.html
       for example of comparator.
-->
<project
    name="createFinalRepo"
    default="build"
    basedir=".">

     <!-- = = = standard properties pattern = = = -->
     <!--
             Note to be cross-platform, "environment variables" are only
             appropriate for some variables, e.g. ones we set, since
             properties are case sensitive, even if the environment variables
             on your operating system are not, e.g. it will be ${env.Path}
             not ${env.PATH} on Windows
         -->
    <property environment="env"/>
     <!--
             Let users override standard properties, if desired. If
             directory, file, or some properties do not exist, then standard
             properties will be used.
         -->
    <property file="${env.LOCAL_BUILD_PROPERTIES_DIR}/${ant.project.name}.properties"/>

     <!-- = = = end standard properties pattern = = = -->



    <fail unless="wtp.builder.home"/>
    <property file="${wtp.builder.home}/build.properties"/>


     <!-- required to get proper value of branch specific values, for example, 
            baseComparatorRepoDir, since can vary from forward "integration" stream, 
            and "maintenance" branch -->
    <property
        name="keyCfgFile"
        value="${env.PROJECT_BUILDERS}/${projectname}/${env.RELENG}/maps/build.cfg"/>
    <echo
        level="info"
        message="keyCfgFile: ${keyCfgFile}"/>
    <property file="${keyCfgFile}"/>

    <fail
        unless="baseComparatorRepoDir"
        message="baseComparatorRepoDir must be defined for this ant script"/>

    <property
        name="baseComparatorRepoDirTests"
        value="${baseComparatorRepoDir}"/>

     <!-- if not otherwise set, use these default properties 
    <property
        name="repoDownloadBaseURL"
        value="http://build.eclipse.org/webtools/committers"/> -->
    <property
        name="repoDownloadBaseURL"
        value=".."/>

    <fail unless="env.PROJECT_PROJECTS"/>
    <fail unless="projectname"/>
    <property
        name="labelfile"
        value="${env.PROJECT_PROJECTS}/${projectname}/workdir/label.properties"/>
    <available
        file="${labelfile}"
        property="labelfileavailable"/>
    <fail unless="labelfileavailable"/>
    <property file="${labelfile}"/>


    <target
        name="build"
        depends="doMirror">

          <!-- add the new build to the composite -->
          <!-- 
         TODO: after promotion, do we need to delete repo's as directories removed? Or just do occasional cleanup, 
         if non-existent child repos are ignored 
         TODO: ok to use absolute file system locations for child repos? (And still access via http, later?  
-->
          <!-- 

 On build machine, URL's to repositories will look similar to following URL: 

 http://build.eclipse.org/webtools/committers/wtp-R3.3.0-I/20100914034658/I-3.3.0-20100914034658/repository/

 which we translate as follows

  repoDownloadBaseURL=http://build.eclipse.org/webtools/committers

  ${repoDownloadBaseURL}/${projectname}/${timestamp}/${buildLabel}/repository/

 repoDownloadBaseURL is machine dependent (will differ from one build machine to another). 

 Will be different on "download server", as well. 

 -->

          <!-- remember, repoLocation won't exist until later, since it is at the "site", not the 
       working directory -->

         <!-- remember that repos are added to comparator set only when they are promoted to download location -->
        <property
            name="repoLocation"
            value="${repoDownloadBaseURL}/committers/${projectname}/${timestamp}/${buildLabel}/repository/"/>
        <echo
            message="${line.separator}    
     created final build repoository at location:${line.separator} 
         ${repoLocation}${line.separator}
              "/>
<!--
        <p2.composite.repository
            destination="file:/${baseComparatorRepoDir}">
            <add>
                <repository
                    location="${repoLocation}"/>
            </add>
        </p2.composite.repository>
-->
    </target>
    <target name="init">
        <available
            file="${baseComparatorRepoDir}"
            type="dir"
            property="baseComparatorRepoDirExists"/>
         <!-- we copy the comparator filter file, used only during tests, to build output location, 
         just so it is always available, even if tests ran later -->
        <copy
            file="${env.PROJECT_BUILDERS}/${projectname}/${env.RELENG}/maps/comparatorfilter.properties"
            todir="${buildDirectory}/${buildLabel}"/>

    </target>
     <!-- normally, the "prime repository" should be created from previous release, 
     or similar, but if that hasn't been done, we'll automatically create an empty 
     composite repo which we've stored away for simplicity.-->
    <target
        name="primeRepo"
        depends="init"
        unless="baseComparatorRepoDirExists">
        <mkdir dir="${baseComparatorRepoDir}"/>
        <copy todir="${baseComparatorRepoDir}">
            <fileset dir="${wtp.builder.home}/emptyRepository/"/>
        </copy>
          <!-- could probably just set to true, but we'll use same logic as in init, just in case -->
        <available
            file="${baseComparatorRepoDir}"
            type="dir"
            property="baseComparatorRepoDirExists"/>
    </target>

    <target
        name="doMirror"
        depends="primeRepo">
        <property
            name="destinationDir"
            value="${buildDirectory}/${buildLabel}/repository"/>
        <echo message="destinationDir: ${destinationDir}"/>

        <p2.mirror
            ignoreErrors="true"
            verbose="true"
            log="${buildDirectory}/${buildLabel}/finalMirrorRepo.log">


            <destination
                kind="metadata"
                location="file:/${destinationDir}"
                name="Web Tools Platform Repository ${buildLabel}"/>
            <destination
                kind="artifact"
                location="file:/${destinationDir}"
                name="Web Tools Platform Repository ${buildLabel}"/>


            <source>
                <repository location="file:/${buildDirectory}/${buildLabel}/buildrepository/"/>
            </source>

            <comparator
                comparator="org.eclipse.equinox.p2.repository.tools.jar.comparator"
                comparatorLog="${buildDirectory}/${buildLabel}/comparator.log">
                <repository location="file:/${baseComparatorRepoDir}"/>

            </comparator>

               <!-- 
             followStrict is sort of a safety measure. All our features should be strict, but in case not, 
             will show up early if not mirrored. 


             -->
            <slicingoptions
                followStrict="true"
                includeNonGreedy="false"
                includeOptional="false"/>

              <!--
                    Specify category and let it pull in its features. 
                    TODO: would be best to make "property[@name='org.eclipse.equinox.p2.type.category']" part of the query, 
                    but a) not sure if/how it will work and b) our names are distinct enough now this suffices. 
               -->

            <iu query="property[@name='org.eclipse.equinox.p2.name' and @value='Web Tools Platform Releng Tests and Tools ${buildId}']"/>

        </p2.mirror>

          <!-- create archived repo from contents of 'repository' at this point -->
        <property
            name="zippedrepo"
            value="${build.distribution}-repo-${buildLabel}.zip"/>
        <zip
            destfile="${buildDirectory}/${buildLabel}/${zippedrepo}"
            basedir="${buildDirectory}/${buildLabel}/repository"/>

        <ant
            antfile="${wtp.builder.home}/scripts/build/buildutilitiesp2.xml"
            target="createChecksums">
            <property
                name="archiveName"
                value="${zippedrepo}"/>
        </ant>

    </target>

</project>