<!--
    Copyright (c) 2010, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->
<project
    name="Automated Eclipse Testing"
    default="all"
    basedir=".">
    <property environment="env"/>
    <!-- required to get proper value of dropinsFolder -->
    <property
        name="keyCfgFile"
        value="${env.PROJECT_BUILDERS}/${projectname}/${env.RELENG}/maps/build.cfg"/>
    <echo
        level="info"
        message="keyCfgFile: ${keyCfgFile}"/>
    <property file="${keyCfgFile}"/>
    <echo
        level="info"
        message="starting Automated Eclipse Testing"/>
    <echo
        level="info"
        message="ant file basedir: ${basedir}"/>
    <echo
        level="info"
        message="testRoot: ${testRoot}"/>
    <fail
        message="testRoot must be set"
        unless="testRoot"/>

    <!--properties file containing the some server name properties -->
    <property file="${testRoot}/testServer.properties"/>

    <!--properties file containing the build information-->
    <property file="${buildDirectory}/label.properties"/>
    <!-- required to get proper value of dropinsFolder -->
    <property
        name="keyCfgFile"
        value="${env.PROJECT_BUILDERS}/${projectname}/${env.RELENG}/maps/build.cfg"/>
    <echo
        level="info"
        message="keyCfgFile: ${keyCfgFile}"/>
    <property file="${keyCfgFile}"/>

    <!--
        name that can be added to report name to identify which platform
        tests results come from
    -->
    <property
        name="platform"
        value=""/>

    <!-- TODO: The framework apps might need this. But we should elmimnate if posslbe. -->
    <property
        name="eclipse-home"
        value="${testRoot}/eclipse"/>
    <!--
        The directory that will contain the xml and html results from
        the tests that are run
    -->
    <property
        name="results"
        value="${testRoot}/results"/>
    <property
        name="library-file"
        value="${testRoot}/wtp-junit-tests.xml"/>

    <!-- jonas install directory -->
    <!-- TODO: why do this twice? Here and in init? -->
    <property
        name="jonas432Dir"
        value="${testRoot}/${jonas432DirName}"/>
    <target name="init">
        <antcall target="setPropertyJOnAS432Dir"/>
    </target>
    <target
        name="setPropertyJOnAS432Dir"
        unless="jonas432Dir">
        <property
            name="jonas432Dir"
            value="${testRoot}/${jonas432DirName}"/>
    </target>
    <target
        name="all"
        depends="init,runOneTestBundle"
        unless="testBundle">
        <echo
            level="info"
            message="Starting Unit Tests"/>
             <!--
                 unlike other ant tasks, subant defaults to false for
                 inheritall!
             -->
        <subant
            target="performance"
            inheritall="true"
            failonerror="false">
            <fileset dir="${eclipse-home}/">
                <include name="**/test.xml"/>
            </fileset>
        </subant>
        <echo
            level="info"
            message="Ended Unit Tests"/>
    </target>
    <!--
        by making 'all' the default, and "runOneTestBundle" as its
        'depends' we can effectively control which is ran based on
        whether or not 'testBundle' has been defined by caller. If so,
        only that one is ran, otherwise, all of them are. Note, in many
        cases a collection of tests could be ran, by specifying a
        partial bundle name. for example, specifying
        "org.eclipse.jst.jsp" would execute the 3 JSP tests:
        org.eclipse.jst.jsp.core.tests,
        org.eclipse.jst.jsp.tests.encoding,
        org.eclipse.jst.jsp.ui.tests.
    -->
    <target
        name="runOneTestBundle"
        depends="init"
        if="testBundle">
        <echo
            level="info"
            message="Starting Unit Tests for one test bundle"/>
        <!--
            unlike other ant tasks, subant defaults to false for
            inheritall!
        -->
        <subant
            target="performance"
            inheritall="true">
            <fileset dir="${eclipse-home}/">
                <include name="**/${testBundle}*/test.xml"/>
            </fileset>
        </subant>
        <echo
            level="info"
            message="Finished Unit Tests for one test bundle"/>
    </target>
    <target
        name="genHtml"
        description="Generates HTML results with provided JUNIT.XSL provided"
        unless="genHtml.disable">
        <xslt
            style="JUNIT.XSL"
            basedir="${results}/xml"
            destdir="${results}/html"/>
    </target>
</project>