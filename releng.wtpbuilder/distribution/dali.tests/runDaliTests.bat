@rem ***************************************************************************
@rem Copyright (c) 2009, 2019 IBM Corporation and others.
@rem This program and the accompanying materials
@rem are made available under the terms of the Eclipse Public License 2.0
@rem which accompanies this distribution, and is available at
@rem https://www.eclipse.org/legal/epl-2.0/
@rem
@rem SPDX-License-Identifier: EPL-2.0
@rem
@rem Contributors:
@rem     IBM Corporation - initial API and implementation
@rem ***************************************************************************
REM Example script to run standalone Dali tests for a given local build
REM
@echo off
SETLOCAL

REM These "home" directories must be accurate for your machine.
set BUILD_HOME=T:\work\runDaliTests
set ANT_HOME=C:\bin\ant-1.7.0
set JAVA_HOME=J:\jdk1.6.0_13
set JAVA_6_HOME=J:\jdk1.6.0_13

REM standard for windows. Linux or other platforms would need to change these.
set BASEOS=win32
set BASEWS=win32
set BASEARCH=x86

REM These 'set' commands should rarely, if ever have to be changed
REM Some are not actually used, but values are provided
REM to prevent spurious "warnings" in logs.
REM In other cases, the directory must exist before this script runs.
set PATH=%ANT_HOME%\bin;%JAVA_HOME%\bin;%PATH%
set BASE_BUILDERS=%BUILD_HOME%\basebuilders
set LOCAL_PREREQS_CACHE=%BUILD_HOME%\prereqcache
set RELENG=webtools.maps/releng
set RELENG_BUILDER=webtools.releng/releng.wtpbuilder
set DISPLAY=localhost:0
set BASE_BUILDERS=%BUILD_HOME%/basebuilders
set PROJECT_BUILDERS=%BUILD_HOME%/projectBuilders
set PROJECT_PROJECTS=%BUILD_HOME%/projects
set PROJECT_TESTS=%BUILD_HOME%/project-tests
set RECOMMENDED_TMP_DIR=%BUILD_HOME%/temp

REM Distribution specific properties
set DISTRIBUTION_NAME=dali
set PROJECT_NAME=Dali-Build
set RELENGDALI=webtools.maps/releng.dali

REM the following comments apply to the -D variables provided to the ant command
REM projectname: what ever the project would is called in cruisecontrol (even though CC isn't used here)
REM build.distribution: the name of the distribution as it appears in releng.wtpbuilder
REM build.committers: should be true if you want to download and reproduce a build from 'committers' area.
REM                   If working from a build on 'downloads' area, omit the variable (do not just set to false).
REM DbuildDirectory: location of the local build to test.
REM DtestRoot: location where the tests will be installed and run.
REM DkeyCfgFile: path of the releng build.cfg file.
REM Dwtp.builder.home: location of the releng.wtpbuilder.
REM buildType:        I, S, or R
REM buildId:          Basically the "middle" part of a download zip or build name, such as the 3.2I in wtp-sdk-I-3.2I-20090722042933.zip
REM timestamp         Basically the "end" part of a download zip or build name, such as 20090722042933 in wtp-sdk-I-3.2I-20090722042933.zip
REM build.stream      (not sure if needed)
REM buildBranch       Build branch used in various scripts ... usually the "middle" part of projectname, such as R3.2 in wtp-R3.2-I
REM testTarget        The specific test suite(s) to run. These target names are from the test.xml file. Omit or use 'all' to run all tests.
REM -f antfile        The name or path should not have to be changed, but the "releng.wtpbuilder" project must already be checked out from CVS
REM                   (or exported from Eclipse) into a directory named 'webtools.releng' which is immediately under the BUILD_HOME directory.
REM                   It is assume this batch file, then, is in the BUILD_HOME directory.
REM Note: the buildDirectory should have at least two folders, one named 'webtools.maps' which contains all the maps, 
REM and another folder which contains the result of a build and which is named ${buildType}-${buildId}-${timestamp}.

ant  -Dprojectname=%PROJECT_NAME% ^
     -Dbuild.distribution=%DISTRIBUTION_NAME%   ^
     -Dbuild.committers=true   ^
     -DbuildDirectory=%PROJECT_BUILDERS%/%PROJECT_NAME% ^
     -DtestRoot=%PROJECT_TESTS% ^
     -DkeyCfgFile=%PROJECT_BUILDERS%/%PROJECT_NAME%/%RELENG%/maps/build.cfg ^
     -Dwtp.builder.home=%BUILD_HOME%/%RELENG_BUILDER% ^
     -DbuildType=I ^
     -DbuildId=I200907231609 ^
     -Dtimestamp=200907231609 ^
     -Dbuild.stream=R3.2 ^
     -DbuildBranch=R3.2 ^
     -DtestTarget=dali-core-tests ^
     -f webtools.releng\releng.wtpbuilder\build.xml test

ENDLOCAL
