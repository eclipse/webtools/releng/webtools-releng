<!--
    Copyright (c) 2009, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->
<project
    name="Build specific targets and properties"
    default="runTest">
    <!--
        Note to be cross-platform, "environment variables" are only
        appropriate for some variables, e.g. ones we set, since
        properties are case sensitive, even if the environment variables
        on your operating system are not, e.g. it will be ${env.Path}
        not ${env.PATH} on Windows
    -->
    <property environment="env"/>
    <fail
        message="testRoot must be set for this task"
        unless="testRoot"/>
    <!-- required to get proper value of branch specific values -->
    <property
        name="keyCfgFile"
        value="${env.PROJECT_BUILDERS}/${projectname}/${env.RELENG}/maps/build.cfg"/>
    <echo
        level="debug"
        message="keyCfgFile: ${keyCfgFile}"/>
    <property file="${keyCfgFile}"/>


    <!-- 
        typcially already set, but in case of standalone tests, may not
        be
    -->
    <property
        name="buildLabel"
        value="${buildType}-${buildId}-${timestamp}"/>


    <!-- 
        Steps to do after the build is done. 
      -->
    <target name="test">
        <dirname
            file="${ant.file}"
            property="currentDirectory"/>
        <ant
            antfile="${ant.file}"
            target="runTest"
            dir="${currentDirectory}"/>
    </target>

    <!-- 
       Steps to do to test the build results
   -->
    <target name="runTest">
        <dirname
            file="${ant.file}"
            property="test.component.dir"/>
        <ant antfile="${wtp.builder.home}/scripts/build/label.xml"/>
        <property file="${buildDirectory}/label.properties"/>
        <property file="${wtp.builder.home}/build.properties"/>
        <fail
            message="testRoot must be set for this task"
            unless="testRoot"/>
        <mkdir dir="${testRoot}"/>

        <!-- if results already exist, delete them -->
        <delete
	     quiet="true"
            dir="${testRoot}/results"
            failOnError="false"/>
        <mkdir dir="${testRoot}/results"/>
        <mkdir dir="${testRoot}/results/consolelogs"/>
        <copy
            todir="${testRoot}"
            overwrite="true">
            <fileset dir="${test.component.dir}/testScripts"/>
        </copy>
        <echo level="debug"  message="BASEOS: ${env.BASEOS}   BASEWS: ${env.BASEWS}   BASEARCH: ${env.BASEARCH} "/>

        <property
            name="dependency.properties"
            value="${buildDirectory}/maps/${env.RELENG}/${dependencyFileLocation}"/>
        <available
            file="${dependency.properties}"
            property="dependency.properties.exists"/>
        <fail
            message="dependency file not found: ${dependency.properties}"
            unless="dependency.properties.exists"/>
        <property
            name="test.dependency"
            value="${wtp.builder.home}/scripts/dependency/dependency.xml"/>
        <available
            file="${test.dependency}"
            property="test.dependency.exists"/>
        <fail
            message="test dependency file not found: ${test.dependency}"
            unless="test.dependency.exists"/>
        <echo level="debug"  message="test.dependency file: ${test.dependency}"/>
        <ant
            antfile="${test.dependency}"
            target="get">
            <property
                name="base.install.dir"
                value="${testRoot}"/>


            <property
                name="tobeinstalled.properties"
                value="${buildDirectory}/maps/${env.RELENG}/distribution/${build.distribution}.tests/tobeInstalled.properties"/>

            <property
                name="installWorkingDirectory"
                value="${testRoot}"/>



        </ant>
        <antcall target="installTests"/>
        <antcall target="runTestEclipse">
            <param
                name="testTarget"
                value="all"/>
        </antcall>
        <antcall target="postRunTestEclipse">
            <param
                name="testTarget"
                value="all"/>
        </antcall>
    </target>

    <target name="installTests">

         <!-- hard code for now 
        <antcall
            target="installTestsFromZips">
            <param
                name="compName"
                value="patches32x"/>
        </antcall>
        -->
        <antcall target="installTestsFromRepo">
        </antcall>


    </target>

    <target
        name="installTestsFromZips"
        depends="init">

         <!-- a bit hard coded here, will need work for general case -->
        <property
            name="zipFile"
            value="${compName}-${buildLabel}.zip"/>


       <!--
     We use the exec method for unzip, so we won't fail if a 
     prereq can not be unzipped for some reason. See 
     https://bugs.eclipse.org/bugs/show_bug.cgi?id=283968 
-->
        <mkdir dir="${install.destination}"/>
        <exec
            dir="."
            executable="unzip">
            <arg line="-o -qq ${buildDirectory}/${buildLabel}/${zipFile} -d ${install.destination}/${compName}"/>
        </exec>

    </target>

    <target name="installTestsFromRepo">

        <java
            taskname="p2Director Install"
            fork="true"
            resultProperty="p2DirectorInstallResult"
            failonerror="false"
            timeout="${testTimeLimit}"
            dir="${testRoot}"
            jvm="${env.JAVA_7_HOME}/bin/java"
            classname="org.eclipse.equinox.launcher.Main">
            <classpath>
                <fileset dir="${testRoot}/eclipse/plugins">
                    <include name="org.eclipse.equinox.launcher_*.jar"/>
                </fileset>
            </classpath>
            <jvmarg value="-Djava.io.tmpdir=${env.RECOMMENDED_TMP_DIR}"/>
            <jvmarg value="-Dwtp.builder.home=${wtp.builder.home}"/>
            <jvmarg value="-Dbuild.distribution=${build.distribution}"/>
            <arg value="-nosplash"/>
            <arg value="-debug"/>
            <arg value="-consolelog"/>
            <arg value="-data"/>
            <arg value="${testRoot}/p2DirectorInstall"/>
            <arg value="-application"/>
            <arg value="org.eclipse.equinox.p2.director"/>
            <arg value="-destination"/>
            <arg value="${testRoot}/eclipse"/>

            <arg value="-repository"/>
            <arg value="file://${buildDirectory}/${buildLabel}/repository/,file://${buildDirectory}/${buildLabel}/repositoryunittests/"/>
            <arg value="-installIU"/>
            <arg value="${jsdtFeatureIUs},${jsdtTestFeatureIUs}"/>

            <!--  make sure our forked env has a DISPLAY -->
            <env
                key="DISPLAY"
                value="${env.DISPLAY}"/>
            <redirector
                output="${testRoot}/results/fullOutput.txt"
                error="${testRoot}/results/fullErrorLog.txt"/>

        </java>
         <!-- If the task succeeds, this ouput log won't be that relevent ... (can be make better in future ... but for now we'll make a copy, 
         just in case we need it -->
        <echo level="debug"  message="p2DirectorInstallResult: ${p2DirectorInstallResult}"/>

        <condition property="p2DirectorInstallFailed">
            <not>
                <equals
                    arg1="0"
                    arg2="${p2DirectorInstallResult}"/>
            </not>
        </condition>

<!-- always copy to test results, even when successful -->
        <copy
            file="${testRoot}/results/fullOutput.txt"
            tofile="${buildDirectory}/${buildLabel}/testResults/p2DirectorInstall.log.txt"
            overwrite="false"
            failonerror="false">
        </copy>

        <antcall target="handleFailedInstall"/>


    </target>
    <target
        name="handleFailedInstall"
        if="p2DirectorInstallFailed">

         <!-- copy to direct location, as a signal (to web pages) it failed and link should be provided -->
        <copy
            file="${testRoot}/results/fullOutput.txt"
            tofile="${buildDirectory}/${buildLabel}/p2DirectorInstall.log.txt"
            overwrite="false"
            failonerror="false">
        </copy>

        <fail
            message="installation of tests failed. See p2DirectorInstall.log.txt."
            if="p2DirectorInstallResult"/>

    </target>
    <!--
        time out may need to be set/adjust for api or performance tests?
        This testTimeLimit is the whole, overall limit on tests. There's
        a shorter one for individual suites. some common values, of
        milliseconds to more recognizable units: 
        14400000: 4 hours
        7200000: 2 hours 
        3600000: 1 hour  
        1800000: 30 minutes 
        600000: 10 minutes
    -->
    <property
        name="testTimeLimit"
        value="1800000"/>
    <property
        name="testFailOnError"
        value="false"/>
    <target
        name="runTestEclipse"
        description="Run our JUnit's within an instance of antRunner">
        <property
            name="test-vm"
            value="${env.JAVA_7_HOME}/bin/java"/>
        <!--
            set path to eclipse folder. If local folder, use '.';
            otherwise, use c:\path\to\eclipse or /path/to/eclipse/
        -->
        <property
            name="eclipse.home"
            value="${testRoot}"/>
        <echo level="debug" message="testTarget: ${testTarget}"/>
        <!--
            can not pass in empty values in jvmargs so if not testBundle
            specified, we'll pass a junk (unused) value
        -->
        <condition
            property="testBundleParam"
            value="-DtestBundle=${testBundle}"
            else="-Dunused=nouse">
            <isset property="testBundle"/>
        </condition>

        <echo message="Running junits"/>
        <!--
            If there is not exactly one launcher in the stack, we'd best
            fail fast, since we are not expecting that, and may indicate
            an installation that would produce unpredictable results
        -->
        <!--
            requires ant 1.7, and at this point, we're running 1.6 from
            eclipse ... <condition property="expectedNumberOfLaunchers">
            <resourcecount when="equal" count="1" > <fileset
            dir="${testRoot}/eclipse/plugins"> <include
            name="org.eclipse.equinox.launcher_*.jar" /> </fileset>
            </resourcecount> </condition> <fail message="Did not find
            expected number of launcher jars. Check installation."
            unless="expectedNumberOfLaunchers" />
        -->

        <condition
            property="antQuietValue"
            value="-quiet"
            else="">
            <istrue value="${env.USE_QUIET}"/>
        </condition>
        <java
            taskname="unit-test-masterprocess"
            fork="true"
            resultProperty="wtpmasterjunitresult"
            failonerror="false"
            timeout="${testTimeLimit}"
            dir="${testRoot}/eclipse"
            jvm="${env.JAVA_7_HOME}/bin/java"
            classname="org.eclipse.equinox.launcher.Main">
            <classpath>
                <fileset dir="${testRoot}/eclipse/plugins">
                    <include name="org.eclipse.equinox.launcher_*.jar"/>
                </fileset>
            </classpath>
            <jvmarg value="-Dosgi.ws=${env.BASEWS}"/>
            <jvmarg value="-Dosgi.os=${env.BASEOS}"/>
            <jvmarg value="-Dosgi.arch=${env.BASEARCH}"/>
            <jvmarg value="-Dws=${env.BASEWS}"/>
            <jvmarg value="-Dos=${env.BASEOS}"/>
            <jvmarg value="-Darch=${env.BASEARCH}"/>
            <jvmarg value="-DbuildBranch=${buildBranch}"/>
            <jvmarg value="-DbuildType=${buildType}"/>
            <jvmarg value="-DdependencyFileLocation=${dependencyFileLocation}"/>
            <jvmarg value="-DbuildId=${buildId}"/>
            <jvmarg value="-Dprojectname=${projectname}"/>
            <jvmarg value="-Djava.io.tmpdir=${env.RECOMMENDED_TMP_DIR}"/>
            <jvmarg value="-Dwtp.builder.home=${wtp.builder.home}"/>
            <jvmarg value="-Dbuild.distribution=${build.distribution}"/>
            <jvmarg value="${testBundleParam}"/>
            <arg value="-nosplash"/>
            <arg value="-consolelog"/>
            <arg value="-data"/>
            <arg value="${testRoot}/overallTestWorkspace"/>
            <arg value="-application"/>
            <arg value="org.eclipse.ant.core.antRunner"/>
            <arg value="${antQuietValue}"/>
            <arg value="-logger"/>
            <arg value="org.apache.tools.ant.DefaultLogger"/>
            <arg value="-file"/>
            <arg value="${testRoot}/test.xml"/>
            <arg value="${testTarget}"/>

            <!--  make sure our forked env has a DISPLAY -->
            <env
                key="DISPLAY"
                value="${env.DISPLAY}"/>
            <redirector
                output="${testRoot}/results/fullOutput.txt"
                error="${testRoot}/results/fullErrorLog.txt"/>
            <jvmarg value="-Dbuild.home=${env.BUILD_HOME}"/>
            <jvmarg value="-DbuildDirectory=${buildDirectory}"/>
            <jvmarg value="-DbuildLabel=${buildLabel}"/>
            <jvmarg value="-DbaseLocation=${baseLocation}"/>
            <jvmarg value="-DtestRoot=${testRoot}"/>
            <jvmarg value="-DtestDir=${testRoot}"/>
            <jvmarg value="-DeclipseBuilderDirectory=${pde.builder.path}"/>
            <!--
                <!- - IBM_JAVA_OPTIONS contains JIT work arounds for bug
                284441 - -> <env key="IBM_JAVA_OPTIONS"
                value="-Xjit:{org/eclipse/wst/html/core/internal/contenttype/HTMLHeadTokenizer.primGetNextToken()Ljava/lang/String;}(disableLookahead),{org/eclipse/jst/jsp/core/internal/parser/internal/JSPTokenizer.primGetNextToken()Ljava/lang/String;}(disableLookahead)"/>
            -->
        </java>
        <echo message="wtpmasterjunitresult ${wtpmasterjunitresult}"/>
    </target>
    <target name="postRunTestEclipse">
        <copy
            todir="${buildDirectory}/${buildLabel}/testResults/consolelogs/testLogs"
            overwrite="true"
            failonerror="false">
            <fileset dir="${testRoot}/results">
                <include name="*output.txt"/>
            </fileset>
        </copy>
        <copy
            todir="${buildDirectory}/${buildLabel}/testResults/consolelogs/testSysErrorLogs"
            overwrite="true"
            failonerror="false">
            <fileset dir="${testRoot}/results">
                <include name="*error.txt"/>
            </fileset>
        </copy>
        <copy
            file="${testRoot}/results/fullOutput.txt"
            toDir="${buildDirectory}/${buildLabel}/testResults/consolelogs/full"
            overwrite="true"
            failonerror="false">
        </copy>
        <copy
            file="${testRoot}/results/fullErrorLog.txt"
            toDir="${buildDirectory}/${buildLabel}/testResults/consolelogs/full"
            overwrite="true"
            failonerror="false">
        </copy>
        <copy
            todir="${buildDirectory}/${buildLabel}/testResults/xml"
            overwrite="true"
            failonerror="false">
            <fileset dir="${testRoot}/results/xml">
                <include name="*.xml"/>
            </fileset>
        </copy>
        <copy
            todir="${buildDirectory}/${buildLabel}/testResults/html"
            overwrite="true"
            failonerror="false">
            <fileset dir="${testRoot}/results/html">
                <include name="*.html"/>
            </fileset>
        </copy>
         <copy
             todir="${buildDirectory}/${buildLabel}/testResults/httplogstest"
             overwrite="true"
             failonerror="false">
             <fileset dir="${testRoot}/results/httplogstest">
                 <include name="*.log"/>
             </fileset>
         </copy>
         <copy
            todir="${buildDirectory}/${buildLabel}/testResults/"
            overwrite="true"
            failonerror="false">
            <fileset dir="${testRoot}/results/">
                <include name="*.html"/>
            </fileset>
        </copy>
    </target>

    <target name="init">

        <echo level="debug" message="dropinsFolder: ${dropinsFolder} "/>
        <condition
            property="install.destination"
            value="${testRoot}${dropinsFolder}/"
            else="${testRoot}/eclipse">
            <isset property="dropinsFolder"/>
        </condition>

    </target>

</project>