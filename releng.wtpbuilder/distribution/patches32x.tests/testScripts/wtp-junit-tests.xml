<?xml version="1.0"?>
<!--
    Copyright (c) 2010, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->

<project
    name="JUnit tests"
    basedir=".">
    <property environment="env"/>

    <!-- required to get proper value of branch specific properties -->
    <property
        name="keyCfgFile"
        value="${env.PROJECT_BUILDERS}/${projectname}/${env.RELENG}/maps/build.cfg"/>
    <echo
        level="info"
        message="keyCfgFile: ${keyCfgFile}"/>
    <property file="${keyCfgFile}"/>
    <target name="init">
        <property
            name="junit-report-output"
            value="${testRoot}/results"/>
        <mkdir dir="${junit-report-output}/raw"/>
        <!--
            if not otherwise set, be sure extraVMargs is at least some
            string, even if unused string. 
            
            Remember, though, extraVMArgs can contain more than one extra arg, 
            so much be passed to VM as a "line", not "value". 
        -->
        <property
            name="extraVMargs"
            value="-DextraVMargsunused=notused"/>

        <!-- if not otherwise set, use a unique workspace name -->
        <property
            name="data-dir"
            value="${testRoot}/junitworkspaces/workspace-${classname}"/>
        <!--
        Some common values, of milliseconds to more recognizable units: 
        14400000: 4 hours
        7200000: 2 hours 
        3600000: 1 hour  
        1800000: 30 minutes 
        600000: 10 minutes
        -->
        <property
            name="timeout"
            value="5400000"/>
        <property
            name="junit-report-output"
            value="${junit-report-output}"/>
        <property
            name="formatter"
            value="org.apache.tools.ant.taskdefs.optional.junit.XMLJUnitResultFormatter"/>
        <property
            name="test-output"
            value="${junit-report-output}/raw/${classname}.xml"/>
        <property
            name="jvm"
            value="${env.JAVA_7_HOME}/bin/java"/>
        <dirname
            file="${ant.file}"
            property="currentDir"/>
        <echo
            level="info"
            message="data-dir: ${data-dir}"/>
        <echo
            level="info"
            message="plugin-name: ${plugin-name}"/>
        <echo
            level="info"
            message="classname: ${classname}"/>
        <echo
            level="info"
            message="extraVMargs: ${extraVMargs}"/>
        <echo
            level="info"
            message="timeout: ${timeout}"/>
        <echo
            level="info"
            message="testRoot: ${testRoot}"/>
        <echo
            level="info"
            message="ant file basedir: ${basedir}"/>
        <echo
            level="info"
            message="junit-report-output: ${junit-report-output}"/>
        <echo
            level="info"
            message="test-output: ${test-output}"/>
        <echo
            level="info"
            message="buildDirectory: ${buildDirectory}"/>
        <echo
            level="info"
            message="buildLabel: ${buildLabel}"/>
        <echo
            level="info"
            message="jvm: ${jvm}"/>
    </target>
    <target
        name="core-test"
        depends="init">
        <echo
            level="info"
            message="test-type: core-test"/>
        <ant
            antfile="${ant.file}"
            dir="${currentDir}"
            target="eclipse-test">
            <property
                name="application"
                value="org.eclipse.test.coretestapplication"/>
        </ant>
    </target>
    <target
        name="ui-test"
        depends="init">
        <echo
            level="info"
            message="test-type: ui-test"/>
        <ant
            antfile="${ant.file}"
            dir="${currentDir}"
            target="eclipse-test">
            <property
                name="application"
                value="org.eclipse.test.uitestapplication"/>
        </ant>
    </target>
    <target
        name="eclipse-test"
        description="Runs the specified classname as a plug-in test.">
        <echo
            level="info"
            message="Running ${classname}"/>
        <echo
            level="info"
            message="Result file: ${junit-report-output}/${classname}.xml."/>
        <echo
            level="info"
            message="DISPLAY: ${env.DISPLAY}"/>
        <mkdir dir="${junit-report-output}/httplogstest"/>
        <echo message="http log messages to: ${junit-report-output}/httplogstest"/>
        <condition
            property="xulRunnerParam"
            value="org.eclipse.swt.browser.XULRunnerPath"
            else="XULRunnerPathNotSpecified">
            <isset property="env.WTP_XULRUNNER"/>
        </condition>
        <echo message="xulRunnerParam: ${xulRunnerParam}"/>
        <echo message="WTP_XULRUNNER: ${env.WTP_XULRUNNER}"/>
        <echo message="redirected output: ${junit-report-output}/${classname}.output.txt"/>
        <echo message="redirected error: ${junit-report-output}/${classname}.error.txt"/>
        <!--
            If there is not exactly one launcher in the stack, we'd best
            fail fast, since we are not expecting that, and may indicate
            an installation that would produce unpredictable results
        -->
        <!--
            requires ant 1.7, and at this point, we're running 1.6 from
            eclipse ... <condition property="expectedNumberOfLaunchers">
            <resourcecount when="equal" count="1" > <fileset
            dir="${testRoot}/eclipse/plugins"> <include
            name="org.eclipse.equinox.launcher_*.jar" /> </fileset>
            </resourcecount> </condition> <fail message="Did not find
            expected number of launcher jars. Check installation."
            unless="expectedNumberOfLaunchers" />
        -->
        <java
            taskname="suiteUnitTest"
            fork="true"
            resultProperty="suitejunitresult"
            failonerror="false"
            timeout="${timeout}"
            dir="${testRoot}/eclipse"
            jvm="${jvm}"
            classname="org.eclipse.equinox.launcher.Main">
            <classpath>
                <fileset dir="${testRoot}/eclipse/plugins">
                    <include name="org.eclipse.equinox.launcher_*.jar"/>
                </fileset>
            </classpath>
            <jvmarg value="-Xmx758m"/>
            <jvmarg value="-Xverify"/>
            <jvmarg value="-Dosgi.ws=${env.BASEWS}"/>
            <jvmarg value="-Dosgi.os=${env.BASEOS}"/>
            <jvmarg value="-Dosgi.arch=${env.BASEARCH}"/>
            <jvmarg value="-DbuildLabel=${buildLabel}"/>
            <jvmarg value="-DbuildDirectory=${buildDirectory}"/>
            <jvmarg value="-Djava.protocol.handler.pkgs=org.eclipse.wtp.releng.www.protocol"/>
            <jvmarg value="-DurlLogLocation=${junit-report-output}/httplogstest/outgoinghttplog-${plugin-name}.log"/>
            <jvmarg value="-Dwtp.autotest.noninteractive=true"/>
            <jvmarg line="${extraVMargs}"/>
            <jvmarg value="-Djava.io.tmpdir=${env.RECOMMENDED_TMP_DIR}"/>
            <jvmarg value="-Dprojectname=${projectname}"/>
            <jvmarg value="-Djunit-report-output=${junit-report-output}"/>
            <jvmarg value="-D${xulRunnerParam}=${env.WTP_XULRUNNER}"/>
            <jvmarg value="-Dwtp.builder.home=${wtp.builder.home}"/>
            <jvmarg value="-Dbuild.distribution=${build.distribution}"/>
            <arg value="-nosplash"/>
            <arg value="-consolelog"/>
            <arg value="-debug"/>
            <arg value="-data"/>
            <arg value="${data-dir}"/>
            <arg value="-application"/>
            <arg value="${application}"/>
            <arg value="formatter=${formatter},${test-output}"/>
            <arg value="-testPluginName"/>
            <arg value="${plugin-name}"/>
            <arg value="-className"/>
            <arg value="${classname}"/>
            <env
                key="DISPLAY"
                value="${env.DISPLAY}"/>
            <redirector
                output="${junit-report-output}/${classname}.output.txt"
                error="${junit-report-output}/${classname}.error.txt"/>
        </java>
        <echo
            level="info"
            message="suitejunitresult: ${suitejunitresult}"/>
        <antcall target="summarizeFailure"/>
        <antcall target="collect-results"/>
    </target>
    <target name="collect-results">
        <dirname
            property="output-dir"
            file="${test-output}"/>
        <basename
            property="output-file-name"
            file="${test-output}"/>
        <mkdir dir="${junit-report-output}/xml"/>
        <junitreport
            todir="${junit-report-output}/xml"
            tofile="${classname}.xml">
            <fileset dir="${output-dir}">
                <include name="${output-file-name}"/>
            </fileset>
        </junitreport>
        <mkdir dir="${junit-report-output}/html"/>
        <xslt
            style="${testRoot}/JUNIT.XSL"
            basedir="${junit-report-output}"
            includes="${classname}.xml"
            destdir="${junit-report-output}/html"/>
    </target>
    <target name="collect">
        <!--
            deprecated, each test.xml uses it, and we should remove,
            eventually
        -->
        <echo
            message="Deprecated. There is no need to call the collect task. It no longer does anything. Please remove from your test.xml file"/>
    </target>
    <target
        name="summarizeFailure"
        depends="checkFailure"
        if="doFailure">
        <!--
            TODO: fix sdo junitFailureList.log goes in 'results', at
            first. Then copied. Then coordinate with releng scripts
        -->
        <echo
            append="true"
            file="${buildDirectory}/junitFailureList.log">${plugin-name}${line.separator}</echo>
    </target>
    <target name="checkFailure">
        <condition property="doFailure">
            <not>
                <equals
                    arg1="${suitejunitresult}"
                    arg2="0"
                    trim="true"/>
            </not>
        </condition>
    </target>
</project>