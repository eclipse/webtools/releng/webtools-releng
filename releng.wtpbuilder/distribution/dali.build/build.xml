<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright (c) 2006, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->

<project
    default="build"
    basedir=".">
    <fail unless="wtp.builder.home"/>
    <property file="${wtp.builder.home}/build.properties"/>
    <property environment="env"/>

     <!-- required to get proper value of doCreateFprojZips -->
    <property
        name="keyCfgFile"
        value="${env.PROJECT_BUILDERS}/${projectname}/${env.RELENG}/maps/build.cfg"/>
    <echo
        level="info"
        message="keyCfgFile: ${keyCfgFile}"/>
    <property file="${keyCfgFile}"/>

    <echo message="transformedRepoLocation: ${transformedRepoLocation}"/>
    <mkdir dir="${transformedRepoLocation}"/>


     <!-- main -->
    <target
        name="build"
        depends="getBaseBuilder">

        <property
            name="buildTargets"
            value="${wtp.builder.home}/scripts/build/runbuild.xml"/>
        <ant antfile="${wtp.builder.home}/scripts/build/label.xml"/>

        <!-- for this distribution, dependencies come from dali.releng -->
        <property
            name="dependency.properties"
            value="${buildDirectory}/maps/${env.RELENGDALI}/maps/dependencies.properties"/>

        <ant antfile="${buildTargets}">
            <property
                name="component"
                value="${build.distribution}-sdk"/>
        </ant>
        <ant antfile="${buildTargets}">
            <property
                name="component"
                value="${build.distribution}.tests"/>
        </ant>

         <!-- copy early, if we can ... else site won't be created, if we fail -->
        <ant antfile="${wtp.builder.home}/scripts/build/copyArtifactsEarly.xml"/>

        <antcall target="handleCompilationProblems"/>

    </target>

    <target
        name="handleCompilationProblems"
        depends="checkIfCompileProblems"
        if="compilation.problem.found">

         <!-- if there is a problem, leave a marker file in download directory, so the webpage can 
         describe what happened -->

        <echo
            message="Compilation problems were found"
            file="${buildDirectory}/${buildLabel}/compilationProblems.txt"/>

       <!-- always force an early copy, before fail, so all results and files get to "downloads" area -->
        <ant antfile="${wtp.builder.home}/scripts/build/copyArtifactsEarly.xml"/>
        <fail
            if="compilation.problem.found"
            message="The remaining build and tests were canceled since compilation problems were found."/>
    </target>


    <target name="checkIfCompileProblems">
       <!-- 
       Not sure how reliable the presence of this 'compilation.problem' file is ... 
                I just noticed it in recent PDE builds with compile problems. 
                May have to make this "fancier" in future. 
                For now, looks only one level down in plugins directory (in build's workdir) for a file with 
                the name 'complilation.problem'. We may have a "summary" in xml file format, summarizing logs, 
                 ... but not sure then created (during site? We'd have to call site first. This way, we'll see if the match :) . 
-->
        <available
            file="compilation.problem"
            type="file"
            property="compilation.problem.found">
            <filepath>
                <dirset dir="${buildDirectory}">
                    <include name="plugins/*"/>
                </dirset>

            </filepath>
        </available>
        <echo message="compilation.problem path buildDirectory: ${buildDirectory}/plugins/*/"/>
    </target>


    <!-- invoking runbuild.xml targetting getBaseBuilder -->
    <target
        name="getBaseBuilder">

        <ant
            antfile="${wtp.builder.home}/scripts/build/runbuild.xml"
            target="getBaseBuilder"/>

    </target>
</project>