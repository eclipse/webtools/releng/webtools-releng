<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<!-- generated from releng.wtpbuilder/distribution/wtp.site/templateFiles/index.html.template.php -->
<!-- DEPRECATED. See webtools.releng.aggregator/wtp-parent/templateFiles/index.html.template.php -->

<?php

//ini_set("display_errors", "true");
//error_reporting (E_ALL);

include_once("miscUtil.php");
include_once('buildvariables.php');
include_once('dependency.properties.php');

// our summary results handling requires php 5 (for simple xml file loading)
// so, if not php 5, just don't display any summary results
// This was found to be required, since some mirror our whole site (e.g. IBM)
// and not all mirrors use PHP 5
$displayTestSummary=false;
if (phpversion() >= 5) {

     $code_totalBundles=0;
     $code_totalErrors=0;
     $code_totalWarnings=0;
     $code_totalforbiddenAccessWarningCount=0;
     $code_totaldiscouragedAccessWarningCount=0;

     $test_totalBundles=0;
     $test_totalErrors=0;
     $test_totalWarnings=0;
     $test_totalforbiddenAccessWarningCount=0;
     $test_totaldiscouragedAccessWarningCount=0;


     $displayTestSummary=true;
     // expecting grandTotalErrors and grandTotalTests
     $filename = "unitTestsSummary.xml";
     if (file_exists($filename)) {
          $prefix = "unitTests_";
          $unitTestsSummary = simplexml_load_file($filename);
          foreach ($unitTestsSummary->summaryItem as $summaryItem) {
               $name = $summaryItem->name;
               $value = $summaryItem->value;
               $code= "\$" . $prefix . $name . " = " . $value . ";";
               //echo "<br />code: " . $code;
               eval($code);
          }
     }

     $filename = "compilelogsSummary.xml";
     if (file_exists($filename)) {
          $prefix = "code_";
          $compileSummary = simplexml_load_file($filename);
          foreach ($compileSummary->summaryItem as $summaryItem) {
               $name = $summaryItem->name;
               $value = $summaryItem->value;
               $code= "\$" . $prefix . $name . " = " . $value . ";";
               //echo "<br />code: " . $code;
               eval($code);
          }
     }

     $filename = "testcompilelogsSummary.xml";
     if (file_exists($filename)) {
          $prefix = "test_";
          $compileSummary = simplexml_load_file($filename);
          foreach ($compileSummary->summaryItem as $summaryItem) {
               $name = $summaryItem->name;
               $value = $summaryItem->value;
               $code= "\$" . $prefix . $name . " = " . $value . ";";
               //echo "<br />code: " . $code;
               eval($code);
          }
     }
}


?>

<?php include 'headingIntro.php';?>


<?php include 'displayPrereqs.php';?>

<!-- ***********  P2 Zips **************  -->



<table border=0 cellspacing=2 cellpadding=2 width="100%">
     <tr>
          <td align=left valign=top colspan="2" bgcolor="#0080C0"><font
               face="'Bitstream Vera',Helvetica,Arial" color="#FFFFFF">p2 repos (update sites)</font></td>
     </tr>

     <tr>
          <td align="left" valign="top" colspan="5">
          <p>These are p2 repositories (or zips of repos). Zips can be
          downloaded and installed into a development environment or PDE target.
          <br/>Do not simply unzip these - you should use Eclipse / p2 to perform an installation instead.</p>
          </td>
     </tr>

     <tr>
          <td>
          <table border=0 cellspacing=2 cellpadding=2 width="90%" align="center">

          <?php

          $shortname=$build_distribution."-repo";

          $zipfilename=$shortname."-".$build;
          $filename=$zipfilename.".zip";
          if (file_exists($filename)) {
               ?>
               <tr>
                    <td align="left" valign="top" width="10%"><b><a href=repository>Code Repository</a></b></td>
                    <td align="left" valign="top">
                    <p>Archived <a href=repository>p2 repository of WTP code</a>. Good for product builders. </p>
                    </td>
                    <?php
                    $zipfilesize=fileSizeForDisplay($filename);

                    displayFileLine($downloadprefix, $filename, $zipfilesize, $shortname);
                    ?>
               </tr>
               <?php } ?>
              <?php

              $shortname=$build_distribution."-tests-repo";

              $zipfilename=$shortname."-".$build;
              $filename=$zipfilename.".zip";
              if (file_exists($filename)) {
                     ?>
                     <tr>
                            <td align="left" valign="top" width="10%"><b><a href=repositoryunittests>Tests Repository</a></b></td>
                            <td align="left" valign="top">
                            <p>Archived <a href=repositoryunittests>p2 repository of WTP unit tests</a>.</p>
                            </td>
                            <?php
                            $zipfilesize=fileSizeForDisplay($filename);

                            displayFileLine($downloadprefix, $filename, $zipfilesize, $shortname);
                            ?>
                     </tr>
                     <?php } ?>
          </table>

     </tr>
</table>


<!-- ***********  Traditional Zips **************  -->
<table border=0 cellspacing=2 cellpadding=2 width="100%">
     <tr>
          <td align=left valign=top colspan="5" bgcolor="#0080C0"><font
               face="'Bitstream Vera',Helvetica,Arial" color="#FFFFFF"> Traditional
          Zip Files</font></td>
     </tr>

     <tr>
          <td align="left" valign="top" colspan="5"><?php
          $zipbellwether_name="$build_distribution-$build.zip";
          if (file_exists($zipbellwether_name)) {
               // if-then-include section
               // include this whole section if (and only if) the bellwether file exists.
               // For example, may not have been created yet
               ?>

          <p>The Zip files includes the features and plugins for this build.</p>
          <p>Note: These traditional zip files should be considered deprecated,
          in favor of installing the archived P2 repositories. The tradtional
          zip files may eventually no longer be provided.</p>
          <?php } else { ?>
          <p>The traditonal zip files have not been produced yet (or, perhaps,
          the build failed or was cancelled). Check back in few minutes.</p>
          <?php } ?></td>
     </tr>
     <tr>
          <td>
          <table border=0 cellspacing=2 cellpadding=2 width="90%" align="center">

          <?php
          $zipfilename="$build_distribution-$build";
          $filename=$zipfilename.".zip";
          if (file_exists($filename)) {
               ?>
               <tr>
                    <td align="left" valign="top" width="10%"><b>Web App Developers:</b></td>
                    <td align="left" valign="top" width="55%">
                    <p>This non-SDK package is for most users. It includes the runnable
                    code and end-user documentation for those using the tools to develop
                    web applications.</p>
                    </td>
                    <?php
                    $zipfilesize=fileSizeForDisplay($filename);
                    $fileShortDescription=$build_distribution;
                    displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);
                    ?>
               </tr>
               <?php } ?>

               <?php
               $zipfilename=$build_distribution."-sdk-".$build;
               $filename=$zipfilename.".zip";
               if (file_exists($filename)) {
                    ?>
               <tr>
                    <td align="left" valign="top" width="10%"><b>Tool Developers:</b></td>
                    <td align="left" valign="top">
                    <p>The SDK package includes source code and developer documentation
                    for those using WTP as a platform to build more tools, as well as
                    everything that is in the non-SDK version.</p>
                    </td>
                    <?php
                    $zipfilesize=fileSizeForDisplay($filename);
                    $fileShortDescription=$build_distribution . "-sdk";
                    displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);
                    ?>
               </tr>
               <?php } ?>

               <?php
               $zipfilename=$build_distribution."-tests-".$build;
               $filename=$zipfilename.".zip";
               if (file_exists($filename)) {
                    ?>
               <tr>
                    <td align="left" valign="top" width="10%"><b>Unit Tests</b></td>
                    <td align="left" valign="top">
                    <p>The automated test zip contains unit tests.</p>
                    </td>
                    <?php
                    $zipfilesize=fileSizeForDisplay($filename);
                    $fileShortDescription=$build_distribution . "-tests";
                    displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);
                    ?>

               </tr>
               <?php } ?>
          </table>
          </td>


     </tr>
</table>

               <?php include 'webdev.php'; ?>

               <?php include 'fproject.php'; ?>



<!-- ***********  Build Status **************  -->
<table border=0 cellspacing=2 cellpadding=2 width="100%">
     <tr>
          <td align=left valign=top bgcolor="#0080C0"><font
               face="'Bitstream Vera',Helvetica,Arial" color="#FFFFFF">Status, tests
          and other interesting details</font></td>
     </tr>
     <tr>
          <td>
          <table border=0 cellspacing=2 cellpadding=2 width="90%" align="center">

               <tr>
                    <td><?php 
                    if (isset($displayBuildNotes) && $displayBuildNotes) {
                         echo "<a href=\"buildNotes.php\">Build notes</a> <br />";
                    }
                    ?> <a href="directory.txt">map files</a> <br />
                    <?php

                    if (file_exists("components")) {
                         echo "<a href=\"components/components.php\">Misc Components</a> <br />\n";
                    }


                    if ($displayTestSummary) {


                         if (isset($unitTests_grandTotalErrors)) {
                              $errorColor="green";
                              if ($unitTests_grandTotalErrors > 0) {
                                   $errorColor="red";
                              }
                              echo "<a href=\"testResults.php\">Unit test results</a>&nbsp;";
                              echo "<img src=\"junit_err.gif\"/><font color=\"" . $errorColor . "\">" . $unitTests_grandTotalErrors . "</font>&nbsp;&nbsp;Total: " . $unitTests_grandTotalTests;
                         }
                         else {
                              $compileProblemMarkerFile="compilationProblems.txt";
                              if (file_exists($compileProblemMarkerFile)) {
                                   echo "<br /> <img src=\"compile_err.gif\"/>&nbsp;&nbsp;No unit tests available. The remaining build and tests were canceled since compilation problems were found. Check compiler output summaries.";
                              } else {
                                   $installLogName="p2DirectorInstall.log.txt";
                                   if (file_exists($installLogName)) {
                                        echo "<br /><img src=\"compile_err.gif\"/>&nbsp;&nbsp;No unit tests available. See <a href=\"" . $installLogName . "\">the p2Director install log file </a>from failed test installation attempt";
                                   } else {
                                        $noTestsProvidedMarkerFile="noTestsProvided.txt";
                                        if (file_exists($noTestsProvidedMarkerFile)) {
                                             echo "<br /><img src=\"compile_warn.gif\"/>&nbsp;&nbsp;No unit tests available. This build component does not providing any unit tests.";
                                        } else {

                                             // we may really be pending (tests still running) or maybe they failed in unexpted way?.
                                             echo "<br /><font color=\"red\">Unit tests are not available in this build. See latest <a href=\"https://hudson.eclipse.org/webtools/view/webtools_R3_10/\">CI builds here</a> for unit tests per component.</font>";

                                        }
                                   }
                              }
                         }





                         echo "<br />";

                         echo "<br />";
                         echo "<a href=\"compileResults.php\">Compile logs: Code Bundles</a>";

                         echo "&nbsp;&nbsp;($code_totalBundles)&nbsp;&nbsp;";
                         echo "<img src=\"compile_err.gif\"/><font color=red>$code_totalErrors</font>&nbsp;";
                         echo "<img src=\"compile_warn.gif\"/><font color=orange>$code_totalWarnings</font>&nbsp;";
                         echo "<img src=\"access_err.gif\"/><font color=red>$code_totalforbiddenAccessWarningCount</font>&nbsp;";
                         echo "<img src=\"access_warn.gif\"/><font color=orange>$code_totaldiscouragedAccessWarningCount</font>&nbsp;";

                         echo "<br />";
                         echo "<a href=\"testCompileResults.php\">Compile logs: Test Bundles</a>";

                         echo "&nbsp;&nbsp;($test_totalBundles)&nbsp;&nbsp;";
                         echo "<img src=\"compile_err.gif\"/><font color=red>$test_totalErrors</font>&nbsp;";
                         echo "<img src=\"compile_warn.gif\"/><font color=orange>$test_totalWarnings</font>&nbsp;";
                         echo "<img src=\"access_err.gif\"/><font color=red>$test_totalforbiddenAccessWarningCount</font>&nbsp;";
                         echo "<img src=\"access_warn.gif\"/><font color=orange>$test_totaldiscouragedAccessWarningCount</font>&nbsp;";

                         $logname="antBuilderOutput.log";
                         if (file_exists($logname)) {
                              echo "<br /> <a href=\"".$logname."\" type=\"text/plain\">Full Log</a> from build ". fileSizeForDisplay($logname);
                         }


                    }

                    ?> <br />

                    <?php
                    if (file_exists("versioningReportName.php")) {
                         include "versioningReportName.php";
                         $fname=$versionReportFilename.".html";
                         if (file_exists($fname)) {
                              echo "<br /> <a href='$fname'>Versioning Information</a>";
                         }
                    }
                    ?></td>
               </tr>
          </table>
          </td>
     </tr>
</table>



<!-- footer -->
<center>
<hr>
<p>All downloads are provided under the terms and conditions of the <a
     href="http://www.eclipse.org/legal/notice.html">Eclipse.org Software
User Agreement</a> unless otherwise specified.</p>

<p>If you have problems downloading the drops, contact the <font
     face="'Bitstream Vera',Helvetica,Arial" size="-1"><a
     href="mailto:webmaster@eclipse.org">webmaster</a></font>.</p>

</center>
<!-- end footer -->








</body>
</html>
