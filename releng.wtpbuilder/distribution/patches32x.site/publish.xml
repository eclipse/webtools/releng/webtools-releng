<!--
    Copyright (c) 2010, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->
<project
    name="Publish Build"
    default="default"
    basedir=".">
    <!--
        Properties that must be passed to this script: buildDirectory:
        Path to perform the build in. (A working directory) buildType:
        Type of build (nightly, integration etc.) buildId: Build name
        buildLabel: <buildType>-<buildName>-<timestamp>
    -->
    <property environment="env"/>
    <fail
        unless="buildDirectory"
        message="buildDirectory must be provided to publish"/>
    <fail
        unless="buildLabel"
        message="buildLabel must be provided to publish"/>
    <fail
        unless="wtp.builder.home"
        message="wtp.builder.home must be provided to publish"/>
    <fail
        unless="build.distribution"
        message="build.distribution must be provided to publish"/>
    <property
        name="resultDir"
        value="${buildDirectory}/${buildLabel}"/>
    <!--name of generated index page-->
    <property
        name="indexFileName"
        value="index.php"/>
    <property
        name="templateDir"
        value="${wtp.builder.home}/distribution/${build.distribution}.site"/>
    <property
        name="resultingIndex"
        value="${resultDir}/${indexFileName}"/>
    <echo
        level="debug"
        message="resultDir: ${resultDir}"/>
    <echo
        level="debug"
        message="templateDir: ${templateDir}"/>
    <echo
        level="debug"
        message="resultingIndex: ${resultingIndex}"/>

    <property
        name="dependency.properties"
        value="${buildDirectory}/maps/${env.RELENG}/${dependencyFileLocation}"/>
    <available
        file="${dependency.properties}"
        property="dependency.properties.exists"/>
    <fail
        message="dependency file not found: ${dependency.properties}"
        unless="dependency.properties.exists"/>
    <echo level="debug"  message="dependency file: ${dependency.properties}"/>

    <target name="default">
        <copy todir="${resultDir}">
            <fileset dir="${templateDir}/templateFiles"/>
        </copy>

        <copy
            todir="${resultDir}"
            file="${buildDirectory}/maps/${env.RELENG}/distribution/${build.distribution}.site/prereqsToDisplay.php"/>

        <copy
            overwrite="true"
            file="${wtp.builder.home}/distribution/${build.distribution}.site/templateFiles/${indexTemplateFilename}"
            tofile="${resultingIndex}"/>
        <copy
            overwrite="true"
            file="${wtp.builder.home}/distribution/${build.distribution}.site/templateFiles/components.template.php"
            tofile="${resultDir}/components/components.php"/>
        <antcall target="countFiles"/>
        <antcall target="generateCompileIndex"/>
        <antcall target="generateJUnitTestsIndex"/>
        <antcall target="getStaticFiles"/>
    </target>
    <target name="generateCompileIndex">

        <property
            name="dependency.properties"
            value="${buildDirectory}/maps/${env.RELENG}/${dependencyFileLocation}"/>

        <available
            file="${dependency.properties}"
            property="dependency.properties.exists"/>

        <fail
            message="dependency file not found: ${dependency.properties}"
            unless="dependency.properties.exists"/>

        <!-- create tables of test plugin compiles first -->
        <antcall target="generateTestCompileOutput">
            <param
                name="outputDirectory"
                value="${resultDir}/testcompilelogs"/>
        </antcall>

        <!-- create table of code plugins compiles -->
        <antcall target="generateCompileOutput">
            <param
                name="outputDirectory"
                value="${resultDir}/compilelogs"/>
        </antcall>

        <tstamp>
            <format
                property="TODAY"
                pattern="MMMM d, yyyy"/>
        </tstamp>

        <antcall target="replaceData"/>

        <!--
            Update timestamp on file to permit overwrite through Ant
            copy task
        -->
        <touch file="${resultingIndex}"/>

    </target>
    <target
        name="generateTestCompileOutput"
        depends="checkIfCompileLogs"
        if="doCompileLogs">
        <summarizeResults
            isBuildTested="false"
            includeAll="true"
            dropTokenList="${dropTokenList}"
            xmlDirectoryName=""
            dropDirectoryName="${resultDir}"
            testResultsTemplateFileName="${templateDir}/templateFiles/testCompileResults.php.template"
            testResultsHtmlFileName="testCompileResults.php"
            hrefTestResultsTargetPath=""
            hrefCompileLogsTargetPath="testcompilelogs"
            compileLogsDirectoryName="${outputDirectory}"/>
        <antcall target="compilerXMLToHTML"/>
    </target>
    <target
        name="generateCompileOutput"
        depends="checkIfCompileLogs"
        if="doCompileLogs">
        <summarizeResults
            isBuildTested="false"
            includeAll="true"
            dropTokenList="${dropTokenList}"
            xmlDirectoryName=""
            dropDirectoryName="${resultDir}"
            testResultsTemplateFileName="${templateDir}/templateFiles/compileResults.php.template"
            testResultsHtmlFileName="compileResults.php"
            hrefTestResultsTargetPath="testResults/html"
            hrefCompileLogsTargetPath="compilelogs"
            compileLogsDirectoryName="${outputDirectory}"/>
        <antcall target="compilerXMLToHTML"/>
    </target>
    <target name="compilerXMLToHTML">
        <xslt
            basedir="${outputDirectory}"
            destdir="${outputDirectory}"
            includes="**/*.xml"
            scanincludeddirectories="true"
            style="compilerXMLOutputToHTML.xsl"
            force="true"/>
    </target>
    <target
        name="generateJUnitTestsIndex"
        depends="checkIfTested"
        if="doTests">
        <!--
            isBuildTested: true|false should JUnit plugin test results
            be used to generate index page dropTokenList: comma
            separated list of strings which should be replaced by the
            fileName attribute settings in the testManifest.xml.
            xmlDirectoryName: path to directory containing JUnit plugin
            test results in xml format (see doc is org.eclipse.test).
            dropDirectoryName: path to directory containing the result
            of the build. testResultsTemplateFileName: path to template
            file used to generate page with links to JUnit test results
            testResultsHtmlFileName: name of file which will be
            generated with links to JUnit test results dropHtmlFileName:
            name of generated index page hrefTestResultsTargetPath:
            relative path from index page to directory containing JUnit
            html test results hrefCompileLogsTargetPath: relative path
            from index page directory containing compilelogs
            testManifestFileName: name of xml file containing
            descriptions of zip types and log files
        -->

        <property
            name="dependency.properties"
            value="${buildDirectory}/maps/${env.RELENG}/${dependencyFileLocation}"/>
        <available
            file="${dependency.properties}"
            property="dependency.properties.exists"/>
        <fail
            message="dependency file not found: ${dependency.properties}"
            unless="dependency.properties.exists"/>

        <!-- create table of test plugin compiles first -->
        <!-- set isBuildTested to true for the "compile only" results -->
        <summarizeResults
            isBuildTested="true"
            dropTokenList="${dropTokenList}"
            dropDirectoryName="${resultDir}"
            xmlDirectoryName="${resultDir}/testResults/xml"
            testResultsTemplateFileName="${templateDir}/templateFiles/testResults.php.template.php"
            testResultsHtmlFileName="testResults.php"
            hrefTestResultsTargetPath="testResults/html"
            compileLogsDirectoryName=""/>
        <tstamp>
            <format
                property="TODAY"
                pattern="MMMM d, yyyy"/>
        </tstamp>

        <antcall target="replaceData"/>
        
        <!--
            Update timestamp on file to permit overwrite through Ant
            copy task
        -->
        <touch file="${resultingIndex}"/>

    </target>
    <target name="getStaticFiles">
        <!--get static files required in the buildLabel directory-->
        <copy todir="${resultDir}">
            <fileset dir="${templateDir}/staticDropFiles"/>
        </copy>

        <!--copy buildnotes from plugin directories-->
        <mkdir dir="${resultDir}/buildnotes"/>
        <copy
            failonerror="false"
            todir="${resultDir}/buildnotes"
            flatten="true">
            <fileset
                dir="${buildDirectory}/plugins"
                includes="**/buildnotes_*.html"/>
            <fileset
                dir="${buildDirectory}/features"
                includes="**/buildnotes_*.html"/>
        </copy>
        <copy
            failonerror="false"
            file="${templateDir}/staticDropFiles/logIndex.php"
            tofile="${resultDir}/testResults/consolelogs/full/logIndex.php"/>
        <copy
            failonerror="false"
            file="${templateDir}/staticDropFiles/logIndex.php"
            tofile="${resultDir}/testResults/consolelogs/testLogs/logIndex.php"/>
        <copy
            failonerror="false"
            file="${templateDir}/staticDropFiles/logIndex.php"
            tofile="${resultDir}/testResults/consolelogs/testSysErrorLogs/logIndex.php"/>
    </target>

    <target name="countFiles">

        <resourcecount property="files.count">
            <fileset dir="${resultDir}">
                <include name="*.zip"/>
            </fileset>
        </resourcecount>
        <echo file="${resultDir}/files.count">${files.count}</echo>
    </target>


    <target name="checkIfTested">
        <echo
            level="debug"
            message="isBuildTested: ${isBuildTested}"/>
        <condition property="doTests">
            <equals
                arg1="${isBuildTested}"
                arg2="true"
                trim="true"
                casesensitive="false"/>
        </condition>
    </target>
    <target name="checkIfCompileLogs">
        <condition property="doCompileLogs">
            <available file="${outputDirectory}"/>
        </condition>
        <echo level="debug" message="doCompileLogs: ${doCompileLogs)   outputDirectory: ${outputDirectory}"/>
    </target>
    <target name="replaceData">



        <writePropertiesAsPHP
            propFileName="${dependency.properties}"
            outPHPFileName="${resultDir}/dependency.properties.php"
            buildlabel="${buildLabel}"/>
         

        
        <!--
        
        <property file="${dependency.properties}"/>
        
        -->

        <replace
            dir="${resultDir}"
            includes="**/buildvariables.php"
            value=""
            propertyFile="${dependency.properties}">      

        <!-- general build info -->

            <replacefilter
                token="@buildtype@"
                value="${buildType}"/>

            <replacefilter
                token="@date@"
                value="${TODAY}"/>

            <replacefilter
                token="@build@"
                value="${buildLabel}"/>

            <replacefilter
                token="@buildBranch@"
                value="${buildBranch}"/>

            <replacefilter
                token="@build_distribution@"
                value="${build.distribution}"/>

        </replace>

    </target>
</project>