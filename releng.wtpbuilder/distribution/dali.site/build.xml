<!--
    Copyright (c) 2006, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->
<project
    name="Build specific targets and properties"
    default="build"
    basedir=".">
    <!--
        Note to be cross-platform, "environment variables" are only
        appropriate for some variables, e.g. ones we set, since
        properties are case sensitive, even if the environment variables
        on your operating system are not, e.g. it will be ${env.Path}
        not ${env.PATH} on Windows
    -->
    <property environment="env"/>
    <!--
        Let users override standard properties, if desired. If
        directory, file, or some properties do not exist, then standard
        properties will be used.
    -->
    <property file="${env.LOCAL_BUILD_PROPERTIES_DIR}/${ant.project.name}.properties"/>

    <!-- = = = end standard properties pattern = = = -->
    <echo level="debug" message="ant.file: ${ant.file}"/>
    <target name="build">

          <!-- make sure there is a base builder, since we need the launcher -->
        <ant
            antfile="${wtp.builder.home}/scripts/build/runbuild.xml"
            target="getBaseBuilder"/>
        <condition
            property="antQuietValue"
            value="-quiet"
            else="">
            <istrue value="${env.USE_QUIET}"/>
        </condition>
        <!-- TODO: should set 'failonerror' to false, and follow up with error 
        check and appropriate cleanup/finishup actions before failing -->

        <java
            fork="true"
            failonerror="true"
            classname="org.eclipse.equinox.launcher.Main">
            <classpath>
                <fileset dir="${pde.builder.path}/plugins">
                    <include name="org.eclipse.equinox.launcher_*.jar"/>
                </fileset>
            </classpath>
            <jvmarg value="-Dosgi.ws=${env.BASEWS}"/>
            <jvmarg value="-Dosgi.os=${env.BASEOS}"/>
            <jvmarg value="-Dosgi.arch=${env.BASEARCH}"/>
            <jvmarg value="-Dbuild.donottagmaps=${build.donottagmaps}"/>
            <jvmarg value="-DbuildBranch=${buildBranch}"/>
            <jvmarg value="-DbuildType=${buildType}"/>
            <jvmarg value="-DdependencyFileLocation=${dependencyFileLocation}"/>

            <jvmarg value="-DbuildId=${buildId}"/>
            <jvmarg value="-DmapVersionTag=${mapVersionTag}"/>
            <jvmarg value="-Dbuild.distribution=${build.distribution}"/>
            <jvmarg value="-DbuildDirectory=${buildDirectory}"/>
            <jvmarg value="-Dwtp.builder.home=${wtp.builder.home}"/>
            <jvmarg value="-Dprojectname=${projectname}"/>
            <jvmarg value="-Djava.io.tmpdir=${env.RECOMMENDED_TMP_DIR}"/>
            <jvmarg value="-Djava.protocol.handler.pkgs=org.eclipse.wtp.releng.www.protocol"/>
            <jvmarg value="-DurlLogLocation=${buildDirectory}/${buildLabel}/outgoinghttplogfromsitebuild.log"/>
            <arg value="-data"/>
            <arg value="${basedir}/workspace"/>
            <arg value="-application"/>
            <arg value="org.eclipse.ant.core.antRunner"/>
            <arg value="${antQuietValue}"/>
            <arg value="-buildfile"/>
            <arg value="${ant.file}"/>
            <arg value="publish"/>
        </java>
    </target>
    <!--
        =====================================================================
    -->
    <!-- Steps to do to publish the build results -->
    <!--
        =====================================================================
    -->
    <target name="publish">
        <dirname
            file="${ant.file}"
            property="component.dir"/>
        <ant antfile="${wtp.builder.home}/scripts/build/label.xml"/>
        <property file="${buildDirectory}/label.properties"/>

         <!-- buildLabel is defined in label.properties -->
        <mkdir dir="${buildDirectory}/${buildLabel}"/>

        <property
            name="publish.xml"
            value="${component.dir}/publish.xml"/>
        <property
            name="indexFileName"
            value="index.php"/>
        <property
            name="result"
            value="${buildDirectory}/${buildLabel}"/>
        <property
            name="indexTemplateFilename"
            value="index.html.template.php"/>

        <condition
            property="isBuildTested"
            value="true">
            <available file="${buildDirectory}/${buildLabel}/testResults/html"/>
        </condition>
        <ant
            antfile="${publish.xml}"
            dir="${component.dir}">
            <property
                name="dropTokenList"
                value="%wtpruntime%,%wtpsdk%,%wtptest%,%wst%,%wst-sdk%,%wst-tests%,%jst-tests%,%wst-perf-tests%,%jst-perf-tests%,%jpt-runtime%,%jpt-sdk%,%jpt-tests%"/>
             <property
                name="webtoolsDownloadURL"
                value="http://www.eclipse.org/downloads/download.php?file=/webtools/committers/drops"/>
            <property
                name="buildBranch"
                value="${buildBranch}"/>
            <property
                name="isBuildTested"
                value="${isBuildTested}"/>
            <property
                name="indexTemplateFilename"
                value="${indexTemplateFilename}"/>
        </ant>

        <!--  Get the build map over for the results to point to. -->
        <copy
            failonerror="false"
            file="${buildDirectory}/directory.txt"
            tofile="${result}/directory.txt"/>

        <!-- Copy info for build identification -->
        <copy
            failonerror="false"
            file="${buildDirectory}/label.properties"
            tofile="${result}/label.properties"/>

        <!-- http access logs -->
        <copy
            todir="${result}/"
            failonerror="false">
            <fileset
                dir="${buildDirectory}"
                includes="*.log"/>
        </copy>

        <!-- ant build log, from control directory -->
        <copy
            todir="${result}/"
            failonerror="false">
            <fileset
                dir="${env.ANT_WORKING}/${projectname}"
                includes="antBuilderOutput.log"/>
        </copy>
               
        <!-- final count files -->
        <countBuildFiles
            sourceDirectory="${buildDirectory}/${buildLabel}"
            filterString=".zip,.tar.gz"
            outputFile="${buildDirectory}/${buildLabel}/files.count"/>
    </target>
</project>