#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2010, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# no longer used. left here as an example. 

APP_NAME=org.eclipse.equinox.p2.publisher.CategoryPublisher

devworkspace=./workspace

JAVA_5_HOME=/shared/webtools/apps/ibm-java2-i386-50
JAVA_6_HOME=/shared/webtools/apps/ibm-java-i386-60
export JAVA_HOME=${JAVA_6_HOME}
devJRE=$JAVA_HOME/jre/bin/java

ibmDevArgs="-Xms128M -Xmx256M -Dosgi.ws=gtk -Dosgi.os=linux -Dosgi.arch=x86" 

DROPDIR="/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.2.0/R-3.2.0-20100615235519"

APP_ARGS="\
-metadataRepository file:/${DROPDIR}/repositoryb/ \
-artifactRepository file:/${DROPDIR}/repositoryb/ \
-categoryDefinition file:/${PWD}/wtp320category.xml \
-categoryQualifier \
-compress" 

# remember, the '&' should NOT be unescaped here ... the p2 api (or underlying xml) will escape it. 
devArgs="$ibmDevArgs"

echo "dev:          " $0
echo
echo "devworkspace: " $devworkspace
echo
echo "devJRE:       " $devJRE
echo
echo "devArgs:      " $devArgs
echo
echo "APP_NAME:     " $APP_NAME
echo "APP_ARGS:     " ${APP_ARGS}

$devJRE -version


echo

ECLIPSE_INSTALL=/shared/webtools/apps/eclipse361/eclipse

$ECLIPSE_INSTALL/eclipse  -debug -nosplash -consolelog -console -data $devworkspace --launcher.suppressErrors -application ${APP_NAME} ${APP_ARGS} -vm $devJRE -vmargs $devArgs

