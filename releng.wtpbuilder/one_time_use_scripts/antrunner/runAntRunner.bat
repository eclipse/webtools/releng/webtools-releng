@rem ***************************************************************************
@rem Copyright (c) 2010, 2019 IBM Corporation and others.
@rem This program and the accompanying materials
@rem are made available under the terms of the Eclipse Public License 2.0
@rem which accompanies this distribution, and is available at
@rem https://www.eclipse.org/legal/epl-2.0/
@rem
@rem SPDX-License-Identifier: EPL-2.0
@rem
@rem Contributors:
@rem     IBM Corporation - initial API and implementation
@rem ***************************************************************************
@echo off
setlocal

rem specify devworkspace and JRE to use to runEclipse

set devworkspace=%1
if %devworkspace%. == . set devworkspace=./workspace

set locationname=%2

rem IBM's 1.6 64
rem set JAVA_HOME=\jdks\ibm-java-sdk-60-win-x86_64
rem set JAVA_HOME=\jdks\jdk1.6.0_17
rem IBM's 1.6 32
rem set JAVA_HOME=\jdks\ibm-java-sdk-6.0.6.0-win-i386
rem set JAVA_HOME=\jdks\ibm-java-win-506b
    set JAVA_HOME=\jdks\ibm-java-sdk-607-win-i386-ifix11
set devJRE=%JAVA_HOME%\jre\bin\java.exe

set ibmDevArgs=-Xms512M -Xmx1500M -Dosgi.arch=x86 -Dosgi.win=win32 

rem for IBM's 64 bit, I got odd error that unpack200 could not run. Did not investigate, much, but 
rem found specifying this sun one was good enough that 'install software' would then work.
rem -Dorg.eclipse.update.jarprocessor.pack200=\jdks\jdk1.6.0_17\jre\bin

set devArgs=%ibmDevArgs% 

echo dev:          %0
echo.
echo devworkspace: %devworkspace%
echo.
echo devJRE:       %devJRE%
%devJRE% -version
echo.

set ECLIPSE_INSTALL=D:\builds\M5ishPlus\eclipse

%ECLIPSE_INSTALL%\eclipse  -showlocation %locationname% -nosplash -debug -consolelog -console -data %devworkspace% -application org.eclipse.ant.core.antRunner -vm %devJRE% -vmargs %devArgs%

endlocal
rem pause
