#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2010, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
# specify devworkspace and JRE to use to runEclipse


devworkspace=./workspace



JAVA_HOME=/shared/webtools/apps/ibm-java-ppc-606
devJRE=$JAVA_HOME/jre/bin/java

ibmDevArgs="-Xms128M -Xmx256M -Dosgi.ws=gtk -Dosgi.os=linux -Dosgi.arch=ppc"

devArgs=$ibmDevArgs

echo dev:          $0
echo
echo devworkspace: $devworkspace
echo
echo devJRE:       $devJRE
$devJRE -version
echo

ECLIPSE_INSTALL=/shared/webtools/apps/eclipse36M6

$ECLIPSE_INSTALL/eclipse  -nosplash -debug -consolelog -console -data $devworkspace -application org.eclipse.ant.core.antRunner -vm $devJRE -vmargs $devArgs
