#*******************************************************************************
# Copyright (c) 2008, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
mv /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/wtp-jpt-sdk-R-3.0-20080616152118.zip /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/wtp-jpt-sdk-R-3.0-20080616152118ORIG.zip
mv /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/wtp-sdk-noop-R-3.0-20080616152118.zip /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/wtp-sdk-noop-R-3.0-20080616152118ORIG.zip
mv /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/wtp-sdk-R-3.0-20080616152118.zip /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/wtp-sdk-R-3.0-20080616152118ORIG.zip

mv /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-jpt-sdk-R-3.0-20080616152118.zip.md5 /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-jpt-sdk-R-3.0-20080616152118ORIG.zip.md5
mv /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-sdk-noop-R-3.0-20080616152118.zip.md5 /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-sdk-noop-R-3.0-20080616152118ORIG.zip.md5
mv /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-sdk-R-3.0-20080616152118.zip.md5 /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-sdk-R-3.0-20080616152118ORIG.zip.md5

mv /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-jpt-sdk-R-3.0-20080616152118.zip.md5antformat /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-jpt-sdk-R-3.0-20080616152118ORIG.zip.md5antformat
mv /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-sdk-noop-R-3.0-20080616152118.zip.md5antformat /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-sdk-noop-R-3.0-20080616152118ORIG.zip.md5antformat
mv /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-sdk-R-3.0-20080616152118.zip.md5antformat /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/wtp-sdk-R-3.0-20080616152118ORIG.zip.md5antformat

fromString="20080616152118"
toString="20080616152118ORIG"
replaceCommand="s!${fromString}!${toString}!g"

perl -w -pi -e ${replaceCommand} /shared/webtools/committers/wtp-R3.0-R/20080616152118/R-3.0-20080616152118/checksum/*.md5
