#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2011, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
mkdir -p ./new
mkdir -p ./old
# use your real committer id in scpcmd
scpcmd="scp <real-committer-id>@build.eclipse.org"

jb="/plugins/org.eclipse.jem.workbench_2.0.300.v200910290230.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M2-20100923155521/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/jem/workbench/utility/JemProjec#itUtilities.class is different.

jb="/plugins/org.eclipse.wst.common.project.facet.ui_1.4.200.v201101240054.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M5-20110127064115/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/wst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/wst/common/project/facet/ui/internal/FacetsPropertyPage.class is different.

jb="/plugins/org.eclipse.wst.common.snippets_1.2.100.v201101130442.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M5-20110127064115/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/wst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/wst/common/snippets/internal/util/VisibilityUtil.class is different.

jb="/plugins/org.eclipse.jst.ws.axis.consumption.core_1.0.406.v201004211805.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M2-20100923155521/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/jst/ws/internal/axis/consumption/core/command/Java2WSDLCommand.class is different.

jb="/plugins/org.eclipse.jst.ws.axis.creation.ui_1.0.700.v201010181550.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M3-20101104191817/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/jst/ws/internal/axis/creation/ui/command/CopyDeploymentFileCommand.class is different.

jb="/plugins/org.eclipse.jst.jsf.facesconfig_1.2.1.v20100906.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M2-20100923155521/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/jst/jsf/facesconfig/util/FacesConfigArtifactEdit.class is different.

jb="/plugins/org.eclipse.wst.wsi_1.0.400.v201004220210.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M2-20100923155521/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/wst/wsi/internal/core/analyzer/BasicProfileAnalyzer.class is different.

jb="/plugins/org.eclipse.jst.common.ui_1.0.100.v201101101900.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M5-20110127064115/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/jst/common/ui/internal/assembly/wizard/ClasspathContainerRefLabelProvider.class is different.

jb="/plugins/org.eclipse.jst.ws_1.0.507.v201004211805.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M2-20100923155521/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/jst/ws/internal/common/J2EEUtils.class is different.

jb="/plugins/org.eclipse.wst.wsdl.validation_1.1.501.v201004201506.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M2-20100923155521/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/wst/wsdl/validation/internal/wsdl11/WSDLDocument.class is different.

jb="/plugins/org.eclipse.wst.wsi.ui_1.0.501.v201004201506.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M2-20100923155521/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/wst/wsi/ui/internal/actions/ValidateAction.class is different.

jb="/plugins/org.eclipse.wst.xsd.core_1.1.600.v201011122042.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M4-20101209114749/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/wst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/wst/xsd/core/internal/validation/XSDValidator.class is different.

jb="/plugins/org.eclipse.jst.ws.jaxrs.ui_1.0.400.v201012031836.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M4-20101209114749/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/jst/ws/jaxrs/ui/internal/project/facet/JAXRSFacetInstallPage.class is different.

jb="/plugins/org.eclipse.jst.j2ee.ejb.annotations.xdoclet_1.2.100.v201003112036.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M2-20100923155521/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/jst/j2ee/ejb/annotations/internal/xdoclet/XDocletBuilder.class is different.

jb="/plugins/org.eclipse.jst.ws.jaxws.ui_1.0.100.v201009051717.jar"
fo=":/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R3.3.0/S-3.3.0M2-20100923155521/repository"
fn=":/shared/webtools/projects/wtp-R3.3.0-I/workdir/I-3.3.0-20110324075937/buildrepository/jst-sdk"
$scpcmd$fo$jb ./old/
$scpcmd$fo$jb ./new/
#The class org/eclipse/jst/ws/internal/jaxws/ui/annotations/correction/APTCompileProblemMarkerResolutionGenerator.class is different.


