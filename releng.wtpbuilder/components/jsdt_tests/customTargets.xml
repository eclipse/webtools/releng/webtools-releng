<!--
    Copyright (c) 2009, 2019 IBM Corporation and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
        IBM Corporation - initial API and implementation
 -->
<project
    name="PDECustomTargets"
    default="noDefault"
    basedir=".">
    <!--
        = = = standard properties pattern = = = Note to be
        cross-platform, "environment variables" are only appropriate for
        some variables, e.g. ones we set, since properties are case
        sensitive, even if the environment variables on your operating
        system are not, e.g. it will be ${env.Path} not ${env.PATH} on
        Windows
    -->
    <property environment="env"/>
    <!--
        Let users override standard properties, if desired. If
        directory, file, or some properties do not exist, then standard
        properties will be used.
    -->
    <property file="${env.LOCAL_BUILD_PROPERTIES_DIR}/${ant.project.name}.properties"/>

    <!-- let this project specify properties, if not already set -->
    <property file="build.properties"/>
    <fail
        message="dependency.properties must be specified by distribution script"
        unless="dependency.properties"/>
    <fail
        unless="buildDirectory"
        message="buildDirectory must be set"/>

    <property
        name="archiveName"
        value="${component}-buildrepo-${buildLabel}.zip"/>
    <property
        name="p2.metadata.repo.name"
        value="WTP Build repository: ${buildLabel}/${component}"/>
    <property
        name="p2.artifact.repo.name"
        value="WTP Build repository: ${buildLabel}/${component}"/>
    <property
        name="p2buildrepoDirectory"
        value="${buildDirectory}/${buildLabel}/buildrepository/${component}/"/>
    <property
        name="p2.build.repo"
        value="file://${p2buildrepoDirectory}/"/>


    <property
        name="p2.category.definition.file"
        value="${buildDirectory}/maps/${env.RELENG}/components/${component}/category.xml"/>

    <property
        name="p2.category.definition"
        value="file:/${p2.category.definition.file}"/>

    <!--
        Run a given ${target} on all elements being built Add on <ant>
        task for each top level element being built.
    -->
    <available
        property="allElementsFile"
        file="${builder}/allElements.xml"
        value="${builder}/allElements.xml"/>
    <property
        name="allElementsFile"
        location="${eclipse.pdebuild.templates}/headless-build/allElements.xml"/>
    <import file="${allElementsFile}"/>
    <target
        name="allElements"
        depends="init">

        <antcall target="allElementsDelegator"/>
    </target>

    <!--
        Check out map files from repository
    -->
    <target name="getMapFiles">

        <!-- for now, get all, at once. May componetize later. -->
        <ant
            antfile="${wtp.builder.home}/scripts/build/maptasks.xml"
            target="fetchAllMaps"/>
    </target>


    <!--
        Steps to do before setup
    -->
    <target name="preSetup">
    </target>
    <!--
        Steps to do after setup but before starting the build proper
    -->
    <target
        name="postSetup"
        depends="init">
        <dirname
            file="${ant.file}"
            property="component.dir"/>

        <!-- Get the dependencies -->
        <antcall target="getDependencies"/>

        <antcall target="updateCategoriesFiles"/>
    </target>
    <!--
        =====================================================================
    -->
    <!-- Get the dependencies based on releng maps -->
    <!--
        =====================================================================
    -->
    <target name="getDependencies">
        <ant
            antfile="${wtp.builder.home}/scripts/dependency/dependency.xml"
            target="get">
            <property
                name="base.install.dir"
                value="${buildRoot}"/>
            <property
                name="tobeinstalled.properties"
                value="${buildDirectory}/maps/${env.RELENG}/components/${component}/tobeInstalled.properties"/>

        </ant>
    </target>

    <!--
        Steps to do before fetching the build elements
    -->
    <target name="preFetch">
    </target>
    <target name="postFetch">
        <ant
            antfile="${wtp.builder.home}/scripts/build/maptasks.xml"
            target="unrewriteMaps"/>

    </target>
    <!--
        Steps to do before the repositories are being processed
    -->
    <target name="preProcessRepos">
    </target>
    <!--
        Steps to do after the repositories have been processed
    -->
    <target name="postProcessRepos">
    </target>

    <!--
        Steps to do before generating the build scripts.
    -->
    <target name="preGenerate">
    </target>

    <!--
        Steps to do after generating the build scripts.
    -->
    <target name="postGenerate">

        <ant antfile="${wtp.builder.home}/scripts/build/customizeAccessRules.xml"/>
    </target>
    <!--
        Steps to do before running the build.xmls for the elements being
        built.
    -->
    <target name="preProcess">
        <replace
            dir="${buildDirectory}/plugins"
            value="${timestamp}"
            token="@build@">
            <include name="**/about.mappings"/>
        </replace>
    </target>
    <!--
        Steps to do after running the build.xmls for the elements being
        built.
    -->
    <target name="postProcess">
        <condition property="logsAvailable">
            <istrue value="${javacVerbose}"/>
        </condition>
        <antcall target="gatherLogs"/>
    </target>

    <!-- Steps to do before running assemble. -->
    <target name="preAssemble">
    </target>

    <!--
        Steps to do after running assemble.
    -->
    <target name="postAssemble">

        <ant
            antfile="${wtp.builder.home}/scripts/build/buildutilitiesp2.xml"
            target="conditionAndSignJars"/>
        <ant
            antfile="${wtp.builder.home}/scripts/build/buildutilitiesp2.xml"
            target="finishPackages"/>

    </target>

    <!--
        Steps to do after the build is done.
    -->
    <target name="prePackage">
    </target>

    <!-- Steps to do after running package. -->
    <target name="postPackage">
    </target>

    <!--
        Steps to do after the build is done.
    -->
    <target name="postBuild">
    </target>

    <!--
        Steps to do to test the build results
    -->
    <target name="test">
    </target>

    <!--
        Steps to do to publish the build results
    -->
    <target name="publish">
    </target>

    <!--
        Helper targets
    -->
    <target
        name="gatherLogs"
        if="logsAvailable"
        depends="init">
        <mkdir dir="${buildDirectory}/${buildLabel}/${compilelogsDir}"/>

        <antcall target="allElements">
            <param
                name="target"
                value="gatherLogs"/>
            <param
                name="logExtension"
                value="${logExtension}"/>
        </antcall>
        <unzip
            dest="${buildDirectory}/${buildLabel}/${compilelogsDir}"
            overwrite="true">
            <fileset dir="${buildDirectory}/features/${topLevelElementId}">
                <include name="**/*.log.zip"/>
            </fileset>
        </unzip>
        <!--
            workaround: see
            https://bugs.eclipse.org/bugs/show_bug.cgi?id=156043 and
            https://bugs.eclipse.org/bugs/show_bug.cgi?id=172361 and
            https://bugs.eclipse.org/bugs/show_bug.cgi?id=294070
        -->

        <copy
            todir="${buildDirectory}/${buildLabel}/${compilelogsDir}/plugins"
            overwrite="true">
            <!--
                we want all @dot.xml files, but assumes a certain
                structure, to include directory for name of plugin
            -->
            <fileset dir="${buildDirectory}/features/">
                <include name="**/*tests*feature*/feature.temp.folder/"/>
            </fileset>
            <compositemapper>
                <mapper
                    type="regexp"
                    from="(.*)/plugins/(.*)/@dot.xml"
                    to="/\2/@dot.xml"/>
                <mapper
                    type="regexp"
                    from="(.*)/plugins/(.*)/(.*)bin.xml"
                    to="/\2/\3bin.xml"/>
            </compositemapper>
        </copy>

    </target>
    <target
        name="clean"
        unless="noclean">
        <antcall target="allElements">
            <param
                name="target"
                value="cleanElement"/>
        </antcall>
    </target>

    <!--
   Note: buildId is typically of the form "3.3.0"
   may want to use buildLabel, which is of form I-3.3.0-20100912032913
   -->

    <target
        name="updateCategoriesFiles"
        depends="checkCategoriesFiles"
        if="categoryFileExists">
        <replace
            file="${p2.category.definition.file}"
            value="${buildId}"
            token="@buildversionid@">
        </replace>


    </target>
    <target name="checkCategoriesFiles">
        <available
            file="${p2.category.definition.file}"
            property="categoryFileExists"/>
    </target>     


    <!--
        Default target
    -->
    <target name="noDefault">
        <echo
            level="error"
            message="You must specify a target when invoking this file"/>
    </target>


    <target
        name="init"
        unless="customTargetsInitialized">

        <echo message="buildDirectory: ${buildDirectory}"/>
        <echo message="p2.build.repo: ${p2.build.repo}"/>
        <echo message="p2.category.definition: ${p2.category.definition}"/>
        <echo
            level="info"
            message="target: ${target} "/>
        <echo
            level="info"
            message="basedir: ${basedir}"/>
        <echo
            level="info"
            message="component: ${component}"/>
        <echo
            level="info"
            message="buildDirectory: ${buildDirectory}"/>
        <echo
            level="info"
            message="baseLocation: ${baseLocation}"/>
        <echo
            level="info"
            message="generic target: ${genericTargets}"/>
        <echo
            level="info"
            message="topLevelElementId: ${topLevelElementId}"/>
        <echo message="compilerArg: ${compilerArg}"/>
        <echo message="individualSourceBundles: ${individualSourceBundles}"/>
        <echo
            level="debug"
            message="ant.file: ${ant.file}"/>
        <echo
            level="debug"
            message="component.dir: ${component.dir}"/>
        <echo
            level="debug"
            message="logExtension: ${logExtension}"/>
        <property
            name="customTargetsInitialized"
            value="true"/>


    </target>

</project>