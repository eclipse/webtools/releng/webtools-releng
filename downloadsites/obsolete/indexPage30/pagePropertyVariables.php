<?php

$pageTitle="Eclipse Web Tools Platform (WTP) Downloads";
$indexTop="<font class=indextop>
 Web Tools Platform<br />
downloads</font><br />
<font class=indexsub>Latest downloads from the Web Tools Platform project</font>";

$pageExplanation="
    <p>This is the starting page for where you can find the latest <a
      href=\"http://wiki.eclipse.org/index.php/WTP_Build_Types\">declared build</a> produced by the <a
      href=\"http://www.eclipse.org/webtools/main.php\">Eclipse Web Tools
    Platform (WTP) Project</a>.</p>
    <p>Please note that each declared-build page details the pre-reqs for
    that particular build. The WTP 1.0.x builds go with Eclipse 3.1 based
    pre-reqs, and the WTP 1.5.x builds go with Eclipse 3.2 based pre-reqs.</p>

    <p>As an alternative to downloading zips from the build pages, our
    released builds can be <a
      href=\"http://download.eclipse.org/webtools/updates/\">installed via
    Update Manager</a>, from an existing installation of Eclipse.</p>
";

$mainTableHeader="Latest Downloads";

$pageFooterEnd="<p><a href=\"http://build.eclipse.org/webtools/committers/\"
              target=\"_top\">Continuous builds</a> are also available which are for
            committers and early testers.</p>"; 

$subsectionHeading="Recent History";

?>