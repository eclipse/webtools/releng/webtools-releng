<html>
<head>
<meta
    http-equiv="Content-Type"
    content="text/html; charset=iso-8859-1">
<title>WTP Downloads</title>
<?php

$buildBranch="R3.0";
$build="R-3.0-20080616152118";
$type="Release";
$builddate="June 25, 2008";

// /eclipse/downloads/drops/R-3.4-200806172000/eclipse-SDK-3.4-linux-gtk.tar.gz
$eclipseMirrorPrefixuri="/eclipse/downloads/drops/R-3.4-200806172000";
$eclipseFSpathPrefix="/home/data/httpd/download.eclipse.org";

$eclipsefilelinux="eclipse-SDK-3.4-linux-gtk.tar.gz";
$eclipsefilewindows="eclipse-SDK-3.4-win32.zip";
$eclipsefilemacosx="eclipse-SDK-3.4-macosx-carbon.tar.gz";

$eclipseURL="http://download.eclipse.org/eclipse/downloads/drops/R-3.4-200806172000/eclipse-SDK-3.4-linux-gtk.tar.gz";
$eclipseFile="eclipse-SDK-3.4-linux-gtk.tar.gz";
$eclipseBuildURL="http://download.eclipse.org/eclipse/downloads/drops/R-3.4-200806172000";
$eclipseBuildHome="http://download.eclipse.org/eclipse/downloads/";
$testURL="http://download.eclipse.org/eclipse/downloads/drops/R-3.4-200806172000/eclipse-test-framework-3.4.zip";
$testFile="eclipse-test-framework-3.4.zip";

$wstURL="http://download.eclipse.org/webtools/downloads/drops/R3.0/R-3.0-20080616152118/wtp-wst-R-3.0-20080616152118.zip";
$wstFile="wtp-wst-R-3.0-20080616152118.zip";
$wstMirrorPrefixuri="webtools/downloads/drops/R3.0/R-3.0-20080616152118";
$wstBuildHome="http://download.eclipse.org/webtools/downloads/";

// /tools/gef/downloads/drops/3.4.0/R200806091334/GEF-ALL-3.4.0.zip
$gefURL="http://download.eclipse.org/tools/gef/downloads/drops/3.4.0/R200806091334/GEF-SDK-3.4.0.zip";
$gefFile="GEF-SDK-3.4.0.zip";
$gefMirrorPrefixuri="/tools/gef/downloads/drops/3.4.0/R200806091334";
$gefBuildHome="http://download.eclipse.org/tools/gef/downloads/";

$emfURL="http://download.eclipse.org/modeling/emf/emf/downloads/drops/2.4.0/R200806091234/emf-runtime-2.4.0.zip";
$emfFile="emf-runtime-2.4.0.zip";
$emfMirrorPrefixuri="/modeling/emf/emf/downloads/drops/2.4.0/R200806091234";
$emfBuildHome="http://www.eclipse.org/modeling/emf/downloads/";
$emfName="EMF v2.4 Runtime";
$emfDescription="Minimum executable code.";

$emfsourceURL="http://download.eclipse.org/modeling/emf/emf/downloads/drops/2.4.0/R200806091234/emf-sourcedoc-2.4.0.zip";
$emfsourceFile="emf-sourcedoc-2.4.0.zip";
$emfsourceMirrorPrefixuri="/modeling/emf/emf/downloads/drops/2.4.0/R200806091234";
$emfsourceBuildHome="http://www.eclipse.org/modeling/emf/downloads/";
$emfsourceName="EMF v2.4 Source and Docs";
$emfsourceDescription="Additional download with Source and Documentation.";

$emfxsdURL="http://download.eclipse.org/modeling/emf/emf/downloads/drops/2.4.0/R200806091234/xsd-runtime-2.4.0.zip";
$emfxsdFile="xsd-runtime-2.4.0.zip";
$emfxsdMirrorPrefixuri="/modeling/emf/emf/downloads/drops/2.4.0/R200806091234";
$emfxsdBuildHome="http://www.eclipse.org/modeling/emf/downloads/";
$emfxsdName="EMF XSD v2.4 Runtime";
$emfxsdDescription="Minimum executable code.";

$emfxsdsourceURL="http://download.eclipse.org/modeling/emf/emf/downloads/drops/2.4.0/R200806091234/xsd-sourcedoc-2.4.0.zip";
$emfxsdsourceFile="xsd-sourcedoc-2.4.0.zip";
$emfxsdsourceMirrorPrefixuri="/modeling/emf/emf/downloads/drops/2.4.0/R200806091234";
$emfxsdsourceBuildHome="http://www.eclipse.org/modeling/emf/downloads/";
$emfxsdsourceName="EMF XSD v2.4 Source and Docs";
$emfxsdsourceDescription="Additional download with Source and Documentation.";

// /datatools/downloads/1.6/dtp-sdk_1.6.0_200806181028.zip
$dtpURL="http://download.eclipse.org/datatools/downloads/1.6/dtp-sdk_1.6.0.zip";
$dtpFile="dtp-sdk_1.6.0.zip";
$dtpMirrorPrefixuri="/datatools/downloads/1.6";
$dtpBuildHome="http://www.eclipse.org/datatools/downloads.php";


$eclipserelengFile="org.eclipse.releng.tools-3.4.zip";
$eclipserelengURL="http://download.eclipse.org/eclipse/downloads/drops/R-3.4-200806172000/org.eclipse.releng.tools-3.4.zip";
$orbitthirdpartyzipFile="orbit-R20080611105805.zip";
$orbitthirdpartyzipURL="http://download.eclipse.org/tools/orbit/downloads/drops/R20080611105805/orbit-R20080611105805.zip";
$orbitthirdpartyzipBuildHome="http://download.eclipse.org/tools/orbit/";
$orbitthirdpartyzipMirrorPrefixuri="/tools/orbit/downloads/drops/R20080611105805";




include("miscUtil.php");



//ini_set("display_errors", "true");
//error_reporting (E_ALL);


$debugScript = false;
$debugFunctions = false;

$defaultMirrorScript="";
$defaultWTPMirrorPrefix="./";

$eclipseMirrorScript="http://www.eclipse.org/downloads/download.php?file=";

// TODO: improve so this hard coding isn't required.
// This depends on the declare script changing webtools/downloads to webtools/downloads
// And, the logic is such that if it is not mirrored, this URI is not used at all, just
// a relative reference only
$eclipseWTPMirrorPrefix="/webtools/downloads/drops/$buildBranch/$build/";


$mirrorScript=$defaultMirrorScript;
$downloadprefix=$defaultWTPMirrorPrefix;


$keytestMirrorString=$eclipseMirrorScript . "$eclipseWTPMirrorPrefix/wtp-sdk-$build"."a".".zip";
if (isMirrored($keytestMirrorString) ) {
    $mirrorScript=$eclipseMirrorScript;
    $downloadprefix="${mirrorScript}${eclipseWTPMirrorPrefix}";
}

if ($debugScript)  {
    echo "inferred platform: " . getPlatform();
}


// our summary results handling requires php 5 (for simple xml file loading)
// so, if not php 5, just don't display any summary results
// This was found to be required, since some mirror our whole site (e.g. IBM)
// and not all mirrors use PHP 5
$displayTestSummary=false;
if (phpversion() >= 5) {

    $displayTestSummary=true;
    // expecting grandTotalErrors and grandTotalTests
    $filename = "unitTestsSummary.xml";
    if (file_exists($filename)) {
        $prefix = "unitTests_";
        $unitTestsSummary = simplexml_load_file($filename);
        foreach ($unitTestsSummary->summaryItem as $summaryItem) {
            $name = $summaryItem->name;
            $value = $summaryItem->value;
            $code= "\$" . $prefix . $name . " = " . $value . ";";
            //echo "<br />code: " . $code;
            eval($code);
        }
    }

    $filename = "compilelogsSummary.xml";
    $prefix = "code_";
    $compileSummary = simplexml_load_file($filename);
    foreach ($compileSummary->summaryItem as $summaryItem) {
        $name = $summaryItem->name;
        $value = $summaryItem->value;
        $code= "\$" . $prefix . $name . " = " . $value . ";";
        //echo "<br />code: " . $code;
        eval($code);
    }

    $filename = "testcompilelogsSummary.xml";
    $prefix = "test_";
    if (file_exists($filename)) {
        $compileSummary = simplexml_load_file($filename);
        foreach ($compileSummary->summaryItem as $summaryItem) {
            $name = $summaryItem->name;
            $value = $summaryItem->value;
            $code= "\$" . $prefix . $name . " = " . $value . ";";
            //echo "<br />code: " . $code;
            eval($code);
        }
    }
}


?>
</head>
<body>
<?php

// tiny banner to remind when looking at "local" machine results
$serverName = $_SERVER["SERVER_NAME"];

if (!stristr($serverName, "eclipse.org") && !stristr($serverName,"you.are.at.eclipsecon.org")) {
    echo '<center>
          <p>
          Reminder: this is <font color="#FF0000">' .
    $serverName .
    '</font>
          See also
          <a href="http://download.eclipse.org/webtools/downloads" target="_top">the live public Eclipse site</a>.
          </p>
          <hr />
          </center>';

}
?>
<table
    BORDER=0
    CELLSPACING=2
    CELLPADDING=2
    WIDTH="100%">
    <tr>
        <td ALIGN=left><font
            face="'Bitstream Vera',Helvetica,Arial"
            size="+2"><b><?php echo "$type";?> Build: <?php echo "$build";?></b></font></td>
    
    
    <tr>
        <td><font size="-1"><?php echo "$builddate";?></font></td>
    </tr>
    <tr>
        <td>
        <p>The Eclipse Web Tools Platform Project provides tools for Web Development, and is a platform for adopters
        making add-on tools for Web Development.</p>
        </td>
    </tr>
</table>
<!-- ***********  End Users **************  -->
<table
    border=0
    cellspacing=2
    cellpadding=2
    width="100%">
    <tr>
        <td
            align="left"
            valign="top"
            bgcolor="#0080C0"><font
            face="'Bitstream Vera',Helvetica,Arial"
            color="#FFFFFF">All-in-one Package and Update Sites</font></td>
    </tr>
    <tr>
        <td>
        <p>We recommend, for nearly all users, the all-in-one <a href="http://www.eclipse.org/downloads/packages/">Eclipse IDE
        for Java EE Developers</a> (only 165 megabytes!). Just unzip it and go (well, you do also need a <a
            href="http://www.eclipse.org/downloads/moreinfo/jre.php">suitable JDK </a> at the 1.5 or 1.6 level).</p>
        <p>If you would prefer to pick and choose exactly what you want, from a subset of WTP or from the scores of
        other Eclipse Projects, we recommend the Ganymede update site. That update site is built into the Eclipse
        Platform: from an existing installation of Eclipse 3.4, under "Help", select "Software Updates", and under
        "Available Software" look for the <b>Web and Java EE Development</b> category. Note: you can not start with
        Eclipse 3.3 and upgrade to Eclipse 3.4 or WTP 3.0. You must start with an fresh version of Eclipse at the 3.4
        level.</p>
        <p>After you have installed WTP, you can occasionally visit our <a href="http://download.eclipse.org/webtools/updates/">Web Tools specific update site</a> for other 
        new, interesting, and fun items to install, such as our new (incubating) <a href="http://wiki.eclipse.org/XSLT_Project">XSL Tooling</a>.   
        <!-- temporary spacing method -->
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    
    </tr>
</table>
<!-- ***********  Required Prerequisites **************  -->
<table
    border=0
    cellspacing=2
    cellpadding=2
    width="100%">
    <tr>
        <td
            align="left"
            valign="top"
            bgcolor="#0080C0"><font
            face="'Bitstream Vera',Helvetica,Arial"
            color="#FFFFFF">Prerequisites for Custom Installs and Handy Extras</font></td>
    </tr>
    <tr>
        <td>
        <p>All the following zip files are for adopters, committers, and testers who need to construct specific
        configurations. If you are interested in building web applications, you do not need these zips. You only need
        the "all in one" <a href="http://www.eclipse.org/downloads/packages/">Eclipse IDE for Java EE Developers</a>.</p>
        <p>These are the prerequisites to install and run the Web Tools Platform. Also listed are some frequently needed
        links for committer-required packages when creating new development environments, or targets to run against.</p>
        <p>Note that WTP requires Java 5 or higher (and, for some things, actually requires a JDK rather than only a
        JRE) even though many other <a href="http://www.eclipse.org/downloads/">Eclipse Projects</a> can run with <a
            href="http://www.eclipse.org/downloads/moreinfo/jre.php">other JRE levels</a>.</p>
        </td>
    </tr>
    <tr>
        <td>
        <table
            border=0
            cellspacing=1
            cellpadding=1
            width="90%"
            align="center">
            <tr valign="top">
                <td width="10%"></td>
                <td width="55%">Eclipse Platform (Platform, JDT)</td>
                <?php
                //customize page depending on user's browser/platform, if we can detect it
                $usersPlatform = getPlatform();
                // assume windows by default, since likely most frequent, even for cases where
                // platform is "unknown". I've noticed Opera reports 'unknown' :(
                $recommendedFile=$eclipsefilewindows;
                if (strcmp($usersPlatform,"linux")== 0) {
                    $recommendedFile=$eclipsefilelinux;
                } else if (strcmp($usersPlatform,"mac") == 0) {
                    $recommendedFile=$eclipsefilemacosx;
                }
                ?>
                <td
                    align="right"
                    width="35%"><?php
                    echo getPrereqReferenceOrName($eclipseMirrorScript, $eclipseMirrorPrefixuri, $eclipseURL, $recommendedFile, $eclipseFSpathPrefix);
                    echo " or <a href=\"" . $eclipseBuildURL . "\">appropriate platform</a>";
                    echo " or <a href=\"" . $eclipseBuildHome . "\">equivalent</a></td>";
                    ?>
            
            </tr>
            <tr valign="top">
                <td width="10%"></td>
                <td><?php echo "$emfName $emfDescription" ?></td>
                <td align="right"><?php
                echo getPrereqReferenceOrName($eclipseMirrorScript, $emfMirrorPrefixuri, $emfURL, $emfFile, $eclipseFSpathPrefix);
                echo " or <a href=\"" . $emfBuildHome . "\">equivalent</a></td>";
                ?>
            
            </tr>
            <tr valign="top">
                <td width="10%"></td>
                <td><?php echo "$emfxsdName $emfxsdDescription" ?></td>
                <td align="right"><?php
                echo getPrereqReferenceOrName($eclipseMirrorScript, $emfxsdMirrorPrefixuri, $emfxsdURL, $emfxsdFile, $eclipseFSpathPrefix);
                echo " or <a href=\"" . $emfxsdBuildHome . "\">equivalent</a></td>";
                ?>
            
            </tr>
            <tr valign="top">
                <td width="10%"></td>
                <td>Graphical Editing Framework (GEF)</td>
                <td align="right"><?php
                echo getPrereqReferenceOrName($eclipseMirrorScript, $gefMirrorPrefixuri, $gefURL, $gefFile, $eclipseFSpathPrefix);
                echo " or <a href=\"" . $gefBuildHome . "\">equivalent</a></td>";
                ?>
            
            </tr>
            <tr valign="top">
                <td width="10%"></td>
                <td>Data Tools Platform (DTP) (required only for EJB or JPT development)</td>
                <td align="right"><?php
                echo getPrereqReferenceOrName($eclipseMirrorScript, $dtpMirrorPrefixuri, $dtpURL, $dtpFile, $eclipseFSpathPrefix);
                echo " or <a href=\"" . $dtpBuildHome . "\">equivalent</a></td>";
                ?>
            
            </tr>
            <tr valign="middle">
                <td colspan="3">
                <hr />
                </td>
            </tr>
            <tr valign="top">
                <td width="10%"></td>
                <td><?php echo "$emfsourceName $emfsourceDescription" ?></td>
                <td align="right"><?php
                echo getPrereqReferenceOrName($eclipseMirrorScript, $emfsourceMirrorPrefixuri, $emfsourceURL, $emfsourceFile, $eclipseFSpathPrefix);
                echo " or <a href=\"" . $emfsourceBuildHome . "\">equivalent</a></td>";
                ?>
            
            </tr>
            <tr valign="top">
                <td width="10%"></td>
                <td><?php echo "$emfxsdsourceName $emfxsdsourceDescription" ?></td>
                <td align="right"><?php
                echo getPrereqReferenceOrName($eclipseMirrorScript, $emfxsdsourceMirrorPrefixuri, $emfxsdsourceURL, $emfxsdsourceFile, $eclipseFSpathPrefix);
                echo " or <a href=\"" . $emfxsdsourceBuildHome . "\">equivalent</a></td>";
                ?>
            
            </tr>
            <tr valign="top">
                <td width="10%"></td>
                <td>Eclipse Test Framework (required only for Automated JUnit tests)</td>
                <td align="right"><?php
                echo getPrereqReferenceOrName($eclipseMirrorScript, $eclipseMirrorPrefixuri, $testURL, $testFile, $eclipseFSpathPrefix);
                echo " or <a href=\"" . $eclipseBuildHome . "\">equivalent</a></td>";
                ?>
            
            </tr>
            <tr valign="top">
                <td width="10%"></td>
                <td>Eclipse releng tool (required only for committers to more easily "release" code to a build)</td>
                <td align="right"><?php
                echo getPrereqReferenceOrName($eclipseMirrorScript, $eclipseMirrorPrefixuri, $eclipserelengURL, $eclipserelengFile, $eclipseFSpathPrefix);
                echo " or <a href=\"" . $eclipseBuildHome . "\">equivalent</a></td>";
                ?>
            
            </tr>
            <tr valign="top">
                <td width="10%"></td>
                <td>Third Party code from Orbit. Not required and is currently much more than needed for WTP, but some
                committers like using to create a PDE target.</td>
                <td align="right"><?php
                echo getPrereqReferenceOrName($eclipseMirrorScript, $orbitthirdpartyzipMirrorPrefixuri, $orbitthirdpartyzipURL, $orbitthirdpartyzipFile,$eclipseFSpathPrefix);
                echo " or <a href=\"" . $orbitthirdpartyzipBuildHome . "\">equivalent</a></td>";
                ?>
            
            </tr>
        </table>
        </td>
    </tr>
</table>
<!-- ***********  WTP **************  -->
<table
    border=0
    cellspacing=2
    cellpadding=2
    width="100%">
    <tr>
        <td
            align=left
            valign=top
            colspan="5"
            bgcolor="#0080C0"><font
            face="'Bitstream Vera',Helvetica,Arial"
            color="#FFFFFF">Java EE Developer Tools</font></td>
    </tr>
    <tr>
        <td
            align="left"
            valign="top"
            colspan="5">
        <p>The wtp zip files includes all the features and plugins from the WTP Project and includes all the tools for Java
        EE Developers.</p>
        </td>
    </tr>
    <tr>
        <td>
        <table
            border=0
            cellspacing=2
            cellpadding=2
            width="90%"
            align="center">
            <tr>
                <td
                    align="left"
                    valign="top"
                    width="10%"><b>Web App Developers:</b></td>
                <td
                    align="left"
                    valign="top"
                    width="55%">
                <p>This non-SDK package is to construct products for end-users. It includes the runnable code and
                end-user documentation for those using the tools to develop web applications.</p>
                </td>
                <?php

                $zipfilename="wtp-$build"."a";

                $filename=$zipfilename.".zip";
                $zipfilesize=fileSizeForDisplay($filename);
                $fileShortDescription="wtp";
                displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);
                ?>
            </tr>
            <tr>
                <td
                    align="left"
                    valign="top"
                    width="10%"><b>Tool Developers:</b></td>
                <td
                    align="left"
                    valign="top">
                <p>The SDK package includes source code and developer documentation for those using WTP as a platform to
                build more tools, as well as everything that is in the non-SDK version.</p>
                <?php

                $zipfilename="wtp-sdk-$build"."a";

                $filename=$zipfilename.".zip";
                $zipfilesize=fileSizeForDisplay($filename);
                $fileShortDescription="wtp-sdk";
                displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);

                ?>
            
            </tr>
            <tr>
                <td
                    align="left"
                    valign="top"
                    width="10%"></td>
                <td
                    align="left"
                    valign="top">
                <p>The Automated Test zip contains the unit tests.</p>
                <?php

                $zipfilename="wtp-Automated-Tests-$build";

                $filename=$zipfilename.".zip";
                $zipfilesize=fileSizeForDisplay($filename);
                $fileShortDescription="wtp-Automated-Tests";
                displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);

                ?>
            
            </tr>
        </table>
        </td>
    
    
    <tr>

</table>
<!-- ***********  WST **************  -->
<table
    border=0
    cellspacing=2
    cellpadding=2
    width="100%">
    <tr>
        <td
            align=left
            valign=top
            colspan="5"
            bgcolor="#0080C0"><font
            face="'Bitstream Vera',Helvetica,Arial"
            color="#FFFFFF"> Web Developer Tools</font></td>
    </tr>
    <tr>
        <td
            align="left"
            valign="top"
            colspan="5">
        <p>These wtp-wst zip files are a subset of the above, but includes enough to do web development (e.g. HTML, CSS,
        Javascript, and XML). This functionality requires only the Eclipse Platform, GEF and portions of EMF (no JDT,
        etc.).</p>
        </td>
    </tr>
    <tr>
        <td>
        <table
            border=0
            cellspacing=2
            cellpadding=2
            width="90%"
            align="center">
            <tr>
                <td
                    align="left"
                    valign="top"
                    width="10%"><b>Web App Developers:</b></td>
                <td
                    align="left"
                    valign="top">
                <p>This non-SDK package is for most users. It includes the runnable code and end-user documentation for
                those using the tools to develop web applications. In particular, non-Java oriented Web Apps (such as
                HTML, XML, CSS, etc).</p>
                </td>
                <?php

                $zipfilename="wtp-wst-$build";

                $filename=$zipfilename.".zip";
                $zipfilesize=fileSizeForDisplay($filename);
                $fileShortDescription="wst";
                displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);

                ?>
            </tr>
            <tr>
            
            
            <tr>
                <td
                    align="left"
                    valign="top"
                    width="10%"><b>Tool Developers:</b></td>
                <td
                    align="left"
                    valign="top">
                <p>The SDK package includes source code and developer documentation for those using WST as a platform to
                build more tools, as well as everything that is in the non-SDK version.</p>
                <?php

                $zipfilename="wtp-wst-sdk-$build";

                $filename=$zipfilename.".zip";
                $zipfilesize=fileSizeForDisplay($filename);
                $fileShortDescription="wtp-wst-sdk";
                displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);
                ?>
            
            </tr>
            <tr>
                <td
                    align="left"
                    valign="top"
                    width="10%"></td>
                <td
                    align="left"
                    valign="top">
                <p>The Automated Test zip contains the unit tests.</p>
                <?php

                $zipfilename="wtp-wst-Automated-Tests-$build";

                $filename=$zipfilename.".zip";
                $zipfilesize=fileSizeForDisplay($filename);
                $fileShortDescription="wtp-wst-Automated-Tests";
                displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);

                ?>
            
            </tr>
        </table>
        </td>
    
    
    <tr>

</table>
<!-- ***********  XML **************  -->
<table
    border=0
    cellspacing=2
    cellpadding=2
    width="100%">
    <tr>
        <td
            align=left
            valign=top
            colspan="5"
            bgcolor="#0080C0"><font
            face="'Bitstream Vera',Helvetica,Arial"
            color="#FFFFFF"> XML Developer Tools</font></td>
    </tr>
    <tr>
        <td
            align="left"
            valign="top"
            colspan="5">
        <p>These wtp-xml zip files are a subset of the above, but includes enough to do xml development. This
        functionality requires only the Eclipse Platform, GEF and portions of EMF (no JDT, etc.).</p>
        </td>
    </tr>
    <tr>
        <td>
        <table
            border=0
            cellspacing=2
            cellpadding=2
            width="90%"
            align="center">
            <tr>
                <td
                    align="left"
                    valign="top"
                    width="10%"><b>XML App Developers:</b></td>
                <td
                    align="left"
                    valign="top">
                <p>This non-SDK package is for most users. It includes the runnable code and end-user documentation for
                those using the tools to develop XML applications.</p>
                </td>
                <?php

                $zipfilename="wtp-xml-$build";

                $filename=$zipfilename.".zip";
                $zipfilesize=fileSizeForDisplay($filename);
                $fileShortDescription="wtp-xml";
                displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);

                ?>
            </tr>
            <tr>
            
            
            <tr>
                <td
                    align="left"
                    valign="top"
                    width="10%"><b>Tool Developers:</b></td>
                <td
                    align="left"
                    valign="top">
                <p>The SDK package includes source code and developer documentation for those using this as a platform
                to build more tools, as well as everything that is in the non-SDK version.</p>
                <?php

                $zipfilename="wtp-xml-sdk-$build";

                $filename=$zipfilename.".zip";
                $zipfilesize=fileSizeForDisplay($filename);
                $fileShortDescription="wtp-xml-sdk";
                displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);
                ?>
            
            </tr>
            <!-- ***********  Javascript **************  -->
            <table
                border=0
                cellspacing=2
                cellpadding=2
                width="100%">
                <tr>
                    <td
                        align=left
                        valign=top
                        colspan="5"
                        bgcolor="#0080C0"><font
                        face="'Bitstream Vera',Helvetica,Arial"
                        color="#FFFFFF"> Javascript Developer Tools</font></td>
                </tr>
                <tr>
                    <td
                        align="left"
                        valign="top"
                        colspan="5">
                    <p>These wtp-jsdt zip files are a subset of the above, but includes enough to do Javascript
                    development. This functionality requires only the Eclipse Platform (no EMF, JDT, etc.).</p>
                    </td>
                </tr>
                <tr>
                    <td>
                    <table
                        border=0
                        cellspacing=2
                        cellpadding=2
                        width="90%"
                        align="center">
                        <tr>
                            <td
                                align="left"
                                valign="top"
                                width="10%"><b>Javascript App Developers:</b></td>
                            <td
                                align="left"
                                valign="top">
                            <p>This non-SDK package is for most users. It includes the runnable code and end-user
                            documentation for those using the tools to develop Javascript applications.</p>
                            </td>
                            <?php

                            $zipfilename="wtp-jsdt-$build";

                            $filename=$zipfilename.".zip";
                            $zipfilesize=fileSizeForDisplay($filename);
                            $fileShortDescription="wtp-jsdt";
                            displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);

                            ?>
                        </tr>
                        <tr>
                        
                        
                        <tr>
                            <td
                                align="left"
                                valign="top"
                                width="10%"><b>Tool Developers:</b></td>
                            <td
                                align="left"
                                valign="top">
                            <p>The SDK package includes source code and developer documentation for those using this as
                            a platform to build more tools, as well as everything that is in the non-SDK version.</p>
                            <?php

                            $zipfilename="wtp-jsdt-sdk-$build";

                            $filename=$zipfilename.".zip";
                            $zipfilesize=fileSizeForDisplay($filename);
                            $fileShortDescription="wtp-jsdt-sdk";
                            displayFileLine($downloadprefix, $filename, $zipfilesize, $fileShortDescription);
                            ?>
                        
                        </tr>
                    </table>
                    </td>
                
                
                <tr>
            
            </table>
            <!-- ***********  Build Status **************  -->
            <table
                border=0
                cellspacing=2
                cellpadding=2
                width="100%">
                <tr>
                    <td
                        align=left
                        valign=top
                        bgcolor="#0080C0"><font
                        face="'Bitstream Vera',Helvetica,Arial"
                        color="#FFFFFF">Status, tests and other interesting details</font></td>
                </tr>
                <tr>
                    <td>
                    <table
                        border=0
                        cellspacing=2
                        cellpadding=2
                        width="90%"
                        align="center">
                        <tr>
                            <td><a href="buildNotes.php">Build notes</a> <br />
                            <a href="directory.txt">map files</a> <br />
                            <?php

                            if ($displayTestSummary) {


                                if (isset($unitTests_grandTotalErrors)) {
                                    $errorColor="green";
                                    if ($unitTests_grandTotalErrors > 0) {
                                        $errorColor="red";
                                    }
                                    echo "<a href=\"testResults.php\">Unit test results</a>&nbsp;";
                                    echo "<img src=\"junit_err.gif\"/><font color=\"" . $errorColor . "\">" . $unitTests_grandTotalErrors . "</font>&nbsp;&nbsp;Total: " . $unitTests_grandTotalTests;
                                }
                                else {
                                    echo "<br /><font color=\"orange\">Unit tests are pending</font>&nbsp;&nbsp;<img src=\"pending.gif\"/>";
                                }

                                echo "<br />";
                                echo "<a href=\"compileResults.php\">Compile logs: Code Bundles</a>";

                                echo "&nbsp;&nbsp;($code_totalBundles)&nbsp;&nbsp;";
                                echo "<img src=\"compile_err.gif\"/><font color=red>$code_totalErrors</font>&nbsp;";
                                echo "<img src=\"compile_warn.gif\"/><font color=orange>$code_totalWarnings</font>&nbsp;";
                                echo "<img src=\"access_err.gif\"/><font color=red>$code_totalforbiddenAccessWarningCount</font>&nbsp;";
                                echo "<img src=\"access_warn.gif\"/><font color=orange>$code_totaldiscouragedAccessWarningCount</font>&nbsp;";

                                echo "<br />";
                                echo "<a href=\"testCompileResults.php\">Compile logs: Test Bundles</a>";

                                echo "&nbsp;&nbsp;($test_totalBundles)&nbsp;&nbsp;";
                                echo "<img src=\"compile_err.gif\"/><font color=red>$test_totalErrors</font>&nbsp;";
                                echo "<img src=\"compile_warn.gif\"/><font color=orange>$test_totalWarnings</font>&nbsp;";
                                echo "<img src=\"access_err.gif\"/><font color=red>$test_totalforbiddenAccessWarningCount</font>&nbsp;";
                                echo "<img src=\"access_warn.gif\"/><font color=orange>$test_totaldiscouragedAccessWarningCount</font>&nbsp;";
                            }

                            ?> <br />
                            <?php
                            if (file_exists("versioningReportName.php")) {
                                include "versioningReportName.php";
                                $fname="${versionReportFilename}.html";
                                if (file_exists($fname)) {
                                    echo "<br /> <a href='$fname'>Versioning Information</a>";
                                }
}
?> <?php
echo "<br />";
if (file_exists("./apiresults/api-progress.html"))
{
    echo "<br /> <a href=\"apiresults/api-progress.html\">API Progress Report</a>";
}
if (file_exists("./apiresults/api-info-summary.html"))
{
    echo "<br /> <a href=\"apiresults/api-info-summary.html\">APIs Defined by Each Component</a>";
}
if (file_exists("./apiresults/api-ref-compatibility.html"))
{
    echo "<br /> <a href=\"apiresults/api-ref-compatibility.html\">Adopter Breakage Report</a>";
}
if (file_exists("./apiresults/api-violation-summary.html"))
{
    echo "<br /> <a href=\"apiresults/api-violation-summary.html\">API Violations</a>";
}
if (file_exists("./apiresults/component-api-violation-all.html"))
{
    echo "<br /> <a href=\"apiresults/component-api-violation-all.html\">Non-API dependencies</a>";
}
if (file_exists("./apiresults/api-tc-summary.html"))
{
    echo "<br /> <a href=\"apiresults/api-tc-summary.html\">API Test Coverage</a>";
}
if (file_exists("./apiresults/api-javadoc-summary.html"))
{
    echo "<br /> <a href=\"apiresults/api-javadoc-summary.html\">API Javadoc Coverage</a>";
}
if (file_exists("./apiresults/api-tc-summary.html"))
{
    echo "<br /><br /> <a href=\"apiresults/full_test_coverage/api-tc-summary.html\">Test Coverage for All Classes and Methods</a>";
}
?> <?php
if (file_exists("./perfresults/graph/performance.php"))
{
    echo "<br />";
    echo "<br /> <a href=\"perfresults/graph/performance.php\">Performance Results</a>";
    echo "<br />";
}
?></td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            <!-- footer -->
            <center>
            <hr>
            <p>All downloads are provided under the terms and conditions of the <a
                href="http://www.eclipse.org/legal/notice.html">Eclipse.org Software User Agreement</a> unless otherwise
            specified.</p>
            <p>If you have problems downloading the drops, contact the <font
                face="'Bitstream Vera',Helvetica,Arial"
                size="-1"><a href="mailto:webmaster@eclipse.org">webmaster</a></font>.</p>
            </center>
            <!-- end footer -->

</body>
</html>
