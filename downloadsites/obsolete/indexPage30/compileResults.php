<html>
<head>

<?php
        $parts = explode("/", getcwd());
        $parts2 = explode("-", $parts[count($parts) - 1]);
        $buildName = $parts2[1];

        echo "<title>Compile Logs: Code Bundles for $buildName</title>";
?>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<style>
.bold, .bold TD, .bold TH, .bold TR
{
font-weight:bold;
}
.numeric, .numeric TD
{
text-align:right;
padding-right:2%;
}
.normaltable, .normaltable TD, .normaltable TH
{
font-family:Bitstream Vera Sans Mono, monospace;
font-size:0.9em;
color:Black;
background-color:White;
}
.errorltable, .errortable TD, .errortable TH
{
font-family:Bitstream Vera Sans Mono, monospace;
font-size:0.9em;
color:Black;
background-color:Red;
font-weight:bold;
}
.warningtable, .warningtable TD, .warningtable TH
{
font-family:Bitstream Vera Sans Mono, monospace;
font-size:0.9em;
color:Black;
background-color:khaki;
}
.extraWarningTable, .extraWarningTable TD, .extraWarningTable TH
{
font-family:Bitstream Vera Sans Mono, monospace;
font-size:0.9em;
color:Black;
background-color:Yellow;
}
</style>
</head>
<body>

<?php
        echo "<h1>Compile Logs: Code Bundles for $buildName</h1>";
?>


<table id=tabledata align = "center" width="75%" border="1">
  <tr>
    <td class="bold" align="center">Compile Logs</td>
    <td class="bold" align="center">Compile Errors</td>
    <td class="bold" align="center">Compiler Warnings</td>
    <td class="bold" align="center">Access Violations</td>
    <td class="bold" align="center">Access Warnings</td>
  </tr>

  <tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jem.beaninfo_2.0.100.v200806051600/@dot.bin.html" type='text/plain' >org.eclipse.jem.beaninfo</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.beaninfo_2.0.100.v200806051600/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.beaninfo_2.0.100.v200806051600/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.beaninfo_2.0.100.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.beaninfo_2.0.100.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jem.beaninfo_2.0.100.v200806051600/vm/beaninfovm.jar.bin.html" type='text/plain' >org.eclipse.jem.beaninfo/vm</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.beaninfo_2.0.100.v200806051600/vm/beaninfovm.jar.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.beaninfo_2.0.100.v200806051600/vm/beaninfovm.jar.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.beaninfo_2.0.100.v200806051600/vm/beaninfovm.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.beaninfo_2.0.100.v200806051600/vm/beaninfovm.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jem.proxy_2.0.100.v200806051600/@dot.bin.html" type='text/plain' >org.eclipse.jem.proxy</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.proxy_2.0.100.v200806051600/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.proxy_2.0.100.v200806051600/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.proxy_2.0.100.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.proxy_2.0.100.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jem.proxy_2.0.100.v200806051600/vm/remotevm.jar.bin.html" type='text/plain' >org.eclipse.jem.proxy/vm</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.proxy_2.0.100.v200806051600/vm/remotevm.jar.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.proxy_2.0.100.v200806051600/vm/remotevm.jar.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.proxy_2.0.100.v200806051600/vm/remotevm.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.proxy_2.0.100.v200806051600/vm/remotevm.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jem.ui_2.0.100.v200806051600/@dot.bin.html" type='text/plain' >org.eclipse.jem.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.ui_2.0.100.v200806051600/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.ui_2.0.100.v200806051600/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.ui_2.0.100.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.ui_2.0.100.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jem.util_2.0.100.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jem.util</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.util_2.0.100.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.util_2.0.100.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.util_2.0.100.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.util_2.0.100.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jem.workbench_2.0.100.v200806051600/@dot.bin.html" type='text/plain' >org.eclipse.jem.workbench</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.workbench_2.0.100.v200806051600/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.workbench_2.0.100.v200806051600/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.workbench_2.0.100.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem.workbench_2.0.100.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jem_2.0.200.v200806051600/@dot.bin.html" type='text/plain' >org.eclipse.jem</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem_2.0.200.v200806051600/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem_2.0.200.v200806051600/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem_2.0.200.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jem_2.0.200.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jpt.core_2.0.0.v200805220000/@dot.bin.html" type='text/plain' >org.eclipse.jpt.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.core_2.0.0.v200805220000/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.core_2.0.0.v200805220000/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >3</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.core_2.0.0.v200805220000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.core_2.0.0.v200805220000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jpt.db.ui_1.0.1.v200805020000/@dot.bin.html" type='text/plain' >org.eclipse.jpt.db.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.db.ui_1.0.1.v200805020000/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.db.ui_1.0.1.v200805020000/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.db.ui_1.0.1.v200805020000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.db.ui_1.0.1.v200805020000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jpt.db_1.1.1.v200805020000/@dot.bin.html" type='text/plain' >org.eclipse.jpt.db</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.db_1.1.1.v200805020000/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.db_1.1.1.v200805020000/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.db_1.1.1.v200805020000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.db_1.1.1.v200805020000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.core.ddlgen_1.0.0/@dot.bin.html" type='text/plain' >org.eclipse.jpt.eclipselink.core.ddlgen</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.core.ddlgen_1.0.0/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.core.ddlgen_1.0.0/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.core.ddlgen_1.0.0/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.core.ddlgen_1.0.0/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.core_1.0.0.v200806050000/@dot.bin.html" type='text/plain' >org.eclipse.jpt.eclipselink.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.core_1.0.0.v200806050000/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.core_1.0.0.v200806050000/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.core_1.0.0.v200806050000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.core_1.0.0.v200806050000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >5</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.ui_1.0.0.v200805300000/@dot.bin.html" type='text/plain' >org.eclipse.jpt.eclipselink.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.ui_1.0.0.v200805300000/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.ui_1.0.0.v200805300000/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >6</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.ui_1.0.0.v200805300000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.eclipselink.ui_1.0.0.v200805300000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jpt.gen_1.1.0.v200805020000/@dot.bin.html" type='text/plain' >org.eclipse.jpt.gen</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.gen_1.1.0.v200805020000/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.gen_1.1.0.v200805020000/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.gen_1.1.0.v200805020000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.gen_1.1.0.v200805020000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jpt.ui_2.0.0.v200806050000/@dot.bin.html" type='text/plain' >org.eclipse.jpt.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.ui_2.0.0.v200806050000/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.ui_2.0.0.v200806050000/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >9</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.ui_2.0.0.v200806050000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.ui_2.0.0.v200806050000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >23</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jpt.utility_1.2.0.v200805140000/@dot.bin.html" type='text/plain' >org.eclipse.jpt.utility</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.utility_1.2.0.v200805140000/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.utility_1.2.0.v200805140000/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.utility_1.2.0.v200805140000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jpt.utility_1.2.0.v200805140000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.common.annotations.controller_1.1.100.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jst.common.annotations.controller</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.controller_1.1.100.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.controller_1.1.100.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.controller_1.1.100.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.controller_1.1.100.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.common.annotations.core_1.1.100.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jst.common.annotations.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.core_1.1.100.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.core_1.1.100.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.core_1.1.100.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.core_1.1.100.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.common.annotations.ui_1.1.102.v200806010600/@dot.bin.html" type='text/plain' >org.eclipse.jst.common.annotations.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.ui_1.1.102.v200806010600/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.ui_1.1.102.v200806010600/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.ui_1.1.102.v200806010600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.annotations.ui_1.1.102.v200806010600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.common.frameworks_1.1.200.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jst.common.frameworks</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.frameworks_1.1.200.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.frameworks_1.1.200.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.frameworks_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.frameworks_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.common.project.facet.core_1.3.0.v200805151903/@dot.bin.html" type='text/plain' >org.eclipse.jst.common.project.facet.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.project.facet.core_1.3.0.v200805151903/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.project.facet.core_1.3.0.v200805151903/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.project.facet.core_1.3.0.v200805151903/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.project.facet.core_1.3.0.v200805151903/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.common.project.facet.ui_1.3.0.v200805092201/@dot.bin.html" type='text/plain' >org.eclipse.jst.common.project.facet.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.project.facet.ui_1.3.0.v200805092201/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.project.facet.ui_1.3.0.v200805092201/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.project.facet.ui_1.3.0.v200805092201/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.common.project.facet.ui_1.3.0.v200805092201/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ejb.ui_1.1.200.v200805292155/@dot.bin.html" type='text/plain' >org.eclipse.jst.ejb.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ejb.ui_1.1.200.v200805292155/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ejb.ui_1.1.200.v200805292155/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >3</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ejb.ui_1.1.200.v200805292155/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ejb.ui_1.1.200.v200805292155/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >26</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.core_1.1.200.v200806052000/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.core_1.1.200.v200806052000/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.core_1.1.200.v200806052000/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >20</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.core_1.1.200.v200806052000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.core_1.1.200.v200806052000/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >15</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotation.model_1.1.102.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.ejb.annotation.model</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotation.model_1.1.102.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotation.model_1.1.102.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotation.model_1.1.102.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotation.model_1.1.102.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.emitter_1.1.102.v200805140135/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.ejb.annotations.emitter</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.emitter_1.1.102.v200805140135/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.emitter_1.1.102.v200805140135/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.emitter_1.1.102.v200805140135/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.emitter_1.1.102.v200805140135/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.ui_1.1.102.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.ejb.annotations.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.ui_1.1.102.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.ui_1.1.102.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >2</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.ui_1.1.102.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.ui_1.1.102.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >8</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.xdoclet_1.2.0.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.ejb.annotations.xdoclet</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.xdoclet_1.2.0.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.xdoclet_1.2.0.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.xdoclet_1.2.0.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb.annotations.xdoclet_1.2.0.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb_1.1.200.v200806041500/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.ejb</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb_1.1.200.v200806041500/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb_1.1.200.v200806041500/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >39</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb_1.1.200.v200806041500/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ejb_1.1.200.v200806041500/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >13</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.jca.ui_1.1.200.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.jca.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.jca.ui_1.1.200.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.jca.ui_1.1.200.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.jca.ui_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.jca.ui_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.jca_1.1.200.v200806041500/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.jca</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.jca_1.1.200.v200806041500/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.jca_1.1.200.v200806041500/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.jca_1.1.200.v200806041500/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.jca_1.1.200.v200806041500/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.navigator.ui_1.1.200.v200805221242/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.navigator.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.navigator.ui_1.1.200.v200805221242/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.navigator.ui_1.1.200.v200805221242/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >2</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.navigator.ui_1.1.200.v200805221242/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.navigator.ui_1.1.200.v200805221242/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.ui_1.1.200.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ui_1.1.200.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ui_1.1.200.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >15</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ui_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.ui_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >96</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.web_1.1.200.v200806051600/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.web</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.web_1.1.200.v200806051600/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.web_1.1.200.v200806051600/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >28</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.web_1.1.200.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.web_1.1.200.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.webservice.ui_1.1.200.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.webservice.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.webservice.ui_1.1.200.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.webservice.ui_1.1.200.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.webservice.ui_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.webservice.ui_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.webservice_1.1.200.v200805150230/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.webservice</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.webservice_1.1.200.v200805150230/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.webservice_1.1.200.v200805150230/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >4</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.webservice_1.1.200.v200805150230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.webservice_1.1.200.v200805150230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee.xdoclet.runtime_1.1.102.v200805140135/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee.xdoclet.runtime</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.xdoclet.runtime_1.1.102.v200805140135/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.xdoclet.runtime_1.1.102.v200805140135/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >2</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.xdoclet.runtime_1.1.102.v200805140135/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee.xdoclet.runtime_1.1.102.v200805140135/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.j2ee_1.1.200.v200806051600/@dot.bin.html" type='text/plain' >org.eclipse.jst.j2ee</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee_1.1.200.v200806051600/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee_1.1.200.v200806051600/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >40</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee_1.1.200.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.j2ee_1.1.200.v200806051600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >56</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jee.ejb_1.0.100.v200805151150/@dot.bin.html" type='text/plain' >org.eclipse.jst.jee.ejb</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.ejb_1.0.100.v200805151150/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.ejb_1.0.100.v200805151150/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >7</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.ejb_1.0.100.v200805151150/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.ejb_1.0.100.v200805151150/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jee.ui_1.0.100.v200805221242/@dot.bin.html" type='text/plain' >org.eclipse.jst.jee.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.ui_1.0.100.v200805221242/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.ui_1.0.100.v200805221242/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >14</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.ui_1.0.100.v200805221242/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.ui_1.0.100.v200805221242/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >13</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jee.web_1.0.100.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jst.jee.web</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.web_1.0.100.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.web_1.0.100.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >4</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.web_1.0.100.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee.web_1.0.100.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jee_1.0.100.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.jst.jee</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee_1.0.100.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee_1.0.100.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >11</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee_1.0.100.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jee_1.0.100.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jsf.apache.trinidad.tagsupport_1.0.0.v20080605/@dot.bin.html" type='text/plain' >org.eclipse.jst.jsf.apache.trinidad.tagsupport</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.apache.trinidad.tagsupport_1.0.0.v20080605/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.apache.trinidad.tagsupport_1.0.0.v20080605/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.apache.trinidad.tagsupport_1.0.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.apache.trinidad.tagsupport_1.0.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jsf.common.runtime_1.0.0.v20080605/@dot.bin.html" type='text/plain' >org.eclipse.jst.jsf.common.runtime</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common.runtime_1.0.0.v20080605/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common.runtime_1.0.0.v20080605/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common.runtime_1.0.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common.runtime_1.0.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jsf.common.ui_1.1.0.v20080605/@dot.bin.html" type='text/plain' >org.eclipse.jst.jsf.common.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common.ui_1.1.0.v20080605/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common.ui_1.1.0.v20080605/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common.ui_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common.ui_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jsf.common_1.1.0.v20080605/@dot.bin.html" type='text/plain' >org.eclipse.jst.jsf.common</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common_1.1.0.v20080605/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common_1.1.0.v20080605/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >27</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.common_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jsf.core_1.1.0.v20080606/@dot.bin.html" type='text/plain' >org.eclipse.jst.jsf.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.core_1.1.0.v20080606/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.core_1.1.0.v20080606/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >5</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.core_1.1.0.v20080606/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.core_1.1.0.v20080606/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >3</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jsf.facesconfig.ui_1.1.0.v20080610/@dot.bin.html" type='text/plain' >org.eclipse.jst.jsf.facesconfig.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.facesconfig.ui_1.1.0.v20080610/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.facesconfig.ui_1.1.0.v20080610/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >2</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.facesconfig.ui_1.1.0.v20080610/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.facesconfig.ui_1.1.0.v20080610/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jsf.facesconfig_1.1.0.v20080605/@dot.bin.html" type='text/plain' >org.eclipse.jst.jsf.facesconfig</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.facesconfig_1.1.0.v20080605/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.facesconfig_1.1.0.v20080605/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >31</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.facesconfig_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.facesconfig_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jsf.standard.tagsupport_1.1.0.v20080515/@dot.bin.html" type='text/plain' >org.eclipse.jst.jsf.standard.tagsupport</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.standard.tagsupport_1.1.0.v20080515/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.standard.tagsupport_1.1.0.v20080515/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.standard.tagsupport_1.1.0.v20080515/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.standard.tagsupport_1.1.0.v20080515/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jsf.ui_1.1.0.v20080606/@dot.bin.html" type='text/plain' >org.eclipse.jst.jsf.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.ui_1.1.0.v20080606/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.ui_1.1.0.v20080606/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.ui_1.1.0.v20080606/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsf.ui_1.1.0.v20080606/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >3</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jsp.core_1.2.100.v200806050300/@dot.bin.html" type='text/plain' >org.eclipse.jst.jsp.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsp.core_1.2.100.v200806050300/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsp.core_1.2.100.v200806050300/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsp.core_1.2.100.v200806050300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsp.core_1.2.100.v200806050300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >19</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.jsp.ui_1.1.300.v200805152207/@dot.bin.html" type='text/plain' >org.eclipse.jst.jsp.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsp.ui_1.1.300.v200805152207/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsp.ui_1.1.300.v200805152207/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsp.ui_1.1.300.v200805152207/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.jsp.ui_1.1.300.v200805152207/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >17</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.pagedesigner.jsf.ui_1.1.0.v20080605/@dot.bin.html" type='text/plain' >org.eclipse.jst.pagedesigner.jsf.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner.jsf.ui_1.1.0.v20080605/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner.jsf.ui_1.1.0.v20080605/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner.jsf.ui_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner.jsf.ui_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >2</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.pagedesigner.jsp.core_1.1.0.v20080605/@dot.bin.html" type='text/plain' >org.eclipse.jst.pagedesigner.jsp.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner.jsp.core_1.1.0.v20080605/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner.jsp.core_1.1.0.v20080605/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner.jsp.core_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner.jsp.core_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.pagedesigner_1.1.0.v20080605/@dot.bin.html" type='text/plain' >org.eclipse.jst.pagedesigner</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner_1.1.0.v20080605/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner_1.1.0.v20080605/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >2</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.pagedesigner_1.1.0.v20080605/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >12</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.server.core_1.1.0.v20080528/@dot.bin.html" type='text/plain' >org.eclipse.jst.server.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.core_1.1.0.v20080528/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.core_1.1.0.v20080528/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.core_1.1.0.v20080528/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.core_1.1.0.v20080528/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.server.generic.core_1.0.305.v200805140145/@dot.bin.html" type='text/plain' >org.eclipse.jst.server.generic.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.generic.core_1.0.305.v200805140145/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.generic.core_1.0.305.v200805140145/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >4</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.generic.core_1.0.305.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.generic.core_1.0.305.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >13</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.server.generic.ui_1.0.305.v200805140145/@dot.bin.html" type='text/plain' >org.eclipse.jst.server.generic.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.generic.ui_1.0.305.v200805140145/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.generic.ui_1.0.305.v200805140145/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.generic.ui_1.0.305.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.generic.ui_1.0.305.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.server.preview.adapter_1.0.100.v200805140145/@dot.bin.html" type='text/plain' >org.eclipse.jst.server.preview.adapter</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.preview.adapter_1.0.100.v200805140145/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.preview.adapter_1.0.100.v200805140145/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.preview.adapter_1.0.100.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.preview.adapter_1.0.100.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.server.tomcat.core_1.1.101.v200805140145/@dot.bin.html" type='text/plain' >org.eclipse.jst.server.tomcat.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.tomcat.core_1.1.101.v200805140145/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.tomcat.core_1.1.101.v200805140145/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.tomcat.core_1.1.101.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.tomcat.core_1.1.101.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.server.tomcat.ui_1.1.100.v20080528/@dot.bin.html" type='text/plain' >org.eclipse.jst.server.tomcat.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.tomcat.ui_1.1.100.v20080528/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.tomcat.ui_1.1.100.v20080528/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.tomcat.ui_1.1.100.v20080528/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.tomcat.ui_1.1.100.v20080528/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.server.ui_1.0.303.v200805140145/@dot.bin.html" type='text/plain' >org.eclipse.jst.server.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.ui_1.0.303.v200805140145/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.ui_1.0.303.v200805140145/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.ui_1.0.303.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.ui_1.0.303.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >11</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.server.websphere.core_1.0.301.v200805140145/webspherecore.jar.bin.html" type='text/plain' >org.eclipse.jst.server.websphere.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.websphere.core_1.0.301.v200805140145/webspherecore.jar.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.websphere.core_1.0.301.v200805140145/webspherecore.jar.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.websphere.core_1.0.301.v200805140145/webspherecore.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.server.websphere.core_1.0.301.v200805140145/webspherecore.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.servlet.ui_1.1.200.v200805150230/@dot.bin.html" type='text/plain' >org.eclipse.jst.servlet.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.servlet.ui_1.1.200.v200805150230/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.servlet.ui_1.1.200.v200805150230/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >5</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.servlet.ui_1.1.200.v200805150230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.servlet.ui_1.1.200.v200805150230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >10</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.axis.consumption.core_1.0.304.v200805140230/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.axis.consumption.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.consumption.core_1.0.304.v200805140230/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.consumption.core_1.0.304.v200805140230/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.consumption.core_1.0.304.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.consumption.core_1.0.304.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.axis.consumption.ui_1.0.303.v200805140230/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.axis.consumption.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.consumption.ui_1.0.303.v200805140230/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.consumption.ui_1.0.303.v200805140230/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.consumption.ui_1.0.303.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.consumption.ui_1.0.303.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.axis.creation.ui_1.0.305.v200805140230/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.axis.creation.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.creation.ui_1.0.305.v200805140230/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.creation.ui_1.0.305.v200805140230/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.creation.ui_1.0.305.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis.creation.ui_1.0.305.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.consumption.core_1.0.102.v200805301834/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.axis2.consumption.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.consumption.core_1.0.102.v200805301834/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.consumption.core_1.0.102.v200805301834/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >25</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.consumption.core_1.0.102.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.consumption.core_1.0.102.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >3</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.consumption.ui_1.0.101.v200805301834/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.axis2.consumption.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.consumption.ui_1.0.101.v200805301834/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.consumption.ui_1.0.101.v200805301834/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >2</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.consumption.ui_1.0.101.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.consumption.ui_1.0.101.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.core_1.0.102.v200805301834/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.axis2.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.core_1.0.102.v200805301834/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.core_1.0.102.v200805301834/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.core_1.0.102.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.core_1.0.102.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.creation.core_1.0.102.v200805301834/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.axis2.creation.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.creation.core_1.0.102.v200805301834/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.creation.core_1.0.102.v200805301834/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >4</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.creation.core_1.0.102.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.creation.core_1.0.102.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.creation.ui_1.0.102.v200805301834/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.axis2.creation.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.creation.ui_1.0.102.v200805301834/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.creation.ui_1.0.102.v200805301834/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >2</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.creation.ui_1.0.102.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.creation.ui_1.0.102.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.ui_1.0.201.v200805301834/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.axis2.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.ui_1.0.201.v200805301834/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.ui_1.0.201.v200805301834/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.ui_1.0.201.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.axis2.ui_1.0.201.v200805301834/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.consumption.ui_1.1.0.v200806050222/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.consumption.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption.ui_1.1.0.v200806050222/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption.ui_1.1.0.v200806050222/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption.ui_1.1.0.v200806050222/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption.ui_1.1.0.v200806050222/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="errortable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.consumption_1.0.304.v200805140330/webserviceutils.jar.bin.html" type='text/plain' >org.eclipse.jst.ws.consumption</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption_1.0.304.v200805140330/webserviceutils.jar.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption_1.0.304.v200805140330/webserviceutils.jar.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption_1.0.304.v200805140330/webserviceutils.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >40</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption_1.0.304.v200805140330/webserviceutils.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.consumption_1.0.304.v200805140330/wsc.jar.bin.html" type='text/plain' >org.eclipse.jst.ws.consumption</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption_1.0.304.v200805140330/wsc.jar.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption_1.0.304.v200805140330/wsc.jar.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption_1.0.304.v200805140330/wsc.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.consumption_1.0.304.v200805140330/wsc.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.creation.ui_1.0.305.v200805281530/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.creation.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.creation.ui_1.0.305.v200805281530/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.creation.ui_1.0.305.v200805281530/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.creation.ui_1.0.305.v200805281530/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.creation.ui_1.0.305.v200805281530/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.uddiregistry_1.0.300.v200805140230/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.uddiregistry</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.uddiregistry_1.0.300.v200805140230/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.uddiregistry_1.0.300.v200805140230/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.uddiregistry_1.0.300.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.uddiregistry_1.0.300.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws.ui_1.0.302.v200805140230/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.ui_1.0.302.v200805140230/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.ui_1.0.302.v200805140230/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.ui_1.0.302.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws.ui_1.0.302.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.jst.ws_1.0.304.v200805140230/@dot.bin.html" type='text/plain' >org.eclipse.jst.ws</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws_1.0.304.v200805140230/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws_1.0.304.v200805140230/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws_1.0.304.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.jst.ws_1.0.304.v200805140230/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.command.env.core_1.0.204.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.wst.command.env.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env.core_1.0.204.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env.core_1.0.204.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env.core_1.0.204.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env.core_1.0.204.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.command.env.ui_1.1.0.v200805140415/@dot.bin.html" type='text/plain' >org.eclipse.wst.command.env.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env.ui_1.1.0.v200805140415/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env.ui_1.1.0.v200805140415/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env.ui_1.1.0.v200805140415/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env.ui_1.1.0.v200805140415/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.command.env_1.0.305.v200805281530/ant-lib/anttasks.jar.bin.html" type='text/plain' >org.eclipse.wst.command.env/ant-lib</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env_1.0.305.v200805281530/ant-lib/anttasks.jar.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env_1.0.305.v200805281530/ant-lib/anttasks.jar.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env_1.0.305.v200805281530/ant-lib/anttasks.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env_1.0.305.v200805281530/ant-lib/anttasks.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.command.env_1.0.305.v200805281530/env.jar.bin.html" type='text/plain' >org.eclipse.wst.command.env</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env_1.0.305.v200805281530/env.jar.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env_1.0.305.v200805281530/env.jar.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env_1.0.305.v200805281530/env.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.command.env_1.0.305.v200805281530/env.jar.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.core_1.1.201.v200806010600/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.core_1.1.201.v200806010600/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.core_1.1.201.v200806010600/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.core_1.1.201.v200806010600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.core_1.1.201.v200806010600/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.emf_1.1.200.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.emf</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.emf_1.1.200.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.emf_1.1.200.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.emf_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.emf_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.emfworkbench.integration_1.1.200.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.emfworkbench.integration</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.emfworkbench.integration_1.1.200.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.emfworkbench.integration_1.1.200.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.emfworkbench.integration_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.emfworkbench.integration_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >5</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.environment_1.0.201.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.environment</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.environment_1.0.201.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.environment_1.0.201.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.environment_1.0.201.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.environment_1.0.201.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.frameworks.ui_1.1.200.v200805221400/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.frameworks.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.frameworks.ui_1.1.200.v200805221400/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.frameworks.ui_1.1.200.v200805221400/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.frameworks.ui_1.1.200.v200805221400/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.frameworks.ui_1.1.200.v200805221400/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.frameworks_1.1.200.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.frameworks</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.frameworks_1.1.200.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.frameworks_1.1.200.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.frameworks_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.frameworks_1.1.200.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >3</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.modulecore_1.1.200.v200806091800/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.modulecore</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.modulecore_1.1.200.v200806091800/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.modulecore_1.1.200.v200806091800/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >3</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.modulecore_1.1.200.v200806091800/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.modulecore_1.1.200.v200806091800/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >11</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.project.facet.core_1.3.0.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.project.facet.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.project.facet.core_1.3.0.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.project.facet.core_1.3.0.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >10</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.project.facet.core_1.3.0.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.project.facet.core_1.3.0.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.project.facet.ui_1.3.0.v200805281731/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.project.facet.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.project.facet.ui_1.3.0.v200805281731/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.project.facet.ui_1.3.0.v200805281731/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.project.facet.ui_1.3.0.v200805281731/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.project.facet.ui_1.3.0.v200805281731/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.snippets_1.1.200.v200805140415/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.snippets</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.snippets_1.1.200.v200805140415/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.snippets_1.1.200.v200805140415/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.snippets_1.1.200.v200805140415/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.snippets_1.1.200.v200805140415/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.ui_1.1.301.v200805140415/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.ui_1.1.301.v200805140415/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.ui_1.1.301.v200805140415/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.ui_1.1.301.v200805140415/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.ui_1.1.301.v200805140415/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.common.uriresolver_1.1.301.v200805140415/@dot.bin.html" type='text/plain' >org.eclipse.wst.common.uriresolver</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.uriresolver_1.1.301.v200805140415/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.uriresolver_1.1.301.v200805140415/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.uriresolver_1.1.301.v200805140415/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.common.uriresolver_1.1.301.v200805140415/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.css.core_1.1.200.v200805140200/@dot.bin.html" type='text/plain' >org.eclipse.wst.css.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.css.core_1.1.200.v200805140200/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.css.core_1.1.200.v200805140200/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >2</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.css.core_1.1.200.v200805140200/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.css.core_1.1.200.v200805140200/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.css.ui_1.0.300.v200805152207/@dot.bin.html" type='text/plain' >org.eclipse.wst.css.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.css.ui_1.0.300.v200805152207/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.css.ui_1.0.300.v200805152207/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.css.ui_1.0.300.v200805152207/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.css.ui_1.0.300.v200805152207/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.dtd.core_1.1.200.v200805140200/@dot.bin.html" type='text/plain' >org.eclipse.wst.dtd.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.dtd.core_1.1.200.v200805140200/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.dtd.core_1.1.200.v200805140200/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.dtd.core_1.1.200.v200805140200/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.dtd.core_1.1.200.v200805140200/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.dtd.ui_1.0.300.v200805152207/@dot.bin.html" type='text/plain' >org.eclipse.wst.dtd.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.dtd.ui_1.0.300.v200805152207/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.dtd.ui_1.0.300.v200805152207/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.dtd.ui_1.0.300.v200805152207/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.dtd.ui_1.0.300.v200805152207/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.html.core_1.1.200.v200806050300/@dot.bin.html" type='text/plain' >org.eclipse.wst.html.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.core_1.1.200.v200806050300/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.core_1.1.200.v200806050300/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.core_1.1.200.v200806050300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.core_1.1.200.v200806050300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.html.ui_1.0.300.v200805152207/@dot.bin.html" type='text/plain' >org.eclipse.wst.html.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.ui_1.0.300.v200805152207/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.ui_1.0.300.v200805152207/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.ui_1.0.300.v200805152207/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.ui_1.0.300.v200805152207/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.internet.cache_1.0.301.v200805140020/@dot.bin.html" type='text/plain' >org.eclipse.wst.internet.cache</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.cache_1.0.301.v200805140020/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.cache_1.0.301.v200805140020/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.cache_1.0.301.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.cache_1.0.301.v200805140020/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.internet.monitor.core_1.0.303.v200805140145/@dot.bin.html" type='text/plain' >org.eclipse.wst.internet.monitor.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.monitor.core_1.0.303.v200805140145/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.monitor.core_1.0.303.v200805140145/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.monitor.core_1.0.303.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.monitor.core_1.0.303.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.internet.monitor.ui_1.0.304.v200805140145/@dot.bin.html" type='text/plain' >org.eclipse.wst.internet.monitor.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.monitor.ui_1.0.304.v200805140145/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.monitor.ui_1.0.304.v200805140145/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.monitor.ui_1.0.304.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.internet.monitor.ui_1.0.304.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.javascript.core_1.0.200.v200805140200/@dot.bin.html" type='text/plain' >org.eclipse.wst.javascript.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.javascript.core_1.0.200.v200805140200/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.javascript.core_1.0.200.v200805140200/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.javascript.core_1.0.200.v200805140200/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.javascript.core_1.0.200.v200805140200/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.javascript.ui_1.0.200.v200805160650/@dot.bin.html" type='text/plain' >org.eclipse.wst.javascript.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.javascript.ui_1.0.200.v200805160650/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.javascript.ui_1.0.200.v200805160650/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.javascript.ui_1.0.200.v200805160650/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.javascript.ui_1.0.200.v200805160650/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.jsdt.core_1.0.0.v200805291857/@dot.bin.html" type='text/plain' >org.eclipse.wst.jsdt.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.core_1.0.0.v200805291857/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.core_1.0.0.v200805291857/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >52</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.core_1.0.0.v200805291857/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.core_1.0.0.v200805291857/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.jsdt.manipulation_1.0.0.v200804302218/@dot.bin.html" type='text/plain' >org.eclipse.wst.jsdt.manipulation</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.manipulation_1.0.0.v200804302218/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.manipulation_1.0.0.v200804302218/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.manipulation_1.0.0.v200804302218/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.manipulation_1.0.0.v200804302218/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.jsdt.support.firefox_1.0.0.v200804302218/@dot.bin.html" type='text/plain' >org.eclipse.wst.jsdt.support.firefox</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.support.firefox_1.0.0.v200804302218/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.support.firefox_1.0.0.v200804302218/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.support.firefox_1.0.0.v200804302218/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.support.firefox_1.0.0.v200804302218/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.jsdt.support.ie_1.0.0.v200804302218/@dot.bin.html" type='text/plain' >org.eclipse.wst.jsdt.support.ie</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.support.ie_1.0.0.v200804302218/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.support.ie_1.0.0.v200804302218/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >2</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.support.ie_1.0.0.v200804302218/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.support.ie_1.0.0.v200804302218/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.jsdt.ui_1.0.0.v200805282135/@dot.bin.html" type='text/plain' >org.eclipse.wst.jsdt.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.ui_1.0.0.v200805282135/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.ui_1.0.0.v200805282135/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >54</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.ui_1.0.0.v200805282135/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.ui_1.0.0.v200805282135/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.core_1.0.0.v200805221604/@dot.bin.html" type='text/plain' >org.eclipse.wst.jsdt.web.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.core_1.0.0.v200805221604/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.core_1.0.0.v200805221604/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >15</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.core_1.0.0.v200805221604/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.core_1.0.0.v200805221604/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.support.jsp_1.0.0.v200804302218/@dot.bin.html" type='text/plain' >org.eclipse.wst.jsdt.web.support.jsp</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.support.jsp_1.0.0.v200804302218/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.support.jsp_1.0.0.v200804302218/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.support.jsp_1.0.0.v200804302218/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.support.jsp_1.0.0.v200804302218/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.ui_1.0.0.v200804302218/@dot.bin.html" type='text/plain' >org.eclipse.wst.jsdt.web.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.ui_1.0.0.v200804302218/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.ui_1.0.0.v200804302218/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >13</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.ui_1.0.0.v200804302218/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.jsdt.web.ui_1.0.0.v200804302218/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >3</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.server.core_1.1.0.v20080530/@dot.bin.html" type='text/plain' >org.eclipse.wst.server.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.core_1.1.0.v20080530/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.core_1.1.0.v20080530/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >2</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.core_1.1.0.v20080530/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.core_1.1.0.v20080530/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.server.http.core_1.0.100.v200805140145/@dot.bin.html" type='text/plain' >org.eclipse.wst.server.http.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.http.core_1.0.100.v200805140145/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.http.core_1.0.100.v200805140145/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.http.core_1.0.100.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.http.core_1.0.100.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.server.http.ui_1.0.100.v200805140145/@dot.bin.html" type='text/plain' >org.eclipse.wst.server.http.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.http.ui_1.0.100.v200805140145/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.http.ui_1.0.100.v200805140145/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.http.ui_1.0.100.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.http.ui_1.0.100.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.server.preview.adapter_1.0.100.v200805140145/@dot.bin.html" type='text/plain' >org.eclipse.wst.server.preview.adapter</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.preview.adapter_1.0.100.v200805140145/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.preview.adapter_1.0.100.v200805140145/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.preview.adapter_1.0.100.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.preview.adapter_1.0.100.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.server.preview_1.0.100.v200805140145/@dot.bin.html" type='text/plain' >org.eclipse.wst.server.preview</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.preview_1.0.100.v200805140145/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.preview_1.0.100.v200805140145/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.preview_1.0.100.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.preview_1.0.100.v200805140145/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.server.ui_1.1.0.v20080515/@dot.bin.html" type='text/plain' >org.eclipse.wst.server.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.ui_1.1.0.v20080515/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.ui_1.1.0.v20080515/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.ui_1.1.0.v20080515/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.server.ui_1.1.0.v20080515/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >46</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.sse.core_1.1.300.v200805221633/@dot.bin.html" type='text/plain' >org.eclipse.wst.sse.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.sse.core_1.1.300.v200805221633/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.sse.core_1.1.300.v200805221633/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.sse.core_1.1.300.v200805221633/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.sse.core_1.1.300.v200805221633/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.sse.ui_1.1.0.v200806041847/@dot.bin.html" type='text/plain' >org.eclipse.wst.sse.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.sse.ui_1.1.0.v200806041847/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.sse.ui_1.1.0.v200806041847/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.sse.ui_1.1.0.v200806041847/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.sse.ui_1.1.0.v200806041847/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >25</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.validation.ui_1.2.0.v200805262011/@dot.bin.html" type='text/plain' >org.eclipse.wst.validation.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.validation.ui_1.2.0.v200805262011/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.validation.ui_1.2.0.v200805262011/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.validation.ui_1.2.0.v200805262011/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.validation.ui_1.2.0.v200805262011/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.validation_1.2.0.v200806051402/@dot.bin.html" type='text/plain' >org.eclipse.wst.validation</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.validation_1.2.0.v200806051402/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.validation_1.2.0.v200806051402/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.validation_1.2.0.v200806051402/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.validation_1.2.0.v200806051402/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.web.ui_1.1.200.v200805281530/@dot.bin.html" type='text/plain' >org.eclipse.wst.web.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.web.ui_1.1.200.v200805281530/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.web.ui_1.1.200.v200805281530/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.web.ui_1.1.200.v200805281530/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.web.ui_1.1.200.v200805281530/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.web_1.1.202.v200805140200/@dot.bin.html" type='text/plain' >org.eclipse.wst.web</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.web_1.1.202.v200805140200/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.web_1.1.202.v200805140200/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.web_1.1.202.v200805140200/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.web_1.1.202.v200805140200/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.ws.explorer_1.0.305.v200806041425/@dot.bin.html" type='text/plain' >org.eclipse.wst.ws.explorer</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.explorer_1.0.305.v200806041425/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.explorer_1.0.305.v200806041425/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.explorer_1.0.305.v200806041425/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.explorer_1.0.305.v200806041425/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >5</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.ws.parser_1.0.202.v200805140300/@dot.bin.html" type='text/plain' >org.eclipse.wst.ws.parser</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.parser_1.0.202.v200805140300/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.parser_1.0.202.v200805140300/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.parser_1.0.202.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.parser_1.0.202.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.ws.service.policy.ui_1.0.0.v200806031937/@dot.bin.html" type='text/plain' >org.eclipse.wst.ws.service.policy.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.service.policy.ui_1.0.0.v200806031937/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.service.policy.ui_1.0.0.v200806031937/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.service.policy.ui_1.0.0.v200806031937/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.service.policy.ui_1.0.0.v200806031937/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.ws.service.policy_1.0.0.v200805221441/@dot.bin.html" type='text/plain' >org.eclipse.wst.ws.service.policy</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.service.policy_1.0.0.v200805221441/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.service.policy_1.0.0.v200805221441/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.service.policy_1.0.0.v200805221441/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.service.policy_1.0.0.v200805221441/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.ws.ui_1.1.0.v200805140300/@dot.bin.html" type='text/plain' >org.eclipse.wst.ws.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.ui_1.1.0.v200805140300/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.ui_1.1.0.v200805140300/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.ui_1.1.0.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws.ui_1.1.0.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.ws_1.1.0.v200805140300/@dot.bin.html" type='text/plain' >org.eclipse.wst.ws</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws_1.1.0.v200805140300/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws_1.1.0.v200805140300/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws_1.1.0.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.ws_1.1.0.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.wsdl.ui_1.2.100.v200806051913/@dot.bin.html" type='text/plain' >org.eclipse.wst.wsdl.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl.ui_1.2.100.v200806051913/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl.ui_1.2.100.v200806051913/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl.ui_1.2.100.v200806051913/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl.ui_1.2.100.v200806051913/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.wsdl.validation_1.1.301.v200805140300/@dot.bin.html" type='text/plain' >org.eclipse.wst.wsdl.validation</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl.validation_1.1.301.v200805140300/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl.validation_1.1.301.v200805140300/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl.validation_1.1.301.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl.validation_1.1.301.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.wsdl_1.1.200.v200805140300/@dot.bin.html" type='text/plain' >org.eclipse.wst.wsdl</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl_1.1.200.v200805140300/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl_1.1.200.v200805140300/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl_1.1.200.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsdl_1.1.200.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.wsi.ui_1.0.400.v200805140300/@dot.bin.html" type='text/plain' >org.eclipse.wst.wsi.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsi.ui_1.0.400.v200805140300/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsi.ui_1.0.400.v200805140300/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsi.ui_1.0.400.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsi.ui_1.0.400.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.wsi_1.0.205.v200805140300/@dot.bin.html" type='text/plain' >org.eclipse.wst.wsi</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsi_1.0.205.v200805140300/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsi_1.0.205.v200805140300/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsi_1.0.205.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.wsi_1.0.205.v200805140300/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.html.core_1.1.300.v200806051910/@dot.bin.html" type='text/plain' >org.eclipse.wst.html.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.core_1.1.300.v200806051910/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.core_1.1.300.v200806051910/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >1</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.core_1.1.300.v200806051910/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.core_1.1.300.v200806051910/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.html.ui_1.0.400.v200806051910/@dot.bin.html" type='text/plain' >org.eclipse.wst.html.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.ui_1.0.400.v200806051910/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.ui_1.0.400.v200806051910/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.ui_1.0.400.v200806051910/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.html.ui_1.0.400.v200806051910/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="normaltable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.xsd.core_1.1.300.v200805282012/@dot.bin.html" type='text/plain' >org.eclipse.wst.xsd.core</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.xsd.core_1.1.300.v200805282012/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.xsd.core_1.1.300.v200805282012/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >0</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.xsd.core_1.1.300.v200805282012/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.xsd.core_1.1.300.v200805282012/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="warningtable">
<td>
<a href="compilelogs/plugins/org.eclipse.wst.xsd.ui_1.2.101.v200806051910/@dot.bin.html" type='text/plain' >org.eclipse.wst.xsd.ui</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.xsd.ui_1.2.101.v200806051910/@dot.bin.html#ERRORS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.xsd.ui_1.2.101.v200806051910/@dot.bin.html#OTHER_WARNINGS" type='text/plain' >3</a></td><td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.xsd.ui_1.2.101.v200806051910/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>
<td CLASS="numeric"><a href="compilelogs/plugins/org.eclipse.wst.xsd.ui_1.2.101.v200806051910/@dot.bin.html#ACCESSRULES_WARNINGS" type='text/plain' >0</a></td>

</tr>
<tr CLASS="extraWarningTable bold">
<td>TOTALS  (144)</td><td CLASS="numeric">0</td><td CLASS="numeric">487</td><td CLASS="numeric">40</td><td CLASS="numeric">446</td>
</tr>


</table>

</body>
</html>
