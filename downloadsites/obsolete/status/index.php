<?php
echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?> \n" ;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<link rel="stylesheet" href="http://dev.eclipse.org/default_style.css" />
<link rel="stylesheet" href="../commonPages/wtpDropStyle.css" />
<title>WTP Build Status</title>
</head>
<body>

<H1>WTP Build Status</h1>

<table border=1 cellpadding=5 cellspacing=0>
<tr>
<th>Stream</th><th>Build Mode</th><th>Code Release?</th><th>Test Status</th>
</tr>
<tr>
<th>3.0.x</th><td>Open</td><td><font color="green">Yes</font></td><td>Previous <a href="http://wiki.eclipse.org/WTP_Smoke_Test_Results_R304_010809">R304_010809</a></td>
</tr>

<tr>
<th>3.1</th><td>Open</td><td><font color="green">Yes</font></td><td>Previous: <a href="http://wiki.eclipse.org/WTP_Smoke_Test_Results_R31_010809">R31_010809</a> </td>

</tr>
</table>
<?php
putenv('TZ=America/New_York');
//echo "  Current time (Eastern): " . date("Y-m-d h:i",time()) . "<br />";
$PHP_SELF = $_SERVER['PHP_SELF'];
// echo "php self: " . $PHP_SELF . "<br />";
$file = basename($PHP_SELF);
//$updatetime=filemtime($file);
// echo "php self basename: " . $file . "<br />";
echo "<br />";
echo "Status updated (Eastern): " . date("Y-m-d h:i",filemtime($file)) . "<br />"; 
?>

</body>
</html>

