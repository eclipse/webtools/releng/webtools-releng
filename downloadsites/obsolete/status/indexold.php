<?php
echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?> \n" ;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<link rel="stylesheet" href="http://dev.eclipse.org/default_style.css" />
<link rel="stylesheet" href="../commonPages/wtpDropStyle.css" />
<title>WTP Build Status</title>
</head>
<body>

<H1>WTP Build Status</h1>

<?php
putenv('TZ=America/New_York');
echo date("Y-m-d h:i:sa",time());
?>

<h3>3.0.x</h3>
<table border=1 cellpadding=5 cellspacing=0>
<tr>
<td><b> Build Mode </b> </td><td> <b>Code Release? </b></td><td> <b>Test Status</b> </td>
</tr>
<tr>
<td>Open</td><td><font color="green">Yes</font></td><td>Previous <a href="http://wiki.eclipse.org/WTP_Smoke_Test_Results_R304_010809">R304_010809</a></td>
</tr>
</table>

<h3>3.1</h3>
<table border=1 cellpadding=5 cellspacing=0>
<tr>
<td><b> Build Mode</b> </td><td> <b> Code Release?</b></td><td> <b>Test Status</b> </td>
</tr>
<tr>
<td>Open</td><td><font color="green">Yes</font></td><td>Previous: <a href="http://wiki.eclipse.org/WTP_Smoke_Test_Results_R31_010809">R31_010809</a> </td>

</tr>
</table>




</body>
</html>

