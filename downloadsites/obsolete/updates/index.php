<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
    ini_set("display_errors", "true");
    error_reporting (E_ALL);

    #
    # Begin: page-specific settings.
    $pageTitle          = "Web Tools Platform Repository";
    $pageKeywords       = "Web Toosl Platform Repository Updates Software Install Eclipse";
    $pageAuthor         = "David Williams";
    $theme = "Nova";


    # Paste your HTML content between the markers!
    ob_start();
?>


<div id="maincontent" style="padding: 5em; width: 90%; margins: 5%;">



<?php 
    $xhtmlFile = 'indexHTML.html';
    // Load the source. Will treat as "raw" XHMTL for now
    //$doc = new DOMDocument;
    //$doc->loadHTMLFile($xhtmlFile);
    //echo $doc->saveHTML();
    echo file_get_contents($xhtmlFile);

    $current_dir=dirname(realpath($xhtmlFile));

    $lastSegment = basename(rtrim($current_dir, '/'));

    // this assumes the html text ends with something about "... the following URL ... "
    echo "<p>&nbsp;&nbsp;&nbsp;&nbsp;<b> <code>http://download.eclipse.org/webtools/" . $lastSegment . "</code></b></p>";

?>


</div>

<?php
    $html = ob_get_contents();
    ob_end_clean();

    # Generate the web page
    $App->generatePage($theme, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
