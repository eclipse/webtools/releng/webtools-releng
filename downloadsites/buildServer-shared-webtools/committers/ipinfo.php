<?php
echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?> \n" ;

ini_set("display_errors", "true");
error_reporting (E_ALL);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta
    http-equiv="Content-Type"
    content="text/html; charset=ISO-8859-1" />
<title>IP Address Information</title>
</head>
<body>
<?php
$yourIP = $_SERVER['REMOTE_ADDR'];
$hostname = gethostbyaddr($yourIP);
$ip = gethostbyname($hostname);
$ipsMatch = ($ip == $yourIP);

echo "Your IP: " . $yourIP . "<br />";
echo "Your hostname (reverse lookup): " . $hostname . "<br />";
echo "Your hostname's IP (forward lookup of your reverse lookup): " . $ip . "<br />";

if( $ipsMatch ) {
    echo "Your DNS seems okay!";
}
else {
    echo "Your DNS is broken. This may cause your connection to CVS to be slow!";
}

?>
</body>
</html>
