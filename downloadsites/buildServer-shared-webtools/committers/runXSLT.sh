#!/bin/sh
#*******************************************************************************
# Copyright (c) 2009, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# remember to leave no slashes on commonVariations in source command,
# so that users path is used to find it (first). But, path on
# commonComputedVariables means we expect to execute only our
# version

if [ -z $BUILD_INITIALIZED ]
then
    # if releng_control not set, we assume we are already in releng_control directory
    if [ -z $RELENG_CONTROL ] 
    then 
        RELENG_CONROL=`pwd`
    fi   
    pushd .
    cd ${RELENG_CONTROL}
    source commonVariations.shsource
    source ${RELENG_CONTROL}/commonComputedVariables.shsource
    popd
fi

# For most ant tasks, we want Java 4 to be default, 
# so if not desired (such as for WTP 2.0 unit tests), 
# then we have to spec Java 5 right there where we run 
# the tests.
# Note: this must be Java 4, for now, for "customizeAccessRules" 
# to work. I'll recompile it for Java 5 eventually, but it causes 
# a problem, and a problem only on PPC machine/VM. 
export JAVA_HOME=${JAVA_4_HOME}


ANT_CMD=${ANT_HOME}/bin/ant

exec "$ANT_CMD" -v -f transformReportsToHTML.xml


