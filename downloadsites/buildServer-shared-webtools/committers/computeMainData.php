<?php

function getSubDirectory($directoryName) {
	if (file_exists($directoryName) && is_dir($directoryName)) {
		//echo "Debug: anArtifactEntry (directory): " . $completeName . "<br />";
		$aDirectory = dir($directoryName);
		while (false !== ($anEntry = $aDirectory->read())) {
			if ($anEntry != "." && $anEntry!="..") {
				return $directoryName."/".$anEntry;
			}
		}
	}
}

function computeMainData($buildBranches, $debugScript)

{
	global $buckets, $latestFile, $latestTimeStamp, $timeStamps;

	foreach ($buildBranches as $buildBranch ) {

		if (file_exists($buildBranch) && is_dir($buildBranch)) {
			$aDirectory = dir($buildBranch);
			$latestTimeStamp[$buildBranch] = array();
			$latestFile[$buildBranch] = array();

			while (false !== ($anArtifactEntry = $aDirectory->read())) {
				//echo "anArtifactEntryDirectory: " . $anArtifactEntry . "<br />";

				// Short cut because we know aDirectory only contains other directories.
				if ($anArtifactEntry != "." && $anArtifactEntry!="..") {
					$completeName = $buildBranch."/".$anArtifactEntry;
					if (file_exists($completeName) && is_dir($completeName)) {
						//echo "Debug: anArtifactEntry (directory): " . $completeName . "<br />";
						$anArtifactEntryDirectory = dir($completeName);

						while (false !== ($anEntry = $anArtifactEntryDirectory->read())) {

							// Short cut because we know aDirectory only contains other directories.
							if ($anEntry != "." && $anEntry!="..") {

								//echo "Debug anEntry: $anEntry<br />" ;
								$aDropDirectoryName = getSubDirectory($completeName);
								//echo "Debug aDropDirectoryDirectoryName: $aDropDirectoryName <br />" ;


								if (is_dir($aDropDirectoryName) && is_Readable($aDropDirectoryName)) {
									$aDropDirectory = dir($aDropDirectoryName);
									//echo "Debug aDropDirectory: $aDropDirectory->path <br />" ;

									$fileCount = 0;
									while ($aDropEntry = $aDropDirectory->read()) {
										//echo "Debug aDropEntry: $aDropEntry<br />" ;
										if ( (stristr($aDropEntry, ".tar.gz")) || (stristr($aDropEntry, ".zip")) )  {
											// Count the dropfile entry in the directory (so we won't display links, if not all there
											$fileCount = $fileCount + 1;
										}
									}

									$aDropDirectory->close();

								}
								// Read the count file
								$countFile = $aDropDirectoryName."/files.count";
								$indexFile = $aDropDirectoryName."/index.html";


								if (!file_exists($indexFile)) {
									$indexFile = $aDropDirectoryName."/index.php";
								}


								if (file_exists($countFile) && file_exists($indexFile)) {
									$anArray = file($countFile);
									// echo "Debug: Number according to files.count: ", $anArray[0], "<br />";
									// echo "Debug:    actual counted files: ", $fileCount, "<br />";

									// If a match - process the directoryVV -- we simply look that there's more
									// zip's than we expect, since it frequently breaks where the count is slighly
									// off, such as when we add, after the fact, an all-in-one zip.
									if ($anArray[0] <= $fileCount) {
										//echo "Debug: yes, counted equaled expected count<br />";
										$anEntryLine = explode("/", $aDropDirectoryName);
										$artifactTimeStamp = $anEntryLine[1]."/";
										$entryParts = explode("-", $anEntryLine[2]);
										if (count($entryParts) == 3) {
											include '../commonPages/processDropDirectory.php';
										}
									}
								}

							}
						}
						$anArtifactEntryDirectory->close();
					}

				}
			}
			$aDirectory->close();
		}
	}
}
?>
