<?php
    echo "Your IP: " . $_SERVER['REMOTE_ADDR'] . "<br />";
    $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
    echo "Your hostname (reverse lookup): " . $hostname . "<br />";
    $ip = gethostbyname($hostname);
    echo "Your hostname's IP (forward lookup of your reverse lookup): " . $ip . "<br />";
    
    if($ip != $_SERVER['REMOTE_ADDR']) {
        echo "<font size=\"+4\" color=\"red\">Your DNS is broken. This may cause your connection to CVS to be slow!</font>";
    }
    else {
        echo "<font size=\"+2\" color=\"green\">Your DNS seems okay! Thanks!</font>";
    }
    
?>