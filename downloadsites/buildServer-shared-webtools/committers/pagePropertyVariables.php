<?php

$pageTitle="Eclipse Web Tools Platform (WTP) Downloads - for Committers and
Early Testers";
$indexTop="<font class=indextop>
Web Tools Platform<br />
committer downloads</font><br />
<font class=indexsub>Latest early trial downloads from the Web Tools Platform project</font>";

$pageExplanation="
    <p>This is the starting page for where you can find the latest <a
      href=\"http://wiki.eclipse.org/index.php/WTP_Build_Types\">continuous build</a> produced by the <a
      href=\"http://www.eclipse.org/webtools/main.php\">Eclipse Web Tools
    Platform (WTP) Project</a>. Please note that each build page details
    the pre-reqs for that particular build.</p>

    <p>If you got here to this continuous-build site by accident or casual
    browsing, please be aware that <a
      href=\"http://download.eclipse.org/webtools/downloads/\" target=\"_top\">
    declared builds</a> are available!</p>

";

$mainTableHeader="Latest Downloads (In progress, towards a declared build)";

$pageFooterEnd="<p><a href=\"http://download.eclipse.org/webtools/downloads/\"
              target=\"_top\">Declared builds</a> are available which are for
            end-users and adopters.</p>"; 

$subsectionHeading="Recent Builds";

?>