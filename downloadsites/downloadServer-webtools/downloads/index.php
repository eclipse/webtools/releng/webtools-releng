<?php
echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?> \n" ;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<?php
include 'pagePropertyVariables.php';
?>

<link rel="stylesheet" href="http://dev.eclipse.org/default_style.css" />
<link rel="stylesheet" href="../commonPages/wtpDropStyle.css" />
<title><?php echo $pageTitle; ?></title>
</head>
<body>


<?php

// tiny banner to remind when looking at "local" machine results
$serverName = $_SERVER["SERVER_NAME"];

if (!stristr($serverName, "eclipse.org") && !stristr($serverName,"you.are.at.eclipsecon.org")) {
    echo '<center>
          <p>
          Reminder: this is <font color="#FF0000">' . 
    $serverName .
    '</font>
          See also 
          <a href="http://download.eclipse.org/webtools/downloads" target="_top">
          the live public Eclipse site
          </a>.
          </p>
          <hr />
          </center>';

}

if (function_exists("date_default_timezone_set")) {
    date_default_timezone_set("UTC");
    //echo "<p>default timezone: ";
    //echo date_default_timezone_get();
    //echo "</p>";
}

?>


<?php
ini_set("display_errors", "true");
error_reporting (E_ALL);
?>

<?php
$QString = $_SERVER['QUERY_STRING'];
$C = strcmp($QString, "test");
?>


<?php
include 'buildbranches.php';
include '../commonPages/computeMainData.php';
include '../commonPages/topAndInit.php';
include '../commonPages/latestBuilds.php';
include '../commonPages/recentHistory.php';
include '../commonPages/bottomAndFooter.php';
?>


</body>
</html>

