<?php

    $pageTitle="Eclipse Web Tools Platform (WTP) Downloads";
    $indexTop="<font class=indextop>
    Web Tools Platform<br />
    downloads</font><br />
    <font class=indexsub>Latest downloads from the Web Tools Platform project</font>";

    $pageExplanation="
    <p>This is the starting page for where you can find the latest <a
    href=\"http://wiki.eclipse.org/index.php/WTP_Build_Types\">declared build</a> produced by the <a
    href=\"http://www.eclipse.org/webtools/\">Eclipse Web Tools
    Platform (WTP) Project</a>.</p>

    <p>As an alternative to downloading zips from the build pages, our
    released builds can be <a
    href=\"http://download.eclipse.org/webtools/updates/\">installed from our 
    software repository site</a>, from an existing installation of Eclipse.</p>

    <p>Please note that all-in-one packages can be downloaded from 
    the <a href=\"http://www.eclipse.org/downloads/\">main Eclipse download site</a>. This 
    is usually the easiest way to get started quickly with web development.</p>
    
    <p>Also, for most WTP deliverables, 
    the main Eclipse release software repositories (such as 
    for the <a href=\"http://download.eclipse.org/releases/galileo/\">Eclipse Galileo Release</a>, or the 
    <a href=\"http://download.eclipse.org/releases/helios/\">Eclipse Helios Release</a>) 
    usually has all you need and usually easiest to use that 
    repository, unless there is something specific you need that you know is only in the webtools 
    specific repository. </p>
    ";

    $mainTableHeader="Latest Downloads";

    $pageFooterEnd="<p><a href=\"http://build.eclipse.org/webtools/committers/\"
    target=\"_top\">Continuous builds</a> are also available which are for
    committers and early testers.</p>"; 

    $subsectionHeading="Recent History";

?>
