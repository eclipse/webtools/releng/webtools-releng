<?php

$pageTitle="Eclipse Web Tools Platform (WTP) Patch Downloads";
$indexTop="<font class=indextop>
 Web Tools Platform<br />
patch downloads</font><br />
<font class=indexsub>Latest patch downloads from the Web Tools Platform project</font>";

$pageExplanation="
    <p>This is the starting page for where you can find the latest <a
      href=\"http://wiki.eclipse.org/index.php/WTP_Build_Types\">patch features</a> produced by the <a
      href=\"http://www.eclipse.org/webtools\">Eclipse Web Tools
    Platform (WTP) Project</a>.</p>
    <p>They are normaly very specialized, for a specific purpose or adopter, and not for 
    general use. Those that are for general use -- and end-users -- will be made available 
    via update manager.
    </p>
    <p>Also, please note, the builds and zip files here may appear and disappear while patches
    are being investigated, tested, etc, so if it is relatively recent, it may not be final. 
    If there are any specific questions, feel free to ask on <a href=\"mailto:wtp-releng@eclipse.org\">wtp-releng</a>.</p> 
    <p>If you got here to this patch-build site by accident or casual
    browsing, please be aware that <a
      href=\"http://download.eclipse.org/webtools/downloads/\" target=\"_top\">
    declared builds</a> are available!</p>
";

$mainTableHeader="Latest Patch Feature Downloads";

$pageFooterEnd="<p><a href=\"http://download.eclipse.org/webtools/committers/\"
              target=\"_top\">Continuous builds</a> are also available which are for
            committers and early testers.</p>"; 

$subsectionHeading="History of Patch Builds";
?>