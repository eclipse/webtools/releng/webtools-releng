#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# This script file is to help get fresh files from cvs

if [[ !("${1}" == "patches" || "${1}" == "downloads" || "${1}" == "committers") ]] ; then
   echo ""
   echo "    Usage: ${0} patches | downloads | committers"
   echo ""
else

subdir="${1}"

backupdir="${subdir}TempBackup"

echo "  save backup copies ..."
mkdir $backupdir
# Note: do not use recurvise, since that would copy all of 'drops'
cp ${subdir}/* $backupdir

rm ${subdir}/*
echo "  checking out head of $subdir from cvs ..."
cvs -Q -d :pserver:anonymous@dev.eclipse.org:/cvsroot/webtools export -d $subdir -r HEAD releng.wtptools/downloadsites/webtools/$subdir 

echo "  make sure files have proper EOL format ..."
dos2unix -quiet -keepdate ${subdir}/* > /dev/null 2>/dev/null

fi
