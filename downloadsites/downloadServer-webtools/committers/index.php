<?php
echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?> \n" ;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link
    rel="stylesheet"
    href="http://dev.eclipse.org/default_style.css" />
<link
    rel="stylesheet"
    href="../commonPages/wtpDropStyle.css" />
<title>Committers Download Page has Moved</title>
<meta
    http-equiv="content-type"
    content="text/html;charset=iso-8859-1" />
<meta
    http-equiv="refresh"
    content="30; URL=http://build.eclipse.org/webtools/committers/" />
</head>
<body>
<p>The <a href="http://build.eclipse.org/webtools/committers/">Committers continuous builds download page</a> has moved.
</p>
<p>Please update your book marks and report broken links. You should automatically be redirected to the <a
    href="http://build.eclipse.org/webtools/committers/">new page</a> in a few seconds.</p>
</body>
</html>
