<?php
echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?> \n" ;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include 'pagePropertyVariables.php';
?>
<link
    rel="stylesheet"
    href="http://dev.eclipse.org/default_style.css" />
<title><?php echo $pageTitle; ?></title>
</head>
<body>
<?php

// tiny banner to remind when looking at "local" machine results
$serverName = $_SERVER["SERVER_NAME"];

if (!stristr($serverName, "eclipse.org") && !stristr($serverName,"you.are.at.eclipsecon.org")) {
    echo '<center>
          <p>
          Reminder: this is <font color="#FF0000">' . 
    $serverName .
    '</font>
          See also 
          <a href="http://download.eclipse.org/webtools/downloads" target="_top">
          the live public Eclipse site
          </a>.
          </p>
          <hr />
          </center>';

}

if (function_exists("date_default_timezone_set")) {
    date_default_timezone_set("UTC");
    //echo "<p>default timezone: ";
    //echo date_default_timezone_get();
    //echo "</p>";
}

?>
<?php
ini_set("display_errors", "true");
error_reporting (E_ALL);
?>
<?php
$QString = $_SERVER['QUERY_STRING'];
$C = strcmp($QString, "test");
?>
<table
    border=0
    cellpadding=2
    width="100%">
    <tr>
        <td width="72%"><font class=indextop> Web Tools Platform<br>
        archived downloads</font>
        </td>
        <td width="28%"><img
            src="wtplogonarrow.jpg"
            height=95
            width=206></td>
    </tr>
</table>
<!-- heading end -->
<hr />
<table
    border=0
    cellpadding=2
    width="100%">
    <tr>
        <td>
        <p>On this page you can find the <b>archived builds</b> produced by the <a
            href="http://www.eclipse.org/webtools/"
            target="_top"> Eclipse Webtools Project (WTP)</a>. Archived builds consist of older releases and are not
        propagated to Eclipse mirrors.</p>
        <p>If you got here to this <b>archive</b> site by accident or casual browsing, please be aware that more <a
            href="http://download.eclipse.org/webtools/downloads/"
            target="_top"> current builds</a> are available!</p>
        </td>
    </tr>
</table>
<?php

//$fileHandle = fopen("drops/dlconfig.txt", "r");
//while (!feof($fileHandle)) {
//
//       $aLine = fgets($fileHandle, 4096);
//        parse_str($aLine);
//
//
//}
//
//fclose($fileHandle);

// fix for the next version of php

$contents = substr(file_get_contents('drops/dlconfig.txt'),0,-1);
$contents = str_replace("\n", "", $contents);

#split the content file by & and fill the arrays
$elements = explode("&",$contents);
$t = 0;
$p = 0;
$tString = "dropType";
$pString = "dropPrefix";
$dropType = array();
$dropPrefix = array();

for ($c = 0; $c < count($elements)-1; $c++) {
    if (!strstr($elements[$c],"#")) {
        if (strstr($elements[$c],$tString)) {
            $temp = preg_split("/=/",$elements[$c]);
            $dropType[$t] = trim($temp[1]);
            $t++;
        }
        if (strstr($elements[$c],$pString)) {
            $temp = preg_split("/=/",$elements[$c]);
            $dropPrefix[$p] = trim($temp[1]);
            $p++;
        }
    }
}

// debug
// echo "Debug: droptype count: ", count($dropType), "<br />";
$typeToPrefix = array();
for ($i = 0; $i < count($dropType); $i++) {
    $dt = $dropType[$i];
    $dt = trim($dt);
    if (array_key_exists($i, $dropPrefix)) {
        $typeToPrefix[$dt] = $dropPrefix[$i];
    }

    // echo "Debug prefix: ", $dropPrefix[$i], "<br />";
    // echo "Debug dropType: ", $dropType[$i], "<br />";

}

$buildBranches = array();
$buildBranches[]="R3.2.3";
$buildBranches[]="R3.2.2";
$buildBranches[]="R3.2.1";
$buildBranches[]="R3.2.0";
$buildBranches[]="R3.1.2";
$buildBranches[]="R3.1";
$buildBranches[]="R3.0";
$buildBranches[]="R2.1";
$buildBranches[]="R2.0";
$buildBranches[]="R1.5";
$buildBranches[]="R1.0";
$buildBranches[]="R0.7";
$buildBranches[]="R0.5";
include 'drops/report.php';


foreach ($buildBranches as $buildBranch ) {

    $dirfilename = "drops/".$buildBranch;
    if (file_exists($dirfilename) && is_dir($dirfilename)) {
        $aDirectory = dir($dirfilename);

        while (false !== ($anEntry = $aDirectory->read())) {

            // Short cut because we know aDirectory only contains other directories.
            if ($anEntry != "." && $anEntry!=".." ) {
                //echo "Debug anEntry: $anEntry<br />" ;
                $aDropDirectoryName = "drops/".$buildBranch."/".$anEntry;
                if (is_Readable($aDropDirectoryName)) {
                    $aDropDirectory = dir($aDropDirectoryName);
                    //echo "Debug aDropDirectory: $aDropDirectory->path <br />" ;

                    $fileCount = 0;
                    while ($aDropEntry = $aDropDirectory->read()) {
                        // echo "Debug aDropEntry: $aDropEntry<br />" ;
                        if ( (stristr($aDropEntry, ".tar.gz")) || (stristr($aDropEntry, ".zip")) )  {
                            // Count the dropfile entry in the directory (so we won't display links, if not all there
                            $fileCount = $fileCount + 1;
                        }
                    }

                    $aDropDirectory->close();
                }
                // Read the count file
                $countFile = "drops/".$buildBranch."/".$anEntry."/files.count";
                $indexFile = "drops/".$buildBranch."/".$anEntry."/index.html";


                if (!file_exists($indexFile)) {
                    $indexFile = "drops/".$buildBranch."/".$anEntry."/index.php";
                }


                if (file_exists($countFile) && file_exists($indexFile)) {
                    $anArray = file($countFile);
                    // debug
                    //echo "Number according to files.count: ", $anArray[0];
                    //echo "   actual counted files: ", $fileCount;

                    // If a match - process the directoryVV
                    if ($anArray[0] <= $fileCount) {
                        // debug
                        //echo "yes, counted equaled expected count<br>";

                        $parts = explode("-", $anEntry);
                        if (count($parts) == 3) {
                            // debug
                            //echo "yes, counted parts was 3<br>";
                            $buckets[$buildBranch][$parts[0]][] = $anEntry;

                            $timePart = $parts[2];
                            $year = substr($timePart, 0, 4);
                            $month = substr($timePart, 4, 2);
                            $day = substr($timePart, 6, 2);
                            $hour = substr($timePart,8,2);
                            $minute = substr($timePart,10,2);
                            $timeStamp = mktime($hour, $minute, 0, $month, $day, $year);

                            $timeStamps[$anEntry] = date("D, j M Y -- H:i \(\U\T\C\)", $timeStamp);

                        }

                    }
                }

            }
        }

        $aDirectory->close();
    }
}


foreach($dropType as $value) {
    if (key_exists($value, $typeToPrefix)) {
        $prefix=$typeToPrefix[$value];
    } 
    else {
        $prefix = "none";
    }


    echo "
    <table width=\"100%\" cellpadding=2>
    <tr bgcolor=\"#999999\">
    <td align=left width=\"30%\"><b><a name=\"$value\"><font color=\"#FFFFFF\" face=\"Arial,Helvetica\">$value";
    echo "</font></a></b></td>
                         </TR>
                         <TR>
                         <td align=left>
                         <TABLE  width=\"100%\" cellpadding=\"2\">
                         <tr>
                         <td width=\"30%\"><b>Build Name</b></td><td><b>Build Stream</b></td>
                         <td><b>Build Date</b></td>
                         </tr>";

    foreach($buildBranches as $bValue) {
        if (array_key_exists($bValue,$buckets) && array_key_exists($prefix, $buckets[$bValue])) {
            echo "<tr><td colspan=\"7\"/><hr/></tr>";
            $aBucket = $buckets[$bValue][$prefix];
            if (isset($aBucket)) {
                rsort($aBucket);

                $i = 0;
                $ts = array();
                $ts2iv = array();
                foreach($aBucket as $iv) {
                    $parts = explode("-", $iv);
                    $ts[$i] = $parts[2];
                    $ts2iv[$ts[$i]] = $iv;
                    $i++;
                }

                rsort($ts);
                $i = 0;
                $aBucket = array();
                foreach($ts as $tsvalue) {
                    $aBucket[$i] = $ts2iv[$tsvalue];
                    $i++;
                }

                foreach($aBucket as $innerValue) {
                    $parts = explode("-", $innerValue);
                    echo "<tr>";

                    // Uncomment the line below if we need click through licenses.
                    // echo "<td><a href=\"license.php?license=drops/$bValue/$innerValue\">$parts[1]</a></td>";

                    // Comment the line below if we need click through licenses.
                    echo "<td><a href=\"drops/$bValue/$innerValue/\">$parts[1]</a></td>";
                    echo "<td>$bValue</td>";
                    echo "<td>$timeStamps[$innerValue]</td>";

                }
            }}}
            echo "</table></table>";
}
?>
<!-- footer -->
<center>
<hr />
<p>All downloads are provided under the terms and conditions of the <a href="http://www.eclipse.org/legal/notice.html">Eclipse.org
Software User Agreement</a> unless otherwise specified.</p>
<p>If you have problems downloading the drops, contact the <font
    size="-1"
    face="arial,helvetica,geneva"><a href="mailto:webmaster@eclipse.org">webmaster</a></font>.</p>
</center>
<!-- end footer -->
</body>
</html>
