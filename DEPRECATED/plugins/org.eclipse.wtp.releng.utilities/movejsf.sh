#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

. mainparams.shsource
. copyAndRemove.sh

echo;
echo "   performing $0";
echo;

# be sure these are done before jee
newDir=${oldcvsdir}/org.eclipse.jsf/components/jsf/features
oldDir=${oldcvsdir}/jst/components/web/features/org.eclipse.jst.webpageeditor.feature
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/web/features/org.eclipse.jst.webpageeditor_sdk.feature
copyAndRemove $oldDir $newDir