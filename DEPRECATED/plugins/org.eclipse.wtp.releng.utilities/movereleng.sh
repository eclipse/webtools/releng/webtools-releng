#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

. mainparams.shsource
. copyAndRemove.sh

echo;
echo "   performing $0";
echo;

newDir=${newcvsdir}/webtools.releng
oldDir=${oldcvsdir}/wst/components/assembly
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/jst/components/assembly
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/org.eclipse.wtp.releng
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/org.eclipse.wtp.releng.tests
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/org.eclipse.wtp.releng.webupdatesite
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/releng.builder
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/releng.control
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/releng.wtpbuilder
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/releng.wtptools
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/working
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/doc/features/org.eclipse.wst.doc.isv.feature
newDir=${newcvsdir}/webtools.releng/features
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/doc/features/org.eclipse.wst.doc.user.feature
newDir=${newcvsdir}/webtools.releng/features
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/wst/components/doc/plugins
newBaseDir=${newcvsdir}/webtools.releng/docs

wtpmodule=org.eclipse.wst.doc.isv
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.webtools.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/org.eclipse.wtp
newBaseDir=${newcvsdir}/webtools.releng/tests
wtpmodule=org.eclipse.wtp.releng.tests
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

newBaseDir=${newcvsdir}/webtools.releng/features
wtpmodule=org.eclipse.wtp.releng.tests.feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/org.eclipse.wtp
newBaseDir=${newcvsdir}/webtools.releng/plugins
wtpmodule=org.eclipse.wtp.releng.utilities
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

