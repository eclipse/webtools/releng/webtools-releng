#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

. mainparams.shsource
. copyAndRemove.sh

echo;
echo "   performing $0";
echo;

oldDir=${oldcvsdir}/wst/components/xml/thirdparty
newDir=${newcvsdir}/sourceediting/archive
copyAndRemove $oldDir $newDir

newDir=${newcvsdir}/sourceediting/docs
oldDir=${oldcvsdir}/wst/components/jsdt/plugins/org.eclipse.wst.jsdt.doc
copyAndRemove $oldDir $newDir

newDir=${newcvsdir}/sourceediting/archive
oldDir=${oldcvsdir}/wst/components/xsd/plugins/org.eclipse.wst.xsd.contentmodel
copyAndRemove $oldDir $newDir

newDir=${newcvsdir}/sourceediting

# remember final slash needed here to include all, but correctly flatten
oldDir=${oldcvsdir}/wst/components/sse/
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/wst/components/xml/
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/wst/components/xsd/
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/wst/components/dtd/
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/wst/components/javascript/
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/wst/components/jsdt/
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/wst/components/css/
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/wst/components/html/
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/jst/components/jsp/
copyAndRemove $oldDir $newDir


oldBaseDir=${oldcvsdir}/wst/components/web/plugins
newBaseDir=${newcvsdir}/sourceediting/plugins
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.wst.standard.schemas

oldBaseDir=${oldcvsdir}/jst/components/j2ee/plugins
newBaseDir=${newcvsdir}/sourceediting/plugins
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.standard.schemas


oldBaseDir=${oldcvsdir}/wst/components/doc/plugins
newBaseDir=${newcvsdir}/sourceediting/docs

wtpmodule=org.eclipse.wst.dtdeditor.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.dtd.ui.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.html.ui.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.javascript.ui.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.sse.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.sse.ui.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.xmleditor.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.xml.ui.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.xsdeditor.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/doc/plugins
newBaseDir=${newcvsdir}/sourceediting/docs
wtpmodule=org.eclipse.jst.jsp.ui.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir
