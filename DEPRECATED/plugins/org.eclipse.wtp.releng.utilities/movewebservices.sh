#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

. mainparams.shsource
. copyAndRemove.sh

echo;
echo "   performing $0";
echo;

newDir=${newcvsdir}/webservices/archive
oldDir=${oldcvsdir}/wst/components/ws/thirdparty
copyAndRemove $oldDir $newDir
newDir=${newcvsdir}/webservices/archive
oldDir=${oldcvsdir}/wst/components/wsdl/thirdparty
copyAndRemove $oldDir $newDir

#remember, trailing slash is 
#required here, to include all, but to still flatten
newDir=${newcvsdir}/webservices
oldDir=${oldcvsdir}/wst/components/ws/
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/wst/components/wsdl/
copyAndRemove $oldDir $newDir
oldDir=${oldcvsdir}/wst/components/wsi/
copyAndRemove $oldDir $newDir


oldBaseDir=${oldcvsdir}/wst/components/doc/plugins
newBaseDir=${newcvsdir}/webservices/docs

wtpmodule=org.eclipse.wst.wsdl.ui.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.ws.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.wsi.ui.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/doc/plugins
newBaseDir=${newcvsdir}/webservices/docs
wtpmodule=org.eclipse.jst.ws.axis.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.ws.axis.ui.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.ws.consumption.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.ws.consumption.ui.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.ws.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.ws.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/ws/plugins
newBaseDir=${newcvsdir}/webservices/plugins

copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis2.consumption.core
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis2.consumption.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis2.core
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis2.creation.core
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis2.creation.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis2.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis.consumption.core
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis.consumption.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis.creation.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.consumption
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.consumption.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.creation.ejb.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.creation.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.uddiregistry
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.ui

newBaseDir=${newcvsdir}/webservices/archive
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis.ant


oldBaseDir=${oldcvsdir}/jst/components/ws/tests
newBaseDir=${newcvsdir}/webservices/tests

copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.axis.consumption.core.tests
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.tests
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.ws.tests.performance




