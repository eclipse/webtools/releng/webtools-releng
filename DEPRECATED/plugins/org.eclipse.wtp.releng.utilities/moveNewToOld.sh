#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# this file is to move the "new" temporary created directories, 
# back to the main 'webtools' cvs location. 

. mainparams.shsource

. copyAndRemove.sh

echo;
echo "   performing $0";
echo;


# don't need cvsroot

rm -fr ${newcvsdir}/CVSROOT
mv ${newcvsdir}/* ${oldcvsdir}
 
find ${oldcvsdir}/ -type d -exec chmod g+s '{}' \;
