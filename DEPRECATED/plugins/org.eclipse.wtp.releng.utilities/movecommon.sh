#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

. mainparams.shsource

. copyAndRemove.sh

echo;
echo "   performing $0";
echo;

oldDir=${oldcvsdir}/wst/components/internet/plugins/org.eclipse.wst.internet.proxy
newDir=${newcvsdir}/common/archive
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/doc/plugins/org.eclipse.wst.internet.proxy.infopop
newDir=${newcvsdir}/common/archive
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/common/features/org.eclipse.jst.common_core.feature
newDir=${newcvsdir}/common/features
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/common/features/org.eclipse.jst.common_sdk.feature
newDir=${newcvsdir}/common/features
copyAndRemove $oldDir $newDir


oldBaseDir=${oldcvsdir}/wst/components/common
newBaseDir=${newcvsdir}/common
wtpmodule=features
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir


oldDir=${oldcvsdir}/jst/components/jem/org.eclipse.jem.util
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/common/plugins/org.eclipse.jst.common.project.facet.core
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/command/plugins/org.eclipse.wst.command.env
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/command/plugins/org.eclipse.wst.command.env.core
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/doc/plugins/org.eclipse.wst.command.env.infopop
newDir=${newcvsdir}/common/docs
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/command/plugins/org.eclipse.wst.command.env.ui
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/command/tests/org.eclipse.wst.command.env.tests
newDir=${newcvsdir}/common/archive
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.core
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.emf
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.emfworkbench.integration
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.environment
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.frameworks
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.frameworks.ui
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/doc/plugins/org.eclipse.wst.common.infopop
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.modulecore
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.project.facet.core
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.project.facet.ui
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.snippets
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.ui
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.ui.properties
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.uriresolver
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/internet/plugins/org.eclipse.wst.internet.cache
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/validation/plugins/org.eclipse.wst.validation
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/doc/plugins/org.eclipse.wst.validation.infopop
newDir=${newcvsdir}/common/docs
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/validation/plugins/org.eclipse.wst.validation.ui
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.explorer
newDir=${newcvsdir}/common/archive
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.navigator.views
newDir=${newcvsdir}/common/archive
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.navigator.workbench
newDir=${newcvsdir}/common/archive
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/plugins/org.eclipse.wst.common.runtime.core
newDir=${newcvsdir}/common/plugins
copyAndRemove $oldDir $newDir


oldDir=${oldcvsdir}/wst/components/common/tests/org.eclipse.wst.common.project.facet.core.tests
newDir=${newcvsdir}/common/tests
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/tests/org.eclipse.wst.common.project.facet.ui.tests
newDir=${newcvsdir}/common/tests
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/tests/org.eclipse.wst.common.snippets.tests
newDir=${newcvsdir}/common/tests
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/tests/org.eclipse.wst.common.tests
newDir=${newcvsdir}/common/tests
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/tests/org.eclipse.wst.common.tests.collector
newDir=${newcvsdir}/common/tests
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/performance-tests/org.eclipse.wst.common.tests.performance
newDir=${newcvsdir}/common/tests
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/common/tests/org.eclipse.wst.common.tests.ui
newDir=${newcvsdir}/common/tests
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/internet/tests/org.eclipse.wst.internet.cache.tests
newDir=${newcvsdir}/common/tests
copyAndRemove $oldDir $newDir


oldBaseDir=${oldcvsdir}/wst/components/doc/plugins
newBaseDir=${newcvsdir}/common/docs

wtpmodule=org.eclipse.wst.command.env.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir


oldBaseDir=${oldcvsdir}/wst/components/common
newBaseDir=${newcvsdir}/common

wtpmodule=development
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir





