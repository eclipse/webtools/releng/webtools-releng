#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

. mainparams.shsource
. copyAndRemove.sh

echo;
echo "   performing $0";
echo;

newDir=${newcvsdir}/servertools

# need trailing slash to flatten
oldDir=${oldcvsdir}/wst/components/server/
copyAndRemove $oldDir $newDir

# need trailing slash to flatten
oldDir=${oldcvsdir}/jst/components/server/
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/wst/components/internet/plugins
newBaseDir=${newcvsdir}/servertools/plugins

wtpmodule=org.eclipse.wst.internet.monitor.core
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.internet.monitor.ui
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir


oldBaseDir=${oldcvsdir}/wst/components/internet/tests
newBaseDir=${newcvsdir}/servertools/tests

wtpmodule=org.eclipse.wst.internet.monitor.core.tests
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.internet.monitor.ui.tests
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir






oldBaseDir=${oldcvsdir}/wst/components/doc/plugins
newBaseDir=${newcvsdir}/servertools/docs

wtpmodule=org.eclipse.wst.server.ui.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.server.ui.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/doc/plugins
newBaseDir=${newcvsdir}/servertools/docs
wtpmodule=org.eclipse.jst.server.ui.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.server.ui.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir
