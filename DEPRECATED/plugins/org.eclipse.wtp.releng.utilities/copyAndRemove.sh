#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

copyAndRemove ()
{
    oldDir=${1}
    newDir=${2}
    if [ ! -d ${oldDir} ] 
    then 
        echo "${oldDir} does not exist"
        return;
    fi
    
    echo "oldDir: ${oldDir}"
    echo "newDir: ${newDir}"
    
    mkdir -p ${newDir}
    rsync --backup --recursive ${oldDir} ${newDir}
    if [ "$?" -eq "0" ] 
    then 
        rm -fr ${oldDir}
    fi
    return;
}

copyAndRemoveModule () {


oldBaseDir=${1}
newBaseDir=${2}
wtpmodule=${3}

oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir
return;
}