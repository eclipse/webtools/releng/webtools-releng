#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************


. mainparams.shsource

treeoutput=www/webtools/development/treeout
mkdir -p ${treeoutput}
wtpproject=common
tree --noreport --nolinks -d -T ${wtpproject} ${oldcvsdir}/${wtpproject} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html
wtpproject=datatools
tree --noreport --nolinks -d -T ${wtpproject} ${oldcvsdir}/${wtpproject} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html
wtpproject=servertools
tree --noreport --nolinks -d -T ${wtpproject} ${oldcvsdir}/${wtpproject} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html
wtpproject=webservices
tree --noreport --nolinks -d -T ${wtpproject} ${oldcvsdir}/${wtpproject} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html
wtpproject=sourceediting
tree --noreport --nolinks -d -T ${wtpproject} ${oldcvsdir}/${wtpproject} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html
wtpproject=ejbtools
tree --noreport --nolinks -d -T ${wtpproject} ${oldcvsdir}/${wtpproject} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html
wtpproject=jeetools
tree --noreport --nolinks -d -T ${wtpproject} ${oldcvsdir}/${wtpproject} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html
wtpproject=webtools.releng
tree --noreport --nolinks -d -T ${wtpproject} ${oldcvsdir}/${wtpproject} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html
wtpproject=webtools.maps
tree --noreport --nolinks -d -T ${wtpproject} ${oldcvsdir}/${wtpproject} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html


wtpproject=org.eclipse.atf
topDirectory=${wtpproject}/components
tree --noreport --nolinks -d -T ${topDirectory} ${oldcvsdir}/${topDirectory} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html

wtpproject=org.eclipse.jpa
topDirectory=${wtpproject}/components/jpa
tree --noreport --nolinks -d -T ${topDirectory} ${oldcvsdir}/${topDirectory} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html

wtpproject=org.eclipse.jsf
topDirectory=${wtpproject}/components/jsf
tree --noreport --nolinks -d -T ${topDirectory} ${oldcvsdir}/${topDirectory} -H ./ -L 2 -o ${treeoutput}/${wtpproject}.html
