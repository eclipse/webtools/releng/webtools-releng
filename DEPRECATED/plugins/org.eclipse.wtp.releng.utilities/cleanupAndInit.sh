#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************


. mainparams.shsource

. copyAndRemove.sh

echo;
echo "Remember, this script should NOT be ran on the production ";
echo "machine, during the real move. It is for testing only, ";
echo "when using snapshots.";
echo;

snapshot=/home/davidw/files/snapshots/webtools-cvs-7.tgz

mkdir -p ${topdir}/${reponame}
rm -fr ${topdir}/${reponame}
cvs --no-verify -d ${topdir}/${reponame} init

echo "   Current snapshot: $snapshot";
echo;
tar zxf $snapshot -C ${topdir}




