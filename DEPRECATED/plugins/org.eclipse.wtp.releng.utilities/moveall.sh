#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# These scripts pretty much have to be ran in this order, to work correctly, 
# since some copy and remove whole sub-trees, where as others copy and remove 
# individual pieces of those subtrees

echo;
echo "   performing $0";
echo;

./initNew.sh
./movejsf.sh
./moverdb.sh
./movecommon.sh
./moveservertools.sh
./movewebservices.sh
./movesourceediting.sh
./movereleng.sh
./movemaps.sh
./moveejb.sh
./movejee.sh
./moveCleanup.sh
./moveNewToOld.sh
