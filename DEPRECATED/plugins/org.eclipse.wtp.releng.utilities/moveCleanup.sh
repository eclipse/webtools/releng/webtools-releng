#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# this file is to run last, and just "hits" all major directories in old structure. 
# There actually should be nothing copies here, unless it's some stray file such as 
# a .project file under a directory ... but, just wanted to be complete. 
# plus, it has the pleasent side effect of removing all directories, just for easy 
# confirmation that all has been accounted for. It has to be ran last, naturally 
# (or else it would simply copy everything to tempmisc).

. mainparams.shsource

. copyAndRemove.sh

echo;
echo "   performing $0";
echo;

# remove misc files
find ${oldcvsdir}/wst -name .keepme -exec rm '{}' \; 
find ${oldcvsdir}/jst -name .keepme -exec rm '{}' \; 
find ${oldcvsdir}/wst -name .keepme,v -exec rm -f '{}' \; 
find ${oldcvsdir}/jst -name .keepme,v -exec rm -f '{}' \; 

find ${oldcvsdir}/wst -name .project -exec rm '{}' \; 
find ${oldcvsdir}/jst -name .project -exec rm '{}' \; 
find ${oldcvsdir}/wst -name .project,v -exec rm -f '{}' \; 
find ${oldcvsdir}/jst -name .project,v -exec rm -f '{}' \; 

find ${oldcvsdir}/wst -depth -type d -empty -exec rmdir {} \;
find ${oldcvsdir}/jst -depth -type d -empty -exec rmdir {} \;

newDir=${newcvsdir}/webtools.releng/tempmisc



oldDir=${oldcvsdir}/wst/components
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/org.eclipse.wtp
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst
copyAndRemove $oldDir $newDir

