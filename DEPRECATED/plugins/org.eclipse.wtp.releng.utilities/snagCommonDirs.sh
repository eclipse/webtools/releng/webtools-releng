#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

. mainparams.shsource
. copyAndRemove.sh

echo;
echo "   performing $0";
echo;

# This file is not needed on a regular basis, but was used at one point to get the 
# full patch of module names

# first one creates new oldList file, the rest append to it. 
find ${oldcvsdir} -name org.eclipse.wst.internet.proxy > oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.internet.proxy.infopop >> oldList.txt
find ${oldcvsdir} -name org.eclipse.jst.common_core.feature >> oldList.txt
find ${oldcvsdir} -name org.eclipse.jst.common_sdk.feature >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common_core.feature >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common_sdk.feature >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common_tests.feature >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common_ui.feature >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common_userdoc.feature >> oldList.txt
find ${oldcvsdir} -name org.eclipse.jem.util >> oldList.txt
find ${oldcvsdir} -name org.eclipse.jst.common.project.facet.core >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.command.env >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.command.env.core >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.command.env.infopop >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.command.env.ui >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.core >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.emf >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.emfworkbench.integration >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.environment >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.frameworks >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.frameworks.ui >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.infopop >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.modulecore >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.project.facet.core >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.project.facet.ui >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.snippets >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.ui >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.ui.properties >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.uriresolver >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.internet.cache >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.validation >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.validation.infopop >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.validation.ui >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.project.facet.core.tests >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.project.facet.ui.tests >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.snippets.tests >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.tests >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.tests.collector >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.tests.performance >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.common.tests.ui >> oldList.txt
find ${oldcvsdir} -name org.eclipse.wst.internet.cache.tests >> oldList.txt