#!/vin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# this file needs to be executed at top of /cvsroot/webtools directory
# and ... as root? 

chgrp -R webtools 				common
chgrp -R cvs 					CVSROOT
chgrp -R webtools 				datatools
chgrp -R webtools 				ejbtools
chgrp -R webtools 				jeetools
chgrp -R atf-dev 				org.eclipse.atf
chgrp -R jpa-dev 				org.eclipse.jpa
chgrp -R jsf-dev 				org.eclipse.jsf
chgrp -R webtools.incubator-dev org.eclipse.wtp.incubator 
chgrp -R webtools 				servertools
chgrp -R webtools 				sourceediting
chgrp -R webtools 				webservices
chgrp -R webtools 				webtools.maps
chgrp -R webtools 				webtools.releng
