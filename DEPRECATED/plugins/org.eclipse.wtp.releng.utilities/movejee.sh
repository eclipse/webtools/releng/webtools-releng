#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

. mainparams.shsource

. copyAndRemove.sh

echo;
echo "   performing $0";
echo;


oldBaseDir=${oldcvsdir}/jst/components/common/plugins
newBaseDir=${newcvsdir}/jeetools/plugins

wtpmodule=org.eclipse.jst.common.annotations.controller
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.common.annotations.core
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.common.annotations.ui
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.common.frameworks
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

newBaseDir=${newcvsdir}/jeetools/archive
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.sample.web.project 
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.common.navigator.java 
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.common.launcher.ant
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.common.frameworks.ui 




oldBaseDir=${oldcvsdir}/wst/components/web/plugins
newBaseDir=${newcvsdir}/jeetools/plugins
wtpmodule=org.eclipse.wst.web
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/wst/components/web/plugins
newBaseDir=${newcvsdir}/jeetools/plugins
wtpmodule=org.eclipse.wst.web.ui
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir


oldBaseDir=${oldcvsdir}/jst/components/jem
newBaseDir=${newcvsdir}/jeetools/plugins

wtpmodule=org.eclipse.jem
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jem.beaninfo
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jem.beaninfo.common
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jem.beaninfo.ui
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jem.proxy
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jem.ui
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jem.workbench
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/servlet/plugins
wtpmodule=org.eclipse.jst.servlet.ui
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/jem
newBaseDir=${newcvsdir}/jeetools/features
wtpmodule=org.eclipse.jem-feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/jem/tests
newBaseDir=${newcvsdir}/jeetools/features
wtpmodule=org.eclipse.jem.tests-feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/servlet/tests
newBaseDir=${newcvsdir}/jeetools/tests
wtpmodule=org.eclipse.jst.servlet.tests
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir


oldBaseDir=${oldcvsdir}/jst/components/jem/tests
newBaseDir=${newcvsdir}/jeetools/tests
wtpmodule=org.eclipse.jem.tests
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/ejb/features
newBaseDir=${newcvsdir}/jeetools/features
wtpmodule=org.eclipse.jst.enterprise_core.feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.enterprise_sdk.feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.enterprise_tests.feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.enterprise_ui.feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.enterprise_ui.feature.patch
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.enterprise_userdoc.feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.servlet.ui.patch
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/doc/features
newBaseDir=${newcvsdir}/jeetools
oldDir=${oldBaseDir}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/doc/plugins
newBaseDir=${newcvsdir}/jeetools/docs

wtpmodule=org.eclipse.jst.doc.isv
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.j2ee.doc.user
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.j2ee.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.jst.servlet.ui.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/wst/components/web/features
newBaseDir=${newcvsdir}/jeetools/features
wtpmodule=org.eclipse.wst.web_core.feature.patch
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.web_core.feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.web_sdk.feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.web_tests.feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.web_ui.feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

wtpmodule=org.eclipse.wst.web_userdoc.feature
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/wst/components/doc/plugins
newBaseDir=${newcvsdir}/jeetools/docs

wtpmodule=org.eclipse.wst.web.ui.infopop
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/j2ee
newBaseDir=${newcvsdir}/jeetools

wtpmodule=development
oldDir=${oldBaseDir}/${wtpmodule}
newDir=${newBaseDir}
copyAndRemove $oldDir $newDir

oldBaseDir=${oldcvsdir}/jst/components/j2ee/plugins
newBaseDir=${newcvsdir}/jeetools/plugins

copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee 
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.core
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.jca
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.jca.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.navigator.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.web
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.webservice
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.webservice.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.jee
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.jee.ui
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.jee.web

oldBaseDir=${oldcvsdir}/jst/components/j2ee/tests
newBaseDir=${newcvsdir}/jeetools/tests

copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.core.tests
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.core.tests.performance
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.tests
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.j2ee.tests.performance
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.validation.sample
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.validation.test

newBaseDir=${newcvsdir}/jeetools/archive
copyAndRemoveModule $oldBaseDir $newBaseDir org.eclipse.jst.validation.test.fwk

oldDir=${oldcvsdir}/jst/components/ejb/features/*
newDir=${newcvsdir}/jeetools/features
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/wst/components/web/features/*
newDir=${newcvsdir}/jeetools/features
copyAndRemove $oldDir $newDir
