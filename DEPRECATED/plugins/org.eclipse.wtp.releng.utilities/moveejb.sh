#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

. mainparams.shsource
. copyAndRemove.sh

echo;
echo "   performing $0";
echo;

oldDir=${oldcvsdir}/jst/components/ejb/plugins/org.eclipse.jst.ejb.ui
newDir=${newcvsdir}/ejbtools/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/ejb/plugins/org.eclipse.jst.j2ee.ejb.annotation.model
newDir=${newcvsdir}/ejbtools/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/ejb/plugins/org.eclipse.jst.j2ee.ejb.annotations.emitter
newDir=${newcvsdir}/ejbtools/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/ejb/plugins/org.eclipse.jst.j2ee.ejb.annotations.ui
newDir=${newcvsdir}/ejbtools/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/ejb/plugins/org.eclipse.jst.j2ee.ejb.annotations.xdoclet
newDir=${newcvsdir}/ejbtools/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/ejb/plugins/org.eclipse.jst.j2ee.xdoclet.runtime
newDir=${newcvsdir}/ejbtools/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/ejb/tests/org.eclipse.jst.j2ee.ejb.generation.tests
newDir=${newcvsdir}/ejbtools/tests
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/doc/plugins/org.eclipse.jst.ejb.doc.user
newDir=${newcvsdir}/ejbtools/docs
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/j2ee/plugins/org.eclipse.jst.j2ee.ejb
newDir=${newcvsdir}/ejbtools/plugins
copyAndRemove $oldDir $newDir

oldDir=${oldcvsdir}/jst/components/j2ee/plugins/org.eclipse.jst.jee.ejb
newDir=${newcvsdir}/ejbtools/plugins
copyAndRemove $oldDir $newDir

