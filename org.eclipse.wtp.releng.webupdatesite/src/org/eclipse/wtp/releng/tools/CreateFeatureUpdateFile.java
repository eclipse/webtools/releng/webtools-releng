/*******************************************************************************
 * Copyright (c) 2008, 2019 IBM Corporation and others.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.wtp.releng.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;

import org.eclipse.callisto.tools.utils.CommonXML;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class CreateFeatureUpdateFile {

	final private static String FEATURES_PROPERTY = "featuresLocation";
	final private static String EOL = System.getProperty("line.separator");
	final private static String PATH_SEPARATOR = System.getProperty("file.separator");

	private String rawUpdateSite;
	private String featuresDirectoryLocation;
	private FullJarNameParser fullJarNameParser = new FullJarNameParser();


	private String[] featuresOfInterest = new String[]{"org.eclipse.jpt.feature", "org.eclipse.jpt.eclipselink.feature", "org.eclipse.wst.xml_ui.feature", "org.eclipse.wst.jsdt.feature", "org.eclipse.wst.common_ui.feature", "org.eclipse.jst.webpageeditor.feature", "org.eclipse.jst.jsf.apache.trinidad.tagsupport.feature", "org.eclipse.jst.ws.axis2tools.feature", "org.eclipse.wst.xsl.feature", "org.eclipse.jst.ws.cxf.feature", "org.eclipse.jpt_sdk.feature", "org.eclipse.jpt.eclipselink_sdk.feature", "org.eclipse.wst.xml_sdk.feature", "org.eclipse.wst.jsdt_sdk.feature", "org.eclipse.wst.common_sdk.feature", "org.eclipse.jst.webpageeditor_sdk.feature", "org.eclipse.jst.jsf.apache.trinidad.tagsupport_sdk.feature", "org.eclipse.jst.ws.axis2tools_sdk.feature", "org.eclipse.wst.xsl_sdk.feature", "org.eclipse.jst.ws.cxf_sdk.feature",};

	private String[] shortListOfInterest = new String[]{"org.eclipse.jpt.feature", "org.eclipse.jpt.eclipselink.feature", "org.eclipse.wst.xml_ui.feature", "org.eclipse.wst.jsdt.feature", "org.eclipse.wst.common_ui.feature", "org.eclipse.jst.webpageeditor.feature", "org.eclipse.jst.jsf.apache.trinidad.tagsupport.feature", "org.eclipse.jst.ws.axis2tools.feature",};

	public static void main(String[] args) {

		int returnCode = 0;
		if (args.length != 2) {
			System.out.println("    Error: Program needs stream and rawUpdateSite");
		}
		else {
			try {
				returnCode = new CreateFeatureUpdateFile().createFeatureFiles(args);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			catch (SAXException e) {
				e.printStackTrace();
			}
		}
		System.exit(returnCode);
	}

	private int createFeatureFiles(String[] args) throws IOException, SAXException {
		int returnCode = 0;
		String stream = args[0].trim();
		rawUpdateSite = args[1].trim();
		if (!rawUpdateSite.endsWith(PATH_SEPARATOR)) {
			rawUpdateSite = rawUpdateSite + PATH_SEPARATOR;
		}
		featuresDirectoryLocation = rawUpdateSite + "features" + PATH_SEPARATOR;

		System.out.println("featuresDirectoryLocation: " + featuresDirectoryLocation);
		File featuresDirectory = new File(featuresDirectoryLocation);
		if (!featuresDirectory.exists()) {
			System.out.println("ERROR: no directory at location: " + featuresDirectoryLocation);
		}
		else {
			String[] featuresDirectories = featuresDirectory.list();
			if (featuresDirectories == null || featuresDirectories.length <= 0) {
				System.out.println("ERROR: no featues found at location: " + featuresDirectoryLocation);
				returnCode = 9;
			}
			else {
				Map currentFeatureVersions = getCurrentFeatures(featuresDirectories);
				if ("Galileo".equals(stream)) {
					String scfilename = rawUpdateSite + "webtools.build";
					System.out.println("Creating short List for Galileo site");
					System.out.println("   filename: " + scfilename);
					doListForGalileo(currentFeatureVersions, shortListOfInterest, scfilename);
					// update site.xml, incase some features are "new".
					// this is just needed for now, as long as we use site.xml
					// to define categories
					String sitexmlFileName = rawUpdateSite + "site.xml";
					updateSiteXml(currentFeatureVersions, sitexmlFileName);
				}
				else if ("Helios".equals(stream)) {
					String scfilename = rawUpdateSite + "webtools.build";
					System.out.println("Creating short List for Helios site");
					System.out.println("   filename: " + scfilename);
					doListForHelios(currentFeatureVersions, shortListOfInterest, scfilename);
					// update site.xml, incase some features are "new".
					// this is just needed for now, as long as we use site.xml
					// to define categories
					String sitexmlFileName = rawUpdateSite + "site.xml";
					updateSiteXml(currentFeatureVersions, sitexmlFileName);
				}
				else if ("Ganymede".equals(stream)) {
					String filename = "features-ganymede-wtp.xml";
					System.out.println("Creating full List for main WTP site");
					System.out.println("   filename: " + filename);
					doList(featuresDirectories, featuresOfInterest, filename);
					System.out.println();


					// System.out.println("Short List for Europa site");
					// System.out.println();
					// doList(featuresDirectories, shortListOfInterest);

					String scfilename = "wtp.sc";
					System.out.println("Creating short List for Ganymede site");
					System.out.println("   filename: " + scfilename);
					doList2(featuresDirectories, shortListOfInterest, scfilename);
				}
				else if ("Europa".equals(stream)) {
					String filename = "features-europa-wtp.xml";
					System.out.println("Creating full List for main WTP site");
					System.out.println("   filename: " + filename);
					doList(featuresDirectories, featuresOfInterest, filename);
					System.out.println();

				}
				else if ("Patches".equals(stream)) {
					String filename = "features-ganymede-patches.xml";
					System.out.println("Creating full List for main WTP site");
					System.out.println("   filename: " + filename);
					doList(featuresDirectories, featuresOfInterest, filename);
					System.out.println();

				}
				else {
					throw new RuntimeException("Illegal stream: " + stream);
				}
			}
		}
		return returnCode;
	}

	private void updateSiteXml(Map currentFeatureVersions, String sitexmlFileName) throws IOException, SAXException {
		FileOutputStream outfile = null;
		Document siteDOM = getSiteDOM(sitexmlFileName);

		try {

			Document newDOM = updateSiteFeatureDOM(siteDOM, currentFeatureVersions);

			// we'll write over what we read it
			outfile = new FileOutputStream(sitexmlFileName);
			CommonXML.serialize(newDOM, outfile);

			System.out.println("\tSite.xml output to: " + sitexmlFileName); //$NON-NLS-1$

		}
		finally {
			if (outfile != null) {
				outfile.close();
			}
		}
	}


	private Document updateSiteFeatureDOM(Document siteDOM, Map currentFeatureVersions) {
		NodeList features = siteDOM.getElementsByTagName("feature");

		for (int i = 0; i < features.getLength(); i++) {
			Node featureElement = features.item(i);

			NamedNodeMap attributes = featureElement.getAttributes();

			Node featureURLNode = attributes.getNamedItem("url");
			String featureURL = featureURLNode.getNodeValue();

			Node featureIDNode = attributes.getNamedItem("id");
			String featureID = featureIDNode.getNodeValue();

			Node featureVersionNode = attributes.getNamedItem("version");
			String featureVersion = featureVersionNode.getNodeValue();

			// check if still has substitutable value
			if (featureVersion.equals("${" + featureID + "}")) {
				String currentVersion = (String) currentFeatureVersions.get(featureID);
				System.out.println("Substitutable value found: ");
				System.out.println("featureID  " + featureID);
				
				if (currentVersion != null && currentVersion.length() > 0) {
					System.out.println("currentVersion  " + currentVersion);
					featureVersionNode.setNodeValue(currentVersion);
					String newfeatureURL = featureURL.replace("${" + featureID + "}", currentVersion);
					featureURLNode.setNodeValue(newfeatureURL);
				} else {
					System.out.println("     but no version in this build, so no substitution performed");
				}
			}

		}
		return siteDOM;


	}

	private void doList(String[] featureDirectories, String[] featureOfFocus, String filename) throws IOException {
		File outFile = new File(filename);
		Writer output = new FileWriter(outFile);
		try {
			output.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + EOL + "<project" + EOL + "    name=\"update\"" + EOL + "    default=\"update\">" + EOL + "    <property" + EOL + "        environment=\"env\" />" + EOL + "    <target" + EOL + "        name=\"update\">" + EOL + "        <echo" + EOL + "            message=\"-------- Project: WTP Responsible: david_williams\" />" + EOL + "		<!-- intentionally no site.xml in this particular site -->" + EOL + "        <!--" + EOL + "            don't forget, for now have to first manually copy xsl" + EOL + "            bundles to 3.0 site" + EOL + "        -->" + EOL + "        <property" + EOL + "            name=\"from.update.site\"" + EOL + "            value=\"file://" + rawUpdateSite + "\" />" + EOL + "        <echo" + EOL + "            message=\"   pulling update jars from ${from.update.site}\" />" + EOL);
			for (int i = 0; i < featureDirectories.length; i++) {
				String directoryName = featureDirectories[i];
				fullJarNameParser.parse(directoryName);
				String projectName = fullJarNameParser.getProjectString();
				String versionString = fullJarNameParser.getVersionString();
				if (contains(projectName, featureOfFocus) || projectName.endsWith(".patch")) {
					output.write("        <ant antfile=\"updateMirrorProject.xml\">" + EOL + "            <property" + EOL + "                name=\"featureId\"" + EOL + "                value=\"" + projectName + "\" />" + EOL + "            <property" + EOL + "                name=\"version\"" + EOL + "                value=\"" + versionString + "\" />" + EOL + "        </ant>" + EOL);

				}
			}
			output.write("    </target>" + EOL + "</project>");
		}
		finally {
			if (output != null) {
				output.close();
			}
		}
	}

	private boolean contains(String needle, String[] haystack) {
		boolean result = false;

		for (int i = 0; i < haystack.length; i++) {
			if (needle.equals(haystack[i])) {
				result = true;
				break;
			}
		}
		return result;
	}

	private void doList2(String[] featureDirectories, String[] featureOfFocus, String filename) throws IOException {

		String scfilehead = "<?xml version='1.0' encoding=\"UTF-8\"?>" + EOL + "<sc:siteContribution" + EOL + "    xmlns=\"http://www.eclipse.org/buckminster/CSpec-1.0\"" + EOL + "    xmlns:sc=\"http://www.eclipse.org/buckminster/SiteContribution-1.0\"" + EOL + "    updateSite=\"${downloads}/webtools/tempTestUpdates/site.xml\"" + EOL + "    projectId=\"wtp\">" + EOL + "    <sc:member" + EOL + "        name=\"David Williams\"" + EOL + "        email=\"david_williams@us.ibm.com\" />" + EOL + "    <sc:cspec" + EOL + "        name=\"org.eclipse.wtp-sc\">" + EOL + "        <dependencies>" + EOL;

		String scfiletail = "        </dependencies>" + EOL + "        <groups>" + EOL + "            <public" + EOL + "                name=\"Web and Java EE Development\">" + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.wst.xml_ui.feature\" />" + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.wst.jsdt.feature\" />    " + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.wst\" />" + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.jst\" />" + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.jpt.feature\" />" + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.jpt.eclipselink.feature\" />" + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.jst.webpageeditor.feature\" />   " + EOL + "                <attribute" +
					EOL + "                    component=\"org.eclipse.jst.jsf.apache.trinidad.tagsupport.feature\" />   " + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.jst.ws.axis2tools.feature\" />   " + EOL + "            </public>" + EOL + "            <public" + EOL + "                name=\"Enabling Features\">" + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.wst.common_ui.feature\" />" + EOL + "            </public>" + EOL + "            <public" + EOL + "                name=\"Java Development\">" + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.jpt.feature\" />" + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.jpt.eclipselink.feature\" />" + EOL + "                <attribute" + EOL + "                    component=\"org.eclipse.jst\" />" + EOL + "            </public>" + EOL + "        </groups>" + EOL +
					"    </sc:cspec>" + EOL + "</sc:siteContribution>";

		File outFile = new File(filename);
		Writer output = new FileWriter(outFile);
		try {
			output.write(scfilehead);

			for (int i = 0; i < featureDirectories.length; i++) {
				String directoryName = featureDirectories[i];
				fullJarNameParser.parse(directoryName);
				String projectName = fullJarNameParser.getProjectString();
				String versionString = fullJarNameParser.getVersionString();
				if (contains(projectName, featureOfFocus) || projectName.endsWith(".patch")) {
					output.write("\t\t\t" + "<dependency name=\"" + projectName + "\" versionDesignator=\"[" + versionString + "]\" />" + EOL);
				}
			}
			output.write(scfiletail);
		}
		finally {
			if (output != null) {
				output.close();
			}
		}

	}

	private void doListForGalileo(Map currentFeatureVersions, String[] featureOfFocus, String filename) throws IOException {

		String galeleoTemplateFile = "galileo.webtools.build.xml";
		createBuildFile(currentFeatureVersions, filename, galeleoTemplateFile);

	}

	private void doListForHelios(Map currentFeatureVersions, String[] featureOfFocus, String filename) throws IOException {

		String galeleoTemplateFile = "helios.webtools.build.xml";
		createBuildFile(currentFeatureVersions, filename, galeleoTemplateFile);

	}

	private void createBuildFile(Map currentFeatureVersions, String filename, String templateFile) throws IOException, FileNotFoundException {
		FileOutputStream outfile = null;
		Document templateDom = getTemplateDOM(templateFile);

		try {

			Document newDOM = updateFeatureDOM(templateDom, currentFeatureVersions);


			outfile = new FileOutputStream(filename);
			CommonXML.serialize(newDOM, outfile);

			System.out.println("\tOutput to: " + filename); //$NON-NLS-1$

		}
		finally {
			if (outfile != null) {
				outfile.close();
			}
		}
	}

	private Document updateFeatureDOM(Document startingDOM, Map currentFeatureVersions) {
		NodeList features = startingDOM.getElementsByTagName("features");

		for (int i = 0; i < features.getLength(); i++) {
			Node featureElement = features.item(i);

			NamedNodeMap attributes = featureElement.getAttributes();
			Node featureNameNode = attributes.getNamedItem("id");
			String featureName = featureNameNode.getNodeValue();
			System.out.println(featureName);
			String currentVersion = (String) currentFeatureVersions.get(featureName);
			System.out.println(currentVersion);
			Node versionNode = attributes.getNamedItem("version");
			versionNode.setNodeValue(currentVersion);
		}
		return startingDOM;
	}

	private Map getCurrentFeatures(String[] featureDirectories) {
		Map currentFeatureVersions = new HashMap();
		for (int i = 0; i < featureDirectories.length; i++) {
			String directoryName = featureDirectories[i];
			fullJarNameParser.parse(directoryName);
			String projectName = fullJarNameParser.getProjectString();
			String versionString = fullJarNameParser.getVersionString();
			currentFeatureVersions.put(projectName, versionString);
		}
		return currentFeatureVersions;
	}

	private Document getTemplateDOM(String templateFileName) throws IOException {



		InputStream templateFile = this.getClass().getResourceAsStream(templateFileName);

		DocumentBuilder documentBuilder = CommonXML.getDocumentBuilder();
		Document templateDom = null;
		try {
			templateDom = documentBuilder.parse(templateFile);
		}
		catch (SAXException e) {
			e.printStackTrace();
		}
		finally {
			if (templateFile != null) {
				templateFile.close();
			}
		}
		return templateDom;

	}

	private Document getSiteDOM(String siteFileName) throws IOException, SAXException {


		File siteFile = new File(siteFileName);

		DocumentBuilder documentBuilder = CommonXML.getDocumentBuilder();
		Document templateDom = null;
		templateDom = documentBuilder.parse(siteFile);
		return templateDom;

	}
}
