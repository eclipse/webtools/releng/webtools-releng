#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# This script should be "copied up" and ran from a
# directory "above" the main working directories of
# ${updateToolsDir}.
# This is typically the users ${HOME} directory

# have to code this here, not include, 
# since normally the properties.shsource is in a different directory
updateToolsDir=org.eclipse.wtp.releng.webupdatesite
relengProject=webtools.releng

echo " "
echo "   Removing old ${updateToolsDir} and exporting fresh HEAD versions." 
echo " "

rm -fr ${updateToolsDir}
echo " "

# ISSUE: we should move away from using 'head' version
# Note: we can use "local host" since we are running on the same cluser as the cvs server
# otherwise, if running remotely, localhos should be changed to 'dev.eclpise.org'
cvs -Q -f -d :pserver:anonymous@dev.eclipse.org:/cvsroot/webtools export -d ${updateToolsDir} -r HEAD ${relengProject}/${updateToolsDir}

dos2unix -q ${updateToolsDir}/build-home/*.sh
dos2unix -q ${updateToolsDir}/build-home/*.xml
chmod -R +x ${updateToolsDir}/build-home/*.sh


echo "    for complete refresh, use something similar to following"
echo " " 
echo "    screen -D -RR -h 5000"
echo " " 
echo "    cd ${updateToolsDir}/build-home/"
echo " " 
echo "    # for Galilo"
echo "    ./runUpdateAllProjects.sh -vcs Galileo -b <buildrepositorypath> | tee out.txt"
echo " " 
echo "    # for Galilo"
echo "    ./runUpdateAllProjects.sh -vcs Galileo -b ~/downloads/webtools/downloads/drops/R3.1/M-3.1M-20090903145215/repository | tee out.txt"
echo " "
echo "    # for Helios"
echo "    ./runUpdateAllProjects.sh -vcs Helios -b <buildrepositorypath> | tee out.txt"
echo " " 
echo "    # for Helios"
#echo "    ./runUpdateAllProjects.sh -vcs Helios -b ${HOME}/downloads/webtools/downloads/drops/R3.2/S-3.2M1-20090813235332/repository | tee out.txt"
echo "    ./runUpdateAllProjects.sh -vcs Helios -b /shared/webtools/projects/wtp-R3.3-I/workdir/I-3.3I-20090814141102/repository | tee out.txt"
echo " "
echo "    When complete, be sure to inspect or at least grep the out.txt file for anomilies, e.g. "
echo "        grep failed     out.txt"
echo "        grep error      out.txt"
echo "        grep exception  out.txt"
echo " "

