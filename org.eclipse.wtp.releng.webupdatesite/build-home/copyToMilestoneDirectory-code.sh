#!/bin/sh
#*******************************************************************************
# Copyright (c) 2006, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# script to copy update jars from their staging area to the releases area

if [ -z $BUILD_INITIALIZED ]
then
 source properties.shsource
 source parseSiteArg.shsource $stream
 export BUILD_INITIALIZED=1
fi

if [ "${2}" != "-doit" ] ; then
   echo ""
   echo "    This is a dry run. Add -doit to actually copy"
   echo ""
   doit="--dry-run"
else
   doit=""
fi

echo ""
echo "    Copying new plugins and features "
echo "        from  ${testUpdateSite}"
echo "          to  ${milestoneDirectory}"
echo ""

rsync ${doit}  --ignore-existing -rvtup ${testUpdateSite}/features ${milestoneDirectory}
rsync ${doit}  --ignore-existing -rvtup ${testUpdateSite}/plugins ${milestoneDirectory}

if [ "${doit}" = "--dry-run" ] ; then
   echo ""
   echo "    This was a dry run. Add -doit to actually copy"
   echo ""
   exit 1
fi



