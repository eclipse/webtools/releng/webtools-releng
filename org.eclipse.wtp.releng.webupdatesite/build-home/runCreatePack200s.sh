#!/bin/sh
#*******************************************************************************
# Copyright (c) 2006, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# script to run update, setting properties, for all projects

function usage() {
    printf "\n\tUsage: %s [-h] | [-v] [-c] -s stream [-t | -m | -r]" $(basename $0) >&2
    printf "\n\t\t%s" "where "
    printf "\n\t\t\t%s\t%s" "-h" "help" >&2
    printf "\n\t\t\t%s\t%s" "-v" "verbose" >&2
    printf "\n\t\t\t%s\t%s" "-c" "clean" >&2
    printf "\n\t\t\t%s\t%s" "-s" "steam" >&2
    printf "\n\t\t\t\t\t%s" "where stream is Ganymede, Europa, etc" >&2
    printf "\n\t\t\t%s\t%s" "-t" "use the temp test update site for this stream" >&2
    printf "\n\t\t\t%s\t%s" "-m" "use the milestone update site for this stream" >&2
    printf "\n\t\t\t%s\t%s" "-r" "use the release update site for this stream" >&2
    printf "\n\n"  >&2
}

  source properties.shsource
  
verbose=
clean=
stream=
toUpdateSite=
while getopts 'hvcs:tmr' OPTION
do
    case $OPTION in
        h)    usage
        exit 1
        ;;
        v)    export verbose=1
        ;;
        c)    export clean=1
        ;;
        s)    stream=$OPTARG
              source parseSiteArg.shsource $stream
        ;;   
        t)    toUpdateSite=${testUpdateSite}
        ;;   
        m)    toUpdateSite=${milestoneDirectory}
        ;;
        r)    toUpdateSite=${releaseDir}
        ;;
        ?)    usage
        exit 2
        ;;
    esac
done

shift $(($OPTIND - 1))

if [ -z $stream ]
then
  echo ""
  echo "    Need a stream argument"
  usage
  exit 1
fi 

if [ -z $toUpdateSite ]
then
  echo ""
  echo "    Need a toUpdateSite argument"
  usage
  exit 1
fi 

if [ verbose ] 
then 
  printf "\n\tstream: " "${stream}" >&2
  printf "\n\tto spdate site: : " "${toUpdateSite}" >&2
  
fi

export BUILD_INITIALIZED=1

./ant.sh -f createPack200s.xml -Declipse.home=${eclipseLocation} -Dupdate.site=${toUpdateSite}

