#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

source properties.shsource

if [ "$1" != "-doit" ] ; then
   echo "    This is a dry run. Add -doit to actually remove"
   doit="--dry-run"
else
   doit=""
fi

export FROM_UPDATE_SITE=~/downloads/webtools/milestones
export TO_UPDATE_SITE=~/downloads/webtools/updates_mirror

echo "Number of jars before cleaning: "
find ${FROM_UPDATE_SITE} -name "*.jar" | wc -l 

./ant.sh -f createMirrorSite.xml -Declipse.home=${eclipseLocation}

rsync $doit --delete --ignore-existing -rv ${TO_UPDATE_SITE}/plugins ${FROM_UPDATE_SITE}

rsync $doit --delete --ignore-existing -rv ${TO_UPDATE_SITE}/features ${FROM_UPDATE_SITE}

if [ "$doit" = "--dry-run" ] ; then
   echo "    This was a dry run. Add -doit to actually remove"
else
   echo "Number of jars after cleaning: "
   find ${FROM_UPDATE_SITE} -name "*.jar" | wc -l 
fi
