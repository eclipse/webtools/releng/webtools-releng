#!/bin/sh
#*******************************************************************************
# Copyright (c) 2007, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# script to run update, setting properties, for all projects


function usage() {
    printf "\n\tUsage: %s [-h] | [-v] [-c] -s stream -b buildrepositorypath"  $(basename $0) >&2
    printf "\n\t\t%s\n\n" "where h==help, v==verbose, c==clean and s==stream, where steam is Helios, Galileo, Ganymede, etc., " >&2
    printf "\n\t\t%s\n\n" "   and b is absolute path to directory of update site repository " >&2
     
}


clean=
stream=
verbose=
while getopts 'hvcs:b:' OPTION
do
    case $OPTION in
        h)    usage
        exit 1
        ;;
        c)    export clean=1
        ;;
        v)    export verbose=1
        ;;
        s)    stream=$OPTARG
        ;;     
        b)    buildrepositorypath=$OPTARG
        ;;   
        ?)    usage
        exit 2
        ;;
    esac
done

shift $(($OPTIND - 1))

if [ -z $stream ]
then
  echo ""
  echo "    Need a stream argument"
  usage
  exit 1
fi 

echo "     Stream: " $stream

if [ -z $BUILD_INITIALIZED ]
then
  source properties.shsource
  source parseSiteArg.shsource $stream
  export BUILD_INITIALIZED=1
fi


if [ $clean ] ; then

	 echo "   Removing ${testUpdateSite}/features/"
	 rm -fr ${testUpdateSite}/features/
	
	 echo "   Removing ${testUpdateSite}/plugins/"
	 rm -fr ${testUpdateSite}/plugins/
	
	 echo "   Removing index and site xml files"
	 rm -fr ${testUpdateSite}/*

fi

WTP_DEFAULT=I-3.2.0-20091119201510

# if not provided, we'll use the one coded here	
if [ -z "${buildrepositorypath}" ] 	
then
   echo "Using default build repository path" 
   buildrepositorypath=${HOME}/downloads/webtools/downloads/drops/R3.2.0/"${WTP_DEFAULT}"/repository/
else
   echo "Using the provided build repository path"
   echo "buildrepositorypath: " "${buildrepositorypath}"
fi


if [ -d "${buildrepositorypath}" ]	
then
       echo "   Updating index and web site files, and site.xml file"
       rsync -vrp "${buildrepositorypath}"/* "${testUpdateSite}"
else
       echo "  build repo directory does not exisit"
       echo "  buildrepositorypath: $buildrepositorypath"
       exit 99
fi

rsync -vp ${HOME}/${updateToolsDir}/WebContent/${mainIndexFileName} ${testUpdateSite}/index.html
rsync -vp ${HOME}/${updateToolsDir}/WebContent/*.jpg ${testUpdateSite}

echo "add capabilities to repository"

CAP_BUILD=20090622054830/I-3.2I-20090622054830

rsync -vrp /shared/webtools/committers/wtpcap-R3.2-I/"${CAP_BUILD}"/repository/features/* "${testUpdateSite}"/features/
rsync -vrp /shared/webtools/committers/wtpcap-R3.2-I/"${CAP_BUILD}"/repository/plugins/* "${testUpdateSite}"/plugins/

echo "add xml-security to repository"

XMLSEC_BUILD=20100126050534/I-I20100126050534-20100126050534

rsync -vrp /shared/webtools/committers/xmlsec-R0.5-I/"${XMLSEC_BUILD}"/repository/features/* "${testUpdateSite}"/features/
rsync -vrp /shared/webtools/committers/xmlsec-R0.5-I/"${XMLSEC_BUILD}"/repository/plugins/* "${testUpdateSite}"/plugins/


./createMirrorFile.sh -s "$stream" -b "${testUpdateSite}"

if [ "$?" -ne 0 ] 
then 
   exit $?
fi  

# re-create P2 repository metadata
./ant.sh  -f createP2Repo.xml  -Declipse.home=${eclipseLocation} -Dupdate.site=${testUpdateSite}  

