#!/bin/sh
#*******************************************************************************
# Copyright (c) 2008, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# script to run update digests, meta data, and pack 200 all the jars. 
# this is a handy script to use if/when the site is partially created "by hand", 
# and then need to finalize. 


function usage() {
    printf "\n\tUsage: %s [-h] | [-v] -s stream" $(basename $0) >&2
    printf "\n\t\t%s\n\n" "where h==help, v==verbose, c==clean and s==stream, where steam is Ganymede, Europa, etc" >&2 
}


clean=
stream=
verbose=
while getopts 'hvs:' OPTION
do
    case $OPTION in
        h)    usage
        exit 1
        ;;
        v)    export verbose=1
        ;;
        s)    stream=$OPTARG
        ;;     
        ?)    usage
        exit 2
        ;;
    esac
done

shift $(($OPTIND - 1))

if [ -z $stream ]
then
  echo ""
  echo "    Need a stream argument"
  usage
  exit 1
fi 

echo "     Stream: " $stream

if [ -z $BUILD_INITIALIZED ]
then
  source properties.shsource
  source parseSiteArg.shsource $stream
  export BUILD_INITIALIZED=1
fi

# create P2 repository metadata
./ant.sh  -f createP2Repo.xml       -Declipse.home=${eclipseLocation} -Dupdate.site=${releaseDir}  

if [[ $? > 0 ]] 
then 
   exit $?
fi


# do create pack's later ... takes a long time
./ant.sh -f createPack200s.xml      -Declipse.home=${eclipseLocation} -Dupdate.site=${releaseDir}

# create P2 repository metadata
./ant.sh  -f createP2Repo.xml       -Declipse.home=${eclipseLocation} -Dupdate.site=${releaseDir}  
