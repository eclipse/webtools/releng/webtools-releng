#!/bin/sh
#*******************************************************************************
# Copyright (c) 2009, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# script to run merge ant script


source properties.shsource

repoSource=/home/data/httpd/download.eclipse.org/webtools/downloads/drops/R0.5/I-I20091213171835-20091213171835/repository 
repoDestination=/home/data/httpd/download.eclipse.org/webtools/milestones

./ant.sh -f runAntRunner.xml -Dantrunnerfile=merge-xml-sec.xml -Declipse.home=${eclipseLocation} -DrepoSource=${repoSource} -DrepoDestination=${repoDestination}

