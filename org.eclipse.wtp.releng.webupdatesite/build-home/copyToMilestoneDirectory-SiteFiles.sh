#!/bin/sh
#*******************************************************************************
# Copyright (c) 2008, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************

# script to copy site files 

if [ -z $BUILD_INITIALIZED ]
then
 source properties.shsource
 source parseSiteArg.shsource $stream
 export BUILD_INITIALIZED=1
fi

# important: make sure digest is updated same time as site.xml's
# TODO: should digest be recreated on final site? No for "exact" sites, yes, if "merged" sites. 
#rsync -p ${testUpdateSite}/digest.zip ${milestoneDirectory}


rsync -p ${testUpdateSite}/*.html ${milestoneDirectory}
rsync -p ${testUpdateSite}/*.jpg ${milestoneDirectory}
rsync -p ${testUpdateSite}/*.xml ${milestoneDirectory}
rsync -p ${testUpdateSite}/*.build ${milestoneDirectory}

# Typically, as of this writing
# Europa: 'testTempUpdates' to 'milestones' 


# compute last segment in directory names, as a heurisic to update internal URL links
fromLastSegment=${testUpdateSite##*/}
toLastSegment=${milestoneDirectory##*/}

echo ""
echo "  Updating site files:"
echo "    from: ${fromLastSegment}"
echo "      to: ${toLastSegment}"
echo ""

fromString="webtools/${fromLastSegment}"
toString="webtools/${toLastSegment}"
replaceCommand="s!${fromString}!${toString}!g"

echo "replaceCommand: ${replaceCommand}"

perl -pi -w -e ${replaceCommand} ${milestoneDirectory}/*.html
perl -pi -w -e ${replaceCommand} ${milestoneDirectory}/*.xml

# create P2 repository metadata
./ant.sh  -f createP2Repo.xml  -Declipse.home=${eclipseLocation} -Dupdate.site=${milestoneDirectory}  


