#!/bin/sh
#*******************************************************************************
# Copyright (c) 2008, 2019 IBM Corporation and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
#
# Script to promote the latest build in the specified committers area
#

function usage() {
    printf "\n\tUsage: %s [-h] | [-v] -b buildrepositorypath " $(basename $0) >&2
    printf "\n\t\t%s\n\n" "where <buildrepositorypath> is similar to /shared/webtools/committers/wtp-R3.0-R/20080611172140/R-3.0-20080611172140/repository" >&2 
}

while getopts 'vhs:b:' OPTION
do
    case $OPTION in
        h)    usage
        exit 1
        ;;
        v)    verboseFlag=1
        ;;
        s)    stream=$OPTARG
        ;;
        b)    buildrepositorypath=$OPTARG
        ;;
        ?)    usage
        exit 2
        ;;
    esac
done

shift $(($OPTIND - 1))



# check we have the buildrepositorypath
if [ -z $buildrepositorypath ]
then
    printf "\n\t%s\n"   "Error: buildrepositorypath is required." >&2
    usage
    exit 1
fi 

echo "buildrepositorypath: " $buildrepositorypath

if [ -z $BUILD_INITIALIZED ]
then
  source properties.shsource
  export BUILD_INITIALIZED=1
fi

${JAVA_HOME}/bin/java -jar featureFileCreate.jar $stream $buildrepositorypath

if [ "$?" -ne "0" ] 
then 
   exit $?
fi  

# copy to the build repository just for safe keeping, to be used later, if needed
if [ -e wtp.sc ] 
then
  cp wtp.sc $buildrepositorypath
fi 

if [ -e features-ganymede-wtp.xml ]
then  
  cp features-ganymede-wtp.xml $buildrepositorypath
fi

if [ -e features-europa-wtp.xml ]
then  
  cp features-europa-wtp.xml $buildrepositorypath
fi

