<?xml version='1.0' encoding="UTF-8"?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/webtools/preReleaseUpdates/site.xml"
    projectId="wtp">
    <sc:member
        name="David Williams"
        email="david_williams@us.ibm.com" />
    <sc:cspec
        name="org.eclipse.wtp-sc">
        <dependencies>
			<dependency name="org.eclipse.wst.jsdt.feature" versionDesignator="[1.0.1.v200807220139-6-6BcMAAwAsOKHgML]" />
			<dependency name="org.eclipse.jpt.feature" versionDesignator="[2.0.100.v200807280200-793DqCYQCD4CwFhCICC]" />
			<dependency name="org.eclipse.wst.xml_ui.feature" versionDesignator="[3.0.1.v200807220139-7F2EN8Cwum5N69_5QlJ2RckfDPMb]" />
			<dependency name="org.eclipse.wst" versionDesignator="[3.0.2.v200808250510-7A-8c8QqU3lw4VSXCJ09ZNa-HB8a]" />
			<dependency name="org.eclipse.wst.common_ui.feature" versionDesignator="[3.0.1.v200807220139-7C78ELWE8VrRVorKr-jb1AsObZCW]" />
			<dependency name="org.eclipse.jst.ws.axis2tools.feature" versionDesignator="[1.0.1.v200807092124-77-E_BCYQCD4CyNjOPRU]" />
			<dependency name="org.eclipse.jst" versionDesignator="[3.0.2.v200808250510-7U-8k9LJ8Lo4rV082dQfryShz-38]" />
			<dependency name="org.eclipse.jpt.eclipselink.feature" versionDesignator="[2.0.0.v200806090000-3--8s733I3D683333]" />
			<dependency name="org.eclipse.jst.webpageeditor.feature" versionDesignator="[2.0.1.v20080811-32-9oA55S5M5J]" />
			<dependency name="org.eclipse.jst.jsf.apache.trinidad.tagsupport.feature" versionDesignator="[2.0.1.v200808071446-1-8Y7w311919182557]" />
        </dependencies>
        <groups>
            <public
                name="Web and Java EE Development">
                <attribute
                    component="org.eclipse.wst.xml_ui.feature" />
                <attribute
                    component="org.eclipse.wst.jsdt.feature" />    
                <attribute
                    component="org.eclipse.wst" />
                <attribute
                    component="org.eclipse.jst" />
                <attribute
                    component="org.eclipse.jpt.feature" />
                <attribute
                    component="org.eclipse.jpt.eclipselink.feature" />
                <attribute
                    component="org.eclipse.jst.webpageeditor.feature" />   
                <attribute
                    component="org.eclipse.jst.jsf.apache.trinidad.tagsupport.feature" />   
                <attribute
                    component="org.eclipse.jst.ws.axis2tools.feature" />   
            </public>
            <public
                name="Enabling Features">
                <attribute
                    component="org.eclipse.wst.common_ui.feature" />
            </public>
            <public
                name="Java Development">
                <attribute
                    component="org.eclipse.jpt.feature" />
                <attribute
                    component="org.eclipse.jpt.eclipselink.feature" />
                <attribute
                    component="org.eclipse.jst" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>